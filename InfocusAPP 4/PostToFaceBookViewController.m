//
//  PostToFaceBookViewController.m
//  BusinessCard
//
//  Created by STEVEN ABRAMS on 7/19/14.
//  Copyright (c) 2014 Elixir Software group. All rights reserved.
//

#import "PostToFaceBookViewController.h"
#import "Social/Social.h"
#import "Flurry.h"
#import "QuantcastMeasurement.h"

@interface PostToFaceBookViewController ()


@property (strong, nonatomic) IBOutlet UIView *postFaceBookView;
@property (strong, nonatomic) IBOutlet UITextView *faceBookTextView;
@property (strong, nonatomic) IBOutlet UIButton *neverPostButton;
@property (strong, nonatomic) IBOutlet UIButton *laterPostButton;
@property (strong, nonatomic) IBOutlet UIButton *faceBookButton;
@property (strong, nonatomic) IBOutlet UIButton *tweetPostButton;

- (IBAction)neverPostButton_Clicked:(id)sender;
- (IBAction)laterPostButton_Clicked:(id)sender;
- (IBAction)faceBookButton_Clicked:(id)sender;
- (IBAction)tweetPostButton_Clicked:(id)sender;

@end

@implementation PostToFaceBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.faceBookTextView.textAlignment = NSTextAlignmentJustified;
    
    
    //Setup & Show View
    self.postFaceBookView.hidden = NO;
    self.postFaceBookView.layer.cornerRadius = 5;
    self.postFaceBookView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.postFaceBookView.layer.shadowOpacity = 0.5;
    self.postFaceBookView.layer.shadowRadius = 5.0;
    self.postFaceBookView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    
    
    //Custom "Never" Button
    [self.neverPostButton setTitle:@"Never" forState:UIControlStateNormal];
    [self.neverPostButton setTitleColor:[UIColor colorWithRed:220.0/256.0 green:220.0/256.0 blue:220.0/256.0 alpha:1.0] forState:UIControlStateNormal];
    [self.neverPostButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.neverPostButton.layer.cornerRadius = 10.0;
    self.neverPostButton.layer.borderWidth = 1.0;
    self.neverPostButton.layer.borderColor = [[UIColor colorWithRed:220.0/256.0 green:220.0/256.0 blue:220.0/256.0 alpha:1.0]CGColor];
    self.neverPostButton.backgroundColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    
    
    //Custom Later" Button
    [self.laterPostButton setTitle:@"Later" forState:UIControlStateNormal];
    [self.laterPostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.laterPostButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.laterPostButton.layer.cornerRadius = 10.0;
    self.laterPostButton.layer.borderWidth = 1.0;
    self.laterPostButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.laterPostButton.backgroundColor = [UIColor darkGrayColor];
    
    //Custom "Facebook" Button
    [self.faceBookButton setTitle:@"Facebook" forState:UIControlStateNormal];
    [self.faceBookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.faceBookButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.faceBookButton.layer.cornerRadius = 10.0;
    self.faceBookButton.layer.borderWidth = 1.0;
    self.faceBookButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.faceBookButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    //Custom "Twitter" Button
    [self.tweetPostButton setTitle:@"Twitter" forState:UIControlStateNormal];
    [self.tweetPostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.tweetPostButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.tweetPostButton.layer.cornerRadius = 10.0;
    self.tweetPostButton.layer.borderWidth = 1.0;
    self.tweetPostButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.tweetPostButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    
}


-(void) viewWillAppear:(BOOL)animated
{
    
    if (IS_IPHONE_5) {
        
        self.view.frame = CGRectMake(0, 0, 320, 568);
        
        if ( (self.view.superview.bounds.size.width == 568)) //Adjust Frame & bounds if Superview changes
        {
            self.view.bounds = CGRectMake(0, 0, 568, 320);
            self.view.frame = CGRectMake(0, 0, 568, 320);
        }
    }
    else{
        
        self.view.frame = CGRectMake(0, 0, 320, 480);
        
        if ( (self.view.superview.bounds.size.width == 480)) //Adjust Frame & bounds if Superview changes
        {
            self.view.bounds = CGRectMake(0, 0, 480, 320);
            self.view.frame = CGRectMake(0, 0, 480, 320);
        }
    }

    
    //NSLog(@"superview %@",self.view.superview.description);
    //NSLog(@"self.view.bounds 2 =  %@", NSStringFromCGRect(self.view.bounds));
    //NSLog(@"self.view.frame 2 =  %@", NSStringFromCGRect(self.view.frame));
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)neverPostButton_Clicked:(id)sender {
    
    [Flurry logEvent:@"Popup - Never PostToSocial"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Never PostToSocial" withLabels:nil];
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    //[self.rateAppView_MainView.layer addAnimation:transition forKey:kCATransitionFade];
    [self.view.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.view.hidden = YES;
    
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"PostToSocialNetworks"];  //so app is never posted to Social Network
    [sharedDefaults synchronize];
}

- (IBAction)laterPostButton_Clicked:(id)sender {
    
    [Flurry logEvent:@"Popup - Later PostToSocial"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Later PostToSocial" withLabels:nil];
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.view.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.view.hidden = YES;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
    [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //so app can be rated later
    [sharedDefaults synchronize];
}

- (IBAction)faceBookButton_Clicked:(id)sender {
    
    [Flurry logEvent:@"Popup - Facebook Post"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Facebook Post" withLabels:nil];
    

    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    SLComposeViewController *fbController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result)
        {
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
//*********** Bug with new facebook update. Never shows Cancel
                    NSLog(@"Cancelled.....");
                    [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Facebook was Cancelled
                    [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
                    
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                    [sharedDefaults setBool:NO forKey:@"PostToSocialNetworks"];  //so won't ask to post again
                }
                    break;
            }};

        [fbController setCompletionHandler:completionHandler];
        

        //[fbController setInitialText:@"Life is short. You too can be more productive, get your work done faster, make more money and enjoy your free time more often. All you need are the right tools. Find out more "];
        //[fbController addImage:[UIImage imageNamed:@"FaceBookDogAd2.jpg"]];
        [fbController addURL:[NSURL URLWithString:@"http://bit.ly/1fUMWWN"]];
        [self presentViewController:fbController animated:YES completion:nil];
    }
    else //Facebook is not on device
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FaceBook Login"
                                                        message:@"Your FaceBook account is not setup. Please go to your device settings and setup your FaceBook Account."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Facebook was not Setup
        [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
    }
    

    //Remove View
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.view.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.view.hidden = YES;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
}

- (IBAction)tweetPostButton_Clicked:(id)sender {
    
    [Flurry logEvent:@"Popup - Twitter Post"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Twitter Post" withLabels:nil];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result)
        {
            [tweetSheet dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    NSLog(@"Cancelled.....");
                    [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Twitter was Cancelled
                    [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
                    
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                    [sharedDefaults setBool:NO forKey:@"PostToSocialNetworks"];  //so won't ask to post again
                }
                    break;
            }};
        
        [tweetSheet setCompletionHandler:completionHandler];
        
        
        [tweetSheet setInitialText:@"You too can be more productive and enjoy more free time with the right tools. See how."];
        [tweetSheet addURL:[NSURL URLWithString:@"http://bit.ly/1fUMWWN"]];
        [tweetSheet addImage:[UIImage imageNamed:@"FaceBookDogAd2.jpg"]];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else //Twitter Account Not Setup
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Login"
                                                        message:@"Your Twitter account is not setup. Please go to your device settings and setup your Twitter Account."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Facebook was not Setup
        [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
    }
    
    
    //remove view
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.view.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.view.hidden = YES;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;


}





@end

//
//  Cal_Todo_OptionsVC.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 6/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Cal_Todo_OptionsVC.h"
#import "ToDoParentController.h"
#import "ToDoViewController.h"


@implementation Cal_Todo_OptionsVC
@synthesize btnShowAllTodo;


-(id)initWithContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal withParentVC:(id)parentVCLocal {
	if ((self = [super initWithNibName:@"Cal_Todo_OptionsVC" bundle:[NSBundle mainBundle]])){
		contentFrame = frame;	
		arrowDirection  = arrowDirectionLocal;
		arrowStartPoint = arrowStartPointLocal;
		parentVC = parentVCLocal;
	}
	return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self addAdditionalControlsInSearchBar];
}


#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	//	UITouch *touch = [touches anyObject];
	if (![touches containsObject:borderImageView] || ![touches containsObject:[borderImageView viewWithTag:121]] || ![touches containsObject:arrowImageView]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
		[self.view  setAlpha:0];
		[UIView commitAnimations];
		[self performSelector:@selector(removeView) withObject:nil afterDelay:0.3];
	}
}	

-(void)removeView {
	[self.view removeFromSuperview];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}


-(void)addAdditionalControlsInSearchBar {
	CGFloat frameX = contentFrame.origin.x;
	CGFloat frameY = contentFrame.origin.y; 
	//	NSInteger frameWidth = contentFrame.size.width;
	//	NSInteger frameHeight = contentFrame.size.height;
	CGFloat arrowPointX = arrowStartPoint.x;
	CGFloat arrowPointY = arrowStartPoint.y; 
	
	CGRect frame = CGRectZero;
	
	frame.origin.x = frameX;
	frame.origin.y = frameY;
	frame.size.width = borderImageView.image.size.width;
	frame.size.height = borderImageView.image.size.height; 
	[borderImageView setFrame:frame];

	frame.origin.x = arrowPointX;
	frame.origin.y = arrowPointY;
	frame.size.width = arrowImageView.image.size.width;
	frame.size.height = arrowImageView.image.size.height; 
	[arrowImageView setFrame:frame];

	frame.origin.x = borderImageView.frame.origin.x + 8;
	frame.origin.y = borderImageView.frame.origin.y + 26;
	frame.size.width = 49;
	frame.size.height = 49; 
	
	[btnShowAllTodo setFrame:frame];
	
	frame.origin.x = borderImageView.frame.origin.x + 8 + 49;
	frame.origin.y = borderImageView.frame.origin.y + 26;
	frame.size.width = 180;
	frame.size.height = 49; 
	
	[lblShowAllTodo setFrame:frame];
}

-(IBAction)showAllTodobtn_Clicked:(id) sender {
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	if ([(ToDoParentController*)parentVC showDoneTODOs]) {
		[sender setSelected:NO];
		[prefs setInteger:0 forKey:@"ShowDoneTodo"];
		[(ToDoParentController*)parentVC setShowDoneTODOs:NO];
	}else {
		[sender setSelected:YES];
		[prefs setInteger:1 forKey:@"ShowDoneTodo"];
		[(ToDoParentController*)parentVC setShowDoneTODOs:YES];
	}
	[(ToDoParentController*)parentVC filterButton_Clicked:(UIButton*)[[(ToDoParentController*)parentVC filterParentScrollView] viewWithTag:[(ToDoParentController*)parentVC  selectedButtonIndex]]];
   
    //[(ToDoViewController*)parentVC filterButton_Clicked:(UIButton*)[[(ToDoViewController*)parentVC filterParentScrollView] viewWithTag:[(ToDoViewController*)parentVC  selectedButtonIndex]]];
	[prefs synchronize];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

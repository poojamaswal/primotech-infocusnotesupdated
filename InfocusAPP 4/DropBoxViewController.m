//
//  DropBoxViewController.m
//  Organizer
//
//  Created by Primotech on 6/26/15.
//
//

#import "DropBoxViewController.h"
#import "DBXUtil.h"
#import "OrganizerAppDelegate.h"

#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

//#define DATABASE_FILE_NAME @"EmployeeDetails.sqlite" //@"OrganizeriOS_1.0.sqlite"

@interface DropBoxViewController (){
    
    IBOutlet UIActivityIndicatorView*_activity;
    IBOutlet UIProgressView* _progressView;
    IBOutlet UIButton* _btnUpload;
    IBOutlet UIButton* _btnDownload;
    
    UIBackgroundTaskIdentifier bgTaskForLoading;
}


@end

@implementation DropBoxViewController
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIDevice *dev = [UIDevice currentDevice];
    NSLog(@"%@", dev.name);
    
    _activity.hidden=TRUE;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navBarDropBox setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navBarDropBox.tintColor = [UIColor whiteColor];
    
    self.navBarDropBox.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
    self.navBarDropBox.translucent=YES;

     self.navBarDropBox.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBarHidden = YES;

    

    if(!dbu){
        dbu = [[DBXUtil alloc] init];
        dbu.delegate=self;
        [dbu linkDB];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
-(void)uploadCoreDataFile{
    

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *localPath=[documentsDirectory stringByAppendingPathComponent:@"/"DATABASE_FILE_NAME];
//    
//    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Notes" withExtension:@"mom"];
//    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Notes.sqlite"];
//   
//    NSLog(@"%@",localPath);
//    
//    [dbu uploadFileTo:@"/""Notes.sqlite" from:localPath];
//    
//    bgTaskForLoading = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//        [[UIApplication sharedApplication] endBackgroundTask:bgTaskForLoading];
//    }];
    
    
}

-(void)sendIndicatorStopAnimationNotification:(NSString*)strMsg{
    [_activity stopAnimating];
    _activity.hidden=TRUE;
    _progressView.hidden=YES;
    
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [[UIApplication sharedApplication] endBackgroundTask:bgTaskForLoading];
    
    if([strMsg isEqualToString:@"Uploaded File"]){
        
        //Coredata
        
       // [self uploadCoreDataFile];
        
       
        
        UIAlertView*alertUpload = [[UIAlertView alloc]initWithTitle:@"Uploaded" message:@"Backup has been successfully uploaded to your dropbox" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        alertUpload.tag = 101;
        [alertUpload show];
        _btnDownload.enabled=TRUE;
        
        
        
    }
    else if([strMsg isEqualToString:@"Downloaded File"]){
        UIAlertView * alertDownload = [[UIAlertView alloc]initWithTitle:@"Downloaded" message:@"Backup has been successfully Downloaded" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alertDownload.tag = 102;
        [alertDownload show];
        _btnUpload.enabled=TRUE;
    }
    else if([strMsg isEqualToString:@"Upload Failed"] || [strMsg isEqualToString:@"Loading Failed"]){
        UIAlertView * alertFailed = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alertFailed.tag = 103;
        [alertFailed show];
        _btnDownload.enabled=TRUE;
        _btnUpload.enabled=TRUE;
    }
    
    else if([strMsg isEqualToString:@"File has been deleted"]){
        UIAlertView * alertFailed = [[UIAlertView alloc]initWithTitle:@"Error" message:@"No file to download. Please make sure that file exists in your dropbox account." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alertFailed.tag = 103;
        [alertFailed show];
        _btnDownload.enabled=TRUE;
        _btnUpload.enabled=TRUE;
    }
    else{
        NSLog(@"Error: %@",strMsg);
        _btnDownload.enabled=TRUE;
        _btnUpload.enabled=TRUE;
    }
    
    
}

-(void)progressBarStatus:(CGFloat)progrssBarStatus{
    [_progressView setProgress:progrssBarStatus animated:YES];
}

-(IBAction)BackButtonClicked:(id)sender{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)uploadDatabase:(id)sender
{
    if([self hasConnectivity]){
        
        _progressView.hidden=FALSE;
        _activity.hidden=FALSE;
        [_activity startAnimating];
        [_progressView setProgress:0.0];
        _btnDownload.enabled=FALSE;
        _btnUpload.enabled=FALSE;
        
        
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        if(!dbu){
            dbu = [[DBXUtil alloc] init];
        }
        [dbu linkDB];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *localPath=[documentsDirectory stringByAppendingPathComponent:@"/"DATABASE_FILE_NAME];
        
        
        NSLog(@"%@",localPath);
        [dbu uploadFileTo:@"/"DATABASE_FILE_NAME from:localPath];
        bgTaskForLoading = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            [[UIApplication sharedApplication] endBackgroundTask:bgTaskForLoading];
        }];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Upload backup to dropbox requires an internet connection. Please connect to the internet to upload backup to your dropbox account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
    }
}


- (IBAction)DownloadDatabaseBackUp:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please upload your changes before downloading backup files or you will lose the changes you have made." delegate:self cancelButtonTitle:@"Upload changes" otherButtonTitles:@"Download", nil ];
    alert.tag=700;
    [alert show];
    
    
}

-(void)downloadBackUp{
    
    if([self hasConnectivity]){
        
        _progressView.hidden=FALSE;
        _activity.hidden=FALSE;
        [_activity startAnimating];
        [_progressView setProgress:0.0];
        _btnDownload.enabled=FALSE;
        _btnUpload.enabled=FALSE;
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        if(!dbu){
            dbu = [[DBXUtil alloc] init];
            
        }
        [dbu linkDB];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *localPath=[documentsDirectory stringByAppendingPathComponent:@"/"DATABASE_FILE_NAME];
        
        
        NSLog(@"%@",localPath);
        [dbu downloadFileFrom:@"/"DATABASE_FILE_NAME to:localPath];
        bgTaskForLoading = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            [[UIApplication sharedApplication] endBackgroundTask:bgTaskForLoading];
        }];
        
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Download backup to dropbox requires an internet connection. Please connect to the internet to download backup from your dropbox account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
    }
    
}



#pragma mark -
#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if(alertView.tag == 101 || alertView.tag == 103)
    {
        if([title isEqualToString:@"Ok"])
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
    else if(alertView.tag == 102)
    {
        if([title isEqualToString:@"Ok"])
        {
            OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate initializeDatabase];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
    
    else if (alertView.tag==700){
        if(buttonIndex==1){
            [self downloadBackUp];
        }
        else{
            [self uploadDatabase:nil];
        }
    }
}


- (IBAction)unlock:(id)sender
{
    [dbu listFiles:@"/"];
}

-(IBAction)signInWithOtherDropboxAccount:(id)sender{
    
    if([self hasConnectivity]){
        
        [dbu unlinkDB];
        
        
        
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Sign out from dropbox requires an internet connection. Please connect to the internet to sign in with another dropbox account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
    }
    
}

#pragma mark Check For Internet Connection

/*Connectivity testing code pulled from Apple's Reachability Example: http://developer.apple.com/library/ios/#samplecode/Reachability
 */
-(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
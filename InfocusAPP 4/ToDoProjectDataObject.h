//
//  ToDoProjectDataObject.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ToDoProjectDataObject : NSObject 
{
	
	NSInteger todoid;
	NSString  *todotitle;
	NSString *todotitletype;
	NSData *titleBinary;
	NSInteger belongtoProjectID;
}

@property(nonatomic,assign)NSInteger todoid;
@property(nonatomic,assign)NSInteger belongtoProjectID;
@property(nonatomic,strong)NSString *todotitletype;
@property(nonatomic,strong)NSString *todotitle;
@property(nonatomic,strong)NSData *titleBinary;

@end

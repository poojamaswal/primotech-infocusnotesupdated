
#import "ColorfulButton.h"

@implementation UIButton(ColorfulButton) 

-(void)setGradientWithHiColor:(UIColor *) colorHigh lowColor:(UIColor *) colorLow forHighlighted:(BOOL) isHighlighted {
    
	CAGradientLayer *gradientLayer = [[CAGradientLayer alloc] init];
	[gradientLayer setColors:[NSArray arrayWithObjects:(id)[colorHigh CGColor], (id)[colorLow CGColor], nil]];
    [gradientLayer setBounds:[self bounds]];
    [gradientLayer setPosition:CGPointMake([self  bounds].size.width/2, [self  bounds].size.height/2)];
	[[[[self layer] sublayers] objectAtIndex:0] removeFromSuperlayer];
	[[self layer] insertSublayer:gradientLayer atIndex:0];
    [[self layer] setCornerRadius:5.0f];
    [[self layer] setMasksToBounds:YES];
    [[self layer] setBorderWidth:0.7f];
	
//	if (isHighlighted) {
//		[[self layer] setMasksToBounds:YES];
//		self.layer.shadowColor = [[UIColor blackColor] CGColor];
//		self.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
//		self.layer.shadowOpacity = 0.5f;
//		self.layer.shadowRadius = 5.0f;
//	}else {
//		[[self layer] setMasksToBounds:YES];
//		self.layer.shadowColor = [[UIColor clearColor] CGColor];
//		self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//		self.layer.shadowOpacity = 1.0f;
//		self.layer.shadowRadius = 0.0f;
//	}
}	

-(void)setGradientWithHiColor:(UIColor *) colorHigh midColor:(UIColor *) colorMid lowColor:(UIColor *) colorLow forHighlighted:(BOOL) isHighlighted {
    
	CAGradientLayer *gradientLayer = [[CAGradientLayer alloc] init];
	[gradientLayer setColors:[NSArray arrayWithObjects:(id)[colorHigh CGColor],(id)[colorMid CGColor], (id)[colorLow CGColor], nil]];
    [gradientLayer setBounds:[self  bounds]];
    [gradientLayer setPosition:CGPointMake([self  bounds].size.width/2, [self  bounds].size.height/2)];
	[[[[self layer] sublayers] objectAtIndex:0] removeFromSuperlayer];
	[[self layer] insertSublayer:gradientLayer atIndex:0];
    [[self layer] setCornerRadius:5.0f];
    [[self layer] setMasksToBounds:YES];
    [[self layer] setBorderWidth:0.7f];
	
//	if (isHighlighted) {
//		[[self layer] setMasksToBounds:YES];
//		self.layer.shadowColor = [[UIColor blackColor] CGColor];
//		self.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
//		self.layer.shadowOpacity = 0.5f;
//		self.layer.shadowRadius = 5.0f;
//	}else {
//		[[self layer] setMasksToBounds:YES];
//		self.layer.shadowColor = [[UIColor clearColor] CGColor];
//		self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//		self.layer.shadowOpacity = 1.0f;
//		self.layer.shadowRadius = 0.0f;
//	}
	
}		


@end

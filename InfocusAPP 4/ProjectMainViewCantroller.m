//
//  ProjectMainViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProjectMainViewCantroller.h"
#import "AddProjectViewCantroller.h"
#import "RGBColorViewCantroller.h"
#import "OrganizerAppDelegate.h"
#import "ProjectReorderViewCantroller.h"
#import "ViewProjectTodoCantroller.h"
#import "ToDoParentController.h"
#import "CalendarParentController.h"
#import "CustomBadge.h"
#import "ProjectReorderViewCantroller.h"
#import "ViewProjectViewCantroller.h"
#import "ListToDoViewController.h"
#import "NotesViewCantroller.h"
#import "LIstToDoProjectViewController.h"
//#import "AdWhirlView.h"
#import "QuantcastMeasurement.h"
#import "Flurry.h" //Steve
#import "PasswordProtectAppViewController.h"//Steve



@interface ProjectMainViewCantroller()

@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;


@end


@implementation ProjectMainViewCantroller
@synthesize calendarBtnClicked;

//@synthesize adView;
//@synthesize pidArray;
@synthesize pnameArray;
@synthesize search_Bar;
@synthesize noteButtonClicked;
@synthesize priorityArray;
@synthesize projectdataobj;
@synthesize queryVal;
@synthesize Qval,conSize,pidArray,PimageArray,listofIMG;
@synthesize collect1,collect2,collect3;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidLoad 
{
    [super viewDidLoad];
   
    NSLog(@"ProjectMainViewController");
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Projects" withLabels:Nil];//Steve
    [Flurry logEvent:@"Projects"]; //Steve
    
    self.view.userInteractionEnabled = YES;
    isMenuClosed = YES; // if viewDidLoad method is called, then Menu is closed.  Used to set userEndabled to YES when Reveal button clicked (revealToggleHandle)
    
    self.navigationController.navigationBarHidden = YES;
    if(IS_IPAD)
        search_Bar.frame = CGRectMake(0, 936, 307, 44);
    else
        search_Bar.frame = CGRectMake(0, 500, 320, 44);
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Steve - loads password viewController
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        if ( ![sharedDefaults boolForKey:@"PasswordAccepted"] && [sharedDefaults boolForKey:@"NavigationNew"]){
            
            PasswordProtectAppViewController *passwordProtectAppViewController=
            [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController" bundle:[NSBundle mainBundle]];
            
            [self.navigationController pushViewController:passwordProtectAppViewController animated:NO];
        }
    }
    
    if(IS_LITE_VERSION)
    {
 
    // [super viewDidLoad]; //Steve - shows on top also.  Needed????
    }
    else if (IS_PROJECTS_VER){ //Steve added 12-24-12
        
        [homeBtn removeFromSuperview];
        [noteBtn removeFromSuperview];
        [listBtn removeFromSuperview];
        [todoBtn removeFromSuperview];
        //Calender icon tag set on XIB and tag = 1116
        [[self.view viewWithTag:1116] setFrame:CGRectMake(5,3,48,34)];
        
        NSMutableArray     *items = [bottomBar.items mutableCopy];
        //Steve Note: Removes the double space bar needed for Pro & Lite Version
        [items removeObjectAtIndex:2];
        bottomBar.items = items;
    }
    else{ //Steve - Pro & Lite version
        NSMutableArray     *items = [bottomBar.items mutableCopy];
        //Steve Note: Each item removed makes the item list smaller, which changes the item index order
        //therefore items are remove with highest number first
        [items removeObjectAtIndex:9];
        [items removeObjectAtIndex:8];
        [items removeObjectAtIndex:7];
        [items removeObjectAtIndex:6];
        bottomBar.items = items;
    }
    
	pidArray              = [[NSMutableArray alloc]init];
	pnameArray            = [[NSMutableArray alloc]init];
	priorityArray         = [[NSMutableArray alloc]init];
    PimageArray           = [[NSMutableArray alloc]init];
	arrayM_SearchContents = [[NSMutableArray alloc]init];
    arrayM_OriginalData   = [[NSMutableArray alloc]init];

	srchView.alpha =0;
	
}


-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear ProjectMainViewCantroller");
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    scroll.bounces = YES;
    
    
    UIBarButtonItem *addButton_iOS7;
    UIBarButtonItem *revealButtonItem;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        //NSLog(@"New Navigation");
        
        self.navigationController.navigationBarHidden = NO;
        navBar.hidden = YES;
        homeBtn.hidden = YES;
        noteBtn.hidden = YES;
        todoBtn.hidden = YES;
        listBtn.hidden = YES;
        calendarButton.hidden = YES;
        
    
        if (IS_IOS_7) {

            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            self.view.backgroundColor = [UIColor blackColor];
            scroll.backgroundColor = [UIColor whiteColor];
            
#pragma mark -- IPad Primotech
            
            if(IS_IPAD)
            {
                UIImage * imgAdd;
                UIButton * btnAdd;
                imgAdd = [UIImage imageNamed:@"add_icon.png"];
                btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnAdd setImage:imgAdd forState:UIControlStateNormal];
                btnAdd.showsTouchWhenHighlighted = YES;
                btnAdd.frame = CGRectMake(0.0, 0.0, imgAdd.size.width, imgAdd.size.height);
                
                [btnAdd addTarget:self action:@selector(addProject_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * add = [[UIBarButtonItem alloc]initWithCustomView:btnAdd];
                
                
                UIImage * imgSearch;
                UIButton * btnSearch;
                imgSearch = [UIImage imageNamed:@"search_icon.png"];
                btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnSearch setImage:imgSearch forState:UIControlStateNormal];
                btnSearch.showsTouchWhenHighlighted = YES;
                btnSearch.frame = CGRectMake(0.0, 0.0, imgSearch.size.width, imgSearch.size.height);
                
                [btnSearch addTarget:self action:@selector(search_Click:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * search = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
               
                UIImage * imgShare;
                UIButton * btnShare;
                imgShare = [UIImage imageNamed:@"share_icon.png"];
                btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnShare setImage:imgShare forState:UIControlStateNormal];
                btnShare.showsTouchWhenHighlighted = YES;
                btnShare.frame = CGRectMake(0.0, 0.0, imgShare.size.width, imgShare.size.height);
                
                [btnShare addTarget:self action:@selector(mailBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * share = [[UIBarButtonItem alloc]initWithCustomView:btnShare];
                
                
                self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:add,search,share,nil];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
            }
            else
            {
                bottomBar.hidden = NO;
                [self.view bringSubviewToFront:bottomBar];
                addButton.hidden = YES;
                
                //Add Button
                addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addProject_Clicked:)];
                self.navigationItem.rightBarButtonItem = addButton_iOS7;
                
                revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(revealToggleHandle)];
                
                self.navigationItem.leftBarButtonItem = revealButtonItem;
            }
    
             self.view.backgroundColor = [UIColor whiteColor];
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                bottomBar.translucent = YES;
                bottomBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                
                addButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                revealButtonItem.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                _searchButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                _shareButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                        [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
            }
            else{ //Dark Theme
                
                if(IS_IPAD)
                {
                    
                }
                else
                {
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    
                    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                    self.navigationController.navigationBar.translucent = YES;
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    
                    bottomBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                    bottomBar.tintColor = [UIColor whiteColor];
                    
                    addButton_iOS7.tintColor = [UIColor whiteColor];
                    revealButtonItem.tintColor = [UIColor whiteColor];
                    
                    _searchButton.tintColor = [UIColor whiteColor];
                    _shareButton.tintColor = [UIColor whiteColor];
                    
                    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                }
            }
            
        }
        else{ //iOS 6
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
          
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 568-20 - 44); //Steve - minus status(20) minus Nav (44)
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20); //Steve - Minus StatusBar (20)
                    bottomBar.frame = CGRectMake(0, 460, 320, 44);
                }
                else {
                    self.view.frame = CGRectMake(0, 0, 320, 480-20 - 44); //Steve - minus status(20) minus Nav (44)
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20); //Steve - Minus StatusBar (20)
                    bottomBar.frame = CGRectMake(0, 416-44, 320, 44);
                }
                self.navigationItem.rightBarButtonItem = nil;
                addButton.frame = CGRectMake(270, 44/2 - 29/2, 51, 29);
                [self.navigationController.navigationBar addSubview:addButton];
            
            //Plus Button
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
            
            UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon_OLD.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(revealToggleHandle)];
            
            self.navigationItem.leftBarButtonItem = revealButtonItem;
            revealButtonItem.tintColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            
        }
        
        if(IS_IPAD)
            self.title = @"";
        else
            self.title = @"Projects";
        
    }
    else{ //Old Nav
        // NSLog(@"Old Navigation");
        
        navBar.hidden = NO;
        homeBtn.hidden = NO;
        noteBtn.hidden = NO;
        todoBtn.hidden = NO;
        listBtn.hidden = NO;
        calendarButton.hidden = NO;
        self.navigationController.navigationBarHidden = YES;
        
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
        
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    }
    
    
    
    // ***************** Steve End ***************************
    
    
    
    //Steve changed so icons will appear white again
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
    
    
    
    
	for(int i=0;i<[pidArray count];i++)
	{
		UIButton *random = (UIButton *)[self.view viewWithTag:[[pidArray objectAtIndex:i]intValue]];
		[random removeFromSuperview];
		
	}
	
	[pidArray removeAllObjects];
	srchView.alpha=0;
    search_Bar.hidden = YES;
    searching = FALSE;
    [search_Bar resignFirstResponder];
    // [self.view endEditing:YES]; //Steve
    
	[super viewWillAppear:YES];
    
	[self getProject];
    
    
	conSize = 320;
	//NSLog(@"%d",conSize);
	arrayM_OriginalData = [pnameArray copy];
    cnt=[pnameArray count];
    
    
    
	[self reloadScrollView];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_Project"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];
        
        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = NO;
        }
        

        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_Project"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardNotification:)name:UIKeyboardWillShowNotification object:nil];//Steve
}


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}



//Steve added for Help views
-(void)viewDidAppear:(BOOL)animated{
    //Steve added
    //If first time running App, show some "Help" Views
    
    /*
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_Project"]) 
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];  
        helpView.hidden = NO; 
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_Project"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];     
    }
    */
}


-(void) revealToggleHandle{
    //NSLog(@"revealToggleHandle");
    
    if (isMenuClosed){
        self.view.userInteractionEnabled = NO;
        isMenuClosed = NO;
    }
    else{
        self.view.userInteractionEnabled = YES;
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
    [revealController revealToggle:self];
}



-(void)reloadScrollView{
   // NSLog(@"reloadScrollView");
    
    [scroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        if (IS_IOS_7) {
            
            if(IS_IPAD)
            {
                self.view.frame = CGRectMake(0, 0, 307, 1024);
                scroll.frame = CGRectMake(0, 0, 307, 1024);
            }
            else
            {
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 460 + 88);
                    bottomBar.frame = CGRectMake(0, 416 + 88, 320, 44);
                    scroll.frame = CGRectMake(0, 44, 320, 504);
                }
                else{
                    self.view.frame = CGRectMake(0, 0, 320, 460);
                    bottomBar.frame = CGRectMake(0, 416, 320, 44);
                    scroll.frame = CGRectMake(0, 44, 320, 416);
                }
            }

        }
        else{//iOS 6
            
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 460 + 88);
                bottomBar.frame = CGRectMake(0, 416 + 88, 320, 44);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 460);
                bottomBar.frame = CGRectMake(0, 416, 320, 44);
            }

        }
        
        
        yVal = 60; //60
        
        
        if(IS_LITE_VERSION)
            [scroll setFrame:CGRectMake(0, 44 - 44, 320, 325 + 44)];
        else
            if(IS_IPAD)
            {
                [scroll setFrame:CGRectMake(0, 0, 307, 1024)];
                [scroll setBounds:CGRectMake(0, 0, 307, 1024)];
            }
            else
            {
                if (IS_IPHONE_5){
                    [scroll setFrame:CGRectMake(0, 0, 320, 504 + 44)];
                    [scroll setBounds:CGRectMake(0, 0, 320, 504 + 44)];
                    UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, 44, 0);
                    [scroll setContentInset:inset];
                    
                }
                else{
                    [scroll setFrame:CGRectMake(0, 0, 320, 416 + 44)];
                    [scroll setBounds:CGRectMake(0, 0, 320, 416 + 44)];
                    UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, 44, 0);
                    [scroll setContentInset:inset];
                }
            }
        
         // NSLog(@"scroll.frame #1 = %@", NSStringFromCGRect(scroll.frame));
        
    }
    else{ //Old Navigation
        
         yVal=10; //Was 30
        
        if(IS_LITE_VERSION)
            [scroll setFrame:CGRectMake(0, 44, 320, 325)];
        else if(IS_IPAD)
        {
            self.view.frame = CGRectMake(0, 0, 307, 1024);
            scroll.frame = CGRectMake(0, 0, 307, 1024);
        }
        else{
            if(IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 548);
                scroll.frame = CGRectMake(0, 44, 320, 504);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 460);
                scroll.frame = CGRectMake(0, 44, 320, 416);
            }
        }
    }
    
// ******************************************************************
	
	for(int i=0;i<cnt;i++)
	{
		
		int t1=0;
		int t2=0;
		int t3=0;
		
		
		collect1=[self findBage:[[pidArray objectAtIndex:i]intValue]];
		collectTODO1=[self findTODOTotal:[[pidArray objectAtIndex:i]intValue]];
        
        int totalObjects;
        
        totalObjects = [self getBadgeCountsForNotes:[[pidArray objectAtIndex:i]intValue]];
        
        totalObjects = totalObjects + collectTODO1;
        
        collectTODO1 = totalObjects;
        
        
        // ******************** Gets Total To do & Note for Badge Count??? 
		for(int mm=0;mm<[collect1 count];mm++)
		{
			collectTODO2=[self findTODOTotal:[[collect1 objectAtIndex:mm]intValue]];
            
            
            int totalObjects;
            
            totalObjects = [self getBadgeCountsForNotes:[[collect1 objectAtIndex:mm]intValue]];
            
            totalObjects = totalObjects + collectTODO2;
            
            collectTODO2 = totalObjects;
            
            
            
            
            
			t1=t1+collectTODO2;
			collect2=[self findBage:[[collect1 objectAtIndex:mm]intValue]];
			
			for(int kk=0;kk<[collect2 count];kk++)
			{
				collectTODO3=[self findTODOTotal:[[collect2 objectAtIndex:0]intValue]];
                
                int totalObjects;
                
                totalObjects = [self getBadgeCountsForNotes:[[collect2 objectAtIndex:0]intValue]];
                
                totalObjects = totalObjects + collectTODO3;
                
                collectTODO3 = totalObjects;
                

                
                
				t2=t2+collectTODO3;
				collect3=[self findBage:[[collect2 objectAtIndex:kk]intValue]];
				for(int nn=0;nn<[collect3 count];nn++)
				{
					collectTODO4=[self findTODOTotal:[[collect3 objectAtIndex:nn]intValue]];
                    
                    int totalObjects;
                    
                    totalObjects = [self getBadgeCountsForNotes:[[collect3 objectAtIndex:nn]intValue]];
                    
                    totalObjects = totalObjects + collectTODO4;
                    
                    collectTODO4 = totalObjects;
                    

                    
                    
				    t3=t3+collectTODO4;
				}
			}
			
		}
        
        
        
        
        NSInteger totalB=t1+t2+t3+collectTODO1;
		UIImage *fbimg;
        
        // ************** Steve TEST ******************
		UIButton *btni =[[UIButton alloc] initWithFrame:CGRectMake(10, yVal, 300, 48)];
        [btni addTarget:self action:@selector(Pview_click:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(searching== YES)
        {
            fbimg =[UIImage imageNamed:[[arrayM_SearchContents objectAtIndex:i]valueForKey:@"image"]];
            [btni setBackgroundImage:fbimg forState:UIControlStateNormal];
            
            btni.tag=[[[arrayM_SearchContents objectAtIndex:i]valueForKey:@"number"]intValue];
        }
        else
        {
            fbimg=[UIImage imageNamed:[PimageArray objectAtIndex:i]];
            btni.tag=[[pidArray objectAtIndex:i]intValue];
            [btni setBackgroundImage:fbimg forState:UIControlStateNormal];
            
        }        
        
        
       
        
        
        if(searching)
        {
            
            if([[[arrayM_SearchContents objectAtIndex:i]valueForKey:@"priority"] isEqualToString:@"None"])
            {
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,14,250,30)]; //CGRectMake(10,10,250,30)
                if(searching == TRUE)
                    myLabel.text = [[arrayM_SearchContents objectAtIndex:i]valueForKey:@"Name"];
                else
                    myLabel.text = [pnameArray objectAtIndex:i];
                myLabel.backgroundColor = [UIColor clearColor];
                //[myLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
                myLabel.textColor = [UIColor whiteColor];
                [btni addSubview:myLabel];  
            }
            else {
                
                UILabel *priorityLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,14,200,30)]; //CGRectMake(10,10,200,30)
                priorityLabel.text = [[arrayM_SearchContents objectAtIndex:i]valueForKey:@"priority"];
                priorityLabel.backgroundColor = [UIColor clearColor];
                //[priorityLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [priorityLabel setFont:[UIFont fontWithName:@"Helvetica" size:18]];
                priorityLabel.textColor = [UIColor whiteColor];
                [btni addSubview:priorityLabel];
    
                
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,14,200,30)]; //CGRectMake(38,10,200,30)
                
                if(searching == TRUE)
                    myLabel.text = [[arrayM_SearchContents objectAtIndex:i]valueForKey:@"Name"];
                else
                    myLabel.text = [pnameArray objectAtIndex:i];
                myLabel.backgroundColor = [UIColor clearColor];
                //[myLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
                myLabel.textColor = [UIColor whiteColor];
                [btni addSubview:myLabel]; 
            }
            
        }
        else
        {
            
            //HelveticaNeue-Bold
        
            if([[priorityArray objectAtIndex:i] isEqualToString:@"None"])
            {                                                               //35,13
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,14,250,30)]; //CGRectMake(10,10,250,30)
                if(searching == TRUE)
                    myLabel.text = [[arrayM_SearchContents objectAtIndex:i]valueForKey:@"Name"];
                else
                    myLabel.text = [pnameArray objectAtIndex:i];
                myLabel.backgroundColor = [UIColor clearColor];
                //[myLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
                myLabel.textColor = [UIColor whiteColor];
                [btni addSubview:myLabel];
            }
            else {
                
                UILabel *priorityLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,14,200,30)]; //CGRectMake(10,10,200,30)
                priorityLabel.text = [priorityArray objectAtIndex:i];
                priorityLabel.backgroundColor = [UIColor clearColor];
                //[priorityLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [priorityLabel setFont:[UIFont fontWithName:@"Helvetica" size:18]];
                priorityLabel.textColor = [UIColor whiteColor];
                [btni addSubview:priorityLabel];
                
                
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,14,200,30)];  //CGRectMake(38,10,200,30)
                
                if(searching == TRUE)
                    myLabel.text = [[arrayM_SearchContents objectAtIndex:i]valueForKey:@"Name"];
                else
                    
                myLabel.text = [pnameArray objectAtIndex:i];
                myLabel.backgroundColor = [UIColor clearColor];
                //[myLabel setFont:[UIFont fontWithName:@"Verdana" size:18]];
                [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
                myLabel.textColor = [UIColor whiteColor];
                [btni addSubview:myLabel]; 
            }
        
        }
		CustomBadge *badgeViewAll1  = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d",totalB]];
        [badgeViewAll1 setFrame:CGRectMake(btni.frame.size.width -34,20, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
		[btni addSubview:badgeViewAll1];
		
		[scroll addSubview:btni];
		yVal=yVal+50;
		//NSLog(@"%d",yVal);
		if(yVal>320)
		{
            if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                conSize=yVal;
                scroll.contentSize =CGSizeMake(320,conSize);
            }
            else{
                conSize=yVal + 54;
                scroll.contentSize =CGSizeMake(320,conSize);
            }

            
            //NSLog(@" conSize = %d", conSize);
            //NSLog(@" scroll.frame  = %@", NSStringFromCGRect(scroll.frame));
            
			
		}
		
	}

    //[self animateTextFieldUp:search_Bar up:NO]; //Steve TEST *********************************
     //NSLog(@"search_Bar.frame reLoadScrollView = %@", NSStringFromCGRect(search_Bar.frame));

}


-(NSMutableArray*)findBage:(NSInteger)valueB 
{
    NSMutableArray *bage1=[[NSMutableArray alloc]init];
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
		if(selectBadgeStmt == nil && appDelegate.database)
		{
			
			Sql = [[NSString alloc] initWithFormat:@"SELECT projectID from ProjectMaster where belongstoID=%d ",valueB];
			//NSLog(@" query %@",Sql);
			
			
			if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, nil) != SQLITE_OK) 
			{
				NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
			}
			else
			{
				while(sqlite3_step(selectBadgeStmt) == SQLITE_ROW) 
				{
					NSInteger valG = sqlite3_column_int(selectBadgeStmt, 0);
					[bage1 addObject:[NSNumber numberWithInt:valG]];
				}							        
				sqlite3_finalize(selectBadgeStmt);		
				selectBadgeStmt = nil;        
			}
		}
		
	
	return bage1;
}


-(int)getBadgeCountsForNotes:(NSInteger)value {
	
    int notesBadgeCount;
    
    notesBadgeCount = 0;
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
		NSString  *SqlT = [[NSString alloc] initWithFormat:@"SELECT COUNT(NoteID)  from NotesMaster WHERE BelongstoProjectID=%d",value];
		
		//NSLog(@"%@",SqlT);
		
		if(sqlite3_prepare_v2(appDelegate.database, [SqlT UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
				notesBadgeCount = sqlite3_column_int(selectBadgeStmt, 0);
			}
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
	}
	selectBadgeStmt = nil;
	
	return notesBadgeCount;
}

- (IBAction)noteBtnClicked:(id)sender {
    NotesViewCantroller *notesViewCantroller =[[NotesViewCantroller alloc]initWithNibName:@"NotesViewCantroller" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:notesViewCantroller animated:YES];

}

- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}


-(NSInteger)findTODOTotal:(NSInteger)valueTODO
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
		  Sql = [[NSString alloc] initWithFormat:@"SELECT COUNT(TODOID)  from ToDoTable WHERE BelongstoProjectID=%d and progress != 100",valueTODO];
		
		//NSLog(@"%@",Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
				 totalTODO = sqlite3_column_int(selectBadgeStmt, 0);
				
							}
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
	}
	selectBadgeStmt = nil;
	
	return totalTODO;
	
}

//Steve added - finds keyboard size
- (void)keyBoardNotification:(NSNotification*)notification{
    // NSLog(@"keyBoardNotification notification");
    
    //**************************************************
    //******** Steve Test - Adjust Search bar to Keyboard
    //**************************************************
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    NSValue* keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
    
    float keyboardYPositionBegin = keyboardFrameBeginRect.origin.y;
    float keyboardYPositionEnd = keyboardFrameEndRect.origin.y;
    
    keyboardYPositionBeginVARIABLE = keyboardYPositionBegin;
    keyboardYPositionEndVARIABLE = keyboardYPositionEnd;
    
    
    //NSLog(@"********** keyboardFrameBeginRect = %@", NSStringFromCGRect(keyboardFrameBeginRect));
    //NSLog(@"********** keyboardYPositionBegin = %f", keyboardYPositionBegin);
    
    //NSLog(@"********** keyboardFrameEndRect = %@", NSStringFromCGRect(keyboardFrameEndRect));
    //NSLog(@"********** keyboardYPositionEnd = %f", keyboardYPositionEnd);
    
    [self animateTextFieldUp:search_Bar up:YES];
    
}


-(IBAction)search_Click:(id)sender;
{
    //NSLog(@"search_Click");
    
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    
    //[search_Bar setBarStyle:UIBarStyleDefault];
    [search_Bar setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

    
    //NSLog(@"search_Bar.tintColor =  %@", search_Bar.tintColor);
    
    search_Bar.hidden= NO;
    [search_Bar becomeFirstResponder];
    search_Bar.text = @"";
    searching = TRUE;
    
    if (IS_IOS_7) {
        
        //Steve - Adjust so everything aligns correctly
        if(IS_IPAD)
        {
            self.view.frame = CGRectMake(0, 0, 307, 1024);
            scroll.frame =    CGRectMake(0, 0, 307, 1024);
        }
        else
        {
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 480 + 88);
                scroll.frame =    CGRectMake(0, 20, 320, 416 + 88 - 44);
                bottomBar.frame = CGRectMake(0, 436 + 88, 320, 44);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 480);
                scroll.frame =    CGRectMake(0, 20, 320, 416 - 44);
                bottomBar.frame = CGRectMake(0, 436, 320, 44);
            }
        }
    }
    
    //NSLog(@"search_Bar.frame **search_Click ** = %@", NSStringFromCGRect(search_Bar.frame));
}



#pragma mark to scroll the searchBar up when keyboard Appears

- (void) animateTextFieldUp: (UISearchBar*) textField up: (BOOL) up
{
    //NSLog(@"animateTextFieldUp");
    //NSLog(@"keyboardYPositionEndVARIABLE #1=  %f", keyboardYPositionEndVARIABLE);
    
    
    //Steve - fixes search_Bar so animates properly
    if (keyboardYPositionEndVARIABLE == 0) {
        if(IS_IPAD)
        {
             keyboardYPositionEndVARIABLE = 1024;
        }
        else
        {
            if (IS_IPHONE_5)
                keyboardYPositionEndVARIABLE = 568;
            else
                keyboardYPositionEndVARIABLE = 480;
        }
    }
    
    
    //NSLog(@"search_Bar.frame **animateTextFieldUp 1 ** = %@", NSStringFromCGRect(search_Bar.frame));
    
    const float movementDuration = 0.3f; // tweak as needed
    CGRect frameSearchBar = textField.frame;
    //frameSearchBar.origin.y = 202.0f;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        if (IS_IOS_7) {
            
            frameSearchBar.origin.y = keyboardYPositionEndVARIABLE - search_Bar.frame.size.height;//Adjust for Nav bar
        }
        else{ //iOS 6
            
            frameSearchBar.origin.y = keyboardYPositionEndVARIABLE - search_Bar.frame.size.height -20 - 44;//Adjust for Nav bar
        }
    
    }
    else{
        //************ Adjust searchbar Height over keyboard ****************
        // Steve - to calculate Y of SearchBar so it sits on top of keyboard
        //    SearchBar Y Origin =  Keyboard Y Origin - SearchBarHeight (44) - 20 (Status Bar Height)
        frameSearchBar.origin.y = keyboardYPositionEndVARIABLE - search_Bar.frame.size.height -20;
    }
    
    
    
    if (IS_IOS_7) { //Steve
        
        //Steve - Adjust so everything aligns correctly
#pragma mark -- IPad Primotech
        
        if(IS_IPAD)
        {
            self.view.frame = CGRectMake(0, 0, 307, 1024);
            scroll.frame =    CGRectMake(0, 0, 307, 1024);
            search_Bar.frame =CGRectMake(0, 830, 307, 44);
        }
        else
        {
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 480 + 88);
                scroll.frame =    CGRectMake(0, 20, 320, 416 + 88 - 44);
                bottomBar.frame = CGRectMake(0, 436 + 88, 320, 44);
                search_Bar.frame =CGRectMake(0, 156 + 88, 320, 44);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 480);
                scroll.frame =    CGRectMake(0, 20, 320, 416 - 44);
                bottomBar.frame = CGRectMake(0, 436, 320, 44);
                search_Bar.frame =CGRectMake(0, 156, 320, 44);
                
                NSLog(@"search_Bar.frame = %@", NSStringFromCGRect(search_Bar.frame));
            }
        }
        
    }

        
    //NSLog(@"keyboardYPositionEndVARIABLE #2 =  %f", keyboardYPositionEndVARIABLE);
    //NSLog(@"frameSearchBar.origin.y =  %f", search_Bar.frame.size.height);
    //NSLog(@"frameSearchBar.origin.y =  %f", frameSearchBar.origin.y);
    
	if (up) {
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        
        textField.frame = frameSearchBar;
        
        [UIView commitAnimations];
    }
    else{
          textField.frame = frameSearchBar;
    }
    
    
    
    // NSLog(@"search_Bar.frame **animateTextFieldUp 2 ** = %@", NSStringFromCGRect(search_Bar.frame));
    // NSLog(@"textField.frame **animateTextFieldUp 2 ** = %@", NSStringFromCGRect(textField.frame));
	
}
#pragma mark to scroll the searchBar up when keyboard Disappears

- (void) animateTextFieldDown: (UISearchBar*) textField up: (BOOL) down
{
    //NSLog(@"animateTextFieldDown");
    
    //Steve changed so icons will appear white again
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];

    
    const float movementDuration = 0.3f; // tweak as needed
    CGRect frameSearchBar = textField.frame;
    //frameSearchBar.origin.y = 373.0f;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        frameSearchBar.origin.y = keyboardYPositionBeginVARIABLE - search_Bar.frame.size.height - 44 - 20 - 44;;//Adjust for Nav bar
        
        //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    }
    else{
        //************ Adjust searchbar Height above ToolBar ****************
        // Steve - to calculate Y of SearchBar so it sits on top of ToolBar
        // SearchBar Y Origin =  Keyboard Y Origin - SearchBarHeight (44) - ToolBar Height(44) - 20 (Status Bar Height)
        frameSearchBar.origin.y = keyboardYPositionBeginVARIABLE - search_Bar.frame.size.height - 44 - 20;
    }

	
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    
    textField.frame = frameSearchBar;
    [UIView commitAnimations];
    
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        
        if (IS_IOS_7) { //Steve
            
            //Steve - Adjust so everything aligns correctly
#pragma mark -- IPad Primotech //pooja-iPad
            if(IS_IPAD)
            {
                self.view.frame = CGRectMake(0, 0, 307, 1024);
                scroll.frame =    CGRectMake(0, 0, 307, 1024);
                search_Bar.frame =CGRectMake(0, 936, 307, 44);
            }
            else
            {
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 480 + 88);
                    scroll.frame =    CGRectMake(0, 20, 320, 416 + 88 - 44);
                    bottomBar.frame = CGRectMake(0, 436 + 88, 320, 44);
                    search_Bar.frame =CGRectMake(0, 392 + 88, 320, 44);
                }
                else{
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    scroll.frame =    CGRectMake(0, 20, 320, 416 - 44);
                    bottomBar.frame = CGRectMake(0, 436, 320, 44);
                    search_Bar.frame =CGRectMake(0, 392, 320, 44);
                    
                    //NSLog(@"search_Bar.frame = %@", NSStringFromCGRect(search_Bar.frame));
                }
            }

        }
        else{ //iOS 6
            
            //Steve - Adjust so everything aligns correctly
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 460 + 88);
                scroll.frame = CGRectMake(0, -44, 320, 416 + 88 - 44);
                bottomBar.frame = CGRectMake(0, 372 + 88, 320, 44);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 460);
                scroll.frame = CGRectMake(0, -44, 320, 416 - 44);
                bottomBar.frame = CGRectMake(0, 372, 320, 44);
            }

        }

    }
    
    /*
    NSLog(@"********* animateTextFieldDown ***********");
    NSLog(@"scroll.frame = %@", NSStringFromCGRect(scroll.frame));
    NSLog(@"scroll.bounds = %@", NSStringFromCGRect(scroll.bounds));
    NSLog(@"self.view.frame = %@", NSStringFromCGRect(self.view.frame));
    NSLog(@"self.navigationController.view.frame = %@", NSStringFromCGRect(self.navigationController.view.frame));
    NSLog(@"self.navigationController.view.bounds = %@", NSStringFromCGRect(self.navigationController.view.bounds));
    */
	
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //NSLog(@"searchBarTextDidBeginEditing");
    
    [self animateTextFieldUp:searchBar up:YES];
    searchBar.showsCancelButton = YES;
	searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    searching = TRUE;
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    //NSLog(@"searchBarTextDidEndEditing");
    
    [self animateTextFieldDown:searchBar up:NO];
    searchBar.showsCancelButton = NO;
    searching = FALSE;
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // NSLog(@"searchBar");

    [arrayM_SearchContents removeAllObjects];
    
	if([searchText isEqualToString:@""])
	{
        cnt = [pnameArray count];
        searching = FALSE;

	}
    else
    {
	NSInteger counter = 0; 
	
        //NSLog(@"\nOriginal Data->%@",arrayM_OriginalData);
        
	for(NSString *name in arrayM_OriginalData)
		
	{		
		NSRange r = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
		if(r.length > 0)
		{
            
			[arrayM_SearchContents addObject:[NSDictionary dictionaryWithObjectsAndKeys:[pnameArray objectAtIndex:[arrayM_OriginalData indexOfObject:name]],@"Name",[pidArray objectAtIndex:[arrayM_OriginalData indexOfObject:name]],@"number",
            [PimageArray objectAtIndex:[arrayM_OriginalData indexOfObject:name]],@"image",[priorityArray objectAtIndex:[arrayM_OriginalData indexOfObject:name]],@"priority",nil]];
		}
		
		counter++;
		
		
	}
        cnt = [arrayM_SearchContents count];

        searching = TRUE;

    }
    
    [self reloadScrollView];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        if (IS_IOS_7) {
            
            
            //Steve - Adjust so everything aligns correctly
#pragma mark -- IPad Primotech
            if(IS_IPAD)
            {
                self.view.frame = CGRectMake(0, 0, 307, 1024);
                scroll.frame =    CGRectMake(0, 0, 307, 1024);
        
            }
            else
            {
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 480 + 88);
                    scroll.frame =    CGRectMake(0, 20, 320, 416 + 88);
                    bottomBar.frame = CGRectMake(0, 436 + 88, 320, 44);
                }
                else{
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    scroll.frame =    CGRectMake(0, 20, 320, 416);
                    bottomBar.frame = CGRectMake(0, 436, 320, 44);
                    //searchBar.frame = CGRectMake(0, 264, 320, 44);
                }
            }
        }
        else{//iOS 6
            
            //Steve - Adjust so everything aligns correctly
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 460 + 88);
                scroll.frame = CGRectMake(0, -44, 320, 416 + 88);
                bottomBar.frame = CGRectMake(0, 372 + 88, 320, 44);
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 460);
                scroll.frame = CGRectMake(0, -44, 320, 416);
                bottomBar.frame = CGRectMake(0, 372, 320, 44);
            }
        }

    }

}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar // called when cancel button pressed
{
     
    [searchBar resignFirstResponder];
    // [self.view endEditing:YES]; //Steve
    searchBar.text = @"";
    search_Bar.hidden = YES;
    searching = FALSE;
    cnt = [pnameArray count];
    
    [self reloadScrollView];
    [self animateTextFieldDown:searchBar up:NO];
    
    
    if (IS_IOS_7) {
        
#pragma mark -- IPad Primotech
        if(IS_IPAD)
        {
            scroll.frame = CGRectMake(0, 0, 307, 1024);
            conSize=conSize + 54;
            scroll.contentSize =CGSizeMake(320,conSize);
        }
        else
        {
            //Steve - Adjust so everything aligns correctly
            if (IS_IPHONE_5) {
                scroll.frame = CGRectMake(0, 20, 320, 416 + 88 + 44);
                conSize=conSize + 54;
                scroll.contentSize =CGSizeMake(320,conSize);
            }
            else{
                scroll.frame = CGRectMake(0, 20, 320, 416 + 44);
                conSize=conSize + 54;
                scroll.contentSize =CGSizeMake(320,conSize);
            }
        }
    }
    else{ //iOS 6
        
        //Steve - Adjust so everything aligns correctly
        if (IS_IPHONE_5) {
            
            scroll.frame = CGRectMake(0, -44, 320, 416 + 88 + 44);//(0, 20, 320, 416 + 88 + 44)
            conSize=conSize; //+ 54
            scroll.contentSize =CGSizeMake(320,conSize);
        }
        else{
            scroll.frame = CGRectMake(0, -44, 320, 416 + 44);//(0, 20, 320, 416 + 44)
            conSize=conSize; //+ 54
            scroll.contentSize =CGSizeMake(320,conSize);
        }
    }
    
  
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    
    [searchBar resignFirstResponder];
    for(id subview in [searchBar subviews])
    {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:YES];
        }
    }

}


-(void)getBadgeCounts:(NSInteger)value {
	
	badgeCount[0] = 0;
  	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
    Sql = [[NSString alloc] initWithFormat:@"SELECT  COUNT(projectID)  FROM projectMaster left join TodoTable ON projectMaster.projectId=ToDoTable.BelongstoProjectID Where ToDoTable.BelongstoProjectID=%d",value];
		
		//NSLog(@"%@",Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
				badgeCount[0] = sqlite3_column_int(selectBadgeStmt, 0);
				
				//NSLog(@"starImage %d", badgeCount[0]);
			}
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
			}
	selectBadgeStmt = nil;
	
	return;
}




-(void)textFieldShouldReturn:(UITextField *)theTextField 
{
	[srchtxt resignFirstResponder];
	
}


-(IBAction)mailBarButton_Clicked:(id)sender
{
    /*
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    */
        
	if ([MFMailComposeViewController canSendMail]) {
        
        if (IS_IOS_7) {
            
            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
            if(IS_IPAD)
            {
                 self.view.frame = CGRectMake(0, 0, 307, 1024);
                
            }
            else
            {
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    //self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20);
                    bottomBar.frame = CGRectMake(0, 460 + 64, 320, 44);
                    
                }
                else {
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    //self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20);
                    bottomBar.frame = CGRectMake(0, 416-44 + 64, 320, 44);
                    
                }
            }
             NSLog(@" self.view.frame = %@", NSStringFromCGRect(self.view.frame) );
            
        }
        else{
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        

		
		Qval=0;
		srchView.alpha =0;
		MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
		[mailCntrlr setMailComposeDelegate:self];
        
        if (IS_PROJECTS_VER) {
            [mailCntrlr setSubject:@"InFocus Projects"];
        }
        else{
            [mailCntrlr setSubject:@"InFocus Pro - Project List"];
        }
		
		
		NSString *body=@"<table border=\"1\" style=\"background-color:white; width=\"100%\"><tr align=\"center\" ><th>Project Name</th><th>Priority</th><th>Count</th></tr>\n ";
		
		int count=1;
        int totalObjects;
        
        int todoCount;
		for (int row = 0; row < cnt; row++) {
            
			NSString *pname = [[NSString alloc]init];
			pname=[pnameArray objectAtIndex:row];
            
            //Steve added - % symbol causes an email bug in the title string, so it needs to be changed to %%
            NSString *projectNameString = pname;
            projectNameString = [projectNameString stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];

            
			NSString *priorityname =[[NSString alloc]init]; 
			priorityname=[priorityArray objectAtIndex:row];
            
            totalObjects = 0;
            todoCount = 0;
            
            todoCount=[self findTODOTotal:[[pidArray objectAtIndex:row]intValue]];
            totalObjects = [self getBadgeCountsForNotes:[[pidArray objectAtIndex:row]intValue]];
            totalObjects = totalObjects + todoCount;

			body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr align=\"center\"><td>%@</td><td>%@</td><td>%d</td></tr>",projectNameString,priorityname,totalObjects]];
			count++;
		}
		
		body = [body stringByAppendingString:@"</table>"];
        
        //Steve - Infocus Pro Link
        NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
        body = [body stringByAppendingString:bodyURL];
        
        
		[mailCntrlr setMessageBody:body isHTML:YES];
        [self presentViewController:mailCntrlr animated:YES completion:nil];
       
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
		//[alert release];
	}
	
    
	
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */



-(IBAction)edit_option:(id)sender
{
	[search_Bar resignFirstResponder]; //Steve
    
	ProjectReorderViewCantroller *editadd = [[ProjectReorderViewCantroller alloc] initWithNibName:@"ProjectReorderViewCantroller" bundle:[NSBundle mainBundle]];
	editadd.myOption =2;
	
	[self.navigationController pushViewController:editadd animated:YES];
}	


-(IBAction)add_todo:(id)sender
{
    
	[search_Bar resignFirstResponder]; //Steve
    
	ToDoParentController *todoadd = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	
	[self.navigationController pushViewController:todoadd animated:YES];
	
	
	
	
}

-(IBAction)add_calender:(id)sender
{
    [search_Bar resignFirstResponder];
	
	//OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	//	
	//	[appDelegate.navigationController popViewControllerAnimated:NO];
	NSString *selectStat=@"select count(projectID) from ProjectMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];   
    if(CountRecords >= MAX_LIMIT_PROJECT && IS_LITE_VERSION) //Steve added IS_LITE_VERSION
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Project Folder Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d project folders.",MAX_LIMIT_PROJECT] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
    }
    else{  
	CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:calendarParentController animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1)
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];
}


-(IBAction)Pview_click:(id)sender
{
    
    
    [search_Bar resignFirstResponder];
	if(IS_IPAD)
    {
        OrganizerAppDelegate * app_delegate  =(OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
        UISplitViewController * splitVC = [[UISplitViewController alloc]init];
        splitVC = app_delegate.splitVCProject;

        UINavigationController * nav=[[UINavigationController alloc]init];
        ViewProjectViewCantroller *viewprojectview = [[ViewProjectViewCantroller alloc] initWithNibName:@"ViewProjectViewCantrollerIPad" bundle:nil];
        viewprojectview.PIDval =[sender tag];
        nav.viewControllers=@[viewprojectview];
       // viewprojectview = [[splitVC viewControllers] objectAtIndex:1];
       // [splitVC viewControllers:[viewprojectview] ];
       
        [[splitVC.viewControllers objectAtIndex:0] removeFromParentViewController];
// NSLog(@"%@,%@",[splitVC.viewControllers objectAtIndex:0],[splitVC.viewControllers objectAtIndex:1]);
//        
       splitVC.viewControllers=@[[splitVC.viewControllers objectAtIndex:0] ,nav];
//        
        
    
    }
    else
    {
	ViewProjectViewCantroller *viewprojectview = [[ViewProjectViewCantroller alloc] initWithNibName:@"ViewProjectViewCantroller" bundle:[NSBundle mainBundle]];
		viewprojectview.PIDval =[sender tag];
		[self.navigationController pushViewController:viewprojectview animated:YES];
    }
	
	
	//
//	ViewProjectTodoCantroller *viewprojectview = [[ViewProjectTodoCantroller alloc] initWithNibName:@"ViewProjectTodoCantroller" bundle:[NSBundle mainBundle]];
//	viewprojectview.PIDval =[sender tag];
//	[self.navigationController pushViewController:viewprojectview animated:YES];
//	[viewprojectview release];
	
}

-(void)getProject;
{
	
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	[pnameArray removeAllObjects];
	[priorityArray removeAllObjects];
	[PimageArray removeAllObjects];
	
	
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		if(Qval)
		{
			Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,projectPriority,folderImage from ProjectMaster where level = %d AND projectName like '%%%@%%' ORDER BY projectPriority ASC ",0,queryVal];
			//NSLog(@" query %@",Sql);
		}
		else
		{
			
			Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,projectPriority,folderImage from ProjectMaster where level = %d ORDER BY projectPriority ASC",0];
			//NSLog(@" query %@",Sql);
			
			//SELECT * FROM table_name WHERE columnname LIKE value%
		}
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
						
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				projectdataobj = [[AddProjectDataObject alloc]init];
				
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				
				[pidArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
				
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pnameArray addObject:projectdataobj.projectname];
				
			
                [projectdataobj setProjectpriority: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                [priorityArray addObject:projectdataobj.projectpriority];
                
                [projectdataobj setProjectIMGname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
                [PimageArray addObject:projectdataobj.projectIMGname];
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
	
}

- (IBAction)calenderButtonClicked:(id)sender {
    
    CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:calendarParentController animated:YES];

}
- (IBAction)listBtnClicked:(id)sender {
    LIstToDoProjectViewController *ptr_ListToDoViewController = [[LIstToDoProjectViewController alloc]initWithNibName:@"LIstToDoProjectViewController" bundle:nil];
    [self.navigationController pushViewController:ptr_ListToDoViewController animated:YES];
}


-(IBAction)addProject_Clicked:(id)sender
{
    [search_Bar resignFirstResponder];//Steve
    
	NSString *selectStat=@"select count(projectID) from ProjectMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    
    if(CountRecords >= MAX_LIMIT_PROJECT && IS_LITE_VERSION) //Steve added IS_LITE_VERSION
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Project Folder Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d project folders.",MAX_LIMIT_PROJECT] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
    }
    else{
        
        if (IS_IOS_7) {
            if(IS_IPAD)
            {
                AddProjectViewCantroller *addprojectview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantrollerIPad" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:addprojectview animated:YES];

            }
            else
            {
                AddProjectViewCantroller *addprojectview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantroller_iOS7" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:addprojectview animated:YES];
            }
        }
        else{
            
            AddProjectViewCantroller *addprojectview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantrollerIPad" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:addprojectview animated:YES];
        }
        
    }
}


-(IBAction)home_click:(id)sender
{
    
    id viewCont =  [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
    
    
    if([viewCont isKindOfClass:[ToDoParentController class]]||[viewCont isKindOfClass:[CalendarParentController class]]||[viewCont isKindOfClass:[ListToDoViewController class]]||[viewCont isKindOfClass:[NotesViewCantroller class]])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];

    }
    else
    {
    	[self.navigationController popViewControllerAnimated:YES];
    }
}


-(IBAction)option_click:(id)sender
{
    [search_Bar resignFirstResponder];//Steve
    
	ProjectReorderViewCantroller *projectReorder = [[ProjectReorderViewCantroller alloc] initWithNibName:@"ProjectReorderViewCantroller" bundle:[NSBundle mainBundle]];
	projectReorder.myOption = 1;
	[self.navigationController pushViewController:projectReorder animated:YES];
	
	
}

-(IBAction)settingBarButton_Clicked:(id)sender{
   // SettingsViewController *settingsViewController = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    //[self.navigationController pushViewController:settingsViewController animated:YES];
    
    [search_Bar resignFirstResponder];//Steve
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SettingsTableViewController" bundle:nil];
    UIViewController *settingsTableViewController = [sb instantiateViewControllerWithIdentifier:@"SettingsTableViewController"];
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    [self presentViewController:settingsTableViewController animated:YES completion:nil];
}


- (IBAction)infoBarButton_Clicked:(id)sender{
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"FREE Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"FREE Button"]; //Steve
    
    
    //Steve - goes right to App store instead of using sales page below
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];
    
    //**********************************************************************
    //**************** Steve - Removed Sales Page ***************************
    //**********************************************************************
    
    /*
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [navBarUpgradeView setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    if (IS_IPHONE_5) {
        upgradeView.frame = CGRectMake(0, 0, 320, 568);
        upgradeScrollView.frame = CGRectMake(0, 44, 320, 568);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1088)];
    }
    else{
        upgradeView.frame = CGRectMake(0, 0, 320, 480);
        upgradeScrollView.frame = CGRectMake(0, 44, 320, 480);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1010)];
    }
    
    upgradeScrollView.userInteractionEnabled = YES;
    upgradeScrollView.bounces = NO;
    
    upgradeView.hidden = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromBottom];
    [self.view addSubview:upgradeView];
     */
    
}

- (IBAction)cancelUpgradeButton_Clicked:(id)sender {
    
    [search_Bar resignFirstResponder];//Steve
    
    upgradeView.hidden = YES;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
}

- (IBAction)upgradeButton_Clicked:(id)sender {
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Upgrade Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"Upgrade Button"]; //Steve
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];
    
}


- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewWillDisappear:(BOOL)animated {
    //[self.view endEditing:YES];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];//Steve
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
    {
        
        [addButton removeFromSuperview];
    }
    
}

    

- (void)viewDidUnload {
    [self setSearch_Bar:nil];
    navBar = nil;
    bottomBar = nil;
    [self setNoteButtonClicked:nil];
    [self setCalendarBtnClicked:nil];
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    homeBtn = nil;
    noteBtn = nil;
    todoBtn = nil;
    listBtn = nil;
    upgradeView = nil;
    upgradeScrollView = nil;
    navBarUpgradeView = nil;

    addButton = nil;
    calendarButton = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end








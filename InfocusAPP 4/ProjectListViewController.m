//
//  ProjectListViewController.m
//  Organizer
//
//  Created by Tarun on 6/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProjectListViewController.h"
#import "OrganizerAppDelegate.h"
#import "MyTreeViewCell.h"
#import "AddProjectDataObject.h"
#import "AddProjectViewCantroller.h"

@interface ProjectListViewController()


-(void)getAllProjectData;

@end


@implementation ProjectListViewController
@synthesize projectArray;
@synthesize delegate;
@synthesize getLevelRootArray;
@synthesize getLevelRootNameArray;
@synthesize getLevelRootLevelArray;
@synthesize getLevelRootBelongsToArray;
@synthesize pidleveloneArray;
@synthesize pidleveloneNameArray;
@synthesize pidleveloneLevelArray;
@synthesize pidleveloneBelongsToArray;
@synthesize pidleveltwoArray;
@synthesize pidleveltwoNameArray;
@synthesize pidleveltwoLevelArray;
@synthesize pidleveltwoBelongsTolArray;
@synthesize pidlevelthreeArray;
@synthesize pidlevelthreeLevelArray;
@synthesize pidlevelthreeBelongsToArray;
@synthesize pidlevelthreeNameArray;
@synthesize getLevelRootimageArray; 
@synthesize getLevelOneimageArray; 
@synthesize getLevelTwoimageArray; 
@synthesize getLevelThreetimageArray; 
@synthesize getTodoID,setVal;
@synthesize pidRootInclusiveArray; //Steve
@synthesize pidleveloneInclusiveArray, pidleveltwoInclusiveArray, pidlevelthreeInclusiveArray; //STeve




#pragma mark -
#pragma mark ViewController LifeCycle
#pragma mark -

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withProjectID:(NSInteger) projectId{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		lastProjectID = projectId;
        //NSLog(@"lastProjectID = %d",lastProjectID);
    }
    return self;
}

- (void)viewDidLoad {
	
    pidRootInclusiveArray = [[NSMutableArray alloc]init]; //Steve
    pidleveloneInclusiveArray = [[NSMutableArray alloc]init]; //Steve
    pidleveltwoInclusiveArray = [[NSMutableArray alloc]init]; //Steve
    pidlevelthreeInclusiveArray = [[NSMutableArray alloc]init]; //Steve
    
	getLevelRootLevelArray=[[NSMutableArray alloc]init];
	pidleveloneLevelArray=[[NSMutableArray alloc]init];
	pidleveltwoLevelArray=[[NSMutableArray alloc]init];
	
	getLevelRootArray=[[NSMutableArray alloc]init];
	getLevelRootNameArray=[[NSMutableArray alloc]init];
	
	pidleveloneArray = [[NSMutableArray alloc]init];
	pidleveloneNameArray=[[NSMutableArray alloc]init];
	
	pidleveltwoArray=[[NSMutableArray alloc]init];
	pidleveltwoNameArray=[[NSMutableArray alloc]init];
	
    pidlevelthreeNameArray=[[NSMutableArray alloc]init];
	pidlevelthreeArray=[[NSMutableArray alloc]init];
	
	pidlevelthreeArray=[[NSMutableArray alloc]init];
	pidlevelthreeNameArray=[[NSMutableArray alloc]init];
	pidlevelthreeLevelArray=[[NSMutableArray alloc]init];
	
	getLevelRootBelongsToArray=[[NSMutableArray alloc]init];
	pidleveloneBelongsToArray=[[NSMutableArray alloc]init];
	pidleveltwoBelongsTolArray=[[NSMutableArray alloc]init];
	pidlevelthreeBelongsToArray=[[NSMutableArray alloc]init];
	
	getLevelRootimageArray=[[NSMutableArray alloc]init];
	getLevelOneimageArray=[[NSMutableArray alloc]init];
	
	getLevelTwoimageArray=[[NSMutableArray alloc]init];
	
	getLevelThreetimageArray=[[NSMutableArray alloc]init];
	
	
	[projectTblVw.layer setCornerRadius:5.0];
	projectTblVw.layer.masksToBounds = YES;
	projectTblVw.layer.borderColor = [UIColor lightGrayColor].CGColor; //Steve 
	projectTblVw.layer.borderWidth = 0.5;
    projectArray = [[NSMutableArray alloc] init];
	[super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    
	[super viewWillAppear:YES];
    
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        self.view.autoresizingMask = NO;
        self.view.autoresizesSubviews = NO;
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            projectTblVw.backgroundColor = [UIColor whiteColor];
            UIBarButtonItem *addButton_iOS7;
            
            //pooja-iPad
            if(IS_IPAD)
            {
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
                
                [navBar removeFromSuperview];
                addProjectButton.hidden = YES;
               addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(ProjectButton_click:)];
                self.navigationItem.rightBarButtonItem = addButton_iOS7;
            }
            else
            {
                //Add Button
                addProjectButton.hidden = YES;
                addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(ProjectButton_click:)];
                self.navigationItem.rightBarButtonItem = addButton_iOS7;
            }
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
                addButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                //pooja-iPad
                if(IS_IPAD)
                {
                    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                    self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
                    self.navigationController.navigationBar.translucent = NO;
                }
                else
                {
                self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
                addButton_iOS7.tintColor = [UIColor whiteColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                }
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
               
            }
            
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
            
            
            //pooja-iPad
            if(IS_IPAD)
            {
                checkMe.frame = CGRectMake(300, 25, 50, 34);
                noneButton.frame = CGRectMake(25, 19, 235, 40);
                projectLabelNone.frame = CGRectMake(61, 29, 97, 21);

                
                //self.view.frame = CGRectMake(0, 0, 459, 1024);
                //projectTblVw.frame = CGRectMake(0, 67, 459, 957);

            }
            else
            {
                
                checkMe.frame = CGRectMake(258, 61 - 44, 50, 34);
                noneButton.frame = CGRectMake(24, 53 - 44, 235, 40);
                projectLabelNone.frame = CGRectMake(41, 67 - 44, 97, 21);

                if (IS_IPHONE_5){
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    projectTblVw.frame = CGRectMake(6, 101 - 44, 307, 356 + 88);
                }
                
                else{
                    
                    projectTblVw.frame = CGRectMake(6, 101 - 44, 307, 356);
                }
            }
            
            self.title =@"Projects";
            
            [self.navigationController.navigationBar addSubview:addProjectButton];

        }

        else{ //iOS 6
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            projectTblVw.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            
            checkMe.frame = CGRectMake(258, 61 - 44, 50, 34);
            noneButton.frame = CGRectMake(24, 53 - 44, 235, 40);
            projectLabelNone.frame = CGRectMake(41, 67 - 44, 97, 21);
            
            if (IS_IPHONE_5)
                projectTblVw.frame = CGRectMake(6, 101 - 44, 307, 356 + 88);
            else
                projectTblVw.frame = CGRectMake(6, 101 - 44, 307, 356);
            
            self.title =@"Projects";
            
            [self.navigationController.navigationBar addSubview:addProjectButton];
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

        }
        

        
    }
    else{ // Old Nav
        
        
        self.navigationController.navigationBarHidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        projectTblVw.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        
        //backButton = [[UIButton alloc]init];
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************


    
	
	if(lastProjectID == 0)
	{
        
        //Steve
        if (IS_IPAD) { //iPhone
    
            [checkMe setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            
            //Adjust Checkmark for iPad
            UIImage *checkImage = [UIImage imageNamed:@"TickArrow.png"];
            UIImageView *checkImageView = [[UIImageView alloc]initWithImage:checkImage];
            checkImageView.frame = CGRectMake(self.view.frame.size.width - checkMe.frame.origin.x - (self.view.frame.size.width - projectTblVw.frame.size.width) - 15, checkMe.frame.size.height/2 - checkImage.size.height/2, checkImage.size.width, checkImage.size.height);
            [checkMe addSubview:checkImageView];
            
        }
        else{ //iPhone
            
            [checkMe setImage:[UIImage imageNamed:@"TickArrow.png"] forState:UIControlStateNormal];
        }
		
	}
	else 
	{
		[checkMe setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
	}
    
	
	
	[self FindRecored];
    
	
	treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
	for(int i=0;i<[getLevelRootArray count];i++)
	{
		//NSLog(@"%d",[[getLevelRootBelongsToArray objectAtIndex:i]intValue]);
		node1 = [[MyTreeNode alloc] initWithValue:[getLevelRootNameArray objectAtIndex:i]];
		node1.ID=[[getLevelRootArray objectAtIndex:i]intValue];
		node1.belongToID=[[getLevelRootBelongsToArray objectAtIndex:i]intValue];
		node1.LevelVal=[[getLevelRootLevelArray objectAtIndex:i]intValue];
		node1.prntID=[[getLevelRootArray objectAtIndex:i]intValue];
		node1.prjctID=[[getLevelRootArray objectAtIndex:i]intValue];
		node1.Fimg=[getLevelRootimageArray objectAtIndex:i];
        //node1.inclusive = [[pidRootInclusiveArray objectAtIndex:i] intValue]; //Steve Commented - can be used to show current state (Collapse/Expanded) 
        
		[treeNode addChild:node1];
		
		//NSLog(@"%@",getLevelRootArray);	
		[self getLevelOne:[[getLevelRootArray objectAtIndex:i]intValue]];
        
        //////////////// Steve - Changes all Node1 to "+" (Collapsed). ////////////////////////////////
        //This saves from showing all subfolders, which can be a problem if there are many, it will be hard to find the correct folder
        
        if (node1.inclusive == YES) {
            
            //Steve - removes "+" sign if no folders underneath it.  Otherwise to-do's & notes will cause it to show "+"
            if (pidleveloneArray.count == 0) 
                node1.inclusive = YES;
            else
                node1.inclusive = NO; //Converts it to "+" (Collapsed) even if the saved value is not
        }
		
        
		for(int j=0;j<[pidleveloneArray count];j++)
		{
			node2 = [[MyTreeNode alloc] initWithValue:[pidleveloneNameArray objectAtIndex:j]];	
			node2.ID = [[pidleveloneArray objectAtIndex:j]intValue];
			node2.LevelVal=[[pidleveloneLevelArray objectAtIndex:j]intValue];
			node2.belongToID=[[pidleveloneBelongsToArray objectAtIndex:j]intValue];
			node2.prntID=[[getLevelRootArray objectAtIndex:i]intValue];
			node2.prjctID=[[pidleveloneArray objectAtIndex:j]intValue];
			node2.Fimg=[getLevelOneimageArray objectAtIndex:j];
            node2.inclusive = [[pidleveloneInclusiveArray objectAtIndex:j] intValue]; //Steve Shows current State
            
			[ node1 addChild:node2];
            
			[self getLevelTwo: [[pidleveloneArray objectAtIndex:j]intValue]];
            
            //Steve - removes "+" sign if no folders underneath it.  Otherwise to-do's & notes will cause it to show "+"
            if (pidleveltwoArray.count == 0) {
                node2.inclusive = YES;
            }
            
            
			{
				for(int k=0;k<[pidleveltwoArray count];k++)
				{
					
					node3 = [[MyTreeNode alloc] initWithValue:[pidleveltwoNameArray objectAtIndex:k]];	
					node3.ID=[[pidleveltwoArray objectAtIndex:k]intValue];
					node3.LevelVal=[[pidleveltwoLevelArray objectAtIndex:k]intValue];
					node3.belongToID=[[pidleveltwoBelongsTolArray objectAtIndex:k]intValue];
					node3.prntID=[[getLevelRootArray objectAtIndex:i]intValue];
					node3.prjctID=[[pidleveltwoArray objectAtIndex:k]intValue];
					node3.Fimg=[getLevelTwoimageArray objectAtIndex:k];
                    node3.inclusive = [[pidleveltwoInclusiveArray objectAtIndex:k] intValue]; //Steve Shows current State
					
					[node2 addChild:node3];
                    
					[self getLevelThree:[[pidleveltwoArray objectAtIndex:k]intValue]];
                    
                    //Steve - removes "+" sign if no folders underneath it.  Otherwise to-do's & notes will cause it to show "+"
                    if (pidlevelthreeArray.count == 0) {
                        node3.inclusive = 1;
                    }
                    
					for(int l=0;l<[pidlevelthreeArray count];l++)
					{
						node4=[[MyTreeNode alloc] initWithValue:[pidlevelthreeNameArray objectAtIndex:l]];
						node4.ID=[[pidlevelthreeArray objectAtIndex:l]intValue];
						node4.LevelVal=[[pidlevelthreeLevelArray objectAtIndex:l]intValue];
						node4.belongToID=[[pidlevelthreeBelongsToArray objectAtIndex:l]intValue];
						node4.prntID=[[getLevelRootArray objectAtIndex:i]intValue];
						node4.prjctID=[[pidlevelthreeArray objectAtIndex:l]intValue];
						node4.Fimg=[getLevelThreetimageArray objectAtIndex:l];
                        
                        //Steve Commented because level 4 never has subfolders since its the last level allowed
                        //node4.inclusive = [[pidlevelthreeInclusiveArray objectAtIndex:l] intValue]; 
						
						[node3 addChild:node4];
					}
					
				}
			}
          
		}
		
	}
	
	
    
	UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[editBtn setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
	[editBtn setFrame:CGRectMake(220, 10, 45, 19)];
	[editBtn addTarget:self action:@selector(editCLicked) forControlEvents:UIControlEventTouchUpInside];
    //	[self.navigationController.navigationBar addSubview:editBtn];
    
	[projectTblVw reloadData];
    
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_TodoAddProject"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];

        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = YES;
        }
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoAddProject"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
        
    }
    
    
    //Steve
    if (IS_IPAD) {
        
        //Clear border so I can move tableview over to the rihgt and left sides
        projectTblVw.layer.borderColor = [[UIColor clearColor] CGColor];
        
        
        //Steve - adds line underneath "None" folder for iPad
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = [UIColor colorWithRed:200.0/255.0f green:200.0/255.0f blue:200.0/255.0f alpha:1.0f];
        lineView.frame = CGRectMake(projectTblVw.frame.origin.x + 12, checkMe.frame.size.height + checkMe.frame.origin.y + 10, projectTblVw.frame.size.width - 12, 1);
        [self.view addSubview:lineView];
        
        
    }
    
    
    //NSLog(@"addProjectButton.tintColor =  %@", addProjectButton.tintColor);
    /*
    NSLog(@" self.view.frame = %@", NSStringFromCGRect(self.view.frame));
    NSLog(@" projectTblVw.frame = %@", NSStringFromCGRect(projectTblVw.frame));
    NSLog(@" projectTblVw.bounds = %@", NSStringFromCGRect(projectTblVw.bounds));
     */
}





-(void)viewDidAppear:(BOOL)animated{

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark DataFetch Method
-(void)FindRecored
{
	
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		
		//Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where level =%d  ORDER BY projectPriority,projectName ASC",0];
        Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage, Inclusive from ProjectMaster where level =%d  ORDER BY projectPriority,projectName ASC",0];  //Steve added Inclusive
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
			file://localhost/Users/naresh.chauhan/Documents/Organizer/AddNewSubfolderViewCantroller.m	{
            [getLevelRootArray removeAllObjects];
            [getLevelRootNameArray removeAllObjects];
            [getLevelRootimageArray removeAllObjects];
            [pidRootInclusiveArray removeAllObjects]; //STeve TEST
        
		while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW)
		{
			AddProjectDataObject  *projectdataobj = [[AddProjectDataObject alloc]init];
			[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
			[getLevelRootArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
			[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
			[getLevelRootNameArray addObject:projectdataobj.projectname];
			[projectdataobj setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
			[getLevelRootLevelArray addObject:[NSNumber numberWithInt:projectdataobj.level]];
			
			[getLevelRootLevelArray addObject:[NSNumber numberWithInt:projectdataobj.level]];
			
			[projectdataobj setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
			[getLevelRootBelongsToArray addObject:[NSNumber numberWithInt:projectdataobj.belongstoid]];
			[projectdataobj setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
			[getLevelRootimageArray addObject:projectdataobj.projectIMGname];
            
            [pidRootInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]]; //Steve TEST
		}							        
		sqlite3_finalize(selectNotes_Stmt);		
		selectNotes_Stmt = nil;        
	}
	
}
//}

-(void)getLevelOne:(NSInteger)lONE
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		//Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lONE,1];
        Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lONE,1]; //Steve added: Inclusive
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[pidleveloneArray  removeAllObjects];
			[pidleveloneNameArray  removeAllObjects];
			[pidleveloneBelongsToArray removeAllObjects]; 
			[getLevelOneimageArray removeAllObjects];
            [pidleveloneInclusiveArray removeAllObjects]; //STeve TEST
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjone = [[AddProjectDataObject alloc]init];
				[projectdataobjone setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidleveloneArray addObject:[NSNumber numberWithInt:projectdataobjone.projectid]];
				
				[projectdataobjone setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidleveloneNameArray addObject:projectdataobjone.projectname];
				
				[projectdataobjone setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
				[pidleveloneLevelArray addObject:[NSNumber numberWithInt:projectdataobjone.level]];
				[projectdataobjone setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[pidleveloneBelongsToArray addObject:[NSNumber numberWithInt:projectdataobjone.belongstoid]];
				
				[projectdataobjone setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[getLevelOneimageArray addObject:projectdataobjone.projectIMGname];
				
                [pidleveloneInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]]; //Steve TEST
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
}

-(void)getLevelTwo:(NSInteger)lTWO
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		//Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lTWO,2];
        Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lTWO,2]; //Steve added: Inclusive
        
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[pidleveltwoArray  removeAllObjects];
			[pidleveltwoNameArray removeAllObjects];
			[getLevelTwoimageArray removeAllObjects];
            [pidleveltwoInclusiveArray removeAllObjects]; //STeve TEST
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjtwo = [[AddProjectDataObject alloc]init];
				[projectdataobjtwo setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidleveltwoArray addObject:[NSNumber numberWithInt:projectdataobjtwo.projectid]];
				
				[projectdataobjtwo setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidleveltwoNameArray addObject:projectdataobjtwo.projectname];
				
				
				[projectdataobjtwo setLevel:sqlite3_column_int(selectNotes_Stmt,2)];
				[pidleveltwoLevelArray addObject:[NSNumber numberWithInt:projectdataobjtwo.level]];
				[projectdataobjtwo setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[pidleveltwoBelongsTolArray addObject:[NSNumber numberWithInt:projectdataobjtwo.belongstoid]];
				
				[projectdataobjtwo setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[getLevelTwoimageArray addObject:projectdataobjtwo.projectIMGname];
                
                [pidleveltwoInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]]; //Steve TEST
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
}

-(void)getLevelThree:(NSInteger)lThree
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		//Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lThree,3];
        Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lThree,3]; //Steve added: Inclusive
        
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
			
		{
			[pidlevelthreeArray  removeAllObjects];
			[pidlevelthreeNameArray removeAllObjects];
			[getLevelThreetimageArray removeAllObjects];
            [pidlevelthreeInclusiveArray removeAllObjects]; //STeve TEST
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjthree = [[AddProjectDataObject alloc]init];
				[projectdataobjthree setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidlevelthreeArray addObject:[NSNumber numberWithInt:projectdataobjthree.projectid]];
				
				[projectdataobjthree setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidlevelthreeNameArray addObject:projectdataobjthree.projectname];
				
				
				[projectdataobjthree setLevel:sqlite3_column_int(selectNotes_Stmt,2)];
				[pidlevelthreeLevelArray addObject:[NSNumber numberWithInt:projectdataobjthree.level]];
				
				[projectdataobjthree setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[pidlevelthreeBelongsToArray addObject:[NSNumber numberWithInt:projectdataobjthree.belongstoid]];
				
				[projectdataobjthree setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[getLevelThreetimageArray addObject:projectdataobjthree.projectIMGname];
                
                [pidlevelthreeInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]]; //Steve TEST
			}
            
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		
	}	
}



#pragma mark -
#pragma mark DataFetch Method

-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -


// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}



// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [treeNode descendantCount];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	float height;
	//MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	
	height=60.40;
	return height;
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//projectDataObject = [projectArray objectAtIndex:indexPath.row];
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive]; 
    
	[[cell arrowImage]  addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
	[[cell arrowImage] setTag:indexPath.row + 1];
	
    
    //////////// Steve Notes //////////////////////////////////
    // node.inclusive == 0 --> Collapsed (Doesn't show)
    // node.inclusive == 1 --> Expanded (Shows)
    // node.ID --> Project Node ID, if Node.ID == 0 --> Then To Do or Note

    
    
	if(node.LevelVal==0)
	{
		UIImage *imgl1=[UIImage imageNamed:node.Fimg];
		buttonLevelone = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelone setBackgroundImage:imgl1 forState:UIControlStateNormal];
		buttonLevelone.frame = CGRectMake(25,7.0,235.0, 40.0);
		buttonLevelone.tag=indexPath.row;
		[buttonLevelone addTarget:self action:@selector(ShowNext:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:buttonLevelone];
		
        UILabel *myLabel = [[UILabel alloc] init];
        
        //pooja-iPad
        if(IS_IPAD)
            myLabel.frame=CGRectMake(34,8,190,30);
        else
            myLabel.frame=CGRectMake(4,8,190,30);
        
		//myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
		myLabel.text=node.value;
		myLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		
		UILabel *viewmB=[[UILabel alloc]init];
		viewmB.text =[NSString stringWithFormat:@"%d~%d~%@",node.LevelVal,node.ID,node.value];
		viewmB.tag =666;
		[buttonLevelone addSubview:viewmB];
		[buttonLevelone addSubview:myLabel];
	}
	
    
	if(node.LevelVal==1)
	{
		
		UIImage *imgl2=[UIImage imageNamed:node.Fimg];
		buttonLeveltwo = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLeveltwo setBackgroundImage:imgl2 forState:UIControlStateNormal];
		buttonLeveltwo.frame = CGRectMake(53.0,7.0,205.0, 40.0);
		buttonLeveltwo.tag=indexPath.row;
		[buttonLeveltwo addTarget:self action:@selector(ShowNext:) forControlEvents:UIControlEventTouchUpInside];
        
		[cell.contentView addSubview:buttonLeveltwo];
        UILabel *myLabel = [[UILabel alloc] init];
        //pooja-iPad
        if(IS_IPAD)
            myLabel.frame=CGRectMake(34,8,190,30);
        else
            myLabel.frame=CGRectMake(4,8,190,30);
		myLabel.text=node.value;
		myLabel.textColor = [UIColor whiteColor];
		myLabel.backgroundColor = [UIColor clearColor];
		[buttonLeveltwo addSubview:myLabel];
	}
	
	if(node.LevelVal==2)
	{
		
		UIImage *imgl3=[UIImage imageNamed:node.Fimg];
		buttonLevelthree = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelthree setBackgroundImage:imgl3 forState:UIControlStateNormal];
		buttonLevelthree.frame = CGRectMake(82.0,7.0,165.0, 40.0);
		buttonLevelthree.tag=indexPath.row;
		[buttonLevelthree addTarget:self action:@selector(ShowNext:) forControlEvents:UIControlEventTouchUpInside];
        
		[cell.contentView addSubview:buttonLevelthree];
		
        UILabel *myLabel = [[UILabel alloc] init];
        //pooja-iPad
        if(IS_IPAD)
            myLabel.frame=CGRectMake(34,8,180,30);
        else
            myLabel.frame=CGRectMake(4,8,180,30);
        myLabel.text=node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		[buttonLevelthree addSubview:myLabel];
	}
	
	if(node.LevelVal==3)
	{
		
		UIImage *imgl4=[UIImage imageNamed:node.Fimg];
		buttonLevelfour = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelfour setBackgroundImage:imgl4 forState:UIControlStateNormal];
		buttonLevelfour.frame = CGRectMake(110.0,7.0,145.0, 40.0);
		buttonLevelfour.tag=indexPath.row;
		[buttonLevelfour addTarget:self action:@selector(ShowNext:) forControlEvents:UIControlEventTouchUpInside];
        
		[cell.contentView addSubview:buttonLevelfour];
        UILabel *myLabel = [[UILabel alloc] init];
        //pooja-iPad
        if(IS_IPAD)
            myLabel.frame=CGRectMake(34,8,120,30);
        else
            myLabel.frame=CGRectMake(4,8,120,30);
		myLabel.text=node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		[buttonLevelfour addSubview:myLabel];
		
	}
	
    
	if (lastProjectID != 0) {
		if (node.ID == lastProjectID) {
			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
            
            //pooja-iPad
            if(IS_IPAD)
                [tickImageView setFrame:CGRectMake(self.view.frame.size.width - tickImageView.frame.size.width - (self.view.frame.size.width - projectTblVw.frame.size.width) - 20, 25, chekMarkImage.size.width, chekMarkImage.size.height)];
            else
                [tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
			[cell.contentView addSubview:tickImageView];
		}
	}
	
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (IS_IOS_7) {
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{ //iOS 6
        
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row+1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];  
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
    //pooja-iPad
    if(IS_IPAD)
        [tickImageView setFrame:CGRectMake(384, 25, 50, 34)];
	else
        [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	[cell.contentView addSubview:tickImageView];
    
    if(node.ID)
    {
        myArray=[[NSMutableArray alloc]init ];
        y=[self fetch_level_one:node.ID];
        yy=Pname;
        [myArray addObject:yy];
        if((y)&&(y!=0)) 
        {
            x= [self fetch_level_one:y];
            xx=Pname;
            [myArray addObject:xx];
            if((x)&&(x!=0)) 
            { 
                u=[self fetch_level_one:x];
                uu=Pname;
                [myArray addObject:uu];
                if((u)&&(u!=0)) 
                { 
                    v= [self fetch_level_one:u];
                    vv=Pname;
                    [myArray addObject:vv];
                }
            }
        }
    }
    
    // Append All folder in A String By Naresh 
    NSString *finalText=[[NSString alloc]init];   
    for(int k=[myArray count] - 1;k>=0; k--)
    {
        finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
    }
    
    if ( [finalText length] > 0)
        finalText = [finalText substringToIndex:[finalText length] - 1];
    
    
    [myArray removeAllObjects];
    
    
    
    if(setVal)
    {
        
        [[self delegate] setProjectName:finalText projID:node.ID todoID:getTodoID prentID:node.prntID];  
    }
    else 
    {
        [[self delegate] setProjectName:finalText projID:node.ID prentID:node.prntID];
    }
	
	[self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)fetch_level_one:(NSInteger)projid
{
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    if (selected_stmt == nil && appDelegate.database)
    {
        Sql = [[NSString alloc]initWithFormat:@"SELECT  belongstoId,projectName from ProjectMaster where projectID =%d",projid];
       // NSLog(@"%@",Sql);
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW) 
            {
                retVal= sqlite3_column_int(selected_stmt, 0);
			    Pname= [NSString stringWithUTF8String:(char *) sqlite3_column_text(selected_stmt,1)];
                //NSLog(@"%d   =  %@",retVal,Pname);
                
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    return retVal;
}


-(IBAction)ShowNext:(id)sender{
    
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]+1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];  
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
    //pooja-iPad
    if(IS_IPAD)
        [tickImageView setFrame:CGRectMake(384, 25, 50, 34)];
    else
        [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
    
    
	
	[cell.contentView addSubview:tickImageView];
    
    if(node.ID)
    {
        myArray=[[NSMutableArray alloc]init ];
        y=[self fetch_level_one:node.ID];
        yy=Pname;
        [myArray addObject:yy];
        if((y)&&(y!=0)) 
        {
            x= [self fetch_level_one:y];
            xx=Pname;
            [myArray addObject:xx];
            if((x)&&(x!=0)) 
            { 
                u=[self fetch_level_one:x];
                uu=Pname;
                [myArray addObject:uu];
                if((u)&&(u!=0)) 
                { 
                    v= [self fetch_level_one:u];
                    vv=Pname;
                    [myArray addObject:vv];
                }
            }
        }
    }
    
    // Append All folder in A String By Naresh 
    NSString *finalText=[[NSString alloc]init];   
    for(int k=[myArray count] - 1;k>=0; k--)
    {
        finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
    }
    
    if ( [finalText length] > 0)
        finalText = [finalText substringToIndex:[finalText length] - 1];
    
    
    [myArray removeAllObjects];
    
    
    
    if(setVal)
    {
        
        [[self delegate] setProjectName:finalText projID:node.ID todoID:getTodoID prentID:node.prntID];  
    }
    else 
    {
        [[self delegate] setProjectName:finalText projID:node.ID prentID:node.prntID];
    }
	
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)PlusNow:(id)sender
{
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]];
    
	if (!node.hasChildren) return;
    
	node.inclusive = !node.inclusive;	
	[treeNode flattenElementsWithCacheRefresh:YES];
    [projectTblVw reloadData];
}

-(IBAction)noneButton:(id)sender
{
    
    //Steve changed was: objectAtIndex:[sender tag]+1]
    //was crashing when no Projects are present & user clicks "None" Project
	static NSString *CellIdentifier = @"Cell";
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];
    
    
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
    //pooja-iPad
    if(IS_IPAD)
        [tickImageView setFrame:CGRectMake(384, 25, 50, 34)];
	else
        [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	[cell.contentView addSubview:tickImageView];
    
    if(setVal)
    {
        [[self delegate] setProjectName:node.value projID:0 todoID:getTodoID prentID:node.prntID];  
    }
    else 
    {
        [[self delegate] setProjectName:[NSString stringWithFormat:@"%@",@"None"] projID:0 prentID:node.prntID];
	}
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ProjectButton_click:(id)sender
{
    NSString *selectStat=@"select count(projectID) from ProjectMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    if(CountRecords >= MAX_LIMIT_PROJECT && IS_LITE_VERSION) //Steve added IS_LITE_VERSION
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Project Folder Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d project folders.",MAX_LIMIT_PROJECT] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
    }
    else{
        
        if (IS_IOS_7) {
            AddProjectViewCantroller *projectbtn;
            if(IS_IPAD)
            {
                projectbtn =[[AddProjectViewCantroller alloc]initWithNibName: @"AddProjectViewCantrollerIPad" bundle:[NSBundle mainBundle]];
            }
            else
            {
                projectbtn =[[AddProjectViewCantroller alloc]initWithNibName: @"AddProjectViewCantroller_iOS7" bundle:[NSBundle mainBundle]];
            }
            [self.navigationController pushViewController:projectbtn animated:YES];
        }
        else{ //iOS 6
            
             AddProjectViewCantroller *projectbtn=[[AddProjectViewCantroller alloc]initWithNibName: @"AddProjectViewCantroller" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:projectbtn animated:YES];
        }
       
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1)
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];
}


- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}


#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        [addProjectButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    backButton = nil;
    addProjectButton = nil;
    projectLabelNone = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

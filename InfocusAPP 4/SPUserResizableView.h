//
//  SPUserResizableView.h
//  SPUserResizableView
//
//  Created by Stephen Poletto on 12/10/11.
//
//  SPUserResizableView is a user-resizable, user-repositionable
//  UIView subclass.

#import <Foundation/Foundation.h>
#import "ACEDrawingView.h"

typedef enum
{
    SPUserResizableViewTypeSticky,
    SPUserResizableViewTypeImage
}SPUserResizableViewType;

typedef struct SPUserResizableViewAnchorPoint {
    CGFloat adjustsX;
    CGFloat adjustsY;
    CGFloat adjustsH;
    CGFloat adjustsW;
} SPUserResizableViewAnchorPoint;

@protocol SPUserResizableViewDelegate;
@class SPGripViewBorderView;

@interface SPUserResizableView : UIView {
    SPGripViewBorderView *borderView;
    //    UIView *contentViewCustom;
    CGPoint touchStart;
    CGFloat minWidth;
    CGFloat minHeight;
    
    // Used to determine which components of the bounds we'll be modifying, based upon where the user's touch started.
    SPUserResizableViewAnchorPoint anchorPoint;
    
    //    id <SPUserResizableViewDelegate> delegate;
}


@property (nonatomic,assign) SPUserResizableViewType viewType;
@property (nonatomic, weak) id <SPUserResizableViewDelegate,ACEDrawingViewDelegate> delegate;
@property (nonatomic, assign) BOOL isResizableImage;
// Will be retained as a subview.
@property (nonatomic, assign) UIView *contentViewCustom;
@property (nonatomic, strong) ACEDrawingView *contentViewCustomUpperView;

// Default is 48.0 for each.
@property (nonatomic) CGFloat minWidth;
@property (nonatomic) CGFloat minHeight;

// Defaults to YES. Disables the user from dragging the view outside the parent view's bounds.
@property (nonatomic) BOOL preventsPositionOutsideSuperview;

@property (nonatomic) int viewNumber;

- (void)hideEditingHandles;
- (void)showEditingHandles;
- (void)setDrawingView;

@end

@protocol SPUserResizableViewDelegate <NSObject>

@optional

// Called when the resizable view receives touchesBegan: and activates the editing handles.
- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView;

// Called when the resizable view receives touchesEnded: or touchesCancelled:
- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView;

- (void)closeButtonAction:(SPUserResizableView*)resizableView;

@end

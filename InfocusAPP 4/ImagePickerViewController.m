

#import "ImagePickerViewController.h"
#import "ToolSelectionObject.h"

@interface ImagePickerViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(strong, nonatomic) UIImagePickerController *galleryImagePickerController;

@end

@implementation ImagePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.galleryImagePickerController = [[UIImagePickerController alloc] init];
    self.galleryImagePickerController.delegate = self;
    if ([ToolSelectionObject sharedObject].imageSourceType == UIImagePickerControllerSourceTypeCamera) {
        self.galleryImagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        self.galleryImagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary | UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    [self.view addSubview:self.galleryImagePickerController.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[ToolSelectionObject sharedObject] insertSelectedImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[ToolSelectionObject sharedObject] insertSelectedImage:nil
     ];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

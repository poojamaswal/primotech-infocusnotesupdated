//
//  AddCalenderViewController.m
//  Organizer
//
//  Created by Priyanka Taneja on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddCalenderViewController.h"
#import "OrganizerAppDelegate.h"
#import "CalendarDataObject.h"
#import "CalenderAddButtonController.h"

@implementation AddCalenderViewController
@synthesize delegate, calenderdataObj, addCalenderArray;

- (id)initWithCalendarID:(NSInteger) calendarIDLocal {
    if (self = [super initWithNibName:@"AddCalenderViewController" bundle:[NSBundle mainBundle]]) {
        //	lastCalendarID = calendarIDLocal;
    }
    return self;
}
- (id)initWithCalendar:(CalendarDataObject *)calendarObj{
    
    if(IS_IPAD){
        if (self = [super initWithNibName:@"AddCalenderViewController_iPad2" bundle:[NSBundle mainBundle]])
            lastCalendarObj =calendarObj;
    }
    else
    {
        if (self = [super initWithNibName:@"AddCalenderViewController" bundle:[NSBundle mainBundle]])
            lastCalendarObj =calendarObj;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [calNameTbl.layer setCornerRadius:0.0]; //was 5.0
    calNameTbl.layer.masksToBounds = YES;
    calNameTbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
    calNameTbl.layer.borderWidth = 0.0; //was 0.5
    
    calNameArr = [[NSMutableArray alloc] init];
    tickImageView = [[UIImageView alloc] init];
    IsEditing = NO;
    calDataDictionary= [[NSMutableDictionary alloc]init];
    calObj = [[CalendarDataObject alloc]init];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        if (IS_IOS_7) {
            
            if (IS_IPAD) {
                
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
            }
            else{ //iPhone
                
                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                navBar.hidden = YES;
            }
            
            
            
            
            backButton.hidden = YES;
            editButton.hidden = YES;
            doneButton.hidden = YES;
            plusButton.hidden = YES;
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            
            self.view.backgroundColor = [UIColor whiteColor];
            
            
            //done Button
            doneButton.hidden = YES;
            
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(doneClicked:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            
            //Add Button
            UIBarButtonItem *addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAppointment:)];
            
            //self.navigationItem.rightBarButtonItem = addButton_iOS7;
            
            
            
            self.title =@"Calendar";
            
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                calNameTbl.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:1];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                addButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
                //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                calNameTbl.backgroundColor= [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:1];
                
                if(IS_IPAD)
                {
                    self.navigationController.navigationBarHidden = NO;
                    _nabBar_iPad.hidden = NO;
                    [_nabBar_iPad setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    _nabBar_iPad.tintColor = [UIColor whiteColor];
                    _nabBar_iPad.barTintColor = [UIColor darkGrayColor];
                    _nabBar_iPad.translucent = YES;
                    _nabBar_iPad.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    
                    
                    
                }
                else
                {
                    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:0.7];
                    self.navigationController.navigationBar.translucent = NO;
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    
                    doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                addButton_iOS7.tintColor = [UIColor whiteColor];
            }
            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
             doneButton_iOS7.tintColor = [UIColor whiteColor];
            if(IS_IPAD)
            {
                calNameTbl.frame = CGRectMake(0, 44, 768, 980);//980
            }
            else
            {
                if (IS_IPHONE_5) {
                    calNameTbl.frame = CGRectMake(0, 0, 320, 416 + 88 + 20);
                }
                else {
                    calNameTbl.frame = CGRectMake(0, 0, 320, 416 + 20);
                }
            }
            
        }
        else{ //iOS 6
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            [calNameTbl setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
            
            navBar.hidden = YES;
            backButton.hidden = YES;
            
            editButton.hidden = NO;
            doneButton.hidden = NO;
            plusButton.hidden = NO;
            
            
            if (IS_IPHONE_5) {
                calNameTbl.frame = CGRectMake(0, 0, 320, 416 + 88);
            }
            else {
                calNameTbl.frame = CGRectMake(0, 0, 320, 416);
            }
            
            
            //NSLog(@"shareButton =  %@", NSStringFromCGRect( shareButton.frame));
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
            //myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
            titleLabel.text=@"Calendar";
            titleLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            
            self.navigationItem.titleView = titleLabel;
            
            [self.navigationController.navigationBar addSubview:editButton];
            [self.navigationController.navigationBar addSubview:plusButton];
            [self.navigationController.navigationBar addSubview:doneButton];
            
            
            //Back Button
            UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(Back_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            
            [[self navigationItem] setLeftBarButtonItem:barButton];
            
        }
        
        
        
        
    }
    else{
        
        if (!IS_IPHONE_5) { //Not iPhone5
            
        }
        
        self.navigationController.navigationBarHidden = YES;
        
        //backButton = [[UIButton alloc]init];
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    
    [self listCalendars];
    
    //UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    //[navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    
    [calNameArr removeAllObjects];
    
    
    [self getCalNameData];
    
    [calNameTbl reloadData];
    
    IsEditing = NO;
    [[self.view viewWithTag:124] setHidden:NO];
    [[self.view viewWithTag:125] setHidden:NO];
    [[self.view viewWithTag:127] setHidden:NO];
    [(UIButton*)[self.view viewWithTag:126] setBackgroundImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
    [[navBar.items objectAtIndex:0] setTitle:@"Calendar"];
    
}

- (NSDictionary *)listCalendars {
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKCalendar *calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore]; //Steve
    EKSource *theSource = nil;
    NSMutableArray *arrCal =[[NSMutableArray alloc]init];
    
    
    for (EKSource *source in eventStore.sources) {
        theSource = source;
        
        
        if (theSource && ![theSource.title isEqualToString:@"Other"])
        {
            calendar.source = theSource;
            [arrCal removeAllObjects];
            
            for (EKCalendar *thisCalendar in theSource.calendars)
            {
                
                // [arrCal addObject:thisCalendar];
                calObj = [[CalendarDataObject alloc]init];
                [calObj setCalendarIdentifire:thisCalendar.calendarIdentifier];
                [calObj setCalendarName:thisCalendar.title];
                [calObj setCalendarSource:thisCalendar.source.title];
                
                //NSLog(@" calendar.source.title = %@", thisCalendar.source.title);
                
                const CGFloat *component = CGColorGetComponents(thisCalendar.CGColor);
                [calObj setCalendarColorRed:component[0]];
                [calObj setCalendarColorGreen:component[1]];
                [calObj setCalendarColorBlue:component[2]];
                
                
                if(thisCalendar.immutable)
                    [calObj setIsCalendarImmutable:YES];
                else
                    [calObj setIsCalendarImmutable:NO];
                
                
                if(thisCalendar.allowsContentModifications)
                    [calObj setIsAllowsContentModifications:YES];
                else
                    [calObj setIsAllowsContentModifications:NO];
                
                [arrCal addObject: calObj];
                
            }
            
            if([arrCal count]>=1)
                [calDataDictionary setObject:[NSArray arrayWithArray:arrCal] forKey:theSource.title];
            
        }
        
    }
    return calDataDictionary;
}




-(IBAction)Back_buttonClicked:(id)sender
{
    if(!IsEditing)
        [self.navigationController popViewControllerAnimated:YES];
    else{
        IsEditing = NO;
        [[self.view viewWithTag:124] setHidden:NO];
        [[self.view viewWithTag:125] setHidden:NO];
        [[self.view viewWithTag:127] setHidden:NO];
        [(UIButton *)[self.view viewWithTag:126] setBackgroundImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
        [[navBar.items objectAtIndex:0] setTitle:@"Calendar"];
        [calNameTbl reloadData];
    }
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray*keys=[calDataDictionary allKeys];
    
    return [[calDataDictionary objectForKey:[keys objectAtIndex:section]] count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [calDataDictionary count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray*keys=[calDataDictionary allKeys];
    return [keys objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"];
    
    calObj = [[calDataDictionary valueForKey:[[calDataDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    
    UIButton *btnCalColorIndicator;
    
    btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(5, 22-10, 20, 20)];
    [btnCalColorIndicator setUserInteractionEnabled:NO];
    [btnCalColorIndicator setTag:40];
    [btnCalColorIndicator.layer setCornerRadius:10.0];
    btnCalColorIndicator.layer.masksToBounds = YES;
    btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
    btnCalColorIndicator.layer.borderWidth = 1.5;
    [btnCalColorIndicator.layer setOpaque:YES];
    [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:calObj.calendarColorRed green:calObj.calendarColorGreen blue:calObj.calendarColorBlue alpha:0.85]];
    [cell.contentView addSubview:btnCalColorIndicator];
    
    if(IS_IPAD)
        aTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 15, 22-15, 730, GENERAL_LABEL_HEIGHT)];
    else
        aTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 15, 22-15, 290, GENERAL_LABEL_HEIGHT)];
    
    [aTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 16.0]];
    [aTitleLabel setBackgroundColor:[UIColor clearColor]];
    [aTitleLabel setText:calObj.calendarName];
    [cell.contentView addSubview:aTitleLabel];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    
    if(![calObj isAllowsContentModifications])
        [aTitleLabel setTextColor:[UIColor lightGrayColor]];
    
    if(IsEditing){
        if([calObj isCalendarImmutable])
            [aTitleLabel setTextColor:[UIColor lightGrayColor]];
        else
            cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else{
        //Alok Modified for displaying Event Store calendar
        
        if ([lastCalendarObj.calendarIdentifire isEqualToString:calObj.calendarIdentifire]) {
            selectedCalIndex = [indexPath row];
            
            if (tickImageView) {
                [tickImageView removeFromSuperview];
            }
            
            UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
            [tickImageView setImage:checkImage];
            if(IS_IPAD)
                [tickImageView setFrame:CGRectMake(680, 10, 27, 25)];
            else
                [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
            
            //Steve added to Dim tickImageView if not allowed modified & set isCalendarAllowedToBeModified to NO
            if(!calObj.isAllowsContentModifications){
                tickImageView.alpha = 0.5;
                isCalendarAllowedToBeModified = NO; //if NO, disable didSelectRowAtIndexPath
            }
            else {
                tickImageView.alpha = 1.0;
                isCalendarAllowedToBeModified = YES; //if YES, allow didSelectRowAtIndexPath
            }
            
            [cell.contentView addSubview:tickImageView];
            lastCalendarObj = calObj;
        }
        
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(IsEditing){
        
        //Alok Gupta changed for updatd Calendar DB to Event Store
        //calObj = [calNameArr objectAtIndex:indexPath.row];
        //  calObj = [[CalendarDataObject alloc]init];
        calObj = [[calDataDictionary valueForKey:[[calDataDictionary allKeys] objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row];
        if([calObj isCalendarImmutable])
            return;
        
        CalenderAddButtonController *editCalender = [[CalenderAddButtonController alloc] initWithDataObj:calObj];
        [self.navigationController pushViewController:editCalender animated:YES];
        //  [editCalender setDelegate:self];
        IsEditing = NO;
    }
    else{
        selectedCalIndex = [indexPath row];
        calObj =[[calDataDictionary valueForKey:[[calDataDictionary allKeys] objectAtIndex:indexPath.section]]objectAtIndex:selectedCalIndex];
        
        if([calObj isAllowsContentModifications])
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            if (tickImageView) {
                [tickImageView removeFromSuperview];
            }
            
            UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
            [tickImageView setImage:checkImage];
            if(IS_IPAD)
                [tickImageView setFrame:CGRectMake(680, 10, 27, 25)];
            else
                [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
            UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
            
            [Cell.contentView addSubview:tickImageView];
            
            //Alok Commented for Change fetching calendar frome db to Event Store
            //lastCalendarID  = [[calNameArr objectAtIndex:indexPath.row] calendarID];
            lastCalendarObj = [[calDataDictionary valueForKey:[[calDataDictionary allKeys] objectAtIndex:indexPath.section]]objectAtIndex:selectedCalIndex];
            
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    id from,to;
    
    from = [calNameArr objectAtIndex:fromIndexPath.row];
    to = [calNameArr objectAtIndex:toIndexPath.row];
    [calNameArr replaceObjectAtIndex:fromIndexPath.row withObject:to];
    [calNameArr replaceObjectAtIndex:toIndexPath.row withObject:from];
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//	return YES;
//}
//
//- (void)tableView:(UITableView *) aTableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *) indexPath {
//
//	if (editingStyle == UITableViewCellEditingStyleDelete) {
//
//		NSInteger row	  = [indexPath row];
//		NSInteger section = [indexPath section];
//
//		if (section == 0) {
//			row = [indexPath row];
//		}else {
//			NSInteger tempInt = 0;
//			for (int i = 0; i < section; i++) {
//				tempInt = tempInt + [aTableView numberOfRowsInSection:i];
//			}
//			row = tempInt + [indexPath row];
//		}
//
//		calObj = [[calNameArr objectAtIndex:row] retain];
//		BOOL isDeleted = [GetAllDataObjectsClass deleteCalenderWithID:calObj.calendarID];
//
//        [calObj release];
//
//        if (isDeleted == YES)
//		{
//			[calNameArr removeObjectAtIndex:row];
//        }
// 	}
//	else if (editingStyle == UITableViewCellEditingStyleInsert)
//	{
//
//	}
//	[calNameTbl reloadData];
//}

#pragma mark -
#pragma mark All Methods
#pragma mark -

-(IBAction)doneClicked:(id)sender {
    
    
    if (IS_IPAD) {
        
        [[self delegate] setCalendarData:lastCalendarObj];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        
        [[self delegate] setCalendarData:lastCalendarObj];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void) getCalNameData {
    
    [calNameArr removeAllObjects];
    
    
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKCalendar *calendar = [EKCalendar calendarWithEventStore:eventStore];
    EKSource *theSource = nil;
    
    for (EKSource *source in eventStore.sources) {
        
        if (source.sourceType == EKSourceTypeCalDAV) {
            theSource = source;
            break;
        }
    }
    
    
    if (theSource)
        calendar.source = theSource;
    
    
    for (EKCalendar *thisCalendar in theSource.calendars)
    {
        
        int calID=[GetAllDataObjectsClass getCalendarId:thisCalendar];
        
        const CGFloat *components = CGColorGetComponents(thisCalendar.CGColor);
        CGFloat red = components[0];
        CGFloat green = components[1];
        CGFloat blue = components[2];
        // CGFloat alpha = components[3];
        
        NSString* Calname =[NSString stringWithFormat:@"%@~%@",thisCalendar.title,thisCalendar.calendarIdentifier];
        
        CalendarDataObject *calendarDataObject = [[CalendarDataObject alloc] initWithCalendarID:calID calendarName:Calname calendarColorRed:red calendarColorGreen:green calendarColorBlue:blue isOrganizerCalendar:0 isDefaultCalendar:0];
        [calNameArr addObject:calendarDataObject];
        
    }
    
    [calNameTbl reloadData];
    return;
}
-(IBAction)EditBtnClicked:(id)sender{
    //Tag set from XIB For Done->123 +->124 Edit->125 Cancel->126
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if(!IsEditing)
    {
        IsEditing = YES;
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            editButton.hidden = YES;
            doneButton.hidden = YES;
            plusButton.hidden = YES;
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
            //myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
            titleLabel.text=@"Edit Calendar";
            titleLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            
            self.navigationItem.titleView = titleLabel;
            
            UIImage *backButtonImage = [UIImage imageNamed:@"CancelSmallBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(EditBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            [[self navigationItem] setLeftBarButtonItem:barButton];
        }
        else{
            [[self.view viewWithTag:124]setHidden:YES]; //Plus Button
            [[self.view viewWithTag:125] setHidden:YES]; //Edit Button
            [[self.view viewWithTag:127] setHidden:YES]; //Done button
            [(UIButton*)[self.view viewWithTag:126] setBackgroundImage:[UIImage imageNamed:@"CancelSmallBtn.png"] forState:UIControlStateNormal]; //Tag 126 - Back Button
            [self.navigationItem setTitle:@"Edit Calendar"];
            [[navBar.items objectAtIndex:0] setTitle:@"Edit Calendar"];
        }
        
        
    }
    else{
        IsEditing = NO;
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            editButton.hidden = NO;
            doneButton.hidden = NO;
            plusButton.hidden = NO;
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
            //myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
            titleLabel.text=@"Calendar";
            titleLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            
            self.navigationItem.titleView = titleLabel;
            
            UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(Back_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            [[self navigationItem] setLeftBarButtonItem:barButton];
        }
    }
    
    
    
    [calNameTbl reloadData];
    
}

-(void)getAddCal
{
    [addCalenderArray removeAllObjects];
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    
    
    if (selected_stmt == nil && appDelegate.database)
    {
        NSString *Sql = @"SELECT * from CalenderMaster";
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW)
            {
                AddCalenderDataObject *objAddCalender = [[AddCalenderDataObject alloc] initWithCalId:sqlite3_column_int(selected_stmt, 0) calName:[NSString stringWithUTF8String:(char*)sqlite3_column_text(selected_stmt, 1)] ];
                //  calFolderImage:[NSString stringWithUTF8String:(char*)sqlite3_column_text(selected_stmt, 2)]];
                [addCalenderArray addObject:objAddCalender];
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    
    
    [calNameTbl reloadData];
}

-(IBAction)addCalenderButton:(id)sender
{
    CalenderAddButtonController *addbtn;
    if(IS_IPAD)
    {
        addbtn =[[CalenderAddButtonController alloc]initWithNibName: @"CalenderAddButtonController_iPad" bundle:[NSBundle mainBundle]];
    }else
    {
        addbtn =[[CalenderAddButtonController alloc]initWithNibName: @"CalenderAddButtonController" bundle:[NSBundle mainBundle]];
    }
    [self.navigationController pushViewController:addbtn animated:YES];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewWillDisappear:(BOOL)animated{
    NSLog(@"viewWillDisappear");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [editButton removeFromSuperview];
        [plusButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }
    
    
    [super viewWillDisappear:YES];
    
}

- (void)viewDidUnload {
    NSLog(@"viewDidUnload");
    
    navBar = nil;
    editButton = nil;
    plusButton = nil;
    backButton = nil;
    doneButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

//
//  iSpeechViewControllerVTT.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 3/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISpeechSDK.h"
#import "GlobalUtility.h"


@interface iSpeechViewControllerVTT : UIViewController<ISSpeechSynthesisDelegate,ISSpeechRecognitionDelegate>
{
    UILabel *resultsLabel;
    UITextField* searchBox;
    
    id parentVC;
}

@property (nonatomic, retain, readonly) iSpeechSDK *iSpeech;
@property(nonatomic,strong) UITextField* searchBox;
@property(nonatomic, strong)id parentVC;



-(IBAction)backBtn_Clicked:(id) sender;
-(IBAction)doneClicked:(id)sender;
-(IBAction) recognizeButtonClicked: (id) sender;
-(void)addAdditionalControlsInVoicetoText;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal;
- (void)recognition:(ISSpeechRecognition *)speechRecognition didFailWithError:(NSError *)error;
- (void)recognitionDidFinishRecording:(ISSpeechRecognition *)speechRecognition;


//+ (iSpeechViewControllerVTT *)appDelegate;

@end

//
//  AddCalenderViewController.h
//  Organizer
//
//  Created by Priyanka Taneja on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCalenderDataObject.h"
#import "CalendarDataObject.h"
#import "GetAllDataObjectsClass.h"


@interface AddCalenderViewController : UIViewController {
    IBOutlet UITableView		*calNameTbl;
    UIImageView                 *tickImageView;
    NSMutableArray              *calNameArr;
    CalendarDataObject          *lastCalendarObj;
    
    NSInteger                   selectedCalIndex;
    NSMutableDictionary         *calDataDictionary;
    id __unsafe_unretained      delegate;
    AddCalenderDataObject       *calenderdataObj;
    CalendarDataObject          *calObj;
    NSMutableArray              *addCalenderArray;
    UILabel                     *aTitleLabel;
    IBOutlet UINavigationBar    *navBar;
    BOOL                        IsEditing;
    BOOL                        isCalendarAllowedToBeModified; //Steve - if Not allowed, then doesn't allow changing calendars
    
    //Steve added
    IBOutlet UIButton *editButton;
    IBOutlet UIButton *plusButton;
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    
}

@property (nonatomic, strong) NSMutableArray *addCalenderArray;
@property (unsafe_unretained) id delegate;
@property (nonatomic, strong) AddCalenderDataObject *calenderdataObj;
@property (weak, nonatomic) IBOutlet UINavigationBar *nabBar_iPad; //Steve



-(id)initWithCalendarID:(NSInteger) calendarIDLocal;
-(id)initWithCalendar:(EKCalendar*)calendarObj;
-(IBAction) Back_buttonClicked:(id)sender;
-(IBAction) doneClicked:(id)sender;
-(void) getCalNameData;
-(IBAction) addCalenderButton:(id)sender;
-(void)getAddCal;
-(IBAction)EditBtnClicked:(id)sender;

@end

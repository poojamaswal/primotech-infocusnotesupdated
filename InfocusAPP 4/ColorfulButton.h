#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIButton(ColorfulButton)

-(void)setGradientWithHiColor:(UIColor *) colorHigh lowColor:(UIColor *) colorLow forHighlighted:(BOOL) isHighlighted;
-(void)setGradientWithHiColor:(UIColor *) colorHigh midColor:(UIColor *) colorMid lowColor:(UIColor *) colorLow forHighlighted:(BOOL) isHighlighted;

@end

//
//  AddAndEditCalendarViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/3/13.
//
//

#import "AddAndEditCalendarViewController.h"
#import "CalendarDataObject.h"
#import "OrganizerAppDelegate.h"
//#import "CustomCalendarColorViewController.h"

@interface AddAndEditCalendarViewController (){
    
    NSString        *CalIdentifier;
    
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (strong, nonatomic) IBOutlet UILabel          *deleteLabel;
@property (strong, nonatomic) IBOutlet UITextField      *titleTextField;

@property (strong, nonatomic) IBOutlet UILabel *redLabel;
@property (strong, nonatomic) IBOutlet UILabel *orangeLabel;
@property (strong, nonatomic) IBOutlet UILabel *yellowLabel;
@property (strong, nonatomic) IBOutlet UILabel *greenLabel;
@property (strong, nonatomic) IBOutlet UILabel *blueLabel;
@property (strong, nonatomic) IBOutlet UILabel *purpleLabel;
@property (strong, nonatomic) IBOutlet UILabel *brownLabel;
@property (strong, nonatomic) IBOutlet UILabel *customlabel;

@property (assign, nonatomic) BOOL              isRedColor;
@property (assign, nonatomic) BOOL              isOrangeColor;
@property (assign, nonatomic) BOOL              isYellowColor;
@property (assign, nonatomic) BOOL              isGreenColor;
@property (assign, nonatomic) BOOL              isBlueColor;
@property (assign, nonatomic) BOOL              isPurpleColor;
@property (assign, nonatomic) BOOL              isBrownColor;
@property (assign, nonatomic) BOOL              isCustomColor;

@property (strong, nonatomic) UIButton          *checkMarkButton;
@property (strong, nonatomic)EKEventStore       *eventStore;
@property (strong, nonatomic)OrganizerAppDelegate *appDelegate;

@property (assign, nonatomic) float             redColor;
@property (assign, nonatomic) float             greenColor;
@property (assign, nonatomic) float             blueColor;



-(void)deleteRowclicked;
- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender;

@end

@implementation AddAndEditCalendarViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    if (!self.isEditing) { //sets up default color if "Add Calendar"
        self.isBrownColor = YES;
    }
    
    
    if (IS_IOS_7) {
        if(IS_IPAD)
        {
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.translucent = YES;
            [self.navigationController.navigationBar setBarTintColor:[UIColor darkGrayColor]];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            _doneButton.tintColor = [UIColor whiteColor];
        }
        else
        {
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            _doneButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        }
    }
    
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    _doneButton.tintColor = [UIColor whiteColor];
    if (self.isEditing) {
        
        //self.titleTextField.frame = CGRectMake(10, 10, 250, 25);
        self.titleTextField.text = self.calendarObject.calendarName;
        CalIdentifier = self.calendarObject.calendarIdentifire;
        
        //Check if standard color from Apple's iCal
        [self checkIfStandardCalendarColorWithRed:self.calendarObject.calendarColorRed
                                    colorForGreen:self.calendarObject.calendarColorGreen
                                     colorForBlue:self.calendarObject.calendarColorBlue];
        
        
    }
    
}

-(void)checkIfStandardCalendarColorWithRed:(float)red  colorForGreen:(float)green colorForBlue:(float)blue{

    
    //Can't compare values as floats since it will never be an exact match unless we know the RGB colors in fraction format so need to convert values to string
    
    //Convert float to string so they can be compared
    NSString *redString = [NSString stringWithFormat:@"%f", red];
    NSString *greenString = [NSString stringWithFormat:@"%f", green];
    NSString *blueString = [NSString stringWithFormat:@"%f", blue];
    
    //compare the string value of the color to Apple's standard Colors in string format
    if ([redString isEqualToString:@"1.000000"] && [greenString isEqualToString:@"0.160784"] && [blueString isEqualToString:@"0.407843"])
        self.isRedColor = YES;
    else if ([redString isEqualToString:@"1.000000"] && [greenString isEqualToString:@"0.584314"] && [blueString isEqualToString:@"0.000000"])
        self.isOrangeColor = YES;
    else if ([redString isEqualToString:@"1.000000"] && [greenString isEqualToString:@"0.800000"] && [blueString isEqualToString:@"0.000000"])
        self.isYellowColor = YES;
    else if ([redString isEqualToString:@"0.388235"] && [greenString isEqualToString:@"0.854902"] && [blueString isEqualToString:@"0.219608"])
        self.isGreenColor = YES;
    else if ([redString isEqualToString:@"0.105882"] && [greenString isEqualToString:@"0.678431"] && [blueString isEqualToString:@"0.972549"])
        self.isBlueColor = YES;
    else if ([redString isEqualToString:@"0.800000"] && [greenString isEqualToString:@"0.450980"] && [blueString isEqualToString:@"0.882353"])
        self.isPurpleColor = YES;
    else if ([redString isEqualToString:@"0.635294"] && [greenString isEqualToString:@"0.517647"] && [blueString isEqualToString:@"0.368627"])
        self.isBrownColor = YES;
    else{
        
        self.isCustomColor = YES;
        self.redColor = self.calendarObject.calendarColorRed;
        self.greenColor = self.calendarObject.calendarColorGreen;
        self.blueColor = self.calendarObject.calendarColorBlue;
    }
 
}


#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{


}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numberOfSections;
    
    if (self.isEditing) {
        
        numberOfSections = 3;
    }
    else{
        
        numberOfSections = 2;
    }
    
    return numberOfSections;
}
 

/*
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
}
*/

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

}
*/

/*
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return 0;
}
*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    for (UIView  *subViews in [cell.contentView subviews]) {
        [subViews removeFromSuperview];
    }
    
    
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    
    //Checkmark - setup
    UIImage *checkMarkImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
    self.checkMarkButton = [[UIButton alloc] init];
    
    //pooja-iPad
    if(IS_IPAD)
    {
        [self.checkMarkButton setFrame: CGRectMake(720, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
    }
    else
    {
        [self.checkMarkButton setFrame: CGRectMake(290, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
    }
    
    [self.checkMarkButton setImage:checkMarkImage forState:UIControlStateNormal];
    [cell.contentView addSubview:self.checkMarkButton];
    
    
    if (!IS_IOS_7) //iOS 6
        self.checkMarkButton.frame = CGRectMake(290 - 20, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75);
    

    
    if (indexPath.section == 0) { //Title
        
        self.checkMarkButton.hidden = YES;
        [cell.contentView addSubview:self.titleTextField];
    }
    else if (indexPath.section == 1 && indexPath.row == 0){//red Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        

        if (self.isRedColor){
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        

        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:1.0 green:0.160784  blue:0.407843  alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.redLabel];
     }
    else if (indexPath.section == 1 && indexPath.row == 1){ //Orange Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isOrangeColor) {
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:1.0 green:0.584314  blue:0.0  alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.orangeLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 2){//yellow Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isYellowColor) {
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:1.0 green:0.8  blue:0.0 alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.yellowLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 3){//Green Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isGreenColor) {
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:0.388235 green:0.854902  blue:0.219608 alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.greenLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 4){//Blue Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isBlueColor) {
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:0.105882 green:0.678431  blue:0.972549 alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.blueLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 5){//Purple Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isPurpleColor) {
            
            self.checkMarkButton.hidden = NO;
        }
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:0.8 green:0.450980  blue:0.882353 alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.purpleLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 6){//Brown Label
        
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        if (self.isBrownColor)
            self.checkMarkButton.hidden = NO;
        else
            self.checkMarkButton.hidden = YES;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:0.635294 green:0.517647  blue:0.368627 alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        [cell.contentView addSubview:self.brownLabel];
    }
    else if (indexPath.section == 1 && indexPath.row == 7){//Custom Label
        
        cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        
        
        //Add Calendar Color Circle
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:self.redColor green:self.greenColor  blue:self.blueColor alpha:0.85]];
        [cell.contentView addSubview:btnCalColorIndicator];
        
        
        if (self.isCustomColor)
        {
            self.checkMarkButton.hidden = NO;
            self.customlabel.text = @"Custom Color";
            btnCalColorIndicator.hidden = NO;
            
            
            if (IS_IOS_7)
            {
                if(IS_IPAD)
                {
                    [self.checkMarkButton setFrame: CGRectMake(720, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
                }
                else
                {
                    [self.checkMarkButton setFrame: CGRectMake(290, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
                }
            }
            else //iOS 6
                self.checkMarkButton.frame = CGRectMake(260 - 20, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75);
           
        }
        else{
            
            btnCalColorIndicator.hidden = YES;
            self.checkMarkButton.hidden = YES;
        }
        
        
        [cell.contentView addSubview:self.customlabel];
    }
    else if (indexPath.section == 2){ //Delete Button
        
        self.checkMarkButton.hidden = YES;
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        [cell.contentView addSubview:self.deleteLabel];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1 && indexPath.row == 0) { //Red
        
        self.isRedColor = YES;
        self.isOrangeColor = NO;
        self.isYellowColor = NO;
        self.isGreenColor = NO;
        self.isBlueColor = NO;
        self.isPurpleColor = NO;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 1) { //Orange
        
        self.isRedColor = NO;
        self.isOrangeColor = YES;
        self.isYellowColor = NO;
        self.isGreenColor = NO;
        self.isBlueColor = NO;
        self.isPurpleColor = NO;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 2) { //Yellow
        
        self.isRedColor = NO;
        self.isOrangeColor = NO;
        self.isYellowColor = YES;
        self.isGreenColor = NO;
        self.isBlueColor = NO;
        self.isPurpleColor = NO;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 3) { //Green
        
        self.isRedColor = NO;
        self.isOrangeColor = NO;
        self.isYellowColor = NO;
        self.isGreenColor = YES;
        self.isBlueColor = NO;
        self.isPurpleColor = NO;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 4) { //Blue
        
        self.isRedColor = NO;
        self.isOrangeColor = NO;
        self.isYellowColor = NO;
        self.isGreenColor = NO;
        self.isBlueColor = YES;
        self.isPurpleColor = NO;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 5) {//Purple
        
        self.isRedColor = NO;
        self.isOrangeColor = NO;
        self.isYellowColor = NO;
        self.isGreenColor = NO;
        self.isBlueColor = NO;
        self.isPurpleColor = YES;
        self.isBrownColor = NO;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 6) {//Brown
        
        self.isRedColor = NO;
        self.isOrangeColor = NO;
        self.isYellowColor = NO;
        self.isGreenColor = NO;
        self.isBlueColor = NO;
        self.isPurpleColor = NO;
        self.isBrownColor = YES;
        self.isCustomColor = NO;
        
        [tableView reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row == 7){ //Custom color
        
         [self performSegueWithIdentifier: @"CustomCalendarColorViewController" sender: self];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {//Delete Row
        
        [self deleteRowclicked];
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    if([segue.identifier isEqualToString:@"CustomCalendarColorViewController"])
    {
        CustomCalendarColorViewController *customCalendarColorVC = segue.destinationViewController;
        customCalendarColorVC.delegate = self;
        customCalendarColorVC.calendarObject = self.calendarObject; //passes dataObject
        customCalendarColorVC.isCustomColor = self.isCustomColor;
    }
    
}


//Protocol from CustomCalendarViewController to update Calendar Custom Colors
-(void) updateCalendarObject:(CalendarDataObject*)calObject{
    
    self.isCustomColor = YES;
    self.isRedColor = NO;
    self.isOrangeColor = NO;
    self.isYellowColor = NO;
    self.isGreenColor = NO;
    self.isBlueColor = NO;
    self.isPurpleColor = NO;
    self.isBrownColor = NO;
    
    self.calendarObject = calObject; //updates CalendarObject with new colors from CustomCalendarViewController
    
    
    self.redColor = self.calendarObject.calendarColorRed;
    self.greenColor = self.calendarObject.calendarColorGreen;
    self.blueColor = self.calendarObject.calendarColorBlue;
    
    [self.tableView reloadData];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [self.titleTextField resignFirstResponder];
    
}



-(void)deleteRowclicked{
    
    
    if(self.calendarObject.calendarID == 100){ //CalendarID is never 100??????
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry! This is a default calendar that you can't delete." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    UIActionSheet *ActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure to delete this calendar?\n All events associated with the calendar will also be deleted." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Calendar" otherButtonTitles:nil,nil];
    [ActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [ActionSheet showInView:self.view];
    
}


-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex ==0){
        
        EKEventStore *eventStore = [[EKEventStore alloc] init];
        EKCalendar *calendar = [eventStore calendarWithIdentifier:CalIdentifier];
        
        if (calendar) {
            
            NSError *error = nil;
            BOOL result = [eventStore removeCalendar:calendar commit:YES error:&error];
            
            if (result) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                
                NSLog(@"Deleting calendar failed: %@.", error);
            }
        }
        
    }
    
}



- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender {
    
    
    
    NSString *cal =self.titleTextField.text;
    
    if(cal.length<1){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter calendar name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
        [alert show];
        return;
    }
    else{
        NSArray * temp = [cal componentsSeparatedByString:@"~"];
        if([temp count]>1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Calendar name should not be contain \"~\" symbol." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
            [alert show];
            return;
        }
        
    }

    
    
    if (self.isRedColor) {
        self.redColor = 255.0/255.0f;
        self.greenColor = 41.0/255.0f;
        self.blueColor = 104.0/255.0f;
    }
    else if (self.isOrangeColor){
        self.redColor = 255.0/255.0f;
        self.greenColor = 149.0/255.0f;
        self.blueColor = 0.0/255.0f;
    }
    else if (self.isYellowColor){
        self.redColor = 255.0/255.0f;
        self.greenColor = 204.0/255.0f;
        self.blueColor = 0.0/255.0f;
    }
    else if (self.isGreenColor){
        self.redColor = 99.0/255.0f;
        self.greenColor = 218.0/255.0f;
        self.blueColor = 56.0/255.0f;
    }
    else if (self.isBlueColor){
        self.redColor = 27.0/255.0f;
        self.greenColor = 173.0/255.0f;
        self.blueColor = 248.0/255.0f;
    }
    else if (self.isPurpleColor){
        self.redColor = 204.0/255.0f;
        self.greenColor = 115.0/255.0f;
        self.blueColor = 225.0/255.0f;
    }
    else if (self.isBrownColor){
        self.redColor = 162.0/255.0f;
        self.greenColor = 132.0/255.0f;
        self.blueColor = 94.0/255.0f;
    }
    
    
    
    self.appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.eventStore = self.appDelegate.eventStore;
    
    if (self.eventStore == nil) {
        self.eventStore = [[EKEventStore alloc]init];
    }
    
    
    //EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKSource *theSource = nil;
    NSError *error = nil;
    BOOL result = NO;
    
    UIColor *calcolor = [UIColor colorWithRed:self.redColor green:self.greenColor blue:self.blueColor alpha:1];
    
    
    if(!self.isEditing)
    {
        
        EKCalendar *calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:self.eventStore];
        
     
        for (EKSource *source in self.eventStore.sources)
        {
            if (source.sourceType == EKSourceTypeCalDAV &&
                [source.title isEqualToString:@"iCloud"])
            {
                theSource = source;
                break;
            }
        }
        
        if (theSource == nil)
        {
            for (EKSource *source in self.eventStore.sources)
            {
                if (source.sourceType == EKSourceTypeLocal)
                {
                    theSource = source;
                    break;
                }
            }
        }
        
        

    
        if (theSource) {
            
            calendar.source = theSource;

        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Sorry, iCal source not available on device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
            [alert show];
            return;
        }
         
        
        calendar.title = self.titleTextField.text;
        calendar.CGColor = calcolor.CGColor;
        result = [self.eventStore saveCalendar:calendar commit:YES error:&error];
        CalIdentifier = calendar.calendarIdentifier;

    }
    else{ //isEditing
        
        //Update EventStore Calendar
        EKCalendar *calendar1 = [self.eventStore calendarWithIdentifier:CalIdentifier];
        calendar1.title = self.titleTextField.text;
        calendar1.CGColor = calcolor.CGColor;
        result = [self.eventStore saveCalendar:calendar1 commit:YES error:&error];
        
    }
    
    
    if (result) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        
        NSLog(@"Error saving calendar: %@.", error);
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error saving calendar." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil] ;
        [alert show];

        
        return;
    }
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  StartEndCalendarViewController.m
//  Organizer
//
//  Created by Steven Abrams on 9/11/13.
//
//

#import "StartEndCalendarViewController.h"

@interface StartEndCalendarViewController ()

-(void) allDaySwitchValueChanged;

@end

@implementation StartEndCalendarViewController

@synthesize delegate;


-(id)initWithStartDateString:(NSString*) startDateStr
                  endDateStr:(NSString*) endDateStr
                    isAllDay:(BOOL) allDay
              startDate_Date:(NSDate*) startDate_Date
                endDate_Date:(NSDate*) endDate_Date
{
    
    self = [super initWithNibName:@"StartEndCalendarViewController" bundle:[NSBundle mainBundle]];
    
    if (self) {
        
		self.startDateString = startDateStr ;
		self.endDateString  = endDateStr ;
        
        isAllDayEvent = allDay;
        
        self.startDate_Date = startDate_Date;
        self.endDate_Date = endDate_Date;
        
        
    }
    
    //NSLog(@"isAllDayEvent  = %d",isAllDayEvent);
    //NSLog(@"self.startDateString  = %@", self.startDateString);
    //NSLog(@"self.startDate_Date  = %@", self.startDate_Date);

    
    return self;
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];

    
    self.startLabel = [[UILabel alloc]init];
    self.endLabel = [[UILabel alloc]init];
    self.allDaySwitch = [[UISwitch alloc]init];
    
    self.datePicker.timeZone = [NSTimeZone localTimeZone];
    self.datePicker.locale = [NSLocale currentLocale];
    self.datePicker.minuteInterval = 5;
    self.datePicker.calendar = [NSCalendar currentCalendar];
    self.datePicker.date = self.startDate_Date; //initial date and time of picker
    
    self.startLabel.text = self.startDateString;
    self.endLabel.text = self.endDateString;
    
    if (isAllDayEvent) {
        self.allDaySwitch.on = YES;
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    }
    else{
        self.allDaySwitch.on = NO;
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    
    
    //initial values
    isStartDateButton = YES; //makes sure picker changes initially if nothing is selected
    isEndDateButton = NO;
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = NO;
    self.title =@"Start/End";
    
   // _doneButton.frame = CGRectMake(264, 7, 51, 29);
    //[self.navigationController.navigationBar addSubview:_doneButton];
    
    UIImage *doneButtonImage = [UIImage imageNamed:@"Done.png"];
    self.doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
    [self.doneButton setBackgroundImage:doneButtonImage forState:UIControlStateNormal];
    [self.doneButton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:self.doneButton];
    [[self navigationItem] setRightBarButtonItem:barButton];
    
    

    
}


- (IBAction)datePickerValueChanged:(id)sender {
    
    self.datePicker.timeZone = [NSTimeZone localTimeZone];
    self.datePicker.locale = [NSLocale currentLocale];
    self.datePicker.minuteInterval = 5;
    self.datePicker.calendar = [NSCalendar currentCalendar];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *pickerChangedDate = self.datePicker.date;
    
    
    if (isAllDayEvent) {
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    }
    else{
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    
    
    if (isStartDateButton) {

        
        self.startLabel.text = [dateFormatter stringFromDate:pickerChangedDate];
        self.startDateString =  [dateFormatter stringFromDate:pickerChangedDate];
        self.startDate_Date = pickerChangedDate;
        
        //end date add one hour
        pickerChangedDate = [pickerChangedDate dateByAddingHours:1];
        self.endLabel.text = [dateFormatter stringFromDate:pickerChangedDate];
        self.endDateString =  [dateFormatter stringFromDate:pickerChangedDate];
        self.endDate_Date = pickerChangedDate;
        
    }
    else if (isEndDateButton){

        
        NSDate *tempEndDate = pickerChangedDate;
        
        //NSLog(@"self.startDate_Date =  %@", self.startDate_Date);
        //NSLog(@"tempEndDate =  %@", tempEndDate);
        
        
        
        if ([self.startDate_Date compare:tempEndDate] == NSOrderedDescending ||
            [self.startDate_Date compare:tempEndDate] == NSOrderedSame)
        {
            
            self.startLabel.textColor = [UIColor redColor];
            
        }
        else
        {
            
            self.startLabel.textColor = [UIColor grayColor];
        }

            
        self.endLabel.text = [dateFormatter stringFromDate:pickerChangedDate];
        self.endDateString =  [dateFormatter stringFromDate:pickerChangedDate];
        self.endDate_Date = pickerChangedDate;
    


        
    }

    /*
    NSLog(@"self.startLabel.text =  %@", self.startLabel.text);
    NSLog(@"self.startDateString =  %@", self.startDateString);
    NSLog(@"self.startDate_Date =  %@", self.startDate_Date);
    
    NSLog(@"self.endLabel.text =  %@", self.endLabel.text);
    NSLog(@"self.endDateString =  %@", self.endDateString);
    NSLog(@"self.endDate_Date =  %@", self.endDate_Date);
    */
    
}

- (void)doneButtonClicked:(id)sender {
    
    
    if ([self.startDate_Date compare:self.endDate_Date] == NSOrderedDescending ||
        [self.startDate_Date compare:self.endDate_Date] == NSOrderedSame)
    {

     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"End date can't be a date before Starting date. Please enter a date greater than starting date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     
        return;
    }
    else{
        
        [self.delegate setStartDateLabel:self.startDateString];
        [self.delegate setEndDateLabel:self.endDateString];
        
        //Brings back to parent VC to keep in date format for international compatibility
        [self.delegate setStartDateInternational:self.startDate_Date];
        [self.delegate setEndDateInternational:self.endDate_Date];
        
        [self.delegate setIsAllDay:isAllDayEvent];
 
       // NSLog(@"isAllDayEvent = %d", isAllDayEvent);
        //NSLog(@"self.startDateString = %@", self.startDateString);
        //NSLog(@"self.endDateString = %@", self.endDateString);
        
        //NSLog(@"self.startDate_Date] = %@", self.startDate_Date);
        //NSLog(@"self.endDate_Date = %@", self.endDate_Date);
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}

-(void) allDaySwitchValueChanged{
    
    
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.timeZone = [NSTimeZone localTimeZone];
    self.datePicker.locale = [NSLocale currentLocale];
    self.datePicker.minuteInterval = 5;
    self.datePicker.calendar = [NSCalendar currentCalendar];
   
    
    
    if (self.allDaySwitch.on) {
        
        isAllDayEvent = YES;
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:( NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:self.startDate_Date];
        
        [components setHour:00];
        [components setMinute:00];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.startDate_Date = [currentCalendarUsed dateFromComponents:components];
        
       // NSLog(@"self.startDate_Date = %@", self.startDate_Date);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        //Sets startLabel with No Time
        self.startLabel.text = [dateFormatter stringFromDate:self.startDate_Date];
        self.startDateString = [dateFormatter stringFromDate:self.startDate_Date];
        
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.endDate_Date = [currentCalendarUsed dateFromComponents:components];
        
        self.endLabel.text = [dateFormatter stringFromDate:self.endDate_Date];
        self.endDateString = [dateFormatter stringFromDate:self.endDate_Date];
        
        
    }
    else{
        
        isAllDayEvent = NO;
        
        self.datePicker.timeZone = [NSTimeZone localTimeZone];
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        
        
        //Set new start date uning [NSDate date] since its an absolute time
        //needed since coming from "All Day", event has no timezone & therefore causes bugs
        NSDate *todayDate = [NSDate date];
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:NSMinuteCalendarUnit
                                                            fromDate:todayDate
                                                              toDate:self.startDate_Date
                                                             options:0];
        
        
        NSInteger minutes = [components minute];
        NSDate *newDate = [todayDate dateByAddingMinutes:minutes];
        
        /*
        NSLog(@"components = %@", components);
        NSLog(@"minutes = %d", minutes);
        NSLog(@"self.startDate_Date = %@", self.startDate_Date);
        NSLog(@"self.startDate_Date = %@", [self.startDate_Date descriptionWithLocale:@"GMT"]);
        NSLog(@"newDate = %@", newDate);
        NSLog(@"newDate = %@", [newDate descriptionWithLocale:@"GMT"]);
        */
        
        
        self.startDate_Date = newDate;
        self.datePicker.date = newDate;
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        //get date strings
        self.startLabel.text = [dateFormatter stringFromDate:self.startDate_Date];
        self.startDateString = [dateFormatter stringFromDate:self.startDate_Date];
        
        //End Date add 1 hour
        NSDate *newEndDate = [self.startDate_Date dateByAddingHours:1];
        self.endDate_Date = newEndDate;
        
        self.endLabel.text = [dateFormatter stringFromDate:newEndDate];
        self.endDateString = [dateFormatter stringFromDate:newEndDate];
        
         //NSLog(@"self.startDate_Date = %@", self.startDate_Date);
         //NSLog(@"self.startDate_Date = %@", [self.startDate_Date descriptionWithLocale:@"GMT"]);
        
         
        /*
        NSDate *currentDate = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        //Finds current Date only of where you are at on the Calendar (The date your using)
        NSString *usedDateString = [dateFormatter stringFromDate:self.startDate_Date];
        NSString *useDateOnly = [usedDateString substringToIndex:11];
        
        //Find the current Time only
        NSString *currentDateString = [dateFormatter stringFromDate:currentDate];
        NSString *currentTimeOnly = [currentDateString substringFromIndex:11];

        //Adds the date and Time
        NSString *newDateAndTimeString = [useDateOnly stringByAppendingString:currentTimeOnly];
        
        NSLog(@"newDateAndTimeString = %@", newDateAndTimeString);
        
        //Saves new Date & updates picker
        self.startDate_Date = [dateFormatter dateFromString:newDateAndTimeString];
        
        
        //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        self.startLabel.text = [dateFormatter stringFromDate:self.startDate_Date];
        self.startDateString = [dateFormatter stringFromDate:self.startDate_Date];
        
        //End Date add 1 hour
        NSDate *newEndDate = [self.startDate_Date dateByAddingHours:1];
        
        self.endLabel.text = [dateFormatter stringFromDate:newEndDate];
        self.endDateString = [dateFormatter stringFromDate:newEndDate];
        
        
        //Set the date picker to correct time
        self.datePicker.date = self.startDate_Date;
        */
        
    }
    
    
}


#pragma mark -
#pragma mark TableView Methods
#pragma mark -

// Customize the number of sections in the table view.

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 10;
    return 1.0;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 5.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return 40;
    
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    }
	
    
	for (UIView  *subViews in [cell.contentView subviews]) {
		[subViews removeFromSuperview];
	}
    
    //initialize labels
    UILabel *startFixedLabel = [[UILabel alloc]init];
    UILabel *endFixedLabel = [[UILabel alloc]init];
    

    int y_Top = 7; //y of each standard label
    
    
     if (indexPath.section == 0 && indexPath.row == 0){ //Starts
        
        //UILabel *startFixedLabel = [[UILabel alloc]init];
        startFixedLabel.frame = CGRectMake(10, y_Top, 70, 24);
        startFixedLabel.text = @"Start";
        [startFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        startFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:startFixedLabel];
         
         
        self.startLabel.frame = CGRectMake(85, y_Top, 202, 24);
        self.startLabel.backgroundColor = [UIColor clearColor];
        self.startLabel.numberOfLines = 1;
        self.startLabel.minimumScaleFactor = 12;
        self.startLabel.adjustsFontSizeToFitWidth = YES;
        self.startLabel.textAlignment = NSTextAlignmentRight;
        //self.startLabel.textColor = [UIColor grayColor];
         
        [cell.contentView addSubview:self.startLabel];
            
    }
    
    
    else if (indexPath.section == 0 && indexPath.row == 1){ //End
        
        NSIndexPath *rowToSelect = [NSIndexPath indexPathForRow:0 inSection:1];
        
        if (isEndDateButton) { //highlights cell to blue if selected
            
            self.startLabel.textColor = [UIColor whiteColor];
            [tableView selectRowAtIndexPath:rowToSelect animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else{
            self.startLabel.textColor = [UIColor grayColor];
        }
        
        
        //UILabel *endFixedLabel = [[UILabel alloc]init];
        endFixedLabel.frame = CGRectMake(10, y_Top, 70, 24);
        endFixedLabel.text = @"End";
        [endFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        endFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:endFixedLabel];
        
        self.endLabel.frame = CGRectMake(85, y_Top, 202, 24);
        self.endLabel.backgroundColor = [UIColor clearColor];
        self.endLabel.numberOfLines = 1;
        self.endLabel.minimumScaleFactor = 12;
        self.endLabel.adjustsFontSizeToFitWidth = YES;
        self.endLabel.textAlignment = NSTextAlignmentRight;
        //self.endLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:self.endLabel];
        
    }
    
    
    else if (indexPath.section == 0 && indexPath.row == 2){ //All Day
        
        int y_Top = 7;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        UILabel *allDayFixedLabel = [[UILabel alloc]init];
        allDayFixedLabel.frame = CGRectMake(10, y_Top, 70, 24);
        allDayFixedLabel.text = @"All-day";
        [allDayFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        allDayFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:allDayFixedLabel];
        
        
        self.allDaySwitch.frame = CGRectMake(212, cell.frame.size.height/2 - 27/2 -2, 79, 27);
        [self.allDaySwitch addTarget:self action:@selector(allDaySwitchValueChanged) forControlEvents:UIControlEventValueChanged];
        self.allDaySwitch.userInteractionEnabled = YES;
        [cell.contentView addSubview:self.allDaySwitch];
         
    }

    
    //highlights start cell to blue if selected or on initial startup
    if (isStartDateButton) {
        
        NSIndexPath *rowToSelect = [NSIndexPath indexPathForRow:0 inSection:0];
        
        self.startLabel.textColor = [UIColor whiteColor];
        startFixedLabel.textColor = [UIColor whiteColor];
        
        [tableView selectRowAtIndexPath:rowToSelect animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    else{
        self.startLabel.textColor = [UIColor grayColor];
        startFixedLabel.textColor = [UIColor blackColor];
        
    }
    
    
    //highlight end cell blue
    if (isEndDateButton) {
        
        NSIndexPath *rowToSelect = [NSIndexPath indexPathForRow:1 inSection:0];
        
        self.endLabel.textColor = [UIColor whiteColor];
        endFixedLabel.textColor = [UIColor whiteColor];
        
        [tableView selectRowAtIndexPath:rowToSelect animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    else{
        
        self.endLabel.textColor = [UIColor grayColor];
        endFixedLabel.textColor = [UIColor blackColor];
    }

    
    cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.datePicker.timeZone = [NSTimeZone localTimeZone];
    self.datePicker.locale = [NSLocale currentLocale];
    self.datePicker.minuteInterval = 5;
    self.datePicker.calendar = [NSCalendar currentCalendar];
    
    if (isAllDayEvent) {
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    }
    else{
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    
	
    if (indexPath.section ==0 && indexPath.row == 0) { //Start
        
        isStartDateButton = YES;
        isEndDateButton = NO;
        
        self.datePicker.date = self.startDate_Date;
        
        [tableView reloadData];
    }
    if (indexPath.section == 0 && indexPath.row == 1) { //End
        
        isStartDateButton = NO;
        isEndDateButton = YES;
        
        self.datePicker.date = self.endDate_Date;
        
        [tableView reloadData];
    }
    if (indexPath.section == 0 && indexPath.row == 2) { //All Day
        
        [tableView reloadData];
    }
    
}


- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    

    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.doneButton removeFromSuperview];
    
    [super viewWillDisappear:YES];
}

- (void)viewDidUnload {
    [self setMyTableView:nil];
    [self setDatePicker:nil];
    [self setDoneButton:nil];
    self.startLabel = nil;
    self.endLabel = nil;
    self.startDate_Date = nil;
    self.endDate_Date = nil;
    self.startDateString = nil;
    self.endDateString = nil;
    self.allDaySwitch = nil;
    
    
    [super viewDidUnload];
}
@end

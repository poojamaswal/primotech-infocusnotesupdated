//
//  NotesViewCantroller.h
//  Organizer
//
//  Created by Naresh Chouhan on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrganizerAppDelegate.h"
#import "NotesDetailViewCantroller.h"
#import "InputTypeSelectorViewController.h"
#import "NotesPictureViewCantroller.h"
#import "inputTypeSelectorViewController.h"
#import "SearchBarViewController.h"

//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"






@class InputTypeSelectorViewController;


@interface NotesViewCantroller : UIViewController<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UIPopoverControllerDelegate>
{
	IBOutlet UITableView *notesTableView;
	IBOutlet UISearchBar *searchBar;
	NSMutableArray *notesDateObjArray;
    NSMutableArray *notesTitleDataArray;
	NSInteger optionVal;
	NSInteger viewStatus;
	NSUserDefaults *userDefaults;
	NSMutableArray *getImgValueArray;
	NSMutableArray *getSktValueArray,*theCopyListOfItems;
	NSInteger SetVAL;
	NotesPictureViewCantroller *notespicview;
	InputTypeSelectorViewController *inputTypeSelectorViewController;
	SearchBarViewController			*searchBarViewController;
    UIView *searchBarPlaceholderView;
    BOOL hideSearchBar;
    BOOL searching;
    BOOL letUserSelectRow;
    
    IBOutlet UINavigationBar *navBar;
    //AdWhirlView *adView;
    
    //Steve
    IBOutlet UIButton *homeBtn;
    IBOutlet UIButton *listBtn;
    IBOutlet UIButton *calendarBtn;
    IBOutlet UIButton *todoBtn;
    IBOutlet UIButton *projectBtn;
    IBOutlet UIButton *freeBtn;
    IBOutlet UIButton *settingsBtn;
    IBOutlet UIButton *addBtn;
    
    //Steve
    IBOutlet UIView *upgradeView;
    IBOutlet UIScrollView *upgradeScrollView;
    IBOutlet UINavigationBar *navBarUpgradeView;
    
    BOOL    isSearchTextBlank; //Steve added
    BOOL     isMenuClosed; //Steve
    
   
}
- (IBAction)projectButtonClicked:(id)sender;
- (IBAction)todoButtonClicked:(id)sender;

//@property(nonatomic,retain)AdWhirlView *adView;
@property(nonatomic,strong)NotesPictureViewCantroller *notespicview;
@property(readwrite)NSInteger SetVAL;
@property(nonatomic,strong)NSMutableArray *getImgValueArray;
@property(nonatomic,strong)NSMutableArray *getSktValueArray;

@property(nonatomic,strong)NSUserDefaults *userDefaults;
@property(readwrite)NSInteger optionVal;
@property(readwrite)NSInteger viewStatus;
@property(nonatomic,strong)NSMutableArray *notesTitleDataArray;
@property(strong ,nonatomic)IBOutlet UITableView *notesTableView;
@property(strong ,nonatomic)IBOutlet UISearchBar *searchBar;
@property(strong ,nonatomic)NSMutableArray *notesDateObjArray;
//Changed name to comply with ARC & getter method. 
//Can't use "copy" in begining
@property(strong ,nonatomic)NSMutableArray *theCopyListOfItems;



-(IBAction)homeButton_Clicked:(id)sender;
-(IBAction)notesViewoption_Clicked:(id)sender;
-(IBAction)addNewnotes_Clicked:(id)sender;
-(void)addNotes;
//-(IBAction)getImageData;
//-(IBAction)getSkteData;
-(IBAction)inputOptionButton_Clicked:(id)sender;
-(void)showView:(id)sender;
-(void)showPicture:(id)sender;
-(IBAction)CalendarClicked:(id)sender;
- (void) searchTableView;
- (IBAction)listBtnClicked:(id)sender;

//Steve added

-(IBAction)settingBarButton_Clicked:(id)sender;
- (IBAction)infoBarButton_Clicked:(id)sender;
- (IBAction)cancelUpgradeButton_Clicked:(id)sender ;
- (IBAction)upgradeButton_Clicked:(id)sender ;

@end

//
//  ViewProjectViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTreeNode.h"
#import "TODODataObject.h"
#import "ViewTODOScreen.h"
#import "MyTreeViewCell.h"
#import "NotesDataObject.h"


#import "ACEViewController.h"
#import "DatabaseManager.h"
#import "NotesDataObject.h"
//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"



@interface ViewProjectViewCantroller : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
	
	
	MyTreeNode *treeNode;	
	MyTreeNode *node1;
	MyTreeNode *node2;
	MyTreeNode *node3;
	MyTreeNode *node4;
	
	MyTreeNode *nodeT1;
	MyTreeNode *nodeT2;
	MyTreeNode *nodeT3;
	MyTreeNode *nodeT4;
    MyTreeViewCell *treeCell;
	
	UIButton *titleImageView;
	UIButton *titleImageViewEdit;
	UIImage *titleImage;
	
	
	NSMutableArray *getLevelRootArray;
	NSMutableArray *getLevelRootLevelArray;
	NSMutableArray *getLevelRootNameArray;
	NSMutableArray *getLevelRootTextTypeArray;
	NSMutableArray *getLevelRootBinaryArray;
    NSMutableArray *getLevelRootBinarLargeyArray;
    NSMutableArray *getLevelRootPriortyArray;
    NSMutableArray *pidRootInclusiveArray; // Anil
    NSMutableArray *getLevelRootObjectTypeArray; // Anil
    
    NSMutableArray *getLevelRootimageArray;
	
	NSMutableArray *pidleveloneArray;
	NSMutableArray *pidleveloneLevelArray;
	NSMutableArray *pidleveloneNameArray;
    NSMutableArray *pidleveloneImgArray;
    NSMutableArray *pidleveloneInclusiveArray; // Anil
    NSMutableArray *pidleveloneObjectTypeArray; // Anil
    
    
    NSMutableArray *pidlevelonePriorityArray;
    NSMutableArray *pidleveltwoPriorityArray;
    NSMutableArray *pidlevelthreePriorityArray;
	
	NSMutableArray *pidleveltwoArray;
	NSMutableArray *pidleveltwoNameArray;
	NSMutableArray *pidleveltwoLevelArray;
    NSMutableArray *pidleveltwoImgArray;
    NSMutableArray *pidleveltwoInclusiveArray; // Anil
    NSMutableArray *pidleveltwoObjectTypeArray; // Anil
    
	NSMutableArray *pidlevelthreeNameArray;
	NSMutableArray *pidlevelthreeLevelArray;
    NSMutableArray *pidlevelthreeImgArray;
    NSMutableArray *pidlevelthreeInclusiveArray; // Anil
    NSMutableArray *pidlevelthreeObjectTypeArray; // Anil
    
	NSMutableArray *getTODOTwoArray;
	NSMutableArray *getTODOTwoNameArray;
	
	NSMutableArray *getTODOThreeArray;
	NSMutableArray *getTODOThreeNameArray;
	
	NSMutableArray *TODOTitletypeOneArray;
	NSMutableArray *TODOtitleBinaryOneArray;
    NSMutableArray *TODOtitleBinaryLargeOneArray;
	
	
	NSMutableArray *TODOTitletypeTwoArray;
	NSMutableArray *TODOtitleBinaryTwoArray;
    NSMutableArray *TODOtitleBinaryLargeTwoArray;
    
    
	
	NSMutableArray *TODOTitletypeThreeArray;
	NSMutableArray *TODOtitleBinaryThreeArray;
    NSMutableArray *TODOtitleBinaryLargeThreeArray;
    
    
	
	NSMutableArray *getONE;
	
	
	NSInteger PIDval;
	BOOL editOPT;
	BOOL isEdit;
	NSInteger optnView;
	UIButton *dbtn3;
	
	UIButton *buttonLevelone;
	UIButton  *buttonLeveltwo;
	UIButton  *buttonLevelthree;
	UIButton  *buttonLevelfour;
	UIButton *test;
	NSString *select;
	NSMutableArray *getTODOTitleArray;
	NSMutableArray *getTODOIDeArray;
	
	
	NSMutableArray *getTODOTitleOneArray;
	NSMutableArray *getTODOIDOneArray;
	
    
	IBOutlet UIButton *buttontodoone;
	IBOutlet UIButton *buttontodoTwo;
	
	TODODataObject  *toDODataObject;
	TODODataObject  *toDODataObjectOne;
	TODODataObject  *toDODataObjectTwo;
	TODODataObject  *toDODataObjectThree;
	ViewTODOScreen *viewToDOScreen;
	TODODataObject *tosoDataObj;
    BOOL isEditNow;
    id __unsafe_unretained delegate;
    int	badgeCount[2];
	
	NSMutableArray *priorityRootArray;
	NSMutableArray *priorityOneArray;
	NSMutableArray *priorityTwoArray;
	NSMutableArray *priorityThreeArray;
	NSMutableArray *progresRootArray;
	NSMutableArray *progresOneArray;
	NSMutableArray *progresTwoArray;
	NSMutableArray *progresThreetArray;
	int labh;
	UIImage *proIndBorderImage;
	UIButton *pbutton;
	UIImageView *proIndBorder;
	UILabel *myLabelOne;
    
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIView *helpView;  //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;  //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView; //Steve
    
    //AdWhirlView         *adView;
    
    IBOutlet UIButton   *backButton;
    UIButton            *doneButton;
    UIButton            *editButton;
    IBOutlet UILabel    *projectViewLabel;
    
    
    
    // New Notes work karan
    
      NSMutableDictionary *_dictData;
       
    
    NotesDataObject *notesDataObject;
    NSString * noteTittleStr;
    
    
}
//@property(nonatomic,retain)AdWhirlView *adView;
@property(nonatomic,strong)NSMutableArray *pidlevelonePriorityArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoPriorityArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreePriorityArray;

@property(nonatomic,strong)NSMutableArray *getLevelRootPriortyArray;
@property(nonatomic,strong)NSMutableArray *priorityRootArray;
@property(nonatomic,strong)NSMutableArray *priorityOneArray;
@property(nonatomic,strong)NSMutableArray *priorityTwoArray;
@property(nonatomic,strong)NSMutableArray *priorityThreeArray;
@property(unsafe_unretained) id delegate;
@property(nonatomic,assign)NSInteger optnView;
@property(nonatomic,assign)BOOL isEditNow;
@property(nonatomic,strong)NSMutableArray *getLevelRootBinarLargeyArray;
@property(nonatomic,strong)NSMutableArray *TODOtitleBinaryLargeOneArray;
@property(nonatomic,strong)NSMutableArray *TODOtitleBinaryLargeTwoArray;
@property(nonatomic,strong)NSMutableArray *TODOtitleBinaryLargeThreeArray;

@property(nonatomic,strong)NSMutableArray *getLevelRootimageArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneImgArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoImgArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeImgArray;

@property(nonatomic,strong) NSMutableArray *TODOTitletypeOneArray;
@property(nonatomic,strong) NSMutableArray *TODOtitleBinaryOneArray;


@property(nonatomic,strong) NSMutableArray *TODOTitletypeTwoArray;
@property(nonatomic,strong) NSMutableArray *TODOtitleBinaryTwoArray;


@property(nonatomic,strong) NSMutableArray *TODOTitletypeThreeArray;
@property(nonatomic,strong) NSMutableArray *TODOtitleBinaryThreeArray;


@property(nonatomic,strong) NSMutableArray *getLevelRootTextTypeArray;
@property(nonatomic,strong) NSMutableArray *getLevelRootBinaryArray;
@property(nonatomic,strong)TODODataObject  *toDODataObject;
@property(nonatomic,strong)TODODataObject  *toDODataObjectOne;
@property(nonatomic,strong)TODODataObject  *toDODataObjectTwo;
@property(nonatomic,strong)TODODataObject  *toDODataObjectThree;
@property(nonatomic,strong)NSMutableArray *getTODOTitleOneArray;
@property(nonatomic,strong)NSMutableArray *getTODOIDOneArray;


@property(nonatomic,strong)NSMutableArray *getTODOTwoArray;
@property(nonatomic,strong)NSMutableArray *getTODOTwoNameArray;

@property(nonatomic,strong)NSMutableArray *getTODOThreeArray;
@property(nonatomic,strong)NSMutableArray *getTODOThreeNameArray;

@property(nonatomic,strong)NSMutableArray *getTODOIDeArray;
@property(nonatomic,strong)NSMutableArray *getTODOTitleArray;
@property(nonatomic,strong)NSMutableArray *getLevelRootLevelArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneLevelArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoLevelArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeLevelArray;
@property(nonatomic)BOOL isEdit;
@property(nonatomic,assign)BOOL editOPT;
@property(nonatomic,strong)NSMutableArray *getLevelRootNameArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneNameArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoNameArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeNameArray;

@property(nonatomic,strong)NSMutableArray *getONE;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoArray;
@property(nonatomic,assign)NSInteger PIDval;
@property(nonatomic,strong)NSMutableArray *getLevelRootArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneArray;

@property(nonatomic,strong) NSMutableArray *pidleveloneInclusiveArray, *pidRootInclusiveArray; // Anil

@property(nonatomic,strong) NSMutableArray *pidleveltwoInclusiveArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidlevelthreeInclusiveArray; // Anil

@property(nonatomic,strong) NSMutableArray *getLevelRootObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidleveloneObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidleveltwoObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidlevelthreeObjectTypeArray; // Anil



-(void)FindRecored;
-(void)getLevelOne:(NSInteger)lONE;
-(void)getLevelTwo:(NSInteger)lTWO;
-(void)getLevelThree:(NSInteger)lThree;

@property (strong, nonatomic) IBOutlet UITableView *mytable;
-(IBAction)cancel_click:(id)sender;
-(void)gotoToday:(id)sender;

-(IBAction)Edit_Project:(id)sender;
-(IBAction)Done_Project:(id)sender;

-(IBAction)gotoToEditProject:(id)sender;
-(IBAction)addSub_folder:(id)sender;
- (IBAction)helpView_Tapped:(id)sender;

- (IBAction)mailBarButton_Clicked:(id)sender;
-(void)getLevelOneTODO:(NSInteger)pidVAL;
-(void)getLevelThreeTODO:(NSInteger)pidVal;
-(void)getLevelTwoTODO:(NSInteger)pidVal;
-(void)FindTodo;
//-(IBAction)ViewTODO:(id)sender;
-(void)getBadgeCounts:(NSInteger)value;
-(void)FindAllToDo:(NSInteger)todoVal;
-(void)EditFolder:(id)sender;

-(void)gotoTodo:(id)sender;
-(void)gotoFolder:(id)sender;
-(void)PlusNow:(id)sender;
-(IBAction)Edit_ProjectNew:(id)sender;

-(void)refreshTableView;

-(void)updateInclusiveValue:(BOOL)_inclusive:(int)_treeID;

-(void)getLevelOneNote:(NSInteger)pidVAL;
-(void)FindNote;
-(void)getLevelThreeNote:(NSInteger)pidVal;
-(void)getLevelTwoNote:(NSInteger)pidVal;
-(int)getBadgeCountsForNotes:(NSInteger)value; // This method calculates the number of notes belonging to a particular project id





@end
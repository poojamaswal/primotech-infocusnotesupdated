//
//  SettingsViewController.m
//  Organizer
//
//  Created by alok gupta on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "Appirater.h"
#import "GTMBase64.h"
#import "BackupViewController.h"
#import "OrganizerAppDelegate.h" //Steve
#import "QuantcastMeasurement.h"
#import "Flurry.h" //Steve

#define exportAlertViewTag 444 

@implementation SettingsViewController

@synthesize userNameVTTAuthTextField;
@synthesize passwordVTTAuthTextField;

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
 {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }*/

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated{
    IsAlert =YES;
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"]; 
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    NSLog(@" IS_LITE_VERSION  %d", IS_LITE_VERSION);
    NSLog(@" IS_LIST_VERSION  %d", IS_LIST_VERSION);
    NSLog(@" IS_TO_DO_VERSION  %d", IS_TO_DO_VERSION);
    
    if(!IS_LITE_VERSION && !IS_LIST_VERSION && !IS_TO_DO_VERSION && !IS_PROJECTS_VER && !IS_NOTES_VER) //Only for Pro version
    {
        NSLog(@" IS_LITE_VERSION  %d", IS_LITE_VERSION);
        NSLog(@" IS_LIST_VERSION  %d", IS_LIST_VERSION);
        ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        [lbl_export removeFromSuperview];
        [btn_export removeFromSuperview];
        [img_export_line removeFromSuperview];
        
        [img_export setImage:[UIImage imageNamed:@"box-one-line.png"]];
        [img_export setFrame:CGRectMake(16, 372, 291,38)];
        [lbl_backup bringSubviewToFront:self.view];
     AppsTarget = @"Pro";
    }
    
     // NSLog(@" iTunesURL 1 = %@", ItuneURL);
    
    if(IS_LIST_VERSION)
    {
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-checklist/id584611125?ls=1&mt=8";
        AppsTarget = @"Checklist";
    }
    if(IS_LITE_VERSION)
    {
        //Steve - ************** WRONG URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-checklist/id584611125?ls=1&mt=8";
        AppsTarget = @"Lite";
    }
    if (IS_TO_DO_VERSION) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-to-do/id591343485?ls=1&mt=8";
        AppsTarget = @"TO DO";
        
        NSLog(@" iTunesURL 2 = %@", ItuneURL);
    }
    if (IS_PROJECTS_VER) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-projects/id591634217?ls=1&mt=8";
        AppsTarget = @"Projects";
    }
    if (IS_NOTES_VER) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-notes/id591713931?ls=1&mt=8";
        AppsTarget = @"Notes";
    }
    
      NSLog(@" iTunesURL 3 = %@", ItuneURL);
    
    //Steve added - Shows info button if IsVTTDevice
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	if(appdelegate.IsVTTDevice && !IS_LIST_VERSION && !IS_LITE_VERSION){
        VTTinfoButton.hidden = NO;
    }
    else
        VTTinfoButton.hidden = YES;
    
   
    //Steve added - sets up "infoButtonView" view
    infoButtonView.hidden = YES;
    infoButtonView.layer.borderWidth = 0.0f;
    infoButtonView.layer.cornerRadius = 8.0f;
    infoButtonView.layer.shadowColor = [[UIColor grayColor] CGColor];
    infoButtonView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    infoButtonView.layer.shadowOpacity = 1.0f;
    infoButtonView.layer.shadowRadius = 5.0f;
    infoButtonView.layer.masksToBounds = NO;
    infoButtonView.clipsToBounds = NO;
    
    //Sets the About page "OK" button on "infoButtonView" view
    [okButtonVTTAuth setTitle:@"OK" forState:UIControlStateNormal];
    [okButtonVTTAuth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okButtonVTTAuth setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];

    okButtonVTTAuth.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    okButtonVTTAuth.layer.cornerRadius = 10.0;
    okButtonVTTAuth.layer.borderWidth = 1.0;
    //okButtonVTTAuth.layer.borderColor = [[UIColor grayColor]CGColor];
    okButtonVTTAuth.layer.borderColor = [[UIColor whiteColor]CGColor];
    okButtonVTTAuth.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    //Sets the About page "Cancel" button on "infoButtonView" view
    [cancelButtonVTTAuth setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButtonVTTAuth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButtonVTTAuth setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
    cancelButtonVTTAuth.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    cancelButtonVTTAuth.layer.cornerRadius = 10.0;
    cancelButtonVTTAuth.layer.borderWidth = 1.0;
    //cancelButtonVTTAuth.layer.borderColor = [[UIColor grayColor]CGColor];
    //cancelButtonVTTAuth.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    cancelButtonVTTAuth.layer.borderColor = [[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]CGColor];
    cancelButtonVTTAuth.backgroundColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    
}


- (void)viewDidLoad
{
    IsAlert =YES;
    [super viewDidLoad];
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Settings" withLabels:Nil]; //Steve
    [Flurry logEvent:@"Settings"]; //Steve
    
}


-(IBAction)BackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)HelpButtonClicked:(id)sender{
    
    HelpViewController *helpView = [[HelpViewController alloc]initWithNibName:@"HelpViewController" bundle:nil];
    [self.navigationController pushViewController:helpView animated:YES];
}


-(IBAction)GiftButtonClicked:(id)sender{
    
    BOOL atLeastIOS5 = [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0;
    if (atLeastIOS5) {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Gift InFocus %@",AppsTarget] message:@"Sorry, your operating system is iOS 6. Apple currently does not have a way to gift an app in iOS 6 from your device.If you want to gift this app, you need to log onto your computer and gift it from iTunes."delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Gift InFocus %@",AppsTarget] message:@"Your family and friends will love you for helping them stay productive and \"InFocus\"." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Gift", nil];
    [alert setTag:123];
    [alert show];
    }
}


-(IBAction)ExportDataToProVersion:(id)sender{
    //Alok Added for takeing Database backup issue
    
    if(IsAlert){
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to overwrite InFocus Pro database?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
     [alert setTag:exportAlertViewTag];
        [alert show];
        
     return;
     }
    
    NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[searchPath objectAtIndex:0]  stringByAppendingString:[NSString stringWithFormat:@"/%@",@"OrganizeriOS_1.0.sqlite"]];
    NSData *fileData = [NSData dataWithContentsOfFile:path];
    
    NSString *encodedString = [GTMBase64 stringByWebSafeEncodingData:fileData padded:YES];
    
    NSString *urlString = [NSString stringWithFormat:@"myappx://localhost/importDatabase?%@", encodedString];
    NSURL *openURL = [NSURL URLWithString:urlString];
    if([[UIApplication sharedApplication] canOpenURL:openURL])
    {
        [[UIApplication sharedApplication] openURL:openURL];
    }
    else
    {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"\"InFocus Pro\" must be installed to export data to it.  Please install Infocus Pro, then export your data." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}


-(IBAction)CreateBackup:(id)sender{
    BackupViewController *backupView =[[BackupViewController alloc]initWithNibName:@"BackupViewController" bundle:nil];  
    [self.navigationController pushViewController:backupView animated:YES];
}


- (IBAction)VTTinfoButton_Clicked:(id)sender {
    NSLog(@"VTTinfoButton_Clicked");
    
    infoButtonView.userInteractionEnabled = YES;
    helpButton.userInteractionEnabled = NO;
    faqButton.userInteractionEnabled = NO;
    reccomendButton.userInteractionEnabled = NO;
    giftButton.userInteractionEnabled = NO;
    rateButton.userInteractionEnabled = NO;
    supportEmailButton.userInteractionEnabled = NO;
    btn_export.userInteractionEnabled = NO;
    btn_backup.userInteractionEnabled = NO;
    backButton.userInteractionEnabled = NO;
    doneButton.userInteractionEnabled = NO;
    
    [UIView beginAnimations:nil context:nil];
    infoButtonView.hidden = NO;
    infoButtonView.alpha = 0.0f;
    //[self.view addSubview:loadingView];
    
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:0.7];
    [infoButtonView  setAlpha:1];
    [UIView commitAnimations];
    
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //if is VTT authorized, no need to offer username & password to reset.
	if(appDelegate.isVTTDeviceAuthorized) {
        
        infoButtonView.frame = CGRectMake(20, 70, 280, 85);
        
        youAreAuthLabel.textColor = [UIColor blueColor];
        youAreAuthLabel.hidden = NO;
        
        notAvailableOnFREELabel.hidden = YES;
        
        okButtonVTTAuth.hidden = YES;
        cancelButtonVTTAuth.hidden = YES;
        userNameVTTAuthTextField.hidden = YES;
        passwordVTTAuthTextField.hidden = YES;
        userNameLabel.hidden = YES;
        passwordLabel.hidden = YES;
        
        tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelButtonVTTAuth_Clicked:)];
        [self.view addGestureRecognizer:tap];
        
    }
    else{
        infoButtonView.frame = CGRectMake(20, 70, 280, 170);
        
        youAreAuthLabel.hidden = YES;
        
        notAvailableOnFREELabel.hidden = NO;
        notAvailableOnFREELabel.textColor = [UIColor blueColor];
        
        okButtonVTTAuth.hidden = NO;
        cancelButtonVTTAuth.hidden = NO;
        userNameVTTAuthTextField.hidden = NO;
        passwordVTTAuthTextField.hidden = NO;
        userNameLabel.hidden = NO;
        passwordLabel.hidden = NO;
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn");
    
    [textField resignFirstResponder];
    [self okBuutonVTTAuth_Clicked:textField];
    
    return YES;
}


- (IBAction)okBuutonVTTAuth_Clicked:(id)sender {
     NSLog(@"okBuutonVTTAuth_Clicked");
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

    NSString *fileNameForUser = @"VTTfile";
    NSString *content = @"FFF5553Aa";
    NSError  *error;
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Checks username & password with text fields to see if correct
    if ([userNameVTTAuthTextField.text isEqualToString:fileNameForUser] && [passwordVTTAuthTextField.text isEqualToString:content] ) {
        
        appDelegate.isVTTDeviceAuthorized = YES;
        
        //get the documents directory:
        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        
        //make a directory & file name to write the data to using the documents directory:
        NSString *directoryName = [NSString stringWithFormat:@"%@/VoiceToText",documentsDirectory];
        NSString *fileName = [NSString stringWithFormat:@"%@/VTTfile.txt",directoryName];
        
        //Create Directory Folder & then write content to file
        [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
        [content writeToFile:fileName atomically:NO  encoding:NSStringEncodingConversionAllowLossy  error:&error];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authorized" message:@"You are authorized to use Voice To Text." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];


    }
    else{
        appDelegate.isVTTDeviceAuthorized = NO;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Your user name and/or password is not correct. If you downloaded the FREE version of this app you will not have Voice To Text. Please contact customer support if you feel this is an error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
    }

    infoButtonView.hidden = YES;
    infoButtonView.userInteractionEnabled = NO;
    helpButton.userInteractionEnabled = YES;
    faqButton.userInteractionEnabled = YES;
    reccomendButton.userInteractionEnabled = YES;
    giftButton.userInteractionEnabled = YES;
    rateButton.userInteractionEnabled = YES;
    supportEmailButton.userInteractionEnabled = YES;
    btn_export.userInteractionEnabled = YES;
    btn_backup.userInteractionEnabled = YES;
    backButton.userInteractionEnabled = YES;
    doneButton.userInteractionEnabled = YES;
    
    
    ///////////////////////////////////////////
    //----- TEST --> LIST ALL FILES -----
    ///////////////////////////////////////////
    
    NSLog(@"LISTING ALL FILES FOUND");
    
    NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    //make a directory & file name to write the data to using the documents directory:
    NSString *directoryName = [NSString stringWithFormat:@"%@/VoiceToText",documentsDirectory];
    
    int Count;
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:directoryName error:&error];
    
    for (Count = 0; Count < (int)[directoryContent count]; Count++)
    {
        NSLog(@"File %d: %@", (Count + 1), [directoryContent objectAtIndex:Count]);
    }
    
}


- (IBAction)cancelButtonVTTAuth_Clicked:(id)sender {
     NSLog(@"cancelButtonVTTAuth_Clicked");
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];

    if (appDelegate.isVTTDeviceAuthorized) {
         [self.view removeGestureRecognizer:tap];
    }
   
    infoButtonView.hidden = YES;
    
    infoButtonView.userInteractionEnabled = NO;
    helpButton.userInteractionEnabled = YES;
    faqButton.userInteractionEnabled = YES;
    reccomendButton.userInteractionEnabled = YES;
    giftButton.userInteractionEnabled = YES;
    rateButton.userInteractionEnabled = YES;
    supportEmailButton.userInteractionEnabled = YES;
    btn_export.userInteractionEnabled = YES;
    btn_backup.userInteractionEnabled = YES;
    backButton.userInteractionEnabled = YES;
    doneButton.userInteractionEnabled = YES;
    
}


- (IBAction)faqButton_Clicked:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"FAQViewController" bundle:nil];
    UIViewController *faqViewController = [sb instantiateViewControllerWithIdentifier:@"FAQViewController"];
    
    if (!IS_IOS_7) {
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    }

    
    [self.navigationController presentViewController:faqViewController animated:YES completion:nil];
    
}


- (IBAction)supportEmailButton_Clicked:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) 
	{
        
        if (IS_IOS_7) {
            
            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
        }
        else{
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
        
        
        NSArray *myArray;
        NSString *aString = @"InFocusAppInfo@gmail.com";
        
        myArray = [NSArray arrayWithObjects: aString, nil];
        
		MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
		[mailCntrlr setMailComposeDelegate:self];
		[mailCntrlr setSubject:[NSString stringWithFormat:@"InFocus %@ Support", AppsTarget]]; //Steve 
        [mailCntrlr setToRecipients:myArray];
        
        
		[self presentViewController:mailCntrlr animated:YES completion:nil];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
    
    
}


-(IBAction)RateButtonClicked:(id)sender{
    [Appirater appLaunched:YES];
    //    UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Rate/review in App Store" message:@"Thank you for taking the time to rate or write a review for InFocus Pro." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Rate/Review", nil];
    //    [alert setTag:124];
    //    [alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if([alertView tag]==444 && buttonIndex ==1){
        IsAlert =NO;
        // 455 tag set in nib file for export backup
        [(UIButton *)[self.view viewWithTag:455] sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    }

    
    if ([alertView tag]==123 && buttonIndex==1) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:ItuneURL]];
    }
    if ([alertView tag]==124 && buttonIndex == 1
        ) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:ItuneURL]];
    }
}


-(IBAction)RecommendButtonClicked:(id)sender{
    
    if ([MFMailComposeViewController canSendMail]) 
	{
        
        if (IS_IOS_7) {
            
            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
        }
        else{
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
        
		MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
		[mailCntrlr setMailComposeDelegate:self];
		[mailCntrlr setSubject:[NSString stringWithFormat:@"I recommend InFocus %@ App!",AppsTarget]];
        
		NSString *body = [NSString stringWithFormat:@"InFocus %@ has been a valuable tool in helping me stay organized. I think you will like this App.<br><a href=\"%@\">iTunes Link</a>",AppsTarget,ItuneURL]  ;
        //<a herf=\"http:\\apple.com\">iTunes Link</a>";
        //Steve - Link to Apple Not working.
		
        
		[mailCntrlr setMessageBody:body isHTML:YES];
		[self presentViewController:mailCntrlr animated:YES completion:nil];
        
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
    
}


#pragma mark -
#pragma mark MailComposer  Delegates
#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidUnload
{
    img_export = nil;
    img_export_line = nil;
    VTTinfoButton = nil;
    infoButtonView = nil;
    okButtonVTTAuth = nil;
    cancelButtonVTTAuth = nil;
    helpButton = nil;
    faqButton = nil;
    reccomendButton = nil;
    giftButton = nil;
    rateButton = nil;
    supportEmailButton = nil;
    userNameVTTAuthTextField = nil;
    passwordVTTAuthTextField = nil;
    userNameLabel = nil;
    passwordLabel = nil;
    youAreAuthLabel = nil;
    backButton = nil;
    doneButton = nil;
    notAvailableOnFREELabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

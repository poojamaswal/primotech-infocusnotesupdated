//
//  penColorViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 12/24/14.
//
//

#import "PenColorViewController.h"

@interface PenColorViewController ()

@property (strong, nonatomic) IBOutlet UISlider *redSlider;
@property (strong, nonatomic) IBOutlet UISlider *greenSlider;
@property (strong, nonatomic) IBOutlet UISlider *blueSlider;

@property (strong, nonatomic) IBOutlet UILabel *redSliderLabel;
@property (strong, nonatomic) IBOutlet UILabel *greenSliderLabel;
@property (strong, nonatomic) IBOutlet UILabel *blueSliderlabel;

@property (strong, nonatomic) IBOutlet UIView *colorBox;

@property (strong, nonatomic) IBOutlet UIButton *blackColor;
@property (strong, nonatomic) IBOutlet UIView *blackColorView;

@property (strong, nonatomic) IBOutlet UIButton *charcoalColor;
@property (strong, nonatomic) IBOutlet UIView *charcoalColorView;

@property (strong, nonatomic) IBOutlet UIButton *darkBlueColor;
@property (strong, nonatomic) IBOutlet UIView *darkBlueColorView;

@property (strong, nonatomic) IBOutlet UIButton *lightBlueColor;
@property (strong, nonatomic) IBOutlet UIView *lightBlueColorView;

@property (strong, nonatomic) IBOutlet UIButton *purpleColor;
@property (strong, nonatomic) IBOutlet UIView *purpleColorView;

@property (strong, nonatomic) IBOutlet UIButton *pinkColor;
@property (strong, nonatomic) IBOutlet UIView *pinkColorView;

@property (strong, nonatomic) IBOutlet UIButton *darkGreenColor;
@property (strong, nonatomic) IBOutlet UIView *darkGreenColorView;

@property (strong, nonatomic) IBOutlet UIButton *lightGreenColor;
@property (strong, nonatomic) IBOutlet UIView *lightGreenColorView;

@property (strong, nonatomic) IBOutlet UIButton *redColor;
@property (strong, nonatomic) IBOutlet UIView *redColorView;

@property (strong, nonatomic) IBOutlet UIButton *darkRedColor;
@property (strong, nonatomic) IBOutlet UIView *darkRedColorView;

@property (strong, nonatomic) IBOutlet UIButton *tealColor;
@property (strong, nonatomic) IBOutlet UIView *tealColorView;

@property (strong, nonatomic) IBOutlet UIButton *violetColor;
@property (strong, nonatomic) IBOutlet UIView *violetColorView;

@property (strong, nonatomic) IBOutlet UIButton *brownColor;
@property (strong, nonatomic) IBOutlet UIView *brownColorView;

@property (strong, nonatomic) IBOutlet UIButton *oliveColor;
@property (strong, nonatomic) IBOutlet UIView *oliveColorView;

@property (strong, nonatomic) IBOutlet UIButton *indigoColor;
@property (strong, nonatomic) IBOutlet UIView *indigoColorView;

@property (strong, nonatomic) IBOutlet UIButton *dimGrayColor;
@property (strong, nonatomic) IBOutlet UIView *dimGrayColorView;

@property (strong, nonatomic) IBOutlet UISlider *penSizeSlider;
@property (strong, nonatomic) IBOutlet UILabel *penSizeLabel;

@property (strong, nonatomic) IBOutlet UIButton *penSizeShortcut_1;
@property (strong, nonatomic) IBOutlet UIButton *penSizeShortcut_2;
@property (strong, nonatomic) IBOutlet UIButton *penSizeShortcut_3;
@property (strong, nonatomic) IBOutlet UIButton *penSizeShortcut_4;
@property (strong, nonatomic) IBOutlet UIButton *penSizeShortcut_5;

@property (strong, nonatomic) IBOutlet UISlider *alphaSlider;
@property (strong, nonatomic) IBOutlet UILabel *alphaSliderlabel;
@property (strong, nonatomic) IBOutlet UILabel *alphaLabel;


@property (assign, nonatomic) float redColorValue;
@property (assign, nonatomic) float greenColorValue;
@property (assign, nonatomic) float blueColorValue;

@property (assign, nonatomic) float drawLineWidth;



- (IBAction)palletteSliderValueChanged:(id)sender;
- (IBAction)penSizeSliderValueChanged:(id)sender;
- (IBAction)alphaSliderValueChanged:(id)sender;
-(void)highlightColorSelection;

@end





@implementation PenColorViewController

@synthesize redSlider;
@synthesize greenSlider;
@synthesize blueSlider;
@synthesize redSliderLabel;
@synthesize greenSliderLabel;
@synthesize blueSliderlabel;
@synthesize colorBox;
@synthesize blackColor;
@synthesize darkBlueColor;
@synthesize lightBlueColor;
@synthesize purpleColor;
@synthesize pinkColor;
@synthesize darkGreenColor;
@synthesize lightGreenColor;
@synthesize tealColor;
@synthesize redColor;
@synthesize darkRedColor;
@synthesize brownColor;
@synthesize violetColor;
@synthesize oliveColor, indigoColor, dimGrayColor;
@synthesize penSizeSlider;
@synthesize penSizeLabel;
@synthesize penSizeShortcut_1;
@synthesize penSizeShortcut_2;
@synthesize penSizeShortcut_3;
@synthesize penSizeShortcut_4;
@synthesize penSizeShortcut_5;
@synthesize alphaSlider;
@synthesize alphaSliderlabel;
@synthesize alphaLabel;
@synthesize redColorValue, greenColorValue, blueColorValue, drawLineWidth;
@synthesize blackColorView,charcoalColor,charcoalColorView,darkBlueColorView,lightBlueColorView,purpleColorView,pinkColorView,darkGreenColorView,lightGreenColorView,redColorView,darkRedColorView,tealColorView,violetColorView,brownColorView,oliveColorView,indigoColorView,dimGrayColorView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //NIB shows these views as blue so they are visible.  This changes it so they are not visible
    blackColorView.backgroundColor = [UIColor whiteColor];
    charcoalColorView.backgroundColor = [UIColor whiteColor];
    darkBlueColorView.backgroundColor = [UIColor whiteColor];
    lightBlueColorView.backgroundColor = [UIColor whiteColor];
    purpleColorView.backgroundColor = [UIColor whiteColor];
    pinkColorView.backgroundColor = [UIColor whiteColor];
    darkGreenColorView.backgroundColor = [UIColor whiteColor];
    lightGreenColorView.backgroundColor = [UIColor whiteColor];
    redColorView.backgroundColor = [UIColor whiteColor];
    darkRedColorView.backgroundColor = [UIColor whiteColor];
    tealColorView.backgroundColor = [UIColor whiteColor];
    violetColorView.backgroundColor = [UIColor whiteColor];
    brownColorView.backgroundColor = [UIColor whiteColor];
    oliveColorView.backgroundColor = [UIColor whiteColor];
    indigoColorView.backgroundColor = [UIColor whiteColor];
    dimGrayColorView.backgroundColor = [UIColor whiteColor];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    
    //Find User Default settings - last used values
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([sharedDefaults boolForKey:@"PenTool"]) {
        
        
        //Make sure Black Button is changed back to Black
        UIImage *blackImage = [UIImage imageNamed:@"black1.png"];
        [blackColor setImage:blackImage forState:UIControlStateNormal];
        
        //Make sure Charcoal Button is changed back to Charcoal
        UIImage *charcoalImage = [UIImage imageNamed:@"charcoal1.png"];
        [charcoalColor setImage:charcoalImage forState:UIControlStateNormal];
        
        
        redSlider.value = 255*[sharedDefaults floatForKey:@"PenRedColor"];
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        
        greenSlider.value = 255*[sharedDefaults floatForKey:@"PenGreenColor"];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        
        blueSlider.value = 255*[sharedDefaults floatForKey:@"PenBlueColor"];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        penSizeSlider.value = [sharedDefaults floatForKey:@"PenSize"];
        alphaSlider.value = 100*[sharedDefaults floatForKey:@"PenAlpha"];
        
        alphaSlider.hidden = NO;
        alphaSliderlabel.hidden = NO;
        alphaLabel.hidden = NO;
        
        NSLog(@"****** Pen ******");
        NSLog(@"redSlider Value = %f",redSlider.value);
        NSLog(@"redSlider Value = %f",greenSlider.value);
        NSLog(@"redSlider Value = %f",blueSlider.value);
        
    }
    else{ //Highlighter tool
        
        
        //Change Black button image to Yellow
        UIImage *yellowImage = [UIImage imageNamed:@"yellow.png"];
        [blackColor setImage:yellowImage forState:UIControlStateNormal];
        
        //Change Charcoal button image to Orange
        UIImage *orangeImage = [UIImage imageNamed:@"orange.png"];
        [charcoalColor setImage:orangeImage forState:UIControlStateNormal];
        
        
        
        redSlider.value = 255*[sharedDefaults floatForKey:@"highlighterRedColor"];
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        
        greenSlider.value = 255*[sharedDefaults floatForKey:@"highlighterGreenColor"];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        
        blueSlider.value = 255*[sharedDefaults floatForKey:@"highlighterBlueColor"];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        penSizeSlider.value = [sharedDefaults floatForKey:@"highlighterSize"];
        alphaSlider.value = 100*[sharedDefaults floatForKey:@"highlighterAlpha"];
        
        alphaSlider.hidden = YES;
        alphaSliderlabel.hidden = YES;
        alphaLabel.hidden = YES;
        
        NSLog(@"****** Highligher ******");
        NSLog(@"redSlider Value = %f",redSlider.value);
        NSLog(@"redSlider Value = %f",greenSlider.value);
        NSLog(@"redSlider Value = %f",blueSlider.value);
        
    }
    
    
    [self palletteSliderValueChanged:self];
    [self penSizeSliderValueChanged:self];
    [self alphaSliderValueChanged:self];
    
    [self highlightColorSelection];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Sliders and shortcuts clicked


- (IBAction)palletteSliderValueChanged:(id)sender {
    

    if (blackColor.highlighted == YES)
    {
        blackColorView.layer.borderWidth = 1;
        blackColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        if (self.isHighlighter) { //Yellow
            
            [redSlider setValue:255.0f animated:YES];
            [greenSlider setValue:255.0f animated:YES];
            [blueSlider setValue:0.0f animated:YES];
        }
        else{ //Black
            
            [redSlider setValue:0.0f animated:YES];
            [greenSlider setValue:0.0f animated:YES];
            [blueSlider setValue:0.0f animated:YES];
        }
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];

    }
    else
    {
        blackColorView.layer.borderWidth = 0;
        blackColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }
    
    if (charcoalColor.highlighted == YES)
    {
        charcoalColorView.layer.borderWidth = 1;
        charcoalColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"charcoal1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];

        
        if (self.isHighlighter) { //Orange
            
            [redSlider setValue:255.0f animated:YES];
            [greenSlider setValue:125.0f animated:YES];
            [blueSlider setValue:0.0f animated:YES];
        }
        else{ //Charcoal
            
            [redSlider setValue:red*255.0f animated:YES];
            [greenSlider setValue:green*255.0f animated:YES];
            [blueSlider setValue:blue*255.0f animated:YES];
        }
    
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    else
    {
        charcoalColorView.layer.borderWidth = 0;
        charcoalColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }
    
    if (darkBlueColor.highlighted == YES)
    {
        darkBlueColorView.layer.borderWidth = 1;
        darkBlueColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:0.0f animated:YES];
        [greenSlider setValue:0.0f animated:YES];
        [blueSlider setValue:255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
       [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        darkBlueColorView.layer.borderWidth = 0;
        darkBlueColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (lightBlueColor.highlighted == YES)
    {
        
        lightBlueColorView.layer.borderWidth = 1;
        lightBlueColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:0.0f animated:YES];
        [greenSlider setValue:128.0f animated:YES];
        [blueSlider setValue:255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    
    else
    {
        lightBlueColorView.layer.borderWidth = 0;
        lightBlueColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (purpleColor.highlighted == YES)
    {
        purpleColorView.layer.borderWidth = 1;
        purpleColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:169.0f animated:YES];
        [greenSlider setValue:0.0f animated:YES];
        [blueSlider setValue:255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        purpleColorView.layer.borderWidth = 0;
        purpleColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (pinkColor.highlighted == YES)
    {
        pinkColorView.layer.borderWidth = 1;
        pinkColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:255.0f animated:YES];
        [greenSlider setValue:0.0f animated:YES];
        [blueSlider setValue:255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
       [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        pinkColorView.layer.borderWidth = 0;
        pinkColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (darkGreenColor.highlighted == YES)
    {
        darkGreenColorView.layer.borderWidth = 1;
        darkGreenColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:0.0f animated:YES];
        [greenSlider setValue:128.0f animated:YES];
        [blueSlider setValue:0.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
       [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        darkGreenColorView.layer.borderWidth = 0;
        darkGreenColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (lightGreenColor.highlighted == YES)
    {
        lightGreenColorView.layer.borderWidth = 1;
        lightGreenColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:0.0f animated:YES];
        [greenSlider setValue:255.0f animated:YES];
        [blueSlider setValue:0.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        lightGreenColorView.layer.borderWidth = 0;
        lightGreenColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    if (redColor.highlighted == YES)
    {
        redColorView.layer.borderWidth = 1;
        redColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:255.0f animated:YES];
        [greenSlider setValue:0.0f animated:YES];
        [blueSlider setValue:0.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        redColorView.layer.borderWidth = 0;
        redColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }


    if (darkRedColor.highlighted == YES)
    {
        darkRedColorView.layer.borderWidth = 1;
        darkRedColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:128.0f animated:YES];
        [greenSlider setValue:0.0f animated:YES];
        [blueSlider setValue:0.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        darkRedColorView.layer.borderWidth = 0;
        darkRedColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }

    
    if (tealColor.highlighted == YES)
    {
        tealColorView.layer.borderWidth = 1;
        tealColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"teal1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
        
        
        [redSlider setValue:red*255.0f animated:YES];
        [greenSlider setValue:green*255.0f animated:YES];
        [blueSlider setValue:blue*255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    else
    {
        tealColorView.layer.borderWidth = 0;
        tealColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }


    if (violetColor.highlighted == YES)
    {
        violetColorView.layer.borderWidth = 1;
        violetColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"blueviolet1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
        
        
        [redSlider setValue:red*255.0f animated:YES];
        [greenSlider setValue:green*255.0f animated:YES];
        [blueSlider setValue:blue*255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        violetColorView.layer.borderWidth = 0;
        violetColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    
    if (brownColor.highlighted == YES)
    {
        brownColorView.layer.borderWidth = 1;
        brownColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        [redSlider setValue:128.0f animated:YES];
        [greenSlider setValue:64.0f animated:YES];
        [blueSlider setValue:0.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    else
    {
        brownColorView.layer.borderWidth = 0;
        brownColorView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    
    if (oliveColor.highlighted == YES)
    {
        oliveColorView.layer.borderWidth = 1;
        oliveColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"olive1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
        
        
        [redSlider setValue:red*255.0f animated:YES];
        [greenSlider setValue:green*255.0f animated:YES];
        [blueSlider setValue:blue*255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    else
    {
        oliveColorView.layer.borderWidth = 0;
        oliveColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }
    
    
    if (indigoColor.highlighted == YES)
    {
        indigoColorView.layer.borderWidth = 1;
        indigoColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"indigo1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
        
        
        [redSlider setValue:red*255.0f animated:YES];
        [greenSlider setValue:green*255.0f animated:YES];
        [blueSlider setValue:blue*255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    else
    {
        indigoColorView.layer.borderWidth = 0;
        indigoColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }
    
    
    if (dimGrayColor.highlighted == YES)
    {
        dimGrayColorView.layer.borderWidth = 1;
        dimGrayColorView.layer.borderColor = [UIColor blueColor].CGColor;
        
        //Detrermine image color in RGB since I'm not sure what it is.
        UIImage *image = [UIImage imageNamed:@"dimgray1.png"];
        UIColor *buttonColor = [self getRGBAsFromImage:image atX:10 andY:10];
        UIColor *rgbColor = buttonColor;
        CGFloat red,green,blue,alpha;
        [rgbColor getRed:&red green:&green blue:&blue alpha:&alpha];
        
        
        [redSlider setValue:red*255.0f animated:YES];
        [greenSlider setValue:green*255.0f animated:YES];
        [blueSlider setValue:blue*255.0f animated:YES];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
        
    }
    else
    {
        dimGrayColorView.layer.borderWidth = 0;
        dimGrayColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        
    }

    
    
    
    
    
    //first sees if slider moves
    if (redSlider.continuous || greenSlider.continuous || blueSlider.continuous) {
        
        //Change colorBox
        colorBox.backgroundColor = [UIColor colorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255 alpha:1.0];
        
        [redSliderLabel setText:[NSString stringWithFormat:@"%.f", redSlider.value]];
        [greenSliderLabel setText:[NSString stringWithFormat:@"%.f", greenSlider.value]];
        [blueSliderlabel setText:[NSString stringWithFormat:@"%.f", blueSlider.value]];
        
        [self.delegate setBrushColorWithRed:redSlider.value/255 green:greenSlider.value/255 blue:blueSlider.value/255];
    }
    
    
    
    
    redColorValue = redSlider.value;
    greenColorValue = greenSlider.value;
    blueColorValue = blueSlider.value;
    
}


-(void)highlightColorSelection{
    //highlights the preset colors if the saved defalut values match
    
    float red = redSlider.value;
    float green = greenSlider.value;
    float blue = blueSlider.value;
    
    
    if (red == 0.0 && green == 0.0 && blue == 0.0) {
        blackColorView.layer.borderWidth = 1;
        blackColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 255.0 && green == 255.0 && blue == 0.0){
        if (self.isHighlighter) {
            blackColorView.layer.borderWidth = 1;
            blackColorView.layer.borderColor = [UIColor blueColor].CGColor;
        }
    }
    else if (red == 50.0 && green == 50.0 && blue == 50.0) {
        charcoalColorView.layer.borderWidth = 1;
        charcoalColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 255.0 && green == 125.0 && blue == 0.0){
        if (self.isHighlighter) {
            charcoalColorView.layer.borderWidth = 1;
            charcoalColorView.layer.borderColor = [UIColor blueColor].CGColor;
        }
    }
    else if (red == 0.0 && green == 0.0 && blue == 255.0) {
        darkBlueColorView.layer.borderWidth = 1;
        darkBlueColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 0.0 && green == 128.0 && blue == 255.0) {
        lightBlueColorView.layer.borderWidth = 1;
        lightBlueColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 169.0 && green == 0.0 && blue == 255.0) {
        purpleColorView.layer.borderWidth = 1;
        purpleColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 255.0 && green == 0.0 && blue == 255.0) {
        pinkColorView.layer.borderWidth = 1;
        pinkColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 0.0 && green == 128.0 && blue == 0.0) {
        darkGreenColor.layer.borderWidth = 1;
        darkGreenColor.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 0.0 && green == 255.0 && blue == 0.0) {
        lightGreenColorView.layer.borderWidth = 1;
        lightGreenColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 255.0 && green == 0.0 && blue == 0.0) {
        redColorView.layer.borderWidth = 1;
        redColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 128.0 && green == 0.0 && blue == 0.0) {
        darkRedColorView.layer.borderWidth = 1;
        darkRedColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 0.0 && green == 128.0 && blue == 128.0) {
        tealColorView.layer.borderWidth = 1;
        tealColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 138.0 && green == 43.0 && blue == 226.0) {
        violetColorView.layer.borderWidth = 1;
        violetColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 128.0 && green == 64.0 && blue == 0.0) {
        brownColorView.layer.borderWidth = 1;
        brownColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 128.0 && green == 128.0 && blue == 0.0) {
        oliveColorView.layer.borderWidth = 1;
        oliveColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 75.0 && green == 0.0 && blue == 130.0) {
        indigoColorView.layer.borderWidth = 1;
        indigoColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else if (red == 105.0 && green == 105.0 && blue == 105.0) {
        dimGrayColorView.layer.borderWidth = 1;
        dimGrayColorView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    
    
}

- (IBAction)penSizeSliderValueChanged:(id)sender {
    
    
    //first sees if slider moves
    if (penSizeSlider.continuous) {
    
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        //Use the same String value and convert to number
        NSString *penSizeString = [NSString stringWithFormat:@"%.f", penSizeSlider.value];
        int penSize = penSizeString.intValue;

        [self.delegate SetLineWidth:penSize];
    }
    
    
    if (penSizeShortcut_1.highlighted == YES)
    {
        [penSizeSlider setValue:3.0f animated:YES];
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        [self.delegate SetLineWidth:3];
    }
    else if (penSizeShortcut_2.highlighted == YES)
    {
        [penSizeSlider setValue:7.0f animated:YES];
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        [self.delegate SetLineWidth:6];
    }
    else if (penSizeShortcut_3.highlighted == YES)
    {
        [penSizeSlider setValue:12.0f animated:YES];
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        [self.delegate SetLineWidth:9];
    }
    else if (penSizeShortcut_4.highlighted == YES)
    {
        [penSizeSlider setValue:16.0f animated:YES];
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        [self.delegate SetLineWidth:12];
    }
    else if (penSizeShortcut_5.highlighted == YES)
    {
        [penSizeSlider setValue:22.0f animated:YES];
        [penSizeLabel setText:[NSString stringWithFormat:@"%.f", penSizeSlider.value]];
        
        [self.delegate SetLineWidth:15];
    }
    
    drawLineWidth = penSizeSlider.value;
}

- (IBAction)alphaSliderValueChanged:(id)sender {
    
    if (self.isHighlighter) {
        
        [self.delegate setAlphaColor:0.50f];
    }
    else{
        
        if (alphaSlider.continuous == YES) {
            [alphaSliderlabel setText:[NSString stringWithFormat:@"%.f%@", alphaSlider.value,@"%"]];
            [self.delegate setAlphaColor:alphaSlider.value/100]; //divide by 100 to get %
        }
    }
    

}



//Method that takes image and determines the RGB color the image is, at point x,y
- (UIColor *)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy {
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,bitsPerComponent, bytesPerRow, colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
    byteIndex += 4;
    
    UIColor *acolor = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
    
    return acolor;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
     NSLog(@"****** ViewWillDisappear ******");
    
    //Save userDefaults
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if (!self.isHighlighter) {  //Pen
        
        
        [sharedDefaults setFloat:redSlider.value/255.0 forKey:@"PenRedColor"];
        [sharedDefaults setFloat:greenSlider.value/255.0 forKey:@"PenGreenColor"];
        [sharedDefaults setFloat:blueSlider.value/255.0 forKey:@"PenBlueColor"];
        
        [sharedDefaults setFloat:penSizeSlider.value forKey:@"PenSize"];
        [sharedDefaults setFloat:alphaSlider.value/100.0 forKey:@"PenAlpha"];
        
        /*
        NSLog(@"****** Pen ******");
        NSLog(@"redSlider Value = %f",redSlider.value);
        NSLog(@"redSlider Value = %f",greenSlider.value);
        NSLog(@"redSlider Value = %f",blueSlider.value);
         */
    }
    else if (self.isHighlighter){  //Highlighter
        
        [sharedDefaults setFloat:redSlider.value/255.0 forKey:@"highlighterRedColor"];
        [sharedDefaults setFloat:greenSlider.value/255.0 forKey:@"highlighterGreenColor"];
        [sharedDefaults setFloat:blueSlider.value/255.0 forKey:@"highlighterBlueColor"];
        
        [sharedDefaults setFloat:penSizeSlider.value forKey:@"highlighterSize"];
        [sharedDefaults setFloat:alphaSlider.value/100.0 forKey:@"highlighterAlpha"];
        
        /*
        NSLog(@"****** Highligher ******");
        NSLog(@"redSlider Value = %f",redSlider.value);
        NSLog(@"redSlider Value = %f",greenSlider.value);
        NSLog(@"redSlider Value = %f",blueSlider.value);
        */
    }
    

    /*
    NSLog(@"@PenSize %f", [sharedDefaults floatForKey:@"PenSize"]);
    NSLog(@"@penSizeSlider %f", penSizeSlider.value);
    
    NSLog(@"@PenAlpha %f", [sharedDefaults floatForKey:@"PenAlpha"]);
    NSLog(@"@alphaSlider %f", alphaSlider.value);
    */
    
}



@end

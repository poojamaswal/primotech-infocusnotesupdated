

#import "NotesSelectionVC.h"

#import "ACEViewController.h"
#import "NotesID.h"
#import "DatabaseManager.h"
#import "NotesTableCell.h"
#import "SWRevealViewController.h"

#define IS_IPAD ([[UIScreen mainScreen]bounds].size.height == 1024)


@interface NotesSelectionVC ()
{
    IBOutlet UITableView * _tableView;
    NSMutableDictionary *_dictData;
        NSInteger noteCount;
}
@end

@implementation NotesSelectionVC




- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    self.title=@"Notes";
    _tableView.dataSource   = self;
    _tableView.delegate     = self;
    _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    _dictData = [[NSMutableDictionary alloc]init];
    
    
    [_tableView registerNib:[UINib nibWithNibName:CellIdentifier bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    
    [_dictData setValue:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] forKey:KeyNotesData];
    [_tableView reloadData];
    
    _tableView.editing=NO;
    

    
    if (!IS_IPAD)
    {
        self.navigationController.navigationBarHidden=YES;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
        
        [_btnEdit setTitle:@"" forState:UIControlStateNormal];
        [_btnEdit setImage:[UIImage imageNamed:@"reveal-icon"] forState:UIControlStateNormal];
        _btnEdit.tag=101;
    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [_dictData setValue:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] forKey:KeyNotesData];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ActionMethods
- (IBAction)editButtonAction:(id)sender
{
    if (!IS_IPAD)
    {
        if (_btnEdit.tag==101)
        {
            SWRevealViewController *revealController = [self revealViewController];
            //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
            [revealController revealToggle:self];
        }
    }
    else
    {
        if(!_tableView.editing)
            [_tableView setEditing:YES animated:YES];
        else
            [_tableView setEditing:NO animated:YES];
    }
}

- (IBAction)newNotesPageAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"Pen"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"Brush"];
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"noteCount"])
    {
        noteCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"noteCount"] integerValue]+1;
    }else
    {
        noteCount=1;
        
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInteger:noteCount] forKey:@"noteCount"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    ACEViewController *aACEViewController ;
    
    if(IS_IPAD)
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"ACEViewController" bundle:nil];
    else
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"AVEViewController-iPhn" bundle:nil];
    
    aACEViewController.boolNewNotepage = YES;
    [aACEViewController newRecordOrExistingRecord:YES withTotalCount:noteCount];
    
    [self.navigationController pushViewController:aACEViewController animated:YES];
}

#pragma mark - TableView DataSource

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotesTableCell *cell = (NotesTableCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[NotesTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell setTextHeader:[[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] objectAtIndex:indexPath.row]];
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray*)[_dictData valueForKey:KeyNotesData]).count;
}


#pragma mark - TableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ACEViewController *aACEViewController ;
    
    if(IS_IPAD)
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"ACEViewController" bundle:nil];
    else
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"AVEViewController-iPhn" bundle:nil];
    
    aACEViewController.boolNewNotepage = NO;
    
    [aACEViewController setRowSelected:[[_dictData valueForKey:KeyNotesData] objectAtIndex:indexPath.row] atSelectedIndex:indexPath.row withTotalCount: ((NSArray*)[_dictData valueForKey:KeyNotesData]).count ];
    
    [aACEViewController newRecordOrExistingRecord:NO withTotalCount:((NSArray*)[_dictData valueForKey:KeyNotesData]).count];
    [self.navigationController pushViewController:aACEViewController animated:YES];
    
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *str =  [[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] objectAtIndex:indexPath.row];
    [self loadDataFromDatabaseWithIndex:indexPath.row];
    [[DatabaseManager sharedManager] deleteNotesWithID:str];
    
    [_dictData setValue:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] forKey:KeyNotesData];
    [_tableView reloadData];
    
    if(((NSArray*)[_dictData valueForKey:KeyNotesData]).count ==0)
    {
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"noteCount"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
}

 -(void)deleteNotes:(NSString *)noteTittle
{
    BOOL isInserted = [AllInsertDataMethods deleteNoteWithTittle:noteTittle];
    if (isInserted)
    {
        NSLog(@"Deleted");
    }
    
}
- (void)loadDataFromDatabaseWithIndex :(NSInteger)index
{
    
    NSLog(@"%@",[[_dictData valueForKey:KeyNotesData] objectAtIndex:index]);
    
    NotesID *nm = [DatabaseManager pageForName:[[_dictData valueForKey:KeyNotesData] objectAtIndex:index]];
    
    idForProject=[nm.projectID integerValue];
    
    if (idForProject !=0)
    {
        NSLog(@"relates with project");
        
        [self deleteNotes:[[_dictData valueForKey:KeyNotesData] objectAtIndex:index]];
        
    }
    NSLog(@"%d",idForProject);
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

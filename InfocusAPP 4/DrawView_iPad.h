//
//  NoteView_iPad.h
//  Organizer
//
//  Created by Steven Abrams on 11/29/14.
//
//

#import <UIKit/UIKit.h>
#import "PenColorViewController.h"
#import "DrawView_iPad.h"
#import "ACEDrawingTools.h"


#define ACEDrawingViewVersion   1.0.0

typedef enum {
    DrawingToolTypePen,
    DrawingToolTypeLine,
    DrawingToolTypeRectagleStroke,
    DrawingToolTypeRectagleFill,
    DrawingToolTypeEllipseStroke,
    DrawingToolTypeEllipseFill,
    DrawingToolTypeEraser,
    DrawingToolTypeText
} DrawingToolType;

//@protocol DrawingViewDelegate, DrawingTool;
//@end
@class DrawView_iPad;
@class drawingTool;


@protocol DrawingViewDelegate <NSObject>

@optional
- (void)drawingView:(DrawView_iPad *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool;
- (void)drawingView:(DrawView_iPad *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool;

@end





@interface DrawView_iPad : UIView <UITextViewDelegate>


@property (nonatomic, assign) DrawingToolType drawTool;
@property (nonatomic, assign) id<DrawingViewDelegate> delegate;


// public properties
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat lineAlpha;

// get the current drawing
@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, strong) UIImage *prev_image;
@property (nonatomic, readonly) NSUInteger undoSteps;

//Disables Draw. Need for zooming
@property (nonatomic, assign) float zoomScale;

// load external image
- (void)loadImage:(UIImage *)image;
- (void)loadImageData:(NSData *)imageData;


// erase all
- (void)clear;

// undo / redo
- (BOOL)canUndo;
- (void)undoLatestStep;

- (BOOL)canRedo;
- (void)redoLatestStep;




@end



#pragma mark -


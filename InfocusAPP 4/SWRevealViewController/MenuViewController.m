
/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

*/

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "LIstToDoProjectViewController.h"
#import "ProjectMainViewCantroller.h"
#import "NotesViewCantroller.h"
#import "CalendarParentController.h"
#import "ToDoParentController.h"

@interface MenuViewController()

@end

@implementation MenuViewController

@synthesize rearTableView = _rearTableView;

#pragma marl - UITableView Data Source

-(void) viewDidLoad{
    //NSLog(@"viewDidLoad MenuViewController");
    [super viewDidLoad];
    
    myCellBackgroundColor = [UIColor colorWithRed:58.0/255.0 green:58.0/255.0 blue:58.0/255.0 alpha:1.0];
    myBackgroundColor = myCellBackgroundColor;
    myFontColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0];
    myFontName = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    _rearTableView.backgroundColor = myBackgroundColor;
    _rearTableView.separatorColor = myCellBackgroundColor;
    //_rearTableView.separatorColor = [UIColor whiteColor];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear MenuViewController");
    
    [super viewWillAppear:animated];
    
    //NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
    
    if (IS_IOS_7) {
        
        navBar.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        //self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.barTintColor = myBackgroundColor;
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
        
        if (IS_IPHONE_5) {
            self.view.frame = CGRectMake(0, 0, 320, 568);
            //self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20);
            
        }
        else {
            self.view.frame = CGRectMake(0, 0, 320, 480);
            //self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20);
            
        }
        
        
        
    }
    else{ //iOS 6
        
        self.navigationController.navigationBarHidden = YES;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    }

    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    // SWRevealViewController *revealController = self.revealViewController;
    
    //UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    
    /*
    if ( [frontNavigationController.topViewController isKindOfClass:[CalendarParentController class]] )
    {
        
        CalendarParentController *calendarParentController = [[CalendarParentController alloc] init];
        
    }
    
    
    if ( [frontNavigationController.topViewController isKindOfClass:[ToDoParentController class]] )
    {
        ToDoParentController *toDoParentController = [[ToDoParentController alloc] init];
        //[toDoParentController invisibleViewTriggeredWithDelay];
    }
     */
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
	
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
	
	if (nil == cell)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
	}
    
    //cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;

    
     //Steve added -  Set selected cell color
     UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
     myBackView.backgroundColor = myCellBackgroundColor;
     cell.selectedBackgroundView = myBackView;
     
    
    //makes button size of cell so I can add borders to cell while keeping blank cells all black
    //otherwise blank cells will be color of the cell line separator
    UIButton *cellButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    //added so cell will recognize touch instead of Button and didSelectRowAtIndexPath method will be invoked for Edit
    [cellButton setUserInteractionEnabled:NO];
    cellButton.layer.borderWidth = 1;
    //cellButton.layer.borderColor = [[UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]CGColor];
    cellButton.layer.borderColor = [myCellBackgroundColor CGColor];
    cellButton.layer.backgroundColor = [myCellBackgroundColor CGColor];
    [cell.contentView addSubview:cellButton];


    
    //Clears the cell text
    cell.textLabel.text = nil;

    
    UILabel *titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 11, 120, 22)];
    [titlelabel setBackgroundColor:[UIColor clearColor]];
    [titlelabel setFont:myFontName];
    [titlelabel setTextColor:myFontColor];
    titlelabel.highlightedTextColor = [UIColor colorWithRed:135.0/255.0 green:206.0/255.0 blue:250.0/255.0 alpha:1.0];
    [cell.contentView addSubview:titlelabel];
    
    cell.imageView.frame = CGRectMake(5, 18, 26, 24);

     NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
 	if (row == 0)
	{
        
        if ([sharedDefaults boolForKey:@"StartUpCalendar"]) {
            //Steve - sets as default selected cell
            [tableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
        
        
        titlelabel.text = @"Calendar";
        
        //cell.imageView.image = [UIImage imageNamed:@"CalendarIcon_ToDo.png"];
        //cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
        //titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
	}
	else if (row == 1)
	{
        
        if ([sharedDefaults boolForKey:@"StartUpList"]) {
            //Steve - sets as default selected cell
            [tableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
        
        
        titlelabel.text = @"Checklist";
        
        //cell.imageView.image = [UIImage imageNamed:@"navlist-icon_ToDo.png"];
        //cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
        //titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
	}
	else if (row == 2)
	{
        
        if ([sharedDefaults boolForKey:@"StartUpToDo"]) {
            //Steve - sets as default selected cell
            [tableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
        
        
        titlelabel.text = @"To Do";
        
        //cell.imageView.image = [UIImage imageNamed:@"header-icon.png"];
        //cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
       // titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
	}
	else if (row == 3)
	{
        
        if ([sharedDefaults boolForKey:@"StartUpProjects"]) {
            //Steve - sets as default selected cell
            [tableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
        
        
        titlelabel.text = @"Projects";
        
        //cell.imageView.image = [UIImage imageNamed:@"projects-icon_ToDo.png"];
       // cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
       // titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
	}
    else if (row == 4)
    {
        
        if ([sharedDefaults boolForKey:@"StartUpNotes"]) {
            //Steve - sets as default selected cell
            [tableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
        

        titlelabel.text = @"Notes";
        
        //cell.imageView.image = [UIImage imageNamed:@"notes-icon.png"];
        //cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
        //titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
    }
    else if (row == 5)
    {

        titlelabel.text = @"Settings";
        
       // cell.imageView.image = [UIImage imageNamed:@"SetttingTabIcon.png"];
       // cell.imageView.highlightedImage = [UIImage imageNamed:@"Done.png"];
        
       // titlelabel.frame = CGRectMake(cell.imageView.image.size.width + 25, 11, titlelabel.frame.size.width, titlelabel.frame.size.height);
        
    }
    
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    NSInteger row = indexPath.row;

	// Here you'd implement some of your own logic... I simply take for granted that the first row (=0) corresponds to the "FrontViewController".
	if (row == 0)
	{
        
		// Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
        //if ( ![frontNavigationController.topViewController isKindOfClass:[CalendarParentController class]] )
       // {
        //Steve  - this allows frontViewController to load viewDidLoad everytine, which sets the BOOL isMenuClosed to NO
        CalendarParentController *calendarParentController = [[CalendarParentController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:calendarParentController];
        [revealController setFrontViewController:navigationController animated:YES];
        //}
		// Seems the user attempts to 'switch' to exactly the same controller he came from!
        /* //Steve commented - this allows frontViewController to load viewDidLoad, which sets the BOOL isMenuClosed to NO
		else
		{
			[revealController revealToggle:self];
		}
         */
	}
    
	else if (row == 1)
	{
        LIstToDoProjectViewController *listToDoProjectViewController = [[LIstToDoProjectViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listToDoProjectViewController];
        [revealController setFrontViewController:navigationController animated:YES];
	}
	else if (row == 2)
	{
        ToDoParentController *todoParentViewController = [[ToDoParentController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:todoParentViewController];
        [revealController setFrontViewController:navigationController animated:YES];
    
	}
	else if (row == 3)
	{
        ProjectMainViewCantroller *projectMainViewController = [[ProjectMainViewCantroller alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:projectMainViewController];
        [revealController setFrontViewController:navigationController animated:YES];

	}
    else if (row == 4)
	{
        
        NotesSelectionVC * notesObj=[[NotesSelectionVC alloc]init];
        
        
        //NotesViewCantroller *notesViewController = [[NotesViewCantroller alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:notesObj];
        [revealController setFrontViewController:navigationController animated:YES];
	}
    else if (row == 5)
	{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SettingsTableViewController" bundle:nil];
        UIViewController *settingsTableViewController = [sb instantiateViewControllerWithIdentifier:@"SettingsTableViewController"];
        
        //UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        //[[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [revealController setFrontViewController:settingsTableViewController animated:YES];
	}
    
}



//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}

- (void)viewDidUnload {
    navBar = nil;
    [super viewDidUnload];
}
@end
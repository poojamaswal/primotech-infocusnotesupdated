//
//  AddAlertViewController.h
//  Organizer
//
//  Created by Priyanka Taneja on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddAlertViewController.h"
#import "OrganizerAppDelegate.h"

@interface AddAlertViewController : UIViewController {
	IBOutlet UILabel				*lblDateTime;
	IBOutlet UISegmentedControl		*segDateTime;
	IBOutlet UIDatePicker			*datePicker;
	IBOutlet UIPickerView			*valuePicker;
	IBOutlet UIToolbar				*barDone;
	
	NSMutableArray					*array;
	NSMutableArray					*pickerviewarray;
	
	NSString						*alertString;
	id	__unsafe_unretained         delegate;
    OrganizerAppDelegate            *appDelegate;
    NSInteger                       lastSelected;//Anil's Added
    IBOutlet UINavigationBar        *navBar;
    
    IBOutlet UIButton               *cancelButton;
    IBOutlet UIButton               *doneButton;
    
}

@property (unsafe_unretained) id delegate;
@property (nonatomic, strong) NSString *alertString;
@property (nonatomic,  strong) NSDate *alertDate_Date; //Steve

-(IBAction) Back_buttonClicked:(id)sender;
-(IBAction) doneClicked:(id)sender;
-(IBAction) dateChanged;
-(IBAction) segment_Clicked;

//- (id)initWithAlertString:(NSString *) alert;
- (id)initWithAlertString:(NSString *) alert alertDate:(NSDate*) alertDateFormat; //Steve


@end

//
//  ProjectsDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProjectsDataObject.h"

@implementation ProjectsDataObject
@synthesize projectName,projectID,projectLevel;

-(id)initWithProjectId:(NSInteger) projectIdLocal projectTitle:(NSString*)projectTitleLocal{
	if (self = [super init]) {
		self.projectID = projectIdLocal;
		self.projectName = projectTitleLocal;
	}
	return self;
}

@end

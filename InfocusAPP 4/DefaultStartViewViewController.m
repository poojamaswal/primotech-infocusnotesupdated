//
//  DefaultStartViewViewController.m
//  Organizer
//
//  Created by Steven Abrams on 8/28/13.
//
//

#import "DefaultStartViewViewController.h"

@interface DefaultStartViewViewController ()

@property (strong, nonatomic) IBOutlet UISwitch *calendarSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *listSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *todoSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *projectsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *notesSwitch;



- (IBAction)calendarSwitch_ValueChanged:(id)sender;
- (IBAction)listSwitch_ValueChanged:(id)sender;
- (IBAction)todoSwitch_ValueChanged:(id)sender;
- (IBAction)projectsSwitch_ValueChanged:(id)sender;
- (IBAction)notesSwitch_ValueChanged:(id)sender;

- (IBAction)doneButton_Clicked:(id)sender;


@end

@implementation DefaultStartViewViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        
        //pooja-iPad
        if(IS_IPAD)
        {
            self.calendarSwitch.frame = CGRectMake(656, 8, 49, 31);
            self.listSwitch.frame = CGRectMake(656, 8, 49, 31);
            self.todoSwitch.frame = CGRectMake(656, 8, 49, 31);
            self.projectsSwitch.frame = CGRectMake(656, 8, 49, 31);
            self.notesSwitch.frame = CGRectMake(656, 8, 49, 31);
        }
        else
        {
            self.calendarSwitch.frame = CGRectMake(251, 8, 51, 31);
            self.listSwitch.frame = CGRectMake(251, 8, 51, 31);
            self.todoSwitch.frame = CGRectMake(251, 8, 51, 31);
            self.projectsSwitch.frame = CGRectMake(251, 8, 51, 31);
            self.notesSwitch.frame = CGRectMake(251, 8, 51, 31);
        }
        
        self.calendarSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        self.listSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        self.todoSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        self.projectsSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        self.notesSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
    }
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"StartUpCalendar"]) {
        self.calendarSwitch.on = YES;
        self.listSwitch.on = NO;
        self.todoSwitch.on = NO;
        self.projectsSwitch.on = NO;
        self.notesSwitch.on = NO;
    }
    else if ([sharedDefaults boolForKey:@"StartUpList"]) {
        self.calendarSwitch.on = NO;
        self.listSwitch.on = YES;
        self.todoSwitch.on = NO;
        self.projectsSwitch.on = NO;
        self.notesSwitch.on = NO;
    }
    else if ([sharedDefaults boolForKey:@"StartUpToDo"]) {
        self.calendarSwitch.on = NO;
        self.listSwitch.on = NO;
        self.todoSwitch.on = YES;
        self.projectsSwitch.on = NO;
        self.notesSwitch.on = NO;
    }
    else if ([sharedDefaults boolForKey:@"StartUpProjects"]) {
        self.calendarSwitch.on = NO;
        self.listSwitch.on = NO;
        self.todoSwitch.on = NO;
        self.projectsSwitch.on = YES;
        self.notesSwitch.on = NO;
    }
    else if ([sharedDefaults boolForKey:@"StartUpNotes"]) {
        self.calendarSwitch.on = NO;
        self.listSwitch.on = NO;
        self.todoSwitch.on = NO;
        self.projectsSwitch.on = NO;
        self.notesSwitch.on = YES;
    }
    
    
    

}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (IS_IOS_7) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.view.backgroundColor = [UIColor whiteColor];
        //pooja-iPad
        if(IS_IPAD)
        {
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
            self.navigationController.navigationBar.translucent = YES;
            
        }
        else
        {
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        }
    }
self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}



- (IBAction)calendarSwitch_ValueChanged:(UISwitch *)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:YES forKey:@"StartUpCalendar"];
    [sharedDefaults setBool:NO forKey:@"StartUpList"];
    [sharedDefaults setBool:NO forKey:@"StartUpToDo"];
    [sharedDefaults setBool:NO forKey:@"StartUpProjects"];
    [sharedDefaults setBool:NO forKey:@"StartUpNotes"];
    
    
    [self.calendarSwitch setOn:YES animated:YES];
    [self.listSwitch setOn:NO animated:YES];
    [self.todoSwitch setOn:NO animated:YES];
    [self.projectsSwitch setOn:NO animated:YES];
    [self.notesSwitch setOn:NO animated:YES];

}


- (IBAction)listSwitch_ValueChanged:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"StartUpCalendar"];
    [sharedDefaults setBool:YES forKey:@"StartUpList"];
    [sharedDefaults setBool:NO forKey:@"StartUpToDo"];
    [sharedDefaults setBool:NO forKey:@"StartUpProjects"];
    [sharedDefaults setBool:NO forKey:@"StartUpNotes"];
    
      
    [self.calendarSwitch setOn:NO animated:YES];
    [self.listSwitch setOn:YES animated:YES];
    [self.todoSwitch setOn:NO animated:YES];
    [self.projectsSwitch setOn:NO animated:YES];
    [self.notesSwitch setOn:NO animated:YES];

}


- (IBAction)todoSwitch_ValueChanged:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"StartUpCalendar"];
    [sharedDefaults setBool:NO forKey:@"StartUpList"];
    [sharedDefaults setBool:YES forKey:@"StartUpToDo"];
    [sharedDefaults setBool:NO forKey:@"StartUpProjects"];
    [sharedDefaults setBool:NO forKey:@"StartUpNotes"];
    
    
    [self.calendarSwitch setOn:NO animated:YES];
    [self.listSwitch setOn:NO animated:YES];
    [self.todoSwitch setOn:YES animated:YES];
    [self.projectsSwitch setOn:NO animated:YES];
    [self.notesSwitch setOn:NO animated:YES];

}


- (IBAction)projectsSwitch_ValueChanged:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"StartUpCalendar"];
    [sharedDefaults setBool:NO forKey:@"StartUpList"];
    [sharedDefaults setBool:NO forKey:@"StartUpToDo"];
    [sharedDefaults setBool:YES forKey:@"StartUpProjects"];
    [sharedDefaults setBool:NO forKey:@"StartUpNotes"];
    
    
    [self.calendarSwitch setOn:NO animated:YES];
    [self.listSwitch setOn:NO animated:YES];
    [self.todoSwitch setOn:NO animated:YES];
    [self.projectsSwitch setOn:YES animated:YES];
    [self.notesSwitch setOn:NO animated:YES];

}


- (IBAction)notesSwitch_ValueChanged:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"StartUpCalendar"];
    [sharedDefaults setBool:NO forKey:@"StartUpList"];
    [sharedDefaults setBool:NO forKey:@"StartUpToDo"];
    [sharedDefaults setBool:NO forKey:@"StartUpProjects"];
    [sharedDefaults setBool:YES forKey:@"StartUpNotes"];
    
    
    [self.calendarSwitch setOn:NO animated:YES];
    [self.listSwitch setOn:NO animated:YES];
    [self.todoSwitch setOn:NO animated:YES];
    [self.projectsSwitch setOn:NO animated:YES];
    [self.notesSwitch setOn:YES animated:YES];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setCalendarSwitch:nil];
    [self setListSwitch:nil];
    [self setTodoSwitch:nil];
    [self setProjectsSwitch:nil];
    [self setNotesSwitch:nil];
    [super viewDidUnload];
}


@end




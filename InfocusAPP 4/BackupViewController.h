//
//  BackupViewController.h
//  Organizer
//
//  Created by alok gupta on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackupViewController : UIViewController{
    
    IBOutlet UINavigationBar *navBar;
    IBOutlet UITableView *tblBackup;
    NSMutableArray *filePathsArray;
    sqlite3 *database;
    NSString *pathString;
    
}

@property(nonatomic, strong) IBOutlet UINavigationBar *navBar; //Steve

-(IBAction)BackButtonClicked:(id)sender;
-(IBAction)CreateBackupClicked:(id)sender;
-(void)RestoreBackup:(NSString *) filename;
- (void)initializeDatabase;
-(void)FillTableRecords;

@end

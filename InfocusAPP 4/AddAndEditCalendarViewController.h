//
//  AddAndEditCalendarViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/3/13.
//
//

#import <UIKit/UIKit.h>
#import "CustomCalendarColorViewController.h"


@interface AddAndEditCalendarViewController : UITableViewController<CustomCalendarColorDelegate, UIActionSheetDelegate>


@property (strong, nonatomic) CalendarDataObject        *calendarObject;
@property (assign, nonatomic) BOOL                      *isEditing;


-(void) updateCalendarObject:(CalendarDataObject*)calObject;

@end

//
//  SketchNoteViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "SketchNoteViewController.h"
#import "GlobalUtility.h"
#import "AppointmentViewController .h"
#import "ColorSelectorViewController.h"
#import "OrganizerAppDelegate.h"
#import "UIImage+Resize.h"
#import "AddNewNotesViewCantroller.h"
//#import "SketchScrollView.h"


@interface SketchNoteViewController(){
    UIPanGestureRecognizer *panOneFinger;
    UIPanGestureRecognizer *panTwoFinger;
}

@property (strong, nonatomic) IBOutlet UIButton *drawButton;
@property (strong, nonatomic) IBOutlet UIButton *eraserButton;
@property (strong, nonatomic) IBOutlet UIButton *colorButton;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;



@end


@implementation SketchNoteViewController

@synthesize parentVC,delegate,whatVAL;




#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal {
    
    if ((self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        
		self.parentVC =  parentVCLocal;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Steve added
    if (IS_IPHONE_5) {
        helpImageiPhone5.hidden = NO;
        helpImageiPhoneRegular.hidden = YES;
    }
    else{
        helpImageiPhone5.hidden = YES;
        helpImageiPhoneRegular.hidden = NO;
    }
    
    //Steve added
    /*
     for (UIGestureRecognizer *gestureRecognizer in scrollView.gestureRecognizers) {     
     if ([gestureRecognizer  isKindOfClass:[UIPanGestureRecognizer class]]) {
     UIPanGestureRecognizer *panGR = (UIPanGestureRecognizer *) gestureRecognizer;
     panGR.minimumNumberOfTouches = 2;               
     }
     }
     */
    
    /*
     for (UIGestureRecognizer *gestureRecognizer in scrollView.gestureRecognizers)
     {
     if([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])
     {
     [(UIPanGestureRecognizer*)gestureRecognizer setMinimumNumberOfTouches:2];
     }
     }
     */
    
    
    //Steve added
    // The account scrolling area
    // define the area that is initially visable
    //    scrollView.frame = CGRectMake(0, 44, 320, 400);
    // then define how much they can scroll it
    //    [scrollView setContentSize:CGSizeMake(749, 1280)];
    
    // UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
    panTwoFinger.minimumNumberOfTouches = 2; 
    panTwoFinger.maximumNumberOfTouches = 2; 
    panOneFinger.minimumNumberOfTouches = 1; 
    panOneFinger.maximumNumberOfTouches = 1; 
    
    // NSLog(@"scrollView.gestureRecognizers = @%@", scrollView.gestureRecognizers);
    //  [[scrollView panGestureRecognizer] requireGestureRecognizerToFail:panScrollView];
    
    
    
}


-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
    
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        self.title =@"Sketch";
        
        UIButton *drawButton_iOS7;
        UIButton *eraseButton_iOS7;
        UIButton *colorButton_iOS7;
        UIButton *clearButton_iOS7;
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
            if (IS_IPAD) {
                
                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
                
                 drawButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                drawButton_iOS7.frame = CGRectMake(35, 4, 50, 37);
                [drawButton_iOS7 setImage:[UIImage imageNamed:@"PenIcon.png"] forState:UIControlStateNormal];
                [drawButton_iOS7 addTarget:self action:@selector(drawButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:drawButton_iOS7];
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _eraserButton.hidden = YES;
                 eraseButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                eraseButton_iOS7.frame = CGRectMake(150, 4, 50, 37);
                [eraseButton_iOS7 setImage:[UIImage imageNamed:@"EraserIcon.png"] forState:UIControlStateNormal];
                [eraseButton_iOS7 addTarget:self action:@selector(eraseButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:eraseButton_iOS7];
                
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _colorButton.hidden = YES;
                 colorButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                colorButton_iOS7.frame = CGRectMake(240, 4, 50, 37);
                [colorButton_iOS7 setImage:[UIImage imageNamed:@"Coloricon.png"] forState:UIControlStateNormal];
                [colorButton_iOS7 addTarget:self action:@selector(colorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:colorButton_iOS7];
                
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _clearButton.hidden = YES;
                 clearButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                clearButton_iOS7.frame = CGRectMake(344, 4, 50, 37);
                [clearButton_iOS7 setImage:[UIImage imageNamed:@"ClearIcon"] forState:UIControlStateNormal];
                [clearButton_iOS7 addTarget:self action:@selector(clearButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:clearButton_iOS7];
                

                
                
            }else
            {
                UIButton *drawButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                drawButton_iOS7.frame = CGRectMake(27, 4, 50, 37);
                [drawButton_iOS7 setImage:[UIImage imageNamed:@"PenIcon.png"] forState:UIControlStateNormal];
                [drawButton_iOS7 addTarget:self action:@selector(drawButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:drawButton_iOS7];
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _eraserButton.hidden = YES;
                UIButton *eraseButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                eraseButton_iOS7.frame = CGRectMake(100, 4, 50, 37);
                [eraseButton_iOS7 setImage:[UIImage imageNamed:@"EraserIcon.png"] forState:UIControlStateNormal];
                [eraseButton_iOS7 addTarget:self action:@selector(eraseButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:eraseButton_iOS7];
                
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _colorButton.hidden = YES;
                UIButton *colorButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                colorButton_iOS7.frame = CGRectMake(172, 4, 50, 37);
                [colorButton_iOS7 setImage:[UIImage imageNamed:@"Coloricon.png"] forState:UIControlStateNormal];
                [colorButton_iOS7 addTarget:self action:@selector(colorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:colorButton_iOS7];
                
                
                //EraseButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
                _clearButton.hidden = YES;
                UIButton *clearButton_iOS7 = [UIButton buttonWithType:UIButtonTypeSystem];
                clearButton_iOS7.frame = CGRectMake(243, 4, 50, 37);
                [clearButton_iOS7 setImage:[UIImage imageNamed:@"ClearIcon"] forState:UIControlStateNormal];
                [clearButton_iOS7 addTarget:self action:@selector(clearButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                [bottomBar addSubview:clearButton_iOS7];
                

            }
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            self.view.backgroundColor = [UIColor blackColor];
            
            bottomBar.hidden = NO;
            bottomBar.translucent = NO;
            [self.view bringSubviewToFront:bottomBar];
            
            //done Button
            saveButton.hidden = YES;
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(doneButton_Clicked:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            //DrawButton - Setup a new button for iOS 7, since custom buttons don't respond to tintColor
            _drawButton.hidden = YES;
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor whiteColor];
                
                bottomBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                drawButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                eraseButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                colorButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                clearButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                doneButton_iOS7.tintColor = [UIColor whiteColor];
                
                bottomBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor whiteColor];
                
                drawButton_iOS7.tintColor = [UIColor whiteColor];
                eraseButton_iOS7.tintColor = [UIColor whiteColor];
                colorButton_iOS7.tintColor = [UIColor whiteColor];
                clearButton_iOS7.tintColor = [UIColor whiteColor];
            }
            
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
             self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            
            
            if (IS_IPHONE_5){
                
                drawImage.frame = CGRectMake(0, 44 - 44, 320, 481);
                bottomBar.frame = CGRectMake(0, 392 + 88, 320, 44);
                
            }
            else{
                
                drawImage.frame = CGRectMake(0, 44 - 44, 320, 481 - 88);
                bottomBar.frame = CGRectMake(0, 392, 320, 44);
            }

            
            
        }
        else{ //iOS 6
            
            saveButton.hidden = NO;
            
            if (IS_IPHONE_5){
                
                drawImage.frame = CGRectMake(0, 44 - 44, 320, 481);
                bottomBar.frame = CGRectMake(0, 372 + 88, 320, 44);
            }
            else{
                
                drawImage.frame = CGRectMake(0, 44 - 44, 320, 393);
                bottomBar.frame = CGRectMake(0, 372, 320, 44);
            }
            
            
            [self.navigationController.navigationBar addSubview:saveButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            if (backButton) {
                backButton = nil;
            }
            
            //Back Button
            UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(backButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            backButton = backButtonNewNav;
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
            
            [[self navigationItem] setLeftBarButtonItem:barButton];
            
            
            [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];

        }

    }
    else{ // Old Navigation
 
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        if (IS_IPAD) {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        }
        self.navigationController.navigationBarHidden = YES;
        
        saveButton.hidden = NO;
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

        
        if (IS_IPHONE_5)
            drawImage.frame = CGRectMake(0, 44, 320, 481);
        else
            drawImage.frame = CGRectMake(0, 44, 320, 393);
        
        
        [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    }
    
    
    // ***************** Steve End ***************************
    
    
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_NotesSketch"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];

        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            
            if (IS_IOS_7) {
                
                if (IS_IPHONE_5)
                    helpImageiPhone5.frame = CGRectMake(-3, -64 + 20, 326, 574);
                else
                    helpImageiPhoneRegular.frame = CGRectMake(-3, -64 + 20, 326, 486);
            }
            else{ //iOS 6
                
                if (IS_IPHONE_5)
                    helpImageiPhone5.frame = CGRectMake(-3, -64, 326, 574);
                else
                    helpImageiPhoneRegular.frame = CGRectMake(-3, -64, 326, 486);
            }
            

        }
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesSketch"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
    }

}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesBegan SketchViewController Class");
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:YES];
	
	if (previousColor) {
		previousColor = nil;
	}
	previousColor  = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
	[drawImage setBrushColorWithRed:0 green:0 blue:0];

    
    //Fixes a bug with iOS 8.  Sketch would turn black on first touch
    [self clearButton_Clicked:nil];
    
    
    //////////////// Steve added ///////////////////////////
    //If first time running App, show some "Help" Views
    
    /*
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_NotesSketch"]) 
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];  
        helpView.hidden = NO; 
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesSketch"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];     
    } 
     */
     //////////////// Steve added End ///////////////////////////
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    NSLog(@"gestureRecognizerShouldBegin");
    
    return YES;
}


#pragma mark -
#pragma mark scroll
#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScroll");
    /*
     if (panOneFinger.numberOfTouches == 1) {
     NSLog(@"pan.numberOfTouches == 1)");
     return;
     }
     */
}



#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -

-(IBAction)doneButton_Clicked:(id) sender
{
    
    UIImage *imgView = [drawImage getImage];
    
    NSData *imgData = UIImageJPEGRepresentation(imgView, 1.0);
    
    [[self.delegate tempSktArray] addObject:imgData];	
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)backButton_Clicked:(id) sender {
	[self.navigationController popViewControllerAnimated:YES];
} 

-(IBAction)drawButton_Clicked:(id) sender {
	const CGFloat* colors = CGColorGetComponents(previousColor.CGColor);
	[drawImage setBrushColorWithRed:colors[0] green:colors[1] blue:colors[2]];
}

-(IBAction)eraseButton_Clicked:(id) sender {
	[drawImage setBrushColorWithRed:1.0 green:1.0 blue:1.0];
}

-(IBAction)colorButton_Clicked:(id) sender {
    
    //NSLog(@"colorButton_Clicked");
	
	[drawImage setUserInteractionEnabled:NO];
    
    
    if (IS_IPHONE_5)
        colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    else
        colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        self.title = @"Color";
        
        if (IS_IOS_7) {
            
            saveButton.hidden = YES;
            self.navigationItem.rightBarButtonItem = nil;

            
            
            //Adds a Save button
            UIButton *saveButtonNewNav = [UIButton buttonWithType:UIButtonTypeSystem];
            saveButtonNewNav.frame = CGRectMake(0,5,40,29);
            [saveButtonNewNav setTitle:@"Save" forState:UIControlStateNormal];
            [saveButtonNewNav addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
            
            
            UIBarButtonItem *barSaveButton = [[UIBarButtonItem alloc] initWithCustomView:saveButtonNewNav];
            //barSaveButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            
            self.navigationItem.rightBarButtonItem = barSaveButton;
            
        
            //Adds a back button
            UIButton *backButtonNewNav = [UIButton buttonWithType:UIButtonTypeSystem];
            backButtonNewNav.frame  = CGRectMake(0,5,51,29);
            [backButtonNewNav setTitle:@"Back" forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            
            self.navigationItem.leftBarButtonItem = barButton;
            
            
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                saveButtonNewNav.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                barButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                [colorPanelView setBackgroundColor:[UIColor whiteColor]];
                [colorPanelView setAlpha:1.0];
                [colorPanelView setTag:1029];
            }
            else{ //Dark Theme
                
                saveButtonNewNav.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                barButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:0.0f/255.0f alpha:1.0f]};
                
                [colorPanelView setBackgroundColor:[UIColor colorWithRed:244.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0f]];
                [colorPanelView setAlpha:1.0];
                [colorPanelView setTag:1029];
            }
            
        }
        else{ //IOS 6
            
            saveButton.hidden = YES;
            
            //Adds a back button
            UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            
            [[self navigationItem] setLeftBarButtonItem:barButton];
        }
        
        
        
    }


    
	int x = 15, y = 15;
    
	
	NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"black1.png", @"charcoal1.png", @"darkblue1.png", @"blue1.png",@"purple1.png", @"pink1.png", @"darkgreen1.png", @"green1.png", @"olive1.png", @"red1.png",
							 @"darkred1.png", @"teal1.png", @"blueviolet1.png", @"saddlebrown1.png", @"indigo1.png",
							 nil];
	
    //  NSLog(@"imgNameArray count = %d", [imgNameArray count] );
    
	for(int i = 0; i < [imgNameArray count]; i++) 
    {
        
        // NSLog(@"i = %d", i );
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setFrame:CGRectMake(x, y, 50, 50)];
		[btn setBackgroundImage:[UIImage imageNamed:[imgNameArray objectAtIndex:i]] forState:UIControlStateNormal];
		[btn setTag:i];
		[btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
		[colorPanelView addSubview:btn];  
		
		x = x + btn.frame.size.width + 9;
		
        //Steve changed to accomodate vertical screen
		if (x > 290 && i < 9) //425
        {
			x = 15; 
			y = 25 + btn.frame.size.height;
		}
        
        //Steve changed to accomodate vertical screen
        if (i == 9) {
            
            //   NSLog(@"if (x > 290 && y == 75.0)");
            
            x = 15; 
			y = 90 + btn.frame.size.height;
        }
        
        
        
        //  NSLog(@"x = %d  y = %d", x, y );
        
	} //End for
	
	if (aView) {
		aView = nil;
	}
	aView = [[UIView alloc] initWithFrame:CGRectMake(200, y + 75, 80, 80)];
	[aView setBackgroundColor:[UIColor blackColor]];
	
	redSlider = [[UISlider alloc] initWithFrame:CGRectMake(15, y + 80, 150, 23)];
	[redSlider setMaximumValue:1.0]; 
	[redSlider setMinimumValue:0.0];
	[redSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorPanelView addSubview:redSlider];
	
	greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 105, 150, 23)];
	[greenSlider setMaximumValue:1.0]; 
	[greenSlider setMinimumValue:0.0];
	[greenSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorPanelView addSubview:greenSlider];
	
	blueSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 130, 150, 23)];
	[blueSlider setMaximumValue:1.0]; 
	[blueSlider setMinimumValue:0.0];
	[blueSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorPanelView addSubview:blueSlider];
	
	[colorPanelView addSubview:aView];
    
    if (IS_IOS_7) {
        
        aView.frame = CGRectMake(200, y + 90, 80, 80);
        
        redSlider.frame = CGRectMake(15, y + 80, 150, 23);
        greenSlider.frame = CGRectMake(15, y + 105 + 10, 150, 23);
        blueSlider.frame = CGRectMake(15, y + 130 + 20, 150, 23);
    }
	
	//UIButton *okButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	//[okButton setTitle:@"Save Color" forState:UIControlStateNormal];
    //[okButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    //okButton.titleLabel.textColor = [UIColor blueColor];
    
    
    if (!IS_IOS_7) {
        
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setBackgroundImage:[UIImage imageNamed:@"SaveBtn.png"] forState:UIControlStateNormal];
        [okButton setFrame:CGRectMake(320/2 - 53/2, y + 130 + 75, 53, 32)];
        [okButton addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
        [colorPanelView addSubview:okButton];
        
    }

    
    [self.view addSubview:colorPanelView];
	
	[UIView beginAnimations:nil context:nil];
    colorPanelView.alpha = 0.0; //Steve added
	[UIView setAnimationDuration:0.75];
	[colorPanelView setAlpha:1.0];
	[UIView commitAnimations];
	
} 

-(IBAction)clearButton_Clicked:(id) sender {
	[drawImage erase];
	[drawImage setFrame:CGRectMake(0, 0, drawImage.frame.size.width, drawImage.frame.size.height)];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.3];
	[UIView commitAnimations];
} 

- (IBAction)helpView_Tapped:(id)sender {
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}



#pragma mark -
#pragma mark Class Methods
#pragma mark -

- (UIColor *)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy {
	// First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,bitsPerComponent, bytesPerRow, colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
	
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
	
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
	byteIndex += 4;
	
	if (previousColor) {
		previousColor = nil;
	}
	
	previousColor  = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
	
	[drawImage setBrushColorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0];
	
	[redSlider setValue:(rawData[byteIndex] * 1.0) / 255.0 animated:YES];
	[greenSlider setValue:(rawData[byteIndex + 1] * 1.0) / 255.0 animated:YES];
	[blueSlider setValue:(rawData[byteIndex + 2] * 1.0) / 255.0 animated:YES];
	
	UIColor *acolor = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
	
	[aView setBackgroundColor:acolor];
    
	free(rawData);
    return acolor;
}

-(void)colorChangeSaved: (id) sender {
    
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
    colorPanelView.alpha = 1.0;
	//[[self.view viewWithTag:1029] setAlpha:1]; //Steve commented
    colorPanelView.alpha = 0.0;
	[UIView commitAnimations];
    
    //[[self.view viewWithTag:1029] removeFromSuperview]; //Steve commented.  Doesn't allow animation
    
	[drawImage setUserInteractionEnabled:YES];
    saveButton.hidden = NO;
    backButton.hidden = YES;
    self.navigationItem.leftBarButtonItem = nil; //Steve
    [backButton removeFromSuperview];
    
    [self viewWillAppear:YES];
}

-(void)valueChanged:(id)sender{
	if (previousColor) {
		previousColor = nil;
	}
    
	previousColor = [UIColor colorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value alpha:1.0];
	[drawImage setBrushColorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value];
	[aView setBackgroundColor: previousColor];
}

-(void)btnClicked:(id)sender {
	UIImage *image = [sender backgroundImageForState:UIControlStateNormal];
	[self getRGBAsFromImage:image atX:10 andY:10];
}



#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL) animated {
	[super viewWillDisappear:YES];
	
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [saveButton removeFromSuperview];
        [backButton removeFromSuperview];
        
    }
    
}

- (void)viewDidUnload {
    
    navBar = nil;
    bottomBar = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    helpImageiPhoneRegular = nil;
    helpImageiPhone5 = nil;
    backButton = nil;
    saveButton = nil;
    colorPanelView = nil;
    [super viewDidUnload];
}

- (void)dealloc {
	
	if (redSlider) {
		redSlider = nil;
	}
	
	if (greenSlider) {
		greenSlider = nil;
	}
	if (blueSlider) {
		blueSlider = nil;
	}
	if (aView) {
		aView = nil;
	}
	
}

@end

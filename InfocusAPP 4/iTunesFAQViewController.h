//
//  iTunesFAQViewController.h
//  Organizer
//
//  Created by Steven Abrams on 7/28/12.
//
//

#import <UIKit/UIKit.h>

@interface iTunesFAQViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

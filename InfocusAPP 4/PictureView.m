//
//  PictureView.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 1/21/15.
//
//

#import "PictureView.h"


#define kResizeThumbSize 15


@interface PictureView()

@property(nonatomic, strong) NSMutableArray *holdingArray;

@end




@implementation PictureView


- (void)setContentView:(UIView *)newContentView {
    
    [_contentView removeFromSuperview];
    
    _contentView = newContentView;
    _contentView.frame = self.bounds;

    [self addSubview:_contentView];
    
    //[_contentImageView removeFromSuperview];
    //[self addSubview:_contentImageView];
}

- (void)setFrame:(CGRect)newFrame {
    
    [super setFrame:newFrame];
    _contentView.frame = self.bounds;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    touchStart = [[touches anyObject] locationInView:self];
    
    isResizing = (self.bounds.size.width - touchStart.x < kResizeThumbSize &&
                  self.bounds.size.height - touchStart.y < kResizeThumbSize);
    
    if (isResizing) {
        touchStart = CGPointMake(touchStart.x - self.bounds.size.width,
                                 touchStart.y - self.bounds.size.height);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    
    if (isResizing) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                                touchPoint.x - touchStart.x, touchPoint.y - touchStart.y);
    }
    else {
        self.center = CGPointMake(self.center.x + touchPoint.x - touchStart.x,
                                  self.center.y + touchPoint.y - touchStart.y);
    }
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    //UIView *hitView = [super hitTest:point withEvent:event];
    UIView *hitView = [super hitTest:point withEvent:event];
    //hitView.layer.borderColor = [UIColor blueColor].CGColor;
    //hitView.layer.borderWidth = 4.0f;
    
    NSSet *sTouches = [event touchesForView:self];
    
    NSLog(@"Touches %d", [sTouches count] );
    
    return self;
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */



@end

//
//  AddRepeatViewController.m
//  Organizer
//
//  Created by Priyanka Taneja on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddRepeatViewController.h"
#import "AddCustomRepeatViewController.h"


@implementation AddRepeatViewController
@synthesize delegate, repeatText, repTblView, nav1, repeatArray, btn;



-(id)initWithRepeatText:(NSString *) repeat {
    
    if(IS_IPAD)
    {
        if (self = [super initWithNibName:@"AddRepeatViewController_iPad" bundle:[NSBundle mainBundle]])
        {
            self.repeatText = repeat;
        }
    }
    else
    {
        if (self = [super initWithNibName:@"AddRepeatViewController" bundle:[NSBundle mainBundle]])
        {
            self.repeatText = repeat;//[repeat isEqualToString:repeat] ? repeat :@"Never";
            
        }
    }
    //NSLog(@"Repert->%@",repeatText);
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        cancelButton.hidden = YES;
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        if (IS_IOS_7) {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            
            self.view.backgroundColor = [UIColor whiteColor];
            repTblView.backgroundColor = [UIColor whiteColor];
            
            
            //done Button
            doneButton.hidden = YES;
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(doneClicked:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            
            self.title =@"Repeat";
            
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
            }
            else{ //Dark Theme
                
                
                if(IS_IPAD)
                {
                    //Steve
                    [[UIApplication sharedApplication] setStatusBarHidden:YES];
                    
                    _navBar_iPad.hidden=NO;
                    
                    
                    
                    _navBar_iPad.translucent = NO;
                    [_navBar_iPad setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    _navBar_iPad.tintColor = [UIColor whiteColor];
                    _navBar_iPad.barTintColor = [UIColor darkGrayColor];
                    _navBar_iPad.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
                    
                    _doneButton_iPad.tintColor = [UIColor whiteColor];
                    
                    
                    cancelButton.hidden=NO;
                    doneButton.hidden=NO;
                    
                    doneButton.tintColor=[UIColor whiteColor];
                    cancelButton.tintColor=[UIColor whiteColor];
                    
                }
                else
                {
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:0.7];
                    self.navigationController.navigationBar.translucent = NO;
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            
            if(IS_IPAD)
            {
                //repTblView.frame = CGRectMake(7, 49 - 44, 754, 984); //Steve commented
            }
            else
            {
                if (IS_IPHONE_5) {
                    repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
                    
                }
                else {
                    repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
                    
                }
            }
            
        }
        else{//iOS 6
            
            if (IS_IPHONE_5) {
                repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
                
            }
            else {
                repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
                
            }
            
            self.title =@"Repeat";
            
            [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            [repTblView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
            
            [repTblView.layer setCornerRadius:5.0];
            repTblView.layer.masksToBounds = YES;
            repTblView.layer.borderColor = [UIColor lightGrayColor].CGColor;  //Steve [UIColor purpleColor].CGColor;
            repTblView.layer.borderWidth = 0.5;
            
        }
        
    }
    else{ //old Navigation
        
        if (!IS_IPHONE_5) { //Not iPhone5
            //shareButton.frame = CGRectMake(105, 416, 111, 37);
        }
        
        self.navigationController.navigationBarHidden = YES;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [repTblView.layer setCornerRadius:5.0];
        repTblView.layer.masksToBounds = YES;
        repTblView.layer.borderColor = [UIColor lightGrayColor].CGColor;  //Steve [UIColor purpleColor].CGColor;
        repTblView.layer.borderWidth = 0.5;
        
    }
    
    
    // ***************** Steve End ***************************
    
    
    tickImageView = [[UIImageView alloc] init];
    
    repeatArray = [[NSMutableArray alloc] init];
    [repeatArray addObject:@"Never"];
    [repeatArray addObject:@"Every Day"];
    [repeatArray addObject:@"Every Week"];
    [repeatArray addObject:@"Every 2 Week"];
    [repeatArray addObject:@"Every Month"];
    [repeatArray addObject:@"Every Year"];
    
}


#pragma mark -
#pragma mark TableViewMethods
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [repeatArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *cellValue = [repeatArray objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    
    if ( [repeatText isEqualToString:cellValue] ) {
        
        if (tickImageView) {
            [tickImageView removeFromSuperview];
        }
        
        UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
        [tickImageView setImage:chekMarkImage];
        
        if(IS_IPAD)                           //Steve changed from 680 to 715
            [tickImageView setFrame:CGRectMake(715, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
        else
            [tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
        [cell.contentView addSubview:tickImageView];
        
    }
    
    
    
    if (IS_IOS_7) {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    repeatText = @"";
    
    if ([indexPath row] == 6 && [[repeatArray objectAtIndex:6] isEqualToString:@"Custom..."]) {
        [repeatArray removeObjectAtIndex:6];
        [repeatArray addObject:@"Every Sunday"];
        [repeatArray addObject:@"Every Monday"];
        [repeatArray addObject:@"Every Tuesday"];
        [repeatArray addObject:@"Every Wednesday"];
        [repeatArray addObject:@"Every Thursday"];
        [repeatArray addObject:@"Every Friday"];
        [repeatArray addObject:@"Every Saturday"];
        [tableView reloadData];
        return;
    }
    
    UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [tickImageView removeFromSuperview];
    
    UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
    tickImageView.image = chekMarkImage;
    
    if(IS_IPAD)
        [tickImageView setFrame:CGRectMake(715, 10, 27, 25)];
    else
        [tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
    
    [Cell.contentView addSubview:tickImageView];
    
    repeatText = [repeatArray objectAtIndex:[indexPath row]];
}

-(IBAction) Back_buttonClicked:(id)sender
{
    
    //Steve
    if (IS_IPAD) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(IBAction)doneClicked:(id)sender
{
    if ([repeatText isEqualToString:@""] || repeatText == nil || [repeatText isEqualToString:@"None"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select the repeat" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert  show];
        return;
    }
    
    [[self delegate] setRepeatString:repeatText];
    
    //Steve
    if (IS_IPAD) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(IBAction)AddCustomRepeat:(id)sender
{
    AddCustomRepeatViewController *addcustomrepeat = [[AddCustomRepeatViewController alloc]init];
    [self.navigationController pushViewController:addcustomrepeat animated:YES];
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [cancelButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    cancelButton = nil;
    doneButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end










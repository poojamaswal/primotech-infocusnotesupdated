//
//  NotesFullViewController_iPad.m
//  Organizer
//
//  Created by Steven Abrams on 11/29/14.
//
//

#import "NotesFullViewController_iPad.h"
#import "NotesViewCantroller.h"
#import "TakePictureTableViewController.h"
#import "UIImage+Resize.h"
#import "UIImage+UIImage_Extensions.h"
#import "PictureView.h"
#import "SPUserResizableView.h"



@interface NotesFullViewController_iPad () <UIScrollViewDelegate, UIGestureRecognizerDelegate, DrawingViewDelegate, SPUserResizableViewDelegate, UITextViewDelegate>{
    
    float kMinimumZoomScale;
    float kMaximumZoomScale;
    
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
}


@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewDrawContent;
@property (strong, nonatomic) UIPinchGestureRecognizer *pinchTwoFingers;

//Shows the Zoom In/Out size.  ie 150%
@property(nonatomic, strong) UIView *zoomScaleAmountView;
@property(nonatomic, strong) UIView *zoomScaleAmountView2;
@property(nonatomic, strong) UILabel *zoomScaleAmountLabel;
@property(nonatomic, strong) UILabel *zoomScaleAmountLabel2;


@property (nonatomic, strong) UIPopoverController *masterPopoverController;
@property (nonatomic, strong) UIPopoverController *barButtonItemPopover;
@property (nonatomic, strong) UIView *drawImageView;

@property (strong, nonatomic) IBOutlet UIView *zoomView;
@property (strong, nonatomic) IBOutlet UIToolbar *zoomViewToolBar;
@property (strong, nonatomic) UIView *zoomCoverDrawImageView;

@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) PenColorViewController *colorPicker;
@property (nonatomic, strong) UIPopoverController *colorPickerPopover;

@property (nonatomic, strong) UIPopoverController *takePicturePopover;
@property (nonatomic, strong) TakePictureTableViewController *picturePicker;

@property (strong, nonatomic) IBOutlet UIView *drawHolderView; //Holds drawView & drawTextView
@property (strong, nonatomic) UIBarButtonItem *addImageBarButton;

@property (strong, nonatomic) UIImageView *pictureImageView;

@property (nonatomic, strong) NSMutableArray *pictureArray;
@property (nonatomic, assign) NSUInteger selectedPictureIndex;
@property (nonatomic, readonly) PictureView *selectedPictureView;

@property (nonatomic, strong) UIGestureRecognizer *currentGestureRecognizer;
@property (nonatomic, strong) UITouch *currentTouch;

@property (nonatomic, strong) NSMutableArray *imageArray; //Keeps track of images

-(void) zoomButtonPressed;
-(void) zoomViewInitilize;
-(void) hideZoomCoverViewWithDelay;
- (void)centerScrollViewContents; //center drawing
- (void)centerScrollViewContentsText; //center Text
-(void) penButton_Clicked:(id)sender;
-(void) highligherButton_Clicked:(id)sender;
-(void) eraserButton_Clicked;
-(void) textButton_Clicked:(id)sender;
-(void) pictureButton_Clicked:(id) sender;

- (void)handleTapGesture;


@end




@implementation NotesFullViewController_iPad


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.autoresizesSubviews = YES;
    _zoomView.hidden = YES;
    
    _selectedPictureIndex = NSNotFound;
    _pictureArray = [NSMutableArray array];
    _imageArray = [[NSMutableArray alloc]init];
    
    
    // create Nav bar items
    UIBarButtonItem *newBarButton = [[UIBarButtonItem alloc]initWithTitle:@"New" style:UIBarButtonItemStylePlain target:self action:@selector(zoomButtonPressed)]; //New Note
    
    UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(save_buttonClicked:)];
    
    UIBarButtonItem *textBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Text" style:UIBarButtonItemStylePlain target:self action:@selector(textButton_Clicked:)]; //Text
    UIBarButtonItem *penBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Pen" style:UIBarButtonItemStylePlain target:self action:@selector(penButton_Clicked:)]; //Pen
    UIBarButtonItem *HighlighterBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Hi" style:UIBarButtonItemStylePlain target:self action:@selector(highligherButton_Clicked:)];//Highligher
    UIBarButtonItem *EraserBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Er" style:UIBarButtonItemStylePlain target:self action:@selector(eraserButton_Clicked)];//Eraser
    UIBarButtonItem *panBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Pan" style:UIBarButtonItemStylePlain target:self action:@selector(zoomButtonPressed)];//Pan page
    UIBarButtonItem *zoomBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Zoom" style:UIBarButtonItemStylePlain target:self action:@selector(zoomButtonPressed)];//Zoom
   // UIBarButtonItem *imageBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Im" style:UIBarButtonItemStylePlain target:self action:@selector(zoomButtonPressed)];//Add Image
    _addImageBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(pictureButton_Clicked:)];
  
    
    // create a spacer
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
    space.width = 25;
    
    NSArray *buttons = @[newBarButton, space,space,space,space,space,space,space,space,space,_addImageBarButton, zoomBarButton, panBarButton, EraserBarButton, HighlighterBarButton, penBarButton, textBarButton];
    
    self.navigationItem.rightBarButtonItems = buttons;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
    
    
    // Setup ScrollView so user can zoom into drawing
    _scrollViewDrawContent = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 44, 776, 980)];
    //_scrollViewDrawContent.frame = CGRectMake(0, 44, 320, 372); //scrollView initial size
    [_scrollViewDrawContent setContentSize:CGSizeMake(776, 980)]; // 1280, 1488) Content size.  drawImage size setup in Nib.  Won't work proper if setup here
    
    _scrollViewDrawContent.canCancelContentTouches = YES;
    _scrollViewDrawContent.delaysContentTouches = NO;
    
    _scrollViewDrawContent.panGestureRecognizer.minimumNumberOfTouches = 2;//***Important to distinguish between draw and zoom or scroll
    _scrollViewDrawContent.scrollEnabled = YES;
    _scrollViewDrawContent.delegate = self;
    
    _scrollViewDrawContent.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrollViewDrawContent];
    
    
    //Add a view that holds drawView & drawTextView so that they both can scroll
    _drawHolderView = [[UIView alloc] initWithFrame:self.view.bounds];
    _drawHolderView.backgroundColor = [UIColor clearColor];
    [_scrollViewDrawContent addSubview:_drawHolderView];
    
    
    //add TextView
    self.drawTextView = [[RichTextEditor alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.drawTextView.multipleTouchEnabled = YES;
    self.drawTextView.userInteractionEnabled = YES;
    self.drawTextView.backgroundColor = [UIColor clearColor];
    self.drawTextView.delegate = self;
    [_drawHolderView addSubview:self.drawTextView];
    
    
    
    UIFont *font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    UIColor *textColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f];
    
    NSDictionary *attrs = @{NSForegroundColorAttributeName: textColor,
                            NSFontAttributeName:            font,
                            NSTextEffectAttributeName:      NSTextEffectLetterpressStyle};
    
    NSAttributedString *attrString = [[NSAttributedString alloc]initWithString:@" " attributes:attrs];
    //self.drawTextView.attributedText = attrString;
    
    // ****** change to user defaults
    self.drawTextView.font = [UIFont fontWithName:@"HelveticaNeue" size:24]; //iniital value
    self.drawTextView.text = @" \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"; //sets up page so you can click anywhere
    
    //NSLog(@" textCont = %@", self.drawTextView.textContainer);
    
    
    //Handles Images when tapped
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture)];
    [gestureRecognizer setDelegate:self];
    [self.drawTextView addGestureRecognizer:gestureRecognizer];

    
    //Add NoteView to draw on
    self.drawView = [[DrawView_iPad alloc]initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.drawView.multipleTouchEnabled = YES;
    self.drawView.userInteractionEnabled = YES;
    self.drawView.backgroundColor = [UIColor clearColor];
    //[_scrollViewDrawContent addSubview:self.drawView];
    [_drawHolderView addSubview:self.drawView];
    
    
    [self zoomViewInitilize];
    
}


-(void) zoomViewInitilize{
    
    //Initilized Zoom view
    _zoomView = [[UIView alloc]initWithFrame:CGRectMake(0, 722, 768, 302)];
    _zoomView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_zoomView];
    
    //ZoomView Toolbar
    _zoomViewToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 768, 44)];
    _zoomViewToolBar.barTintColor = [UIColor yellowColor];
    [_zoomView addSubview:_zoomViewToolBar];
    
    
    //Cover drawing space.  With zoom open you can only draw in zoom view
    float y = self.navigationController.navigationBar.frame.size.height + 20;
    float zoomCoverHeight = self.view.frame.size.height - y - _zoomView.frame.size.height;
    
    _zoomCoverDrawImageView = [[UIView alloc]initWithFrame:CGRectMake(0, y, self.view.frame.size.width, zoomCoverHeight)];
    _zoomCoverDrawImageView.backgroundColor = [UIColor blackColor];
    _zoomCoverDrawImageView.alpha = 0.25;
    [self.view addSubview:_zoomCoverDrawImageView];
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"PenTool"]) {
        
        //Color
        float red = [sharedDefaults floatForKey:@"PenRedColor"];
        float green = [sharedDefaults floatForKey:@"PenGreenColor"];
        float blue = [sharedDefaults floatForKey:@"PenBlueColor"];
        
        float alpha = [sharedDefaults floatForKey:@"PenAlpha"];
        float penSize = [sharedDefaults floatForKey:@"PenSize"];
        
        self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        self.drawView.lineWidth = penSize;
        self.drawView.lineAlpha = alpha;
    }
    else if ([sharedDefaults boolForKey:@"HighLighterTool"]){
        
        //Color
        float red = [sharedDefaults floatForKey:@"highlighterRedColor"];
        float green = [sharedDefaults floatForKey:@"highlighterGreenColor"];
        float blue = [sharedDefaults floatForKey:@"highlighterBlueColor"];
        
        float penSize = [sharedDefaults floatForKey:@"highlighterSize"];
         //float alpha = [sharedDefaults floatForKey:@"highlighterAlpha"];
        float alpha = 0.50;
        
        self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
        self.drawView.lineWidth = penSize;
        self.drawView.lineAlpha = alpha;
    }
    else if ([sharedDefaults boolForKey:@"TextTool"]){
        
        //Color
        float red = [sharedDefaults floatForKey:@"TextRedColor"];
        float green = [sharedDefaults floatForKey:@"TextGreenColor"];
        float blue = [sharedDefaults floatForKey:@"TextBlueColor"];
        
        float alpha = [sharedDefaults floatForKey:@"TextAlpha"];
        float penSize = [sharedDefaults floatForKey:@"TextSize"];
        
        self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        self.drawView.lineWidth = penSize;
        self.drawView.lineAlpha = alpha;
        
        //NSLog(@"self.drawView.lineColor = %@", self.drawView.lineColor);
        //NSLog(@"self.drawView.lineWidth = %f", self.drawView.lineWidth);
        //NSLog(@"self.drawView.lineAlpha = %f", self.drawView.lineAlpha);

    }
    
    
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    //self.view.backgroundColor = [UIColor whiteColor];//whiteColor
    
    _zoomView.hidden = YES; //hides zoom view until button pressed
    _zoomCoverDrawImageView.hidden = YES;
    
    
    _zoomScaleAmountView = nil;
    _zoomScaleAmountView2 = nil;
    _zoomScaleAmountLabel = nil;
    _zoomScaleAmountLabel2 = nil;
    
    /////////////// Steve Added 8-4-12 Scroll view //////////////////
    // Set up the minimum & maximum zoom scales
    CGRect scrollViewFrame = _scrollViewDrawContent.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / _scrollViewDrawContent.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / _scrollViewDrawContent.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    kMaximumZoomScale = 5.0f;
    kMinimumZoomScale = 1.0f;
    
    _scrollViewDrawContent.minimumZoomScale = kMinimumZoomScale; //minScale;
    _scrollViewDrawContent.maximumZoomScale =kMaximumZoomScale;
    _scrollViewDrawContent.zoomScale = 1; //minScale sets it so whole image is shown when view loaded
    
    
    [self centerScrollViewContents];
    //[self centerScrollViewContentsText];
    
    
    
    //Add Button - ******* TEST *****
    /*
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewnotes_Clicked:)];
    self.navigationItem.rightBarButtonItem = addButton;
    */
    
    
    // Nav Bar items
    /*
    UIBarButtonItem *updateButton = [[UIBarButtonItem alloc] initWithTitle:@"New"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(save_click:)];
    self.navigationItem.rightBarButtonItem = updateButton;
    */
    
    
    //export Button from NIB
    //[self.navigationController.navigationBar addSubview:_exportButton];
    
    
    /*
    //Edit Button made in NIB. Note - floating on page, not on view in NIB
    _editButtoniOS7.frame = CGRectMake(255, 8, 51, 29); //CGRectMake(207, 7, 51, 29);
    [_editButtoniOS7 addTarget:self
                        action:@selector(editNotes:)
              forControlEvents:UIControlEventTouchUpInside];
    
    _editButtoniOS7.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:19];
    [self.navigationController.navigationBar addSubview:_editButtoniOS7];
    */
    
    ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
        
        //_editButtoniOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        //_exportButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        
    }
    else{ //Dark Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
    //********* TEST
        //self.navigationController.navigationBar.barTintColor = [UIColor redColor]; //Steve test
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
        //_editButtoniOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        //_exportButton.tintColor = [UIColor whiteColor];
        
    }
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////

}


#pragma mark - Scroll

- (void)centerScrollViewContents {
    
    CGSize boundsSize = _scrollViewDrawContent.bounds.size;
    //CGRect contentsFrame = self.drawView.frame;
    CGRect contentsFrame = _drawHolderView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    //self.drawView.frame = contentsFrame;
    _drawHolderView.frame = contentsFrame;
}


- (void)centerScrollViewContentsText {
    
    CGSize boundsSize = _scrollViewDrawContent.bounds.size;
    //CGRect contentsFrame = self.drawView.frame;
    CGRect contentsFrame = self.drawTextView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    //self.drawView.frame = contentsFrame;
    self.drawTextView.frame = contentsFrame;
}


- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //NSLog(@"viewForZoomingInScrollView");
    
    //returns  _drawHolderView that holds drawView & drawTextView
    return _drawHolderView;

}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    //NSLog(@"scrollViewWillBeginZooming");
    
    _scrollViewDrawContent.scrollEnabled = YES;
    
    float zoomScaleNumber = scrollView.zoomScale * 100;
    
    //Steve - Adds 1st box and label on screen which shows zoom amount
    //This will adjust to center for the new zoom: Bounds is the relative view. Center: (Width/2 + X - 110/2, Height/2 + Y - 30/2)
    UIView *zoomScaleAmountView = [[UIView alloc]initWithFrame:CGRectMake(_scrollViewDrawContent.bounds.size.width/2 + _scrollViewDrawContent.bounds.origin.x -110/2,
                                                                          _scrollViewDrawContent.bounds.size.height/2 + _scrollViewDrawContent.bounds.origin.y-30/2,
                                                                          110,30)];
    zoomScaleAmountView.backgroundColor = [UIColor blackColor];
    zoomScaleAmountView.alpha = 0.70;
    _zoomScaleAmountView = zoomScaleAmountView;
    [zoomScaleAmountView.layer setCornerRadius:8.0f];
    [_scrollViewDrawContent addSubview:zoomScaleAmountView];
    
    UILabel *zoomScaleAmountLabel = [[UILabel alloc]initWithFrame:CGRectMake(zoomScaleAmountView.bounds.origin.x +8, zoomScaleAmountView.frame.size.height/2 - 25/2, 100, 25)];
    [zoomScaleAmountLabel setBackgroundColor:[UIColor clearColor]];
    [zoomScaleAmountLabel setFont:[UIFont fontWithName:@"Arial" size:17]];
    [zoomScaleAmountLabel setTextColor:[UIColor whiteColor]];
    [zoomScaleAmountLabel setText:[NSString stringWithFormat:@"%.0f%@ Zoom",zoomScaleNumber,@"%"]];
    _zoomScaleAmountLabel = zoomScaleAmountLabel;
    //[self.view addSubview:_zoomScaleAmountView];
    [_zoomScaleAmountView addSubview:zoomScaleAmountLabel];
    
    // NSLog(@"zoomScaleAmountLabel = %@",_zoomScaleAmountLabel);
    
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    //NSLog(@"scrollViewDidZoom");
    
    float zoomScaleNumber = scrollView.zoomScale * 100;
    
    //sends message to drawView so draw methods can adjust for zoom to draw
    //used to help eliminate erronous draw calls
    //************* Fix [_drawView zoomScale:zoomScaleNumber];
    self.drawView.zoomScale = zoomScaleNumber;
    
    
    //removes box & label showing zoom to allow for new one
    if (_zoomScaleAmountView2) {
        [_zoomScaleAmountView2 removeFromSuperview];
    }
    if (_zoomScaleAmountLabel2 ) {
        [_zoomScaleAmountLabel2 removeFromSuperview];
    }
    
    //Steve - Adds a new box and label on screen which shows zoom amount
    //This will adjust to center for the new zoom: Bounds is the relative view. Center: (Width/2 + X - 110/2, Height/2 + Y - 30/2)
    UIView *zoomScaleAmountView2 = [[UIView alloc]initWithFrame:CGRectMake(_scrollViewDrawContent.bounds.size.width/2 + _scrollViewDrawContent.bounds.origin.x -110/2,
                                                                           _scrollViewDrawContent.bounds.size.height/2 + _scrollViewDrawContent.bounds.origin.y-30/2,
                                                                           110,30)];
    
    zoomScaleAmountView2.backgroundColor = [UIColor blackColor];
    zoomScaleAmountView2.alpha = 0.70;
    _zoomScaleAmountView2 = zoomScaleAmountView2;
    [zoomScaleAmountView2.layer setCornerRadius:8.0f];
    [_scrollViewDrawContent addSubview:zoomScaleAmountView2];
    
    UILabel *zoomScaleAmountLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(zoomScaleAmountView2.bounds.origin.x +8, zoomScaleAmountView2.frame.size.height/2 - 25/2, 100, 25)];
    [zoomScaleAmountLabel2 setBackgroundColor:[UIColor clearColor]];
    [zoomScaleAmountLabel2 setFont:[UIFont fontWithName:@"Arial" size:17]];
    [zoomScaleAmountLabel2 setTextColor:[UIColor whiteColor]];
    [zoomScaleAmountLabel2 setText:[NSString stringWithFormat:@"%.0f%@ Zoom",zoomScaleNumber,@"%"]];
    _zoomScaleAmountLabel2 = zoomScaleAmountLabel2;
    [zoomScaleAmountView2 addSubview:zoomScaleAmountLabel2];
    
    if (_zoomScaleAmountView) {
        [_zoomScaleAmountView removeFromSuperview];
    }
    if (_zoomScaleAmountLabel) {
        [_zoomScaleAmountLabel removeFromSuperview];
    }
    
    [self centerScrollViewContents];
    //[self centerScrollViewContentsText];
    
    
    //NSLog(@"scrollViewContent bounds %@",NSStringFromCGRect(scrollViewContent.bounds));
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)aScrollView withView:(UIView *)view atScale:(float)scale {
   // NSLog(@"scrollViewDidEndZooming");
    
    [_zoomScaleAmountView removeFromSuperview];
    [_zoomScaleAmountLabel removeFromSuperview];
    
    [_zoomScaleAmountView2 removeFromSuperview];
    [_zoomScaleAmountLabel2 removeFromSuperview];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //NSLog(@"scrollViewDidScroll");
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //NSLog(@"scrollViewWillBeginDragging");
    
    
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
   // NSLog(@"scrollViewDidEndDragging");
}


#pragma gesture recognizers

-(void)twoFinger_Handle:(UIPanGestureRecognizer *)twofingerGestureRecognizer{
   // NSLog(@"ViewController ******* twoFinger_Handle");
    
    
}


- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    // Start cursor on second row.
    NSRange insertionPoint = NSMakeRange(2, 0);
    textView.selectedRange = insertionPoint;
}



#pragma Nav Buttons Clicked


-(void) textButton_Clicked:(id)sender{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    self.drawView.drawTool = DrawingToolTypeText;

    self.drawTextView.userInteractionEnabled = YES;
    self.drawTextView.multipleTouchEnabled = YES;
    self.drawTextView.editable = YES;
    self.drawTextView.canCancelContentTouches = YES;
    [self.drawTextView becomeFirstResponder];
    
    self.drawView.userInteractionEnabled = NO;
    self.drawView.multipleTouchEnabled = NO;
    
    
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    //The Picture picker popover is showing. Hide it.
    [_picturePicker dismissViewControllerAnimated:YES completion:nil];
    _picturePicker = nil;

    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Color
    float red = [sharedDefaults floatForKey:@"TextRedColor"];
    float green = [sharedDefaults floatForKey:@"TextGreenColor"];
    float blue = [sharedDefaults floatForKey:@"TextBlueColor"];
    
    float alpha = [sharedDefaults floatForKey:@"TextAlpha"];
    float penSize = [sharedDefaults floatForKey:@"TextSize"];
    
    self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    self.drawView.lineWidth = penSize;
    self.drawView.lineAlpha = alpha;
    
    //NSLog(@"self.drawView.lineColor = %@", self.drawView.lineColor);
    //NSLog(@"self.drawView.lineWidth = %f", self.drawView.lineWidth);
    //NSLog(@"self.drawView.lineAlpha = %f", self.drawView.lineAlpha);
    
    
    [sharedDefaults setBool:YES forKey:@"TextTool"];
    [sharedDefaults setBool:NO forKey:@"PenTool"];
    [sharedDefaults setBool:NO forKey:@"HighLighterTool"];
    [sharedDefaults setBool:NO forKey:@"ZoomTool"];
    [sharedDefaults setBool:NO forKey:@"EraserTool"];
    
    
    //Removes Zoom view if its not hidden already
    if (_zoomView.hidden == NO) {
        
        [self zoomButtonPressed];
    }
    
    
    
    //Removes Zoom view if its not hidden already
    if (_zoomView.hidden == NO) {
        
        [self zoomButtonPressed];
    }
    
    
    //If pen tool already selected, it will open pen color & size options
    if ([sharedDefaults boolForKey:@"TextToolOptions"] == YES && [sharedDefaults boolForKey:@"TextTool"] == YES) {           //NSLog(@"Pen Yes");
        
        //********* Add here TextOptionViewController
        /*
        if (_colorPicker == nil) {
            
            //Create the ColorPickerViewController.
            _colorPicker = [[PenColorViewController alloc] initWithNibName:@"PenColorViewController" bundle:nil];
            _colorPicker.delegate = self;
        }
        
        //The color picker popover is not showing. Show it.
        _colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:_colorPicker];
        [_colorPickerPopover setPopoverContentSize:CGSizeMake(498, 338) animated:YES];
        [_colorPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        */
        
    }
    else{
        
        [sharedDefaults setBool:YES forKey:@"TextToolOptions"];
        [sharedDefaults setBool:NO forKey:@"PenToolOptions"];
        [sharedDefaults setBool:NO forKey:@"HighLighterToolOptions"];
        [sharedDefaults setBool:NO forKey:@"EraserToolOptions"];
        
    }
    
    
    [sharedDefaults setBool:YES forKey:@"TextTool"];
    [sharedDefaults setBool:NO forKey:@"PenTool"];
    [sharedDefaults setBool:NO forKey:@"HighLighterTool"];
    [sharedDefaults setBool:NO forKey:@"ZoomTool"];
    [sharedDefaults setBool:NO forKey:@"EraserTool"];
    
}



-(void) penButton_Clicked:(id)sender{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    // If ColorPicker is Active, then isHighlighter is YES so
    // ColorPicker viewWillDissappear will save the correct values
    // otherwise it swaps values with Highlighter
    if (_colorPickerPopover != nil) {
        
        if (_colorPicker != nil) {
            
            _colorPicker.isHighlighter = YES;
        }
    }
    
    
    self.drawView.drawTool = DrawingToolTypePen;
    
    self.drawTextView.userInteractionEnabled = NO;
    self.drawTextView.multipleTouchEnabled = NO;
    
    self.drawView.userInteractionEnabled = YES;
    self.drawView.multipleTouchEnabled = YES;
    
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    //The Picture picker popover is showing. Hide it.
    [_picturePicker dismissViewControllerAnimated:YES completion:nil];
    _picturePicker = nil;

    

    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Color
    float red = [sharedDefaults floatForKey:@"PenRedColor"];
    float green = [sharedDefaults floatForKey:@"PenGreenColor"];
    float blue = [sharedDefaults floatForKey:@"PenBlueColor"];
    
    float alpha = [sharedDefaults floatForKey:@"PenAlpha"];
    float penSize = [sharedDefaults floatForKey:@"PenSize"];

    self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    self.drawView.lineWidth = penSize;
    self.drawView.lineAlpha = alpha;
    
    //NSLog(@"self.drawView.lineColor = %@", self.drawView.lineColor);
    //NSLog(@"self.drawView.lineWidth = %f", self.drawView.lineWidth);
    //NSLog(@"self.drawView.lineAlpha = %f", self.drawView.lineAlpha);
    
    
    //Removes Zoom view if its not hidden already
    if (_zoomView.hidden == NO) {
        
        [self zoomButtonPressed];
    }

    
    if ([sharedDefaults boolForKey:@"PenToolOptions"] == YES && [sharedDefaults boolForKey:@"PenTool"] == YES) {   //If pen tool already selected, it will open pen color & size options
        //NSLog(@"Pen Yes");
        

        if (_colorPicker == nil) {
            
            //Create the ColorPickerViewController.
            _colorPicker = [[PenColorViewController alloc] initWithNibName:@"PenColorViewController" bundle:nil];
            _colorPicker.delegate = self;
        }
        
        _colorPicker.isHighlighter = NO;
        
        //The color picker popover is not showing. Show it.
        _colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:_colorPicker];
        [_colorPickerPopover setPopoverContentSize:CGSizeMake(498, 338) animated:YES];
        [_colorPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        

    }
    else{
        
        [sharedDefaults setBool:NO forKey:@"TextToolOptions"];
        [sharedDefaults setBool:YES forKey:@"PenToolOptions"];
        [sharedDefaults setBool:NO forKey:@"HighLighterToolOptions"];
        [sharedDefaults setBool:NO forKey:@"EraserToolOptions"];

    }
    
    
    [sharedDefaults setBool:NO forKey:@"TextTool"];
    [sharedDefaults setBool:YES forKey:@"PenTool"];
    [sharedDefaults setBool:NO forKey:@"HighLighterTool"];
    [sharedDefaults setBool:NO forKey:@"ZoomTool"];
    [sharedDefaults setBool:NO forKey:@"EraserTool"];
    
    
}



-(void) highligherButton_Clicked:(id)sender{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    // If ColorPicker is Active, then isHighlighter is NO so
    // ColorPicker viewWillDissappear will save the correct values
    // otherwise it swaps values with Pen
    if (_colorPickerPopover != nil) {
        
        if (_colorPicker != nil) {
            
            _colorPicker.isHighlighter = NO;
        }
    }
    
    
    self.drawView.drawTool = DrawingToolTypePen;
    
    self.drawTextView.userInteractionEnabled = NO;
    self.drawTextView.multipleTouchEnabled = NO;
    
    self.drawView.userInteractionEnabled = YES;
    self.drawView.multipleTouchEnabled = YES;
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    //The Picture picker popover is showing. Hide it.
    [_picturePicker dismissViewControllerAnimated:YES completion:nil];
    _picturePicker = nil;

    
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Color
    float red = [sharedDefaults floatForKey:@"highlighterRedColor"];
    float green = [sharedDefaults floatForKey:@"highlighterGreenColor"];
    float blue = [sharedDefaults floatForKey:@"highlighterBlueColor"];
    
    float penSize = [sharedDefaults floatForKey:@"highlighterSize"];
    //float alpha = [sharedDefaults floatForKey:@"highlighterAlpha"];
    float alpha = 0.50;
    
    self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    self.drawView.lineWidth = penSize;
    self.drawView.lineAlpha = alpha;
    
    
    //NSLog(@"self.drawView.lineColor = %@", self.drawView.lineColor);
    //NSLog(@"self.drawView.lineWidth = %f", self.drawView.lineWidth);
    //NSLog(@"self.drawView.lineAlpha = %f", self.drawView.lineAlpha);
    
    
    //Removes Zoom view if its not hidden already
    if (_zoomView.hidden == NO) {
        
        [self zoomButtonPressed];
    }
    
    
    
    //If Highligher tool already selected, it will open pen color & size options
    if ([sharedDefaults boolForKey:@"HighLighterToolOptions"] == YES && [sharedDefaults boolForKey:@"HighLighterTool"] == YES) {
        //NSLog(@"Pen Yes");
        
        
        if (_colorPicker == nil) {
            
            //Create the ColorPickerViewController.
            _colorPicker = [[PenColorViewController alloc] initWithNibName:@"PenColorViewController" bundle:nil];
            _colorPicker.delegate = self;
        }
        
        _colorPicker.isHighlighter = YES;
        
        //The color picker popover is not showing. Show it.
        _colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:_colorPicker];
        [_colorPickerPopover setPopoverContentSize:CGSizeMake(498, 338 - 40) animated:YES];
        [_colorPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        
    }
    else{
        
        [sharedDefaults setBool:NO forKey:@"TextToolOptions"];
        [sharedDefaults setBool:NO forKey:@"PenToolOptions"];
        [sharedDefaults setBool:YES forKey:@"HighLighterToolOptions"];
        [sharedDefaults setBool:NO forKey:@"EraserToolOptions"];
    }

    
    [sharedDefaults setBool:NO forKey:@"TextTool"];
    [sharedDefaults setBool:NO forKey:@"PenTool"];
    [sharedDefaults setBool:YES forKey:@"HighLighterTool"];
    [sharedDefaults setBool:NO forKey:@"ZoomTool"];
    [sharedDefaults setBool:NO forKey:@"EraserTool"];
    

}


-(void) eraserButton_Clicked{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    self.drawView.drawTool = DrawingToolTypeEraser;
    
    self.drawTextView.userInteractionEnabled = NO;
    self.drawTextView.multipleTouchEnabled = NO;
    
    self.drawView.userInteractionEnabled = YES;
    self.drawView.multipleTouchEnabled = YES;
    
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    //The Picture picker popover is showing. Hide it.
    [_picturePicker dismissViewControllerAnimated:YES completion:nil];
    _picturePicker = nil;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //********* Not sure if needed yet
    //If pen tool already selected, it will open pen color & size options
    if ([sharedDefaults boolForKey:@"EraserToolOptions"] == YES && [sharedDefaults boolForKey:@"EraserTool"] == YES) {
        //NSLog(@"Eraser Yes");

    }
    else{
        
        [sharedDefaults setBool:NO forKey:@"TextToolOptions"];
        [sharedDefaults setBool:NO forKey:@"PenToolOptions"];
        [sharedDefaults setBool:NO forKey:@"HighLighterToolOptions"];
        [sharedDefaults setBool:YES forKey:@"EraserToolOptions"];
    }
    
    
    [sharedDefaults setBool:NO forKey:@"TextTool"];
    [sharedDefaults setBool:NO forKey:@"PenTool"];
    [sharedDefaults setBool:NO forKey:@"HighLighterTool"];
    [sharedDefaults setBool:NO forKey:@"ZoomTool"];
    [sharedDefaults setBool:YES forKey:@"EraserTool"];
}


-(void) zoomButtonPressed{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    //The Picture picker popover is showing. Hide it.
    [_picturePicker dismissViewControllerAnimated:YES completion:nil];
    _picturePicker = nil;

    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"TextTool"];
    [sharedDefaults setBool:NO forKey:@"PenTool"];
    [sharedDefaults setBool:NO forKey:@"HighLighterTool"];
    [sharedDefaults setBool:YES forKey:@"ZoomTool"];
    [sharedDefaults setBool:NO forKey:@"EraserTool"];
    
    
    
    if (_zoomView.hidden) {
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 0.5;
        [_zoomView.layer addAnimation:transition forKey:kCATransitionFromTop];
        
        _zoomView.hidden = NO;
        
        
        [UIView beginAnimations:nil context:nil];
        _zoomCoverDrawImageView.alpha = 0.0f;
        [UIView setAnimationDelay:0.5];
        [UIView setAnimationDuration:0.0];
        [_zoomCoverDrawImageView  setAlpha:0.25];
        [UIView commitAnimations];
        
        _zoomCoverDrawImageView.hidden = NO;
        
        self.view.userInteractionEnabled = NO;
    }
    else{
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromBottom;
        transition.duration = 0.5;
        [_zoomView.layer addAnimation:transition forKey:kCATransitionFromTop];
        
        _zoomView.hidden = YES;
        
        
        [self performSelector:@selector(hideZoomCoverViewWithDelay)
                   withObject:nil
                   afterDelay:0.5];
    }
    
}


-(void) hideZoomCoverViewWithDelay{
    
    _zoomCoverDrawImageView.hidden = YES;
    self.view.userInteractionEnabled = YES;
}


-(void) pictureButton_Clicked:(id) sender{
    
    [currentlyEditingView hideEditingHandles];
    [lastEditedView hideEditingHandles];
    
    
    //The color picker popover is showing. Hide it.
    [_colorPickerPopover dismissPopoverAnimated:YES];
    _colorPickerPopover = nil;
    
    
    self.drawTextView.userInteractionEnabled = YES;
    self.drawTextView.multipleTouchEnabled = YES;
    self.drawTextView.editable = NO;
    self.drawTextView.canCancelContentTouches = YES;
    
    self.drawView.userInteractionEnabled = NO;
    self.drawView.multipleTouchEnabled = NO;
    
    
    
    if (_picturePicker == nil) {
        
        //Create the TakePictureTableViewController.
        _picturePicker = [[TakePictureTableViewController alloc] initWithNibName:@"TakePictureTableViewController" bundle:nil];
        _picturePicker.photoAlbumDelegate = self;
    }
    
    
    //The color picker popover is not showing. Show it.
    _takePicturePopover = [[UIPopoverController alloc] initWithContentViewController:_picturePicker];
    [_takePicturePopover setPopoverContentSize:CGSizeMake(150, 88) animated:YES];
    [_takePicturePopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

#pragma mark - Protocal methods from TakePictureTableViewController

-(void)photoAlbum{
    
    [_takePicturePopover dismissPopoverAnimated:YES];
    
    
     UIImagePickerController *imagePicker= [[UIImagePickerController alloc]init];
     imagePicker.delegate = self;
     imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
     
     imagePicker.navigationBar.tintColor =  self.navigationController.navigationBar.tintColor;
     imagePicker.navigationBar.barStyle =self.navigationController.navigationBar.barStyle;
     
     UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
     [popoverController presentPopoverFromBarButtonItem:_addImageBarButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}


-(void)takePicture{
    
    [_takePicturePopover dismissPopoverAnimated:YES];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *Alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your device does not support a camera." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        //Steve commented out
        // [Alert release];
        
    }
    
}


#pragma mark - UIImagePickerController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {

    img = [img imageByScalingProportionallyToSize:CGSizeMake(960/2,1176/2)];  //resizes images
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self addPictureToTextView:img];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) addPictureToTextView:(UIImage *) image{
    
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(125, 125, image.size.width, image.size.height);
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image;
    
    
    SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:imageView.frame];
  //  imageResizableView.contentViewSPU = imageView;
  //  imageResizableView.delegate = self;
    imageResizableView.preventsPositionOutsideSuperview = YES;
    [imageResizableView showEditingHandles];
    [self.drawTextView addSubview:imageResizableView];
    
    currentlyEditingView = imageResizableView;
    
    // Wrap around images - set the exclusion path
    //UIBezierPath *imgRect = [UIBezierPath bezierPathWithRect:currentlyEditingView.frame];
    //self.drawTextView.textContainer.exclusionPaths = @[imgRect];
    
    [_imageArray addObject:imageResizableView];
     [self setExclusionPaths:[self framesWithImageView]];

}

#pragma mark - SPUserResizableView

- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    
    [currentlyEditingView hideEditingHandles];
    currentlyEditingView = userResizableView;
}


- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
    lastEditedView = userResizableView;
    
    //UIBezierPath *imgRect = [UIBezierPath bezierPathWithRect:lastEditedView.frame];
    //self.drawTextView.textContainer.exclusionPaths = @[imgRect];
    
    
    NSLog(@"lastEditedView.frame = %@", NSStringFromCGRect(lastEditedView.frame));
    
    
    int index = 0;
    
    for (SPUserResizableView *resizeView in _imageArray) {
        
        if (resizeView == lastEditedView) {
            NSLog(@"Found");
            
            [_imageArray replaceObjectAtIndex:index withObject:lastEditedView];
            break;
        }
        
        index = index + 1;
    }

    
    [self setExclusionPaths:[self framesWithImageView]];
}


- (NSArray *)framesWithImageView{
    
    NSMutableArray *frames = [[NSMutableArray alloc] init];
    
    for (SPUserResizableView *resizeView in _imageArray) {
        
        //UIBezierPath *bPath = [UIBezierPath bezierPathWithCGPath:imageView.bezierPath.CGPath];
        UIBezierPath *imgRect = [UIBezierPath bezierPathWithRect:resizeView.frame];
        [frames addObject:imgRect];
    }
    
    return [NSArray arrayWithArray:frames];
}

- (void)setExclusionPaths:(NSArray *)paths{
    self.drawTextView.textContainer.exclusionPaths = paths;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    //Used to pass information to handleTapGesture method
    _currentGestureRecognizer = gestureRecognizer;
    _currentTouch = touch;

    /*
    if ([currentlyEditingView hitTest:[touch locationInView:currentlyEditingView] withEvent:nil]) {
        return NO;
    }
     */

    return YES;
}




- (void)handleTapGesture {
    
    ////////// Shows & Hides view when touched
    SPUserResizableView *currentView = (SPUserResizableView *)[_currentGestureRecognizer view];
    
    for (SPUserResizableView *subview in currentView.subviews)
        
    {
        CGPoint touchPoint = [_currentTouch locationInView:subview];
        
        if ([subview hitTest:touchPoint withEvent:nil])
        {
            
            if([subview isKindOfClass:[SPUserResizableView class]])
            {
                SPUserResizableView *imageResizableView = subview;
 
                lastEditedView = currentlyEditingView;
                currentlyEditingView = imageResizableView;

                [lastEditedView hideEditingHandles];
                [currentlyEditingView showEditingHandles];
                [self.drawTextView resignFirstResponder];
                return;
            }
            else
            {
                [lastEditedView hideEditingHandles];
                [currentlyEditingView hideEditingHandles];
        
            }
            
        }
        else
        {
            [lastEditedView hideEditingHandles];
            [currentlyEditingView hideEditingHandles];
        }
    }
    
    
    
    //Finds cursor position for the location touched
    if (_currentGestureRecognizer.state==UIGestureRecognizerStateEnded)
    {
        self.drawTextView.editable = YES;
        self.drawTextView.dataDetectorTypes = UIDataDetectorTypeNone;
        [self.drawTextView becomeFirstResponder];
        
        //Consider replacing self.view here with whatever view you want the point within
        CGPoint point = [_currentGestureRecognizer locationInView:self.view];
        UITextPosition *position=[self.drawTextView closestPositionToPoint:point];
        [self.drawTextView setSelectedTextRange:[self.drawTextView textRangeFromPosition:position toPosition:position]];
        
        
        [lastEditedView hideEditingHandles];
        [currentlyEditingView hideEditingHandles];
    }
    
    
    // We only want the gesture recognizer to end the editing session on the last
    // edited view. We wouldn't want to dismiss an editing session in progress.
//    [lastEditedView hideEditingHandles];
//    [currentlyEditingView showEditingHandles];
}



#pragma mark - Split view support

- (void)splitViewController: (UISplitViewController*)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController*)popoverController
{
    barButtonItem.title = NSLocalizedString(@"< Notes", @"Notes");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController: (UISplitViewController*)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


// Called when the hidden view controller is about to be displayed in a popover.
- (void)splitViewController:(UISplitViewController*)svc popoverController:(UIPopoverController*)pc willPresentViewController:(UIViewController *)aViewController
{
    // Check whether the popover presented from the "Tap" UIBarButtonItem is visible.
    if ([self.barButtonItemPopover isPopoverVisible])
    {
        // Dismiss the popover.
        [self.barButtonItemPopover dismissPopoverAnimated:YES];
    } 
}



#pragma mark Stroke details (width, color, alpha)

//Stroke width
-(void)SetLineWidth:(float) lineWidth{
    
    self.drawView.lineWidth = lineWidth;
}


//Stroke Color
-(void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue{

    self.drawView.lineColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}


//Stroke Alpha
-(void)setAlphaColor:(CGFloat) alphaColorSelection{
    
    self.drawView.lineAlpha = alphaColorSelection;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark RichTextEditor

/*
 - (NSArray *)fontSizeSelectionForRichTextEditor:(RichTextEditor *)richTextEditor
 {
	// pas an array of NSNumbers
	return @[@5, @20, @30];
 }
 */

- (RichTextEditorToolbarPresentationStyle)presentarionStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return RichTextEditorToolbarPresentationStyleModal;
}

- (UIModalPresentationStyle)modalPresentationStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return UIModalPresentationFormSheet;
}

- (UIModalTransitionStyle)modalTransitionStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return UIModalTransitionStyleFlipHorizontal;
}


- (RichTextEditorFeature)featuresEnabledForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return RichTextEditorFeatureFontSize | RichTextEditorFeatureFont | RichTextEditorFeatureAll;
    //return RichTextEditorFeatureAll ;
    
}


@end

//
//  DrawingConstants.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString* const CellIdentifier;
extern NSString* const KeyNotesData;
extern NSString* const IconMoonFont;

extern NSString * const KeyFrames;
extern NSString * const KeyData;
extern NSString * const KeyText;
extern NSString * const KeyID;
extern NSString * const KeyIconData;
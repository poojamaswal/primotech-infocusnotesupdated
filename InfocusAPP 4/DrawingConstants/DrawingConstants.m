//
//  DrawingConstants.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DrawingConstants.h"


NSString* const CellIdentifier   = @"NotesTableCell";
NSString* const KeyNotesData     = @"data";

NSString* const KeyFrames = @"frames";
NSString* const KeyData = @"data";
NSString* const KeyText = @"text";
NSString* const KeyID = @"ID";
NSString* const KeyIconData = @"dataIcon";

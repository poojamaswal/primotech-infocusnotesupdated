//
//  CalendarParentController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//Alok commented for get Stroryboard #import "CalendarListViewController.h"
#import "CalendarList2ViewController.h"
#import "CalendarDailyViewController.h"
#import "CalendarWeekViewController.h"
#import "CalendarMonthViewController.h"
#import "InputTypeSelectorViewController.h"
#import "SearchBarViewController.h"
#import "CustomizeCalendarStoryboardViewController.h"

@class CalendarList2ViewController;
@class CalendarDailyViewController;
@class CalendarWeekViewController;
@class CalendarMonthViewController;
@class InputTypeSelectorViewController;


@interface CalendarParentController : UIViewController
{
	IBOutlet UINavigationBar				*aNavBar;
	IBOutlet UIToolbar						*aBottomToolBar;
    IBOutlet UIBarButtonItem                *inputSelectorBarButton;
	
	CalendarList2ViewController				*aCalListViewController;
	CalendarDailyViewController				*aCalDailyViewController;
	CalendarWeekViewController				*aCalWeekViewController;
	CalendarMonthViewController				*aCalMonthViewController;
    InputTypeSelectorViewController			*inputTypeSelectorViewController;
	
	NSInteger								currentSelectedBtnTag;
    OrganizerAppDelegate					*appDelegate;
	SearchBarViewController					*searchBarViewController;
	
    BOOL                                    callIsActive;
    BOOL                                    isHandwritingView;
    IBOutlet UIView                         *progressView;
    IBOutlet UIProgressView                 *progressbar;
    //alok added
    IBOutlet UIView                         *helpView; //Steve added
    IBOutlet UITapGestureRecognizer         *tapHelpView; //Steve added
    IBOutlet UISwipeGestureRecognizer       *swipeDownHelpView;
    IBOutlet UIView                         *helpViewNewNav; //Steve
    IBOutlet UIView                         *helpViewChangeNavInstructions;
    
    
    IBOutlet UIView                         *helpViewOverlay;  //Steve
    IBOutlet UIView                         *calendarAccessView; //Steve added
    IBOutlet UIScrollView                   *calendarAccessScrollView; //Steve added
    IBOutlet UINavigationBar                *aNavBarAccessHelpMenu; //Steve
    EKEventStore                            *eventStore; //Steve
    
    IBOutlet UIButton                       *homeButton; //Steve
    IBOutlet UIButton                       *revealButton; //Steve
    IBOutlet UIButton                       *addButton; //Steve
    
    UISegmentedControl                      *segmentedCalendarControl; //Steve
    BOOL                                    *isViewAppearing;//Steve
    
    BOOL                                    isMenuClosed; //Steve
    BOOL                                    isHelpShowing; //Steve
    
}



@property(nonatomic,strong)	UILocalNotification *notification;
@property(nonatomic,strong)	CalendarList2ViewController				*aCalListViewController;
@property(nonatomic,strong)	CalendarDailyViewController				*aCalDailyViewController;
@property(nonatomic,strong)	CalendarWeekViewController				*aCalWeekViewController;
@property(nonatomic,strong)	CalendarMonthViewController				*aCalMonthViewController;
@property(nonatomic,strong) id  calling_VC;//Aman's Added
@property(nonatomic) BOOL isHandwritingView;
@property (strong, nonatomic) IBOutlet UIView *rateAppView; //Steve - used to rate app
@property (strong, nonatomic) IBOutlet UIView *rateAppView_MainView; //STeve - used to rate app

@property (strong, nonatomic) IBOutlet UIButton *neverRateButton;  //Steve - used to rate app
@property (strong, nonatomic) IBOutlet UIButton *laterRateButton;  //Steve - used to rate app
@property (strong, nonatomic) IBOutlet UIButton *rateBtton;  //Steve - used to rate app
@property (strong, nonatomic) IBOutlet UITextField *rateTextField; //Steve - used to rate app
@property (strong, nonatomic) IBOutlet UILabel *rateTitleLabel; //Steve - used to change title label "InFocus Pro", InFocus Checklist", etc.
@property (strong, nonatomic) IBOutlet UIToolbar	*aBottomToolBar; //Steve

@property (nonatomic,strong) id         parentVC; //Steve


- (IBAction)neverRateButton_Clicked:(id)sender;//Steve - used to rate app
- (IBAction)laterRateButton_Clicked:(id)sender;//Steve - used to rate app
- (IBAction)rateButton_Clicked:(id)sender;//Steve - used to rate app

- (IBAction)noteButtonClicked:(id)sender;
- (IBAction)projectButtonClicked:(id)sender;

-(IBAction)	addAppointment:(id)sender;
-(IBAction) homeButton_Clicked:(id)sender;
-(IBAction) todoButton_Clicked:(id)sender;

-(IBAction)todayButton_Clicked:(id)sender;
-(IBAction)optionButton_Clicked:(id)sender;
-(IBAction)searchBarButton_Clicked:(id)sender;
-(IBAction)inputOptionButton_Clicked:(id)sender;
- (IBAction)listBtnClicked:(id)sender;

-(void)addAdditionalControlsInCalenderParent ;
-(void)changedSelection;
- (IBAction)textToSpeak:(id)sender;
- (IBAction)helpView_Tapped:(id)sender; //Steve
- (IBAction)doneButton_Clicked:(id)sender; //Steve

- (void) invisibleViewTriggered; //Steve
-(void)SetToolBarToFrontPosition:(BOOL) isToolBarFront; //Steve - used with protocal on Calendar Monthview



@end

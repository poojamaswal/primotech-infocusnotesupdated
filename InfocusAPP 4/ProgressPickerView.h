//
//  ProgressPickerView.h
//  Organizer
//
//  Created by Naresh Chauhan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoViewController.h"
@class ToDoViewController;


@interface ProgressPickerView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource,ToDoViewControllerDelegate>{
IBOutlet UILabel *mlabel;
NSMutableArray *arrayNo;
IBOutlet UIPickerView *pickerView;
NSString *getString;
id     __unsafe_unretained delegate;
NSString *intSTR;
NSString *str;
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
	
}
@property(unsafe_unretained) id delegate;
@property (nonatomic, strong) UILabel *mlabel;
@property(nonatomic,strong)	NSString *getString;
@property(nonatomic,strong)	NSString *intSTR;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withProgressVAL:(NSString*) progress;
-(IBAction)Cancel_click:(id)sender;
-(IBAction)Save_click:(id)sender;
- (NSMutableArray*) stringReverse;


@end

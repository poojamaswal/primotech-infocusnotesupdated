//
//  HelpViewController.m
//  Organizer
//
//  Created by alok gupta on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HelpViewController.h"

@implementation HelpViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    
    
    if (IS_IOS_7) {
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
       // [[UIApplication sharedApplication] setStatusBarHidden:YES];
        self.navigationController.navigationBarHidden = NO;
        navBar.hidden = YES;
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        //self.view.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:228.0/255.0  blue:228.0/255.0  alpha:1.0];
        //self.view.backgroundColor = [UIColor whiteColor];
        
        cancelButton.hidden = YES;
        doneButton.hidden = YES;
        
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
        /*
        navBar.barTintColor = [UIColor blackColor];
        navBar.tintColor = [UIColor whiteColor];
        navBar.translucent = NO;
        navBar.alpha = 0.75;
         */
        
        //Cancel
        /*
        UIBarButtonItem *cancelButton_iOS7 = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                              target:self
                                              action:@selector(BackButtonClicked:)];
        */

        //Done
        UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                              target:self
                                              action:@selector(DoneButtonClicked:)];
        
        if(IS_IPAD)
            doneButton_iOS7.tintColor = [UIColor whiteColor];
        else
            doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        self.navigationItem.rightBarButtonItem = doneButton_iOS7;
        
        


        UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"Help"];
        //item.leftBarButtonItem = cancelButton_iOS7;
        item.hidesBackButton = YES;
  
        
    }
    else{ //iOS 6
        
        self.navigationController.navigationBarHidden = NO;
        
        //Steve - Custom button on Nav Bar
        UIImage *buttonImage = [UIImage imageNamed:@"Done.png"];
        UIButton *doneNavButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 51, 29)];
        [doneNavButton setImage:buttonImage forState:UIControlStateNormal];
        [doneNavButton addTarget:self
                       action:@selector(DoneButtonClicked:)
                       forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:doneNavButton];
        self.navigationItem.rightBarButtonItem = barButton;
        
        
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
        
        //Sets Navigation item color
        [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        
        //Steve - Move Switches for iOS 6
        Switch_AllHelp.frame = CGRectMake(208, 6, 51, 31);
        Switch_Calender.frame = CGRectMake(208, 6, 51, 31);
        Switch_List.frame = CGRectMake(208, 6, 51, 31);
        Switch_Todo.frame = CGRectMake(208, 6, 51, 31);
        Switch_Project.frame = CGRectMake(208, 6, 51, 31);
        Switch_Handwriting.frame = CGRectMake(208, 6, 51, 31);
        
    }

    
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"HELP VIEW IS_PROJECTS_VER =   %d", IS_PROJECTS_VER);

    if(IS_LIST_VERSION){
        [navBarList setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        [self.view addSubview:ListView];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"])
            [Switch_List_Calender setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_List"])
            [Switch_List_Listonly setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"] && [sharedDefaults boolForKey:@"FirstLaunch_List"])
            [Switch_List_AllHelp setOn:YES];
         
    }
    else if (IS_TO_DO_VERSION){ //Steve added
        [navBarToDo setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        [self.view addSubview:todoView];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"])
            [Switch_TODO_Calender setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_TodoToday"])
            [Switch_TODO_TODOonly setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"] && [sharedDefaults boolForKey:@"FirstLaunch_TodoToday"])
            [Switch_TODO_AllHelp setOn:YES];
    }
    else if (IS_PROJECTS_VER){
        NSLog(@"HELP VIEW IS_PROJECTS_VER 2 =   %d", IS_PROJECTS_VER);
        [navBarProjects setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        [self.view addSubview:projectsView];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"])
            [Switch_Projects_Calendar setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_Project"])
            [Switch_Projects_Projects setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"] && [sharedDefaults boolForKey:@"FirstLaunch_Project"])
            [Switch_Projects_AllHelp setOn:YES];
    }
    else if (IS_NOTES_VER){
        [navBarNotes setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        [self.view addSubview:notesView];
        if([sharedDefaults boolForKey:@"FirstLaunch_NotesAdd"])
            [Switch_Notes_Notes setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_HandWriting_3"])
            [Switch_Notes_Handwriting setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_NotesAdd"] && [sharedDefaults boolForKey:@"FirstLaunch_HandWriting_3"])
            [Switch_Notes_AllHelp setOn:YES];
    }
    else{ //Pro Version
        
        
        if([sharedDefaults boolForKey:@"NavigationNewFirstLoad"])
        {
            [Switch_Home setOn:YES];
            
            if([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"])
                [Switch_Calender setOn:YES];
        }
        
        
        if([sharedDefaults boolForKey:@"FirstLaunch_List"])
            [Switch_List setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_TodoToday"])
            [Switch_Todo setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_Project"])
            [Switch_Project setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_NotesAdd"])
            [Switch_Notes setOn:YES];
        if([sharedDefaults boolForKey:@"FirstLaunch_HandWriting_3"])
            [Switch_Handwriting setOn:YES];
        
        if([sharedDefaults boolForKey:@"FirstLaunch_Home"] && [sharedDefaults boolForKey:@"FirstLaunch_HandWriting_3"] && [sharedDefaults boolForKey:@"FirstLaunch_Notes"] && [sharedDefaults boolForKey:@"FirstLaunch_Project"] && [sharedDefaults boolForKey:@"FirstLaunch_TodoToday"] && [sharedDefaults boolForKey:@"FirstLaunch_List"] && [sharedDefaults boolForKey:@"FirstLaunch_Calendar2"])
            [Switch_AllHelp setOn:YES];
    }
    
    
}
-(IBAction)BackButtonClicked:(id)sender{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:Nil];
}


-(IBAction)DoneButtonClicked:(id)sender{
    NSLog(@"DoneButtonClicked");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (IS_TO_DO_VERSION) {
        if ([Switch_TODO_Calender isOn]){
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        
        
        if ([Switch_TODO_TODOonly isOn]){
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoSettings"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoToday"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoOverDue"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoAddPriority"];
            [sharedDefaults setInteger:0 forKey:@"AnimateToDoCount"];
        }
        else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoSettings"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoToday"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoOverDue"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoAddPriority"];
            [sharedDefaults setInteger:3 forKey:@"AnimateToDoCount"]; // Steve - when greater than 2, ToDo Tab animation doesn't show anymore
        }
        
        //[self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:Nil];
        return;
    }
    
    if (IS_LIST_VERSION) {
        if ([Switch_List_Calender isOn]){
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        
        
        if ([Switch_List_Listonly isOn])
        {
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_List"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_List_2"];
        }else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_List"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_List_2"];
        }
        
        //[self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:Nil];
        return;
    }

    
    if (IS_PROJECTS_VER) {
        if ([Switch_Projects_Calendar isOn]){
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        
        
        if ([Switch_Projects_Projects isOn])
        {
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Project"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_ProjectView"];
        }else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Project"];
        }
        
        //[self.navigationController popViewControllerAnimated:YES];
         [self dismissViewControllerAnimated:YES completion:Nil];
        return;
    }

    if (IS_NOTES_VER) {
        if ([Switch_Notes_Notes isOn])
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_NotesAdd"];
        else
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesAdd"];
        
        
        if ([Switch_Notes_Handwriting isOn])
        {
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_HandWriting_3"];
        }else{
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_HandWriting_3"];
        }
        
        //[self.navigationController popViewControllerAnimated:YES];
         [self dismissViewControllerAnimated:YES completion:Nil];
        return;
    }
    
    
    // To set help view for List Version Lite and Pro version
    if([Switch_AllHelp isOn])
    {
        [sharedDefaults setBool:YES forKey:@"NavigationNewFirstLoad"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_Home"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_List"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_List_2"];
        
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoSettings"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoToday"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoOverDue"];  //Steve
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoAddPriority"];  //Steve
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoAddProject"];  //Steve
        [sharedDefaults setInteger:0 forKey:@"AnimateToDoCount"]; //Steve added to turn on Animation of To Do tab
        
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_Project"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_ProjectView"]; //Steve
        
        //[sharedDefaults setBool:YES forKey:@"FirstLaunch_Notes"];  //Steve commented - removed help
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_NotesAdd"]; //Steve
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_NotesSketch"]; //Steve
        
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_HandWriting_3"];
    }
    
    
    if([Switch_Home isOn]){
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]){
            
            [sharedDefaults setBool:YES forKey:@"NavigationNewFirstLoad"]; //shows Nav help for NewNav
        }
        else{
            
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Home"]; //shows home help for old style Nav
        }
        
    }
    
    
    if([Switch_Calender isOn]){
        
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        
    }
    
    
    
    if([Switch_List isOn])
    {
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_List"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_List_2"];
    }
    
    if([Switch_Handwriting isOn])[sharedDefaults setBool:YES forKey:@"FirstLaunch_HandWriting_3"];
    
    if ([Switch_Todo isOn])  //Steve - sets help on all Todo help screens
    {
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoSettings"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoToday"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoOverDue"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoAddPriority"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_TodoAddProject"];
        [sharedDefaults setInteger:0 forKey:@"AnimateToDoCount"]; //Steve - turns on animation
    }
    
    if ([Switch_Project isOn]) //Steve - sets help on all Project help screens
    {
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_Project"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_ProjectView"];   
    }
    
    if ([Switch_Notes isOn]) //Steve - sets help on all Note help screens
    {
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_NotesAdd"];
        [sharedDefaults setBool:YES forKey:@"FirstLaunch_NotesSketch"]; 
    }
    
    
    ///////////////////////// Steve added /////////////////////////
    //Allow user to turn off help screens
    
    if(![Switch_AllHelp isOn] && !IS_LIST_VERSION && !IS_TO_DO_VERSION)
    {
        
        if(![Switch_Home isOn] && ![Switch_Calender isOn]){
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }else{
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:YES forKey:@"FirstLaunch_PhotoCalendarSetImage"];
        }
        
        if(![Switch_Calender isOn]){
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
            
        }
        if(![Switch_List isOn])[sharedDefaults setBool:NO forKey:@"FirstLaunch_List"];
        if(![Switch_List isOn])[sharedDefaults setBool:NO forKey:@"FirstLaunch_List_2"];
        if(![Switch_Handwriting isOn])[sharedDefaults setBool:NO forKey:@"FirstLaunch_HandWriting_3"];
        
        
        
        if (![Switch_Todo isOn])  //Steve - sets help on all Todo help screens
        {
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoToday"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoOverDue"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoAddPriority"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoAddProject"];
            [sharedDefaults setInteger:3 forKey:@"AnimateToDoCount"]; //Steve - turns off animation
        }
        
        if (![Switch_Project isOn]) //Steve - sets help on all Project help screens
        {
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_Project"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_ProjectView"];   
        }
        
        if (![Switch_Notes isOn]) //Steve - sets help on all Note help screens
        {
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesAdd"];
            [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesSketch"]; 
        }
        
    }
    
    ///////////////////////// Steve added End  /////////////////////////
    
    
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:Nil];
    
}

- (IBAction)switch_AllHelpChanged:(id)sender {
    NSLog(@"switch_AllHelpChanged");
    
    if (IS_TO_DO_VERSION) {
        if ([Switch_TODO_AllHelp isOn]) {
            [Switch_TODO_Calender setOn:YES animated:YES];
            [Switch_TODO_TODOonly setOn:YES animated:YES];
        }
        else {
            [Switch_TODO_Calender setOn:NO animated:YES];
            [Switch_TODO_TODOonly setOn:NO animated:YES];
        }
        
        return;
    }
    
        
    if (IS_LIST_VERSION) {
        if ([Switch_List_AllHelp isOn]) {
            [Switch_List_Calender setOn:YES animated:YES];
            [Switch_List_Listonly setOn:YES animated:YES];
        }
        else {
            [Switch_List_Calender setOn:NO animated:YES];
            [Switch_List_Listonly setOn:NO animated:YES];
        }
        
        return;
    }
    
    if (IS_PROJECTS_VER) {
        if ([Switch_Projects_AllHelp isOn]) {
            [Switch_Projects_Calendar setOn:YES animated:YES];
            [Switch_Projects_Projects setOn:YES animated:YES];
        }
        else {
            [Switch_Projects_Calendar setOn:NO animated:YES];
            [Switch_Projects_Projects setOn:NO animated:YES];
        }
        
        return;
    }
    
    if (IS_NOTES_VER) {
        if ([Switch_Notes_AllHelp isOn]) {
            [Switch_Notes_Notes setOn:YES animated:YES];
            [Switch_Notes_Handwriting setOn:YES animated:YES];
        }
        else {
            [Switch_Notes_Notes setOn:NO animated:YES];
            [Switch_Notes_Handwriting setOn:NO animated:YES];
        }
        
        return;
    }
    
    
    if([Switch_AllHelp isOn])
    {
        
        [Switch_Calender setOn:YES animated:YES];
        [Switch_Handwriting setOn:YES animated:YES];
        [Switch_Home setOn:YES animated:YES];
        [Switch_List setOn:YES animated:YES];
        [Switch_Notes setOn:YES animated:YES];
        [Switch_Project setOn:YES animated:YES];
        [Switch_Todo setOn:YES animated:YES];
    }
}

- (IBAction)switch_ValueChanged:(id)sender {
    NSLog(@"switch_ValueChanged");
    
    if (IS_TO_DO_VERSION) {
        if ([Switch_TODO_AllHelp isOn] && (![Switch_TODO_Calender isOn] || ![Switch_TODO_TODOonly isOn]) ){
            [Switch_TODO_AllHelp setOn:NO animated:YES];
        }
        
        return;
    }
    
    if (IS_LIST_VERSION) {
        if ([Switch_List_AllHelp isOn] && (![Switch_List_Calender isOn] || ![Switch_List_Listonly isOn]) ){
            [Switch_List_AllHelp setOn:NO animated:YES];
        }
        
        return;
    }

    if (IS_PROJECTS_VER) {
        if ([Switch_Projects_AllHelp isOn] && (![Switch_Projects_Calendar isOn] || ![Switch_Projects_Projects isOn]) ){
            [Switch_Projects_AllHelp setOn:NO animated:YES];
        }
        
        return;
    }
    
    if (IS_NOTES_VER) {
        if ([Switch_Notes_AllHelp isOn] && (![Switch_Notes_Notes isOn] || ![Switch_Notes_Handwriting isOn]) ){
            [Switch_Notes_AllHelp setOn:NO animated:YES];
        }
        
        return;
    }
    
    
    
    // Pro or Lite Version
    if ([Switch_AllHelp isOn] && ( ![Switch_Calender isOn] || ![Switch_Handwriting isOn] || ![Switch_Home isOn] || ![Switch_List isOn] || ![Switch_Notes isOn] || ![Switch_Notes isOn] || ![Switch_Project isOn]  || ![Switch_Todo isOn]  ) )
    {
        [Switch_AllHelp setOn:NO animated:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    Switch_List_AllHelp = nil;
    Switch_List_Calender = nil;
    Switch_List_Listonly = nil;
    Switch_TODO_Calender = nil;
    Switch_TODO_AllHelp = nil;
    Switch_TODO_TODOonly = nil;
    todoView = nil;
    todoView = nil;
    navBarToDo = nil;
    projectsView = nil;
    navBarProjects = nil;
    Switch_Projects_AllHelp = nil;
    Switch_Projects_Calendar = nil;
    Switch_Projects_Projects = nil;
    notesView = nil;
    navBarNotes = nil;
    Switch_Notes_AllHelp = nil;
    Switch_Notes_Notes = nil;
    Switch_Notes_Handwriting = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end

//
//  iSpeechViewControllerVTT.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 3/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "iSpeechViewControllerVTT.h"
#import "OrganizerAppDelegate.h"
#import "AddEditTitleViewController.h"
#import "ToDoViewController.h"


@interface iSpeechViewControllerVTT ()

@end

@implementation iSpeechViewControllerVTT


@synthesize iSpeech=_iSpeech;
@synthesize searchBox;
@synthesize parentVC;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal
{
    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) 
    {
        self.parentVC =  parentVCLocal;
    }
    return self;
}


- (void)viewDidLoad
{
	[super loadView];
    
}

- (void)viewWillAppear:(BOOL)animated 
{
	[super viewWillAppear:animated];
	//[[[OrganizerAppDelegate appDelegate] iSpeech] ISpeechSetRecognizeDone:self];
    [self addAdditionalControlsInVoicetoText];
}


#pragma mark -
#pragma mark Voice To Text Methods
#pragma mark -

//This is set for 3 seconds only, then recognizes 
/*
 - (void)recognizeButtonClicked:(id)sender {
 NSError *error = nil;
 
 if(![[[OrganizerAppDelegate appDelegate] iSpeech] ISpeechListenThenRecognizeWithTimeout:3 error:&error]) {
 if([error code] == kISpeechErrorCodeNoInternetConnection) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You are not connected to the Internet. Please double check your connection settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alert show];
 } else if([error code] == kISpeechErrorCodeNoInputAvailable) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No audio input available. Please plug in a microphone to use speech recognition." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alert show];
 }
 }
 }
 */


- (void)recognizeButtonClicked:(id)sender
{
    //Steve changed was: ISpeechSilenceDetectAfter:2.5 forDuration:2.0
	//[[[OrganizerAppDelegate appDelegate] iSpeech] ISpeechSilenceDetectAfter:2.0 forDuration:2.0];
	
	ISSpeechRecognition *recognition = [[ISSpeechRecognition alloc] init];
	
	NSError *err;
	
	[recognition setDelegate:self];
	
	if(![recognition listen:&err]) {
		NSLog(@"ERROR: %@", err);
	}
    
    //	if(![ISSpeechRecognition did  ]) 
    //    {
    //		if([error code] == kISpeechErrorCodeNoInternetConnection) 
    //        {
    //			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You are not connected to the Internet. Please double check your connection settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //			[alert show];
    //			
    //		} else if([error code] == kISpeechErrorCodeNoInputAvailable) 
    //        {
    //			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No audio input available. Please plug in a microphone to use speech recognition." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //			[alert show];
    //			
    //		}
    //	}
}
- (void)recognition:(ISSpeechRecognition *)speechRecognition didFailWithError:(NSError *)error
{
    
    if([error code] == kISpeechErrorCodeNoInternetConnection) 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You are not connected to the Internet. Please double check your connection settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    } else if([error code] == kISpeechErrorCodeNoInputAvailable) 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No audio input available. Please plug in a microphone to use speech recognition." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (void)recognition:(ISSpeechRecognition *)speechRecognition didGetRecognitionResult:(ISSpeechRecognitionResult *)result {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
	NSLog(@"Result: %@", result.text);
	
	//result.text = [result.text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[result.text substringToIndex:1] uppercaseString]];
    searchBox.text = result.text;
    [searchBox setUserInteractionEnabled:YES];
}

//- (void)ISpeechDelegateFinishedRecognize:(ISpeechSDK *)ispeech withStatus:(NSError *)status result:(NSString *)text {
//	if(status) {
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[status localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[alert show];
//		
//	}
//    
//    //Make first word Capital Letter
//    text = [text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[text substringToIndex:1] uppercaseString]];
//    searchBox.text = text;
//    [searchBox setUserInteractionEnabled:YES];
//    
//}


#pragma mark -
#pragma mark Private methods
#pragma mark -

-(void)addAdditionalControlsInVoicetoText{
	[GlobalUtility setNavBarTitle:@"Voice To Text Input" forViewController:self];
	[self.navigationItem setHidesBackButton:YES];
	
	
	searchBox = [[UITextField alloc] initWithFrame:CGRectMake(20, 50, 280, 31)];
	[searchBox setUserInteractionEnabled:NO];
	[searchBox setBorderStyle:UITextBorderStyleRoundedRect];
	[searchBox setBackgroundColor:[UIColor whiteColor]];
    [searchBox setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [searchBox setKeyboardAppearance:UIKeyboardAppearanceDefault];
    
	[self.view addSubview:searchBox];
}


-(IBAction)backBtn_Clicked:(id) sender 
{ 
	[self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)doneClicked:(id)sender {
	
	if(![searchBox.text isEqualToString:@""] && searchBox.text != nil)
    {
        
		if ([parentVC respondsToSelector:@selector(appointmentTitle)]) //Calendar Title
        {
            
            NSArray *cntrlrsArr = [self.navigationController viewControllers];
			
			AddEditTitleViewController *tempVwCntrlr = (AddEditTitleViewController*)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2];
			[[tempVwCntrlr titleTxtFld] setText:searchBox.text];
			[[tempVwCntrlr titleTxtFld] setTextColor:[UIColor blueColor]];
			[tempVwCntrlr setAppointmentTitle:searchBox.text];
			[tempVwCntrlr setAppointmentType:@""];
			[tempVwCntrlr setAppointmentType:@"V"];
			[tempVwCntrlr setAppointmentBinary:nil];
		}
        else if([parentVC respondsToSelector:@selector(todoTitle)])  //ToDo title
        {
            
            NSArray *cntrlrsArr = [self.navigationController viewControllers];
            
            ToDoViewController *tempVwCntrlr = (ToDoViewController*)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2];
            [[tempVwCntrlr titleTextField] setText:searchBox.text];
            [[tempVwCntrlr titleTextField] setTextColor:[UIColor blueColor]];
            [tempVwCntrlr setTodoTitle:searchBox.text];
            [tempVwCntrlr setTodoTitleType:@""];
            [tempVwCntrlr setTodoTitleType:@"V"];
            [tempVwCntrlr setTodoTitleBinary:nil];
			
		}
        else if([parentVC respondsToSelector:@selector(todoTitleBinary)])
        {
            NSLog(@"Anil");
            
            // Save to DB
            
            
            //***************************************** 
            //Steve added
            //Inserts new Data (user typed into Database
            //******PENDING CODE - Need to add if statement to test if "Done" button was clicked.
            //Also, if black image was clicked
            //***************************************** 
            
            TODODataObject *addToDoObj = [[TODODataObject alloc] init];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            // Sets start & End Date on current time and date
            UILabel *startsLabel = [[UILabel alloc] init];
            UILabel *endsLabel = [[UILabel alloc] init];
            NSDate *sourceDate = [NSDate date];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [startsLabel setText:[dateFormatter stringFromDate:sourceDate]];
            [endsLabel setText:[dateFormatter stringFromDate:[sourceDate dateByAddingHours:1]]];
            
            [addToDoObj setStartDateTime:startsLabel.text];
            [addToDoObj setDueDateTime:endsLabel.text];
            
            [addToDoObj setCreateDateTime:startsLabel.text];
            [addToDoObj setModifyDateTime:startsLabel.text];
            [addToDoObj setProjID:0.0]; 
            [addToDoObj setToDoProgress: 0.0];
            [addToDoObj setPriority:@""];
            [addToDoObj setIsStarred:NO]; 
            [addToDoObj setTodoTitleType:@"V"];
            NSString *dataStr = @"";
            NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
            [addToDoObj setTodoTitleBinary:data];
            [addToDoObj setTodoTitleBinaryLarge:data];
            [addToDoObj setAlert:@"None"];
            [addToDoObj setAlertSecond:@"None"];
            [addToDoObj setIsAlertOn:NO];
            
            //***********************************************************
            //***********************************************************
            //Add new Todo from inputTextField
            //***PENDING*** bring over inputTextField from another method?????
            //[addToDoObj setTodoTitle:inputTextField.text];
            [addToDoObj setTodoTitle:searchBox.text];
            //NSLog(@"TodoTitle = %@", addToDoObj.todoTitle);
            //***********************************************************
            //***********************************************************
            
            
            //   [todoDateObjArray replaceObjectAtIndex:indexPath.row withObject:addToDoObj];
            [AllInsertDataMethods insertToDoDataValues:addToDoObj];
            
            //***************************************** 
            //END  - Inserts new Data (user typed into Database
            //***************************************** 
            
            
            
            
        }
        
		
		[self.navigationController popViewControllerAnimated:YES];
        
	} 
    else 
    {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No Voice is recorded to use as title." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}



- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	//[[[OrganizerAppDelegate appDelegate] iSpeech] ISpeechSetRecognizeDone:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end

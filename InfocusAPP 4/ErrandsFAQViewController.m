//
//  ErrandsFAQViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 7/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ErrandsFAQViewController.h"

@interface ErrandsFAQViewController ()

@end

@implementation ErrandsFAQViewController
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (IS_IPHONE_5) {
        
        scrollView.frame = CGRectMake(0, 44, 320, 436 + 88);
        [scrollView setContentSize:CGSizeMake(320, 1183)];
    }
    else{
        
        scrollView.frame = CGRectMake(0, 44, 320, 436);
        [scrollView setContentSize:CGSizeMake(320, 1183)];
    }

    
    scrollView.backgroundColor = [UIColor whiteColor];
    
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

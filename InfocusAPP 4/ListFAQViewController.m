//
//  ListFAQViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListFAQViewController.h"

@interface ListFAQViewController ()

@end

@implementation ListFAQViewController
@synthesize ScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if (IS_IPHONE_5) {
        
        ScrollView.frame = CGRectMake(0, 44, 320, 436 + 88);
        [ScrollView setContentSize:CGSizeMake(320, 1037)];
    }
    else{
        
        ScrollView.frame = CGRectMake(0, 44, 320, 436 + 44);
        [ScrollView setContentSize:CGSizeMake(320, 1037)];
    }


}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

//
//  DrawingPageListView.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DrawingPageListView.h"

#import "DrawingPageTable.h"
#import "DrawingPageTableCell.h"

@interface DrawingPageListView () <DrawingPagetableDelegate>
{
    IBOutlet DrawingPageTable *_tablePageList;
}

@end

@implementation DrawingPageListView

@synthesize delegate;

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self commonInitialization:@""];
    }
    
    return self;
    
}

- (void)commonInitialization:(NSString*)str
{
    _tablePageList.hidden = NO;
    [_tablePageList setCustomDefaults:str];
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1.0;
    
    _tablePageList.delegateTable = self;
}

- (void)reloadDataAfterInsertDelete:(NSString*)str
{
    [_tablePageList reloadDataAfterInsertDelete:str];
}

#pragma mark - Delegates for Table
-(void)addPage:(int)index
{
    [delegate addDrawingPage:index];
}

-(void)deletePage:(int)index
{
    [delegate deleteDrawingPage:index];
}

- (void)selectedPage:(int)index
{
    [delegate selectedDrawingPage:index];
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

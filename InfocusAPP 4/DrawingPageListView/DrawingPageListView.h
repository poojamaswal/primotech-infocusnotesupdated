//
//  DrawingPageListView.h
//  ACEDrawingViewDemo
//

//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawingPageTable;


@protocol DrawingPageListViewDelegate <NSObject>

- (void)addDrawingPage:(int)index;
- (void)deleteDrawingPage:(int)index;
- (void)selectedDrawingPage:(int)index;


@end

@interface DrawingPageListView : UIView
{
    
}

@property(nonatomic, assign) id <DrawingPageListViewDelegate> delegate;

- (void)commonInitialization:(NSString*)str;
- (void)reloadDataAfterInsertDelete:(NSString*)str;

@end

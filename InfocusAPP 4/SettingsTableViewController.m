//
//  SettingsTableViewController.m
//  Organizer
//
//  Created by Steven Abrams on 1/29/13.
//
//

#import "SettingsTableViewController.h"
//#import "HelpViewController.h"
#import "BackupViewController.h"
//#import "Appirater.h"
#import "GTMBase64.h"
#import "BackupViewController.h"
#import "OrganizerAppDelegate.h" //Steve
#import "QuantcastMeasurement.h"
#import "PasswordProtectAppViewController.h"
#import <Social/Social.h> //Steve
#import "Flurry.h" //Steve
#import "DropBoxViewController.h"
#import "iCloudBackupViewController.h"

#define exportAlertViewTag 444 

@interface SettingsTableViewController () <SKStoreProductViewControllerDelegate>

@property (nonatomic, strong) NSString                  *iTunesGift;//Steve
@property (strong, nonatomic) IBOutlet UITableViewCell  *appThemeCell;
@property (strong, nonatomic) IBOutlet UILabel          *appVersionLabel;

- (void)faceBookButton_Clicked;
- (void)tweetPostButton_Clicked;

@end



@implementation SettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    IsAlert =YES;
    
    self.view.userInteractionEnabled = YES;
    isMenuClosed = YES; // if viewDidLoad method is called, then Menu is closed.  Used to set userEndabled to YES when Reveal button clicked (revealToggleHandle)
    
    //Find App Version Number
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    appVersionString = [NSString stringWithFormat:@"Version %@", appVersionString];
    _appVersionLabel.text = appVersionString;
    _appVersionLabel.textColor = [UIColor blackColor];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        NSLog(@"New Navigation");
        
        self.navigationController.navigationBarHidden = NO;
        self.title = @"Settings";
        self.navigationItem.rightBarButtonItem = nil;
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            //self.view.backgroundColor = [UIColor whiteColor];
            
            
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            //pooja-iPad
           if(IS_IPAD)
           {
               self.navigationItem.leftBarButtonItem = nil;
               [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
               self.navigationController.navigationBar.translucent = YES;
               self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
               self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
               self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
           }
           else{
               
               self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
               self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
               
               if (IS_IPHONE_5) {
                   self.view.frame = CGRectMake(0, 0, 320, 568);
                   //self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20);
                   
               }
               else {
                   self.view.frame = CGRectMake(0, 0, 320, 480);
                   //self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20);
                   
               }
               
               
               UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                                    style:UIBarButtonItemStylePlain
                                                                                   target:self
                                                                                   action:@selector(revealToggleHandle)];
               
               self.navigationItem.leftBarButtonItem = revealButtonItem;
               revealButtonItem.tintColor = [UIColor whiteColor];
            
           }
             self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
        }
        else{ //iOS 6
            
            _appThemeCell.hidden = YES;
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
            
            UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon_OLD.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(revealToggleHandle)];
            
            self.navigationItem.leftBarButtonItem = revealButtonItem;
            revealButtonItem.tintColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
            
            
        }
        
        
    }
    else{
        NSLog(@"Old Navigation");
        
        self.navigationController.navigationBarHidden = NO;
        
        // UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        // [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    }
    
    
    // ***************** Steve End ***************************
    
    
    
    
    
    // NSLog(@" IS_LITE_VERSION  %d", IS_LITE_VERSION);
    // NSLog(@" IS_LIST_VERSION  %d", IS_LIST_VERSION);
    // NSLog(@" IS_TO_DO_VERSION  %d", IS_TO_DO_VERSION);
    
    if(!IS_LITE_VERSION && !IS_LIST_VERSION && !IS_TO_DO_VERSION && !IS_PROJECTS_VER && !IS_NOTES_VER) //Only for Pro version
    {
        
        //ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        //ItuneURL = @"https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/giftSongsWizard?gift=1&salableAdamId=545904979&productType=C&pricingParameter=STDQ";
        
        
        ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        
        self.iTunesGift = @"https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/giftSongsWizard?gift=1&salableAdamId=545904979&productType=C&pricingParameter=STDQ&mt=8";
        
        
        AppsTarget = @"Pro";
    }
    
    //NSLog(@" iTunesURL 1 = %@", ItuneURL);
    
    if(IS_LIST_VERSION)
    {
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-checklist/id584611125?ls=1&mt=8";
        AppsTarget = @"Checklist";
    }
    if(IS_LITE_VERSION)
    {
        //Steve - ************** WRONG URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-checklist/id584611125?ls=1&mt=8";
        AppsTarget = @"Lite";
    }
    if (IS_TO_DO_VERSION) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-to-do/id591343485?ls=1&mt=8";
        AppsTarget = @"TO DO";
        
        NSLog(@" iTunesURL 2 = %@", ItuneURL);
    }
    if (IS_PROJECTS_VER) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-projects/id591634217?ls=1&mt=8";
        AppsTarget = @"Projects";
    }
    if (IS_NOTES_VER) {
        //Steve - ************** CORRECT URL ********************************
        ItuneURL =@"https://itunes.apple.com/us/app/infocus-notes/id591713931?ls=1&mt=8";
        AppsTarget = @"Notes";
    }
    
    
}


-(void) revealToggleHandle{
    NSLog(@"revealToggleHandle");
    
    if (isMenuClosed){
        self.view.userInteractionEnabled = NO;
        isMenuClosed = NO;
    }
    else{
        self.view.userInteractionEnabled = YES;
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
    [revealController revealToggle:self];
}




- (IBAction)backButton_Clicked:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

/*
 - (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
 NSLog(@"numberOfRowsInSection");
 return 44;
 }
 */


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //NSLog(@"numberOfSectionsInTableView");
    
    if (IS_LIST_VERSION || IS_NOTES_VER || IS_PROJECTS_VER || IS_TO_DO_VERSION) {
        return 5;
    }
    
    return 4; //Pro version skips last Section "Export"
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int numberOfRows;
    
    if (section == 0)
        numberOfRows = 4;
    
    else if (section == 1)
        numberOfRows = 4;
    
    else if (section == 2)
        numberOfRows = 3;
    
    //Steve - added back to remove Light Theme for now
    else if (section == 3)
        numberOfRows = 3;
    
    /*
     //Steve - removed Light Theme for now
     else if (!IS_IOS_7 && section == 3) //iOS 6 - remove row for App Theme
     numberOfRows = 3;
     
     else if (IS_IOS_7 && section == 3) //iOS 7 - needs row for App Theme
     numberOfRows = 4;
     */
    
    return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //NSLog(@"heightForHeaderInSection");
    
    
    if(IS_NOTES_VER && section == 3)
        return 1;
    
    
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"heightForRowAtIndexPath");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (IS_NOTES_VER && indexPath.section == 3) {  //removes App Settings Header Height if NOTES VER.
        return 1;
    }
    else if ( ![sharedDefaults boolForKey:@"NavigationNew"] && indexPath.section == 3 && indexPath.row == 2){
        return 0;
    }
    
    
    if ( (IS_LIST_VERSION || IS_TO_DO_VERSION || IS_PROJECTS_VER) && indexPath.section == 3 && indexPath.row == 1){
        return 0;
    }
    
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if(IS_NOTES_VER && section == 3)
        return 1;
    
    return 16;
}


-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(IS_NOTES_VER &&  section == 3)
        return [[UIView alloc] initWithFrame:CGRectZero];
    
    return nil;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    
    //Hides App Settings Header if NOTES VER.
    if (IS_NOTES_VER && section == 3) {
        view.hidden = YES;
    }
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(IS_NOTES_VER && section == 3)
        return [[UIView alloc] initWithFrame:CGRectZero];
    
    return nil;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"indexPath.section =  %d", indexPath.section);
    //NSLog(@"indexPath.row =  %d", indexPath.row);
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Hides App Settings cell if NOTES VER.
    if (IS_NOTES_VER && indexPath.section == 3) {
        cell.hidden = YES;
    }
    else if ( ![sharedDefaults boolForKey:@"NavigationNew"] && indexPath.section == 3 && indexPath.row == 2){
        cell.hidden = YES;
    }
    
    
    if ( (IS_LIST_VERSION || IS_TO_DO_VERSION || IS_PROJECTS_VER) && indexPath.section == 3 && indexPath.row == 1){
        cell.hidden = YES;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{
    
    //Hides App Settings Footer if NOTES VER.
    if (IS_NOTES_VER && section == 3) {
        view.hidden = YES;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSLog(@"********indexPath.section = %d", indexPath.section);
    //NSLog(@"********* indexPath.row)= %d", indexPath.row);
    UIStoryboard *sb;
    /*if (indexPath.section == 0 && indexPath.row == 1) { //Help clicked
    
        UIViewController *helpViewController;
        if(IS_IPAD)
        {
            sb = [UIStoryboard storyboardWithName:@"SettingsTableViewControllerIPad" bundle:nil];
            helpViewController = [sb instantiateViewControllerWithIdentifier:@"HelpViewControllerIPad"];
        }
        [self.navigationController pushViewController:helpViewController animated:NO];
    } 
  */

    if (indexPath.section == 0 && indexPath.row == 2) { //FAQ clicked
        
        UIViewController *faqViewController;
        //pooja-iPad
        if(IS_IPAD){
            sb = [UIStoryboard storyboardWithName:@"FAQViewControllerIPad" bundle:nil];
            faqViewController = [sb instantiateViewControllerWithIdentifier:@"FAQViewControllerIPad"];
            
        }
        else{
            sb = [UIStoryboard storyboardWithName:@"FAQViewController" bundle:nil];
            faqViewController = [sb instantiateViewControllerWithIdentifier:@"FAQViewController"];
        }
        
        [self presentViewController:faqViewController animated:YES completion:nil];
        
    }
    else if (indexPath.section == 0 && indexPath.row == 3) { //Support Email clicked
        if ([MFMailComposeViewController canSendMail])
        {
            
            
            if (IS_IOS_7) {
                
                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
                
            }
            else{
                UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
                [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
                
                [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            }
            
            
            
            NSArray *myArray;
            NSString *aString = @"InFocusAppInfo@gmail.com";
            myArray = [NSArray arrayWithObjects: aString, nil];
            
            //Find App Version Number
            
            NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString *subjectString = [NSString stringWithFormat:@"InFocus %@ Support, ", AppsTarget];
            subjectString = [NSString stringWithFormat:@"%@ Version %@", subjectString, appVersionString];
            
            
            MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
            [mailCntrlr setMailComposeDelegate:self];
            //[mailCntrlr setSubject:[NSString stringWithFormat:@"InFocus %@ Support", AppsTarget]]; //Steve
            [mailCntrlr setSubject:subjectString]; //Steve
            [mailCntrlr setToRecipients:myArray];
            
            
            if (IS_IOS_7) {
                mailCntrlr.navigationController.navigationBar.tintColor = [UIColor blackColor];
            }
            
            
            [self presentViewController:mailCntrlr animated:YES completion:Nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 0){ //Recommend to Friend Clicked
        
        if ([MFMailComposeViewController canSendMail])
        {
            
            
            if (IS_IOS_7) {
                
                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
                
            }
            else{
                UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
                [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
                
                [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            }
            
            
            MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
            [mailCntrlr setMailComposeDelegate:self];
            [mailCntrlr setSubject:[NSString stringWithFormat:@"I recommend InFocus %@ App!", AppsTarget]];
            
            NSString *body = [NSString stringWithFormat:@"InFocus %@ has been a valuable tool in helping me stay organized. I think you will like this App.<br><a href=\"%@\">iTunes Link</a>",AppsTarget,ItuneURL]  ;
            //<a herf=\"http:\\apple.com\">iTunes Link</a>";
            //Steve - Link to Apple Not working.
            
            
            [mailCntrlr setMessageBody:body isHTML:YES];
            [self presentViewController:mailCntrlr animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }
    }
    //***** Gift Cell remvoed from Storyboard since no longer supported
    /*
     else if (indexPath.section == 1 && indexPath.row == 1){//Gift Clicked
     
     BOOL atLeastIOS5 = [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0;
     
     if (IS_IOS_7) {
     
     UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Gift InFocus %@",AppsTarget] message:@"Your family and friends will love you for helping them stay productive and \"InFocus\"." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Gift", nil];
     [alert setTag:123];
     [alert show];
     
     }
     else if (atLeastIOS5) {
     
     UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Gift InFocus %@",AppsTarget] message:@"Sorry, your operating system is iOS 6. Apple currently does not have a way to gift an app in iOS 6 from your device.If you want to gift this app, you need to log onto your computer and gift it from iTunes."delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
     [alert show];
     }
     else{
     
     UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Gift InFocus %@",AppsTarget] message:@"Your family and friends will love you for helping them stay productive and \"InFocus\"." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Gift", nil];
     [alert setTag:123];
     [alert show];
     }
     }
     */
    else if (indexPath.section == 1 && indexPath.row == 1){ //Rate clicked
        
        /////////////////////// Steve update for iOS 7 - Opens App Store to review App /////////////////////////////////////////
        
        if(IS_LIST_VERSION){
            
            [Appirater setAppId:@"584611125"];
            [Appirater rateApp];
            
        }
        else if (IS_TO_DO_VERSION){
            
            [Appirater setAppId:@"591343485"];
            [Appirater rateApp];
            
        }
        else if (IS_PROJECTS_VER){
            
            [Appirater setAppId:@"591634217"];
            [Appirater rateApp];
            
        }
        else if (IS_NOTES_VER){
            
            [Appirater setAppId:@"591713931"];
            [Appirater rateApp];
        }
        else{ //Pro Version
            
            //[Appirater setAppId:@"545904979"];
            //[Appirater rateApp];
            
            NSString *iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
            iOSAppStoreURLFormat = [NSString stringWithFormat:iOSAppStoreURLFormat,545904979];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iOSAppStoreURLFormat]];
            
        }
        
        ////////////////////////////// Steve end ////////////////////////////////////////////
        
        
    }
    else if (indexPath.section == 1 && indexPath.row == 2){ //Share with Facebook
        
        [self faceBookButton_Clicked];
        
    }
    else if (indexPath.section == 1 && indexPath.row == 3){ //Share with Twitter
        
        [self tweetPostButton_Clicked];
    }
    else if (indexPath.section == 2 && indexPath.row == 0){ //Backup & Restore Clicked
        
        BackupViewController *backupView;
        //pooja-iPad
        if(IS_IPAD)
            backupView =[[BackupViewController alloc]initWithNibName:@"BackupViewControllerIPad" bundle:nil];
        else{
            backupView =[[BackupViewController alloc]initWithNibName:@"BackupViewController" bundle:nil];
        }
        [self presentViewController:backupView animated:YES completion:nil];
        
        
        //[self.navigationController pushViewController:backupView animated:YES];
        
    }
    
    else if (indexPath.section == 2 && indexPath.row == 1){ //DropBox Sync Clicked
        DropBoxViewController *dropBoxView;
        //pooja-iPad
        if(IS_IPAD){
            dropBoxView =[[DropBoxViewController alloc]initWithNibName:@"DropBoxViewControllerIPad" bundle:nil];
        }
        else{
            dropBoxView =[[DropBoxViewController alloc]initWithNibName:@"DropBoxViewController" bundle:nil];
        }
        [self presentViewController:dropBoxView animated:YES completion:nil];
        
    }
    
    else if (indexPath.section == 2 && indexPath.row == 2){ //iCloud Sync Clicked
        iCloudBackupViewController *cloudbackupView;
        //pooja-iPad
        if(IS_IPAD){
            
            cloudbackupView =[[iCloudBackupViewController alloc]initWithNibName:@"iCloudBackupViewControllerIPad" bundle:nil];
        }
        else
        {
            cloudbackupView =[[iCloudBackupViewController alloc]initWithNibName:@"iCloudBackupViewController" bundle:nil];
        }
        [self presentViewController:cloudbackupView animated:YES completion:nil];
        
    }
    
    else if (indexPath.section == 3 && indexPath.row == 2){ //Password Protection
        PasswordProtectAppViewController *passwordProtectAppViewController;
        //pooja-iPad
        if(IS_IPAD){
            passwordProtectAppViewController =
            [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController_iPad" bundle:[NSBundle mainBundle] withParent:self];
        }
        else{
            passwordProtectAppViewController =
            [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController" bundle:[NSBundle mainBundle] withParent:self];
        }
 
        [self.navigationController pushViewController:passwordProtectAppViewController animated:YES];
        
    }
    else if (indexPath.section == 4 && indexPath.row == 0){ //Export To Pro Clicked
        //Alok Added for takeing Database backup issue
        
        if(IsAlert){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to overwrite InFocus Pro database?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert setTag:exportAlertViewTag];
            [alert show];
            
            return;
        }
    }
    
        
}


- (void)faceBookButton_Clicked {
    
    [Flurry logEvent:@"Popup - Facebook Post"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Facebook Post" withLabels:nil];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    SLComposeViewController *fbController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        //[fbController setInitialText:@"Life is short. You too can be more productive, get your work done faster, make more money and enjoy your free time more often. All you need are the right tools. Find out more "];
        //[fbController addImage:[UIImage imageNamed:@"FaceBookDogAd2.jpg"]];
        [fbController addURL:[NSURL URLWithString:@"http://bit.ly/1fUMWWN"]];
        [self presentViewController:fbController animated:YES completion:nil];
    }
    else //Facebook is not on device
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FaceBook Login"
                                                        message:@"Your FaceBook account is not setup. Please go to your device settings and setup your FaceBook Account."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Facebook was not Setup
        [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
    }
    
    
    [sharedDefaults setBool:NO forKey:@"PostToSocialNetworks"];  //so won't ask to post again
    
}


- (void)tweetPostButton_Clicked {
    
    [Flurry logEvent:@"Popup - Twitter Post"];
    [[QuantcastMeasurement sharedInstance] logEvent:@"Popup - Twitter Post" withLabels:nil];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        [tweetSheet setInitialText:@"You too can be more productive and enjoy more free time with the right tools. See how."];
        [tweetSheet addURL:[NSURL URLWithString:@"http://bit.ly/1fUMWWN"]];
        [tweetSheet addImage:[UIImage imageNamed:@"FaceBookDogAd2.jpg"]];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else //Twitter Account Not Setup
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Login"
                                                        message:@"Your Twitter account is not setup. Please go to your device settings and setup your Twitter Account."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        [sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];  //Facebook was not Setup
        [sharedDefaults setInteger:0 forKey:@"CountUntilPostToSocialNetworks"]; //Reset to 0 so count can start over
    }
    
    
    [sharedDefaults setBool:NO forKey:@"PostToSocialNetworks"];  //so won't ask to post again
    
    
}


-(void) ExportDataToProVersion{
    //Alok Added for takeing Database backup issue
    
    //Steve commented - Alert is in didSelectRow
    /*
     if(IsAlert){
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to overwrite InFocus Pro database?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
     [alert setTag:exportAlertViewTag];
     [alert show];
     
     return;
     }
     */
    
    NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[searchPath objectAtIndex:0]  stringByAppendingString:[NSString stringWithFormat:@"/%@",@"OrganizeriOS_1.0.sqlite"]];
    NSData *fileData = [NSData dataWithContentsOfFile:path];
    
    NSString *encodedString = [GTMBase64 stringByWebSafeEncodingData:fileData padded:YES];
    
    NSString *urlString = [NSString stringWithFormat:@"myappx://localhost/importDatabase?%@", encodedString];
    NSURL *openURL = [NSURL URLWithString:urlString];
    if([[UIApplication sharedApplication] canOpenURL:openURL])
    {
        [[UIApplication sharedApplication] openURL:openURL];
    }
    else
    {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"\"InFocus Pro\" must be installed to export data to it.  Please install Infocus Pro, then export your data." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}


#pragma mark -
#pragma mark Alert
#pragma mark -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"alertView");
    
    if([alertView tag]==444 && buttonIndex ==1){
        NSLog(@"clickedButtonAtIndex = %d", buttonIndex);
        
        IsAlert =NO;
        // 455 tag set in Storyboard file for export backup
        //[(UIButton *)[self.view viewWithTag:455] sendActionsForControlEvents:UIControlEventTouchUpInside]; //Steve commented
        
        [self ExportDataToProVersion]; //Steve added
        
    }
    
    
    //Steve - Gift no longer supported so removed
    /*
     if ([alertView tag]==123 && buttonIndex==1) {
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.iTunesGift]];
     
     
     }
     if ([alertView tag]==124 && buttonIndex == 1) {
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.iTunesGift]];
     }
     */
}





#pragma mark -
#pragma mark MailComposer  Delegates
#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:Nil];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidDisappear:(BOOL)animated{
    NSLog(@"viewDidDisappear settingsTableViewController");
    
    [super viewDidDisappear:animated];
    
    
}


- (void)viewDidUnload {
    NSLog(@"viewDidUnload settingsTableViewController");
    [super viewDidUnload];
}
@end

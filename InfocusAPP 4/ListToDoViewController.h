//  ToDoParentController.h
//  Organizer
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.


#import <UIKit/UIKit.h>
#import "ProjectListViewController.h"

//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"
#import "WEPopoverController.h" //Steve added
//#import "AddToViewController.h"
#import "AddToDoAlertViewController.h"

#import "AddProjectDataObject.h"
#import "ProjectSelectionDelegate.h"

@class Cal_Todo_OptionsVC;
@class OrganizerAppDelegate;
@class HomeViewController;
@class ProjectListViewController;

//Steve changed was: @protocol ToDoParentControllerDelegate
@protocol ListToDoViewControllerDelegate
-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId todoID:(NSInteger) todoID prentID:(NSInteger)prentID; 
-(void)setStartDate:(NSString*)aStartDate todoID:(NSInteger)todoid;
-(void)setEndDate:(NSString*)aEndDate todoID:(NSInteger)todoid;
//ALOK ADDED
-(void)setAlertString:(NSString *)  alert; 
-(void)setSecondAlertString:(NSString *)  alert;
-(void)setIsAlertOn:(BOOL)_isAlertOn; 


@end


@interface ListToDoViewController: UIViewController <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate, WEPopoverControllerDelegate, UIPopoverControllerDelegate, AlertDelegate, HandWritingDelegate>

{
    
	IBOutlet UITableView  *toDoTableView;
	NSMutableArray		*todoDateObjArray;
	
	Cal_Todo_OptionsVC	*cal_Todo_OptionsVC;
	
	BOOL                fromProject;
	NSInteger		projectIDFromProjMod;
    NSString* projectName;
	int percentComplete;
    UISlider * slider;
	
	int valueId;
	int progresVal;
    BOOL checkboxSelected;
    
    TODODataObject *todoDataObj;
    
    id delegate;
    NSInteger retVal;
    NSInteger y;
    NSInteger x;
    NSInteger u;
    NSInteger v;
    
    NSString *yy;
    NSString *xx;
    NSString *uu;
    NSString *vv;
    NSString *Pname;
    NSString *Fstr;
    NSMutableArray *myArray;
    
    int         selectedIndex;
    NSInteger   firstTimeAppearing; 
    
    //Steve added 4 items
    UIColor     *myBackgroundColor;
    UIColor     *myFontColor;
    UIFont      *myFontName;
    UIColor     *myCellBackgroundColor;
    
    //Steve added
    BOOL  isKeyBoardDone;
    //Alok Added
    IBOutlet UIToolbar *bottomBar;
    IBOutlet UIBarButtonItem                *inputSelectorBarButton;
    int CountRecords;
    NSDate *tempdate;/// Aman's Added
    NSInteger indexOfmainArray;
    NSString                        *str_CategoryName;//Aman's Added
    NSInteger                       str_ProgressPercentage;//Aman's Added
    SearchBarViewController			*searchBarViewController;
    
    BOOL                            isAlertOn;
    BOOL                            isCreateViewOn;
    UITextField                     *inputTextField;
    UILocalNotification             *notification ;
    NSString                        *todoTitle;
	NSString                        *todoTitleType;
	NSData                          *todoTitleBinary;
	NSData                          *todoTitleBinaryLarge;
    NSString                        *todoTitleList;
    InputTypeSelectorViewController	*inputTypeSelectorViewController;
    
    
    int indexWhereCellIsBeingInserted;  // This saves the index where cell gets creating with pinch effect
    
    BOOL isEditingToDo;
    
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIView *helpView; //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;
   // AdWhirlView *adView;
    
    UILabel * lblPercent;
    //__unsafe_unretained IBOutlet
    UILabel *alertsLabel;//Anils Added
    //__unsafe_unretained IBOutlet
    UILabel *secondAlertsLabel;//
    
    //Steve added - for double tap cells
    int tapCount, tappedRow, doubleTapRow;
    UITableViewCell *cellVar;
    NSTimer *tapTimer;
    
    //Steve added - popup
    WEPopoverController *popoverController;
    NSInteger currentPopoverCellIndex;
    Class popoverClass;
    
    BOOL isPenIcon; //Steve added
    IBOutlet UIButton *backButton;
    
}
- (IBAction)notesButtonClicked:(id)sender;
//@property(nonatomic,retain)AdWhirlView *adView;
@property BOOL isCreateViewOn;
@property NSInteger projectIDFromProjMod;
//Steve added 4 items
@property(nonatomic,strong)UIColor *myBackgroundColor; 
@property(nonatomic,strong)UIColor *myFontColor; 
@property(nonatomic, strong) UIFont *myFontName;
@property(nonatomic, strong)  UIColor     *myCellBackgroundColor;

@property(nonatomic,assign)BOOL fromProject; 
@property(nonatomic, strong) NSMutableArray *todoDateObjArray;
@property(nonatomic, strong) UITableView *toDoTableView;
@property(assign)int  selectedButtonIndex;

@property (nonatomic, strong) NSString *todoTitle;
@property (nonatomic, strong) NSString *todoTitleType ;
@property (nonatomic, strong) NSData *todoTitleBinary;
@property (nonatomic, strong) NSData *todoTitleBinaryLarge;
@property (nonatomic, strong) NSString *todoTitleList;

//Steve added
@property(nonatomic, strong) UITableViewCell *cell;

//Steve added - popup
@property (nonatomic, retain) WEPopoverController *popoverController;

//Steve added
- (void) keyBoardDone: (id)sender;


- (IBAction)addToDoButton_Clicked:(id)sender;
- (IBAction)calendarButton_Clicked:(id)sender;
- (IBAction)homeButton_Clicked:(id)sender;
- (IBAction)ProjectButton_click:(id)sender;

- (void)updateToDoProgress:(NSInteger)TODOID ProgresVAL:(NSInteger)ProgresVAL;
-(IBAction)	project_buttonClickedNow:(id)sender;
- (IBAction)helpView_Tapped:(id)sender;  //Steve
-(IBAction)BackButton_Clicked:(id)sender;
- (void)tapTimerFired:(NSTimer *)aTimer; //Steve


- (id)initWithProjectID:(NSInteger ) projId ProjectName:(NSString *)ProjectName;
//-(IBAction) startsEnds_buttonClicked:(id)sender;
-(NSDate *)convertStringToDate:(NSString *)str_Date;
-(void)goToHandwritingView;
-(void)createAddView;

-(void) setAlertString_1:(NSString*) alertString_1 setAlertString_2:(NSString *) alertString_2 isAlertOn:(BOOL) isAlertOnOrOff; //Steve

-(IBAction)sliderValueChanged:(UISlider *)sender;
-(NSInteger)fetch_level_one:(NSInteger)projid;
//Alok Added for Bottom tab bar and others
- (IBAction)inputOptionButton_Clicked:(id)sender;
- (IBAction)optionButton_Clicked:(id)sender;
- (IBAction)searchBarButton_Clicked:(id)sender;
- (IBAction)mailBarButton_Clicked:(id)sender;
-(void)eventToSpeak;
-(BOOL)hasConnectivity;
-(NSMutableString *)stringFortext;
-(NSMutableString *)finalString;

-(NSString *)callToSpeaktext:(NSString *)callingText;
-(void) showHandwritingForiPad; //Steve - called from delegate in HandwritingViewController to refresh view so HW shows


@property(nonatomic, strong) NSMutableString        *strTextToSpeak;//Aman's Added
@property(nonatomic, strong) NSDate                 *strtDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *endDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *NSDate_dateCreated ;//Aman's Added

@property(nonatomic, strong) NSMutableArray         *arrM_finalEvents;//Aman's Added
@property(nonatomic, strong) NSMutableString        *str_todaysDate;//Aman's Added
@property(nonatomic, assign) NSMutableString        *str_SelectedButtonTitle;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrM_ForOtherTabs;


@property (nonatomic, strong) AddProjectDataObject * projDataObj;
@property (strong, nonatomic) UIImageView *blurredImageView; //Steve - Blurred Image of screenshot of

@end

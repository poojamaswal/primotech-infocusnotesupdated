//
//  AddCalenderDataObject.m
//  Organizer
//
//  Created by Nibha Aggarwal on 2/10/12.
//  Copyright 2012 A1 Technology Pvt Ltd. All rights reserved.
//

#import "AddCalenderDataObject.h"


@implementation AddCalenderDataObject
@synthesize calenderid;
@synthesize calendername;
@synthesize calenderIMGname;
@synthesize Cnamearray;
@synthesize Cidarray;
@synthesize belongstoid;
@synthesize folderImage;

-(id)initWithCalId:(NSInteger) calenderID calName:(NSString *)calenderName 
	//calFolderImage:(NSString *)calenderFolderImage
{
	if (self = [super init])
	{
		self.calenderid = calenderID;
		self.calendername = calenderName;
//		self.folderImage = calenderFolderImage;
	}
	return self;
}


@end

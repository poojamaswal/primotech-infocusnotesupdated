//
//  SelectProjectColorViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 11/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectDataObject.h"
#import "MyTreeViewCell.h"
#import "MyTreeNode.h"
#import "AddCalenderDataObject.h"
#import "AddProjectViewCantroller.h"
#import "AddCalenderViewController.h"

@interface SelectProjectColorViewCantroller : UIViewController 
{
	IBOutlet UITableView *newTable;	
	NSMutableArray *listofIMG;
	NSMutableArray *fixArray;
	UIImage *selectimgSel;
	UIButton *buttonLevelone;
    UIImageView* blockView;
     UIImageView* blockView2;
    AddProjectDataObject *PdataObj;
	AddCalenderDataObject *CdataObj;
    NSString *pImagename;
    NSMutableArray *finalfArray;
    NSInteger GValue;
	UIImageView		*tickImageView;
	id __unsafe_unretained delegate;
	
	MyTreeNode *treeNode;	
	MyTreeNode *node1;
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    
   
}
@property (unsafe_unretained) id delegate;
@property(nonatomic,assign) NSInteger GValue;
@property(nonatomic,strong) NSMutableArray *finalfArray;
@property(nonatomic,strong) NSString *pImagename;
@property(nonatomic,strong)AddProjectDataObject *PdataObj;
@property (nonatomic, strong)AddCalenderDataObject *CdataObj;
-(IBAction)cancel_click:(id)sender;
-(IBAction)save_click:(id)sender;
-(void)selectionMethod:(id)sender;



@end

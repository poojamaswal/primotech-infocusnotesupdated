//
//  SktDataObject.h
//  Organizer
//
//  Created by Naresh Chouhan on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SktDataObject : NSObject
{
	NSInteger sktid;
	NSData *sktinfo;
	
}
@property(nonatomic,retain)NSData *sktinfo;
@property(readwrite)NSInteger sktid;

@end
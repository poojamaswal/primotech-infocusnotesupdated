//
//  ViewProjectTodoCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewProjectTodoCantroller :UIViewController
{
	IBOutlet UIScrollView *prscroll;
	
	NSMutableArray *pidArray;
	NSMutableArray *pnameArray;
	NSMutableArray *ppriorityArray;
	NSMutableArray *pcolorRArray;
	NSMutableArray *pcolorGArray;
	NSMutableArray *pcolorBArray;
	NSMutableArray *plevelArray;
	NSMutableArray *pbelongtoArray;
	NSMutableArray *pcreationdateArray;
	NSMutableArray *pmodifyDateArray;
	
	NSMutableArray *pidleveloneArray;
	NSMutableArray *pnameleveloneArray;
	NSMutableArray *pcolorRleveloneArray;
	NSMutableArray *pcolorGleveloneArray;
	NSMutableArray *pcolorBleveloneArray;
	
	
	
	NSMutableArray *pidleveltwoArray;
	NSMutableArray *pnameleveltwoArray;
	NSMutableArray *pcolorRleveltwoArray;
	NSMutableArray *pcolorGleveltwoArray;
	NSMutableArray *pcolorBleveltwoArray;
	
	
	NSMutableArray *pidlevelthreeArray;
	NSMutableArray *pnamelevelthreeArray;
	NSMutableArray *pcolorRlevelthreeArray;
	NSMutableArray *pcolorGlevelthreeArray;
	NSMutableArray *pcolorBlevelthreeArray;
	
	
	int conSize;
	UIButton *btni;
	NSInteger PIDval;
	UIButton *btnlone;
	UIButton *btniii;
	UIButton *btnltwo;
	UIButton *btnlthree;
	int jval;
	int jval2;
	int jval3;
	IBOutlet UIButton *colorBTN;
	
	UIButton *dButton;
	UIButton *delButton;
	UIButton *delButton1;
	BOOL editOption;
	NSString *pname;
    IBOutlet UINavigationBar *navBar;
	
	
}

@property(nonatomic,retain)NSString *pname;
@property(nonatomic,retain)NSMutableArray *pidlevelthreeArray;
@property(nonatomic,retain)NSMutableArray *pnamelevelthreeArray;
@property(nonatomic,retain)NSMutableArray *pcolorRlevelthreeArray;
@property(nonatomic,retain)NSMutableArray *pcolorGlevelthreeArray;
@property(nonatomic,retain)NSMutableArray *pcolorBlevelthreeArray;


@property(nonatomic,retain)NSMutableArray *pidleveltwoArray;
@property(nonatomic,retain)NSMutableArray *pnameleveltwoArray;
@property(nonatomic,retain)NSMutableArray *pcolorRleveltwoArray;
@property(nonatomic,retain)NSMutableArray *pcolorGleveltwoArray;
@property(nonatomic,retain)NSMutableArray *pcolorBleveltwoArray;


@property(nonatomic,retain)NSMutableArray *pidleveloneArray;
@property(nonatomic,retain)NSMutableArray *pnameleveloneArray;
@property(nonatomic,retain)NSMutableArray *pcolorRleveloneArray;
@property(nonatomic,retain)NSMutableArray *pcolorGleveloneArray;
@property(nonatomic,retain)NSMutableArray *pcolorBleveloneArray;

@property(nonatomic,assign)NSInteger PIDval;
@property(nonatomic,retain)NSMutableArray *pidArray;
@property(nonatomic,retain)NSMutableArray *pnameArray;

@property(nonatomic,retain)NSMutableArray *ppriorityArray;
@property(nonatomic,retain)NSMutableArray *pcolorRArray;
@property(nonatomic,retain)NSMutableArray *pcolorGArray;
@property(nonatomic,retain)NSMutableArray *pcolorBArray;
@property(nonatomic,retain)NSMutableArray *plevelArray;

@property(nonatomic,retain)NSMutableArray *pbelongtoArray;

@property(nonatomic,retain)NSMutableArray *pcreationdateArray;
@property(nonatomic,retain)NSMutableArray *pmodifyDateArray;

-(IBAction)home_click:(id)sender;
-(void)FindRecored;
-(void)getLevelOne:(NSInteger)lONE;
-(void)getLevelTwo:(NSInteger)lTWO;
-(void)getLevelThree:(NSInteger)lThree;
-(IBAction)edit_option:(id)sender;
-(void)delet_btnClick:(id)sender;
-(IBAction)add_todo:(id)sender;
-(IBAction)add_calender:(id)sender;


@end

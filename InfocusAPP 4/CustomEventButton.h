//
//  CustomEventButton.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 26/07/11.
//  Copyright 2011 DAVV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomEventButton : UIButton {
	UILabel *timeLabel;
	UILabel *evntTitleLabel;
	UILabel *notesLabel;
	UIButton *titleImage;
	NSString *appTitleType;
    UIButton *whiteBoxForTitleImage; //Steve - adds a white box since new HW is transparant
}

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *evntTitleLabel;
@property (nonatomic, strong) UILabel *notesLabel;
@property (nonatomic, strong) UIButton *titleImage;
@property (nonatomic, strong) NSString *appTitleType;
@property (nonatomic, strong) UIButton *whiteBoxForTitleImage; //Steve


-(id)initWithFrame:(CGRect) frame andAppointmentType:(NSString*) appType;
-(void)setFrames;
@end

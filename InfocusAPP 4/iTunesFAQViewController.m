//
//  iTunesFAQViewController.m
//  Organizer
//
//  Created by Steven Abrams on 7/28/12.
//
//

#import "iTunesFAQViewController.h"

@interface iTunesFAQViewController ()



@end

@implementation iTunesFAQViewController
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(IS_IPAD)
    {
        scrollView.frame = CGRectMake(0, 0, 768, 968);
        [scrollView setContentSize:CGSizeMake(768, 2500)];
    }
    else
    {
        if (IS_IPHONE_5) {
            
            scrollView.frame = CGRectMake(0, 0, 320, 436 + 88);
            [scrollView setContentSize:CGSizeMake(320, 1400)];
        }
        else{
            
            scrollView.frame = CGRectMake(0, 0, 320, 436);
            [scrollView setContentSize:CGSizeMake(320,1150 )];
        }
    }
    
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

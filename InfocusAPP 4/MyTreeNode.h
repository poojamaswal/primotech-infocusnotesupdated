//
//  MyTreeNode.h
//  MyTreeViewPrototype
//
//  Created by Jon Limjap on 4/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTreeNode.h"


typedef enum 
{
    ToDo,
    NoteVal
} objectType;

@interface MyTreeNode : NSObject {
	int index;
	NSString *value;
	NSInteger ID;
	NSInteger TOTAL;
	NSInteger belongToID;
	NSInteger LevelVal;
	NSInteger TODO;
	NSInteger TODOid;
	NSInteger prntID;
    NSString *projectPriorty;
	NSInteger prjctID;
	NSInteger isTemp;
    NSString *Fimg;
	NSString *PriorityTODO;
	BOOL colorOPT;
	NSString *titleType;
	NSData *titlebinary;
	MyTreeNode *parent;
	NSMutableArray *children;
	BOOL inclusive;
	BOOL isSelected;
	NSArray *flattenedTreeCache;
	BOOL isBTN;
	BOOL isStart;
	NSInteger PROGRES;
    
    objectType _objectType;
}
@property(nonatomic,strong) NSString *projectPriorty;
@property(nonatomic)BOOL isStart;
@property(nonatomic,assign)NSInteger PROGRES;
@property(nonatomic,assign)NSInteger TOTAL;
@property(nonatomic,strong) NSString *Fimg;
@property(nonatomic,assign)NSInteger isTemp;
@property(assign)BOOL colorOPT;
@property(nonatomic,assign)NSInteger prjctID;
@property(nonatomic,assign)NSInteger prntID;
@property(nonatomic,assign)NSInteger TODOid;
@property(nonatomic,strong)NSString *titleType;
@property(nonatomic,strong)NSData *titlebinary;
@property(nonatomic,assign)BOOL isBTN;
@property(nonatomic)NSInteger belongToID;
@property(nonatomic)NSInteger LevelVal;
@property(nonatomic) NSInteger ID;
@property(nonatomic) int index;
@property(nonatomic, strong) NSString *value;
@property(nonatomic, strong) MyTreeNode *parent;
@property(nonatomic, strong, readonly) NSMutableArray *children;
@property(nonatomic) BOOL inclusive;
@property(nonatomic)BOOL isSelected;
@property(nonatomic,assign)NSInteger TODO;
@property(nonatomic,strong)NSString *PriorityTODO;
@property objectType objectType;

- (id)initWithValue:(NSString *)_value;

- (void)addChild:(MyTreeNode *)newChild;
- (NSUInteger)descendantCount;
- (NSUInteger)levelDepth;
- (NSArray *)flattenElements;
- (NSArray *)flattenElementsWithCacheRefresh:(BOOL)invalidate;
- (BOOL)isRoot;
- (BOOL)hasChildren;

@end

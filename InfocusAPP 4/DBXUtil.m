//
//  DBXUtil.m
//  DBTest
//



#import "DBXUtil.h"

@implementation DBXUtil

@synthesize remoteDir, destPathToBeUsed, origPathToBeUsed, commandProgress, fileslist;

- (id)init
{
    self = [super init];
	if (nil != self) {
       	// Set these variables before launching the app
        NSString* appKey = @"ai7p7n6bex3nodg";
        NSString* appSecret = @"pdgc4k3iu3dwb2m";
        NSString *root = kDBRootAppFolder; // Should be set to either kDBRootAppFolder or kDBRootDropbox
        
        DBSession* session = 
        [[DBSession alloc] initWithAppKey:appKey appSecret:appSecret root:root];
        session.delegate = self; // DBSessionDelegate methods allow you to handle re-authenticating
        [DBSession setSharedSession:session];
        //        [session release];
        
    }
    command = KDBXNOOP;
    return self;
}

// external interface
- (void)linkDB
{
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] link];
      
    }
}

- (void)unlinkDB
{
    if ([[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] unlinkAll];
        
       
    }
}

- (void)setDir:(NSString *)dir
{
    if (NSOrderedSame != [dir compare:@"/" options:0 range:NSMakeRange(0, 1)]) {
        self.destPathToBeUsed = [NSString stringWithFormat:@"/%@", dir];
    }
    self.destPathToBeUsed = dir;
    command = KDBXSETDIR;
    result = KDBXRCINPROGRESS;
    self.remoteDir = nil;
    [[self restClient] loadMetadata:destPathToBeUsed]; // -> delegate 
}

- (void)removePath:(NSString *)path
{
    NSString *fullpath;
    if (NSOrderedSame != [path compare:@"/" options:0 range:NSMakeRange(0, 1)]) {
        if (remoteDir != nil) {
            fullpath = [remoteDir stringByAppendingPathComponent:path];
        } else {
            result = KDBXRCPARMERR;
            NSLog(@"removePath failed. no remoteDir");
        }
    } else {
        fullpath = path;
    }
    if (fullpath != nil) {
        [[self restClient] deletePath:path];
        command = KDBXREMOVE;
        result = KDBXRCINPROGRESS;
    }
}

- (void)uploadFileTo:(NSString *)toPath from:(NSString *)fromPath
{
    NSString *fullpath;
    if (NSOrderedSame != [toPath compare:@"/" options:0 range:NSMakeRange(0, 1)]) {
        if (remoteDir != nil) {
            fullpath = [remoteDir stringByAppendingPathComponent:toPath];
        } else {
            result = KDBXRCPARMERR;
            NSLog(@"uploadFile failed. no remoteDir");
        }
    } else {
        fullpath = toPath;
    }
    if (fullpath != nil) {
        self.origPathToBeUsed = fromPath;
        self.destPathToBeUsed = fullpath;
        commandProgress = 0.0;
        [[self restClient] loadMetadata:fullpath];
        command = KDBXUPLOAD;
        result = KDBXRCINPROGRESS;
    }
}

- (void)downloadFileFrom:(NSString *)fromPath to:(NSString *)toPath
{
    NSString *fullpath;
    if (NSOrderedSame != [fromPath compare:@"/" options:0 range:NSMakeRange(0, 1)]) {
        if (remoteDir != nil) {
            fullpath = [remoteDir stringByAppendingPathComponent:fromPath];
        } else {
            result = KDBXRCPARMERR;
            NSLog(@"downloadFile failed. no remoteDir");
        }
    } else {
        fullpath = fromPath;
    }
    if (fullpath != nil) {
//        self.destPathToBeUsed = fullpath;
//        self.origPathToBeUsed = fromPath;
        commandProgress = 0.0;
        [[self restClient] loadFile:fullpath intoPath:toPath];
        command = KDBXDOWNLOAD;
        result = KDBXRCINPROGRESS;
    }
}

- (void)listFiles:(NSString *)dir
{
    NSString *fullpath;
    if (NSOrderedSame != [dir compare:@"/" options:0 range:NSMakeRange(0, 1)]) {
        if (remoteDir != nil) {
            fullpath = [remoteDir stringByAppendingPathComponent:dir];
        } else {
            result = KDBXRCPARMERR;
            NSLog(@"removePath failed. no remoteDir");
        }
    } else {
        fullpath = dir;
    }
    if (fullpath != nil) {
        [[self restClient] loadMetadata:fullpath];
        command = KDBXLISTFILES;
        result = KDBXRCINPROGRESS;
        fileslist = nil;
    }
}


// private method
- (void)uploadFile2:(NSString *)rev
{
    NSString *dir = [destPathToBeUsed stringByDeletingLastPathComponent];
    NSString *fname = [destPathToBeUsed lastPathComponent];
    [[self restClient] uploadFile:fname toPath:dir withParentRev:rev fromPath:origPathToBeUsed];
}

// DBRestCleintDelegte
- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata
{
    NSLog(@"loadMetadata");
    switch (command) {
        case KDBXSETDIR:
            if (metadata.isDirectory) {
                self.remoteDir = metadata.path;
                result = KDBXRCNOERR;
                NSLog(@"setDir completed: %@", remoteDir);
            }
            else result = KDBXRCNODIR;
            break;
        case KDBXUPLOAD:
            if (metadata.isDirectory) {
                result = KDBXRCPARMERR;
                NSLog(@"uploadFile failed. pathname is directory");
            } else {
                [self uploadFile2:metadata.rev];
            }
            break;
        case KDBXLISTFILES:
            fileslist = metadata;
            result = KDBXRCNOERR;
            break;
    }
}

- (void)restClient:(DBRestClient*)client metadataUnchangedAtPath:(NSString*)path
{
    NSLog(@"metadataUnchangedAtPath");
}

- (void)restClient:(DBRestClient*)client loadMetadataFailedWithError:(NSError*)error
{
    NSLog(@"loadMetadataFailed: %@", error);
    
    switch (command) {
        case KDBXSETDIR:
            [[self restClient] createFolder:destPathToBeUsed];
            break;
        case KDBXUPLOAD:
            [self uploadFile2:nil];
            break;
        default:
            result = KDBXRCERROR;
            break;
    }
}

- (void)restClient:(DBRestClient*)client createdFolder:(DBMetadata*)folder {
// Folder is the metadata for the newly created folder
    result = KDBXRCNOERR;
    self.remoteDir = folder.path;
    NSLog(@"createdFolder");
}

- (void)restClient:(DBRestClient*)client createFolderFailedWithError:(NSError*)error {
// [error userInfo] contains the root and path
    NSLog(@"createFolderFailed: %@", error);
    result = KDBXRCNODIR;
}

- (void)restClient:(DBRestClient*)client deletedPath:(NSString *)path {
// Folder is the metadata for the newly created folder
    NSLog(@"deletedPath");
    result = KDBXRCNOERR;
}

- (void)restClient:(DBRestClient*)client deletePathFailedWithError:(NSError*)error {
// [error userInfo] contains the root and path
    NSLog(@"deletePathFailed: %@",error);
    result = KDBXRCERROR;
}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath from:(NSString*)srcPath 
          metadata:(DBMetadata*)metadata
{
    NSLog(@"UploadedFile");
   
    result = KDBXRCNOERR;
    
    [self.delegate sendIndicatorStopAnimationNotification:@"Uploaded File"];
}

- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath
{
    commandProgress = progress;
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error
{
// [error userInfo] contains the sourcePath
    NSLog(@"UploadFailed: %@", error);
    result = KDBXRCERROR;
    [self.delegate sendIndicatorStopAnimationNotification:@"Upload Failed"];


}

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)destPath
{
    NSLog(@"loadedFile");
    result = KDBXRCNOERR;
    [self.delegate sendIndicatorStopAnimationNotification:@"Downloaded File"];
}

- (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath
{
    commandProgress = progress;
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error
{
// [error userInfo] contains the destinationPath
    NSLog(@"loadFileFailed: %@", error);
    result = KDBXRCERROR;
     [self.delegate sendIndicatorStopAnimationNotification:@"File has been deleted"];
}

// DBSessionDelegate
- (void)sessionDidReceiveAuthorizationFailure:(DBSession *)session userId:(NSString *)userId
{
    NSLog(@"Autorization Failure:%@",userId);
     [self.delegate sendIndicatorStopAnimationNotification:@"Loading Failed"];
}

// getter for restClient
- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

@end

//
//  PriorityPickerView.m
//  Organizer
//
//  Created by Naresh Chauhan on 12/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PriorityPickerView.h"
#import "ToDoViewController.h"


@implementation PriorityPickerView
@synthesize doublePicker;
@synthesize fillingTypes;
@synthesize breadTypes,delegate ;
@synthesize prioritySwitch; //Steve
//@synthesize priorityLabel_Remove4Char = _priorityLabel_Remove4Char;



// The designated initializer. Override to perform setup that is required before the view is loaded.
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil setNewPriority:(NSString*) valueone{
    //NSLog(@"PriorityPicker initWithNibName ");
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

		NSString *new= valueone;
		int n=[new length];
        
        if(![new isEqualToString:@"None"])
        {
		first = [new substringWithRange: NSMakeRange(0,1)];
		second = [new substringWithRange: NSMakeRange(1,n-1)];
		}
       //  NSLog(@"first = %@ second = %@",first,second);
                
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
   // NSLog(@"PriorityPicker viewDidLoad ");
    
    NSArray *fillingArray = [[NSArray alloc] initWithObjects:@"None",@"A",@"B",@"C",nil]; //Steve changed
    
	self.fillingTypes = fillingArray;
	
	NSArray *breadArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil]; 
    
	self.breadTypes = breadArray;
    
	[super viewDidLoad];

}


-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"PriorityPicker viewWillAppear ");
    
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        priorityFixedLabel.frame = CGRectMake(20, 92 - 44, 74, 24);
        priorityLabel.frame = CGRectMake(96, 95 - 44, 72, 21);
        prioritySwitch.frame = CGRectMake(223, 92 - 44, 79, 27);
        doublePicker.frame = CGRectMake(0, 162 - 44, 320, 216);
        
        self.title =@"Add Priority";
        
        [self.navigationController.navigationBar addSubview:doneButton];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        //Back Button
        /*
        UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
        UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
        [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [backButtonNewNav addTarget:self action:@selector(Clicl_Cancel:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
        
        [[self navigationItem] setLeftBarButtonItem:barButton];
        */
        
    }
    else{
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    
    
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    if([first length]>0){
        [doublePicker selectRow:[fillingTypes indexOfObject:first] inComponent:0 animated:YES];
    }
    else {
        //Switcher set to OFF
        [prioritySwitch setOn:NO];
         
        //Shows Priority: None
        [doublePicker selectRow:0 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
    }
    
    
    if([second length]>0)
        [doublePicker selectRow:[breadTypes indexOfObject:second] inComponent:1 animated:YES];
    
    //NSLog(@"first = %@ second = %@",first,second);
    
    NSString *priorityLabelText = [first stringByAppendingString:second];
    
    if (first == NULL) priorityLabel.text = @"None";
    else priorityLabel.text = priorityLabelText;
    
    priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    
    //NSLog(@"priorityLabel.text = %@",priorityLabel.text);
    
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_TodoAddPriority"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];

        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = YES;
        }
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_TodoAddPriority"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
        
    }


}


-(void)viewDidAppear:(BOOL)animated{

}

- (IBAction)PrioritySwitchClicked:(id)sender {
    
    UISwitch *tSwitch = sender;
	
	if ([tSwitch isOn]) {
		//shows Priority: A 1
        [doublePicker selectRow:1 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
        
        NSInteger breadRow = [doublePicker selectedRowInComponent:kBreadComponent];
        NSInteger fillingRow = [doublePicker selectedRowInComponent:kFillingComponent];
        
        NSString *bread = [breadTypes objectAtIndex:breadRow];
        NSString *filling = [fillingTypes objectAtIndex:fillingRow];
        
        priorityLabel.text = [filling stringByAppendingString:bread];
        
    }
    else{
        //Shows Priority: None
        [doublePicker selectRow:0 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
        
        priorityLabel.text = @"None";
    }
    
}

-(IBAction)Clicl_Cancel:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES]; 
}


-(IBAction)Click_Save:(id)sender
{
    //NSLog(@"PriorityPicker Click_Save");
	
	NSInteger breadRow = [doublePicker selectedRowInComponent:
	                      kBreadComponent];
	NSInteger fillingRow = [doublePicker selectedRowInComponent:
	                        kFillingComponent];
	NSString *bread = [breadTypes objectAtIndex:breadRow];
	NSString *filling = [fillingTypes objectAtIndex:fillingRow];

	
    if ([filling isEqual: @"None"]) {
        bread = @"";
    }

    [[self delegate] setPriorityNew:bread priority2:filling];
    
	[self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if (component == kBreadComponent)
		return[self.breadTypes count];
	return[self.fillingTypes count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    //NSLog(@"didSelectRow");
    
    NSInteger breadRow = [doublePicker selectedRowInComponent:
	                      kBreadComponent];
	NSInteger fillingRow = [doublePicker selectedRowInComponent:
	                        kFillingComponent];
	NSString *bread = [breadTypes objectAtIndex:breadRow];
	NSString *filling = [fillingTypes objectAtIndex:fillingRow];


    if (component == kFillingComponent)
    {
       // NSLog(@"row = %d ", row);
        //NSLog(@"component = %d ", component);
        
        if (row == 0) 
        {
            //if row == 0 then it's None and should only show "None" as label
            priorityLabel.text = filling;
            priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            
            //"None" Switch is OFF
            [prioritySwitch setOn:NO animated:YES];
        }
        
        if ((row > 0))
        {
            priorityLabel.text = [filling stringByAppendingString:bread];
            priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            
            [prioritySwitch setOn:YES animated:YES];
        }
        
    }
    else
    {
        priorityLabel.text = [filling stringByAppendingString:bread];
        
    }

    if ([priorityLabel.text length] > 2)
    {
        
        //remove the first 4 characters to avoid things like "None1", should be "None"
        NSMutableString *tempString = [NSMutableString stringWithString: priorityLabel.text];
        NSString *priorityLabel_Remove4Char = [tempString substringWithRange: NSMakeRange (0, 4)];
        
        if ([priorityLabel_Remove4Char isEqualToString:@"None"])
        {
            
            //if 1st 4 char are "None", then should label is "None". We don't want the priority number
            priorityLabel.text = priorityLabel_Remove4Char;
        }
    }


}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    //NSLog(@"pickerView");
    
	if (component == kBreadComponent)
        return [self.breadTypes objectAtIndex:row];
    
	return [self.fillingTypes objectAtIndex:row];
}




- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
  
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [backButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    prioritySwitch = nil;
    priorityLabel = nil;
    backButton = nil;
    doneButton = nil;
    priorityFixedLabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

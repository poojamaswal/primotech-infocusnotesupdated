//
//  AppNavigationStyleViewController.m
//  Organizer
//
//  Created by Steven Abrams on 3/4/13.
//
//

#import "AppNavigationStyleViewController.h"

@interface AppNavigationStyleViewController ()

- (IBAction)newStyle_ValueDidChange:(UISwitch *)sender;
- (IBAction)oldStyle_ValueDidChange:(UISwitch *)sender;


@end

@implementation AppNavigationStyleViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [newStyle setOn:YES];
        [oldStyle setOn:NO];
    }
    else {
        [newStyle setOn:NO];
        [oldStyle setOn:YES];
    }

    isOldStyleSelected = NO;
    isNewStyleSelected = NO;
    exitAppView.hidden = YES;
    
}

- (IBAction)newStyle_ValueDidChange:(UISwitch *)sender {
    
    if (IS_IOS_5) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The New Navigation Style only works with iOS 6 or greater.  Please update your operating system, then you can switch Navigation Styles" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        isNewStyleSelected = NO;
        [newStyle setOn:NO animated:YES];
        [oldStyle setOn:YES animated:YES];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to change the naviagtion style? By changing the navigation style this app will exit and you will need to restart the app." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];
        
        isNewStyleSelected = YES;
        [newStyle setOn:YES animated:YES];
        [oldStyle setOn:NO animated:YES];
    }
    


}

- (IBAction)oldStyle_ValueDidChange:(UISwitch *)sender {
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to change the naviagtion style? By changing the navigation style this app will exit and you will need to restart the app." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];

    isOldStyleSelected = YES;
    [newStyle setOn:NO animated:YES];
    [oldStyle setOn:YES animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        if (isNewStyleSelected) {
            [newStyle setOn:NO animated:YES];
            [oldStyle setOn:YES animated:YES];
        }
        else if (isOldStyleSelected){
            [newStyle setOn:YES animated:YES];
            [oldStyle setOn:NO animated:YES];
        }
    }
    
    if(buttonIndex==1)
    {
        if (isNewStyleSelected) {
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            [sharedDefaults setBool:YES forKey:@"NavigationNew"];
            [sharedDefaults synchronize];
            
            [newStyle setOn:YES animated:YES];
            [oldStyle setOn:NO animated:YES];
    
            [self animateScreenToExit];
        }
        else if (isOldStyleSelected){
            
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            [sharedDefaults setBool:NO forKey:@"NavigationNew"];
            [sharedDefaults synchronize];
            
            [newStyle setOn:NO animated:YES];
            [oldStyle setOn:YES animated:YES];
            
            [self animateScreenToExit];
        }
        
    }
}

-(void) animateScreenToExit{
    
    exitAppView = [[UIView alloc] initWithFrame:self.view.bounds];
    exitAppView.hidden = NO;
    exitAppView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:exitAppView];
    
    //UILabel *fadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
    UILabel *fadingLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
    fadingLabel.text=@"Exiting App, Please Restart";
    fadingLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
    fadingLabel.backgroundColor = [UIColor clearColor];
    fadingLabel.textColor = [UIColor whiteColor];
    fadingLabel.textAlignment = UITextAlignmentCenter;
    [fadingLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [exitAppView addSubview:fadingLabel];

    [UIView beginAnimations:nil context:nil];
    exitAppView.alpha = 0.30f;
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:2.0];
    [exitAppView  setAlpha:1.0];
    [UIView commitAnimations];
    
     [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(abortApp) userInfo:nil repeats:NO];
}

-(void) abortApp{
    
       abort();
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    newStyle = nil;
    oldStyle = nil;
    exitAppView = nil;
    [super viewDidUnload];
}


@end

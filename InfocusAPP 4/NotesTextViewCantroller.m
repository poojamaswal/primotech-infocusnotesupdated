//
//  NotesTextViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesTextViewCantroller.h"
#import "AddNewNotesViewCantroller.h"


@implementation NotesTextViewCantroller
@synthesize previousNotes,notesTxtVw;
@synthesize delegate;
#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}


//Steve added - to edit existing text
-(id)initWithDataString:(NSString *)text forObjectAtIndex:(int)index isEdit:(BOOL)isEdit {
    //NSLog(@"initWithDataString");
    
	if (self = [super initWithNibName:@"NotesTextViewCantroller" bundle:[NSBundle mainBundle]]){
        previousNotes = text;
        indexForTextArray = index;
        isTextBeingEdited = isEdit;
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    //NSLog(@"viewDidLoad");
    
	[notesTxtVw.layer setCornerRadius:10.0];
	notesTxtVw.layer.masksToBounds = YES;
	notesTxtVw.layer.borderColor = [UIColor lightGrayColor].CGColor;  //Steve changed to light gray
	notesTxtVw.layer.borderWidth = 0.5;
	[notesTxtVw becomeFirstResponder];
    notesTxtVw.text =  [previousNotes isEqualToString:@"None"] ? @"Notes" : previousNotes;
    
    //NSLog(@"notesTxtVw.text =  %@", notesTxtVw.text);
    
}


-(void)viewWillAppear:(BOOL)animated
{
     //NSLog(@"viewWillAppear");
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        self.title =@"Note Text";
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            
            self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
            //self.view.backgroundColor = [UIColor whiteColor];
            
            
            //done Button
            doneButton.hidden = YES;
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(doneClicked:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                //self.navigationController.navigationBar.tintColor = [UIColor yellowColor];
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                //self.navigationController.navigationBar.tintColor = [UIColor yellowColor];
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
              self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            doneButton_iOS7.tintColor = [UIColor whiteColor];
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////

            if (IS_IPAD)
            {
                notesTxtVw.frame = CGRectMake((self.view.frame.size.width-350)/2, 51 - 44 + 64, 350, 270);

            }else
            {
                if (IS_IPHONE_5)
                    notesTxtVw.frame = CGRectMake(7, 51 - 44 + 64, 306, 270);
                else
                    notesTxtVw.frame = CGRectMake(7, 51 - 44 + 64, 306, 190);
                
                
                
                //Make adjustments for iOS 8 new keyboard
                if (IS_IOS_8) {
                    
                    if (IS_IPHONE_5) //works with iPhone 6
                        notesTxtVw.frame = CGRectMake(7, 51 - 44 + 64, 306, 270 - 38);
                    else
                        notesTxtVw.frame = CGRectMake(7, 51 - 44 + 64, 306, 190 - 38);
                }
                

            }
            
            
            
        }
        else{ //iOS 6
            
            
            [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
            
            if (IS_IPHONE_5)
                notesTxtVw.frame = CGRectMake(7, 51 - 44, 306, 270);
            else
                notesTxtVw.frame = CGRectMake(7, 51 - 44, 306, 190);

        }
        
        
        
        
            
    }
    else{
        
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

        
        if (IS_IPHONE_5)
            notesTxtVw.frame = CGRectMake(7, 51, 306, 270);
        else
            notesTxtVw.frame = CGRectMake(7, 51, 306, 190);
        
    }
    
    
    // ***************** Steve End ***************************
    
    

}



-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneClicked:(id)sender {
     NSLog(@"doneClicked");
    
	[notesTxtVw resignFirstResponder];
    
	if ([notesTxtVw.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Empty Notes..!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
	}

    
    //Steve - fixed for iOS 7
    if([self.noteTextDelegate respondsToSelector:@selector(setTextToArray:forIndexForTextArray:isTextBeingEdited:)]) {
        
        if (isTextBeingEdited) {
            
            [self.noteTextDelegate setTextToArray:notesTxtVw.text forIndexForTextArray:indexForTextArray isTextBeingEdited:YES];
            
        }
        else{
            
            [self.noteTextDelegate setTextToArray:notesTxtVw.text forIndexForTextArray:indexForTextArray isTextBeingEdited:NO];
            
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];

    
    //Steve commented
    /*
     if (isTextBeingEdited) {
     
     [[self.delegate temptxtArray] replaceObjectAtIndex: indexForTextArray withObject:notesTxtVw.text];
     }
     else
     [[self.delegate temptxtArray] addObject:notesTxtVw.text];
     
     [self.navigationController popViewControllerAnimated:YES];
     */
    
    
}

#pragma mark -
#pragma mark Memory Management
#pragma mark 

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing");
    
	textView.text = [textView.text isEqualToString:@"Notes"] ? @"" :textView.text;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [doneButton removeFromSuperview];
        [backButton removeFromSuperview];
        
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    backButton = nil;
    doneButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end




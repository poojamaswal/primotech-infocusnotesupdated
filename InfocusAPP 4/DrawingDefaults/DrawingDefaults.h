
#import <Foundation/Foundation.h>

@interface DrawingDefaults : NSObject

@property (nonatomic, retain) NSMutableDictionary *dictCollorPalletValues;
@property (nonatomic, assign) BOOL isEraserSelected;


@property (nonatomic, strong) NSMutableArray *pathArray;
@property (nonatomic, strong) NSMutableArray *bufferArray;

@property (nonatomic, strong) NSMutableDictionary *dictResizableData;
@property (nonatomic, strong) NSMutableDictionary *dictResizableSticky;
@property (nonatomic, readwrite) int intNotesSubNotesCounter;
@property (nonatomic, readwrite) int intNotesSubNotesCount;
@property (nonatomic, strong) NSString* strNoteName;
@property (nonatomic, strong) NSDate* dateStored;


+ (DrawingDefaults*)sharedObject;
@end

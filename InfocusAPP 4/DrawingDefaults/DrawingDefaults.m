

#import "DrawingDefaults.h"

@implementation DrawingDefaults

@synthesize dictCollorPalletValues;
@synthesize isEraserSelected;
@synthesize pathArray;
@synthesize bufferArray;
@synthesize intNotesSubNotesCounter;
@synthesize intNotesSubNotesCount;
@synthesize strNoteName;
@synthesize dateStored;



__strong static DrawingDefaults *drawingDefaults;

+ (DrawingDefaults*)sharedObject {
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        drawingDefaults = [[self alloc] init];
    });
    return drawingDefaults;
}




@end

//
//  NotesTextViewCantroller.h
//  Organizer
//
//  Created by Naresh Chouhan on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesDataObject.h"
//#import "AddNewNotesViewCantroller.h"


@protocol NoteTextDelegate <NSObject> //Steve

@optional

-(void)setTextToArray:(NSString *) textString forIndexForTextArray:(int) indexForArray isTextBeingEdited:(BOOL) isTextEdited;//Steve

@end



@interface NotesTextViewCantroller : UIViewController<UITextViewDelegate> {
	IBOutlet UITextView	*notesTxtVw;
	NSString *previousNotes;
	id delegate;
    IBOutlet UINavigationBar *navBar;
    
    BOOL isTextBeingEdited;
    int  indexForTextArray;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
       
}


@property(nonatomic,strong) NSString *previousNotes;
@property(nonatomic,strong) UITextView	*notesTxtVw;
@property (strong) id delegate;

@property(nonatomic, weak) id<NoteTextDelegate> noteTextDelegate; //Steve


-(IBAction)doneClicked:(id)sender;
-(IBAction)backClicked:(id)sender;

-(id)initWithDataString:(NSString *)text forObjectAtIndex:(int)index isEdit:(BOOL)isEdit; //Steve added

@end


	
	
	
	




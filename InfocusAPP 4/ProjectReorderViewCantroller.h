//
//  ProjectReorderViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectDataObject.h"


@interface ProjectReorderViewCantroller : UIViewController 
{
	IBOutlet UITableView *recordTable;
	NSMutableArray *TempProjectName;
	NSMutableArray *tempProjectPriority;
	NSMutableArray *TempProjectID;
	NSMutableArray *ProjectArrayEdit;
	
	NSString *temp;
	AddProjectDataObject *tempprojectdataobj;
	int myOption;

}
@property(nonatomic,assign)int myOption;
@property(nonatomic,strong)AddProjectDataObject *tempprojectdataobj;
@property(nonatomic,strong)NSMutableArray *TempProjectID;
@property(nonatomic,strong)NSString *temp;
@property(nonatomic,strong)NSMutableArray *TempProjectName;
@property(nonatomic,strong)NSMutableArray *tempProjectPriority;


-(IBAction)editproject_click:(id)sender;

-(IBAction)cancel_click;
-(void)getalldata;
-(IBAction)done_btnClick:(id)sender;
-(IBAction)add_btnClick:(id)sender;
@end

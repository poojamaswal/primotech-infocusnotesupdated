//
//  SelectFolderOptionViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "SelectFolderOptionViewCantroller.h"
#import "AddProjectDataObject.h"
#import "OrganizerAppDelegate.h"
#import "MyTreeViewCell.h"
#import "AddProjectViewCantroller.h"
#import "QuickViewSubfolder.h"

@implementation SelectFolderOptionViewCantroller
@synthesize SgetLevelRootArray,SpidleveloneArray,SpidleveltwoArray,SpidlevelthreeArray;
@synthesize SpidleveloneNameArray,SgetLevelRootNameArray,SpidleveltwoNameArray,SpidlevelthreeNameArray;
@synthesize SgetLevelRootLevelArray,SpidleveloneLevelArray,SpidleveltwoLevelArray;
@synthesize SpidlevelthreeLevelArray;
@synthesize SgetLevelRootBelongsToArray;
@synthesize SpidleveloneBelongsToArray;
@synthesize SpidleveltwoBelongsTolArray;
@synthesize SpidlevelthreeBelongsToArray;
@synthesize sentLvalue,sentBvalue,sentRootValue,Sql,senPID,optn,fname;
@synthesize newpriorty,sql,SaveIT,SgetRootTODOArray,buttonLevelone,getRootVal;
@synthesize SgetLevelRootimageArray,SgetLevelOneimageArray,SgetLevelTwoimageArray,SgetLevelThreetimageArray,fixArray;
@synthesize getfval,lastProjectID,getViewValue;
@synthesize cancelButton; //Steve
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

//-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withProjectID:(NSInteger) projectId{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization.
//		lastProjectID = projectId;
//    }
//    return self;
//}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	 //foldetTable.separatorColor = [UIColor blackColor];
	
	SgetLevelRootLevelArray=[[NSMutableArray alloc]init];
	SpidleveloneLevelArray=[[NSMutableArray alloc]init];
	SpidleveltwoLevelArray=[[NSMutableArray alloc]init];
	
	SgetLevelRootArray=[[NSMutableArray alloc]init];
	SgetLevelRootNameArray=[[NSMutableArray alloc]init];
	
	SpidleveloneArray = [[NSMutableArray alloc]init];
	SpidleveloneNameArray=[[NSMutableArray alloc]init];
	
	SpidleveltwoArray=[[NSMutableArray alloc]init];
	SpidleveltwoNameArray=[[NSMutableArray alloc]init];
	
    SpidlevelthreeNameArray=[[NSMutableArray alloc]init];
	SpidlevelthreeArray=[[NSMutableArray alloc]init];
	
	SpidlevelthreeArray=[[NSMutableArray alloc]init];
	SpidlevelthreeNameArray=[[NSMutableArray alloc]init];
	SpidlevelthreeLevelArray=[[NSMutableArray alloc]init];
	
	SgetLevelRootBelongsToArray=[[NSMutableArray alloc]init];
	SpidleveloneBelongsToArray=[[NSMutableArray alloc]init];
	SpidleveltwoBelongsTolArray=[[NSMutableArray alloc]init];
	SpidlevelthreeBelongsToArray=[[NSMutableArray alloc]init];
	
	SgetLevelRootimageArray=[[NSMutableArray alloc]init];
	SgetLevelOneimageArray=[[NSMutableArray alloc]init];

	SgetLevelTwoimageArray=[[NSMutableArray alloc]init];

	SgetLevelThreetimageArray=[[NSMutableArray alloc]init];
	fixArray=[[NSMutableArray alloc]init];

}

-(void)viewWillAppear:(BOOL)animated
{
	
	[super viewWillAppear:YES];
    
    
    if (IS_IOS_7) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        self.navigationController.navigationBarHidden = NO;
        navBar.hidden = YES;
        cancelButton.hidden = YES;
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.view.backgroundColor = [UIColor whiteColor];
        foldetTable.backgroundColor = [UIColor whiteColor];

        
        ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        
        if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                    [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
            
        }
        else{ //Dark Theme
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            
        }
        
         self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        if (IS_IPHONE_5)
            foldetTable.frame = CGRectMake(0, 0, 320, 416 + 88 + 64);
        else
            foldetTable.frame = CGRectMake(0, 0, 320, 416 + 64);
        
        
        
        self.title =@"Select Folder";
        
    }
    else{ //iOS 6
        
        self.navigationController.navigationBarHidden = YES;
        self.title =@"Select Folder";
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        
        [foldetTable setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    }

	
	[self FindRecored];
	
	treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
	for(int i=0;i<[SgetLevelRootArray count];i++)
	{
	//NSLog(@"%d",[[getLevelRootBelongsToArray objectAtIndex:i]intValue]);
	node1 = [[MyTreeNode alloc] initWithValue:[SgetLevelRootNameArray objectAtIndex:i]];
	node1.ID=[[SgetLevelRootArray objectAtIndex:i]intValue];
	node1.belongToID=[[SgetLevelRootBelongsToArray objectAtIndex:i]intValue];
	node1.LevelVal=[[SgetLevelRootLevelArray objectAtIndex:i]intValue];
	node1.prntID=[[SgetLevelRootArray objectAtIndex:i]intValue];
	node1.prjctID=[[SgetLevelRootArray objectAtIndex:i]intValue];
	node1.Fimg=[SgetLevelRootimageArray objectAtIndex:i];
	[treeNode addChild:node1];
		
	//NSLog(@"%@",getLevelRootArray);	
	[self getLevelOne:[[SgetLevelRootArray objectAtIndex:i]intValue]];
	 
		for(int j=0;j<[SpidleveloneArray count];j++)
		{
			node2 = [[MyTreeNode alloc] initWithValue:[SpidleveloneNameArray objectAtIndex:j]];	
			node2.ID = [[SpidleveloneArray objectAtIndex:j]intValue];
			node2.LevelVal=[[SpidleveloneLevelArray objectAtIndex:j]intValue];
			node2.belongToID=[[SpidleveloneBelongsToArray objectAtIndex:j]intValue];
			node2.prntID=[[SgetLevelRootArray objectAtIndex:i]intValue];
			node2.prjctID=[[SpidleveloneArray objectAtIndex:j]intValue];
			node2.Fimg=[SgetLevelOneimageArray objectAtIndex:j];

			[ node1 addChild:node2]; 
			[self getLevelTwo: [[SpidleveloneArray objectAtIndex:j]intValue]];
			{
				for(int k=0;k<[SpidleveltwoArray count];k++)
				{
					
				node3 = [[MyTreeNode alloc] initWithValue:[SpidleveltwoNameArray objectAtIndex:k]];	
				node3.ID=[[SpidleveltwoArray objectAtIndex:k]intValue];
				node3.LevelVal=[[SpidleveltwoLevelArray objectAtIndex:k]intValue];
				node3.belongToID=[[SpidleveltwoBelongsTolArray objectAtIndex:k]intValue];
				node3.prntID=[[SgetLevelRootArray objectAtIndex:i]intValue];
				node3.prjctID=[[SpidleveltwoArray objectAtIndex:k]intValue];
				node3.Fimg=[SgetLevelTwoimageArray objectAtIndex:k];

				[node2 addChild:node3];
				[self getLevelThree:[[SpidleveltwoArray objectAtIndex:k]intValue]];
					for(int l=0;l<[SpidlevelthreeArray count];l++)
					{
						node4=[[MyTreeNode alloc] initWithValue:[SpidlevelthreeNameArray objectAtIndex:l]];
						node4.ID=[[SpidlevelthreeArray objectAtIndex:l]intValue];
						node4.LevelVal=[[SpidlevelthreeLevelArray objectAtIndex:l]intValue];
						node4.belongToID=[[SpidlevelthreeBelongsToArray objectAtIndex:l]intValue];
						node4.prntID=[[SgetLevelRootArray objectAtIndex:i]intValue];
						node4.prjctID=[[SpidlevelthreeArray objectAtIndex:l]intValue];
						node4.Fimg=[SgetLevelThreetimageArray objectAtIndex:l];

						[node3 addChild:node4];
					}
					 f4=[SpidlevelthreeArray count];
	
				}
					f3=[SpidleveltwoArray count];
			}
					f2=[SpidleveloneArray count];
		
		}
					f1=[SgetLevelRootLevelArray count];
			
	}
	
}
		
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
#pragma mark UserMethods

-(IBAction)Cancel_btn:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


-(void)FindRecored
{

    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where level =%d and projectId=%d ORDER BY projectPriority,projectName ASC",0,getRootVal];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
	file://localhost/Users/naresh.chauhan/Documents/Organizer/AddNewSubfolderViewCantroller.m	{
			[SgetLevelRootArray removeAllObjects];
			[SgetLevelRootNameArray removeAllObjects];
			[SgetLevelRootimageArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobj = [[AddProjectDataObject alloc]init];
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[SgetLevelRootArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[SgetLevelRootNameArray addObject:projectdataobj.projectname];
				[projectdataobj setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
								[SgetLevelRootLevelArray addObject:[NSNumber numberWithInt:projectdataobj.level]];
				
				[SgetLevelRootLevelArray addObject:[NSNumber numberWithInt:projectdataobj.level]];
				
				[projectdataobj setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[SgetLevelRootBelongsToArray addObject:[NSNumber numberWithInt:projectdataobj.belongstoid]];
				[projectdataobj setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[SgetLevelRootimageArray addObject:projectdataobj.projectIMGname];
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		
	}
//}

-(void)getLevelOne:(NSInteger)lONE
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lONE,1];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[SpidleveloneArray  removeAllObjects];
			[SpidleveloneNameArray  removeAllObjects];
			[SpidleveloneBelongsToArray removeAllObjects]; 
			[SgetLevelOneimageArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjone = [[AddProjectDataObject alloc]init];
				[projectdataobjone setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[SpidleveloneArray addObject:[NSNumber numberWithInt:projectdataobjone.projectid]];
				
				[projectdataobjone setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[SpidleveloneNameArray addObject:projectdataobjone.projectname];
				
				[projectdataobjone setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
								[SpidleveloneLevelArray addObject:[NSNumber numberWithInt:projectdataobjone.level]];
				[projectdataobjone setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[SpidleveloneBelongsToArray addObject:[NSNumber numberWithInt:projectdataobjone.belongstoid]];
				
				[projectdataobjone setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[SgetLevelOneimageArray addObject:projectdataobjone.projectIMGname];
				
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
}

-(void)getLevelTwo:(NSInteger)lTWO
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lTWO,2];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[SpidleveltwoArray  removeAllObjects];
			[SpidleveltwoNameArray removeAllObjects];
			[SgetLevelTwoimageArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjtwo = [[AddProjectDataObject alloc]init];
				[projectdataobjtwo setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[SpidleveltwoArray addObject:[NSNumber numberWithInt:projectdataobjtwo.projectid]];
				
				[projectdataobjtwo setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[SpidleveltwoNameArray addObject:projectdataobjtwo.projectname];
				
				
				[projectdataobjtwo setLevel:sqlite3_column_int(selectNotes_Stmt,2)];
					[SpidleveltwoLevelArray addObject:[NSNumber numberWithInt:projectdataobjtwo.level]];
				[projectdataobjtwo setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[SpidleveltwoBelongsTolArray addObject:[NSNumber numberWithInt:projectdataobjtwo.belongstoid]];
				
				[projectdataobjtwo setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[SgetLevelTwoimageArray addObject:projectdataobjtwo.projectIMGname];
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
}

-(void)getLevelThree:(NSInteger)lThree
{

	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,folderImage from ProjectMaster where belongstoID=%d AND level =%d ORDER BY projectPriority,projectName ASC",lThree,3];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
			
		{
			
			[SpidlevelthreeArray  removeAllObjects];
			[SpidlevelthreeNameArray removeAllObjects];
			[SgetLevelThreetimageArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjthree = [[AddProjectDataObject alloc]init];
				[projectdataobjthree setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[SpidlevelthreeArray addObject:[NSNumber numberWithInt:projectdataobjthree.projectid]];
				
				[projectdataobjthree setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[SpidlevelthreeNameArray addObject:projectdataobjthree.projectname];
				
				
				[projectdataobjthree setLevel:sqlite3_column_int(selectNotes_Stmt,2)];
				[SpidlevelthreeLevelArray addObject:[NSNumber numberWithInt:projectdataobjthree.level]];
				
				[projectdataobjthree setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[SpidlevelthreeBelongsToArray addObject:[NSNumber numberWithInt:projectdataobjthree.belongstoid]];
				
				[projectdataobjthree setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[SgetLevelThreetimageArray addObject:projectdataobjthree.projectIMGname];
				
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		
	}	
}

#pragma mark -
#pragma mark Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	float height;
	//MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	
	height=60.40;
	
	return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    //[self performSelector:@selector(selectionMethod:) withObject:indexPath];	
   
}


// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
   
	
	
	return [treeNode descendantCount];
	
	
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d", indexPath.row+1];
	
   MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
   MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
													expanded:node.inclusive];   
   [[cell arrowImage]  addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
   [[cell arrowImage] setTag:indexPath.row + 1];
	
   if(node.LevelVal==0)
	{
		UIImage *imgl1=[UIImage imageNamed:node.Fimg];
		buttonLevelone = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelone setBackgroundImage:imgl1 forState:UIControlStateNormal];
		buttonLevelone.frame = CGRectMake(25.0,7.0,265.0, 40.0);
		buttonLevelone.tag=indexPath.row;
		[buttonLevelone addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:buttonLevelone];
        //////////////
        HidebuttonLevelone = [UIButton buttonWithType:UIButtonTypeCustom];
		[HidebuttonLevelone setBackgroundColor:[UIColor clearColor]];
		HidebuttonLevelone.frame = CGRectMake(35.0,9.0,300.0, 40);
		HidebuttonLevelone.tag=indexPath.row;
		[HidebuttonLevelone addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:HidebuttonLevelone];
        ///////////////
		
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(4,8,190,30)];
		//myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
		myLabel.text=node.value;
		myLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		
		UILabel *viewmB=[[UILabel alloc]init];
		viewmB.text =[NSString stringWithFormat:@"%d~%d~%@",node.LevelVal,node.ID,node.value];
		viewmB.tag =666;
		[buttonLevelone addSubview:viewmB];
		[buttonLevelone addSubview:myLabel];
        
        [HidebuttonLevelone addSubview:viewmB];
	}
	
	if(node.LevelVal==1)
	{
		
		UIImage *imgl2=[UIImage imageNamed:node.Fimg];
		buttonLeveltwo = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLeveltwo setBackgroundImage:imgl2 forState:UIControlStateNormal];
		buttonLeveltwo.frame = CGRectMake(53.0,7.0,235.0, 40.0);
		buttonLeveltwo.tag=indexPath.row;
		[buttonLeveltwo addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
	   //[buttonLeveltwo setTitle:node.value forState:UIControlStateNormal];
		[cell.contentView addSubview:buttonLeveltwo];
       ////////////// 
        HidebuttonLeveltwo = [UIButton buttonWithType:UIButtonTypeCustom];
		[HidebuttonLeveltwo setBackgroundColor:[UIColor clearColor]];
		HidebuttonLeveltwo.frame = CGRectMake(55.0,9.0,280.0, 40.0);
		HidebuttonLeveltwo.tag=indexPath.row;
		[HidebuttonLeveltwo addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
 		[cell.contentView addSubview:HidebuttonLeveltwo];
       /////////////////////// 
		
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(4,8,190,30)];
		//myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
		myLabel.text=node.value;
		myLabel.textColor = [UIColor whiteColor];
		myLabel.backgroundColor = [UIColor clearColor];
		UILabel *viewmB=[[UILabel alloc]init];
		viewmB.text =[NSString stringWithFormat:@"%d~%d~%@",node.LevelVal,node.ID,node.value];
		viewmB.tag =666;
		[buttonLeveltwo addSubview:viewmB];
		[buttonLeveltwo addSubview:myLabel];
        
        [HidebuttonLeveltwo addSubview:viewmB];
		
	}
	
	if(node.LevelVal==2)
	{
		
		UIImage *imgl3=[UIImage imageNamed:node.Fimg];
		buttonLevelthree = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelthree setBackgroundImage:imgl3 forState:UIControlStateNormal];
		buttonLevelthree.frame = CGRectMake(82.0,7.0,205.0, 40.0);
		buttonLevelthree.tag=indexPath.row;
		[buttonLevelthree addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
	    //[buttonLevelthree setTitle:node.value forState:UIControlStateNormal];
		[cell.contentView addSubview:buttonLevelthree];
        /////////////////
        HidebuttonLevelthree = [UIButton buttonWithType:UIButtonTypeCustom];
		[HidebuttonLevelthree setBackgroundColor:[UIColor clearColor]];
		HidebuttonLevelthree.frame = CGRectMake(82.0,6.0,250.0, 40.0);
		HidebuttonLevelthree.tag=indexPath.row;
		[HidebuttonLevelthree addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:HidebuttonLevelthree];
         ////////////////
        
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(4,8,180,30)];
		//myLabel.text =[NSString stringWithFormat:@"%d", node.prjctID];
		myLabel.text=node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		UILabel *viewmB=[[UILabel alloc]init];
		viewmB.text =[NSString stringWithFormat:@"%d~%d~%@",node.LevelVal,node.ID,node.value];		viewmB.tag =666;
		[buttonLevelthree addSubview:viewmB];
		[buttonLevelthree addSubview:myLabel];
        
        [HidebuttonLevelthree addSubview:viewmB];
		
	}
	
	if(node.LevelVal==3)
	{
		
		UIImage *imgl4=[UIImage imageNamed:node.Fimg];
		buttonLevelfour = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelfour setBackgroundImage:imgl4 forState:UIControlStateNormal];
		buttonLevelfour.frame = CGRectMake(110.0,7.0,175.0, 40.0);
		buttonLevelfour.tag=indexPath.row;
		[buttonLevelfour addTarget:self action:@selector(selectNowEnd:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:buttonLevelfour];
        ////////////////
        HidebuttonLevelfour = [UIButton buttonWithType:UIButtonTypeCustom];
		[HidebuttonLevelfour setBackgroundColor:[UIColor clearColor]];
		HidebuttonLevelfour.frame = CGRectMake(110.0,7.0,210.0, 40.0);
		HidebuttonLevelfour.tag=indexPath.row;
		[HidebuttonLevelfour addTarget:self action:@selector(selectNowEnd:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:HidebuttonLevelfour];
        
        //////////////////
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(4,8,150,30)];
		//myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
		myLabel.text=node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor whiteColor];
		[buttonLevelfour addSubview:myLabel];
      
		
	}
	
	if (lastProjectID != 0) {
		if (node.ID == lastProjectID) {
			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
			[tickImageView setFrame:CGRectMake(290, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
			[cell.contentView addSubview:tickImageView];
		}
	}
	
	
	
	
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
	return cell;
}


-(IBAction)selectionMethod:(id)sender
{
	
	
	NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
	//NSLog(@"%@",components);
	sentLvalue=[[components objectAtIndex:0]intValue];
	sentBvalue=[[components objectAtIndex:1]intValue];
	fname=[components objectAtIndex:2];
	
	if(sentBvalue==getfval)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OK" message:@"You can't update this folder" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
	}
	
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]+1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];  
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	
	[cell.contentView addSubview:tickImageView];
	
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setBelongValue:sentBvalue];
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setLevelValue:sentLvalue+1];
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setLEVELOPT:YES];
	
    [self.navigationController popViewControllerAnimated:YES]; 
    
  /*  if(getViewValue==2)
	{
		[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:5] animated:YES]; 
	}
	else 
	{
		[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:4] animated:YES]; 
        
	}*/
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
-(void)PlusNow:(id)sender
{
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]];
	if (!node.hasChildren) return;
	node.inclusive = !node.inclusive;	
	[treeNode flattenElementsWithCacheRefresh:YES];
    [foldetTable reloadData];
}

-(void)selectNowEnd:(id)sender
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Folder Level" message:@"You Can Not Add More Then 3 Level" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}
#pragma mark -
#pragma mark Table view delegate


-(IBAction)newFolderAdd:(id)sender
{
	
	for(int i = 0; [treeNode descendantCount]>i; i++)
	{
		[backImageView removeFromSuperview];
	}
	
	if(selctBTN.alpha==0)
	{
		selctBTN.alpha=1;
		sentLvalue=0;
		sentBvalue=0;
		senPID=0;
		SaveIT=NO;
	}
	else
	{
		selctBTN.alpha=0;
		SaveIT=YES;
	}

}

-(IBAction)SaveValue:(id)sender
{

if(sentBvalue==getfval)
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OK" message:@"You Cant update With This Folder" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}
else
{
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setBelongValue:sentBvalue];
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setLevelValue:sentLvalue+1];
	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -2)]setLEVELOPT:YES];
	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES]; 
}
	
}
-(void)SaveDummy
{
}
	

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload 
{
    navBar = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

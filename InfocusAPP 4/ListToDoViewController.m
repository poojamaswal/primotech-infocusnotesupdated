//
//  ToDoParentController.m
//  Organizer

//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define SCROLL_VIEW_TAG				61111
#define Progress_BTN_TAG			71111
#define BADGE_VIEW_TAG				81111
#define TABLE_ROW_HEIGHT_IPHONE		55  //55
#define TABLE_ROW_HEIGHT_IPAD		70  //70
#define BLACK_BOX_TAG 3333
#define BLACK_BUTTON_TAG 6666
#define TEXT_FIELD_TAG 4444
#define DONE_BUTTON_TAG 6667

#define INPUT_VIEW_TAG 5555

#import "ListToDoViewController.h"
#import "ToDoViewController.h"
//#import "InputTypeSelectorViewController.h"
#import "OrganizerAppDelegate.h"
#import "HomeViewController.h"
//#import "CustomBadge.h"
#import "TODODataObject.h"
#import "CalendarParentController.h"
#import "Cal_Todo_OptionsVC.h"
#import "ViewTODOScreen.h"
#import "ProjectMainViewCantroller.h"
#import "AddEditStartDateEndDateVC.h"
//Alok Added
#import <SystemConfiguration/SystemConfiguration.h>
#import "iSpeechViewControllerTTS.h"

//Steve added: Gesture Recognizers
#import "TransformableTableViewCell.h"
#import "JTTableViewGestureRecognizer.h"
#import "UIColor+JTGestureBasedTableViewHelper.h"

//Steve added
#import "AllInsertDataMethods.h"
#import "HandWritingViewController.h"
#import "VoiceToTextViewController.h"
//#import "AdWhirlView.h"
#import "AddToDoAlertViewController.h"

//Steve added for popup
#import "WEPopoverContentViewController.h"
//#import "UIBarButtonItem+WEPopover.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIImage+ImageEffects.h"//Steve


//Steve changed
@interface ListToDoViewController() <JTTableViewGestureEditingRowDelegate, JTTableViewGestureAddingRowDelegate, JTTableViewGestureMoveRowDelegate,JTTableViewGestureSwipeUpDelegate,JTTableViewGestureSwipeDownDelegate>


@property (nonatomic, strong) NSMutableArray                *rows;
@property (nonatomic, strong) JTTableViewGestureRecognizer  *tableViewRecognizer;
@property (nonatomic, strong) id                            grabbedObject;
@property (nonatomic, strong) UIDatePicker                  *datePicker; //Steve - picker for iOS 7
@property (nonatomic, strong) UIDatePicker                  *datePickerSecondAlert; //Steve - picker for iOS 7
@property (nonatomic, strong) UIView                        *pickerBackgroundView_1; //Steve - background for picker 1
@property (nonatomic, strong) UIView                        *pickerBackgroundView_2; //Steve - background for picker 2
@property (nonatomic, strong) NSDate                        *alertDate_1;//Steve
@property (nonatomic, strong) NSDate                        *alertDate_2;//Steve
@property (nonatomic, strong) NSString                      *alertString_1;//Steve
@property (nonatomic, strong) NSString                      *alertString_2;//Steve
@property (nonatomic, strong) NSString                      *alertStringSaveFormat_1;//Steve
@property (nonatomic, strong) NSString                      *alertStringSaveFormat_2;//Steve
@property (nonatomic, strong) UIImage                       *screenShotImage; //Steve - screenshot of  ChecklistViewController
@property (nonatomic, strong) UIView                        *backgroundViewforHandwriting; //Steve
@property (nonatomic, strong) OrganizerAppDelegate          *organizerAppDelegate; //Steve
@property (nonatomic, assign) BOOL                          isHandwriting;//Steve iPad - when click HWbutton on Nav, lets know its HW

-(void)showInputOptPopover:(BOOL)show;
-(void)showSearchOptPopover:(BOOL)show;
-(void) eyeButton_Clicked:(id)sender; //Steve added
-(void)getValuesFromDatabase;
-(void)alert1DatePickerChanged; //Steve
-(void)secondAlertButtonClicked; //Steve
-(void)secondAlertDatePickerChanged; //Steve

@end


@implementation ListToDoViewController
@synthesize todoDateObjArray, toDoTableView,fromProject;
//@synthesize filterParentScrollView;
@synthesize selectedButtonIndex,projectIDFromProjMod;
//@synthesize showDoneTODOs;

//Steve added 
@synthesize rows;
@synthesize tableViewRecognizer;
@synthesize grabbedObject;
@synthesize myBackgroundColor, myFontColor, myFontName, myCellBackgroundColor;
@synthesize todoTitleBinary, todoTitleBinaryLarge,todoTitleType,todoTitle,todoTitleList;

//@synthesize adView;
@synthesize isCreateViewOn;
BOOL internet_YES_NO;//Alok Added to check Netconnection
@synthesize strTextToSpeak,strtDate,endDate,arrM_finalEvents;//Aman's Added
@synthesize str_todaysDate;// Aman's Added
@synthesize str_SelectedButtonTitle;///Aman's Added
@synthesize arrM_ForOtherTabs;///Aman's Added
@synthesize NSDate_dateCreated;//Aman's Added
@synthesize cell=_cell;  //Steve added
@synthesize popoverController; //Steve popup


//Steve added
//@synthesize tosoDataObj;
//@synthesize inputTextField;

#define PLACEHOLDER @"Drag down to create a new To Do"
#define ADDING_CELL @"Continue..."
#define DONE_CELL @"Done"
#define DUMMY_CELL @"Dummy"
#define COMMITING_CREATE_CELL_HEIGHT 60
#define NORMAL_CELL_FINISHING_HEIGHT 60


#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -


- (id)initWithProjectID:(NSInteger ) projId ProjectName:(NSString *)ProjectName
{
    //Steve added
    // NSLog(@"******* initWithProjectID ********");
    
	if (self = [super initWithNibName:@"ListToDoViewController" bundle:[NSBundle mainBundle]]) 
	{
		projectIDFromProjMod  = projId;
        projectName = ProjectName;
	}
	return self;
}

-(void)keyboardWillShow
{
    if([todoTitleType isEqualToString:@"H"]){
        [inputTextField setBackground:nil];
        if([todoTitleType isEqualToString:@"H"] || ![inputTextField.text isEqualToString:@""])todoTitleType = @"T"; 
        todoTitleBinary = nil;
    }
    //NSLog(@"todoTitleType -->%@",todoTitleType);
}


- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    NSLog(@"ListToDoViewController");
    //Steve added
    //[NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    
    
    if(IS_LITE_VERSION)
    {
        //AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
        //[adWhirlView setFrame:CGRectMake(0, 366, 320, 50)];
        //[adWhirlView setBackgroundColor:[UIColor blackColor]];
        //[self.view addSubview:adWhirlView];
        [toDoTableView setFrame:CGRectMake(0, 44, 320, 326)];
    }
  
    navBar.topItem.title = @"";
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,0,200,40)];
    navLabel.text = projectName;
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.font = [UIFont boldSystemFontOfSize:18];
    navLabel.adjustsFontSizeToFitWidth = YES;
    navLabel.numberOfLines=0;
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.textColor = [UIColor whiteColor];
    
    [navBar addSubview:navLabel];
 
    indexWhereCellIsBeingInserted = 0;
    
	todoDateObjArray = [[NSMutableArray alloc] init];
    
    
    //////////// Steve Added for popup /////////////////////
    
    //Try setting this to UIPopoverController to use the iPad popover. The API is exactly the same!
	popoverClass = [WEPopoverController class];
	
	currentPopoverCellIndex = -1;  //-1
    
    //////////// Steve End  /////////////////////
    
    isPenIcon = YES; //For this class only, if YES, animates the Handwritting animation (rotate)
    

}


-(void)viewWillAppear:(BOOL)animated
{
      [super viewWillAppear: YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        self.navigationController.navigationBarHidden = NO;
        navBar.hidden = YES;
        backButton.hidden = YES;
         [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
       
        if (IS_IOS_7) {
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            
              ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.view.backgroundColor = [UIColor whiteColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:69/255.0f green:165/255.0f blue:232/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:69/255.0 green:165/255.0  blue:232/255.0  alpha:1.0];;
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                
                bottomBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                bottomBar.translucent = NO;
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                           [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.view.backgroundColor = [UIColor blackColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:69/255.0f green:165/255.0f blue:232/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66/255.0 green:66/255.0  blue:66/255.0  alpha:1.0];;
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                
                bottomBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor whiteColor];
                bottomBar.translucent = NO;
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            }
            
#pragma mark -- IPad Primotech
            
            if(IS_IPAD)
            {
                UIImage * imgHW;
                UIButton * btnHW;
                imgHW = [UIImage imageNamed:@"pencil_icon.png"];
                btnHW = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnHW setImage:imgHW forState:UIControlStateNormal];
                btnHW.showsTouchWhenHighlighted = YES;
                btnHW.frame = CGRectMake(0.0, 0.0, imgHW.size.width, imgHW.size.height);
                
                [btnHW addTarget:self action:@selector(inputOptionButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * hw = [[UIBarButtonItem alloc]initWithCustomView:btnHW];
               
                
                UIImage * imgSearch;
                UIButton * btnSearch;
                imgSearch = [UIImage imageNamed:@"search_icon.png"];
                btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnSearch setImage:imgSearch forState:UIControlStateNormal];
                btnSearch.showsTouchWhenHighlighted = YES;
                btnSearch.frame = CGRectMake(0.0, 0.0, imgSearch.size.width, imgSearch.size.height);
                
                [btnSearch addTarget:self action:@selector(searchBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * search = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
                
             
                UIImage * imgVoice;
                UIButton * btnVoice;
                imgVoice = [UIImage imageNamed:@"speaker_icon.png"];
                btnVoice = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnVoice setImage:imgVoice forState:UIControlStateNormal];
                btnVoice.showsTouchWhenHighlighted = YES;
                btnVoice.frame = CGRectMake(0.0, 0.0, imgVoice.size.width, imgVoice.size.height);
                
                [btnVoice addTarget:self action:@selector(optionButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * voice = [[UIBarButtonItem alloc]initWithCustomView:btnVoice];
                
                
                UIImage * imgShare;
                UIButton * btnShare;
                imgShare = [UIImage imageNamed:@"share_icon.png"];
                btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnShare setImage:imgShare forState:UIControlStateNormal];
                btnShare.showsTouchWhenHighlighted = YES;
                btnShare.frame = CGRectMake(0.0, 0.0, imgShare.size.width, imgShare.size.height);
                
                [btnShare addTarget:self action:@selector(mailBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * share = [[UIBarButtonItem alloc]initWithCustomView:btnShare];
                
                
                self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:hw,search,voice,share,nil];
                self.navigationItem.title = @"";
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
                
                toDoTableView.frame = CGRectMake(1, 0, 459, 1024); //Steve
                bottomBar.hidden = YES;//Steve
            }
            else
            {
                if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 568);
                //self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20);
                toDoTableView.frame = CGRectMake(0, 0, 320, 460 + 64);
                bottomBar.frame = CGRectMake(0, 460 + 64, 320, 44);
                
            }
            else {
                self.view.frame = CGRectMake(0, 0, 320, 480);
                //self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20);
                toDoTableView.frame = CGRectMake(0, 0, 320, 372 + 64);
                bottomBar.frame = CGRectMake(0, 416-44 + 64, 320, 44);
                
            }
            }
            
            
        }
        else{ //iOS 6
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
            
            
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 568-20 - 44); //Steve - minus status(20) minus Nav (44)
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 568 - 20); //Steve - Minus StatusBar (20)
                toDoTableView.frame = CGRectMake(0, 0, 320, 460);
                bottomBar.frame = CGRectMake(0, 460, 320, 44);
            }
            else {
                self.view.frame = CGRectMake(0, 0, 320, 480-20 - 44); //Steve - minus status(20) minus Nav (44)
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 480 - 20); //Steve - Minus StatusBar (20)
                toDoTableView.frame = CGRectMake(0, 0, 320, 372);
                bottomBar.frame = CGRectMake(0, 416-44, 320, 44);
            }
            
        }
        
        
        UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,0,200,40)];
        navLabel.text = projectName;
        navLabel.backgroundColor = [UIColor clearColor];
        navLabel.font = [UIFont boldSystemFontOfSize:18];
        navLabel.adjustsFontSizeToFitWidth = YES;
        navLabel.numberOfLines=0;
        navLabel.textAlignment = NSTextAlignmentCenter;
        navLabel.textColor = [UIColor whiteColor];

        self.title = navLabel.text;
        
     }
    else{
        
        if (!IS_IPHONE_5) { //Not iPhone5
            //shareButton.frame = CGRectMake(105, 416, 111, 37);
        }
        
        self.navigationController.navigationBarHidden = YES;
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
    }
    
     // ***************** Steve End ***************************

    
    self.toDoTableView.showsVerticalScrollIndicator = NO;
    
  
    if(!isCreateViewOn && ([todoTitleType isEqualToString:@"H"] || [todoTitleType isEqualToString:@"V"]|| [todoTitleType isEqualToString:@"T"]))
    {
        [self createAddView];
    }
    else if([todoTitleType isEqualToString:@"H"] && todoTitleBinary)
    {
        
        [inputTextField setText:@""];
        [inputTextField setBackground:[UIImage imageWithData:todoTitleBinary]];
    }
    else if([todoTitleType isEqualToString:@"V"])
    {
        [inputTextField setBackground:nil];
        [inputTextField setText:todoTitle];
    }
    
    OrganizerAppDelegate *appdel = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(!appdel.IsVTTDevice || !appdel.isVTTDeviceAuthorized)
        [inputSelectorBarButton setImage:[UIImage imageNamed:@"PenIcon.png"]]; 
    
    
    //Steve commented
   // [NSTimeZone resetSystemTimeZone];
   // [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [self getValuesFromDatabase];

    
    if(!self.tableViewRecognizer){  // Anil's Addition
        self.tableViewRecognizer = [toDoTableView enableGestureTableViewWithDelegate:self];
    }
    
    
    
    ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
        
        //Steve added - Custom colors and fonts
        myBackgroundColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
        
        //Cell color
        myCellBackgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
        
        myFontColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
        myFontName = [UIFont fontWithName:@"Noteworthy-Bold" size:20];
        
        toDoTableView.separatorStyle = UITableViewCellSelectionStyleNone;
        toDoTableView.backgroundColor = [UIColor whiteColor];
       
    }
    else{ //Dark Theme
        
        //Steve added - Custom colors and fonts
        myBackgroundColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
        
        //Cell color
        myCellBackgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        
        myFontColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
        myFontName = [UIFont fontWithName:@"Noteworthy-Bold" size:20];
        
        toDoTableView.separatorStyle  = UITableViewCellSeparatorStyleSingleLine;
        toDoTableView.separatorColor = [UIColor colorWithRed:69.0f/255.0f green:165.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
        toDoTableView.backgroundColor = [UIColor colorWithRed:69.0f/255.0f green:165.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
        
        
        //***** Steve - Keeping this until colors are updated.
        toDoTableView.separatorColor = [UIColor clearColor];
        toDoTableView.backgroundColor = [UIColor blackColor];
        myCellBackgroundColor = [UIColor whiteColor];
    }
    
    
    
    toDoTableView.rowHeight       = NORMAL_CELL_FINISHING_HEIGHT;
    
    //Keybaord visible
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector (keyboardDidHide:)
                                                 name: UIKeyboardDidHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector (keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_List"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer  *swipeDownHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];
        [helpView addGestureRecognizer:tapHelpView];
        [helpView addGestureRecognizer:swipeDownHelpView];
        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = YES;
        }
        
 
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_List"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
        
    }
    
   [toDoTableView reloadData];
}

//Steve added for Help views
-(void)viewDidAppear:(BOOL)animated{
    //Steve added
    //If first time running App, show some "Help" Views
    
    /*
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_List"]) 
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer  *swipeDownHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        
        [helpView addGestureRecognizer:tapHelpView];  
        [helpView addGestureRecognizer:swipeDownHelpView];
        helpView.hidden = NO; 
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_List"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];     
        
    } 
    */
}


- (void)handleGestureFrom:(UISwipeGestureRecognizer *)recognizer 
{
    if ([recognizer numberOfTouches] == 2) {
        // do whatever you need to do
        // NSLog(@"%s",__FUNCTION__);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"detected" message:@"HI" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"detected" message:@"HI" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}

#pragma mark - Project Selection Delegate
-(void)selectedProject:(AddProjectDataObject *)newProject
{
    [self setProjDataObj:newProject];
    
}

#pragma mark - Overridden setters
-(void)setProjDataObj:(AddProjectDataObject *)projDataObj
{
    //Make sure we're not setting up the same monster.
    if (_projDataObj != projDataObj) {
        _projDataObj = projDataObj;
        
        [toDoTableView reloadData];
        
    }
}


#pragma mark -
#pragma mark  from Protocol
#pragma mark -


-(void) setAlertString_1:(NSString*) alertString_1 setAlertString_2:(NSString *) alertString_2 isAlertOn:(BOOL) isAlertOnOrOff
{
    
    alertsLabel.text = alertString_1;
    secondAlertsLabel.text = alertString_2;
    isAlertOn = isAlertOnOrOff;
    
}


#pragma mark -
#pragma mark Button Actions
#pragma mark -

//Alok Added Back Button clicked
-(void)setAlertString:(NSString *) alert {
	alertsLabel.text =  alert;
}
-(void)setSecondAlertString:(NSString *)  alert
{
    secondAlertsLabel.text =  alert;
}

-(void)setIsAlertOn:(BOOL)_isAlertOn
{
    isAlertOn = _isAlertOn;
}



- (IBAction)alerts_buttonClicked:(id)sender {
    
    
    if (IS_IOS_7) {
        
        [inputTextField resignFirstResponder];
        isCreateViewOn =YES;
    
        
        if (_pickerBackgroundView_1 != nil) {
            [_pickerBackgroundView_1 removeFromSuperview];
            _pickerBackgroundView_1 = nil;
        }
        
        _pickerBackgroundView_1 = [[UIView alloc]init];
        //pooja-iPad
        if(IS_IPAD)
            _pickerBackgroundView_1.frame = CGRectMake(0, 135, 459, 220);
        else
            _pickerBackgroundView_1.frame = CGRectMake(0, 135, 320, 220);
        _pickerBackgroundView_1.backgroundColor = myCellBackgroundColor;
        [self.view addSubview:_pickerBackgroundView_1];
        
        _datePicker = [[UIDatePicker alloc]init];
        //pooja-iPad
        if(IS_IPAD)
            _datePicker.frame = CGRectMake(0, 0, 459, 220);
        else
            _datePicker.frame = CGRectMake(0, 0, 320, 220);
        _datePicker.date = [NSDate date];
        _datePicker.minuteInterval = 5;
        _datePicker.timeZone = [NSTimeZone systemTimeZone];
        [_datePicker addTarget:self action:@selector(alert1DatePickerChanged) forControlEvents:UIControlEventValueChanged];
        [_pickerBackgroundView_1 addSubview:_datePicker];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        if ([alertsLabel.text isEqualToString:@"None"] || alertsLabel == nil) {
            
            _alertDate_1 = _datePicker.date;

        }
        else{
            
            _datePicker.date = _alertDate_1;
        }
        
        _alertString_1 = [dateFormatter stringFromDate:_alertDate_1];
        
        NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
        [dateFormatter_2 setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter_2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        _alertStringSaveFormat_1 = [dateFormatter_2 stringFromDate:_datePicker.date];
        
        [self setAlertString:_alertString_1];
        
        
        
        //NSLog(@" _alertDate_1 = %@", _alertDate_1);
        //NSLog(@" _alertDate_1 = %@", [_alertDate_1 descriptionWithLocale:[NSTimeZone localTimeZone]]);
        
        
        
    }
    else{//iOS 6
        
        isCreateViewOn =YES;
        
        NSDate *sourceDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
        
        NSString *CurrentTime =[NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:sourceDate]];
        
        AddToDoAlertViewController *addToDoAlertViewController = [[AddToDoAlertViewController alloc] initWithAlertDate:sourceDate alertSecondDate:sourceDate];//Steve changed
        
        if(![alertsLabel.text isEqualToString:@"None"])
        {
            [addToDoAlertViewController setAlertString:alertsLabel.text];
        }
        else
        {
            [addToDoAlertViewController setAlertString:CurrentTime];
        }
        
        [addToDoAlertViewController setAlertStringSecond:secondAlertsLabel.text];
        addToDoAlertViewController.alertDelegate = self;
        
        [self.navigationController pushViewController:addToDoAlertViewController animated:YES];
    }
    
}


-(void)alert1DatePickerChanged{
    
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    _alertDate_1 = _datePicker.date;
    _alertString_1 = [formatter stringFromDate:_alertDate_1];
    [self setAlertString:_alertString_1];
    
    NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
    [dateFormatter_2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    _alertStringSaveFormat_1 = [dateFormatter_2 stringFromDate:_datePicker.date];
    
}


-(void)secondAlertButtonClicked{
    
    [inputTextField resignFirstResponder];
    isCreateViewOn =YES;
    
    
    if (_pickerBackgroundView_2 != nil) {
        [_pickerBackgroundView_2 removeFromSuperview];
        _pickerBackgroundView_2 = nil;
    }
    
    _pickerBackgroundView_2 = [[UIView alloc]init];
    //pooja-iPad
    if(IS_IPAD)
        _pickerBackgroundView_2.frame = CGRectMake(0, 135, 459, 220);
    else
        _pickerBackgroundView_2.frame = CGRectMake(0, 135, 320, 220);
    _pickerBackgroundView_2.backgroundColor = myCellBackgroundColor;
    [self.view addSubview:_pickerBackgroundView_2];
    
    _datePickerSecondAlert = [[UIDatePicker alloc]init];
    //pooja-iPad
    if(IS_IPAD)
        _datePickerSecondAlert.frame = CGRectMake(0, 0, 459, 220);
    else
        _datePickerSecondAlert.frame = CGRectMake(0, 0, 320, 220);
    _datePickerSecondAlert.date = [NSDate date];
    _datePickerSecondAlert.minuteInterval = 5;
    _datePickerSecondAlert.timeZone = [NSTimeZone systemTimeZone];
    [_datePickerSecondAlert addTarget:self action:@selector(secondAlertDatePickerChanged) forControlEvents:UIControlEventValueChanged];
    [_pickerBackgroundView_2 addSubview:_datePickerSecondAlert];
    
    
    if ([secondAlertsLabel.text isEqualToString:@"None"] || secondAlertsLabel == nil) {
        
        _alertDate_2 = _datePickerSecondAlert.date;
        
    }
    else{
        
        _datePickerSecondAlert.date = _alertDate_2;
    }
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *alertString = [dateFormatter stringFromDate:_alertDate_2];
    [self setSecondAlertString:alertString];
    

    _alertString_2 = [dateFormatter stringFromDate:_alertDate_2];
    [self setSecondAlertString:_alertString_2];
    
    
    NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
    [dateFormatter_2 setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter_2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    _alertStringSaveFormat_2 = [dateFormatter_2 stringFromDate:_datePickerSecondAlert.date];
    
}


-(void)secondAlertDatePickerChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    secondAlertsLabel.text = [formatter stringFromDate:_datePickerSecondAlert.date];
    
    _alertDate_2 = _datePickerSecondAlert.date;
    _alertString_2 = [formatter stringFromDate:_alertDate_2];
    [self setSecondAlertString:_alertString_2];
    
    
    NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
    [dateFormatter_2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    _alertStringSaveFormat_2 = [dateFormatter_2 stringFromDate:_datePickerSecondAlert.date];
    
}


-(IBAction)BackButton_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


//Check box clicked
- (void)progressBtn_Clicked:(id)sender 
{
    //NSLog(@"******* progressBtn_Clicked: ********");
    
	UIButton *tempBtn = (UIButton *)sender;
	NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
	//NSLog(@"%@",getSTR);
	
	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
	
    valueId    =  [[components objectAtIndex:0]intValue];
    progresVal =  [[components objectAtIndex:1]intValue];
	
	//NSLog(@"%d %d",valueId,progresVal);
	
	
	if(progresVal==100)
	{
		[self updateToDoProgress:valueId ProgresVAL:0];
		[toDoTableView reloadData];
        
	}
	else 
	{
		[self updateToDoProgress:valueId ProgresVAL:100];
		[toDoTableView reloadData];
	}
    
	UITableViewCell *Cell = [toDoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tempBtn.tag inSection:0]];
	UIImageView *proIndBorder = (UIImageView *)[Cell.contentView viewWithTag:9909];
	UIButton *progBtn = (UIButton *)[Cell.contentView viewWithTag:Progress_BTN_TAG];
	
	float buttonHeight = 20 - 4;
	percentComplete = progresVal;
	float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
	
	if (percentComplete == 100 ) 
	{
        
		UIImage *btnBgImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
		[progBtn setBackgroundColor:[UIColor clearColor]];
		[progBtn setImage:btnBgImage forState:UIControlStateNormal];
		[proIndBorder setHidden:YES];
		[progBtn setFrame:proIndBorder.frame];
		[toDoTableView reloadData];
		
	}
	else
	{
		if (percentComplete != 100) 
		{
			[progBtn setBackgroundColor:[UIColor redColor]];
			[progBtn setImage:nil forState:UIControlStateNormal];
			[proIndBorder setHidden:NO];
			[progBtn setFrame:CGRectMake(7, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, 16, finalHieght)];
			[toDoTableView reloadData];
		}
	}
}

//Updates Progress bar when checkbox clicked
- (void)updateToDoProgress:(NSInteger)TODOID ProgresVAL:(NSInteger)ProgresVAL;
{
    // NSLog(@"******* updateToDoProgress: ********");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
        NSString *sql = [[NSString alloc]initWithFormat:@"Update ToDoTable set Progress=%d  where TODOID=%d ",ProgresVAL,TODOID];
		//NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	if(SQLITE_DONE != sqlite3_step(insertProjectData))
	{
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	} 
	else 
	{
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		[self getValuesFromDatabase];
	}	
}

- (IBAction)homeButton_Clicked:(id)sender 
{
	
    //NSLog(@"******* homeButton_Clicked: ********");
    
	[self.navigationController  popToRootViewControllerAnimated:YES];
}

- (IBAction)calendarButton_Clicked:(id)sender 
{
    // NSLog(@"******* calendarButton_Clicked: ********");
    
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[appDelegate.navigationController popViewControllerAnimated:NO];
	
	CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[appDelegate.navigationController pushViewController:calendarParentController animated:YES];
}

- (IBAction)addToDoButton_Clicked:(id)sender {
    
    // NSLog(@"******* addToDoButton_Clicked: ********");
    
	//ToDoViewController *toDoViewController =[[ToDoViewController alloc] initWithNibName:@"ToDoViewController" bundle:[NSBundle mainBundle]];
    ToDoParentController *toDoParentController = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:toDoParentController animated:YES];
}

- (IBAction)ProjectButton_click:(id)sender
{
    // NSLog(@"******* ProjectButton_click: ********");
    
	ProjectMainViewCantroller *projectbtn=[[ProjectMainViewCantroller alloc]initWithNibName: @"ProjectMainViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:projectbtn animated:YES];
}

// Steve added 12-17-12
-(void) eyeButton_Clicked:(id)sender{
    //NSLog(@"eyeButton_Clicked");
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
          
    // Navigation logic may go here. Create and push another view controller.
    BOOL shouldShowNewPopover = indexPath.row!= currentPopoverCellIndex;

    
    if (self.popoverController) {
       // NSLog(@" self.popoverController =   %@", self.popoverController);
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
        currentPopoverCellIndex = -1;
    }
    
    if (shouldShowNewPopover) {
        TODODataObject *todoDataObjPass = [todoDateObjArray objectAtIndex:indexPath.row];
        
        UIViewController *contentViewController = [[WEPopoverContentViewController alloc] initWithStyle:UITableViewStylePlain ToDoObject:todoDataObjPass];
        
        CGRect frame = [toDoTableView cellForRowAtIndexPath:indexPath].frame;
        CGRect rect = frame;
        
        popoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
        
        if ([self.popoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
            [self.popoverController setContainerViewProperties:[self improvedContainerViewProperties]];
        }
        
        self.popoverController.delegate = self;
        
        //Uncomment the line below to allow the table view to handle events while the popover is displayed.
        //Otherwise the popover is dismissed automatically if a user touches anywhere outside of its view.
        // self.popoverController.passthroughViews = [NSArray arrayWithObject:self.toDoTableView];
        
        [self.popoverController presentPopoverFromRect:rect
                                                inView:toDoTableView  //inView:self.view  //Steve changed so popup reflects tableview, Not self.view
                              permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown|
                                                        UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight)
                                              animated:YES];
        currentPopoverCellIndex = indexPath.row;
       // NSLog(@"currentPopoverCellIndex = %d",currentPopoverCellIndex);
       // NSLog(@" popoverController) =   %@", popoverController);
    }
    

}

//Steve added
- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
   // NSLog(@"WEPopoverTableViewController improvedContainerViewProperties");
	
	WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc] ;
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0; //0.0
	CGFloat bgCapSize = 0.0; //0.0
	CGFloat contentMargin = 1.0; //4.0  //Steve changed was 4.0
	
	//bgImageName = @"box-two-lines.png";
    bgImageName = @"pop-up-box.png";
    
	
	// These constants are determined by the popoverBg.png image file and are image dependent
    //Steve comments - bgMargin adjust arrow closer or further from popup
	bgMargin = 13; // 13   // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
	bgCapSize = 31; // 31   // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin; //
	props.rightContentMargin = contentMargin; // -1//  Need to shift one pixel for border to look correct //Steve changed was -1
	props.topContentMargin = contentMargin;
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 0.0; //4.0
	
    /*
	props.upArrowImageName = @"PopupBoxArrowUp.png";
	props.downArrowImageName = @"PopupBoxArrowDown";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
    */
    
    props.upArrowImageName = @"pop-up-ArrowUp.png";
	props.downArrowImageName = @"pop-up-ArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
    
	return props;
}

#pragma mark -
#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.popoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
   // NSLog(@"popoverControllerShouldDismissPopover");
    currentPopoverCellIndex = -1; //Steve added 12-17-12. Fixes bug when click on same cell, popup didn't show
	return YES;
}

#pragma mark -
#pragma mark Tableview  Methods
#pragma mark -



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"******** didSelectRowAtIndexPath **********  ");
    
    UITableViewCell *cell = [toDoTableView cellForRowAtIndexPath:indexPath];
    cellVar = cell;
    
    
    NSLog(@" cell.textLabel.text (didSelectRow)= %@  ", cell.textLabel.text);
    
    //Steve added 12-24-12 - fixes bug when clicking the "PLACEHOLDER" (When no todo's) the App was crashing
    if ([cell.textLabel.text isEqualToString:PLACEHOLDER]){
        return;
    }
    
    
    ////////// Steve added ///////////////////////////
    /////////// Detects cell taps ///////////////////
    ////////// Single Tap trigers method tapTimerFired //////////

    
    // checking for double taps here
    if (tapCount == 1 && tapTimer != nil && tappedRow == indexPath.row)
    {
        // double tap - Put your double tap code here
        [tapTimer invalidate];
        tapTimer = nil;
        
       // NSLog(@"double tapped the cell");
        doubleTapRow = tappedRow;
        
        [cell setSelected:NO animated:YES];  // maybe, maybe not
        
        ////////////////////////////////////////////////////
        // popup with doubleTapRow /////////////
        ///////////////////////////////////////////////////
        
        // Navigation logic may go here. Create and push another view controller.
        BOOL shouldShowNewPopover = indexPath.row!= currentPopoverCellIndex;
        
        if (self.popoverController) {
            [self.popoverController dismissPopoverAnimated:YES];
            self.popoverController = nil;
            currentPopoverCellIndex = -1;
        }
        
        if (shouldShowNewPopover) {
            TODODataObject *todoDataObjPass = [todoDateObjArray objectAtIndex:indexPath.row];
            
            UIViewController *contentViewController = [[WEPopoverContentViewController alloc] initWithStyle:UITableViewStylePlain ToDoObject:todoDataObjPass];
            
            CGRect frame = [tableView cellForRowAtIndexPath:indexPath].frame;
            CGRect rect = frame;
            
            //NSLog(@"rect = %@",NSStringFromCGRect(rect));
            
            self.popoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
            
            // NSLog(@" self.popoverController 1 =   %@", self.popoverController);
            
            if ([self.popoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.popoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            
            self.popoverController.delegate = self;
            
             //NSLog(@" self.popoverController 2 =   %@", self.popoverController);
            
            //Uncomment the line below to allow the table view to handle events while the popover is displayed.
            //Otherwise the popover is dismissed automatically if a user touches anywhere outside of its view.
           // self.popoverController.passthroughViews = [NSArray arrayWithObject:self.toDoTableView];
            
            [self.popoverController presentPopoverFromRect:rect
                                                    inView:tableView  //inView:self.view  //Steve changed so popup reflects tableview, Not self.view
                                  permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown|
                                                            UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight)
                                                  animated:YES];
            currentPopoverCellIndex = indexPath.row;
            //NSLog(@"currentPopoverCellIndex = %d",currentPopoverCellIndex);
             
        }

        
        
        tapCount = 0;
        tappedRow = -1;
    }
    else if (tapCount == 0)
    {
        // This is the first tap. If there is no tap till tapTimer is fired, it is a single tap
         //NSLog(@"single tapped the cell");
        
        //Dismissed popover if single tap
        if (self.popoverController) {
            [self.popoverController dismissPopoverAnimated:YES];
            self.popoverController = nil;
            currentPopoverCellIndex = -1;
            return;
        }
        
        tapCount = 1;
        tappedRow = indexPath.row;                        //0.2
        tapTimer = [NSTimer scheduledTimerWithTimeInterval:0.35 target:self
                                                  selector:@selector(tapTimerFired:)
                                                  userInfo:nil repeats:NO];
    }
    else if (tappedRow != indexPath.row)
    {
        // tap on new row
        tapCount = 0;
        if (tapTimer != nil)
        {
            [tapTimer invalidate];
            tapTimer = nil;
        }
    }
    ////////////////// Steve end ////////////////////////////////////////
    
    
    ////////////// Steve Added /////////////////////////
    // fixes bug when Todo's are empty, clicking on the "PLACEHOLDER" cell causes a crash
    
    if ([cell.textLabel.text  isEqual: PLACEHOLDER])
    {
        // NSLog(@"Placehoder *******************");
        return;
    }
    
    ////////////// Steve Added End /////////////////////////

    
}


//Steve added
// Detects single tap on cell for Editing cell
- (void)tapTimerFired:(NSTimer *)aTimer
{
    // timer fired, there was a single tap on indexPath.row = tappedRow
    
    indexWhereCellIsBeingInserted = tappedRow;
    
    TODODataObject *editItem = [todoDateObjArray objectAtIndex:indexWhereCellIsBeingInserted];
    
    // NSLog(@" editItem.todoTitle = %@", editItem.todoTitle);
    
    //Steve changed
    //VTT changed to regular Edit
    if([editItem.todoTitleType isEqualToString:@"T"] || [editItem.todoTitleType isEqualToString:@"V"] || [editItem.todoTitleType isEqualToString:@"H"]  )
    {
        isEditingToDo = YES;
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        // NSLog(@"tableView:didSelectRowAtIndexPath: %@", indexPath);
        //*****************************************
        //set up Input view and fields
        //*****************************************
        
        //new view and input textfield to enter new todo
        UIView *inputView;
        if ([sharedDefaults boolForKey:@"NavigationNew"])
            //pooja-iPad
            if(IS_IPAD)
                inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 459, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
            else
                inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 320, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
        else
            //pooja-iPad
            if(IS_IPAD)
                inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 , 459, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
            else
                inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42, 320, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
        
        inputView.backgroundColor = myCellBackgroundColor;
        //inputView.alpha = 0.0; //Steve added to animate
        [self.view addSubview:inputView];
        // [UIView animateWithDuration:0.10 animations:^{inputView.alpha = 1.0f;}];  //Steve added to animate
        
        [inputView setTag:INPUT_VIEW_TAG];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"])
            //pooja-iPad
            if(IS_IPAD)
                inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10 - 42,400,NORMAL_CELL_FINISHING_HEIGHT-20)];
            else
                inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10 - 42,265,NORMAL_CELL_FINISHING_HEIGHT-20)];
        else
            //pooja-iPad
            if(IS_IPAD)
                inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10,400,NORMAL_CELL_FINISHING_HEIGHT-20)];
            else
                inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10,265,NORMAL_CELL_FINISHING_HEIGHT-20)];
        
        inputTextField.backgroundColor = [UIColor clearColor];
        inputTextField.font = myFontName;
        inputTextField.textColor = myFontColor;
        // [inputTextField setDelegate:self];
        [self.view addSubview:inputTextField];
        
        [inputTextField setTag:TEXT_FIELD_TAG];
        [inputTextField setReturnKeyType:UIReturnKeyDone];
        [inputTextField becomeFirstResponder];
        [inputTextField addTarget:self action:@selector(keyBoardDone:)
                 forControlEvents:UIControlEventEditingDidEndOnExit];
        
        
        [inputTextField setText:editItem.todoTitle];
        
        if([editItem.todoTitleType isEqualToString: @"H"] && editItem.todoTitleBinary) {
            [inputTextField setPlaceholder: @""];
            UIImage *tempTitleImage = [[UIImage alloc] initWithData:editItem.todoTitleBinary];
            todoTitleBinary = editItem.todoTitleBinary;
            todoTitleBinaryLarge = editItem.todoTitleBinaryLarge;
            todoTitleType =@"H";
            [inputTextField setBackground:tempTitleImage];
            [inputTextField setText:@""];
            [inputTextField resignFirstResponder];
            [inputTextField setUserInteractionEnabled:YES];
            
        }
    
        
        UIButton *inputSelecterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"])
            //pooja-iPad
            if(IS_IPAD)
                [inputSelecterButton setFrame:CGRectMake(420,15 -44 ,30,30)];
            else
                [inputSelecterButton setFrame:CGRectMake(280,15 -44 ,30,30)];
        else
            //pooja-iPad
            if(IS_IPAD)
                [inputSelecterButton setFrame:CGRectMake(420,15,30,30)];
            else
                [inputSelecterButton setFrame:CGRectMake(280,15 ,30,30)];
            
        OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
        
        if(!appdelegate.IsVTTDevice || !appdelegate.isVTTDeviceAuthorized)
            [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon_pen.png"] forState:UIControlStateNormal];
        else
            [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon2.png"] forState:UIControlStateNormal];
        
        [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        [inputView addSubview:inputSelecterButton];
        
        
        
        //black opaque space between input box and keyboard
        UIView *blackBox;
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]){
            
            if(IS_IPAD)
            {
                 blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 459, 1024)];
                //NSLog(@"blackbox -- %@",NSStringFromCGRect(blackBox.frame));
            }
            else
            {
                if (IS_IPHONE_5) {
                    blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 320, 284 + 88)];
                }
                else{
                    blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 320, 284)];
                }
            }
        }
        else{
            if(IS_IPAD)
            {
                blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 459, 1024)];
                NSLog(@"blackbox -- %@",NSStringFromCGRect(blackBox.frame));
            }
            else
            {
                if (IS_IPHONE_5) {
                    blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 320, 284 + 88)];
                }
                else{
                    blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 320, 284)];
                }
            }
        }
       
        //UIKeyboardFrameEndUserInfoKey
        
        blackBox.backgroundColor = [UIColor blackColor];
        blackBox.alpha = 0.00;  //Steve changed was: 0.90
        blackBox.tag = BLACK_BOX_TAG;
        [self.view addSubview:blackBox];
        //NSLog(@"%@",NSStringFromCGRect(self.view.frame));
        [self.view bringSubviewToFront:blackBox];
        [UIView animateWithDuration:0.35 animations:^{blackBox.alpha = 0.90f;}];  //Steve added to animate blackBox
        
        
        UIButton *blackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        blackBox.backgroundColor = [UIColor blackColor]; 
        
        if ([sharedDefaults boolForKey:@"NavigationNew"])
            
            //pooja-iPad
            if(IS_IPAD)
                [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 459, 800)]; //Steve 800, was 350
            else
                [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 320, 284)];
        else
            //pooja-iPad
            if(IS_IPAD)
                [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 459, 800)];
            else
                [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 320, 284)];
        
        
        [blackButton addTarget:nil action:@selector(saveToDoToDB:) forControlEvents:UIControlEventTouchUpInside];
        [blackButton setTag:BLACK_BUTTON_TAG];
        [self.view addSubview:blackButton];
        
        /////Start// Alok Added Additional View for Alert & Completion /////
        UIView * AddtionalView = [[UIView alloc]init];
        
        //pooja-iPad
        if(IS_IPAD)
            AddtionalView.frame = CGRectMake(0,NORMAL_CELL_FINISHING_HEIGHT, 459, NORMAL_CELL_FINISHING_HEIGHT+20);
        else
            AddtionalView.frame = CGRectMake(0,NORMAL_CELL_FINISHING_HEIGHT, 320, NORMAL_CELL_FINISHING_HEIGHT+20);
        AddtionalView.backgroundColor = myCellBackgroundColor;
        [inputView addSubview:AddtionalView];
        
        UIImageView * imgblack;
        
        imgblack = [[UIImageView alloc]init];
        
        //pooja-iPad
        if(IS_IPAD)
            imgblack.frame = CGRectMake(0, 0, 459, 2);
        else
            imgblack.frame = CGRectMake(0, 0, 320, 2);
        [imgblack setBackgroundColor:[UIColor blackColor]];
        [AddtionalView addSubview:imgblack];
        
        
        UIButton *alertBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [alertBtn setFrame:CGRectMake(5, AddtionalView.frame.size.height/2-15, 30, 30)];
        [alertBtn setBackgroundImage:[UIImage imageNamed:@"alert_clock.png"] forState:UIControlStateNormal];
        [alertBtn addTarget:nil action:@selector(alerts_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [AddtionalView addSubview:alertBtn];
        
        ////////////// Steve Added /////////////////////////
        //Adds button (disabled) so user knows to click "blackBox" to save
        
        UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //pooja-iPad
        if(IS_IPAD)
            [doneButton setFrame:CGRectMake(175, 650, 109, 35)];
        else
            [doneButton setFrame:CGRectMake(114, 367, 93, 35)]; //(114, 350, 93, 35)
        
        [doneButton setUserInteractionEnabled:NO];
        
        [doneButton setTitle:@"Done" forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
        doneButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        
        doneButton.layer.cornerRadius = 8.0;//10
        doneButton.layer.borderWidth = 1.0;
        doneButton.layer.borderColor = [[UIColor whiteColor]CGColor];
        doneButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
        doneButton.alpha = 0.0f;
        [doneButton setTag:DONE_BUTTON_TAG];
        [self.view addSubview:doneButton];
        [UIView animateWithDuration:0.5 animations:^{doneButton.alpha = 1.0f;}];
        ////////////// Steve Added End /////////////////////////
        
     
        if (IS_IOS_7) { //removes Slider & re-organize % buttons & alert labels
            
            UILabel *lblAlert1 =[[UILabel alloc]init];
            [lblAlert1 setFrame:CGRectMake(40 + 5,5,55,15)];//(40 + 5,AddtionalView.frame.size.height/2-30,55,15)
            [lblAlert1 setBackgroundColor:[UIColor clearColor]];
            [lblAlert1 setText:@"Alert 1:"];
            [lblAlert1 setFont:[UIFont boldSystemFontOfSize:12]];
            [AddtionalView addSubview:lblAlert1];
            
            alertsLabel = [[UILabel alloc]init];
            [alertsLabel setFrame:CGRectMake(45,18,170,15)];
            [alertsLabel setBackgroundColor:[UIColor clearColor]];
            [alertsLabel setFont:[UIFont boldSystemFontOfSize:15]];//10
            [alertsLabel setTextColor:[UIColor blueColor]];
            //[alertsLabel setText:editItem.alert];//Steve commented
            
            ///////////////////// Steve added //////////////////////////////
            
            _alertDate_1 = [self convertStringToDate:editItem.alert];
            
            NSDateFormatter *dateFormatter_1 = [[NSDateFormatter alloc] init];
            [dateFormatter_1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            _alertStringSaveFormat_1 = [dateFormatter_1 stringFromDate:_alertDate_1];
            
            if (_alertStringSaveFormat_1 == nil)
                _alertStringSaveFormat_1 = @"None";
            
            
            NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
            [dateFormatter_2 setLocale:[NSLocale currentLocale]];
            [dateFormatter_2 setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter_2 setTimeStyle:NSDateFormatterShortStyle];
            
            //Converts to local format
            _alertString_1 = [dateFormatter_2 stringFromDate:_alertDate_1];
            
            if (_alertString_1 == nil)
                _alertString_1 = @"None";
            
            
            [alertsLabel setText:_alertString_1];
            ///////////////////// Steve End //////////////////////////////
            
            [AddtionalView addSubview:alertsLabel];
            
            
            UILabel *lblAlert2 =[[UILabel alloc]init];
            [lblAlert2 setFrame:CGRectMake(45, 37,55,15)];
            [lblAlert2 setBackgroundColor:[UIColor clearColor]];
            [lblAlert2 setFont:[UIFont boldSystemFontOfSize:12]];
            [lblAlert2 setText:@"Alert 2:"];
            [AddtionalView addSubview:lblAlert2];
            
            secondAlertsLabel = [[UILabel alloc]init];
            [secondAlertsLabel setFrame:CGRectMake(45,52,170,15)];
            [secondAlertsLabel setBackgroundColor:[UIColor clearColor]];
            [secondAlertsLabel setFont:[UIFont boldSystemFontOfSize:15]];
            [secondAlertsLabel setTextColor:[UIColor blueColor]];
            //[secondAlertsLabel setText:editItem.alertSecond];//Steve commented
            
            
            ///////////////////// Steve added //////////////////////////////
            
            _alertDate_2 = [self convertStringToDate:editItem.alertSecond];
            
            _alertStringSaveFormat_2 = [dateFormatter_1 stringFromDate:_alertDate_2];
            
            if (_alertStringSaveFormat_2 == nil)
                _alertStringSaveFormat_2 = @"None";
            
            
            [dateFormatter_2 setLocale:[NSLocale currentLocale]];
            [dateFormatter_2 setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter_2 setTimeStyle:NSDateFormatterShortStyle];
            
            //Converts to local format
            _alertString_2 = [dateFormatter_2 stringFromDate:_alertDate_2];
            
            if (_alertString_2 == nil)
                _alertString_2 = @"None";
            
            
            [secondAlertsLabel setText:_alertString_2];
            /////////////////////// Steve End //////////////////////////////
            
            [AddtionalView addSubview:secondAlertsLabel];
            
            
            //Steve - adds invisible button to select alerts
            UIButton *invisibleButtonAlert_1 = [UIButton buttonWithType:UIButtonTypeCustom];
            invisibleButtonAlert_1.frame = CGRectMake(alertsLabel.frame.origin.x, alertsLabel.frame.origin.y - 10,
                                                      alertsLabel.frame.size.width, alertsLabel.frame.size.height + 10);
            invisibleButtonAlert_1.backgroundColor = [UIColor clearColor];
            [invisibleButtonAlert_1 addTarget:self action:@selector(alerts_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [AddtionalView addSubview:invisibleButtonAlert_1];
            
            
            //Steve - adds invisible button to select alerts
            UIButton *invisibleButtonAlert_2 = [UIButton buttonWithType:UIButtonTypeCustom];
            invisibleButtonAlert_2.frame = CGRectMake(secondAlertsLabel.frame.origin.x, secondAlertsLabel.frame.origin.y - 10,
                                                      secondAlertsLabel.frame.size.width, secondAlertsLabel.frame.size.height + 10);
            invisibleButtonAlert_2.backgroundColor = [UIColor clearColor];
            [invisibleButtonAlert_2 addTarget:self action:@selector(secondAlertButtonClicked) forControlEvents:UIControlEventTouchUpInside];
            [AddtionalView addSubview:invisibleButtonAlert_2];
            
            
            lblPercent = [[UILabel alloc]init];
            //pooja-iPad
            if(IS_IPAD)
                lblPercent.frame = CGRectMake(340, 1, 80,20);
            else
                lblPercent.frame = CGRectMake(227, 1, 80,20);
            [lblPercent setBackgroundColor:[UIColor clearColor]];
            //[lblPercent setFont:[UIFont fontWithName:@"Arial" size:10]];
            [lblPercent setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:10]];
            [lblPercent setText:[NSString stringWithFormat:@"%d%@ Complete",editItem.toDoProgress,@"%"]];
            [AddtionalView addSubview:lblPercent];
            
            
            UIColor *BtnPercentColor = [UIColor colorWithRed:193.0f/255.0f green:136.0f/255.0f blue:12.0f/255.0f alpha:1.0f]; //Steve
            
            //0% complete
            UIButton *BtnPercent0 =[UIButton buttonWithType:UIButtonTypeCustom];
            //pooja-iPad
            if(IS_IPAD)
                [BtnPercent0 setFrame:CGRectMake(330, 22, 30, 20)];
            else
                [BtnPercent0 setFrame:CGRectMake(220, 22, 30, 20)];
            [BtnPercent0 setTitle:@"  0%" forState:UIControlStateNormal];
            [BtnPercent0 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent0 addTarget:self
                              action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent0 setTag:0];
            BtnPercent0.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent0.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent0.layer.cornerRadius = 8.0f; //10
            BtnPercent0.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent0.layer.shadowOpacity = 0.5f;
            BtnPercent0.layer.shadowRadius = 1.0f;
            BtnPercent0.layer.masksToBounds = NO;
            BtnPercent0.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent0];
            
            UIButton *BtnPercent25 =[UIButton buttonWithType:UIButtonTypeCustom];
            //pooja-iPad
            if(IS_IPAD)
                [BtnPercent25 setFrame:CGRectMake(380, 22, 30, 20)];
            else
                [BtnPercent25 setFrame:CGRectMake(270, 22, 30, 20)];
            [BtnPercent25 setTitle:@" 25%" forState:UIControlStateNormal];
            [BtnPercent25 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent25 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent25 setTag:25];
            BtnPercent25.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent25.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent25.layer.cornerRadius = 8.0f; //10
            BtnPercent25.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent25.layer.shadowOpacity = 0.5f;
            BtnPercent25.layer.shadowRadius = 1.0f;
            BtnPercent25.layer.masksToBounds = NO;
            BtnPercent25.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent25];
            
            UIButton *BtnPercent50 =[UIButton buttonWithType:UIButtonTypeCustom];
            //pooja-iPad
            if(IS_IPAD)
                [BtnPercent50 setFrame:CGRectMake(330, 50, 30, 20)];
            else
                [BtnPercent50 setFrame:CGRectMake(220, 50, 30, 20)];
            [BtnPercent50 setTitle:@" 50%" forState:UIControlStateNormal];
            [BtnPercent50 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent50 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent50 setTag:50];
            BtnPercent50.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent50.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent50.layer.cornerRadius = 8.0f; //10
            BtnPercent50.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent50.layer.shadowOpacity = 0.5f;
            BtnPercent50.layer.shadowRadius = 1.0f;
            BtnPercent50.layer.masksToBounds = NO;
            BtnPercent50.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent50];
            
            UIButton *BtnPercent75 =[UIButton buttonWithType:UIButtonTypeCustom];
            //pooja-iPad
            if(IS_IPAD)
                [BtnPercent75 setFrame:CGRectMake(380, 50, 30, 20)];
            else
                [BtnPercent75 setFrame:CGRectMake(270, 50, 30, 20)];
            [BtnPercent75 setTitle:@" 75%" forState:UIControlStateNormal];
            [BtnPercent75 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent75 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent75 setTag:75];
            BtnPercent75.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent75.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent75.layer.cornerRadius = 8.0f; //10
            BtnPercent75.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent75.layer.shadowOpacity = 0.5f;
            BtnPercent75.layer.shadowRadius = 1.0f;
            BtnPercent75.layer.masksToBounds = NO;
            BtnPercent75.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent75];
 
        }
        else{//iOS 6
            
            UILabel *lblAlert1 =[[UILabel alloc]init];
            [lblAlert1 setFrame:CGRectMake(40,AddtionalView.frame.size.height/2-30,35,15)];
            [lblAlert1 setBackgroundColor:[UIColor clearColor]];
            [lblAlert1 setText:@"Alert 1:"];
            [lblAlert1 setFont:[UIFont boldSystemFontOfSize:10]];
            [AddtionalView addSubview:lblAlert1];
            alertsLabel = [[UILabel alloc]init];
            [alertsLabel setFrame:CGRectMake(40,AddtionalView.frame.size.height/2-15,120,15)];
            [alertsLabel setBackgroundColor:[UIColor clearColor]];
            [alertsLabel setFont:[UIFont boldSystemFontOfSize:10]];
            [alertsLabel setTextColor:[UIColor blueColor]];
            //[alertsLabel setText:editItem.alert];//Steve commented
            
            ///////////////////// Steve added //////////////////////////////
            
            _alertDate_1 = [self convertStringToDate:editItem.alert];
            
            NSDateFormatter *dateFormatter_1 = [[NSDateFormatter alloc] init];
            [dateFormatter_1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            _alertStringSaveFormat_1 = [dateFormatter_1 stringFromDate:_alertDate_1];
            
            if (_alertStringSaveFormat_1 == nil)
                _alertStringSaveFormat_1 = @"None";
            
            
            NSDateFormatter *dateFormatter_2 = [[NSDateFormatter alloc] init];
            [dateFormatter_2 setLocale:[NSLocale currentLocale]];
            [dateFormatter_2 setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter_2 setTimeStyle:NSDateFormatterShortStyle];
            
            //Converts to local format
            _alertString_1 = [dateFormatter_2 stringFromDate:_alertDate_1];
            
            if (_alertString_1 == nil)
                _alertString_1 = @"None";
            
            
            [alertsLabel setText:_alertString_1];
            ///////////////////// Steve End //////////////////////////////
            
            [AddtionalView addSubview:alertsLabel];
            
            
            UILabel *lblAlert2 =[[UILabel alloc]init];
            [lblAlert2 setFrame:CGRectMake(40, AddtionalView.frame.size.height/2,35,15)];
            [lblAlert2 setBackgroundColor:[UIColor clearColor]];
            [lblAlert2 setFont:[UIFont boldSystemFontOfSize:10]];
            [lblAlert2 setText:@"Alert 2:"];
            [AddtionalView addSubview:lblAlert2];
            secondAlertsLabel = [[UILabel alloc]init];
            [secondAlertsLabel setFrame:CGRectMake(40,AddtionalView.frame.size.height/2+15,120,15)];
            [secondAlertsLabel setBackgroundColor:[UIColor clearColor]];
            [secondAlertsLabel setFont:[UIFont boldSystemFontOfSize:10]];
            [secondAlertsLabel setTextColor:[UIColor blueColor]];
            //[secondAlertsLabel setText:editItem.alertSecond];//Steve commented
            
            
            ///////////////////// Steve added //////////////////////////////
            
            _alertDate_2 = [self convertStringToDate:editItem.alertSecond];
            
            _alertStringSaveFormat_2 = [dateFormatter_1 stringFromDate:_alertDate_2];
            
            if (_alertStringSaveFormat_2 == nil)
                _alertStringSaveFormat_2 = @"None";
            
            
            [dateFormatter_2 setLocale:[NSLocale currentLocale]];
            [dateFormatter_2 setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter_2 setTimeStyle:NSDateFormatterShortStyle];
            
            //Converts to local format
            _alertString_2 = [dateFormatter_2 stringFromDate:_alertDate_2];
            
            if (_alertString_2 == nil)
                _alertString_2 = @"None";
            
            
            [secondAlertsLabel setText:_alertString_2];
            /////////////////////// Steve End //////////////////////////////
            
            [AddtionalView addSubview:secondAlertsLabel];
            
            
            slider = [[UISlider alloc]initWithFrame:CGRectMake(165, 22, 140, 5)]; //Steve (150, 22, 140, 5)
            [slider addTarget:self action:@selector(sliderValueChanged:)
             forControlEvents:UIControlEventValueChanged];
            [slider setMinimumValue:0];
            [slider setMaximumValue:100];
            [slider setValue:editItem.toDoProgress];
            [AddtionalView addSubview:slider];
            
            lblPercent = [[UILabel alloc]initWithFrame:CGRectMake(slider.frame.origin.x+35, slider.frame.origin.y-20, 80,20)];
            [lblPercent setBackgroundColor:[UIColor clearColor]];
            [lblPercent setFont:[UIFont fontWithName:@"Arial" size:10]];
            [lblPercent setText:[NSString stringWithFormat:@"%d%@ Complete",editItem.toDoProgress,@"%"]];
            [AddtionalView addSubview:lblPercent];
            
            
            UIColor *BtnPercentColor = [UIColor colorWithRed:193.0f/255.0f green:136.0f/255.0f blue:12.0f/255.0f alpha:1.0f]; //Steve
            
            
            UIButton *BtnPercent25 =[UIButton buttonWithType:UIButtonTypeCustom];
            [BtnPercent25 setFrame:CGRectMake(slider.frame.origin.x, slider.frame.origin.y+25, 30, 20)];
            [BtnPercent25 setTitle:@" 25%" forState:UIControlStateNormal];
            [BtnPercent25 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent25 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent25 setTag:25];
            BtnPercent25.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent25.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent25.layer.cornerRadius = 8.0f; //10
            BtnPercent25.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent25.layer.shadowOpacity = 0.5f;
            BtnPercent25.layer.shadowRadius = 1.0f;
            BtnPercent25.layer.masksToBounds = NO;
            BtnPercent25.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent25];
            
            UIButton *BtnPercent50 =[UIButton buttonWithType:UIButtonTypeCustom];
            [BtnPercent50 setFrame:CGRectMake(slider.frame.origin.x+55, slider.frame.origin.y+25, 30, 20)];
            [BtnPercent50 setTitle:@" 50%" forState:UIControlStateNormal];
            [BtnPercent50 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent50 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent50 setTag:50];
            BtnPercent50.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent50.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent50.layer.cornerRadius = 8.0f; //10
            BtnPercent50.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent50.layer.shadowOpacity = 0.5f;
            BtnPercent50.layer.shadowRadius = 1.0f;
            BtnPercent50.layer.masksToBounds = NO;
            BtnPercent50.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent50];
            
            UIButton *BtnPercent75 =[UIButton buttonWithType:UIButtonTypeCustom];
            [BtnPercent75 setFrame:CGRectMake(slider.frame.origin.x+110, slider.frame.origin.y+25, 30, 20)];
            [BtnPercent75 setTitle:@" 75%" forState:UIControlStateNormal];
            [BtnPercent75 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [BtnPercent75 addTarget:self
                             action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
            [BtnPercent75 setTag:75];
            BtnPercent75.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
            BtnPercent75.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
            BtnPercent75.layer.cornerRadius = 8.0f; //10
            BtnPercent75.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
            BtnPercent75.layer.shadowOpacity = 0.5f;
            BtnPercent75.layer.shadowRadius = 1.0f;
            BtnPercent75.layer.masksToBounds = NO;
            BtnPercent75.clipsToBounds = NO;
            [AddtionalView addSubview:BtnPercent75];

        }
        
        /*
        if (IS_IOS_7) {
            
            slider.frame = CGRectMake(165, 22 + 6, 140, 5);
            lblPercent.frame = CGRectMake(slider.frame.origin.x+35, slider.frame.origin.y-27, 80,20);
            [BtnPercent25 setFrame:CGRectMake(slider.frame.origin.x, slider.frame.origin.y+22, 30, 20)];
            [BtnPercent50 setFrame:CGRectMake(slider.frame.origin.x+55, slider.frame.origin.y+22, 30, 20)];
            [BtnPercent75 setFrame:CGRectMake(slider.frame.origin.x+110, slider.frame.origin.y+22, 30, 20)];
            
        }
         */
        //////END /Alok Added Additional View for Alert & Completion/////
        
    }
    
    
    [cellVar setSelected:NO animated:YES];  // maybe, maybe not
    
    if (tapTimer != nil)
    {
        tapCount = 0;
        tappedRow = -1;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float height;
    
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){ 
		height = TABLE_ROW_HEIGHT_IPHONE; //Steve - was TABLE_ROW_HEIGHT_IPAD
    }else {
		height = TABLE_ROW_HEIGHT_IPHONE;
	}
    
    return height;
	
}


- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section 
{    
	if([todoDateObjArray count]>0)
        return [todoDateObjArray count];
    if(searchBarViewController)
        return 0;
    else
        
        return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSInteger row;
    row = [indexPath row];
    
    //Anil's Added
    
    if([todoDateObjArray count]==0)
    {
        [todoDateObjArray addObject:ADDING_CELL];
    }
    todoDataObj = [todoDateObjArray objectAtIndex:row];
    
    
    UIColor *backgroundColor = [UIColor whiteColor];
    
    //*****************************************************************************
    //Adds a Row on Top
    if ([todoDataObj isEqual: ADDING_CELL]) 
    {
        
        //NSLog(@"[todoDataObj isEqual: ADDING_CELL");
        NSString *cellIdentifier = nil;
        TransformableTableViewCell *cell = nil;
        
        
        //Steve added
        //Clears all subviews so when checked (progress = 100% complete) they don't overlap textlabels, buttons, etc
        for (UIView  *subViews in [cell.contentView subviews])
        {
            [subViews removeFromSuperview];
        }
        
        
        
        // IndexPath.row == 0 is the case we wanted to pick the pullDown style
        if (indexPath.row == 0)
        {
            
            //  NSLog(@"indexPath.row == 0");
            cellIdentifier = @"PullDownTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            
            if (cell == nil) 
            {
                cell = [TransformableTableViewCell transformableTableViewCellWithStyle:TransformableTableViewCellStylePullDown reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                cell.textLabel.textColor = [UIColor grayColor];//Steve
            }
            
            // Setup tint color
            cell.tintColor = backgroundColor;
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT)
            {
                cell.textLabel.font =   myFontName;
                cell.textLabel.textColor = [UIColor grayColor];
                cell.tintColor = myCellBackgroundColor; //Steve added
                cell.textLabel.text = @"  Release to create cell...";
                
            } else {
                //Anil's Added
                
                //NSLog(@"Frame %@",[cell.textLabel description]);
                if([todoDateObjArray count]==1)
                {
                    cell.textLabel.font =   myFontName;
                    cell.textLabel.text = PLACEHOLDER;
                    cell.textLabel.textAlignment = NSTextAlignmentCenter;
                    [cell.contentView.layer setSublayerTransform:CATransform3DIdentity];
                    
                }
                else
                    
                    cell.textLabel.text = @"  Continue Pulling...";
                    cell.tintColor = myCellBackgroundColor; //Steve added
                    cell.textLabel.textColor = [UIColor grayColor];
                
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.detailTextLabel.text = @"";
            
            return cell;
        } 
        //****************************************************************
        // For adding a cell by pinching in the middle of two cells
        else 
        {
            // NSLog(@"UnfoldingTableViewCell");
            
            // Otherwise is the case we wanted to pick the pullDown style
            cellIdentifier = @"UnfoldingTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            
            //Steve added
            //Clears all subviews so when checked (progress = 100% complete) they don't overlap textlabels, buttons, etc
            for (UIView  *subViews in [cell.contentView subviews])
            {
                [subViews removeFromSuperview];
            }
            
            
            
            if (cell == nil) 
            {
                cell = [TransformableTableViewCell transformableTableViewCellWithStyle:TransformableTableViewCellStyleUnfolding reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                //Steve changed was: cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font =   myFontName;
                cell.textLabel.textColor = myFontColor;
                // cell.textLabel.textAlignment = UITextAlignmentCenter;
            }
            
            // Setup tint color
            cell.tintColor = backgroundColor;
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT)
            {
                cell.textLabel.text = @"  Release to create cell...";
                cell.tintColor = myCellBackgroundColor; //Steve
                cell.textLabel.textColor = [UIColor grayColor];//Steve
                
            } else
            {
                      //Anil's Added
                if([todoDateObjArray count]==2)
                {
                    cell.textLabel.font =   myFontName;
                    cell.textLabel.text = PLACEHOLDER;
                    cell.textLabel.textAlignment = UITextAlignmentCenter;
                    [cell.contentView.layer setSublayerTransform:CATransform3DIdentity];
                }
                
                else
                    
                    cell.textLabel.text = @"  Continue Pinching...";
                    cell.textLabel.textColor = [UIColor grayColor];
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.detailTextLabel.text = @" ";
            
            return cell;
        }
        
        /*****************************************************************************/
        //Not Adding Cell???
    } else
    {
        
        static NSString *cellIdentifier = @"MyCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        //Steve added
        //Clears all subviews so when checked (progress = 100% complete) they don't overlap textlabels, buttons, etc
        for (UIView  *subViews in [cell.contentView subviews])
        {
            [subViews removeFromSuperview];
        }
        
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            cell.textLabel.backgroundColor = [UIColor clearColor]; 
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = myFontColor;
        }
        
        //cell.textLabel.text = [NSString stringWithFormat:@"%@", (NSString *)todoDateObjArray];
        //cell.detailTextLabel.text = @" ";
        
        
        
        NSLog(@"cell.textLabel.text = %@", cell.textLabel.text);
        
        
        //Done cell
        if ([todoDataObj isEqual:DONE_CELL]) 
        {
            //NSLog(@" todoDataObj isEqual:DONE_CELL");
            
            //Steve - Clears all subviews so DUMMY_CELL is Not visible
            for (UIView  *subViews in [cell.contentView subviews])
            {
                [subViews removeFromSuperview];
            }
            
            cell.textLabel.textColor = [UIColor grayColor];
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        
        
        //DUMMY_CELL is used when LONG holding a cell and then moving it.  This dummy cell is the background cell
        //Therefore, all cell views should Not show
        if ([todoDataObj isEqual:DUMMY_CELL])
        {
            //NSLog(@" todoDataObj isEqual:DUMMY_CELL");
            
            cell.textLabel.text = @"";
            cell.contentView.backgroundColor = myBackgroundColor;
            
            
            //Clears all subviews so DUMMY_CELL is Not visible
            for (UIView  *subViews in [cell.contentView subviews]) 
            {
                [subViews removeFromSuperview];
            }
            
        } 
        else 
            //*************************************************************************
            //Normal Cell State
            //*************************************************************************
        {
            
            cell.contentView.backgroundColor = backgroundColor;
            
            
            //////////////Steve added /////////////////////////////
            
            //makes button size of cell so I can add borders to cell while keeping blank cells all black
            //otherwise blank cells will be color of the cell line separator
            UIButton *cellButton;
            UIButton *eyeBorderButton;
            
#pragma mark -- IPAd Primotech
            
            if(IS_IPAD)
            {
                cellButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 460, TABLE_ROW_HEIGHT_IPHONE)]; //0,0,446
                eyeBorderButton = [[UIButton alloc] initWithFrame:CGRectMake(370, 0, 70, TABLE_ROW_HEIGHT_IPHONE)];
            }
            else
            {
                cellButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, TABLE_ROW_HEIGHT_IPHONE)];
                eyeBorderButton = [[UIButton alloc] initWithFrame:CGRectMake(260, 0, 70, TABLE_ROW_HEIGHT_IPHONE)];
            }
            
            //added so cell will recognize touch instead of Button and didSelectRowAtIndexPath method will be invoked for Edit
            [cellButton setUserInteractionEnabled:NO];
            
            cellButton.layer.borderWidth = 1;
            cellButton.layer.borderColor = [[UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]CGColor];
            cellButton.layer.backgroundColor = [myCellBackgroundColor CGColor];
            
            [cell.contentView addSubview:cellButton];
            
            
            /////////// Steve added 12-17-12  ///////////////////
            //Adds "eye" icon for displaying popup
            
            //eyeBorderButtton is an invisible button with a larger CGRect than the eyeButton to make clicking easier. Otherwise it can be diffult to click the eyeButton
           
            
            [eyeBorderButton setUserInteractionEnabled:YES];
            [eyeBorderButton setTag:indexPath.row];
            [eyeBorderButton addTarget:self action:@selector(eyeButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:eyeBorderButton];
            
            
            UIImage *eyeImage = [UIImage imageNamed:@"Eye-icon_OLD.png"]; //Steve - keeps the original eye icon
            
            UIButton *eyeButton;
            
            if(IS_IPAD)
            {                                                         //Steve 400, was 380
                eyeButton = [[UIButton alloc] initWithFrame:CGRectMake(410 - eyeImage.size.width/2 , TABLE_ROW_HEIGHT_IPHONE/2 - eyeImage.size.height/2, 29, 16)];
            }
            else
            {
                eyeButton = [[UIButton alloc] initWithFrame:CGRectMake(280 - eyeImage.size.width/2 , TABLE_ROW_HEIGHT_IPHONE/2 - eyeImage.size.height/2, 29, 16)];
            }
            
            [eyeButton setBackgroundImage:eyeImage forState:UIControlStateNormal];
            eyeButton.userInteractionEnabled = YES;
            [eyeButton setTag:indexPath.row];
            [eyeButton addTarget:self action:@selector(eyeButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];

            [cell.contentView addSubview:eyeButton];
            
            //////////////Steve added End /////////////////////////////
            
            //Progress (Check box)
            UIImage *proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBG" ofType:@"png"]];
            UIImageView *proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
            [proIndBorder setTag:9909];
            
            UIButton *progressIndBtn = [[UIButton alloc] init];
            [progressIndBtn setBackgroundColor:[UIColor redColor]];
            [progressIndBtn setTag:Progress_BTN_TAG];
            [cell.contentView addSubview:proIndBorder];
            
            
            [proIndBorder setFrame:CGRectMake(5, TABLE_ROW_HEIGHT_IPHONE/2 - proIndBorderImage.size.width/2, proIndBorderImage.size.width, proIndBorderImage.size.height)];
            
            float buttonHeight = proIndBorderImage.size.height - 4;
            percentComplete = [todoDataObj toDoProgress];
            float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
            
            [progressIndBtn setFrame:CGRectMake(7, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, proIndBorderImage.size.width - 4, finalHieght)];
            
            if (percentComplete == 100) 
            {
                UIImage *btnBgImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
                [progressIndBtn setBackgroundColor:[UIColor clearColor]];
                [progressIndBtn setImage:btnBgImage forState:UIControlStateNormal];
                [proIndBorder setHidden:YES];
                [progressIndBtn setFrame:proIndBorder.frame];
                progressIndBtn.alpha=0.50;
            }
            
            [cell.contentView addSubview:progressIndBtn];
            
            
            //  NSLog(@"todoDataObj.todoTitleType = %@", todoDataObj.todoTitleType);
            if ([todoDataObj.todoTitleType isEqualToString:@"H"]) 
            {
                //Clears the cell text
                cell.textLabel.text = nil;
                
                UIImageView *titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15 + proIndBorderImage.size.width + 5, TABLE_ROW_HEIGHT_IPHONE/2 - 30/2, tableView.frame.size.width - 100,30)];
                
                UIImage *titleImage = [[UIImage alloc] initWithData:[todoDataObj todoTitleBinary]];
                [titleImageView setImage:titleImage];
                
                if (percentComplete == 100) 
                {
                    titleImageView.alpha=0.50;
                }
                
                [cell.contentView addSubview:titleImageView];
                
            }
            else 
            {
                //Clears the cell text
                cell.textLabel.text = nil;
                
                UILabel *titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(15 +  proIndBorderImage.size.width + 5, TABLE_ROW_HEIGHT_IPHONE/2 - 30/2,tableView.frame.size.width - 100, 30)];
                [titlelabel setBackgroundColor:[UIColor clearColor]];
                [titlelabel setFont:myFontName];
                [titlelabel setTextColor:myFontColor];
                [titlelabel setText:[todoDataObj todoTitle]];
                [titlelabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth]; //Steve added
                
               // cell.textLabel.text = todoDataObj.todoTitle; //Steve Test
                
                //NSLog(@"cell.textLabel.text = %@", cell.textLabel.text);
                NSLog(@"titlelabel.text = %@", titlelabel.text);
                
                
                if(percentComplete==100)
                {
                    titlelabel.textColor = [UIColor grayColor];
                }
                
                [cell.contentView addSubview:titlelabel];
            }
            
            
            //Invisible button for progressBtn_Clicked
            UIButton *invisibleIndBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, proIndBorder.frame.origin.y, 30, 30)];
            [invisibleIndBtn setTag:indexPath.row];
            [invisibleIndBtn addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *viewmB=[[UILabel alloc]init];
            viewmB.text =[NSString stringWithFormat:@"%d~%d",todoDataObj.todoID,todoDataObj.toDoProgress];
            viewmB.tag =666;
            [invisibleIndBtn addSubview:viewmB];
            [cell.contentView addSubview:invisibleIndBtn];
            
            
        }
        //*************************************************************************
        //Normal Cell State
        //*************************************************************************
        
        cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero]; // Anil's Addition
        cell.backgroundView.backgroundColor = myCellBackgroundColor;  //Steve modified
        
        
        //NSLog(@"todoDateObjArray count = %d", todoDateObjArray.count);
        //NSLog(@"indexPath.row  = %d", indexPath.row);
        
        //NSLog(@"cell.textLabel.text = %@", cell.textLabel.text);
        //NSLog(@"titlelabel.text = %@", titlelabel.text);
        
        return cell;
    }
    
}


//Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath 
{
	// Return NO if you do not want the specified item to be editable.
	return YES;
}


//Anils Added
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView  editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[todoDateObjArray objectAtIndex:indexPath.row]isKindOfClass:[NSString class]])
    {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        //NSLog(@"commitEditingStyle");
        // NSLog(@"todoDateObjArray count = %d",[todoDateObjArray count]);
        if ([todoDateObjArray count] == 0)
        {
            //NSLog(@"todoDateObjArray count == 0 ---> %d",[todoDateObjArray count]);
            [tableView reloadData];  
            return;
        }
        
		BOOL isDeleted = [AllInsertDataMethods deleteToDoWithID:[[todoDateObjArray objectAtIndex:[indexPath row]] todoID]];
        
        if (isDeleted == YES) 
        {
            //NSLog(@"Delete cellID ->%d",[[todoDateObjArray objectAtIndex:[indexPath row]] todoID]);
            //Alok Added to delete Local notification
            UIApplication *app = [UIApplication sharedApplication];
            NSArray *notificationArray = [app scheduledLocalNotifications];
            
            UILocalNotification *row = nil;
            for (row in notificationArray) {
                NSDictionary *userInfo = row.userInfo;
                NSString *identifier = [userInfo valueForKey:@"toDoID"];
                if([identifier intValue]==[[todoDateObjArray objectAtIndex:[indexPath row]] todoID]) {
                   // NSLog(@"Found a match!");
                    [app cancelLocalNotification:row];
                }
            }	
            
            
            
            //  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            
            [todoDateObjArray removeObjectAtIndex:indexPath.row];
            // NSLog(@"todoDateObjArray count = %d",[todoDateObjArray count]);
            //NSLog(@"todoDateObjArray index.row = %d",[indexPath row]);
            
            //Steve added animation
            if ([todoDateObjArray count] > 0)
            {
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
            
            
            
		}
	} 
	else if (editingStyle == UITableViewCellEditingStyleInsert) 
	{
		
	}   
    //Steve added. If last row being deleted, then reload table
    //if (indexPath.row == 0)
    /*
    if ([todoDateObjArray count] == 0)
    {
        [tableView reloadData];   
    }
    */
    
    [tableView reloadData];  //Steve added 12-17-12. Fixed a bug where after deleting a to-do, the indexPath was off
    //NSLog(@"todoDateObjArray count = %d",[todoDateObjArray count]);
	return;
}



#pragma mark -
#pragma mark Database Transaction 
#pragma mark -
-(void)getValuesFromDatabase 
{
	
	[todoDateObjArray removeAllObjects];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectAllTODO = nil;
	NSString *Sql = @"";
    
    //Steve added
    NSString *selectStr = @"Select a.TODOID, a.Title, a.TitleTextType, a.TitleBinary, a.BelongstoCalenderID, a.BelongstoProjectID, a.DateStarted, a.DueDate, a.DateCreated,  a.DateModified, a.Progress, IFNULL(a.Priority, ''), a.IsStarred, a.ManuallySortTodoID, IFNULL(b.projectName, 'ZZZ'), IFNULL(c.CalendarName, ''), a.TitleBinaryLarge ,b.level,a.Alert,a.AlertSecond,a.isAlertOn,IFNULL(b.folderImage, 'FolderColor1'),a.PerntID From ToDoTable a Left Outer Join ProjectMaster b on a.PerntID = b.ProjectID  Left Outer Join CalendarTable c on a.BelongstoCalenderID = c.CalendarID";
    
    
    //Steve added
    //"PertID" is assigned to the Parent Project. This method shows all Parent Todo's
    NSString  *whereClauseStr = [NSString stringWithFormat:@"Where a.PerntID = %d ", projectIDFromProjMod];
    
    
   // NSLog(@"projectIDFromProjMod = %d", projectIDFromProjMod);
    
    
	if(selectAllTODO == nil && appDelegate.database) 
    {
        
        Sql = [NSString stringWithFormat:@"%@ %@  Order By IFNULL(b.ProjectName, 'ZZZ'), SubStr(a.DueDate, 1, 10) Asc",selectStr, whereClauseStr];
        
       // NSLog(@"THe Final Query ::: %@", Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAllTODO, NULL) == SQLITE_OK) {
            
			
			while (sqlite3_step(selectAllTODO) == SQLITE_ROW) {
				
				NSUInteger blobLength = sqlite3_column_bytes(selectAllTODO, 3);
				NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectAllTODO, 3) length:blobLength];
				
				NSUInteger blobLengthLarge = sqlite3_column_bytes(selectAllTODO, 16);
				NSData *binaryDataLarge = [NSData dataWithBytes:sqlite3_column_blob(selectAllTODO, 16) length:blobLengthLarge];
				
				TODODataObject *toDODataObject = [[TODODataObject alloc] init];
				
				[toDODataObject setTodoID:sqlite3_column_int(selectAllTODO, 0)];
				[toDODataObject setTodoTitle:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 1) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 1)]];
				
				[toDODataObject setTodoTitleType:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 2)]];
				[toDODataObject setTodoTitleBinary:binaryData];
				
				[toDODataObject setCalID:sqlite3_column_int(selectAllTODO, 4)];
				[toDODataObject setProjID:sqlite3_column_int(selectAllTODO, 5)];
                
                
                // NSLog(@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 6)]);
				
				[toDODataObject setStartDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 6)]];
				
                // NSLog(@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 7)]);
                
                //  NSLog(@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 7)]);
				[toDODataObject setDueDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 7)]];
                
                
				[toDODataObject setCreateDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 8)]];
				
				[toDODataObject setModifyDateTime:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 9) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 9)]];
				[toDODataObject setModifyDate:[self convertStringToDate:toDODataObject.modifyDateTime]];
                
				[toDODataObject setToDoProgress:sqlite3_column_int(selectAllTODO, 10)];
				
                
				[toDODataObject setPriority:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 11) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 11)]];
				
				[toDODataObject setIsStarred:sqlite3_column_int(selectAllTODO, 12)];
				[toDODataObject setManuallySortedTodoID:sqlite3_column_int(selectAllTODO, 13)];
				
				[toDODataObject setProjectName:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 14) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 14)]];
				[toDODataObject setCalendarName:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 15) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 15)]];
				
				[toDODataObject setTodoTitleBinaryLarge:binaryDataLarge];
				
                [toDODataObject setAlert:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 18)]]; // Anil's Addition
                [toDODataObject setAlertSecond:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 19)]]; // Anil's Addition
                
                
                //NSLog(@"alert =  %@", [toDODataObject todoTitle]);
                //NSLog(@"modifyDate =  %@", [toDODataObject modifyDate]);
                
                
                
                
                [toDODataObject setIsAlertOn:sqlite3_column_int(selectAllTODO, 20)];// Anil's Addition
                
                /*----------------------------------------------------------------------------*/
                NSInteger pd=sqlite3_column_int(selectAllTODO, 5);
				[toDODataObject setProjID:sqlite3_column_int(selectAllTODO, 5)];
                [toDODataObject setProjPrntID:sqlite3_column_int(selectAllTODO, 22)];
                
                if(pd)
                {
                    myArray=[[NSMutableArray alloc]init ];
                    y=[self fetch_level_one:pd];
                    yy=Pname;
                    [myArray addObject:yy];
                    if((y)&&(y!=0))
                    {
                        x= [self fetch_level_one:y];
                        xx=Pname;
                        [myArray addObject:xx];
                        if((x)&&(x!=0))
                        {
                            u=[self fetch_level_one:x];
                            uu=Pname;
                            [myArray addObject:uu];
                            if((u)&&(u!=0))
                            {
                                v= [self fetch_level_one:u];
                                vv=Pname;
                                [myArray addObject:vv];
                            }
                        }
                    }
                }
                
                // Append All folder in A String By Naresh
                NSString *finalText=[[NSString alloc]init];
                for(int k=[myArray count] - 1;k>=0; k--)
                {
                    finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
                }
                
                if ( [finalText length] > 0)
                    finalText = [finalText substringToIndex:[finalText length] - 1];
                
                [toDODataObject setFullprojPath:finalText];
                [myArray removeAllObjects];
                
                /*----------------------------------------------------------------------------*/
                
                
				[todoDateObjArray addObject:toDODataObject];
                
                //NSLog(@"toDODataObject.todoTitle = %@", toDODataObject.todoTitle);
                
                //NSLog(@"toDODataObject.projectName = %@", toDODataObject.projectName);
                //NSLog(@"toDODataObject.tprojID = %d", toDODataObject.projID);
               // NSLog(@"toDODataObject.projPrntID = %d", toDODataObject.projPrntID);
               // NSLog(@"toDODataObject.projLevel = %@", toDODataObject.projLevel);
                
                
			}
            
            
            
			sqlite3_finalize(selectAllTODO);		
			selectAllTODO = nil;
		}
    }
	selectAllTODO = nil;
    
    
    NSSortDescriptor * frequencyDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifyDate" ascending:NO];
    NSArray * descriptors =
    [NSArray arrayWithObjects:frequencyDescriptor, nil];
    NSArray * sortedArray = [todoDateObjArray sortedArrayUsingDescriptors:descriptors];
    
    [todoDateObjArray removeAllObjects];
    
    todoDateObjArray = [NSMutableArray arrayWithArray:sortedArray];
    
	[toDoTableView reloadData];
    
	return;
}


//Steve added - copied from Todo view 
-(NSInteger)fetch_level_one:(NSInteger)projid
{
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    if (selected_stmt == nil && appDelegate.database)
    {
        NSString *  Sql = [[NSString alloc]initWithFormat:@"SELECT  belongstoId,projectName from ProjectMaster where projectID =%d",projid];
        // NSLog(@"%@",Sql);
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW)
            {
                retVal= sqlite3_column_int(selected_stmt, 0);
			    Pname= [NSString stringWithUTF8String:(char *) sqlite3_column_text(selected_stmt,1)];
                // NSLog(@"%d   =  %@",retVal,Pname);
                
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    return retVal;
}

#pragma mark -
#pragma mark JTTableViewGestureAddingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsAddRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // NSLog(@"gestureRecognizer needsAddRowAtIndexPath");
    //NSLog(@"Adding row at indexPath.row = %d", indexPath.row);
    
    [todoDateObjArray insertObject:ADDING_CELL atIndex:indexPath.row];
    
    
    //NSLog(@"Starting method %@",todoDateObjArray);
    
}

/****************************************************/
//Cell Data????

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCommitRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //Cell Data?
    NSString *selectStat=@"select count(TODOID) from ToDoTable";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"To Do Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d todos.",MAX_LIMIT_TODO] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        
        [alert setTag:501];
        [alert show];
        return;
    }
    
    
    
    // NSLog(@"gestureRecognizer needsCommitRowAtIndexPath");
    // NSLog(@"commiting row at indexPath.row = %d", indexPath.row);
    
    
    indexWhereCellIsBeingInserted = indexPath.row;
    
    [self performSelector:@selector(createAddView) withObject:nil afterDelay:0.001];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 501)
    {
        NSString *selectStat=@"select count(TODOID) from ToDoTable";
        int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
        if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
        {
            
            
            if([todoDateObjArray containsObject:ADDING_CELL])
            {
                [todoDateObjArray removeObject:ADDING_CELL];
                
            }
            
            [toDoTableView reloadData];
            
            [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
            [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
            [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
            [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
            [[self.view viewWithTag:DONE_BUTTON_TAG] removeFromSuperview];
            
            
            
            isKeyBoardDone = YES;
            
            return;
        }
        
    }
    if(buttonIndex==1)
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];
}


-(void)createAddView
{
    
    //***************************************** 
    //set up Input view and fields
    //*****************************************
    
   // NSLog(@"todoTitleType = %@", todoTitleType);
    
    //Steve - fixes iPad issues after I added HW code
    if (!todoTitleType) {
        todoTitleType = @"T";
    }
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //new view and input textfield to enter new todo
    UIView *inputView;
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        //pooja-iPad
        if(IS_IPAD)
            inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 459, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
        else
            inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 320, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
    else
        if(IS_IPAD)
            inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 , 459, NORMAL_CELL_FINISHING_HEIGHT*2+20)];
        else
            inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42, 320, NORMAL_CELL_FINISHING_HEIGHT*2+20)];

    //inputView.backgroundColor = [UIColor whiteColor];//Steve commented
    inputView.backgroundColor = myCellBackgroundColor; //Steve
    [self.view addSubview:inputView];
    
    [inputView setTag:INPUT_VIEW_TAG];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        //pooja-iPad
        if(IS_IPAD)
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10 - 42,400,NORMAL_CELL_FINISHING_HEIGHT-20)];
        else
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42+10 -42,265,NORMAL_CELL_FINISHING_HEIGHT-20)];
    else
        //pooja-iPad
        if(IS_IPAD)
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10,400,NORMAL_CELL_FINISHING_HEIGHT-20)];
        else
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42+10 ,265,NORMAL_CELL_FINISHING_HEIGHT-20)];
    
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.font = myFontName;
    inputTextField.textColor = myFontColor;
    [self.view addSubview:inputTextField];
    
    [inputTextField setTag:TEXT_FIELD_TAG];
    [inputTextField setReturnKeyType:UIReturnKeyDone];
    [inputTextField becomeFirstResponder];
    [inputTextField addTarget:self action:@selector(keyBoardDone:)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    
    if([todoTitleType isEqualToString: @"H"] && todoTitleBinary) {
		[inputTextField setPlaceholder: @""];
		UIImage *tempTitleImage = [[UIImage alloc] initWithData:todoTitleBinary];
		[inputTextField setBackground:tempTitleImage];
		[inputTextField setText:@""];
		[inputTextField resignFirstResponder];
        [inputTextField setUserInteractionEnabled:NO];
    }
    else if (_isHandwriting){ //Steve iPad - coming from HW button on NavBar, but image not present yet.
        [inputTextField setPlaceholder: @""];
        [inputTextField setText:@""];
        [inputTextField resignFirstResponder];
        [inputTextField setUserInteractionEnabled:NO];
        
    }
    else if([todoTitleType isEqualToString: @"V"]){
        [inputTextField setText:todoTitle];
    }
    
    UIButton *inputSelecterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //pooja-iPad
    if(IS_IPAD)
        [inputSelecterButton setFrame:CGRectMake(400,15 ,30,30)]; //Steve - adjust x to 400
    else
        [inputSelecterButton setFrame:CGRectMake(280,15 ,30,30)];
    
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(!appdelegate.IsVTTDevice || !appdelegate.isVTTDeviceAuthorized)
        [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon_pen.png"] forState:UIControlStateNormal];
    else
        [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon2.png"] forState:UIControlStateNormal];
    
    [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [inputView addSubview:inputSelecterButton];
    
    
    //black opaque space between input box and keyboard
    UIView *blackBox;
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        if(IS_IPAD)
        {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 459, 1024)];
            //NSLog(@"blackbox -- %@",NSStringFromCGRect(blackBox.frame));
        }
        else
        {
        if (IS_IPHONE_5) {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 320, 284 + 88)];
        }
        else{
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 320, 284)];
        }
        }
    }
    else{
        if(IS_IPAD)
        {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 459, 1024)];
            //NSLog(@"blackbox -- %@",NSStringFromCGRect(blackBox.frame));
        }
        else
        {
        if (IS_IPHONE_5) {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 320, 284 + 88)];
        }
        else{
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 320, 284)];
        }
        }
    }

    
    
    //UIKeyboardFrameEndUserInfoKey
    blackBox.backgroundColor = [UIColor blackColor];
    blackBox.alpha = 0.00;  //Steve changed was: 0.90
    blackBox.tag = BLACK_BOX_TAG;
    [self.view addSubview:blackBox];
    [UIView animateWithDuration:0.35 animations:^{blackBox.alpha = 0.90f;}];  //Steve added to animate blackBox
    
    
    UIButton *blackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    blackButton.backgroundColor = [UIColor clearColor]; //Steve
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        //pooja-iPad
        if(IS_IPAD)
            [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38 - 44, 459, 800)];//Steve 800, was 350
        else
            [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20+ 38 - 44, 320, 284)];
    else
        //pooja-iPad
        if(IS_IPAD)
            [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20 + 38, 459, 800)];
        else
            [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT*2+20+ 38, 320, 284)];
    
    [blackButton addTarget:nil action:@selector(saveToDoToDB:) forControlEvents:UIControlEventTouchUpInside];
    [blackButton setTag:BLACK_BUTTON_TAG];
    [self.view addSubview:blackButton];
    
    
    /////Start// Alok Added Additional View for Alert & Completion /////
    UIView * AddtionalView = [[UIView alloc]init];
    //pooja-iPad
    if(IS_IPAD)
        AddtionalView.frame = CGRectMake(0,NORMAL_CELL_FINISHING_HEIGHT, 459, NORMAL_CELL_FINISHING_HEIGHT+20);
    else
         AddtionalView.frame = CGRectMake(0,NORMAL_CELL_FINISHING_HEIGHT, 320, NORMAL_CELL_FINISHING_HEIGHT+20);
    
    AddtionalView.backgroundColor =myCellBackgroundColor;
    [AddtionalView bringSubviewToFront:self.view];
    [inputView addSubview:AddtionalView];
    UIImageView * imgblack;
    
    imgblack = [[UIImageView alloc]init];
    
    //pooja-iPad
    if(IS_IPAD)
        imgblack.frame = CGRectMake(0, 0, 459, 2);
    else
        imgblack.frame = CGRectMake(0, 0, 320, 2);
    
    [imgblack setBackgroundColor:[UIColor blackColor]];
    [AddtionalView addSubview:imgblack];
    
    UIButton *alertBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [alertBtn setFrame:CGRectMake(5, AddtionalView.frame.size.height/2-15, 30, 30)];
    [alertBtn setBackgroundImage:[UIImage imageNamed:@"alert_clock.png"] forState:UIControlStateNormal];
    [alertBtn addTarget:nil action:@selector(alerts_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [AddtionalView addSubview:alertBtn];
    
    ////////////// Steve Added /////////////////////////
    //Adds button (disabled) so user knows to click "blackBox" to save
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //pooja-iPad
    if(IS_IPAD)
        [doneButton setFrame:CGRectMake(175, 680, 109, 35)];
    else
        [doneButton setFrame:CGRectMake(114, 367, 93, 35)]; //(114, 350, 93, 35)
    [doneButton setUserInteractionEnabled:NO];
    
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
    doneButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    doneButton.layer.cornerRadius = 8.0;//10
    doneButton.layer.borderWidth = 1.0;
    doneButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    doneButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    doneButton.alpha = 0.0f;  
    [doneButton setTag:6667];
    [self.view addSubview:doneButton];
    [UIView animateWithDuration:0.5 animations:^{doneButton.alpha = 1.0f;}];
    ////////////// Steve Added End /////////////////////////
    
    
    
    
    //removes Slider & re-organize % buttons & alert label
    UILabel *lblAlert1 =[[UILabel alloc]init];
    [lblAlert1 setFrame:CGRectMake(40 + 5,5,55,15)];//(40 + 5,AddtionalView.frame.size.height/2-30,55,15)
    [lblAlert1 setBackgroundColor:[UIColor clearColor]];
    [lblAlert1 setText:@"Alert 1:"];
    [lblAlert1 setFont:[UIFont boldSystemFontOfSize:12]];
    [AddtionalView addSubview:lblAlert1];
    
    alertsLabel = [[UILabel alloc]init];
    [alertsLabel setFrame:CGRectMake(45,18,170,15)];
    [alertsLabel setBackgroundColor:[UIColor clearColor]];
    [alertsLabel setFont:[UIFont boldSystemFontOfSize:15]];//10
    [alertsLabel setTextColor:[UIColor blueColor]];
    [alertsLabel setText:@"None"];
    [AddtionalView addSubview:alertsLabel];
    
    UIButton *invisibleButtonAlert_1 = [UIButton buttonWithType:UIButtonTypeCustom];
    invisibleButtonAlert_1.frame = CGRectMake(alertsLabel.frame.origin.x, alertsLabel.frame.origin.y - 10,
                                              alertsLabel.frame.size.width, alertsLabel.frame.size.height + 10);
    invisibleButtonAlert_1.backgroundColor = [UIColor clearColor];
    [invisibleButtonAlert_1 addTarget:self action:@selector(alerts_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [AddtionalView addSubview:invisibleButtonAlert_1];
    
    
    UILabel *lblAlert2 =[[UILabel alloc]init];
    [lblAlert2 setFrame:CGRectMake(45, 37,55,15)];
    [lblAlert2 setBackgroundColor:[UIColor clearColor]];
    [lblAlert2 setFont:[UIFont boldSystemFontOfSize:12]];
    [lblAlert2 setText:@"Alert 2:"];
    [AddtionalView addSubview:lblAlert2];
    
    secondAlertsLabel = [[UILabel alloc]init];
    [secondAlertsLabel setFrame:CGRectMake(45,52,170,15)];
    [secondAlertsLabel setBackgroundColor:[UIColor clearColor]];
    [secondAlertsLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [secondAlertsLabel setTextColor:[UIColor blueColor]];
    [secondAlertsLabel setText:@"None"];
    [AddtionalView addSubview:secondAlertsLabel];
    
    
    UIButton *invisibleButtonAlert_2 = [UIButton buttonWithType:UIButtonTypeCustom];
    invisibleButtonAlert_2.frame = CGRectMake(secondAlertsLabel.frame.origin.x, secondAlertsLabel.frame.origin.y - 10,
                                              secondAlertsLabel.frame.size.width, secondAlertsLabel.frame.size.height + 10);
    invisibleButtonAlert_2.backgroundColor = [UIColor clearColor];
    [invisibleButtonAlert_2 addTarget:self action:@selector(secondAlertButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [AddtionalView addSubview:invisibleButtonAlert_2];
    
    
    lblPercent = [[UILabel alloc]init];
    
    //pooja-iPad
    if(IS_IPAD)
        lblPercent.frame = CGRectMake(340, 1, 80,20);
    else
        lblPercent.frame = CGRectMake(227, 1, 80,20);
    
    [lblPercent setBackgroundColor:[UIColor clearColor]];
    //[lblPercent setFont:[UIFont fontWithName:@"Arial" size:10]];
    [lblPercent setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:10]];
    //[lblPercent setText:[NSString stringWithFormat:@"%d%@ Complete",editItem.toDoProgress,@"%"]];
    lblPercent.text = @"   0";
    [AddtionalView addSubview:lblPercent];
    
    
    UIColor *BtnPercentColor = [UIColor colorWithRed:193.0f/255.0f green:136.0f/255.0f blue:12.0f/255.0f alpha:1.0f]; //Steve
    
    //0% complete
    UIButton *BtnPercent0 =[UIButton buttonWithType:UIButtonTypeCustom];
    //pooja-iPad
    if(IS_IPAD)
        [BtnPercent0 setFrame:CGRectMake(330, 22, 30, 20)];
    else
        [BtnPercent0 setFrame:CGRectMake(220, 22, 30, 20)];
    
    [BtnPercent0 setTitle:@"  0%" forState:UIControlStateNormal];
    [BtnPercent0 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [BtnPercent0 addTarget:self
                    action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
    [BtnPercent0 setTag:0];
    BtnPercent0.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    BtnPercent0.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
    BtnPercent0.layer.cornerRadius = 8.0f; //10
    BtnPercent0.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    BtnPercent0.layer.shadowOpacity = 0.5f;
    BtnPercent0.layer.shadowRadius = 1.0f;
    BtnPercent0.layer.masksToBounds = NO;
    BtnPercent0.clipsToBounds = NO;
    [AddtionalView addSubview:BtnPercent0];
    
    UIButton *BtnPercent25 =[UIButton buttonWithType:UIButtonTypeCustom];
    
    //pooja-iPad
    if(IS_IPAD)
        [BtnPercent25 setFrame:CGRectMake(380, 22, 30, 20)];
    else
        [BtnPercent25 setFrame:CGRectMake(270, 22, 30, 20)];
    [BtnPercent25 setTitle:@" 25%" forState:UIControlStateNormal];
    [BtnPercent25 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [BtnPercent25 addTarget:self
                     action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
    [BtnPercent25 setTag:25];
    BtnPercent25.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    BtnPercent25.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
    BtnPercent25.layer.cornerRadius = 8.0f; //10
    BtnPercent25.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    BtnPercent25.layer.shadowOpacity = 0.5f;
    BtnPercent25.layer.shadowRadius = 1.0f;
    BtnPercent25.layer.masksToBounds = NO;
    BtnPercent25.clipsToBounds = NO;
    [AddtionalView addSubview:BtnPercent25];
    
    UIButton *BtnPercent50 =[UIButton buttonWithType:UIButtonTypeCustom];
    
    //pooja-iPad
    if(IS_IPAD)
        [BtnPercent50 setFrame:CGRectMake(330, 50, 30, 20)];
    else
        [BtnPercent50 setFrame:CGRectMake(220, 50, 30, 20)];
    
    [BtnPercent50 setTitle:@" 50%" forState:UIControlStateNormal];
    [BtnPercent50 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [BtnPercent50 addTarget:self
                     action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
    [BtnPercent50 setTag:50];
    BtnPercent50.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    BtnPercent50.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
    BtnPercent50.layer.cornerRadius = 8.0f; //10
    BtnPercent50.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    BtnPercent50.layer.shadowOpacity = 0.5f;
    BtnPercent50.layer.shadowRadius = 1.0f;
    BtnPercent50.layer.masksToBounds = NO;
    BtnPercent50.clipsToBounds = NO;
    [AddtionalView addSubview:BtnPercent50];
    
    UIButton *BtnPercent75 =[UIButton buttonWithType:UIButtonTypeCustom];
    //pooja-iPad
    if(IS_IPAD)
        [BtnPercent75 setFrame:CGRectMake(380, 50, 30, 20)];
    else
        [BtnPercent75 setFrame:CGRectMake(270, 50, 30, 20)];
    [BtnPercent75 setTitle:@" 75%" forState:UIControlStateNormal];
    [BtnPercent75 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [BtnPercent75 addTarget:self
                     action:@selector(SetSliderValue:) forControlEvents:UIControlEventTouchUpInside];
    [BtnPercent75 setTag:75];
    BtnPercent75.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    BtnPercent75.layer.backgroundColor = BtnPercentColor.CGColor; //Steve
    BtnPercent75.layer.cornerRadius = 8.0f; //10
    BtnPercent75.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    BtnPercent75.layer.shadowOpacity = 0.5f;
    BtnPercent75.layer.shadowRadius = 1.0f;
    BtnPercent75.layer.masksToBounds = NO;
    BtnPercent75.clipsToBounds = NO;
    [AddtionalView addSubview:BtnPercent75];


    
    //NSLog(@"isKeyBoardDone = %d", isKeyBoardDone);
    
    
}
-(void) SetSliderValue:(UIButton *) sender
{  float val = sender.tag;
    slider.value = val;
    [lblPercent setText:[NSString stringWithFormat:@"%.f%@ Complete",val,@"%"]];
}

-(IBAction)sliderValueChanged:(UISlider *)sender
{
    //NSLog(@"slider value = %f", sender.value);
    [lblPercent setText:[NSString stringWithFormat:@"%.f%@ Complete", sender.value,@"%"]];
}


- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsDiscardRowAtIndexPath:(NSIndexPath *)indexPath 
{
    [todoDateObjArray removeObjectAtIndex:[indexPath row]];
}


- (BOOL)textFieldShouldReturn:(UITextField *) inputTextFields
{
    //NSLog(@" textFieldShouldReturn method");
    [inputTextFields resignFirstResponder];
    isKeyBoardDone = YES;
    return YES;
}

- (void) keyBoardDone:(id)sender
{
    //Steve changed so icons will appear white again
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];


    // NSLog(@"keyBaordDone Method");
    if (searchBarViewController)
        [self RemoveSearchBox];
    
    UITextField *_text = (UITextField *)sender;
    // NSLog(@"the text %@",_text.text);
    
    BOOL isInserted = NO;
    TODODataObject *addToDoObj = [[TODODataObject alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if(!isEditingToDo)
    {
        
        [todoDateObjArray removeObjectAtIndex:indexWhereCellIsBeingInserted];
        // NSLog(@"%@",todoDateObjArray);
        
        OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        if(([_text.text length]>0 || [todoTitleType isEqualToString:@"H"])  && indexWhereCellIsBeingInserted == 0 )
        {
            
            // Sets start & End Date on current time and date
            UILabel *startsLabel = [[UILabel alloc] init];
            UILabel *endsLabel = [[UILabel alloc] init];
            NSDate *sourceDate = [NSDate date];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            //[startsLabel setText:[dateFormatter stringFromDate:sourceDate]];
            //[endsLabel setText:[dateFormatter stringFromDate:[sourceDate dateByAddingHours:1]]];
            [startsLabel setText:@"None"];
            [endsLabel setText:@"None"];
            
            [addToDoObj setStartDateTime:startsLabel.text];
            [addToDoObj setDueDateTime:endsLabel.text];
            
            
            NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
            NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
            NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
            NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
            NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
            NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
            NSDate *createDate  = destinationDate;
            
            //NSLog(@"%@",createDate);
            
            
            [addToDoObj setCreateDateTime:[dateFormatter stringFromDate:createDate]];
            
            NSDateFormatter *newdateFormatter = [[NSDateFormatter alloc] init];
            
            NSDate *ModDate  = [NSDate date];
            [newdateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
            [addToDoObj setModifyDateTime:[newdateFormatter stringFromDate:ModDate]];            
            
            NSString * StrTodoProgress =[lblPercent.text stringByReplacingOccurrencesOfString:@"%" withString:@""  ];
            [addToDoObj setToDoProgress:[StrTodoProgress integerValue]];
            [addToDoObj setPriority:@""];
            [addToDoObj setIsStarred:NO];
            
            if([todoTitleType length]>0)[addToDoObj setTodoTitleType:todoTitleType];
            else [addToDoObj setTodoTitleType:@"T"]; 
            
            NSString *dataStr = @"";
            NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
            [addToDoObj setTodoTitleBinary:data];
            [addToDoObj setTodoTitleBinaryLarge:data];
            
            if ([todoTitleType isEqualToString:@"H"]) {
                [addToDoObj setTodoTitleType:@"H"];
                [addToDoObj setTodoTitleBinary:todoTitleBinary];
                [addToDoObj setTodoTitleBinaryLarge:todoTitleBinaryLarge];
            }
            
            //Steve commented
            /*
            [addToDoObj setAlert:alertsLabel.text];
            [addToDoObj setAlertSecond:secondAlertsLabel.text];
            [addToDoObj setIsAlertOn:isAlertOn];
             */
            
            if (_alertStringSaveFormat_1 == nil) {
                
                _alertStringSaveFormat_1 = @"None";
            }
            
            if (_alertStringSaveFormat_2 == nil) {
                
                _alertStringSaveFormat_2 = @"None";
            }
            
            //Steve changed
            [addToDoObj setAlert:_alertStringSaveFormat_1];
            [addToDoObj setAlertSecond:_alertStringSaveFormat_2];
            [addToDoObj setIsAlertOn:isAlertOn];
            

            [addToDoObj setTodoTitle:_text.text];

            [addToDoObj setProjID:projectIDFromProjMod];
            [addToDoObj setProjPrntID:projectIDFromProjMod];
            
            
            isInserted =  [AllInsertDataMethods insertToDoDataValues:addToDoObj]; //inserts into database
            [addToDoObj setTodoID:sqlite3_last_insert_rowid(appDelegate.database)];
            
            //***************************************** 
            //END  - Inserts new Data (user typed into Database
            //***************************************** 
           // [self getValuesFromDatabase];
            
            
            
        }
        else if(([_text.text length]>0 || [todoTitleType isEqualToString:@"H"]) && indexWhereCellIsBeingInserted != 0 )
        {
            
            // Sets start & End Date on current time and date
            UILabel *startsLabel = [[UILabel alloc] init];
            UILabel *endsLabel = [[UILabel alloc] init];
            NSDate *sourceDate = [NSDate date];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [startsLabel setText:[dateFormatter stringFromDate:sourceDate]];
            [endsLabel setText:[dateFormatter stringFromDate:[sourceDate dateByAddingHours:1]]];
            
            
            [addToDoObj setStartDateTime:startsLabel.text];
            [addToDoObj setDueDateTime:endsLabel.text];
            
            
            NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
            NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
            NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
            NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
            NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
            NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
            NSDate *createDate  = destinationDate;
            
           // NSLog(@"%@",createDate);
            
            
            [addToDoObj setCreateDateTime:[dateFormatter stringFromDate:createDate]];
            
            NSDateFormatter *newdateFormatter = [[NSDateFormatter alloc] init];
            
            NSDate *ModDate  = [NSDate date];
            [newdateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
            [addToDoObj setModifyDateTime:[newdateFormatter stringFromDate:ModDate]];
            [addToDoObj setModifyDate:[self convertStringToDate:addToDoObj.modifyDateTime]];
            
            [addToDoObj setProjID:projectIDFromProjMod];
            [addToDoObj setProjPrntID:projectIDFromProjMod]; 
            NSString * StrTodoProgress =[lblPercent.text stringByReplacingOccurrencesOfString:@"%" withString:@""  ];
            [addToDoObj setToDoProgress:[StrTodoProgress integerValue]];
            [addToDoObj setPriority:@""];
            [addToDoObj setIsStarred:NO];
            
            if([todoTitleType length]>0)[addToDoObj setTodoTitleType:todoTitleType];
            else [addToDoObj setTodoTitleType:@"T"];
            
            NSString *dataStr = @"";
            NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
            [addToDoObj setTodoTitleBinary:data];
            [addToDoObj setTodoTitleBinaryLarge:data];
            
            if ([todoTitleType isEqualToString:@"H"]) {
                [addToDoObj setTodoTitleType:@"H"];
                [addToDoObj setTodoTitleBinary:todoTitleBinary];
                [addToDoObj setTodoTitleBinaryLarge:todoTitleBinaryLarge];
            }
            
            
            [addToDoObj setTodoTitle:_text.text];
            
            //NSLog(@" addToDoObj.todoTitle =  %@", addToDoObj.todoTitle);
            //NSLog(@" addToDoObj.modifyDateTime =  %@", addToDoObj.modifyDateTime);
            //NSLog(@" addToDoObj.modifyDate =  %@", addToDoObj.modifyDate);
        
            
            
            
            //Steve commented
            /*
            [addToDoObj setAlert:alertsLabel.text];
            [addToDoObj setAlertSecond:secondAlertsLabel.text];
            [addToDoObj setIsAlertOn:isAlertOn];
             */
            
            if (_alertStringSaveFormat_1 == nil) {
                
                _alertStringSaveFormat_1 = @"None";
            }
            
            if (_alertStringSaveFormat_2 == nil) {
                
                _alertStringSaveFormat_2 = @"None";
            }
            
            //Steve changed
            [addToDoObj setAlert:_alertStringSaveFormat_1];
            [addToDoObj setAlertSecond:_alertStringSaveFormat_2];
            [addToDoObj setIsAlertOn:isAlertOn];
            

           // [AllInsertDataMethods insertToDoDataValues:addToDoObj]; //insert in database
            
            //Steve added
            isInserted =  [AllInsertDataMethods insertToDoDataValues:addToDoObj]; //inserts into database
            [addToDoObj setTodoID:sqlite3_last_insert_rowid(appDelegate.database)];
            
            //***************************************** 
            //END  - Inserts new Data (user typed into Database
            //***************************************** 
            
            [self getValuesFromDatabase];//Steve commented
            
            
            id itemAt0index = [todoDateObjArray objectAtIndex:0];
            
            [todoDateObjArray removeObjectAtIndex:0];
            
            
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            
            BOOL _added = NO;
            
            for (int k=0;k<[todoDateObjArray count]+1; k++) 
                
            {
                if(k!=indexWhereCellIsBeingInserted)
                {
                    if(!_added)
                        [tempArray addObject:[todoDateObjArray objectAtIndex:k]];
                    else
                    {
                        [tempArray addObject:[todoDateObjArray objectAtIndex:k-1]];
                        
                    }
                }
                else
                {
                    [tempArray addObject:itemAt0index];
                    _added = YES;
                }
            }
            
            [todoDateObjArray removeAllObjects];
            todoDateObjArray = tempArray;
            
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
            
            
            NSDate *_date = [dateFormatter1 dateFromString:[[todoDateObjArray objectAtIndex:0] modifyDateTime]];
            
            
            //Steve added - fixes issue where Projects changes the Modify Date to Nil.
            if (_date == nil) {
                
                //[dateFormatter setDateFormat:@"yyy-MM-dd hh:mm:ss a"];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                _date = [dateFormatter dateFromString:[[todoDateObjArray objectAtIndex:0] modifyDateTime]];
            }
            
            
            for (int _index =[todoDateObjArray count]-1;_index >=0;_index--) 
            {
                TODODataObject *item = [todoDateObjArray objectAtIndex:_index];
                
                _date = [_date dateByAddingTimeInterval:1];
                
                [item setModifyDate:_date];
                [item setModifyDateTime:[dateFormatter1 stringFromDate:_date]];
                BOOL isInserted = [AllInsertDataMethods updateToDoTitleOnly:item];
                //Alok's Changes for updates Todo Titles only
                //updateToDoDataValues:item];
                
            }
            dateFormatter1 = nil;
            
            //[self getValuesFromDatabase];//Steve commented
            //[toDoTableView reloadData];
            
            //NSLog(@" addToDoObj.todoTitle =  %@", addToDoObj.todoTitle);
            //NSLog(@" addToDoObj.modifyDateTime =  %@", addToDoObj.modifyDateTime);
            //NSLog(@" addToDoObj.modifyDate =  %@", addToDoObj.modifyDate);

            
        }
        
        else
        {
            //Do Nothing;
            
            // NSLog(@"%@",todoDateObjArray);
            // [todoDateObjArray removeObjectAtIndex:indexWhereCellIsBeingInserted];
            [toDoTableView reloadData];
        }
        
    }
    else  //isEditingToDo
    {
        isEditingToDo = NO;
        //// Update the to do
        
        TODODataObject *editItem = [todoDateObjArray objectAtIndex:indexWhereCellIsBeingInserted];
        
        if( ([_text.text length]>0 && ([editItem.todoTitleType isEqualToString:@"T"]|| [todoTitleType isEqualToString:@"T"] ))||([_text.text length]>0 && ([editItem.todoTitleType isEqualToString:@"V"] || [todoTitleType isEqualToString:@"T"] )) || [todoTitleType isEqualToString:@"H"])
        {
            editItem.todoTitle = _text.text;
            
            if([_text.text length]>0 && [todoTitleType length]>0)
                editItem.todoTitleType = todoTitleType;
            
            if(todoTitleBinary && [todoTitleType isEqualToString:@"H"])   
            {
                [editItem setTodoTitle:@""];  
                [editItem setTodoTitleType:@"H"];    
                [editItem setTodoTitleBinary:todoTitleBinary];
                [editItem setTodoTitleBinaryLarge:todoTitleBinaryLarge];
            }else
            {
                [editItem setTodoTitleBinary:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
                [editItem setTodoTitleBinaryLarge:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
            }    
            
            NSString * StrTodoProgress =[lblPercent.text stringByReplacingOccurrencesOfString:@"%" withString:@""  ];
            NSDate *sourceDate = [NSDate date];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

            
            NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
            NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
            
            NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
            NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
            NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
            NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
           // NSDate *createDate  = destinationDate;
            
            //NSLog(@"%@",createDate);
            
            
            // [addToDoObj setCreateDateTime:[dateFormatter stringFromDate:createDate]];
            
            
            [editItem setToDoProgress:[StrTodoProgress integerValue]];
            //[editItem setAlert:alertsLabel.text];
            //[editItem setAlertSecond:secondAlertsLabel.text];
            [editItem setAlert:_alertString_1]; //Steve
            [editItem setAlertSecond:_alertString_2]; //Steve
            
            if (_alertStringSaveFormat_1 == nil) {
                
                _alertStringSaveFormat_1 = @"None";
            }
            
            if (_alertStringSaveFormat_2 == nil) {
                
                _alertStringSaveFormat_2 = @"None";
            }
            
            //Steve changed
            [editItem setAlert:_alertStringSaveFormat_1];
            [editItem setAlertSecond:_alertStringSaveFormat_2];

            
            
            if (![alertsLabel.text isEqualToString:@"None"] || ![secondAlertsLabel.text isEqualToString:@"None"]) {
                isAlertOn =YES;
            }
            [editItem setIsAlertOn:isAlertOn]; 
            
            isInserted = [AllInsertDataMethods  updateToDoTitleOnly:editItem];

            //Steve commented
            //[self getValuesFromDatabase];
            //[toDoTableView reloadData];
            
        }
        addToDoObj = editItem;
    }
    
    /// Added to clear Unused data
    [inputTextField setText:@""];
    todoTitle = @"";
    todoTitleType = @"";
    todoTitleBinary =[@"" dataUsingEncoding:NSUTF8StringEncoding];
    todoTitleBinaryLarge = [@"" dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    /////////////ALOK ADDED FOR SETTING TODO ALERTS ////////////////
    
    if (isInserted == YES) {
        
        notification = [[UILocalNotification alloc] init];
        // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        ////////////////////  Setting first Alert //////////////////////

        if (![alertsLabel.text isEqualToString:@"None"]) 
            
        {
            
            notification.timeZone		= [NSTimeZone systemTimeZone];
            //notification.timeZone		= [NSTimeZone defaultTimeZone];//Steve
            
            if([addToDoObj.todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil) 
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [addToDoObj todoTitle];
                
            }
            
            
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            
            
            //Steve commented
            /*
            [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            NSDate *alertDate =[dateFormatter dateFromString:[addToDoObj alert]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            NSLog(@"addToDoObj.alert =  %@", addToDoObj.alert);
            
            //Steve
            if (alertDate == nil) {
                
                alertDate =[dateFormatter dateFromString:[addToDoObj alert]];
            }
            
            
            NSString *strAlert =@"";
            strAlert = [dateFormatter stringFromDate:alertDate];
            
            if ([dateFormatter dateFromString:strAlert])
            {
                notification.fireDate = [dateFormatter dateFromString:strAlert];
            }
             */
            
            if (_alertDate_1 != nil) {
                
                notification.fireDate = _alertDate_1;
            }
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", addToDoObj.todoID] forKey:@"toDoID"];
           // NSLog(@"userDict->%@",userDict);
            notification.userInfo = userDict; 
            //NSLog(@"ALERT 1->%@->%@ - %@",[addToDoObj alert],notification.alertBody,notification.fireDate);
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
        }
        //////////////////////////////////////////////////////////////  
        
        if (![secondAlertsLabel.text isEqualToString:@"None"]) 
            
        {        
            //notification.timeZone		= [NSTimeZone systemTimeZone];
            notification.timeZone		= [NSTimeZone defaultTimeZone];//Steve
            
            if([[addToDoObj todoTitleType] isEqualToString:@"H"] && [addToDoObj todoTitleBinary] != nil) 
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [addToDoObj todoTitle];
                
            }
            
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            
            //Steve commented
            /*
            [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            NSDate *alertDate =[dateFormatter dateFromString:[addToDoObj alertSecond]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            NSString* strAlert =@"";
            strAlert = [dateFormatter stringFromDate:alertDate];
            if ([dateFormatter dateFromString:strAlert])
            {
                notification.fireDate = [dateFormatter dateFromString:strAlert];
            }
             */
            
            if (_alertDate_2 != nil)  {
                
                notification.fireDate = _alertDate_2;
            }

            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",addToDoObj.todoID] forKey:@"toDoID"];
           // NSLog(@"%@",userDict);
            notification.userInfo = userDict;
           // NSLog(@"ALERT 2->%@ - %@",notification.alertBody,notification.fireDate);
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
        }
    } 
    
    ////////////ALOK ADDED FOR SETTING TODO ALERTS/////////////////
    
    
    [inputTextField setText:@""];
    [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
    [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
    [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
    [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
    [[self.view viewWithTag:DONE_BUTTON_TAG] removeFromSuperview];
    
    isKeyBoardDone = YES;
    
    //Steve added
    [self getValuesFromDatabase];
    [toDoTableView reloadData];
    
}
-(void) keyboardDidShow: (NSNotification *)notif 
{
    //NSLog(@"%s",__FUNCTION__);
    
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    // If keyboard is visible, return
    if (isKeyBoardDone) 
    {
        // NSLog(@"Keyboard is already visible. Ignoring notification.");
        return;
    }
    
    // Keyboard is now visible
    
    isKeyBoardDone = NO;
}

-(void) keyboardDidHide: (NSNotification *)notif 
{
    // Is the keyboard already shown
    if (!isKeyBoardDone) 
    {
        //NSLog(@"Keyboard is already hidden. Ignoring notification.");
        return;
    }
    
    
    // Keyboard is no longer visible
    isKeyBoardDone = YES;	
}

-(void) keyboardWillShow: (NSNotification *)notif 
{
    
}

#pragma mark JTTableViewGestureEditingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer didEnterEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = [toDoTableView cellForRowAtIndexPath:indexPath];
    
    UIColor *backgroundColor = nil;
    switch (state) {
        case JTTableViewCellEditingStateMiddle:
            backgroundColor = [UIColor redColor];
            break;
        case JTTableViewCellEditingStateRight:
            backgroundColor = [UIColor greenColor];
            break;
        default:
            backgroundColor = [UIColor lightGrayColor];
            break;
    }
    
    cell.contentView.backgroundColor = backgroundColor;
    
    //Steve added
    //When swiping cell Left or Right
    //changes cell color back to backgroundColor (white), otherwise cell will stay red until another ToDo is added
    if (cell.contentView.backgroundColor == [UIColor redColor])
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelay:0.5];
        [UIView setAnimationDuration:1.0];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        [UIView commitAnimations];
        // NSLog(@"RedColor");
    }
    
    
    if ([cell isKindOfClass:[TransformableTableViewCell class]]) {
        ((TransformableTableViewCell *)cell).tintColor = backgroundColor;
    }
}

// This is needed to be implemented to let our delegate choose whether the panning gesture should work
- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;  // Anil's Addition // Changed YES to NO
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer commitEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableView *tableView = gestureRecognizer.tableView;
    
    [tableView beginUpdates];
    
    if (state == JTTableViewCellEditingStateLeft) {
        // An example to discard the cell at JTTableViewCellEditingStateLeft
        //Steve changed was: [self.rows removeObjectAtIndex:indexPath.row];
        [todoDateObjArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } else if (state == JTTableViewCellEditingStateRight) {
        // An example to retain the cell at commiting at JTTableViewCellEditingStateRight
        //Steve changed was:  [self.rows replaceObjectAtIndex:indexPath.row withObject:DONE_CELL];
        [todoDateObjArray replaceObjectAtIndex:indexPath.row withObject:DONE_CELL];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } else {
        // JTTableViewCellEditingStateMiddle shouldn't really happen in
        // - [JTTableViewGestureDelegate gestureRecognizer:commitEditingState:forRowAtIndexPath:]
    }
    
    [tableView endUpdates];
    
    // Row color needs update after datasource changes, reload it.
    [tableView performSelector:@selector(reloadVisibleRowsExceptIndexPath:) withObject:indexPath afterDelay:JTTableViewRowAnimationDuration];
}

#pragma mark JTTableViewGestureMoveRowDelegate

- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCreatePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Steve changed was: self.grabbedObject = [self.rows objectAtIndex:indexPath.row];
    self.grabbedObject = [todoDateObjArray objectAtIndex:indexPath.row];
    //Steve changed was: [self.rows replaceObjectAtIndex:indexPath.row withObject:DUMMY_CELL];
    [todoDateObjArray replaceObjectAtIndex:indexPath.row withObject:DUMMY_CELL];
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsMoveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    //Steve changed it was:
    //id object = [self.rows objectAtIndex:sourceIndexPath.row];
    //[self.rows removeObjectAtIndex:sourceIndexPath.row];
    //[self.rows insertObject:object atIndex:destinationIndexPath.row];
    
    //STeve changed to:
    id object = [todoDateObjArray objectAtIndex:sourceIndexPath.row];
    [todoDateObjArray removeObjectAtIndex:sourceIndexPath.row];
    [todoDateObjArray insertObject:object atIndex:destinationIndexPath.row];
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsReplacePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {

    [todoDateObjArray replaceObjectAtIndex:indexPath.row withObject:self.grabbedObject];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    
    NSDate *_date = [dateFormatter dateFromString:[[todoDateObjArray objectAtIndex:0] modifyDateTime]];
    
    
    //Steve added - fixes issue where Projects changes the Modify Date to Nil.
    if (_date == nil) {
        
        //[dateFormatter setDateFormat:@"yyy-MM-dd hh:mm:ss a"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        _date = [dateFormatter dateFromString:[[todoDateObjArray objectAtIndex:0] modifyDateTime]];
    }
    
    
   // NSLog(@" [[todoDateObjArray objectAtIndex:0] modifyDateTime]] =  %@", [[todoDateObjArray objectAtIndex:0] modifyDateTime]);
    
    
    for (int _index =[todoDateObjArray count]-1;_index >=0;_index--) 
    {
        TODODataObject *item = [todoDateObjArray objectAtIndex:_index];
        // NSLog(@"%@",item.todoTitle);
        _date = [_date dateByAddingTimeInterval:1];
        
        [item setModifyDateTime:[dateFormatter stringFromDate:_date]];
        
        BOOL isInserted = [AllInsertDataMethods updateToDoTitleOnly:item];
        
        
        //Alok's Changes for updates Todo Titles only
        //updateToDoDataValues:item];
        
         NSLog(@"%d",isInserted);
    }
    
    
    [self getValuesFromDatabase];
    [toDoTableView reloadData];
    
    
    self.grabbedObject = nil;
}

-(NSDate *)convertStringToDate:(NSString *)str_Date
{
    //Steve modified to check all possible string to date types including different locals
    
    NSDateFormatter *dateFormatter_1 = [[NSDateFormatter alloc] init];
    
    [dateFormatter_1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *resultedDate = [dateFormatter_1 dateFromString:str_Date];
    
    
    if (resultedDate == nil) {
        
        [dateFormatter_1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
        resultedDate = [dateFormatter_1 dateFromString:str_Date];
    }
    
    if (resultedDate == nil){
        
        [dateFormatter_1 setDateFormat:@"MMM dd, yyyy, hh:mm a"];
        resultedDate = [dateFormatter_1 dateFromString:str_Date];
    }
    
    if (resultedDate == nil){ //for Europe Time formats
        
        [dateFormatter_1 setDateFormat:@"dd MMM yyyy hh:mm"];
        resultedDate = [dateFormatter_1 dateFromString:str_Date];
    }
 
   
    dateFormatter_1 = nil;
    
    return resultedDate;
    
}





#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    
    [self.popoverController dismissPopoverAnimated:NO]; //Steve
	self.popoverController = nil; //Steve
    
    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    backButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)callSelectorForSwipeUpAction
{
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    isCreateViewOn = NO;
    if(IS_LITE_VERSION) 
    {
        
        return;
    }
    if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized){
        
        // NSLog(@"%s",__FUNCTION__);
        ///Aman's Changed
        VoiceToTextViewController *voiceToTextViewController = [[VoiceToTextViewController alloc] initWithNibName:@"VoiceToTextViewController"  bundle:[NSBundle mainBundle] withParent:[[appDelegate.navigationController viewControllers] objectAtIndex:([[appDelegate.navigationController viewControllers] count] - 1)]];
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 0.4;
        [appDelegate.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromTop];
        [appDelegate.navigationController pushViewController:voiceToTextViewController animated:NO];   
        
    }
}

-(void)callSelectorForSwipeDownAction
{
    
    //Alok Added Get Count Number of todo's 
    NSString *selectStat=@"select count(TODOID) from ToDoTable";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];   
    if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"To Do Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d todos.",MAX_LIMIT_TODO] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
        return;
    }
    
    // NSLog(@"ListToViewController Class --> callSelectorForSwipeDownAction ");
    
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    // NSLog(@"%s",__FUNCTION__);
    [[self.view viewWithTag:TEXT_FIELD_TAG] resignFirstResponder];
    [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
    [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
    [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
    [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
    [[self.view viewWithTag:DONE_BUTTON_TAG] removeFromSuperview];
    
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    [self goToHandwritingView];
}


-(void)goToHandwritingView
{
    //NSLog(@"goToHandwritingView");
    
    // NSLog(@"ListToViewController Class --> goToHandwritingView ");
    
    //NSLog(@"%s",__FUNCTION__);
    isCreateViewOn =NO;
    
    [[self.view viewWithTag:TEXT_FIELD_TAG] resignFirstResponder];
    
    [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
    [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
    [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
    [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
    [[self.view viewWithTag:DONE_BUTTON_TAG] removeFromSuperview];
    
    //Steve added
    //removes Status bar for handwritting
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    
    //Steve commented out "if KeyBoardDone" and "else"
    //if(isKeyBoardDone)
    //{
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    // NSLog(@"%s",__FUNCTION__);
    HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO]; //isPendIcon == NO since there is a differant animation when swiping down
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.4;
   // [appDelegate.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromTop];
   // [appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
     [self.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromTop];
     [self.navigationController pushViewController:handWritingViewController animated:NO];
    // }
    //  else
    //  {
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    //}
}


// Added by anil //
// This method gets called when text field for entering to do appear and user click on black area between text field and keyboard

-(void)saveToDoToDB:(id)sender
{
    [self keyBoardDone:[self.view viewWithTag:TEXT_FIELD_TAG]];
    
    [_pickerBackgroundView_1 removeFromSuperview];
    [_pickerBackgroundView_2 removeFromSuperview];
    
    [_datePicker removeFromSuperview];
    [_datePickerSecondAlert removeFromSuperview];
}


- (IBAction)notesButtonClicked:(id)sender {
    NotesViewCantroller *notesViewCantroller =[[NotesViewCantroller alloc]initWithNibName:@"NotesViewCantroller" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:notesViewCantroller animated:YES];
    
}
#pragma  INPUT TYPE & PEN ICON
///Alok Add for InputType View

-(void) inputTypeSelectorButton_Clicked:(id)sender 
{
    //NSLog(@"inputTypeSelectorButton_Clicked");
    
	[inputTextField resignFirstResponder];
    isCreateViewOn =YES;
    
    
	if (inputTypeSelectorViewController) {
		inputTypeSelectorViewController = nil;
	}
    
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	if(appdelegate.IsVTTDevice && appdelegate.isVTTDeviceAuthorized)
    {
        inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(180, 97, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(285, 86) andParentVC:self fromMainScreen:NO];
        
        [inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:inputTypeSelectorViewController.view];
        [self.view bringSubviewToFront:inputTypeSelectorViewController.view];
    }
    else
    {
        if (IS_IPAD) {
            
            [self handwritingView];
        }
        else{ //iPhone
            
            HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:YES];

            [self.navigationController pushViewController:handWritingViewController animated:NO];//YES
        }
        

    }   
    
}

//Steve
#pragma Handwriting methods for iPad

-(void)handwritingView
{
    
     _organizerAppDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    //Add Handwriting ViewController
    HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController_iPad"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:YES]; //NO
    handWritingViewController.handwritingForiPadDelegate = self; //Steve TEST
    
    NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
    [stringList setValue:@"TODO" forKey:@"Viewcontroller"];
    [stringList synchronize];
    
    [_organizerAppDelegate.window addSubview:handWritingViewController.view];
    [self addChildViewController:handWritingViewController];
    [handWritingViewController didMoveToParentViewController:self];
    
    
    _backgroundViewforHandwriting = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    _backgroundViewforHandwriting.backgroundColor = [UIColor redColor];
    _backgroundViewforHandwriting.alpha = 1.0;
    [_organizerAppDelegate.window addSubview:_backgroundViewforHandwriting];
    
    
    //Adjust size
    int blurredViewWidth = [[UIScreen mainScreen] bounds].size.width;
    int blurredViewHeight = [[UIScreen mainScreen] bounds].size.height - 320;
    _backgroundViewforHandwriting.frame = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight);
    CGRect rect = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight);
    
    
    //Adds the blurr image of the screehshot
    UIImage *imageblurred = [self captureScreenShotAndblurImage];
    UIImageView *imageBlurredImageView = [[UIImageView alloc]initWithImage:imageblurred];
    
    //self.blurredImageView = [[UIImageView alloc]initWithImage:imageblurred];
    //self.blurredImageView.frame = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight); //Change frame size so it doesn't cover HandwritingView
    //[_backgroundViewforHandwriting addSubview:self.blurredImageView];
    
    
    // Render the layer hierarchy to the current context
    //[[_backgroundViewforHandwriting layer] renderInContext:context];
    
    CALayer *layer;
    layer = imageBlurredImageView.layer;
    UIGraphicsBeginImageContext(layer.frame.size);
    CGContextClipToRect (UIGraphicsGetCurrentContext(),rect);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImageBlurredClipped = UIGraphicsGetImageFromCurrentImageContext();
    self.blurredImageView = [[UIImageView alloc]initWithImage:screenImageBlurredClipped];
    [_backgroundViewforHandwriting addSubview:self.blurredImageView];
    
    
    //hides view until animation
    handWritingViewController.view.hidden = YES;
    _backgroundViewforHandwriting.hidden = YES;
    self.blurredImageView.hidden = YES;
    
    
    //animates the handWritingViewController.view
    [self performSelector:@selector(fadeInHandwritingView:) withObject:handWritingViewController.view afterDelay:0.0];


    
    //Animates _backgroundViewforHandwriting (with blurred background of screenshot)
    [self performSelector:@selector(fadeInBlurredImage) withObject:nil afterDelay:0.0];
    
    
    //UIImageView *testImageView = self.blurredImageView;

    
}

-(UIImage*) captureScreenShotAndblurImage{
    
    
    // Create a graphics context with the target size
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    
    UIGraphicsBeginImageContext(imageSize);
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Iterate over every window from back to front
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context
            CGContextSaveGState(context);
            // Center the context around the window's anchor point
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            // Apply the window's transform about the anchor point
            CGContextConcatCTM(context, [window transform]);
            // Offset by the portion of the bounds left of and above the anchor point
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
            
            // Render the layer hierarchy to the current context
            //[[window layer] renderInContext:context];
 
            
            // iOS 7 New API
            [window drawViewHierarchyInRect:[[UIScreen mainScreen] bounds] afterScreenUpdates:NO];
            
            // Restore the context
            CGContextRestoreGState(context);
        }
    }
    
    
    // Get the snapshot
    _screenShotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Now apply the blur effect using Apple's UIImageEffect category
    UIImage *blurredScreenshotImage = [_screenShotImage applyDarkEffectWithLessBlur];//applyLightEffect  applyDarkEffectWithLessBlur
    
    // Or apply any other effects available in "UIImage+ImageEffects.h"
    // UIImage *blurredScreenshotImage = [screenShotImage applyDarkEffect];
    //UIImage *blurredScreenshotImage = [screenShotImage applyExtraLightEffect];
    
    UIGraphicsEndImageContext();
    
    
    return blurredScreenshotImage;
    
}
- (void) fadeInHandwritingView:(UIView*) handwritingView{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.50;
    [handwritingView.layer addAnimation:transition forKey:nil];
    handwritingView.hidden = NO;
    
}

- (void) fadeInBlurredImage{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [_backgroundViewforHandwriting.layer addAnimation:transition forKey:nil];
    _backgroundViewforHandwriting.hidden = NO;
    self.blurredImageView.hidden = NO;
    
}

-(void) showHandwritingForiPad{
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [_backgroundViewforHandwriting.layer addAnimation:transition forKey:nil];
    _backgroundViewforHandwriting.hidden = YES;
    
    [self viewWillAppear:NO];
    
}







//Alok Added to bottom tab bar items
#pragma mark -
#pragma mark Bottom Tab bar items
#pragma mark -

- (IBAction)inputOptionButton_Clicked:(id)sender {
    
    isPenIcon = YES; //Steve
    
    if(searchBarViewController)
    [self RemoveSearchBox];
    
    //Alok Added Get Count Number of todo's
    NSString *selectStat=@"select count(TODOID) from ToDoTable";
    CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    
    if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"To Do Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d todos.",MAX_LIMIT_TODO] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
        return;
    }
    
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized){
        
            isCreateViewOn =NO;
        
        if (inputTypeSelectorViewController) {
            
            if ([inputTypeSelectorViewController.view alpha] == 0) {
                
                if(![self.view viewWithTag:IPTYPE_VC_VIEW_TAG]){
                    
                    [self.view addSubview:inputTypeSelectorViewController.view];
                }
                [self showInputOptPopover:YES];
            }else {
                [self showInputOptPopover:NO];
            }
        }else {
            [self showInputOptPopover:YES];
        }
        [self showSearchOptPopover:NO];
    }
    else //Not Voice To Text
    {
        
        //Steve
        if (IS_IPAD) {
            
            isCreateViewOn = YES;
            _isHandwriting = YES;
            
            [self handwritingView];
            
            [self performSelector:@selector(createAddView) withObject:nil afterDelay:0.10];
            
        }
        else{ //iPhone
            
            BOOL isPenIcon = YES; //Steve added
            isCreateViewOn =NO;
            
            HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:                 [NSBundle mainBundle] withParent:self isPenIcon:YES];
            
            [self.navigationController pushViewController:handWritingViewController animated:NO];
        }
        

    }   
}
-(void)showInputOptPopover:(BOOL) show {
	if (show) {
        inputTypeSelectorViewController =nil; 
		if (inputTypeSelectorViewController) {
			if ([inputTypeSelectorViewController.view alpha] == 0) {
                
				[inputTypeSelectorViewController.view setAlpha:1];
			}
		}else {
            
			inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(10, 291, 150, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(50, 403) andParentVC:self fromMainScreen:NO];
			inputTypeSelectorViewController.view.tag = IPTYPE_VC_VIEW_TAG;
			[inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
			[self.view addSubview:inputTypeSelectorViewController.view];
		}
	} else {
		if (inputTypeSelectorViewController) {
			if ([inputTypeSelectorViewController.view alpha] == 1) {
				[inputTypeSelectorViewController.view setAlpha:0];
			}else {
				[inputTypeSelectorViewController.view removeFromSuperview];
				inputTypeSelectorViewController = nil;
			}
		}	
	}
	[self.view bringSubviewToFront:inputTypeSelectorViewController.view];
}
- (IBAction)optionButton_Clicked:(id)sender {
    
    indexOfmainArray =0;
    tempdate = nil;
    str_CategoryName =@"";
    str_ProgressPercentage = -1;
    todoDataObj = [todoDateObjArray objectAtIndex:0];
    
    if([todoDataObj isEqual :ADDING_CELL])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry. There aren't any to-do's to speak." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    
    if(![todoDataObj isEqual :ADDING_CELL])
        self.arrM_finalEvents = todoDateObjArray;
    [self eventToSpeak];
}
- (IBAction)searchBarButton_Clicked:(id)sender {
    
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    
    
    todoDataObj = [todoDateObjArray objectAtIndex:0];
    if([todoDataObj isEqual :ADDING_CELL])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry. There aren't any to-do's to search." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
	[self showInputOptPopover:NO];
	
	if (searchBarViewController) {
		if ([searchBarViewController.view alpha] == 0) {
			[self showSearchOptPopover:YES];
		}else {
			[self showSearchOptPopover:NO];
            [self RemoveSearchBox];
		}
	}else {
		[self showSearchOptPopover:YES];
	}
}

-(void)RemoveSearchBox{
    [self showSearchOptPopover:NO];
    [self getValuesFromDatabase];
    [toDoTableView reloadData];
}

-(void)showSearchOptPopover:(BOOL) show {
	if (show) {
		if (searchBarViewController) {
			searchBarViewController = nil;
			
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
			
		}else {
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
		}
	} else {
		if (searchBarViewController) {
			if ([searchBarViewController.view alpha] == 1) {
				[searchBarViewController.view setAlpha:0];
			}else {
				[searchBarViewController.view removeFromSuperview];
				searchBarViewController = nil;
			}
		}	
	}
	[self.view bringSubviewToFront:searchBarViewController.view];
}

-(void)eventToSpeak
{
    
    //NSLog(@"%@",arrM_ForOtherTabs );
    
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    
    internet_YES_NO = [self hasConnectivity];
    
    if(internet_YES_NO)
    {
        
        [self finalString];
        int j =0;    
        if([self.arrM_finalEvents count]!=0)
        {
            for (int i  = 0; i<[self.arrM_finalEvents count]; i++) {
                
                TODODataObject *objAppointment = [self.arrM_finalEvents objectAtIndex:i];
                NSDateFormatter *formate = [[NSDateFormatter alloc] init];
                [formate setDateStyle:NSDateFormatterFullStyle];
                [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                self.strtDate = [formate dateFromString:objAppointment.startDateTime];
                self.endDate = [formate dateFromString:objAppointment.dueDateTime];
                
                
                if([self.endDate isEqualToDateIgnoringTime:destinationDate])
                {
                    
                 //   if(j==0)
                   //     self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@ .",[self stringFortext]];
                    
                    
                    
                    if([objAppointment.todoTitleType isEqualToString:@"H"])
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,@"Handwritten Task"];
                    else
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,objAppointment.todoTitle];
                    if(selectedButtonIndex ==100 || selectedButtonIndex ==8)  // Changed 0 to 100
                    {
                        
                    }
                    else
                    {
                        
                        [formate setDateFormat:@"h:mm a"];
                        if(!IS_LIST_VERSION)
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Due Today,\n"];
                    }
                    
                    j++;
                }
                else if([self.endDate isTODO_Cal_Tomorrow:destinationDate])
                    
                {
                    
                    
                  //  if(j==0)
                  //      self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@ .",[self stringFortext]];
                    
                    if([objAppointment.todoTitleType isEqualToString:@"H"])
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,@"Handwritten Task"];
                    else
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,objAppointment.todoTitle];
                    if(selectedButtonIndex ==100 || selectedButtonIndex ==8)  // Changed 0 to 100
                    {
                        
                    }
                    else{
                        [formate setDateFormat:@"h:mm a"];
                        if(!IS_LIST_VERSION)
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Due Tomorrow,\n"];
                    }
                    // self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@ To ",[formate stringFromDate:self.strtDate]];
                    //self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@,",[formate stringFromDate:self.endDate]];
                    j++;
                }
                else if([self.endDate isTODO_Cal_Yesterday:destinationDate])
                {
                    
                    //if(j==0)
                      //  self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@ .",[self stringFortext]];
                    
                    if([objAppointment.todoTitleType isEqualToString:@"H"])
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,@"Handwritten Task"];
                    else
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,.",i+1,objAppointment.todoTitle];
                    if(selectedButtonIndex ==100 || selectedButtonIndex ==8)  // Changed 0 to 100
                    {
                        
                    }
                    else
                    {
                        [formate setDateFormat:@"h:mm a"];
                        if(!IS_LIST_VERSION)
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Due Yesterday,\n"];
                    }
                    // self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Due Yesterday,%@ To ",[formate stringFromDate:self.strtDate]];
                    //self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@,",[formate stringFromDate:self.endDate]];
                    j++;
                }
                else 
                {
                    
                    
                   // if(j==0)
                     //   self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@ .",[self stringFortext]];
                    
                    
                    if([objAppointment.todoTitleType isEqualToString:@"H"])
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,",i+1,@"Handwritten Task"];
                    else
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"Task,%d,%@,",i+1,objAppointment.todoTitle];
                    if(selectedButtonIndex ==100 || selectedButtonIndex ==8)  // Changed 0 to 100
                    {
                        
                    }                        
                    else
                    {
                        [formate setDateFormat:@"MMMM,dd"];
                        if(!IS_LIST_VERSION){
                            
                            //Steve - checks for endDate is Null
                            NSString *endDate_String = [formate stringFromDate:self.endDate];
                            
                            if (endDate_String == nil) {
                                endDate_String = @"None";
                            }
                            
                            
                            if ( ![endDate_String isEqualToString:@"None"] ){
                                self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" Due ,%@,\n",endDate_String];
                            }
                            else{
                                self.strTextToSpeak =(NSMutableString *)self.strTextToSpeak;
                            }
                        }
                    }
                    
                    j++;
                    

                }
                
            }
           // NSLog(@"->%@",self.strTextToSpeak);
            
            
            
            [[iSpeechViewControllerTTS sharedApplication]readText:[self callToSpeaktext:self.strTextToSpeak]];
            
            
        }
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:@"Text to Voice requires an internet connection. Please connect to the internet to use Text to Voice." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
        
        
        
    }
    
}
-(NSString *)callToSpeaktext:(NSString *)callingText
{
    
    NSArray *temp_String = [self.strTextToSpeak componentsSeparatedByString:@"\n"];
    
    
    NSMutableString *tempString = [NSMutableString stringWithString:@""];
    NSMutableString *appendingString = [NSMutableString stringWithString:@""];
    int lengthOfAppendString  = 0;
    int tempStringlength = 0;
    NSInteger tag =0;
    for(int i =0;i<[temp_String count];i++)
    {
        appendingString = [temp_String objectAtIndex:i];
        lengthOfAppendString = [[appendingString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]count];
        if((tempStringlength + lengthOfAppendString)<=50)
        {
            tempString = [NSMutableString stringWithString:                                    [tempString stringByAppendingFormat:@"%@",[temp_String objectAtIndex:i]]];
            tempStringlength = [[tempString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]count];
        }
        else
        {
            tag =1;
            break;
        }
    }
    if(tag ==0)
    {
        tempString = [NSString stringWithString:[tempString stringByReplacingOccurrencesOfString:@"Here is a partial list," withString:@""]];
        
    }
    
    return tempString;
    
}
- (IBAction)mailBarButton_Clicked:(id)sender {
    
	if ([MFMailComposeViewController canSendMail]) {
        
        if (IS_IOS_7) {
            
            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
        }
        else{
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        

        
		MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
		[mailCntrlr setMailComposeDelegate:self];
		[mailCntrlr setSubject:@"InFocus Pro"];
        
        //<th>Progress</th><th>Starred</th><th>Priority</th><th>To-Do Title</th></tr>
		NSString *body = @"<table border=\"1\" style=\"background-color:white;\"><tr style=\"font-weight:bold;\"><th>Title</th>";
        if(IS_LIST_VERSION)
            body = [body stringByAppendingString:@"<th>Progress</th>"];
            else
        body = [body stringByAppendingString:@"<th>StartDate</th><th>DueDate</th><th>Progress</th><th>Priority</th></tr>\n "];
		
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"MM/dd/yyyy"];
        
        
        
		for (int row = 0; row < [todoDateObjArray count]; row++) {
            
            
			TODODataObject *tempDataObj = [todoDateObjArray objectAtIndex:row];
            
            if(![tempDataObj isEqual:ADDING_CELL])
            {
                
                NSDate *strtDateE =[dateFormatter dateFromString:[tempDataObj startDateTime]];
                NSDate *endDateE =[dateFormatter dateFromString:[tempDataObj startDateTime]];
                
                NSString *startDate_String = [dateFormatter1 stringFromDate:strtDateE];
                NSString *endDate_String = [dateFormatter1 stringFromDate:endDateE];
                
                
                //Steve - added to adjust for dates with "None"
                if (startDate_String == nil) {
                    startDate_String = @"None";
                }
                
                if (endDate_String == nil) {
                    endDate_String = @"None";
                }
                
                
                
                NSString *starredStr =@""; 
                if (tempDataObj.isStarred == YES) {
                    starredStr = @"Yes";
                }else{
                    starredStr = @"No";
                }
                
                
                
                if([tempDataObj.todoTitleType isEqualToString:@"H"] && tempDataObj.todoTitleBinary!= nil) 
                {
                    //                UIImage *tempTitleImage = [[UIImage alloc] initWithData:tempDataObj.todoTitleBinary];
                    //                
                    //                NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    //                NSString *path = [[searchPath objectAtIndex:0]  stringByAppendingString:[NSString stringWithFormat:@"/%@",@"TitleImage.png"]];
                    //                NSData *imageData = UIImagePNGRepresentation(tempTitleImage);
                    //                
                    //                [imageData writeToFile:path atomically:YES];
                    
                 if(IS_LIST_VERSION) 
                     body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td>%@</td><td align=\"center\">%d&#37;</td></tr>",@"Handwritten Task",[tempDataObj toDoProgress]]];  
                    else
                    body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td>%@</td><td>%@</td><td>%@</td><td align=\"center\">%d&#37;</td><td>%@</td></tr>",@"Handwritten Task",startDate_String,endDate_String,[tempDataObj toDoProgress],[tempDataObj priority]]];                   
                    
                    
                    
                }
                
                else
                {
                    //Steve added - % symbol causes an email bug in the title string, so it needs to be changed to %%
                    NSString *titleString = [tempDataObj todoTitle];
                    titleString = [titleString stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
                    
                    if(IS_LIST_VERSION) 
                        body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td>%@</td><td align=\"center\" >%d&#37;</td></tr>",titleString,[tempDataObj toDoProgress]]];
                    else
                    body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td>%@</td><td>%@</td><td>%@</td><td align=\"center\">%d&#37;</td><td>%@</td></tr>", titleString,startDate_String,endDate_String,[tempDataObj toDoProgress],[tempDataObj priority]]];
                }
                
                
            }
        }
		body = [body stringByAppendingString:@"</table>"];
        
        
        //Steve - Infocus Pro Link
        NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
        body = [body stringByAppendingString:bodyURL];
        
        
		[mailCntrlr setMessageBody:body isHTML:YES];
        [self presentViewController:mailCntrlr animated:YES completion:nil];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
    
	
}



-(BOOL)hasConnectivity {
    
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}

-(NSMutableString *)finalString
{
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDateFormatter *formate2 = [[NSDateFormatter alloc] init];
    
    [formate2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    //    NSDate *temp = [formate2 dateFromString:self.currentCalanderDate];
    [formate2 setDateFormat:@"MMMM,dd,yyyy"];
    //    
    self.str_todaysDate =[NSMutableString stringWithString:[formate2 stringFromDate:destinationDate]] ;
    
    [formate2 setDateFormat:@"h:mm a"];
    
    // NSLog(@"%@",[formate2 stringFromDate:destinationDate]);
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit fromDate:destinationDate];
    NSInteger hour = [components hour];
    
    
    if(hour >= 0 && hour < 12)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good morning,"];
        // NSLog(@"Good morning,");
    }
    else if(hour >= 12 && hour < 18)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good afternoon,"];
        
        //  NSLog(@"Good afternoon,");
    }
    else if(hour >= 18 && hour < 24 )
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good evening,"];
        
        //  NSLog(@"Good evening,");
    }
    
    
    NSString *str_Week;
    NSString *str_Present_Past_Future;
    if([destinationDate isTODO_Cal_Today:destinationDate])
        
        str_Present_Past_Future= @"Today is";
    
    else if([destinationDate isTODO_Cal_Tomorrow:destinationDate])
        str_Present_Past_Future= @"Tomorrow is";
    
    else if([destinationDate isTODO_Cal_Yesterday:destinationDate])
        str_Present_Past_Future= @"Yesterday was";
    else 
        str_Present_Past_Future= @"";
    
    switch ([components weekday]) {
        case 1:
            str_Week = @"Sunday";
            
            break;
        case 2:
            str_Week = @"Monday";
            
            break;
        case 3:
            str_Week = @"Tuesday";
            break;
        case 4:
            str_Week = @"Wednesday";
            break;
        case 5:
            str_Week = @"Thrusday";
            break;
        case 6:
            str_Week = @"Friday";
            break;
        case 7:
            str_Week = @"Saturday";
            break;
        default:
            break;
    }
    if( selectedButtonIndex ==2 || selectedButtonIndex ==5 || selectedButtonIndex ==6 || selectedButtonIndex ==7 || selectedButtonIndex ==9 )// Due Date Index
    {
        return self.strTextToSpeak;
    }
    else
    {
        self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@ %@,",str_Present_Past_Future,str_Week];
        
        self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@,",self.str_todaysDate];
        
        
    }
     // NSLog(@"self.strTextToSpeak  %@",self.strTextToSpeak);
    return self.strTextToSpeak;
    
    
}
-(NSMutableString *)stringFortext
{
    int E_Number = 0;//[self numberofAppointments];
    
    switch (selectedButtonIndex) {
            
        case 100://All Tasks   // Changed 0 to 100
            self.str_SelectedButtonTitle = (NSMutableString *)@"All";
            
            break;
            
        case 1://Today's
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".Today you have %d Tasks,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".Today you have %d Task,\n",E_Number];
            break;
            
        case 2://Projects
            if(E_Number>1)
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Tasks total,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Task,",E_Number];
            break;
            
        case 3:
            self.str_SelectedButtonTitle = (NSMutableString *)@"Starred";
            
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Starred tasks,\n Here is a partial list,\n",E_Number];            
            else
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Starred task,",E_Number];   
            break;
            
        case 4:
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d over due tasks,\n Here is a partial list,\n",E_Number];            
            else
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d over due task,",E_Number];   
            break;
            
        case 5:// Priority
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Tasks total,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Task,",E_Number];
            break;
            
        case 6://Due Date
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Tasks total,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Task,",E_Number];
            break; 
            
        case 7://Progress
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Tasks total,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Task,",E_Number];
            break;
            
        case 8://Done
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d done tasks,\n Here is a partial list,\n",E_Number];            
            else
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d done task,",E_Number];   
            break;
            
        case 9://Date Created
            if(E_Number>1)
                
                self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Tasks total,\n Here is a partial list,\n",E_Number];
            
            else
                self.str_SelectedButtonTitle = self.str_SelectedButtonTitle = [NSMutableString stringWithFormat:@".You have %d Task,",E_Number];
            break;
            
        default:
            break;
    }
    
    return self.str_SelectedButtonTitle;
    
}



@end

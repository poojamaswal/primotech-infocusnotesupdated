//
//  Cal_Todo_OptionsVC.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 6/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Cal_Todo_OptionsVC : UIViewController {
	IBOutlet UIImageView	*borderImageView;
	IBOutlet UIImageView	*arrowImageView;
	IBOutlet UIButton		*btnShowAllTodo;
	IBOutlet UILabel		*lblShowAllTodo;
	

	CGRect		contentFrame; 
	NSString	*arrowDirection;
	CGPoint		arrowStartPoint;
	id			parentVC;
}
@property(nonatomic, strong)UIButton *btnShowAllTodo;


-(id)initWithContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal withParentVC:(id)parentVCLocal;

-(void)addAdditionalControlsInSearchBar; 

-(IBAction)showAllTodobtn_Clicked:(id) sender;

@end

//
//  CustomCalendarColorViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/7/13.
//
//

#import "CustomCalendarColorViewController.h"


#define kBlackButtonTag     1001
#define kCharcoalButtonTag  1002
#define kdarkBlueButtonTag  1003
#define kBlueButtonTag      1004
#define kpurpleButtonTag    1005

#define kPinkButtonTag      1006
#define kDarkGreenButtonTag 1007
#define kGreenkButtonTag    1008
#define kOrange1ButtonTag   1009
#define kOrange2ttonTag     1010

#define kDarkRedButtonTag   1011
#define kRedButtonTag       1012
#define kLightPinkButtonTag 1013
#define kTealButtonTag      1014
#define kBronwButtonTag     1015


@interface CustomCalendarColorViewController ()

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic) IBOutlet UIView *finalColorView;

@property (strong, nonatomic) IBOutlet UITextField *redNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *greenNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *blueNumberTextField;

@property (strong, nonatomic) IBOutlet UIView *blackView;
@property (strong, nonatomic) IBOutlet UIView *charcoalView;
@property (strong, nonatomic) IBOutlet UIView *darkBlueView;
@property (strong, nonatomic) IBOutlet UIView *blueView;
@property (strong, nonatomic) IBOutlet UIView *purpleView;

@property (strong, nonatomic) IBOutlet UIView *pinkView;
@property (strong, nonatomic) IBOutlet UIView *darkGreenView;
@property (strong, nonatomic) IBOutlet UIView *greenView;
@property (strong, nonatomic) IBOutlet UIView *orange1View;
@property (strong, nonatomic) IBOutlet UIView *orange2View;

@property (strong, nonatomic) IBOutlet UIView *darkRedView;
@property (strong, nonatomic) IBOutlet UIView *redView;
@property (strong, nonatomic) IBOutlet UIView *lightPinkView;
@property (strong, nonatomic) IBOutlet UIView *tealView;
@property (strong, nonatomic) IBOutlet UIView *brownView;

@property (strong, nonatomic) IBOutlet UISlider *redSlider;
@property (strong, nonatomic) IBOutlet UISlider *greenSlider;
@property (strong, nonatomic) IBOutlet UISlider *blueSlider;



- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender;
- (IBAction)sliderValueChanged:(id)sender;
-(void)presetColorButtonClicked: (UIButton*) sender;




@end



@implementation CustomCalendarColorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.isCustomColor) {
        
         _finalColorView.backgroundColor = [UIColor colorWithRed: self.calendarObject.calendarColorRed green:self.calendarObject.calendarColorGreen blue:self.calendarObject.calendarColorBlue alpha:1.0];
        
        int redColor = self.calendarObject.calendarColorRed * 255;
        int greenColor = self.calendarObject.calendarColorGreen * 255;
        int blueColor = self.calendarObject.calendarColorBlue * 255;
        
        
        _redSlider.value = redColor;
        _greenSlider.value = greenColor;
        _blueSlider.value = blueColor;
        
        _redNumberTextField.text = [NSString stringWithFormat:@"%d", redColor];
        _greenNumberTextField.text = [NSString stringWithFormat:@"%d", greenColor];
        _blueNumberTextField.text = [NSString stringWithFormat:@"%d", blueColor];
  
    }
    
    
    
    //Black Preset - set up invisible buttons for preset colors
    UIButton *blackInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_blackView.frame.origin.x, _blackView.frame.origin.y, _blackView.frame.size.width, _blackView.frame.size.height)];
    [blackInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    blackInvisibleButton.tag = kBlackButtonTag;
    [self.view addSubview:blackInvisibleButton];
    
    //Charcoal Preset - set up invisible buttons for preset colors
    UIButton *charcoalInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_charcoalView.frame.origin.x, _charcoalView.frame.origin.y, _charcoalView.frame.size.width, _charcoalView.frame.size.height)];
    [charcoalInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    charcoalInvisibleButton.tag = kCharcoalButtonTag;
    [self.view addSubview:charcoalInvisibleButton];
    
    //Dark Blue Preset - set up invisible buttons for preset colors
    UIButton *darkBlueInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_darkBlueView.frame.origin.x, _darkBlueView.frame.origin.y, _darkBlueView.frame.size.width, _darkBlueView.frame.size.height)];
    [darkBlueInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    darkBlueInvisibleButton.tag = kdarkBlueButtonTag;
    [self.view addSubview:darkBlueInvisibleButton];
    
    //Blue Preset - set up invisible buttons for preset colors
    UIButton *blueInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_blueView.frame.origin.x, _blueView.frame.origin.y, _blueView.frame.size.width, _blueView.frame.size.height)];
    [blueInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    blueInvisibleButton.tag = kBlueButtonTag;
    [self.view addSubview:blueInvisibleButton];
    
    
    //Purple Preset - set up invisible buttons for preset colors
    UIButton *purpleInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_purpleView.frame.origin.x, _purpleView.frame.origin.y, _purpleView.frame.size.width, _purpleView.frame.size.height)];
    [purpleInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    purpleInvisibleButton.tag = kpurpleButtonTag;
    [self.view addSubview:purpleInvisibleButton];
    
    
    //Pink Preset - set up invisible buttons for preset colors
    UIButton *pinkInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_pinkView.frame.origin.x, _pinkView.frame.origin.y, _pinkView.frame.size.width, _pinkView.frame.size.height)];
    [pinkInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    pinkInvisibleButton.tag = kPinkButtonTag;
    [self.view addSubview:pinkInvisibleButton];
    
    
    //Dark Green Preset - set up invisible buttons for preset colors
    UIButton *darkGreenInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_darkGreenView.frame.origin.x, _darkGreenView.frame.origin.y, _darkGreenView.frame.size.width, _darkGreenView.frame.size.height)];
    [darkGreenInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    darkGreenInvisibleButton.tag = kDarkGreenButtonTag;
    [self.view addSubview:darkGreenInvisibleButton];
    
    
    //Green Preset - set up invisible buttons for preset colors
    UIButton *greenInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_greenView.frame.origin.x, _greenView.frame.origin.y, _greenView.frame.size.width, _greenView.frame.size.height)];
    [greenInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    greenInvisibleButton.tag = kGreenkButtonTag;
    [self.view addSubview:greenInvisibleButton];
    
    
    //Orange1 Preset - set up invisible buttons for preset colors
    UIButton *orange1InvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_orange1View.frame.origin.x, _orange1View.frame.origin.y, _orange1View.frame.size.width, _orange1View.frame.size.height)];
    [orange1InvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    orange1InvisibleButton.tag = kOrange1ButtonTag;
    [self.view addSubview:orange1InvisibleButton];
    
    
    //Orange2 Preset - set up invisible buttons for preset colors
    UIButton *orange2InvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_orange2View.frame.origin.x, _orange2View.frame.origin.y, _orange2View.frame.size.width, _orange2View.frame.size.height)];
    [orange2InvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    orange2InvisibleButton.tag = kOrange2ttonTag;
    [self.view addSubview:orange2InvisibleButton];
    
    
    //Dark Red Preset - set up invisible buttons for preset colors
    UIButton *darkRedInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_darkRedView.frame.origin.x, _darkRedView.frame.origin.y, _darkRedView.frame.size.width, _darkRedView.frame.size.height)];
    [darkRedInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    darkRedInvisibleButton.tag = kDarkRedButtonTag;
    [self.view addSubview:darkRedInvisibleButton];
    
    
    //Red Preset - set up invisible buttons for preset colors
    UIButton *redInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_redView.frame.origin.x, _redView.frame.origin.y, _redView.frame.size.width, _redView.frame.size.height)];
    [redInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    redInvisibleButton.tag = kRedButtonTag;
    [self.view addSubview:redInvisibleButton];
    
    
    //Light Pink Preset - set up invisible buttons for preset colors
    UIButton *lightPinkInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_lightPinkView.frame.origin.x, _lightPinkView.frame.origin.y, _lightPinkView.frame.size.width, _lightPinkView.frame.size.height)];
    [lightPinkInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    lightPinkInvisibleButton.tag = kLightPinkButtonTag;
    [self.view addSubview:lightPinkInvisibleButton];
    
    
    //Teal Preset - set up invisible buttons for preset colors
    UIButton *tealInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_tealView.frame.origin.x, _tealView.frame.origin.y, _tealView.frame.size.width, _tealView.frame.size.height)];
    [tealInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    tealInvisibleButton.tag = kTealButtonTag;
    [self.view addSubview:tealInvisibleButton];
    
    
    //Brown Preset - set up invisible buttons for preset colors
    UIButton *brownInvisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(_brownView.frame.origin.x, _brownView.frame.origin.y, _brownView.frame.size.width, _brownView.frame.size.height)];
    [brownInvisibleButton addTarget:self action:@selector(presetColorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    brownInvisibleButton.tag = kBronwButtonTag;
    [self.view addSubview:brownInvisibleButton];
    
	
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (IS_IOS_7) {
        
        //pooja-iPad
        if(IS_IPAD)
        {
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.translucent = YES;
            [self.navigationController.navigationBar setBarTintColor:[UIColor darkGrayColor]];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            _doneButton.tintColor = [UIColor whiteColor];
        }
        else
        {
        
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            _doneButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
    
        }
    
    }
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //pooja-iPad
    if(IS_IPAD)
    {
         _finalColorView.frame = CGRectMake(286, 807, 196, 162);
    }
    else if (!IS_IPHONE_5) { //Not iPhone 5
        
         _finalColorView.frame = CGRectMake(10 + 10, 353, 300 - 20, 45);
    }
    
    _redNumberTextField.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _redNumberTextField.layer.borderWidth = 1;
    _greenNumberTextField.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _greenNumberTextField.layer.borderWidth = 1;
    _blueNumberTextField.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _blueNumberTextField.layer.borderWidth = 1;
   
    
}

-(void)presetColorButtonClicked: (UIButton*) sender{
    
    
    if (sender.tag == kBlackButtonTag) {
        
        [_redSlider setValue:0.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kCharcoalButtonTag){
        
        [_redSlider setValue:100.0f animated:YES];
        [_greenSlider setValue:100.0f animated:YES];
        [_blueSlider setValue:100.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kdarkBlueButtonTag){
        
        [_redSlider setValue:0.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:130.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kBlueButtonTag){
        
        [_redSlider setValue:0.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:255.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kpurpleButtonTag){
        
        [_redSlider setValue:90.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:255.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kPinkButtonTag){
        
        [_redSlider setValue:255.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:255.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kDarkGreenButtonTag){
        
        [_redSlider setValue:0.0f animated:YES];
        [_greenSlider setValue:130.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kGreenkButtonTag){
        
        [_redSlider setValue:0.0f animated:YES];
        [_greenSlider setValue:255.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kOrange1ButtonTag){
        
        [_redSlider setValue:255.0f animated:YES];
        [_greenSlider setValue:194.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kOrange2ttonTag){
        
        [_redSlider setValue:255.0f animated:YES];
        [_greenSlider setValue:130.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kDarkRedButtonTag){
        
        [_redSlider setValue:130.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kRedButtonTag){
        
        [_redSlider setValue:255.0f animated:YES];
        [_greenSlider setValue:0.0f animated:YES];
        [_blueSlider setValue:50.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kLightPinkButtonTag){
        
        [_redSlider setValue:255.0f animated:YES];
        [_greenSlider setValue:60.0f animated:YES];
        [_blueSlider setValue:130.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kTealButtonTag){
        
        [_redSlider setValue:130.0f animated:YES];
        [_greenSlider setValue:130.0f animated:YES];
        [_blueSlider setValue:0.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
    else if (sender.tag == kBronwButtonTag){
        
        [_redSlider setValue:115.0f animated:YES];
        [_greenSlider setValue:75.0f animated:YES];
        [_blueSlider setValue:15.0f animated:YES];
        
        [self sliderValueChanged:self]; //handles the Final colorView & textfields with values
    }
 
    
}


- (IBAction)sliderValueChanged:(id)sender {
    
    int redValue = _redSlider.value;
    int greenValue = _greenSlider.value;
    int blueValue = _blueSlider.value;
    
    _redNumberTextField.text =  [NSString stringWithFormat:@"%d",redValue];
    _greenNumberTextField.text =  [NSString stringWithFormat:@"%d",greenValue];
    _blueNumberTextField.text =  [NSString stringWithFormat:@"%d",blueValue];
    
    _finalColorView.backgroundColor = [UIColor colorWithRed:_redSlider.value/255.0f green:_greenSlider.value/255.0f blue:_blueSlider.value/255.0f alpha:1.0];
    
}


- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender {
    
    self.calendarObject.calendarColorRed = _redSlider.value/255.0f;
    self.calendarObject.calendarColorGreen = _greenSlider.value/255.0f;
    self.calendarObject.calendarColorBlue = _blueSlider.value/255.0f;
    
    
    
    if ([self.delegate respondsToSelector:@selector(updateCalendarObject:)]) {
        
        [self.delegate updateCalendarObject:self.calendarObject];
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end

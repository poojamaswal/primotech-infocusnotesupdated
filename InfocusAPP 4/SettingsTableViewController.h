//
//  SettingsTableViewController.h
//  Organizer
//
//  Created by Steven Abrams on 1/29/13.
//
//

#import <UIKit/UIKit.h>
#import "Appirater.h"

@interface SettingsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate, UITextFieldDelegate, AppiraterDelegate>
{
    
   // IBOutlet UINavigationBar *navBar;
    NSString                *AppsTarget;
    BOOL                    IsAlert;
    NSString                *ItuneURL;
    
    BOOL                    isMenuClosed; //Steve
    
}

@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) NSString *itunesURL_Review;

- (IBAction)backButton_Clicked:(UIButton *)sender;



@end

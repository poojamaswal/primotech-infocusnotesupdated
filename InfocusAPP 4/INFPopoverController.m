//
//  INFPopoverController.m
//  InfocusNotesApp
//
//  Created by jaswinder blagun on 29/07/2015.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import "INFPopoverController.h"

@implementation INFPopoverController

__strong static INFPopoverController *sharedController;

+ (INFPopoverController*)sharedObject {
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        sharedController = [[self alloc] init];
    });
    return sharedController;
}

+ (UIPopoverController*)currentPopoverController {
    return sharedController.popoverController;
}

@end
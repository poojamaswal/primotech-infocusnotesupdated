//
//  ViewAppointmentScreen.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 7/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentsDataObject.h"



@interface ViewAppointmentScreen : UIViewController<MFMailComposeViewControllerDelegate> {
	IBOutlet UIView *containerView;
	IBOutlet UITableView *appDetailTableView;
	
	AppointmentsDataObject *appDataObj;
	NSInteger numberOfRows; 
    IBOutlet UINavigationBar *navBar; 
    
	CalendarDataObject          *calObj; //Steve added
    NSMutableDictionary         *calDataDictionary; //Steve added
    CalendarDataObject          *defaultcal;//Steve added
    NSString                    *calendarIdentifier;//Steve added
    BOOL                        isCalendarAllowedToBeModified; //Steve - if Not allowed, then doesn't allow Editing Event

    IBOutlet UIButton *shareButton; //Steve
    BOOL isRepeatFilled; //Steve
    int  previousIndexPathRow;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *editButton;
}


@property(nonatomic, strong) AppointmentsDataObject *appDataObj;  
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar_iPad;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton_iPad;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton_iPad;



-(id)initWithAppObject:(AppointmentsDataObject *) appObj;
-(IBAction)backButton_Clicked:(id) sender;
-(IBAction)editButton_Clicked:(id) sender;
-(IBAction)shareButton_Clicked:(id) sender;
-(NSString*)getCalendarName:(NSString *)calname;
-(void) checkIfCalendarIsModifiable; //Steve added

@end

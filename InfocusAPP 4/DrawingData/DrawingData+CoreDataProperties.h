//
//  DrawingData+CoreDataProperties.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DrawingData.h"

NS_ASSUME_NONNULL_BEGIN

@interface DrawingData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *drawingImageData;
@property (nullable, nonatomic, retain) NotesID *notesID;
@property (nonatomic, nullable, retain) NSDate *savedDate;
@property (nonatomic, nullable, retain) NSDate *idForResizableImages;
@property (nonatomic, nullable, retain) NSData *drawingIconData;


@end

NS_ASSUME_NONNULL_END

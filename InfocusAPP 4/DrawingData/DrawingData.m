//
//  DrawingData.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DrawingData.h"
#import "NotesID.h"
#import "DrawingDefaults.h"
#import "DatabaseManager.h"


@implementation DrawingData

// Insert code here to add functionality to your managed object subclass

+(DrawingData*)addDrawingData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context
{
    
    
    //    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"DrawingData"];
    //    NSError *error = nil;
    //    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    //    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    //    array = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
    //
    DrawingData *aDrawingData;
    //    NSMutableArray *arrData  = [[NSMutableArray alloc]init];
    //    for(int i=0;i<array.count;i++)
    //    {
    //        aDrawingData = [array objectAtIndex:i];
    //        if([aDrawingData.notesID.pageID isEqualToString:[DrawingDefaults sharedObject].strNoteName])
    //        {
    //            [arrData addObject:aDrawingData];
    //        }
    //    }
    NSArray *arrData = [[DatabaseManager sharedManager] arrayForDrawingDataForPresentNoteID];
    
    if([DrawingDefaults sharedObject].intNotesSubNotesCounter-1 == arrData.count)
    {
        NSLog(@"EQUAL------------------");
        NSEntityDescription *desc = [NSEntityDescription entityForName:@"DrawingData"
                                                inManagedObjectContext:context];
        aDrawingData = [[DrawingData alloc] initWithEntity:desc insertIntoManagedObjectContext:nil];
        
        if([dictData valueForKey:KeyData])
            aDrawingData.drawingImageData = [dictData valueForKey:KeyData];
        
        
        aDrawingData.savedDate = [NSDate date];
        
        [DrawingDefaults sharedObject].dateStored = aDrawingData.savedDate;
        
        aDrawingData.drawingIconData = [dictData valueForKey:KeyIconData];
        
        [context insertObject:aDrawingData];
        
    }
    else
    {
        NSLog(@"NO ------ EQUAL------------------");
        aDrawingData = [arrData objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
        
        NSLog(@"%@",aDrawingData.notesID);
        
        if([dictData valueForKey:KeyData])
            aDrawingData.drawingImageData = [dictData valueForKey:KeyData];
        
        aDrawingData.savedDate = [NSDate date];
        
        NSLog(@"%@",aDrawingData.savedDate);
        [DrawingDefaults sharedObject].dateStored = aDrawingData.savedDate;
        
        aDrawingData.drawingIconData = [dictData valueForKey:KeyIconData];
        
    }
    
    
    
    return aDrawingData;//risk
    
}

@end

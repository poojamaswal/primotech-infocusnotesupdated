//
//  DrawingData+CoreDataProperties.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DrawingData+CoreDataProperties.h"

@implementation DrawingData (CoreDataProperties)

@dynamic drawingImageData;
@dynamic notesID;
@dynamic savedDate;
@dynamic idForResizableImages;
@dynamic drawingIconData;

@end

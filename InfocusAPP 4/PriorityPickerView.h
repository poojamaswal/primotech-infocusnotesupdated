//
//  PriorityPickerView.h
//  Organizer
//
//  Created by Naresh Chauhan on 12/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kFillingComponent  0
#define kBreadComponent  1

@interface PriorityPickerView :UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
{
	IBOutlet UIPickerView *doublePicker;
	NSArray *fillingTypes;
	NSArray *breadTypes;
	id __unsafe_unretained delegate;
	NSString *first;
	NSString *second;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIView *helpView; //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;  //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView;  //Steve
    IBOutlet UISwitch *prioritySwitch; //Steve
    IBOutlet UILabel *priorityLabel;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    IBOutlet UILabel *priorityFixedLabel;
    
    
}
@property(unsafe_unretained)id delegate;

@property (nonatomic,strong)  UIPickerView *doublePicker;
@property (nonatomic,strong)  NSArray *fillingTypes;
@property (nonatomic,strong)  NSArray *breadTypes;
@property (nonatomic, strong) UISwitch *prioritySwitch; //Steve


-(IBAction)buttonPressed;
-(IBAction)Clicl_Cancel:(id)sender;
-(IBAction)Click_Save:(id)sender;
- (IBAction)helpView_Tapped:(id)sender; //Steve
- (IBAction)PrioritySwitchClicked:(id)sender; //Steve

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil setNewPriority:(NSString*) valueone;



@end


//
//  LIstToDoProjectViewController.m
//  Organizer
//
//  Created by alok gupta on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LIstToDoProjectViewController.h"
#define SCROLL_VIEW_TAG				61111
#define Progress_BTN_TAG			71111
#define BADGE_VIEW_TAG				81111
#define TABLE_ROW_HEIGHT_IPHONE		55
#define TABLE_ROW_HEIGHT_IPAD		70
#define BLACK_BOX_TAG 3333
#define BLACK_BUTTON_TAG 6666
#define TEXT_FIELD_TAG 4444

#define INPUT_VIEW_TAG 5555
#import "ListToDoViewController.h"
#import "ToDoViewController.h"
//#import "InputTypeSelectorViewController.h"
#import "OrganizerAppDelegate.h"
#import "HomeViewController.h"
//#import "CustomBadge.h"
#import "TODODataObject.h"
#import "CalendarParentController.h"
#import "Cal_Todo_OptionsVC.h"
#import "ViewTODOScreen.h"
#import "ProjectMainViewCantroller.h"
#import "AddEditStartDateEndDateVC.h"

//Steve added: Gesture Recognizers
#import "TransformableTableViewCell.h"
#import "JTTableViewGestureRecognizer.h"
#import "UIColor+JTGestureBasedTableViewHelper.h"

//Steve added
#import "AllInsertDataMethods.h"
#import "HandWritingViewController.h"
#import "VoiceToTextViewController.h"
//#import "AdWhirlView.h"
#import "QuantcastMeasurement.h"
#import "Flurry.h" //Steve
#import "PasswordProtectAppViewController.h"//Steve



@interface LIstToDoProjectViewController()<JTTableViewGestureEditingRowDelegate, JTTableViewGestureAddingRowDelegate,JTTableViewGestureMoveRowDelegate, JTTableViewGestureSwipeUpDelegate,JTTableViewGestureSwipeDownDelegate>
{
    OrganizerAppDelegate * app_delegate;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;


-(void)showSearchOptPopover:(BOOL)show;

@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, strong) JTTableViewGestureRecognizer *tableViewRecognizer;
@property (nonatomic, strong) id grabbedObject;
-(void)getProject;
//*** UNUSED
-(void)getValuesFromDatabase;

@end

@implementation LIstToDoProjectViewController

//ALok Added
@synthesize projectdataobj,ProjectDataObjArray;


@synthesize  toDoTableView;
//@synthesize filterParentScrollView;
//@synthesize selectedButtonIndex;
//@synthesize showDoneTODOs;

//Steve added 
@synthesize rows;
@synthesize tableViewRecognizer;
@synthesize grabbedObject;
//@synthesize todoTitleBinary, todoTitleBinaryLarge;
//@synthesize adView;

//Steve added
@synthesize myBackgroundColor, myFontColor, myFontName, myCellBackgroundColor;

//Steve added
//@synthesize tosoDataObj;
//@synthesize inputTextField;

#define PLACEHOLDER @"Drag down to create a new To Do"
#define ADDING_CELL @"Continue..."
#define DONE_CELL @"Done"
#define DUMMY_CELL @"Dummy"
#define COMMITING_CREATE_CELL_HEIGHT 60
#define NORMAL_CELL_FINISHING_HEIGHT 60


#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -


/*- (id)initWithProjectID:(NSInteger ) projId 
 {
 //Steve added
 NSLog(@"******* initWithProjectID ********");
 
 if (self = [super initWithNibName:@"ListToDoViewController" bundle:[NSBundle mainBundle]]) 
 {
 projectIDFromProjMod  = projId;
 }
 return self;
 }*/


- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    NSLog(@"ListToDoProjectViewController");
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"List" withLabels:Nil]; //Steve
    [Flurry logEvent:@"List"]; //Steve
    
    self.view.userInteractionEnabled = YES;
    isMenuClosed = YES; // if viewDidLoad method is called, then Menu is closed.  Used to set userEndabled to YES when Reveal button clicked (revealToggleHandle)
    self.navigationController.navigationBarHidden = NO;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Steve - loads password viewController
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        if ( ![sharedDefaults boolForKey:@"PasswordAccepted"] && [sharedDefaults boolForKey:@"NavigationNew"]){
            
            PasswordProtectAppViewController *passwordProtectAppViewController=
            [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController" bundle:[NSBundle mainBundle]];
            
            [self.navigationController pushViewController:passwordProtectAppViewController animated:NO];
        }
    }

     if(IS_LITE_VERSION)
    {
        //AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
        //[adWhirlView setFrame:CGRectMake(0, 366, 320, 50)];
        //[adWhirlView setBackgroundColor:[UIColor blackColor]];
        //[self.view addSubview:adWhirlView];
        [super viewDidLoad];
        [toDoTableView setFrame:CGRectMake(0, 44, 320, 326)];
    }
    
    if(!IS_LIST_VERSION){
        NSMutableArray     *items = [bottomBar.items mutableCopy];
        [items removeObjectAtIndex:5];
        [items removeObjectAtIndex:4];
        [items removeObjectAtIndex:6];
        [items removeLastObject];
        bottomBar.items = items;
    }
    else{
        [homeBtn removeFromSuperview];
        [noteBtn removeFromSuperview];
        [projectBtn removeFromSuperview];
        [todoBtn removeFromSuperview];
        //Alok Gupta added Calender icon tag set on XIB and tag = 1116
        [[self.view viewWithTag:1116] setFrame:CGRectMake(5,3,48,34)];
        [[[navBar items]objectAtIndex:0] setTitle:@"InFocus List"];
    }
    
    ProjectDataObjArray = [[NSMutableArray alloc]init];
    getBIDArray=[[NSMutableArray alloc]init];
    
    // selectedButtonIndex=1;
    if(IS_IPAD)
    {
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
    }
}


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}


-(void)viewWillAppear:(BOOL)animated
{
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    //isViewAppearing = YES; //
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        //NSLog(@"New Navigation");
        
        navBar.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        if (IS_IOS_7) {
   
            self.edgesForExtendedLayout = UIRectEdgeNone;
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.view.backgroundColor = [UIColor whiteColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                
                bottomBar.hidden = NO;
                bottomBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                bottomBar.translucent = NO;
                [self.view bringSubviewToFront:bottomBar];
                
                _shareButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                _searchButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                        [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.view.backgroundColor = [UIColor blackColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                
                
                bottomBar.hidden = NO;
                bottomBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor whiteColor];
                bottomBar.translucent = NO;
                [self.view bringSubviewToFront:bottomBar];
                
                 [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            }

#pragma mark -- IPad Primotech
            
            if(IS_IPAD)
            {
                UIImage * imgSearch;
                UIButton * btnSearch;
                imgSearch = [UIImage imageNamed:@"search_icon.png"];
                btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnSearch setImage:imgSearch forState:UIControlStateNormal];
                btnSearch.showsTouchWhenHighlighted = YES;
                btnSearch.frame = CGRectMake(0.0, 0.0, imgSearch.size.width, imgSearch.size.height);
                
                [btnSearch addTarget:self action:@selector(searchBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * search = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
               
                UIImage * imgShare;
                UIButton * btnShare;
                imgShare = [UIImage imageNamed:@"share_icon.png"];
                btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnShare setImage:imgShare forState:UIControlStateNormal];
                btnShare.showsTouchWhenHighlighted = YES;
                btnShare.frame = CGRectMake(0.0, 0.0, imgShare.size.width, imgShare.size.height);
                
                [btnShare addTarget:self action:@selector(mailBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem * share = [[UIBarButtonItem alloc]initWithCustomView:btnShare];
                
                
                self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:search,share,nil];
                self.navigationItem.title = @"";
                
                self.navigationController.navigationBar.hidden = NO;
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
  
                
            }
            else
            {
                if (IS_IPHONE_5){
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    bottomBar.frame = CGRectMake(0, 524, 320, 44);
                    toDoTableView.frame = CGRectMake(0, 0, 320, 460 + 64);
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 568);
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                }
                else{
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    bottomBar.frame = CGRectMake(0, 436, 320, 44);
                    toDoTableView.frame = CGRectMake(0, 0, 320, 372 + 64);
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 480);
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                }
                
                UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                                     style:UIBarButtonItemStylePlain
                                                                                    target:self
                                                                                    action:@selector(revealToggleHandle)];
                
                self.navigationItem.leftBarButtonItem = revealButtonItem;
                
                revealButtonItem.tintColor = [UIColor whiteColor];
               
            }

             [self.view bringSubviewToFront:bottomBar];
  
            
        }
        else{ //iOS 6
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
            
            
            if (IS_IPHONE_5)
                toDoTableView.frame = CGRectMake(0, 0, 320, 460);
            else
                toDoTableView.frame = CGRectMake(0, 0, 320, 372);
            
            
            UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon_OLD.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(revealToggleHandle)];
            
            
            self.navigationItem.leftBarButtonItem = revealButtonItem;
            revealButtonItem.tintColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
            
        }
        
        if(IS_IPAD)
        {
            self.title = @"";
        }
        else
        {
            self.title = @"CheckList";
            
            //Plus Button
            self.navigationItem.rightBarButtonItem = nil;
        }
        
    }
    else{
        //NSLog(@"Old Navigation");
        
        self.navigationController.navigationBarHidden = YES;
        navBar.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
    }
    
    // ***************** Steve End ***************************
    
    self.toDoTableView.showsVerticalScrollIndicator = NO;
    
    //[searchBarViewController resignFirstResponder];
    [self.view endEditing:YES];
    [self showSearchOptPopover:NO];
    searchBarViewController.view.alpha = 0;
    
    //UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    //[navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    indexWhereCellIsBeingInserted = 0;
    
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

    
    [[self.view viewWithTag:TEXT_FIELD_TAG] resignFirstResponder];
    [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
    [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
    [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
    [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
    
    //NSLog(@"******* viewWillAppear ********");
    
    [super viewWillAppear: YES];
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    [self getProject];

    
    if(!self.tableViewRecognizer){  // Anil's Addition
        self.tableViewRecognizer = [toDoTableView enableGestureTableViewWithDelegate:self];
    }
    
    
    [toDoTableView reloadData];
    
    
    if (IS_IOS_7) {
        
        ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
        
        if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
            
            //Steve added - Custom colors and fonts
            //myBackgroundColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
            
            //Cell color
            myCellBackgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
            myFontColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f]; //Steve
            myBadgeColor = [UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f];//Steve
            myFontName = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            
            //toDoTableView.separatorStyle  = UITableViewCellSeparatorStyleSingleLine;
            toDoTableView.separatorStyle = UITableViewCellSelectionStyleNone;
            toDoTableView.separatorColor = [UIColor colorWithRed:69/255.0f green:165/255.0f blue:232/255.0f alpha:1.0f];
            toDoTableView.backgroundColor = [UIColor whiteColor];
           
        }
        else{ //Dark Theme
            
            //Steve added - Custom colors and fonts
            myBackgroundColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
            
            //Cell color
            myCellBackgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            myFontColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f]; //Steve
            myBadgeColor = [UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f];//Steve
            myFontName = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            
            toDoTableView.separatorStyle  = UITableViewCellSeparatorStyleSingleLine;
           
            toDoTableView.separatorColor = [UIColor colorWithRed:69/255.0f green:165/255.0f blue:232/255.0f alpha:1.0f];
            toDoTableView.backgroundColor = [UIColor colorWithRed:69/255.0f green:165/255.0f blue:232/255.0f alpha:1.0f];;
            
            //***Steve changed until colors are fixed.
            toDoTableView.backgroundColor = [UIColor blackColor];
            toDoTableView.separatorColor = [UIColor clearColor];
            myCellBackgroundColor = [UIColor whiteColor];
            myBackgroundColor = [UIColor blackColor];
            
        }
    }
    else{ //iOS 6
        
        //Steve added - Custom colors and fonts
        myBackgroundColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
        
        //Cell color
        myCellBackgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        myFontColor = [UIColor colorWithRed:20.0f/255.0f green:20.0f/255.0f blue:20.0f/255.0f alpha:1.0f]; //Steve
        myBadgeColor = [UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f];//Steve
        myFontName = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        
        //toDoTableView.separatorStyle  = UITableViewCellSeparatorStyleSingleLine;
        toDoTableView.separatorStyle = UITableViewCellSelectionStyleNone;
        //toDoTableView.separatorColor = [UIColor grayColor];
        toDoTableView.backgroundColor = [UIColor whiteColor];;
    }
    

    
    
    toDoTableView.rowHeight       = NORMAL_CELL_FINISHING_HEIGHT;
    
    //Keybaord visible
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector (keyboardDidHide:)
                                                 name: UIKeyboardDidHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector (keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    
    //NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    ///// steve - shows Instructions on draging with 1 finger to add to do
    if ([sharedDefaults boolForKey:@"FirstLaunch_List_2"]) {
        
        
        UITapGestureRecognizer *tapListHelpDrag = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(listHelpDragView_Tapped:)];
        UISwipeGestureRecognizer *swipeDownListHelpDrag = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listHelpDragView_Tapped:)];
        UISwipeGestureRecognizer *swipeUpListHelpDrag = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listHelpDragView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightListHelpDrag = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listHelpDragView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftListHelpDrag = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(listHelpDragView_Tapped:)];
        
        swipeDownListHelpDrag.direction = UISwipeGestureRecognizerDirectionDown;
        swipeUpListHelpDrag.direction = UISwipeGestureRecognizerDirectionUp;
        swipeRightListHelpDrag.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeLeftListHelpDrag.direction = UISwipeGestureRecognizerDirectionRight;
        
        [ListHelpDragView addGestureRecognizer:swipeDownListHelpDrag];
        [ListHelpDragView addGestureRecognizer:swipeUpListHelpDrag];
        [ListHelpDragView addGestureRecognizer:swipeRightListHelpDrag];
        [ListHelpDragView addGestureRecognizer:swipeLeftListHelpDrag];
        [ListHelpDragView addGestureRecognizer:tapListHelpDrag];
        ListHelpDragView.hidden = NO;
        
        
        [self.view addSubview:ListHelpDragView];
        //[ListHelpDragView bringSubviewToFront:self.view];
    }
    
        
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_List_2"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer  *swipeDownHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];
        [helpView addGestureRecognizer:tapHelpView];
        [helpView addGestureRecognizer:swipeDownHelpView];
        helpView.hidden = NO;
        
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = YES;
        }
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_List_2"];
        [sharedDefaults synchronize];
        
    }
    

}


-(void)viewDidAppear:(BOOL)animated{

}


-(void) revealToggleHandle{
    //NSLog(@"revealToggleHandle");
    
    if (isMenuClosed){
        self.view.userInteractionEnabled = NO;
        isMenuClosed = NO;
    }
    else{
        self.view.userInteractionEnabled = YES;
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
    [revealController revealToggle:self];
}


- (void)handleGestureFrom:(UISwipeGestureRecognizer *)recognizer 
{
    if ([recognizer numberOfTouches] == 2) {
        // do whatever you need to do
       // NSLog(@"%s",__FUNCTION__);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"detected" message:@"HI" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"detected" message:@"HI" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}

- (void) listHelpDragView_Tapped:(id) sender{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [ListHelpDragView.layer addAnimation:transition forKey:kCATransitionFade];
    
    ListHelpDragView.hidden = YES;
    
    [sharedDefaults setBool:NO forKey:@"FirstLaunch_List"];
    [sharedDefaults synchronize];

    
}


#pragma mark -
#pragma mark Button Actions
#pragma mark -

//Check box clicked
- (void)labelBtn_Clicked:(id)sender 
{
    //NSLog(@"******* labelBtn_Clicked: ********");
    
	UIButton *tempBtn = (UIButton *)sender;
    indexWhereCellIsBeingInserted = tempBtn.tag;
    
    //Steve - changes highlighted cell
    if (IS_IPAD) {
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        [sharedDefaults setInteger:projDataObj.projectid forKey:@"ChecklistSelectedRow_iPad"]; //highlights new cell
        [toDoTableView reloadData];
    }
    
	NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
	
	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
	
    ProjId =  [[components objectAtIndex:0]intValue];
    ProjTitle  =  [components objectAtIndex:1];
    if(ProjId==0)return;
    
    
    isEditingProj = YES;
    
    //***************************************** 
    //set up Input view and fields
    //*****************************************
    
    NSUserDefaults *shareDefaults = [NSUserDefaults standardUserDefaults];
    UIView *inputView;
    
    //new view and input textfield to enter new todo
    if ([shareDefaults boolForKey:@"NavigationNew"]) 
         inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 320, NORMAL_CELL_FINISHING_HEIGHT)];
    else
        inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42, 320, NORMAL_CELL_FINISHING_HEIGHT)];
    
    inputView.backgroundColor = [UIColor whiteColor];
    //inputView.alpha = 0.0; //Steve added to animate
    [self.view addSubview:inputView];
    // [UIView animateWithDuration:0.10 animations:^{inputView.alpha = 1.0f;}];  //Steve added to animate 
    
    [inputView setTag:INPUT_VIEW_TAG];
    
    UITextField *inputTextField;
    if ([shareDefaults boolForKey:@"NavigationNew"]) {
        
        if (IS_IOS_7) {
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,0,320,NORMAL_CELL_FINISHING_HEIGHT)];
        }
        else{ //iOS 6
            
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,10,320,NORMAL_CELL_FINISHING_HEIGHT)];
        }
    }
    else{ //Old Navigation
        
        inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10,320,NORMAL_CELL_FINISHING_HEIGHT)];
    }
    
    
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.font = myFontName;
    inputTextField.textColor = myFontColor;
    [self.view addSubview:inputTextField];
    
    [inputTextField setTag:TEXT_FIELD_TAG];
    [inputTextField setReturnKeyType:UIReturnKeyDone];
    [inputTextField becomeFirstResponder];
    [inputTextField addTarget:self action:@selector(keyBoardDone:)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    // [inputTextField setText:editItem.todoTitle];
    [inputTextField setText:ProjTitle];
    
    UIView *blackBox;
    
    //black opaque space between input box and keyboard
    if (IS_IPAD) { //Steve
        blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 800)];
    }
    else{ //iPhone
        blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 254)];
    }
    

    
    blackBox.backgroundColor = [UIColor blackColor];
    blackBox.alpha = 0.00;  //Steve changed was: 0.90
    blackBox.tag = BLACK_BOX_TAG;
    [self.view addSubview:blackBox];
    [UIView animateWithDuration:0.35 animations:^{blackBox.alpha = 0.90f;}];  //Steve added to animate blackBox
    
    UIButton *blackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //Steve
    if (IS_IPAD) {
        [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 1024)];
    }
    else{ //iPhone
        [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 254)];
    }

    
    [blackButton addTarget:nil action:@selector(saveProjectToDB:) forControlEvents:UIControlEventTouchUpInside];
    [blackButton setTag:BLACK_BUTTON_TAG];
    [self.view addSubview:blackButton];
    
    
}



- (IBAction)homeButton_Clicked:(id)sender 
{
   // NSLog(@"******* homeButton_Clicked: ********");
	[self.navigationController  popToRootViewControllerAnimated:YES];
}

- (IBAction)calendarButton_Clicked:(id)sender 
{
    //NSLog(@"******* calendarButton_Clicked: ********");
    
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[appDelegate.navigationController popViewControllerAnimated:NO];
	
	CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[appDelegate.navigationController pushViewController:calendarParentController animated:YES];
}

- (IBAction)addToDoButton_Clicked:(id)sender {
    
   // NSLog(@"******* addToDoButton_Clicked: ********");
    
    ToDoParentController *toDoParentController = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:toDoParentController animated:YES];
}

- (IBAction)ProjectButton_click:(id)sender
{
    //NSLog(@"******* ProjectButton_click: ********");
    
	ProjectMainViewCantroller *projectbtn=[[ProjectMainViewCantroller alloc]initWithNibName: @"ProjectMainViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:projectbtn animated:YES];
}


#pragma mark -
#pragma mark Tableview  Methods
#pragma mark -



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //[searchBarViewController resignFirstResponder];
    [self.view endEditing:YES];
    [self showSearchOptPopover:NO];
    searchBarViewController.view.alpha = 0;
    
   projDataObj = [ProjectDataObjArray objectAtIndex:indexPath.row];
     NSLog(@"PROJ Name ->%@",[projDataObj projectname]);
    NSLog(@"PROJ ID ->%d",[projDataObj projectid]);
    
    
    if(IS_IPAD)
    {
        
        //NSLog(@"im here in Project List");
        
        app_delegate  =(OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
        
        UISplitViewController *splitVC = [[UISplitViewController alloc]init];
        splitVC = app_delegate.splitVCListToDo;
        
        
        NSLog(@"Project id is = = %ld",(long)[projDataObj projectid]);
        
        UINavigationController *nav=[[UINavigationController alloc]init];
        ListToDoViewController *ptr_ListToDoViewController = [[ListToDoViewController alloc]initWithProjectID:[projDataObj projectid] ProjectName:[projDataObj projectname]];
        
         
        
        nav.viewControllers=@[ptr_ListToDoViewController];
        [[splitVC.viewControllers objectAtIndex:0] removeFromParentViewController];
        splitVC.viewControllers=@[[splitVC.viewControllers objectAtIndex:0], nav];
        
        //Steve - remembers the selected row
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        [sharedDefaults setInteger:projDataObj.projectid forKey:@"ChecklistSelectedRow_iPad"]; //stores project ID to hihglight
        
        [tableView reloadData];//Steve

    }
    else  //iPhone
    {
        
        ListToDoViewController *ptr_ListToDoViewController = [[ListToDoViewController alloc]initWithProjectID:[projDataObj projectid] ProjectName:[projDataObj projectname]];
        [self.navigationController pushViewController:ptr_ListToDoViewController animated:YES];
    }
}



/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
 {
 return NORMAL_CELL_FINISHING_HEIGHT;
 }
 */


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float height;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){ 
		height = TABLE_ROW_HEIGHT_IPHONE; //Steve was TABLE_ROW_HEIGHT_IPAD
    }else {
		height = TABLE_ROW_HEIGHT_IPHONE;
	}
    
    return height;
	
}


- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section 
{  
	if([ProjectDataObjArray count]>0)
        return [ProjectDataObjArray count];
    else
        
        return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSInteger row;
    row = [indexPath row];
    
    //Anil's Added
    
    if([ProjectDataObjArray count]==0)
    {
        [ProjectDataObjArray addObject:ADDING_CELL];
    }
    
    
    projDataObj = [ProjectDataObjArray objectAtIndex:row];
    //NSLog(@"cellForRowAtIndexPath projDataObj.projectid = %d", projDataObj.projectid);
    
    UIColor *backgroundColor = [UIColor whiteColor];
    
    //*****************************************************************************
    //Adds a Row on Top
    if ([projDataObj isEqual: ADDING_CELL]) 
    {
        //NSLog(@"[projDataObj isEqual: ADDING_CELL");
        NSString *cellIdentifier = nil;
        TransformableTableViewCell *cell = nil;
        
        // IndexPath.row == 0 is the case we wanted to pick the pullDown style
        if (indexPath.row == 0) {
            //  NSLog(@"indexPath.row == 0");
            cellIdentifier = @"PullDownTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            
            if (cell == nil) 
            {
                cell = [TransformableTableViewCell transformableTableViewCellWithStyle:TransformableTableViewCellStylePullDown
                                                                       reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                cell.textLabel.textColor = myFontColor;
            }
            
            // Setup tint color
            cell.tintColor = backgroundColor;
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT) {
                cell.textLabel.font =   myFontName;
                cell.textLabel.textColor = [UIColor grayColor];//Steve
                cell.tintColor = myCellBackgroundColor; //Steve added
                cell.textLabel.text = @"  Release to create cell...";
                
            } else {
                
                
               // NSLog(@"Frame %@",[cell.textLabel description]);
                if([ProjectDataObjArray count]==1)
                {
                    cell.textLabel.font =   myFontName;
                    // cell.textLabel.text = PLACEHOLDER;
                    //cell.textLabel.textAlignment = UITextAlignmentCenter;
                    cell.textLabel.textAlignment = NSTextAlignmentCenter;
                    [cell.contentView.layer setSublayerTransform:CATransform3DIdentity];
                    
                }
                else
                    
                cell.textLabel.text = @"  Continue Pulling...";
                cell.textLabel.textColor = [UIColor grayColor];//Steve
                cell.tintColor = myCellBackgroundColor; //Steve added
                
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.detailTextLabel.text = @" ";
            return cell;
        } 
        //****************************************************************
        // For adding a cell by pinching in the middle of two cells
        else 
        {
            //NSLog(@"UnfoldingTableViewCell");
            
            // Otherwise is the case we wanted to pick the pullDown style
            cellIdentifier = @"UnfoldingTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) 
            {
                cell = [TransformableTableViewCell transformableTableViewCellWithStyle:TransformableTableViewCellStyleUnfolding
                                                                       reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                //Steve changed was: cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font =   myFontName;
                cell.textLabel.textColor = myFontColor;
                // cell.textLabel.textAlignment = UITextAlignmentCenter;
            }
            
            // Setup tint color
            cell.tintColor = backgroundColor;
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT) {
                cell.textLabel.text = @"  Release to create cell...";
                
                cell.tintColor = myCellBackgroundColor; //Steve
            } else {
               // NSLog(@"Frame %@",[cell.textLabel description]);
                
                
                if([ProjectDataObjArray count]==2)
                {
                    cell.textLabel.font =   myFontName;
                    cell.textLabel.text = PLACEHOLDER;
                    cell.textLabel.textAlignment = NSTextAlignmentCenter;
                    [cell.contentView.layer setSublayerTransform:CATransform3DIdentity];
                }
                
                else
                {
                    cell.textLabel.text = @"  Continue Pinching...";

                }
                

            }
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.detailTextLabel.text = @" ";
            return cell;
        }
        
        /*****************************************************************************/
        //Not Adding Cell???
    } else {
        
        static NSString *cellIdentifier = @"MyCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = myFontColor;
        }
        
        
        
        //Done cell
        if ([projDataObj isEqual:DONE_CELL]) 
        {
            //NSLog(@" projDataObj isEqual:DONE_CELL");
            
            cell.textLabel.textColor = [UIColor grayColor];
            cell.contentView.backgroundColor = [UIColor whiteColor];
        } 
        //DUMMY_CELL is used when LONG holding a cell and then moving it.  This dummy cell is the background cell
        //Therefore, all cell views should Not show
        if ([projDataObj isEqual:DUMMY_CELL])
        {
           // NSLog(@" projDataObj isEqual:DUMMY_CELL");
            
            cell.textLabel.text = @"";
            cell.contentView.backgroundColor = myBackgroundColor;
            //cell.contentView.backgroundColor = [UIColor redColor]; //Steve Test
            
            //Clears all subviews so DUMMY_CELL is Not visible
            for (UIView  *subViews in [cell.contentView subviews]) 
            {
                [subViews removeFromSuperview];
            }
            

            
            
        } 
        else 
            //*************************************************************************
            //Normal Cell State
            //*************************************************************************
        {
            
            cell.contentView.backgroundColor = backgroundColor;
            // cell.textLabel.font = myFontName;
            
            
            //Clears all subviews so when checked (progress = 100% complete) they don't overlap textlabels, buttons, etc
            for (UIView  *subViews in [cell.contentView subviews]) 
            {
                [subViews removeFromSuperview];
            }
            
            
            //Steve - for iPad - shows the selected cell
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            int selectedRow = [sharedDefaults integerForKey:@"ChecklistSelectedRow_iPad"];
            
            
            //makes button size of cell so I can add borders to cell while keeping blank cells all black
            //otherwise blank cells will be color of the cell line separator
            if (IS_IPAD) { //Steve
                
                UIButton *cellButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, TABLE_ROW_HEIGHT_IPHONE)]; //(0, 0, 245, TABLE_ROW_HEIGHT_IPHONE)
                //added so cell will recognize touch instead of Button and didSelectRowAtIndexPath method will be invoked for Edit
                [cellButton setUserInteractionEnabled:NO];
                
                cellButton.layer.borderWidth = 1;
                cellButton.layer.borderColor = [[UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]CGColor];
                cellButton.layer.backgroundColor = [myCellBackgroundColor CGColor];
                [cell.contentView addSubview:cellButton];
                
                
                //Steve - Changes selected cell to different color to highlight it
                if (projDataObj.projectid == selectedRow) {
                    
                    //changes color so it looks higlighted
                    cellButton.layer.backgroundColor = [[UIColor colorWithRed:153.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:0.35f]CGColor];
                    
                    //tells split controller which ListToDoVC to show
//                    app_delegate  =(OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
//                    UISplitViewController *splitVC = [[UISplitViewController alloc]init];
//                    splitVC = app_delegate.splitVCListToDo;
//                    
//                    UINavigationController *nav=[[UINavigationController alloc]init];
//                    ListToDoViewController *ptr_ListToDoViewController = [[ListToDoViewController alloc]initWithProjectID:[projDataObj projectid] ProjectName:[projDataObj projectname]];
//                    
//                    nav.viewControllers=@[ptr_ListToDoViewController];
//                    [[splitVC.viewControllers objectAtIndex:0] removeFromParentViewController];
//                    splitVC.viewControllers=@[[splitVC.viewControllers objectAtIndex:0], nav];
                }
                

            }
            else{ //iPhone
                
                UIButton *cellButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 245, TABLE_ROW_HEIGHT_IPHONE)]; //(0, 0, 250, TABLE_ROW_HEIGHT_IPHONE)
                //added so cell will recognize touch instead of Button and didSelectRowAtIndexPath method will be invoked for Edit
                [cellButton setUserInteractionEnabled:NO];
                
                cellButton.layer.borderWidth = 1;
                cellButton.layer.borderColor = [[UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]CGColor];
                cellButton.layer.backgroundColor = [myCellBackgroundColor CGColor];
                
                [cell.contentView addSubview:cellButton];
            }

            
            
            /////////// Steve added ///////////////////
            //Puts border and sets color around Badge numbers
            UIButton *badgeButton = [[UIButton alloc] initWithFrame:CGRectMake(245, 0, 75, TABLE_ROW_HEIGHT_IPHONE)]; //(250, 0, 70, TABLE_ROW_HEIGHT_IPHONE)
            
            //added so cell will recognize touch instead of Button and didSelectRowAtIndexPath method will be invoked for Edit
            [badgeButton setUserInteractionEnabled:NO];
            
            badgeButton.layer.borderWidth = 1;
            badgeButton.layer.borderColor = [[UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]CGColor];
            badgeButton.layer.backgroundColor = [[UIColor whiteColor] CGColor];
            
            [cell.contentView addSubview:badgeButton];
            
            
            //Steve - Makes badgebutton clear so CellButton color shows through when highlighted
            if (projDataObj.projectid == selectedRow) {
                badgeButton.layer.backgroundColor = [[UIColor clearColor]CGColor];
            }
            
            //Clears the cell text
            cell.textLabel.text = nil;
            
            UILabel *titlelabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, TABLE_ROW_HEIGHT_IPHONE/2 - 30/2,[[projDataObj projectname] length]*12, 30)];
            
            
            [titlelabel setBackgroundColor:[UIColor clearColor]];
            [titlelabel setFont:myFontName];
            [titlelabel setTextColor:myFontColor];
            [titlelabel setText:[projDataObj projectname]];
            
            //Steve added - Changes color of "No Project Assigned" text
           // NSLog(@"titlelabel.text = %@", titlelabel.text);
            if ([titlelabel.text isEqualToString:@"No Project Assigned"]) {
                //NSLog(@"************");
                titlelabel.textColor = [UIColor colorWithRed:147.0f/255.0f green:12.0f/255.0f blue:16.0f/255.0f alpha:1.0f];
            }
            
            [cell.contentView addSubview:titlelabel];
            
            //Calculate the expected size based on the font and linebreak mode of your label
            CGSize maximumLabelSize = CGSizeMake(230 - 5,30);
            CGSize expectedLabelSize = [[projDataObj projectname] sizeWithFont:myFontName 
                                                                  constrainedToSize:maximumLabelSize
                                                                  lineBreakMode:titlelabel.lineBreakMode]; 
            //adjust the label the the new Width.
            CGRect newFrame = titlelabel.frame;
            newFrame.size.width = expectedLabelSize.width;
            titlelabel.frame = newFrame;
            
            
            //Invisible button for labelBtn_Clicked
            UIButton *invisibleIndBtn = [[UIButton alloc] initWithFrame:titlelabel.frame];
            [invisibleIndBtn setTag:indexPath.row];
            [invisibleIndBtn addTarget:self action:@selector(labelBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:invisibleIndBtn];
            
            
            UILabel *viewmB=[[UILabel alloc]init];
            viewmB.text =[NSString stringWithFormat:@"%d~%@",projDataObj.projectid ,projDataObj.projectname];
            viewmB.tag =666;
            [viewmB setBackgroundColor:[UIColor greenColor]];
            [invisibleIndBtn addSubview:viewmB];
            
            
            UILabel *badgelabel = [[UILabel alloc] initWithFrame:CGRectMake(tableView.frame.size.width-43, TABLE_ROW_HEIGHT_IPHONE/2 - 30/2,50, 30)];//50,30
            [badgelabel setBackgroundColor:[UIColor clearColor]];
            [badgelabel setFont:myFontName];
            [badgelabel setTextColor:myBadgeColor];
            [badgelabel setText:[NSString stringWithFormat:@"%d", [self findTODOTotal:[projDataObj projectid]]]]; 
            [cell.contentView addSubview:badgelabel];
            
            
            
        }
        //*************************************************************************
        //Normal Cell State
        //*************************************************************************
        
        cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero]; // Anil's Addition
        cell.backgroundView.backgroundColor = myCellBackgroundColor;  //Steve modified
        
        //cell.backgroundColor = [UIColor redColor]; //Steve Test
        
        
        //NSLog(@"cell  = %@", cell);
        //NSLog(@"ProjectDataObjArray count = %d", ProjectDataObjArray.count);
        //NSLog(@"indexPath.row  = %d", indexPath.row);
        
        
        
        return cell;
    }
    
}



- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView  editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[ProjectDataObjArray objectAtIndex:indexPath.row]isKindOfClass:[NSString class]])
    {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}
//Alok Added For Stoping Editing on Firstrow
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return NO;
    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        //NSLog(@"commitEditingStyle");
        
        if ([ProjectDataObjArray count] == 1)
        {
            [tableView reloadData];  
            return;
        }
        projDataObj = [ProjectDataObjArray objectAtIndex:indexPath.row];
        
        if([self findTODOTotal:[projDataObj projectid]]>=1){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Project" message:@"Are you sure you want to Delete this Project Folder?  If you delete it, all the associated Todo's and Notes will be deleted as well."  delegate:self cancelButtonTitle:@"YES" otherButtonTitles: @"NO", nil]; 
            [alert show];
            [alert setTag:indexPath.row];
            return;
        }
        
        
		BOOL isDeleted = [AllInsertDataMethods deleteProject:[projDataObj projectid]];
        
        if (isDeleted == YES) 
        {
            //NSLog(@"Delete cell");
            
            [ProjectDataObjArray removeObjectAtIndex:indexPath.row];
            
            if ([ProjectDataObjArray count] > 0)
            {
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
            
		}
        
        
        
	} 
	else if (editingStyle == UITableViewCellEditingStyleInsert) 
	{
		
	}   
    if ([ProjectDataObjArray count] == 0)
    {
        [tableView reloadData];   
    }
	return;
}



#pragma mark -
#pragma mark Database Transaction 
#pragma mark -
-(void)getProject;
{
	
    NSLog(@"Get Project Function Call");
    
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil;
    NSString *Sql =@"";
	[ProjectDataObjArray removeAllObjects];
	
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
        Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,projectPriority,folderImage, modifyDate from ProjectMaster where level = %d ORDER BY modifyDate DESC",0];
       // NSLog(@" query %@",Sql);
        
        //SELECT * FROM table_name WHERE columnname LIKE value%
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
            projectdataobj = [[AddProjectDataObject alloc]init];
            [projectdataobj setProjectid:0];
            [projectdataobj setProjectname:@"No Project Assigned"];
            [projectdataobj setProjectpriority:@""];
            [projectdataobj setProjectIMGname:@"FolderColor1.png"];
            [projectdataobj setModifyDate:@"July 01, 1970 11:14:52 AM"];
            [ProjectDataObjArray addObject:projectdataobj];
			
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                projectdataobj = [[AddProjectDataObject alloc]init];
				
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				
				
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                
				[projectdataobj setProjectpriority: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
				
                
                [projectdataobj setProjectIMGname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
                [projectdataobj setModifyDate:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
                
				[ProjectDataObjArray addObject:projectdataobj];
                
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
	
}

-(NSInteger)findTODOTotal:(NSInteger)valueTODO
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	NSString *Sql =@"";
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
        Sql = [[NSString alloc] initWithFormat:@"SELECT COUNT(TODOID)  from ToDoTable WHERE PerntID=%d and progress != 100",valueTODO];
		
		//NSLog(@"%@",Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
                totalTODO = sqlite3_column_int(selectBadgeStmt, 0);
				
            }
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
	}
	selectBadgeStmt = nil;
	
	return totalTODO;
	
}



#pragma mark -
#pragma mark JTTableViewGestureAddingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsAddRowAtIndexPath:(NSIndexPath *)indexPath {
    
   //NSLog(@"gestureRecognizer needsAddRowAtIndexPath");
   // NSLog(@"Adding row at indexPath.row = %d", indexPath.row);
    
    
    [ProjectDataObjArray insertObject:ADDING_CELL atIndex:indexPath.row];
    
    
    // NSLog(@"Starting method %@",ProjectDataObjArray);
    
}

/****************************************************/
//Cell Data????

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCommitRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //Cell Data?
    NSString *selectStat=@"select count(projectID) from ProjectMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    if(CountRecords >= MAX_LIMIT_PROJECT && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Project Folder Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d project folders.",MAX_LIMIT_PROJECT] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert setTag:501];
        [alert show];
        return;
    }
    
 
    
    
    indexWhereCellIsBeingInserted = indexPath.row;
    
    [self performSelector:@selector(createAddView) withObject:nil afterDelay:0.001];
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 501)
    {
        if(buttonIndex==1)
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];
        NSString *selectStat=@"select count(TODOID) from ToDoTable";
        int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
        if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
        {
            if([ProjectDataObjArray containsObject:ADDING_CELL])
            {
                [ProjectDataObjArray removeObject:ADDING_CELL];
            }
            
            [toDoTableView reloadData];
            [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
            [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
            [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
            [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
            isKeyBoardDone = YES;
            return;
        }
        
    }
    else if(alertView.tag == 502)
    {
        //This is for search alert view clicked
    }
    else{
        int row = alertView.tag;
        if(buttonIndex == 0){
            projDataObj = [ProjectDataObjArray objectAtIndex:row]; 
            //  BOOL isDeleted = [AllInsertDataMethods deleteProject:[projDataObj projectid]];
            BOOL isDeleted =YES;
            [self delete_Project:[projDataObj projectid]];
            [getBIDArray addObject:[NSString stringWithFormat:@"%d",[projDataObj projectid]]];
            [self dum:getBIDArray];
            if (isDeleted == YES) 
            {
                [ProjectDataObjArray removeObjectAtIndex:row];
                [toDoTableView reloadData];
            }
            
            
            
        }
    }
}
///ALOK added to delete all Projects///
-(void)delete_Project:(NSInteger)fval
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteproject= nil;
	if(deleteproject ==nil)
	{   sql = [[NSString alloc]initWithFormat:@"select projectID from ProjectMaster where belongstoId = %d ",fval];
		
        
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
		{
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		{
			
			while(sqlite3_step(deleteproject) == SQLITE_ROW) 
			{
				NSInteger Bval=sqlite3_column_int(deleteproject, 0);
				//NSLog(@"%d",Bval);
				[getBIDArray addObject:[NSString stringWithFormat:@"%d",Bval]];
				[self delete_Project:Bval];
			}
			
			sqlite3_finalize(deleteproject);		
			deleteproject = nil;        
		}
	}
}

-(void)dum:(NSMutableArray *)faa;
{
	
	//[faa addObject:[NSString stringWithFormat:@"%d",Fval]];
	
	//NSLog(@"%@",faa);
	
	
	for(int mm=0;mm<[faa count];mm++)
	{
		[self update_todo:[[faa objectAtIndex:mm]intValue]];
        [self delete_notes:[[faa objectAtIndex:mm]intValue]];
		[self todo_Find:[[faa objectAtIndex:mm]intValue]];
	}
	
	
}
-(void)todo_Find:(NSInteger)bValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteproject= nil;
	
	if(deleteproject == nil) 
	{
		sql = [[NSString alloc]initWithFormat:@"delete from ProjectMaster where projectID = %d",bValue];
		
		//NSLog(@"%@",sql);
		
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	
	if (SQLITE_DONE != sqlite3_step(deleteproject)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteproject);
		
	} else {
		sqlite3_finalize(deleteproject);
		
	}
	
}


-(void)delete_notes:(NSInteger)CValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		//sql = [[NSString alloc]initWithFormat:@"Update TODOTable set BelongstoProjectID=%d where BelongstoProjectID=%d",0,CValue];
        
        sql = [[NSString alloc]initWithFormat:@"delete from NotesMaster where BelongstoProjectID=%d",CValue];
        
		//NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
    if(SQLITE_DONE != sqlite3_step(insertProjectData))
    {
        NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    } 
    else 
    {
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    }	
}



-(void)update_todo:(NSInteger)CValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		//sql = [[NSString alloc]initWithFormat:@"Update TODOTable set BelongstoProjectID=%d where BelongstoProjectID=%d",0,CValue];
        
        sql = [[NSString alloc]initWithFormat:@"delete from TODOTable where BelongstoProjectID=%d",CValue];
        
		//NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
    if(SQLITE_DONE != sqlite3_step(insertProjectData))
    {
        NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    } 
    else 
    {
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    }	
}
///ALOK added to delete all Projects///




-(void)createAddView
{
    //Cell Data?
    
    
   // NSLog(@"gestureRecognizer needsCommitRowAtIndexPath");
    //  NSLog(@"commiting row at indexPath.row = %d", indexPath.row);
    
    
    //  indexWhereCellIsBeingInserted = indexPath.row;
    
    //***************************************** 
    //set up Input view and fields
    //*****************************************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //new view and input textfield to enter new todo
    UIView *inputView ;
    if ([sharedDefaults boolForKey:@"NavigationNew"])
    {
        inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42 - 42, 320, NORMAL_CELL_FINISHING_HEIGHT)];
    }
    else
        inputView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 42, 320, NORMAL_CELL_FINISHING_HEIGHT)];
        
    inputView.backgroundColor = [UIColor whiteColor];//Steve commented
    inputView.backgroundColor = myCellBackgroundColor; //Steve
    [self.view addSubview:inputView];
    
    [inputView setTag:INPUT_VIEW_TAG];
    
    
    UITextField *inputTextField;
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        
        if (IS_IOS_7) {
            
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,0,320,NORMAL_CELL_FINISHING_HEIGHT)];
        }
        else{
            
            inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10 - 42,320,NORMAL_CELL_FINISHING_HEIGHT)];
        }
    else //Old Nav
        inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10,42 + 10,320,NORMAL_CELL_FINISHING_HEIGHT)];
    
    
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.font = myFontName;
    inputTextField.textColor = myFontColor;
    [self.view addSubview:inputTextField];
    
    [inputTextField setTag:TEXT_FIELD_TAG];
    [inputTextField setReturnKeyType:UIReturnKeyDone];
    [inputTextField becomeFirstResponder];
    [inputTextField addTarget:self action:@selector(keyBoardDone:)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    //black opaque space between input box and keyboard
    UIView *blackBox;
    if ([sharedDefaults boolForKey:@"NavigationNew"])
    {
        if(IS_IPAD)
        {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 900)];//Steve was 620
        }
        else
        {
            blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 254)];
        }
            NSLog(@"blackbox -- %@",NSStringFromCGRect(blackBox.frame));
    }
    else
        blackBox = [[UIView alloc] initWithFrame: CGRectMake ( 0, NORMAL_CELL_FINISHING_HEIGHT + 38, 320, 254)];    
    
    
    //UIKeyboardFrameEndUserInfoKey
    blackBox.backgroundColor = [UIColor blackColor];
    blackBox.alpha = 0.00;  //Steve changed was: 0.90
    blackBox.tag = BLACK_BOX_TAG;
    [self.view addSubview:blackBox];
    [UIView animateWithDuration:0.35 animations:^{blackBox.alpha = 0.90f;}];  //Steve added to animate blackBox
    
    
    UIButton *blackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IS_IPAD) {
          [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 900)];
    }
    else{ //iPhone
          [blackButton setFrame:CGRectMake( 0, NORMAL_CELL_FINISHING_HEIGHT + 38 - 44, 320, 254)];
    }
    
    
    ////Steve shows new selected cell that was created
    if (IS_IPAD) {
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        [sharedDefaults setInteger:projDataObj.projectid forKey:@"ChecklistSelectedRow_iPad"]; //highlights new cell
    }
    

    
    [blackButton addTarget:nil action:@selector(saveToDoToDB:) forControlEvents:UIControlEventTouchUpInside];
    [blackButton setTag:BLACK_BUTTON_TAG];
    [self.view addSubview:blackButton];
    
    
    
    //NSLog(@"isKeyBoardDone = %d", isKeyBoardDone);
    
    //Keyboard is done?
    if (isKeyBoardDone) {
       // NSLog(@"isKeyBoardDone *******");
    }
    
    
    
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsDiscardRowAtIndexPath:(NSIndexPath *)indexPath 
{
    [ProjectDataObjArray removeObjectAtIndex:[indexPath row]];
}

- (BOOL)textFieldShouldReturn:(UITextField *) inputTextField
{
    //NSLog(@" textFieldShouldReturn method");
    [inputTextField resignFirstResponder];
    isKeyBoardDone = YES;
    return YES;
}
-(void)saveToDoToDB:(id)sender
{
    [self keyBoardDone:[self.view viewWithTag:TEXT_FIELD_TAG]];   
}

- (void) keyBoardDone:(id)sender
{
    //NSLog(@"keyBaordDone Method");
    
    //Steve changed so icons will appear white again
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
 
    
    UITextField *_text = (UITextField *)sender;
    //NSLog(@"the text %@",_text.text);
    
    
    if(!isEditingProj)
    {
        
        
        [ProjectDataObjArray removeObjectAtIndex:indexWhereCellIsBeingInserted];
        
        
        
        //Alok removed from if statement 
        //
        if([_text.text length]>0 && indexWhereCellIsBeingInserted == 0 )
        {
           // NSLog(@"indexWhereCellIsBeingInserted == 0 ");
            // Save to DB
            
            //***************************************** 
            //Inserts new Data (user typed into Database
            //Also, if black image was clicked
            //***************************************** 
            
            AddProjectDataObject *addToDoObj = [[AddProjectDataObject alloc] init];
            
            //Add new Todo from inputTextField
            
            //[addToDoObj setTodoTitle:inputTextField.text];
            [addToDoObj setProjectname:_text.text];
           // NSLog(@"ProjectName = %@", addToDoObj.projectname);
            
            
            
            [AllInsertDataMethods addProjectOnly:addToDoObj];
            
            //***************************************** 
            //END  - Inserts new Data (user typed into Database
            //***************************************** 
            
            [self getProject];    
            [toDoTableView reloadData];
            
            
        }
        else if([_text.text length]>0 && indexWhereCellIsBeingInserted != 0 )
        {
           // NSLog(@"indexWhereCellIsBeingInserted-->%d ",indexWhereCellIsBeingInserted);
            // Save to DB
            
            
            
            AddProjectDataObject *addToDoObj = [[AddProjectDataObject alloc] init];
            
            //***********************************************************
            //***********************************************************
            //Add new Todo from inputTextField
            
            [addToDoObj setProjectname:_text.text];
            //NSLog(@"Project NAME = %@", addToDoObj.projectname);
            //***********************************************************
            //***********************************************************
            [AllInsertDataMethods addProjectOnly:addToDoObj];
            
            
            //***************************************** 
            //END  - Inserts new Data (user typed into Database
            //***************************************** 
            
            [self getProject];    
            
            
            id itemAt0index = [ProjectDataObjArray objectAtIndex:1];
            
            [ProjectDataObjArray removeObjectAtIndex:1];
            
            
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            
            BOOL _added = NO;
            
            for (int k=0;k<[ProjectDataObjArray count]+1; k++) 
                
            {
                if(k!=indexWhereCellIsBeingInserted)
                {
                    if(!_added)
                        [tempArray addObject:[ProjectDataObjArray objectAtIndex:k]];
                    else
                    {
                        [tempArray addObject:[ProjectDataObjArray objectAtIndex:k-1]];
                        
                    }
                }
                else
                {
                    [tempArray addObject:itemAt0index];
                    _added = YES;
                }
            }
            
            [ProjectDataObjArray removeAllObjects];
            ProjectDataObjArray = tempArray;
            
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            
            [dateFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
            
            NSDate *_date = [NSDate date];
            
            for (int _index =[ProjectDataObjArray count]-1;_index >=0;_index--) 
            {
                // TODODataObject *item = [ProjectDataObjArray objectAtIndex:_index];
                AddProjectDataObject *item = [ProjectDataObjArray objectAtIndex:_index];
               // NSLog(@"PNAEM-->%@",item.projectname);
                _date = [_date dateByAddingTimeInterval:1];
                
                [item setModifyDate:[dateFormatter1 stringFromDate:_date]];
                
                BOOL isInserted = [AllInsertDataMethods updateProjectTitleOnly:item];
                //Alok's Changes for updates Todo Titles only
                //updateToDoDataValues:item];
               // NSLog(@"%d",isInserted);
            }
            
            dateFormatter1 = nil;
            [self getProject];
            
            [toDoTableView reloadData];
            
            
        }        
        else
        {
            //Do Nothing;
            
           // NSLog(@"%@",ProjectDataObjArray);
            [toDoTableView reloadData];
        }
        
    }
    else 
    {
        isEditingProj = NO;
        //// Update the Project
        
        AddProjectDataObject *editItem = [ProjectDataObjArray objectAtIndex:indexWhereCellIsBeingInserted];
        
        
        //NSLog(@"Todo title is %@",editItem.projectname);
       // NSLog(@"Todo id is %d",editItem.projectid);
        
        
        if([_text.text length]>0 && ![_text.text isEqualToString:editItem.projectname])
        {
            editItem.projectname = _text.text;
            
            BOOL isInserted = [AllInsertDataMethods 
                               updateProjectTitleOnly:editItem];
            //Alok's Changes for updates Todo Titles only
            //updateToDoDataValues:editItem];
            //NSLog(@"%d",isInserted);
            [self getProject];
            
            [toDoTableView reloadData];
        }  
        
        
    }
    
    [[self.view viewWithTag:BLACK_BOX_TAG] removeFromSuperview];
    [[self.view viewWithTag:TEXT_FIELD_TAG] removeFromSuperview];
    [[self.view viewWithTag:INPUT_VIEW_TAG] removeFromSuperview];
    [[self.view viewWithTag:BLACK_BUTTON_TAG] removeFromSuperview];
    
    
    
    isKeyBoardDone = YES;
    //[self gestureRecognizer:(JTTableViewGestureRecognizer *) canEditRowAtIndexPath:(NSIndexPath *) ]
    
    
}
-(void) keyboardDidShow: (NSNotification *)notif 
{
    //NSLog(@"%s",__FUNCTION__);
    
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    // If keyboard is visible, return
    if (isKeyBoardDone) 
    {
        //NSLog(@"Keyboard is already visible. Ignoring notification.");
        return;
    }
    
    // Keyboard is now visible
    
    isKeyBoardDone = NO;
}

-(void) keyboardDidHide: (NSNotification *)notif 
{
    // Is the keyboard already shown
    if (!isKeyBoardDone) 
    {
        //NSLog(@"Keyboard is already hidden. Ignoring notification.");
        return;
    }
    
    
    // Keyboard is no longer visible
    isKeyBoardDone = YES;	
}

-(void) keyboardWillShow: (NSNotification *)notif 
{
    
}

#pragma mark JTTableViewGestureEditingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer didEnterEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [toDoTableView cellForRowAtIndexPath:indexPath];
    
    UIColor *backgroundColor = nil;
    switch (state) {
        case JTTableViewCellEditingStateMiddle:
            backgroundColor = [UIColor redColor];
            break;
        case JTTableViewCellEditingStateRight:
            backgroundColor = [UIColor greenColor];
            break;
        default:
            backgroundColor = [UIColor lightGrayColor];
            break;
    }
    
    cell.contentView.backgroundColor = backgroundColor;
    
    //Steve added
    //When swiping cell Left or Right
    //changes cell color back to backgroundColor (white), otherwise cell will stay red until another ToDo is added
    if (cell.contentView.backgroundColor == [UIColor redColor])
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelay:0.5];
        [UIView setAnimationDuration:1.0];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        [UIView commitAnimations];
    }
    
    
    if ([cell isKindOfClass:[TransformableTableViewCell class]]) {
        ((TransformableTableViewCell *)cell).tintColor = backgroundColor;
    }
}

// This is needed to be implemented to let our delegate choose whether the panning gesture should work
- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;  
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer commitEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableView *tableView = gestureRecognizer.tableView;
    [tableView beginUpdates];
    if (state == JTTableViewCellEditingStateLeft) {
        // An example to discard the cell at JTTableViewCellEditingStateLeft
        //Steve changed was: [self.rows removeObjectAtIndex:indexPath.row];
        [ProjectDataObjArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } else if (state == JTTableViewCellEditingStateRight) {
        // An example to retain the cell at commiting at JTTableViewCellEditingStateRight
        [ProjectDataObjArray replaceObjectAtIndex:indexPath.row withObject:DONE_CELL];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } else {
        // JTTableViewCellEditingStateMiddle shouldn't really happen in
        // - [JTTableViewGestureDelegate gestureRecognizer:commitEditingState:forRowAtIndexPath:]
    }
    [tableView endUpdates];
    
    // Row color needs update after datasource changes, reload it.
    [tableView performSelector:@selector(reloadVisibleRowsExceptIndexPath:) withObject:indexPath afterDelay:JTTableViewRowAnimationDuration];
}

#pragma mark JTTableViewGestureMoveRowDelegate

- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)return NO;
    
    return YES;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCreatePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.grabbedObject = [ProjectDataObjArray objectAtIndex:indexPath.row];
    //  if(indexPath.row!=0)
    [ProjectDataObjArray replaceObjectAtIndex:indexPath.row withObject:DUMMY_CELL];
    //else   return;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsMoveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    id object = [ProjectDataObjArray objectAtIndex:sourceIndexPath.row];
    [ProjectDataObjArray removeObjectAtIndex:sourceIndexPath.row];
    [ProjectDataObjArray insertObject:object atIndex:destinationIndexPath.row];
    
}


- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsReplacePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [ProjectDataObjArray replaceObjectAtIndex:indexPath.row withObject:self.grabbedObject];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    
    NSDate *_date = [NSDate date];//[dateFormatter dateFromString:@"Aug 05, 2012 11:14:52 AM"];
    
    // NSDate *_date = [dateFormatter dateFromString:[[ProjectDataObjArray objectAtIndex:1] modifyDate]];
    
    
    
    for (int _index =[ProjectDataObjArray count]-1;_index >=1;_index--) 
    {
         AddProjectDataObject *item = [ProjectDataObjArray objectAtIndex:_index];
       // NSLog(@"item.projectname%@",item.projectname);
        _date = [_date dateByAddingTimeInterval:1];
        
        
        [item setModifyDate:[dateFormatter stringFromDate:_date]];
        BOOL isInserted = [AllInsertDataMethods updateProjectTitleOnly:item];
        //Alok's Changes for updates Todo Titles only
        //updateToDoDataValues:item];
        
    }
    
    [self getProject];

    
    [toDoTableView reloadData];
    self.grabbedObject = nil;
}

/*-(NSDate *)convertStringToDate:(NSString *)str_Date
 {
 
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 
 NSDate *resultedDate;
 
 [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
 
 resultedDate = [dateFormatter dateFromString:str_Date];
 dateFormatter = nil;
 
 return resultedDate;
 
 }*/





#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    upgradeView = nil;
    upgradeScrollView = nil;
    navBarUpgradeView = nil;
    ListHelpDragView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)callSelectorForSwipeUpAction
{
    return;
}

-(void)callSelectorForSwipeDownAction
{    
    return;
}




// Added by anil //
// This method gets called when text field for entering to do appear and user click on black area between text field and keyboard

-(void)saveProjectToDB:(id)sender
{
    [self keyBoardDone:[self.view viewWithTag:TEXT_FIELD_TAG]];   
}


- (IBAction)notesButtonClicked:(id)sender {
    NotesViewCantroller *notesViewCantroller =[[NotesViewCantroller alloc]initWithNibName:@"NotesViewCantroller" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:notesViewCantroller animated:YES];
    
}

#pragma mark -
#pragma mark Battom bar Item implementation
#pragma mark -
- (IBAction)searchBarButton_Clicked:(id)sender {
    //NSLog(@"searchBarButton_Clicked");
    
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    
    projDataObj = [ProjectDataObjArray objectAtIndex:0];
    //NSLog(@"projDataObj DESC->%@",projDataObj.projectname);
    if([ProjectDataObjArray count]==1 && [projDataObj.projectname isEqual: @"No Project Assigned"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry! There is no Projects to Search." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert setTag:502];
        return;
        
    }
    
     //[self showInputOptPopover:NO];
    
    if (searchBarViewController) {
        if ([searchBarViewController.view alpha] == 0) {
            [self showSearchOptPopover:YES];
        }else {
            [self showSearchOptPopover:NO];
        }
    }else {
        [self showSearchOptPopover:YES];
    }
}



-(void)showSearchOptPopover:(BOOL) show {
   // NSLog(@"showSearchOptPopover");
    
	if (show) {
		if (searchBarViewController) {
			searchBarViewController = nil;
			
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
            
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
			
		}else {
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
            
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
		}
	} else {
		if (searchBarViewController) {
			if ([searchBarViewController.view alpha] == 1) {
				[searchBarViewController.view setAlpha:0];
			}else {
				[searchBarViewController.view removeFromSuperview];
				searchBarViewController = nil;
			}
		}	
	}
	[self.view bringSubviewToFront:searchBarViewController.view];
}


- (IBAction)mailBarButton_Clicked:(id)sender {
	if ([MFMailComposeViewController canSendMail]) 
	{
        
        if (IS_IOS_7) {
            
            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
        }
        else{
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
         //[self presentViewController:mailCntrlr animated:YES completion:nil];

        
        
		MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
		[mailCntrlr setMailComposeDelegate:self];
        
        if (IS_LIST_VERSION) {
            [mailCntrlr setSubject:@"InFocus Checklist"];
        }
        else{
            [mailCntrlr setSubject:@"InFocus PRO"];
        }
		
		NSString *body = @"<table border=\"1\" style=\"background-color:white;\"><tr style=\"font-weight:bold;\"><th>Project Name</th><th>Number of to-do's</th></tr>\n ";
		
        
        
		for (int row = 0; row < [ProjectDataObjArray count]; row++) {
            
            
			AddProjectDataObject *tempDataObj = [ProjectDataObjArray objectAtIndex:row];
            
            if(![tempDataObj isEqual:ADDING_CELL])
            {
                //Steve added - % symbol causes an email bug in the title string, so it needs to be changed to %%
                NSString *titleString = [tempDataObj projectname];
                titleString = [titleString stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
                
                if(![[tempDataObj projectname] isEqualToString:@"No Project Assigned"])
                    body = [body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td>%@</td><td align=\"center\" >%d</td></tr>",titleString,[self findTODOTotal:[tempDataObj projectid]]]];     
            }
        }
		body = [body stringByAppendingString:@"</table>"];
        
        
        //Steve - Infocus Pro Link
        NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
        body = [body stringByAppendingString:bodyURL];
        
        
		[mailCntrlr setMessageBody:body isHTML:YES];
        
        [self presentViewController:mailCntrlr animated:YES completion:nil];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
	
}

-(IBAction)settingBarButton_Clicked:(id)sender{
 
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SettingsTableViewController" bundle:nil];
    UIViewController *settingsTableViewController = [sb instantiateViewControllerWithIdentifier:@"SettingsTableViewController"];
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    [self presentViewController:settingsTableViewController animated:YES completion:nil];
}


- (IBAction)infoBarButton_Clicked:(id)sender{
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"FREE Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"FREE Button"]; //Steve
    
    //Steve - goes right to App store instead of using sales page below
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];
    
    //**********************************************************************
    //**************** Steve - Removed Sales Page ***************************
    //**********************************************************************
    
    /*
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [navBarUpgradeView setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    upgradeScrollView.userInteractionEnabled = YES;
    upgradeScrollView.bounces = NO;
    
    if (IS_IPHONE_5){
        upgradeScrollView.frame = CGRectMake(0, 44, 320, 568);
        upgradeView.frame = CGRectMake(0, 0, 320, 568);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1088)];
    }
    else{
        upgradeScrollView.frame = CGRectMake(0, 44 + 88, 320, 480); //Some issue with frame size, add 88 (iPhone 5)
        upgradeView.frame = CGRectMake(0, 0, 320, 480);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1010)];
    }

    upgradeView.hidden = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromBottom];
    [self.view addSubview:upgradeView];
     */
    
}

- (IBAction)cancelUpgradeButton_Clicked:(id)sender {
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Upgrade Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"Upgrade Button"]; //Steve
    
    upgradeView.hidden = YES;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
}

- (IBAction)upgradeButton_Clicked:(id)sender {
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];

}



@end








//
//  NewFeaturesViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 2/24/15.
//
//

#import "NewFeaturesViewController.h"

@interface NewFeaturesViewController ()

@property (strong, nonatomic) IBOutlet UIButton *okayButton;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction)okayButton_Clicked:(id)sender;

@end



@implementation NewFeaturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    //Find App Version Number
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *version = @"Version ";
    _versionLabel.text = [version stringByAppendingString:appVersionString];
    
    
    //Custom "OK" Button
    //[_okayButton setTitle:@"OK" forState:UIControlStateNormal];
    [_okayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_okayButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    //_okayButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    _okayButton.layer.cornerRadius = 15.0;
    _okayButton.layer.borderWidth = 1.0;
    _okayButton.layer.borderColor = [[UIColor colorWithRed:220.0/256.0 green:220.0/256.0 blue:220.0/256.0 alpha:1.0]CGColor];
    _okayButton.backgroundColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
    

    
}


- (IBAction)okayButton_Clicked:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:NO forKey:@"NewFeature_3.2.0"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end

//
//  ProjectsDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProjectsDataObject : NSObject {
	NSString *projectName;
	NSInteger projectID;
	NSInteger projectLevel;
	
	
}

@property(nonatomic, strong) NSString *projectName;
@property(nonatomic, assign) NSInteger projectID;
@property(nonatomic, assign) NSInteger projectLevel;

-(id)initWithProjectId:(NSInteger) projectIdLocal projectTitle:(NSString*)projectTitleLocal;
@end

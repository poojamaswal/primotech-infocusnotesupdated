//
//  ProjectSelectionDelegate.h
//  Organizer
//
//  Created by lakshmi on 21/07/15.
//
//

@class AddProjectDataObject;

@protocol ProjectSelectionDelegate <NSObject>

@required
-(void)selectedProject:(AddProjectDataObject *)newProject;
@end
//
//  DatabaseManager.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DatabaseManager.h"

#import <CoreData/CoreData.h>
#import <CoreData/NSManagedObject.h>

#import "NotesID.h"
#import "DrawingData.h"
#import "ImageData.h"
#import "StickyData.h"
#import "DrawingDefaults.h"
#import "TextData.h"

@implementation DatabaseManager

__strong static DatabaseManager *_databaseManager;


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (DatabaseManager*)sharedManager
{
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _databaseManager = [[self alloc] init];
    });
    return _databaseManager;
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Notes" withExtension:@"mom"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Notes.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Database Fetch Methods
-(NSArray*)fetchNumberOFPagesSavedIfAny
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        if(nm.pageID)
            [arrTemp addObject:nm.pageID];
    }
    return arrTemp;
}

-(NSArray*)fetchImagesOFPagesSavedIfAny
{
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        DrawingData *drawingData = nm.drawingData;
        UIImage *image =[DatabaseManager imageWithImage:[UIImage imageWithData:drawingData.drawingImageData scale:0.3] scaledToSize:CGSizeMake(150, 150)];
        [arrTemp addObject:image];
    }
    
    return arrTemp;
    
}

-(NSArray*)fetchImagesOFSubPagesSavedIfAnyFromNotesID:(NSString*)str
{
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        NSSet *drawingDataSet;
        if([str isEqualToString:nm.pageID])
        {
            drawingDataSet = nm.drawingData;
            NSArray *arr_Data = [NSArray arrayWithArray:[drawingDataSet allObjects]];
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
            
            arr_Data = [arr_Data sortedArrayUsingDescriptors:@[sortDescriptor]];
            
            if(arr_Data.count>0)
            {
                for(int i=0;i<arr_Data.count;i++)
                {
                    DrawingData *drawingData = [arr_Data objectAtIndex:i];
                    UIImage *image =[UIImage imageWithData:drawingData.drawingIconData];
                    [arrTemp addObject:image];
                    
                }
                break;
            }
        }
        
        
    }
    
    return arrTemp;
}

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (NotesID*)pageForName:(NSString*)name
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    NotesID *nm1 = nil;
    
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        NSLog(@"%@",nm.pageID);
        if ([nm.pageID isEqualToString:name])
        {
            nm1 = nil;
            nm1 = nm;
            nm1.subNotesCount = [NSNumber numberWithInt:[DrawingDefaults sharedObject].intNotesSubNotesCount];
            
            break;
        }
    }
    
    return nm1;
}


// Get Song Mode if exisi in DB for given songID
-(void)deleteNotesWithID:(NSString *)pageID
{
    
    NotesID *nm =  [DatabaseManager pageForName:pageID];
    [_managedObjectContext deleteObject:nm];
    [_managedObjectContext save:nil];
    
}
// Get Song Mode if exisi in DB for given songID
- (void)deleteNotesWithID:(NSString *)pageID withIndex:(int)index
{
    NotesID *nm =  [DatabaseManager pageForName:pageID];
    
    if([NSArray arrayWithArray:[nm.drawingData allObjects]].count==1)
    {
        [_managedObjectContext deleteObject:nm];
    }
    else
    {
        
        NSSet *setDrawingData = nm.drawingData;
        NSSet *setTextData = nm.textData;
        NSSet *setImageData = nm.imageData;
        NSSet *setStickyData = nm.stickyData;
        
        NSArray *arrData = [NSArray arrayWithArray:[setDrawingData allObjects]];
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
        arrData = [arrData sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        DrawingData* data = [arrData objectAtIndex:index];
        [_managedObjectContext deleteObject:data];
        [DrawingDefaults sharedObject].intNotesSubNotesCount = arrData.count-1;
        if([DrawingDefaults sharedObject].intNotesSubNotesCount>0)
            [DrawingDefaults sharedObject].intNotesSubNotesCounter = [DrawingDefaults sharedObject].intNotesSubNotesCount;
        else
            [DrawingDefaults sharedObject].intNotesSubNotesCounter =1;
        data=nil;
        arrData=nil;
        
        
        
        arrData = [NSArray arrayWithArray:[setTextData allObjects]];
        sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
        arrData = [arrData sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        
        TextData *textData = [arrData objectAtIndex:index];
        [_managedObjectContext deleteObject:textData];
        [DrawingDefaults sharedObject].intNotesSubNotesCount = arrData.count-1;
        if([DrawingDefaults sharedObject].intNotesSubNotesCount>0)
            [DrawingDefaults sharedObject].intNotesSubNotesCounter = [DrawingDefaults sharedObject].intNotesSubNotesCount;
        else
            [DrawingDefaults sharedObject].intNotesSubNotesCounter =1;
        
        
        
        //        arrData = [NSArray arrayWithArray:[setTextData allObjects]];
        //        data = [arrData objectAtIndex:index];
        //        [_managedObjectContext deleteObject:data];
        //        data=nil;
        //        arrData=nil;
        //
        //        arrData = [NSArray arrayWithArray:[setImageData allObjects]];
        //        if(arrData.count>0)
        //        {
        //        data = [arrData objectAtIndex:index];
        //        [_managedObjectContext deleteObject:data];
        //        }
        //        data=nil;
        //        arrData=nil;
        //
        //        arrData = [NSArray arrayWithArray:[setStickyData allObjects]];
        //        if(arrData.count>0)
        //        {
        //        data = [arrData objectAtIndex:index];
        //        [_managedObjectContext deleteObject:data];
        //        }
        data=nil;
        arrData=nil;
    }
    
    [_managedObjectContext save:nil];
    
}
- (void)deleteImagesWithPageID:(NSString*)pageID
{
    NotesID *nm =  [DatabaseManager pageForName:pageID];
    NSSet *docSet = nm.imageData;
    NSArray *arr = [NSArray arrayWithArray:[docSet allObjects]];
    ImageData *dataImage;
    
    for(int i=0;i<arr.count;i++)
    {
        NSArray *arrData = [[DatabaseManager sharedManager] arrayForDrawingDataForPresentNoteID];
        DrawingData * aDrawingData;
        if(arrData.count>[DrawingDefaults sharedObject].intNotesSubNotesCounter-1)
        {
            aDrawingData = [arrData objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
            dataImage = ((ImageData*)[arr objectAtIndex:i]);
            NSLog(@"Sticky      %@",dataImage.savedDate);
            NSLog(@"DrawingData %@",aDrawingData.savedDate);
            if([dataImage.savedDate isEqual:aDrawingData.savedDate])
            {
                //                NSLog(@"%@",dataImage.stickyID);
                
                [_managedObjectContext deleteObject:dataImage];
                [_managedObjectContext save:nil];
            }
        }
    }
}
- (void)deleteStickiesWithPageID:(NSString*)pageID
{
    NotesID *nm =  [DatabaseManager pageForName:pageID];
    NSSet *docSet = nm.stickyData;
    NSArray *arr = [NSArray arrayWithArray:[docSet allObjects]];
    StickyData *dataSticky;
    
    
    for(int i=0;i<arr.count;i++)
    {
        NSArray *arrData = [[DatabaseManager sharedManager] arrayForDrawingDataForPresentNoteID];
        DrawingData * aDrawingData;
        if(arrData.count>[DrawingDefaults sharedObject].intNotesSubNotesCounter-1)
        {
            aDrawingData = [arrData objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
            dataSticky = ((StickyData*)[arr objectAtIndex:i]);
            
            NSLog(@"Sticky      %@",dataSticky.savedDate);
            NSLog(@"DrawingData %@",aDrawingData.savedDate);
            if([dataSticky.savedDate isEqual:aDrawingData.savedDate])
            {
                NSLog(@"%@",dataSticky.stickyID);
                
                [_managedObjectContext deleteObject:dataSticky];
                [_managedObjectContext save:nil];
            }
        }
    }
}

- (int)getCountForSubNotesForNoteID: (NSString*)str
{
    NotesID *nm = [DatabaseManager pageForName:str];
    
    NSSet *aDrawingDataSet = nm.drawingData;
    NSArray *arr_DrawingData = [NSArray arrayWithArray:[aDrawingDataSet allObjects]];
    
    return arr_DrawingData.count;
}

- (NSArray*)arrayForDrawingDataForPresentNoteID
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"DrawingData"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    array = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    DrawingData *aDrawingData;
    NSMutableArray *arrData  = [[NSMutableArray alloc]init];
    for(int i=0;i<array.count;i++)
    {
        aDrawingData = [array objectAtIndex:i];
        if([aDrawingData.notesID.pageID isEqualToString:[DrawingDefaults sharedObject].strNoteName])
        {
            [arrData addObject:aDrawingData];
        }
    }
    return arrData;
}
@end

//
//  DatabaseManager.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NotesID;

@interface DatabaseManager : NSObject


+ (DatabaseManager*)sharedManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (NSArray*)fetchNumberOFPagesSavedIfAny;
- (NSArray*)fetchImagesOFPagesSavedIfAny;
+ (NotesID*)pageForName:(NSString*)name;
- (void)deleteNotesWithID:(NSString *)pageID;
- (void)deleteNotesWithID:(NSString *)pageID withIndex:(int)index;
- (void)deleteImagesWithPageID:(NSString*)pageID;
- (void)deleteStickiesWithPageID:(NSString*)pageID;
-(NSArray*)fetchImagesOFSubPagesSavedIfAnyFromNotesID:(NSString*)str;
- (int)getCountForSubNotesForNoteID: (NSString*)str;
- (NSArray*)arrayForDrawingDataForPresentNoteID;
@end

//
//  CalenderAddButtonController.h
//  Organizer
//
//  Created by Nibha Aggarwal on 2/10/12.
//  Copyright 2012 A1 Technology Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCalenderDataObject.h"
#import "CalendarDataObject.h"
@protocol CalenderAddButtonControllerDelegate
-(void)setNewRGBColor:(float)red greenColor:(float)green blueColor:(float)blue;
@end



@interface CalenderAddButtonController : UIViewController <UITextFieldDelegate,UIActionSheetDelegate>
{
	IBOutlet  UITextField *calName;
	IBOutlet  UIButton *ColorButton;
    IBOutlet UIButton *DeleteButton;
	id __unsafe_unretained delegate;
	float redVal;
	float greenVal;
	float blueVal;
	NSString *sql;
    IBOutlet UINavigationBar *navBar;
    CalendarDataObject *calObj;
    BOOL IsEdit;
    NSString *CalIdentifier;
    
    IBOutlet UIButton *saveButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *calendarButton;
    IBOutlet UIButton *calendarColorButton;
    IBOutlet UIButton *calendarColorDisclBtn;
    IBOutlet UILabel *calendarColorLabel;
    
	
}
@property(nonatomic,strong)CalendarDataObject *calObj;
@property(unsafe_unretained)id delegate;
-(IBAction) saveButton_Click:(id)sender;
-(IBAction) cancelButton_Click:(id)sender;
-(IBAction) rgb_click:(id)sender;
- (IBAction)textFieldDidEndEditing:(UITextField *)textField;
- (IBAction)textFieldDidBeginEditing:(UITextField *)textField;
-(IBAction)DeleteCalendarBtnClicked:(id)sender;
-(id)initWithDataObj: (CalendarDataObject*) caldata;
@end

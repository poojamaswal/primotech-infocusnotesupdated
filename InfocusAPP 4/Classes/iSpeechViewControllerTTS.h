//
//  iSpeechViewControllerTTS.h
//  Organizer
//
//  Created by Aman Gupta on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISpeechSDK.h"

@interface iSpeechViewControllerTTS : UIViewController<ISSpeechSynthesisDelegate>
@property (nonatomic, retain, readonly) iSpeechSDK *iSpeech;
@property(nonatomic, strong)id parentVC;

-(void)readText:(NSString *)textToSpeak;
+(iSpeechViewControllerTTS *)sharedApplication;

@end
id parentVC;

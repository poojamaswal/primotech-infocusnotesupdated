//
//  UseCurrentLocationViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LocationViewController.h"
@interface UseCurrentLocationViewController : UIViewController<MKMapViewDelegate, MKReverseGeocoderDelegate,UIAlertViewDelegate> {
	MKMapView *mapVw;
	MKReverseGeocoder *reverseGeocoder;
	UITextField *prsnNameTxtFld;
	IBOutlet UIBarButtonItem *saveAddressButton;
	
	
}
@property (nonatomic, strong) IBOutlet MKMapView *mapVw;
@property (nonatomic, strong) MKReverseGeocoder *reverseGeocoder;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *saveAddressButton;

- (IBAction)reverseGeocodeCurrentLocation;
-(IBAction)backClicked:(id)sender;
@end

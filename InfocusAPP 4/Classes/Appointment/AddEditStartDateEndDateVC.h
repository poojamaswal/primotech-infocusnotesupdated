//
//  AddEditStartDateEndDateVC.h
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentViewController .h"
//#import "AddEditToDoViewController.h"

@interface AddEditStartDateEndDateVC : UIViewController <UIPickerViewDelegate> {
	IBOutlet UIDatePicker		*datePicker;
	IBOutlet UILabel			*strtDateLbl;
	IBOutlet UILabel			*endDateLbl;
	IBOutlet UISwitch			*allDaySwitch;
 	
	NSString					*strtDateString;
	NSString					*endDateString;
	NSInteger					selectedCellIndex;
    id							__unsafe_unretained delegate;
	
	NSDateFormatter				*formatter;
	
	BOOL						isAllDay;
	
	IBOutlet UIButton			*allDayButton;
	IBOutlet UILabel			*allDayCaptionLbl;
    BOOL                        setVal;
    NSInteger                   todoVal;
    NSString                    *callerVC;
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton           *cancelButton;
    IBOutlet UIButton           *doneButton;
    IBOutlet UIButton           *startsButton;
    IBOutlet UILabel            *startsFixedlabel;
    IBOutlet UIButton           *endsButton;
    IBOutlet UILabel            *endsFixedLabel;
    
    BOOL                        is24HourClock; //Steve - 24 hour or 12 hour clock
    BOOL                        isStartEndON; //Steve - turn ON/OFF start/End dates
   
    
    
}
@property(nonatomic,assign)	 NSInteger  todoVal;
@property(unsafe_unretained)			id		delegate;   
@property(nonatomic,strong)	NSString *strtDateString;
@property(nonatomic,strong)	NSString *endDateString;
@property(nonatomic,strong)	IBOutlet UIButton *allDayButton;
@property(nonatomic,strong)	IBOutlet UILabel  *allDayCaptionLbl;
@property(nonatomic,assign) BOOL     setVal;
@property(nonatomic,strong)	NSString  *callerVC;



-(id)initWithStartDateString:(NSString*) startDateStr endDateStr:(NSString*) endDateStr isAllDay:(BOOL) allDay ;

-(IBAction) startDate_ButtonClicked:(UIButton *)sender;
-(IBAction) endDate_ButtonClicked:(UIButton *)sender;
-(IBAction) updateDateLbl:(id) sender; 
-(IBAction)doneClicked:(id)sender;
-(IBAction)backClicked:(id)sender;
-(IBAction)allDaySwitchClicked:(id)sender;

@end

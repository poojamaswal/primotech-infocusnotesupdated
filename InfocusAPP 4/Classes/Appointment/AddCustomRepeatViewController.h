//
//  AddCustomRepeatViewController.h
//  Organizer
//
//  Created by Priyanka Taneja on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllInsertDataMethods.h"


@interface AddCustomRepeatViewController : UIViewController {

	IBOutlet UINavigationController *__unsafe_unretained nav1;
	IBOutlet UIBarButtonItem *__unsafe_unretained BarBtn1;
	IBOutlet UIButton *save;
	IBOutlet UITextField *txt1;
	IBOutlet UITextField *txt2;
	IBOutlet UITextField *txt3;
	IBOutlet UITextField *txt4;
	IBOutlet UITextField *txt5;
	IBOutlet UITextField *txt6;
	IBOutlet UIScrollView *scroll;
	
}
@property(nonatomic, unsafe_unretained) UINavigationController *nav1;
@property(nonatomic, unsafe_unretained) UIBarButtonItem *BarBtn1;
@property(nonatomic,strong)UIButton *save;
@property (nonatomic,strong)IBOutlet UIScrollView *scroll;

-(IBAction) Back_buttonClicked:(id)sender;
//-(IBAction) Save_buttonClicked:(id)sender;
-(IBAction) keyboarddown:(id) sender;


@end

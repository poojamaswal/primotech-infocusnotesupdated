//
//  AddEditTitleViewController.m
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddEditTitleViewController.h"
#import "LocationListViewController.h"

@interface AddEditTitleViewController(private)
-(void)setLabelValues;
@end

@implementation AddEditTitleViewController
@synthesize locationLbl, titleTxtFld;
@synthesize appointmentType, appointmentBinary, appointmentBinaryLarge;
@synthesize appointmentTitle, locationID,locationtext;
@synthesize delegate;

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

-(id)initWithAppType:(NSString *) appType appTitle:(NSString *) appTitle appBinary:(NSData *) appBinary appBinaryLarge:(NSData *) appBinaryLarge locationID:(NSString *) locationIDLocal  LocText:(NSString*) locText {
	if (self = [super initWithNibName:@"AddEditTitleViewController" bundle:[NSBundle mainBundle]]) {
		
		if (appType) {
			self.appointmentType = appType;
		}
		
		if (appTitle) {
			self.appointmentTitle = appTitle;
		}
		
		if (appBinary) {
			self.appointmentBinary = appBinary;
		}
		
		if (appBinaryLarge) {
			self.appointmentBinaryLarge = appBinaryLarge;
		}
		
		if (locationIDLocal) {
			self.locationID = locationIDLocal;
		}
		
        if (locText) {
			self.locationtext = [locText isEqualToString:@"None"] ? @"Select Location" : locText;
		}
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 119, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(257, 104) andParentVC:self fromMainScreen:NO];
}

-(IBAction)backClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	[self setLabelValues];
    locationLbl.text = locationtext;
	
}

#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -

-(IBAction)selectLocationButton_Clicked:(id)sender 
{
    LocationListViewController *locationViewController = [[LocationListViewController alloc] initWithNibName:@"LocationListViewController" bundle:[NSBundle mainBundle] withLocationID:[locationID intValue]];
    [locationViewController setDelegate:self];
	[self.navigationController pushViewController:locationViewController animated:YES];
}

-(void)setLocObject:(LocationDataObject*)locObj  selectedval:(NSString*)val
{
    locationLbl.text = val;
    self.locationtext = val;
    [[self delegate] setLocObject:locObj selectedval:val];
}

-(IBAction)doneClicked:(id)sender {
	if ([appointmentType isEqualToString:@"H"]){
		if (appointmentBinary == nil) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please enter Title & Location in order to proceed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alert show];
			return;
		}
	}else if([titleTxtFld.text isEqualToString:@""]){
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please enter Title & Location in order to proceed." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		return;
	}
	
	if (appointmentType == nil &&  appointmentTitle == nil && appointmentBinary == nil) {
        // nidhi && locationDataObj == nil) {
		[self.navigationController popViewControllerAnimated:YES];
	}
    else
    {
		[titleTxtFld resignFirstResponder];
		if ([appointmentType isEqualToString: @"T"])
        {
			if (appointmentTitle) {
				appointmentTitle = nil;
			}
			appointmentTitle = titleTxtFld.text;  
		}
		[[self delegate] setAppointmentTitle:titleTxtFld.text];
        [[self delegate] setAppointmentType:appointmentType];
        [[self delegate] setAppointmentBinary:appointmentBinary];
		[[self delegate] setAppointmentBinaryLarge:appointmentBinaryLarge];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

-(IBAction) inputTypeSelectorButton_Clicked:(id)sender {
	[titleTxtFld resignFirstResponder];
	
	if (inputTypeSelectorViewController) {
		inputTypeSelectorViewController = nil;
	}
	
    
    inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 119, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(257, 104) andParentVC:self fromMainScreen:YES];
    
    
    
//	inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 119, 150, 200) andArrowDirection:@"U"andArrowStartPoint:CGPointMake(257, 104) andParentVC:self fromMainScreen:NO]];

                                       
              
       
	[inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
	[self.view addSubview:inputTypeSelectorViewController.view];
	[self.view bringSubviewToFront:inputTypeSelectorViewController.view];
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)setLabelValues {
	if ([appointmentType isEqualToString: @"T"] || [appointmentType isEqualToString: @"V"]) {
		if (appointmentTitle && (![appointmentTitle isEqualToString:@""])) {
			[titleTxtFld setText:appointmentTitle];
			[titleTxtFld setTextColor:[UIColor blueColor]];
		}
//		[titleTxtFld.layer setCornerRadius:0.0];
//		titleTxtFld.layer.borderColor = [UIColor clearColor].CGColor;
//		titleTxtFld.layer.borderWidth = 0.0;
		[titleTxtFld becomeFirstResponder];
	}
	else if ([appointmentType isEqualToString: @"H"] && appointmentBinary) {
		[titleTxtFld setPlaceholder: @""];
		UIImage *tempTitleImage = [[UIImage alloc] initWithData:appointmentBinary];
		[titleTxtFld setBackground:tempTitleImage];
//		[titleTxtFld.layer setCornerRadius:5.0];
//		titleTxtFld.layer.borderColor = [UIColor purpleColor].CGColor;
//		titleTxtFld.layer.borderWidth = 0.5;
		
		[titleTxtFld setText:@""];
		[titleTxtFld resignFirstResponder];
	}
	else if (([appointmentType isEqualToString: @""] || appointmentType == nil)){
		[titleTxtFld setPlaceholder: @"Title"];
		appointmentType = @"T";
		appointmentBinary = nil;
		[titleTxtFld becomeFirstResponder];
//		[titleTxtFld.layer setCornerRadius:0.0];
//		titleTxtFld.layer.borderColor = [UIColor clearColor].CGColor;
//		titleTxtFld.layer.borderWidth = 0.0;
	}
}

#pragma mark -
#pragma mark TextField Delegate Methods
#pragma mark -

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if ([appointmentType  isEqualToString:@"H"]) {
		[textField resignFirstResponder];
		return;
	}
	if (appointmentTitle) {
		appointmentTitle = nil;
	}
	appointmentTitle = textField.text;  
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	if ([appointmentType  isEqualToString:@"H"]) {
		[textField resignFirstResponder];
		return YES;
	}
	if (appointmentTitle) {
		appointmentTitle = nil;
	}
	appointmentTitle = textField.text;  
	return YES;
}

#pragma mark -
#pragma mark AlertView Delegate
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	[titleTxtFld becomeFirstResponder];
}

#pragma mark -
#pragma mark Memory Management Methods
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

//
//  UseMapLocationViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LocationViewController.h"

@interface AddressAnnotation : NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
	NSString *address;
}

@end

@interface UseMapLocationViewController : UIViewController<MKMapViewDelegate,MKReverseGeocoderDelegate,UIAlertViewDelegate,UITextFieldDelegate> {
	IBOutlet UITextField *addressField;
	IBOutlet UIButton *goButton;
	IBOutlet MKMapView *mapVw;
	UITextField *prsnNameTxtFld;
	
	MKReverseGeocoder *reverseGeocoder;
	AddressAnnotation *addAnnotation;
}
@property (nonatomic, strong) MKReverseGeocoder *reverseGeocoder;
-(IBAction)backClicked:(id)sender;
- (IBAction) showAddress;
-(CLLocationCoordinate2D) addressLocation;
-(void)reverseGeocodeInputLocation;

@end

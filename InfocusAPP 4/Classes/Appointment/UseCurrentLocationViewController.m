//
//  UseCurrentLocationViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UseCurrentLocationViewController.h"
#import "AllInsertDataMethods.h"


@implementation UseCurrentLocationViewController

@synthesize mapVw,reverseGeocoder,saveAddressButton;

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[mapVw.layer setCornerRadius:5.0];
	mapVw.layer.masksToBounds = YES;
	mapVw.layer.borderColor = [UIColor purpleColor].CGColor;
	mapVw.layer.borderWidth = 0.5;

	
	prsnNameTxtFld = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	[saveAddressButton setEnabled:NO];
	mapVw.showsUserLocation = YES;

}

-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark ReverseGeoCoder Methods
#pragma mark -
- (IBAction)reverseGeocodeCurrentLocation
{
	UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"\n\n Please enter the name of the concerned, press OK to continue." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[prsnNameTxtFld setBackgroundColor:[UIColor whiteColor]];
	prsnNameTxtFld.placeholder=@"<Enter the Person Name here>";
	[myAlertView addSubview:prsnNameTxtFld];
	[prsnNameTxtFld becomeFirstResponder];
	[myAlertView show];
	
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot obtain address." message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
    LocationDataObject* aLocObj=[[LocationDataObject alloc]init];
	if ([prsnNameTxtFld.text isEqualToString:@""]) {
		aLocObj.locationPersonName=@"";
	}
	else {
    	aLocObj.locationPersonName=prsnNameTxtFld.text;		
	}
    aLocObj.locationAddress=placemark.thoroughfare;
    aLocObj.locationCity=placemark.locality;
    aLocObj.locationState=placemark.administrativeArea;
	
	if (placemark.postalCode!=nil) {
		aLocObj.locationZip=placemark.postalCode;
	}
	else {
		aLocObj.locationZip=@"";
	}

	aLocObj.isVerified=YES;

	BOOL isInserted = [AllInsertDataMethods insertLocationDataValues:aLocObj];
	
	NSLog(@"Pring %d", isInserted);
	
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    // we have received our current location.. now placemarks can be received.
	MKCoordinateRegion region;
	
	region.center.latitude=userLocation.location.coordinate.latitude;
	region.center.longitude=userLocation.location.coordinate.longitude;
	region.span.latitudeDelta  =0.01;
	region.span.longitudeDelta=0.01;
	[mapView setRegion:region animated:TRUE];
	[mapView regionThatFits:region];
	
	[saveAddressButton setEnabled:YES];
	//[self reverseGeocodeCurrentLocation];
}

#pragma mark -
#pragma mark AlertView Delegate Methods
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	self.reverseGeocoder =
	[[MKReverseGeocoder alloc] initWithCoordinate:mapVw.userLocation.location.coordinate];
    reverseGeocoder.delegate = self;
    [reverseGeocoder start];
}
#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
	self.mapVw = nil;
	self.saveAddressButton=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

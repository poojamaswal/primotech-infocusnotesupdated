//
//  AddAlertViewController.h
//  Organizer
//
//  Created by Priyanka Taneja on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddAlertViewController.h"


@interface AddAlertViewController : UIViewController {
	IBOutlet UITableView *alertTblView;
	IBOutlet UINavigationController *nav1;
	IBOutlet UIBarButtonItem *Barbtnleft;
	IBOutlet UIBarButtonItem *Barbtnright;
	NSMutableArray *array;

}
@property (nonatomic,retain) UITableView *alertTblView;
@property (nonatomic,retain) UINavigationController *nav1;
@property (nonatomic,retain) NSMutableArray *array;


-(IBAction) Back_buttonClicked:(id)sender;
-(IBAction)doneClicked:(id)sender;
@end

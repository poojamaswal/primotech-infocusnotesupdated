//
//  AddEditTitleViewController.h
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentViewController .h"
#import "AppointmentsDataObject.h"
#import "LocationDataObject.h"
#import "InputTypeSelectorViewController.h"

@class InputTypeSelectorViewController;

@protocol AddEditTitleViewControllerrDelegate
-(void)setLocObject:(LocationDataObject*)locObj selectedval:(NSString*)val;
@end

@interface AddEditTitleViewController : UIViewController <UITextFieldDelegate> {

	IBOutlet UITextField			*titleTxtFld;
	IBOutlet UILabel				*locationLbl;
	
	NSString						*appointmentTitle;
	NSString						*appointmentType;
	NSData							*appointmentBinary;
	NSData							*appointmentBinaryLarge;
	
    NSString						*locationtext;
    NSString						*locationID;
    id								__unsafe_unretained delegate;
    NSString						*selectedvalue;
    InputTypeSelectorViewController	*inputTypeSelectorViewController;
}

@property (unsafe_unretained) id delegate;

@property(nonatomic, strong) UILabel				*locationLbl;
@property(nonatomic, strong) UITextField			*titleTxtFld;

@property(nonatomic, strong) NSString				*appointmentTitle;
@property(nonatomic, strong) NSString				*appointmentType;
@property(nonatomic, strong) NSData					*appointmentBinary;
@property(nonatomic, strong) NSData					*appointmentBinaryLarge;

@property(nonatomic, strong) NSString				*locationtext;
@property(nonatomic, strong) NSString				*locationID;

-(id)initWithAppType:(NSString *) appType appTitle:(NSString *) appTitle appBinary:(NSData *) appBinary appBinaryLarge:(NSData *) appBinaryLarge locationID:(NSString *) locationIDLocal  LocText:(NSString*) locText; 

-(IBAction)inputTypeSelectorButton_Clicked:(id)sender;
-(IBAction)selectLocationButton_Clicked:(id)sender;
-(IBAction)backClicked:(id)sender;
-(IBAction)doneClicked:(id)sender;
@end

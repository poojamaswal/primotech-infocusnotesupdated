//
//  UseMapLocationViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UseMapLocationViewController.h"
#import "AllInsertDataMethods.h"
#import "LocationDataObject.h"

@implementation AddressAnnotation

@synthesize coordinate;

- (NSString *)subtitle{
	return address;
}
- (NSString *)title{
	return @"Dropped Pin";
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c :(NSString *)locAddress{
	address = locAddress;
	coordinate=c;
	
//	NSLog(@"%f,%f",c.latitude,c.longitude);
	return self;
}

@end

@implementation UseMapLocationViewController
@synthesize reverseGeocoder;

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	[mapVw.layer setCornerRadius:5.0];
	mapVw.layer.masksToBounds = YES;
	mapVw.layer.borderColor = [UIColor purpleColor].CGColor;
	mapVw.layer.borderWidth = 0.5;
	
	prsnNameTxtFld = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	[addressField becomeFirstResponder];
	
}

-(IBAction)backClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Memory Management 
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (IBAction) showAddress {
	[addressField resignFirstResponder];
	MKCoordinateRegion region;
	MKCoordinateSpan span;
	span.latitudeDelta=0.2;
	span.longitudeDelta=0.2;
	CLLocationCoordinate2D location = [self addressLocation];
	region.span=span;
	region.center=location;

	if(addAnnotation != nil) {
		[mapVw removeAnnotation:addAnnotation];
		addAnnotation = nil;
	}
	
	addAnnotation = [[AddressAnnotation alloc] initWithCoordinate:location :addressField.text];
	[mapVw addAnnotation:addAnnotation];
	
	[mapVw setRegion:region animated:TRUE];
	[mapVw regionThatFits:region];
	//[mapView selectAnnotation:mLodgeAnnotation animated:YES];
}

-(CLLocationCoordinate2D) addressLocation {
	NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv", 
						   [addressField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:nil];
	
	NSArray *listItems = [locationString componentsSeparatedByString:@","];
	
	double latitude = 0.0;
	double longitude = 0.0;
	
	if([listItems count] >= 4 && [[listItems objectAtIndex:0] isEqualToString:@"200"]) {
		latitude = [[listItems objectAtIndex:2] doubleValue];
		longitude = [[listItems objectAtIndex:3] doubleValue];
	}
	else {
		//Show error
	}
	CLLocationCoordinate2D location;
	location.latitude = latitude;
	location.longitude = longitude;
	
	return location;
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
	MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
	
	annView.pinColor = MKPinAnnotationColorRed;
	annView.animatesDrop=TRUE;
	annView.canShowCallout = YES;
	annView.calloutOffset = CGPointMake(-5, 5);
	
	UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[infoButton addTarget:self action:@selector(showOptions) forControlEvents:UIControlEventTouchUpInside];
	annView.rightCalloutAccessoryView = infoButton;
	
	mapVw.showsUserLocation=YES;
	return annView;
}

-(void)showOptions{
	UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"\n\n Please enter the name of the concerned and press OK to continue." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	
	[prsnNameTxtFld setBackgroundColor:[UIColor whiteColor]];
	prsnNameTxtFld.placeholder=@"<Enter the Person Name here>";
	[myAlertView addSubview:prsnNameTxtFld];
	[prsnNameTxtFld becomeFirstResponder];
	[myAlertView show];
}

#pragma mark -
#pragma mark ReverseGeoCoder Methods
#pragma mark -
- (void)reverseGeocodeInputLocation
{
	CLLocationCoordinate2D location = [self addressLocation];

    self.reverseGeocoder =
	[[MKReverseGeocoder alloc] initWithCoordinate:location];
    reverseGeocoder.delegate = self;
    [reverseGeocoder start];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot obtain address." message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
	NSMutableArray *tempLocValArray=[[NSMutableArray alloc] init];
	if (prsnNameTxtFld.text!=nil){
		[tempLocValArray addObject:prsnNameTxtFld.text];
		
	}
	else{
		[tempLocValArray addObject:@""];
	}
	[tempLocValArray addObject:placemark.thoroughfare];
	[tempLocValArray addObject:placemark.locality];
	[tempLocValArray addObject:placemark.administrativeArea];
	if (placemark.postalCode==NULL) {
	[tempLocValArray addObject:@""];	
	}
	else {
	[tempLocValArray addObject:placemark.postalCode];	
	}
	
	[tempLocValArray addObject:@"1"];
	
	
	LocationDataObject *temp = [[LocationDataObject alloc] initWithlocationID:1 locationPersonName:[tempLocValArray objectAtIndex:0] locationAddress:@"" locationCity:[tempLocValArray objectAtIndex:2] locationState:@"" locationZip:@"" isVerified:1];
	
	BOOL isInserted  = [AllInsertDataMethods insertLocationDataValues:temp];
	
	NSLog(@"Pring %d", isInserted);

	if (isInserted == YES) {
		NSArray *cntrlrsArr=[self.navigationController viewControllers];
		LocationViewController *tempVwCntrlr = (LocationViewController *)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-3];
		[self.navigationController popToViewController:tempVwCntrlr animated:YES];
	}
	
	
	/*//These Parameters can be known here..
	 placemark.thoroughfare
	 placemark.subThoroughfare;
	 placemark.locality
	 placemark.subLocality
	 placemark.subAdministrativeArea
	 placemark.postalCode
	 placemark.country*/
}

#pragma mark -
#pragma mark AlertView Delegate Methods
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	[prsnNameTxtFld resignFirstResponder];
	
	if (buttonIndex==1) {
		[self reverseGeocodeInputLocation];
	}
	
}
@end

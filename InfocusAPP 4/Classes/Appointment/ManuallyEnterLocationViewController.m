//
//  ManuallyEnterLocationViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ManuallyEnterLocationViewController.h"
#import "AllInsertDataMethods.h"
#import "LocationDataObject.h"
@interface ManuallyEnterLocationViewController()
//-(void)addAdditionalControlsInManuaalyEnterLocation;
//-(void)adjustControls;
@end

@implementation ManuallyEnterLocationViewController

#pragma mark -
#pragma mark ViewController LifeCycle
#pragma mark -
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [scrollView setContentSize:CGSizeMake(320, 600)];
    [super viewDidLoad];
	
}

-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Private Methods
#pragma mark -

#pragma mark -
#pragma mark All Methods
#pragma mark -
-(IBAction)saveBtnClicked:(id)sender{
	
	if (prsnNameTxtFld.text!=nil && address1TxtFld.text!=nil&& address2TxtFld.text!=nil && cityTxtFld.text!=nil && stateTxtFld.text!=nil && zipTxtFld.text!=nil) {
	  
        LocationDataObject* aLocObj=[[LocationDataObject alloc]init];
        aLocObj.locationPersonName=prsnNameTxtFld.text;		
        aLocObj.locationAddress=[NSString stringWithFormat:@"%@,%@",address1TxtFld.text,address2TxtFld.text];
        aLocObj.locationCity=cityTxtFld.text;
        aLocObj.locationState=stateTxtFld.text;
        aLocObj.locationZip=zipTxtFld.text;
        aLocObj.isVerified=YES;
        
        BOOL isInserted = [AllInsertDataMethods insertLocationDataValues:aLocObj];
		NSLog(@"Pring %d", isInserted);

        [self.navigationController popViewControllerAnimated:YES];
 	}
	else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill all the values to continue.." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}

}


-(IBAction)verifyLocation:(id)sender
{
	isVerified=YES;
}

#pragma mark -
#pragma mark TextField delegate Methods
#pragma mark -
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	if (textField == prsnNameTxtFld) {
		[prsnNameTxtFld resignFirstResponder];
		[address1TxtFld becomeFirstResponder];
	}
	
	else if (textField == address1TxtFld) {
		[address1TxtFld resignFirstResponder];
		[address2TxtFld becomeFirstResponder];
	}
	
	else if (textField == address2TxtFld) {
		[address2TxtFld resignFirstResponder];
		[cityTxtFld becomeFirstResponder];
	}
	
	else if (textField == cityTxtFld) {
		[cityTxtFld resignFirstResponder];
		[stateTxtFld becomeFirstResponder];
	}
	
	else if (textField == stateTxtFld) {
		[stateTxtFld resignFirstResponder];
		[zipTxtFld becomeFirstResponder];
	}
	else {
		[textField resignFirstResponder];
	}
	return YES;
}
#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

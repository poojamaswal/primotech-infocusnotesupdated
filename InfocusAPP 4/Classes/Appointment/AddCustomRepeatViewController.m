//
//  AddCustomRepeatViewController.m
//  Organizer
//
//  Created by Priyanka Taneja on 5/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddCustomRepeatViewController.h"
#import "AllInsertDataMethods.h"

@implementation AddCustomRepeatViewController

@synthesize nav1, BarBtn1, save, scroll;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
-(IBAction)keyboarddown:(id) sender 
{
	[txt1 resignFirstResponder];
}

#pragma mark -
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	if (textField == txt1) {
		[txt1 resignFirstResponder];
		//[monthTxtFld becomeFirstResponder];
	}
	
	else if (textField == txt2) {
		[txt2 resignFirstResponder];
		//[weekTxtFld becomeFirstResponder];
	}
	
	else if (textField == txt3) {
		[txt3 resignFirstResponder];
		//[dayTxtFld becomeFirstResponder];
	}
	
	else if (textField == txt4) {
		[txt4 resignFirstResponder];
		//[hourTxtFld becomeFirstResponder];
	}
	
	else if (textField == txt5) {
		[txt5 resignFirstResponder];
		//[minuteTxtFld becomeFirstResponder];
	}
	else {
		[textField resignFirstResponder];
	}
	return YES;
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
//-(IBAction) Save_buttonClicked:(id)sender
//{
//	int year = 0,month = 0, week = 0, day = 0, hour = 0, minute = 0, finalProd = 0;
//	NSString *repeatTitleString=@"Repeat every";
//	if (![txt1.text isEqualToString:@""]){
//		year=[[txt1 text] intValue]*365*24*60;
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@year",txt1.text]];
//	}
//	if (![txt2.text isEqualToString:@""]){
//		month=[[txt2 text] intValue]*30*24*60;
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@month",txt2.text]];
//	}
//	if (![txt3.text isEqualToString:@""]) {
//		week=[[txt3 text] intValue]*7*24*60;
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@week",txt3.text]];
//	}
//	if (![txt4.text isEqualToString:@""]) {
//		day=[[txt4 text] intValue]*24*60;
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@days",txt4.text]];
//	}
//	if (![txt5.text isEqualToString:@""]) {
//		hour=[[txt5 text] intValue]*60;
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@hours",txt5.text]];
//	}
//	if (![txt6.text isEqualToString:@""]) {
//		minute = [[txt6 text] intValue];
//		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@minutes",txt6.text]];
//	}
//	
//	finalProd = year + month + week + day + hour + day;
//	if (finalProd!=0) {
//		NSString *prodString=[NSString stringWithFormat:@"%d",finalProd];
//		
//		NSMutableArray *tempCustomRepeatDataVal=[[NSMutableArray alloc] init];
//		[tempCustomRepeatDataVal addObject:prodString];
//		[tempCustomRepeatDataVal addObject:@"1"];
//		[tempCustomRepeatDataVal addObject:repeatTitleString];
//		
//		BOOL isInserted = [AllInsertDataMethods insertCustomRepeatDataValues:tempCustomRepeatDataVal];
//		[tempCustomRepeatDataVal release];
//		
//		if (isInserted==YES) {
//			[self.navigationController popViewControllerAnimated:YES];
//		}
//	}
//	else {
//		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please input values in atleast one field to save a custom repeat." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//		[alert show];
//		[alert release];
//	}
//	
//}

-(IBAction) Back_buttonClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}




@end











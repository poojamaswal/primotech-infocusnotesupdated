//
//  LocationViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocationViewController.h"
#import "LocationViewController.h"
#import "GetAllDataObjectsClass.h"
#import "LocationDataObject.h"
#import "AddEditTitleViewController.h"
#import "OrganizerAppDelegate.h"
#import "UseCurrentLocationViewController.h"
//#import "UseContactsLocationsViewController.h"
#import "UseMapLocationViewController.h"
#import "ManuallyEnterLocationViewController.h"
@implementation LocationViewController

//@synthesize locationArray;
#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil //withLocationID:(NSInteger) loctionID
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		//lastLocationID = loctionID;
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
//    locationArray = [[NSMutableArray alloc] init];
//    [self getAllLocationData];
    [super viewDidLoad];
}
-(IBAction)currentLoc_Click:(id)sender
{
    UseCurrentLocationViewController *currLctnCntrlr=[[UseCurrentLocationViewController alloc] initWithNibName:@"UseCurrentLocationViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:currLctnCntrlr animated:YES];
}
-(IBAction)contactLoc_Click:(id)sender
{
    [self getContacts];
}
-(IBAction)searchLoc_Click:(id)sender
{
    UseMapLocationViewController *useMapLocationViewController=[[UseMapLocationViewController alloc] initWithNibName:@"UseMapLocationViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:useMapLocationViewController animated:YES];
    
}
-(IBAction)manualLoc_Click:(id)sender
{
    ManuallyEnterLocationViewController *manuallyEnterLocationViewController=[[ManuallyEnterLocationViewController alloc] initWithNibName:@"ManuallyEnterLocationViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:manuallyEnterLocationViewController animated:YES];
}
-(void)getContacts{
	// creating the picker
	ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
	// place the delegate of the picker to the controll
	picker.peoplePickerDelegate = self;

	// showing the picker
    [self presentViewController:picker animated:YES completion:nil];
	// releasing
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    // assigning control back to the main controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
   // By Naresh Chouhan ARC
    // [self dissmissModalViewControllerAnimated:YES];
    return YES;
}


//Change by naresh For ARC

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
	if (property == kABPersonAddressProperty) {
        /*
         * Set up an ABMultiValue to hold the address values; copy from address
         * book record.
         */
//        ABMultiValueRef multi = ABRecordCopyValue(person, property);
//        // Set up an NSArray and copy the values in.
//        NSArray *theArray = [(id)ABMultiValueCopyArrayOfAllValues(multi) autorelease];
//        // Figure out which values we want and store the index.
//        const NSUInteger theIndex = ABMultiValueGetIndexForIdentifier(multi, identifier);
//        // Set up an NSDictionary to hold the contents of the array.
//        NSDictionary *theDict = [theArray objectAtIndex:theIndex];
//        // Set up NSStrings to hold keys and values.  First, how many are there?
//        const NSUInteger theCount = [theDict count];
//        NSString *keys[theCount];
//        NSString *values[theCount];
//        // Get the keys and values from the CFDictionary.  Note that because
//        // we're using the "GetKeysAndValues" function, you don't need to
//        // release keys or values.  It's the "Get Rule" and only applies to
//        // CoreFoundation objects.
//        [theDict getObjects:values andKeys:keys];
 	}		
    return NO;
}


//-(void)viewWillAppear:(BOOL)animated {
//	[super viewWillAppear:YES];
//	
//	UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//	[editBtn setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
//	[editBtn setFrame:CGRectMake(220, 10, 45, 19)];
//	[editBtn addTarget:self action:@selector(editCLicked) forControlEvents:UIControlEventTouchUpInside];
//	[self.navigationController.navigationBar addSubview:editBtn];
//	
////	[self getValuesFromDatabase];
//	//[locationTblVw reloadData];
//}
//-(void) getAllLocationData 
//{
//	
//	[locationArray  removeAllObjects];
//    
//	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
//	//database = [appDelegate database];
//	
//	sqlite3_stmt *selectAllLocationData_stmt = nil;
//	
//	if(selectAllLocationData_stmt == nil && appDelegate.database) {
//		const char* Sql = "Select LocationID, LocationPersonName, LocationAddress,LocationCity, LocationState, LocationZIP,IsVerified From LocationTable";
//		
//		if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectAllLocationData_stmt, NULL) == SQLITE_OK) {
//			while (sqlite3_step(selectAllLocationData_stmt) == SQLITE_ROW) {
//				LocationDataObject *locationDataObject = [[LocationDataObject alloc] init ];
//                [locationDataObject setLocationID:sqlite3_column_int(selectAllLocationData_stmt, 0)];
//                
//				[locationDataObject setLocationPersonName:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 1)]];
//                [locationDataObject setLocationAddress:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 2)]];
//                [locationDataObject setLocationCity:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 3)]];
//                [locationDataObject setLocationState:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 4)]];
//                [locationDataObject setLocationZip:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 5)]];
//                [locationDataObject setIsVerified:sqlite3_column_int(selectAllLocationData_stmt, 6)];
//                
//               	[locationArray addObject:locationDataObject];
//				[locationDataObject release];
//			}
//			sqlite3_finalize(selectAllLocationData_stmt);		
//			selectAllLocationData_stmt = nil;
//			
//		}
//	}
//	return ;
//}
-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}
//-(IBAction)addClicked:(id)sender
//{
//	LocationViewController *locationListViewController=[[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:[NSBundle mainBundle]];
//	[self.navigationController pushViewController:locationListViewController animated:YES];
//	[locationListViewController release];
//}
#pragma mark -
#pragma mark Private Methods
#pragma mark -


#pragma mark -
#pragma mark All Methods
#pragma mark -


#pragma mark -
#pragma mark TableView Methods
#pragma mark -

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//	return [locationArray count];
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return 60;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//	LocationDataObject *locationDataObject = [locationArray objectAtIndex:indexPath.row];
//	
//	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
//	if (cell == nil){
//		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"] autorelease];
//	}
//	
//	
//	
//	[cell.textLabel setText:[NSString stringWithFormat:@"%@, %@, %@",[locationDataObject locationAddress],[locationDataObject locationCity],[locationDataObject locationState]]];
//	
//	if (lastLocationID != 0) {
//		if ([locationDataObject locationID] == lastLocationID) {
//			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
//			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
//			[tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
//			[cell.contentView addSubview:tickImageView];
//			[chekMarkImage release];
//		}
//	}
//	[locationDataObject release];
//	return cell;
//}
//
// //Override to support conditional editing of the table view.
// - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//   // Return NO if you do not want the specified item to be editable.
//	 return YES;
// }
//
//
// // Override to support editing the table view.
// - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
// 
//	 if (editingStyle == UITableViewCellEditingStyleDelete) {
//	
//	 // Delete the row from the data source.
//	    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//	 }   
//	 else if (editingStyle == UITableViewCellEditingStyleInsert) {
//	 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//	 }   
// }
//
// // Override to support rearranging the table view.
// - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
//	 id from,to;
//	 
//	 from = [locationNameArr objectAtIndex:fromIndexPath.row];
//	 to = [locationNameArr objectAtIndex:toIndexPath.row];
//	 [locationNameArr replaceObjectAtIndex:fromIndexPath.row withObject:to];
//
// 	 [locationNameArr replaceObjectAtIndex:toIndexPath.row withObject:from];
// }
//
//// Override to support conditional rearranging of the table view.
// - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
//	 // Return NO if you do not want the item to be re-orderable.
//	 return YES;
// }
// 
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
//	UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
//
//	if (tickImageView) {
//		[tickImageView removeFromSuperview];
//		[tickImageView release];
//	}
//	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
//	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
//	
//	[Cell.contentView addSubview:tickImageView];
//	
//	LocationDataObject *locationDataObject = [[locationNameArr objectAtIndex:indexPath.row] retain];
//	
//	NSArray *cntrlrsArr = [self.navigationController viewControllers];
//	AddEditTitleViewController *tempVwCntrlr = (AddEditTitleViewController*)[cntrlrsArr objectAtIndex:[cntrlrsArr count] - 2] ;
//	//nidhi
////	tempVwCntrlr.locationDataObj = locationDataObject;
//	
////	[tempVwCntrlr setLocationDataObj:locationDataObject];
//	[self.navigationController popViewControllerAnimated:YES];
//	
//	[locationDataObject release];
//}
//
//#pragma mark -
//#pragma mark All Methods
//#pragma mark -
//
//-(void)editCLicked{
//	if (locationTblVw.editing==YES) {
//		[locationTblVw setEditing:NO];
//	}
//	else {
//		[locationTblVw setEditing:YES];
//	}
//}
//
#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end











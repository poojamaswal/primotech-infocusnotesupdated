//
//  AddEditNotesViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddEditNotesViewController.h"



@implementation AddEditNotesViewController
@synthesize previousNotes;
@synthesize delegate;
#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
        // Custom initialization.
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"]; 
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    
	[notesTxtVw.layer setCornerRadius:10.0];
	notesTxtVw.layer.masksToBounds = YES;
	notesTxtVw.layer.borderColor = [UIColor lightGrayColor].CGColor;  //Steve changed was: purpleColor
	notesTxtVw.layer.borderWidth = 0.5;
	[notesTxtVw becomeFirstResponder];
	notesTxtVw.text =  [previousNotes isEqualToString:@"None"] ? @"Notes" : previousNotes;
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        cancelButton.hidden = YES;
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeNone;//UIRectEdgeNone
            //self.view.backgroundColor = [UIColor whiteColor];
            
            
            //Done Button
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:0.7];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            }
            
            
            
            
            if (IS_IPHONE_5) {
                notesTxtVw.frame = CGRectMake(7, 49 - 44, 307, 193 + 44 + 88);
                
            }
            else {
                notesTxtVw.frame = CGRectMake(7, 49 - 44, 307, 193 + 44 + 88);
                
            }

            
            
            
        }
        else{ //iOS 6
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            if (IS_IPHONE_5) {
                notesTxtVw.frame = CGRectMake(7, 49 - 44, 307, 193 + 44 + 88);
                
            }
            else {
                notesTxtVw.frame = CGRectMake(7, 49 - 44, 307, 193 + 44 + 88);
                
            }

            
             [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
        
    
        
        self.title =@"Add Note";
        
       
    }
    else{
        
        if (!IS_IPHONE_5) { //Not iPhone5
            //shareButton.frame = CGRectMake(105, 416, 111, 37);
        }
        
        self.navigationController.navigationBarHidden = YES;
        
        //backButton = [[UIButton alloc]init];
        //backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    

}

-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneClicked:(id)sender {
    
	[notesTxtVw resignFirstResponder];
    
	if ([notesTxtVw.text isEqualToString:@""]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Empty Notes..!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
	}
	
	[[self delegate] setNote:notesTxtVw.text];
	[self.navigationController popViewControllerAnimated:YES]; 
}

#pragma mark -
#pragma mark Memory Management
#pragma mark -


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [cancelButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }

}


- (void)viewDidUnload {
    navBar = nil;
    cancelButton = nil;
    doneButton = nil;
    cancelButton = nil;
    doneButton = nil;
    [super viewDidUnload];
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
	textView.text = [textView.text isEqualToString:@"Notes"] ? @"" :textView.text; 
}

- (void)textViewDidEndEditing:(UITextView *)textView {
//	textView.text = [textView.text isEqualToString:@"Notes"] ? @"Notes" :textView.text; 
//
//	textView.text = [textView.text isEqualToString:@""] ? @"Notes" :textView.text; 
	
}


@end


//
//  ManuallyEnterLocationViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationViewController.h"

@interface ManuallyEnterLocationViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate> {
	IBOutlet UITextField *prsnNameTxtFld;
	IBOutlet UITextField *address1TxtFld;
	IBOutlet UITextField *address2TxtFld;
	IBOutlet UITextField *cityTxtFld;
	IBOutlet UITextField *stateTxtFld;
	IBOutlet UITextField *zipTxtFld;
	
	IBOutlet UIScrollView *scrollView;
	BOOL isVerified;
}
-(IBAction)backClicked:(id)sender;
-(IBAction)saveBtnClicked:(id)sender;
-(IBAction)verifyLocation:(id)sender;
@end

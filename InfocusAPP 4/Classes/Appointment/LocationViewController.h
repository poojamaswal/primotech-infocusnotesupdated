//
//  LocationViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentsDataObject.h"
#import "LocationDataObject.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
@interface LocationViewController : UIViewController<ABPeoplePickerNavigationControllerDelegate> {
	NSMutableArray			*locationNameArr;
	//UIImageView				*tickImageView;
	
	//NSInteger				lastLocationID;
   // NSMutableArray *locationArray;
}
-(IBAction)currentLoc_Click:(id)sender;
-(IBAction)contactLoc_Click:(id)sender;
-(IBAction)searchLoc_Click:(id)sender;
-(IBAction)manualLoc_Click:(id)sender;

//@property(nonatomic, retain) UITableView *locationTblVw;
//@property(nonatomic,retain)NSMutableArray *locationArray;
//-(void)getValuesFromDatabase;
-(IBAction)backClicked:(id)sender;
//-(IBAction)addClicked:(id)sender;
//-(void) getAllLocationData ;
//-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withCurrentAppDataObject:(AppointmentsDataObject *) appDataObj;

//-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withLocationID:(NSInteger) loctionID;

-(void)getContacts;
@end

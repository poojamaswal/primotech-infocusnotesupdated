//
//  UseContectsLocationsViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UseContactsLocationsViewController.h"


@interface UseContactsLocationsViewController()
-(void)getContacts;
@end


@implementation UseContactsLocationsViewController

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self getContacts];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark -
#pragma mark All Methods
#pragma mark -
-(void)getContacts{
	// creating the picker
	ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
	// place the delegate of the picker to the controll
	picker.peoplePickerDelegate = self;
	
	// showing the picker
    [self presentViewController:picker animated:YES completion:nil];
	// releasing
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    // assigning control back to the main controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
shouldContinueAfterSelectingPerson:(ABRecordRef)person
property:(ABPropertyID)property
identifier:(ABMultiValueIdentifier)identifier {
	
	// setting the first name
  NSString *firstName = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
	
	// setting the last name
  NSString *  lastName= (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);	
	
		
	// remove the controller
    [self dismissModalViewControllerAnimated:YES];
	
    return NO;
}*/

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
//    NSString *name = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
//    NSString *firstName = name;
//    [name release];
//    name = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
//    NSString *lastName = name;
//    [name release];
    
    
    // Change By naresh ARC Related
   // [self dissmissModalViewControllerAnimated:YES];
    return YES;
}

//- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
//	if (property == kABPersonAddressProperty) {
//        /*
//         * Set up an ABMultiValue to hold the address values; copy from address
//         * book record.
//         */
//        ABMultiValueRef multi = ABRecordCopyValue(person, property);
//        // Set up an NSArray and copy the values in.
//        NSArray *theArray = [(id)ABMultiValueCopyArrayOfAllValues(multi) autorelease];
//        // Figure out which values we want and store the index.
//        const NSUInteger theIndex = ABMultiValueGetIndexForIdentifier(multi, identifier);
//        // Set up an NSDictionary to hold the contents of the array.
//        NSDictionary *theDict = [theArray objectAtIndex:theIndex];
//        // Set up NSStrings to hold keys and values.  First, how many are there?
//        const NSUInteger theCount = [theDict count];
//        
//        NSString *keys[theCount];
//        NSString *values[theCount];
//        // Get the keys and values from the CFDictionary.  Note that because
//        // we're using the "GetKeysAndValues" function, you don't need to
//        // release keys or values.  It's the "Get Rule" and only applies to
//        // CoreFoundation objects.
//        [theDict getObjects:values andKeys:keys];
//        // Set the address label's text.
////        NSString *address;
////        address = [NSString stringWithFormat:@"%@, %@, %@, %@ %@",
////                   [theDict objectForKey:(NSString *)kABPersonAddressStreetKey],
////                   [theDict objectForKey:(NSString *)kABPersonAddressCityKey],
////                   [theDict objectForKey:(NSString *)kABPersonAddressStateKey],
////                   [theDict objectForKey:(NSString *)kABPersonAddressZIPKey],
////                   [theDict objectForKey:(NSString *)kABPersonAddressCountryKey]];
//		// self.addressLabel.text = address;
//	}		
//    return NO;
//}


@end

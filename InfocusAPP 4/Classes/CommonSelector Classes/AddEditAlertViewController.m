//
//  AddEditAlertViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddEditAlertViewController.h"
#import "GetAllDataObjectsClass.h"
#import "AlertDataObject.h"

@interface AddEditAlertViewController()
-(void)addAdditionalControlsInAddEditAlert;
-(void)getValuesFromDatabase;
@end

@implementation AddEditAlertViewController

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withAlertID:(NSInteger ) alertIDLocal {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		lastAlertID = alertIDLocal;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	[self getValuesFromDatabase];
	[self addAdditionalControlsInAddEditAlert];
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)addAdditionalControlsInAddEditAlert{

	for (UIView *subView in [self.navigationController.navigationBar subviews]) {
		[subView removeFromSuperview];
	}
	
	[GlobalUtility setNavBarTitle:@"Alert" forViewController:self];

	UIBarButtonItem *doneBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
	[doneBarBtn setEnabled:NO];
	self.navigationItem.rightBarButtonItem=doneBarBtn;
	[doneBarBtn release];
	
	alertDscTblVw = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
	[alertDscTblVw setDataSource:self];
	[alertDscTblVw setDelegate:self];
	[self.view addSubview:alertDscTblVw];
}

-(void)getValuesFromDatabase {
	if (alertTxtArr) {
		[alertTxtArr release];
		alertTxtArr = nil;
	}
	alertTxtArr = [[GetAllDataObjectsClass getAlertTextData] retain];
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -

/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
 return 5;
 }*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [alertTxtArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	AlertDataObject *alertDataObjectForRows = [[alertTxtArr objectAtIndex:indexPath.row] retain];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	
	if (cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"] autorelease];
	}
	
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	} 
	
	[cell.textLabel setText:[NSString stringWithFormat:@"%@",[alertDataObjectForRows alertText]]];
	
	if (lastAlertID == [alertDataObjectForRows alertID]) {
		selectedAlertIndex = [indexPath row];
		if (tickImageView) {
			[tickImageView removeFromSuperview];
			[tickImageView release];
		}
		UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dark-blue-tick" ofType:@"jpg"]];
		tickImageView=[[UIImageView alloc] initWithImage:checkImage];
		[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
		[cell.contentView addSubview:tickImageView];
		[checkImage release];
	}
	[alertDataObjectForRows release];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (tickImageView) {
		[tickImageView removeFromSuperview];
		[tickImageView release];
	}
	
	UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dark-blue-tick" ofType:@"jpg"]];
	
	tickImageView=[[UIImageView alloc] initWithImage:checkImage];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	[Cell.contentView addSubview:tickImageView];
	[checkImage release];
	
	selectedAlertIndex = [indexPath row];

	[self.navigationItem.rightBarButtonItem setEnabled:YES];	
}

-(IBAction)doneClicked:(id)sender {
	
	NSArray *cntrlrsArr=[self.navigationController viewControllers];
	id parentVC = [cntrlrsArr objectAtIndex:[cntrlrsArr count] - 2];
	[parentVC setAECalAlertDataObject:[alertTxtArr objectAtIndex:selectedAlertIndex]];
	
	[[parentVC alertDescrpLbl] setTag:0];
	
	[self.navigationController popViewControllerAnimated:YES]; 
	
}

#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[alertDscTblVw release];
	if (alertTxtArr) [alertTxtArr release];	
	[alertTextString release];
	[tickImageView release];
}

@end

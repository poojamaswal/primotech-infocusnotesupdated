//
//  AddCustomRepeatViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddCustomRepeatViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate> {
	UITextField *yearTxtFld;
	UITextField *monthTxtFld;
	UITextField *weekTxtFld;
	UITextField *dayTxtFld;
	UITextField *hourTxtFld;
	UITextField *minuteTxtFld;
}

@end

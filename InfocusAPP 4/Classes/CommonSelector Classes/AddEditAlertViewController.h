//
//  AddEditAlertViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditCalendarViewController.h"
#import "AppointmentsDataObject.h"

@interface AddEditAlertViewController : UIViewController<UITableViewDataSource,UITableViewDelegate> {
	UITableView		*alertDscTblVw;
	NSString		*alertTextString;
	UIImageView		*tickImageView;
	
	NSArray			*alertTxtArr;
	NSInteger		lastAlertID;
	NSInteger		selectedAlertIndex;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withAlertID:(NSInteger ) alertIDLocal;


@end

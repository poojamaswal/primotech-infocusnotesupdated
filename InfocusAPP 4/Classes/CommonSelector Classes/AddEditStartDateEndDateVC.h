//
//  AddEditStartDateEndDateVC.h
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditCalendarViewController.h"
#import "AddEditToDoViewController.h"
#import "AppointmentsDataObject.h"
#import "TODODataObject.h"

@interface AddEditStartDateEndDateVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate> {

	UITableView		*strtEndTblVw;
	UIDatePicker	*datePicker;
	UISwitch		*allDaySwitch;
	UILabel			*strtDateLbl;
	UILabel			*endDateLbl;
	
	NSString		*strtDateString;
	NSString		*endDateString;
	
	NSInteger		selectedCellIndex;

	AppointmentsDataObject	*currentAppDataObject;
	TODODataObject			*currentToDoDataObject;
}

@property(nonatomic, retain) AppointmentsDataObject *currentAppDataObject; 
@property(nonatomic, retain) TODODataObject *currentToDoDataObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withCurrentAppDataObject:(AppointmentsDataObject *) appDataObj;

-(id)initWithStartDateString:(NSString*) startDateStr endDateStr:(NSString*) endDateStr;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withCurrentToDoDataObject:(TODODataObject *) toDoDataObj;


@end

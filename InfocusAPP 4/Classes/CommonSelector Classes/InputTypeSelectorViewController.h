//
//  InputTypeSelectorViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputTypeSelectorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
	UITableView *tblInputTypeSelection;
	
	NSString *selectedInputType;
	CGRect contentFrame; 
	NSString *arrowDirection;
	CGPoint arrowStartPoint;
	id parentVC;
	
	BOOL isFromMainScreen;
    
    BOOL isPenIcon; //Steve
	

}
@property(nonatomic, strong) id parentVC;
@property(nonatomic, strong) UITableView *tblInputTypeSelection; 
@property(nonatomic, strong) NSString *selectedInputType;
@property(nonatomic, assign) CGRect contentFrame; 
@property(nonatomic, strong) NSString *arrowDirection;
@property(nonatomic, assign) CGPoint arrowStartPoint;

-(id)initWithContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal andParentVC:(id) parentLocal fromMainScreen:(BOOL) mainScreen;

@end

//
//  SortingOptionsViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SortingOptionsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	UITableView *sortingComponentTable;
	id parentVC;

	CGRect contentFrame; 
	NSString *arrowDirection;
	CGPoint arrowStartPoint;
}
@property (nonatomic, retain) UITableView *sortingComponentTable;
@property (nonatomic, retain) id parentVC;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal andParentVC:(id) calendarParentLocal fromCalenderMainScreen:(BOOL) calMainScreen;


@end

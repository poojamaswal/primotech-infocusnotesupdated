//
//  AddCustomRepeatViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddCustomRepeatViewController.h"
#import "AllInsertDataMethods.h"

@interface AddCustomRepeatViewController()
-(void)addAdditionalControlsInCustumRepeat;
-(void)adjustControls;
@end

@implementation AddCustomRepeatViewController

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title =@"Add Repeat";
	[self addAdditionalControlsInCustumRepeat];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark -
#pragma mark Private Methods
#pragma mark -
- (void)addAdditionalControlsInCustumRepeat{
	UILabel *repeatInLbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 200, 31)];
	[repeatInLbl setText:@"Repeat in:"];
	[repeatInLbl setBackgroundColor:[UIColor clearColor]];
	[self.view addSubview:repeatInLbl];
	
	UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 41, self.view.frame.size.width, self.view.frame.size.height)];
	[scrollView setDelegate:self];
	[scrollView setScrollEnabled:YES];
	[scrollView setUserInteractionEnabled:YES];
	[scrollView setContentSize:CGSizeMake(320, 600)];
	
	yearTxtFld = [[UITextField alloc] initWithFrame:CGRectMake(30,10, 270, 31)];
	[yearTxtFld setPlaceholder:@"Years"];
	[yearTxtFld setDelegate:self];
	[yearTxtFld setBackgroundColor:[UIColor clearColor]];
	[yearTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[yearTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:yearTxtFld];
	
	monthTxtFld=[[UITextField alloc] initWithFrame:CGRectMake(30,yearTxtFld.frame.size.height+20, 270, 31)];
	[monthTxtFld setPlaceholder:@"Month"];
	[monthTxtFld setDelegate:self];
	[monthTxtFld setBackgroundColor:[UIColor clearColor]];
	[monthTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[monthTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:monthTxtFld];
	
	weekTxtFld=[[UITextField alloc] initWithFrame:CGRectMake(30,yearTxtFld.frame.size.height+monthTxtFld.frame.size.height+30, 270, 31)];
	[weekTxtFld setPlaceholder:@"Week"];
	[weekTxtFld setDelegate:self];
	[weekTxtFld setBackgroundColor:[UIColor clearColor]];
	[weekTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[weekTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:weekTxtFld];
	
	dayTxtFld=[[UITextField alloc] initWithFrame:CGRectMake(30,yearTxtFld.frame.size.height+monthTxtFld.frame.size.height+weekTxtFld.frame.size.height+40, 270, 31)];
	[dayTxtFld setPlaceholder:@"Day"];
	[dayTxtFld setDelegate:self];
	[dayTxtFld setBackgroundColor:[UIColor clearColor]];
	[dayTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[dayTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:dayTxtFld];
	
	hourTxtFld=[[UITextField alloc] initWithFrame:CGRectMake(30,yearTxtFld.frame.size.height+monthTxtFld.frame.size.height+weekTxtFld.frame.size.height+dayTxtFld.frame.size.height+50, 270, 31)];
	[hourTxtFld setPlaceholder:@"Hour"];
	[hourTxtFld setDelegate:self];
	[hourTxtFld setBackgroundColor:[UIColor clearColor]];
	[hourTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[hourTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:hourTxtFld];
	
	minuteTxtFld = [[UITextField alloc] initWithFrame:CGRectMake(30,yearTxtFld.frame.size.height+monthTxtFld.frame.size.height+weekTxtFld.frame.size.height+dayTxtFld.frame.size.height+hourTxtFld.frame.size.height+60, 270, 31)];
	[minuteTxtFld setPlaceholder:@"Minute"];
	[minuteTxtFld setDelegate:self];
	[minuteTxtFld setBackgroundColor:[UIColor clearColor]];
	[minuteTxtFld setBorderStyle:UITextBorderStyleRoundedRect];
	[minuteTxtFld setTextAlignment:UITextAlignmentCenter];
	[scrollView addSubview:minuteTxtFld];
	
	UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[saveBtn setTitle:@"Save" forState:UIControlStateNormal];
	[saveBtn addTarget:self action:@selector(saveClicked:) forControlEvents:UIControlEventTouchUpInside];
	[saveBtn setFrame:CGRectMake(85, yearTxtFld.frame.size.height+monthTxtFld.frame.size.height+weekTxtFld.frame.size.height+dayTxtFld.frame.size.height+hourTxtFld.frame.size.height+dayTxtFld.frame.size.height+80, 150, 31)];
	[scrollView addSubview:saveBtn];
	
	[self.view addSubview:scrollView];
	[scrollView release];
	[repeatInLbl release];
} 

-(void)adjustControls{
}

#pragma mark -
#pragma mark All Methods
#pragma mark -
-(void)saveClicked:(id)sender{
	int year = 0,month = 0, week = 0, day = 0, hour = 0, minute = 0, finalProd = 0;
	NSString *repeatTitleString=@"Repeat every";
	if (![yearTxtFld.text isEqualToString:@""]){
		year=[[yearTxtFld text] intValue]*365*24*60;
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@year",yearTxtFld.text]];
	}
	if (![monthTxtFld.text isEqualToString:@""]){
		month=[[monthTxtFld text] intValue]*30*24*60;
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@month",monthTxtFld.text]];
	}
	if (![weekTxtFld.text isEqualToString:@""]) {
		week=[[weekTxtFld text] intValue]*7*24*60;
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@week",weekTxtFld.text]];
	}
	if (![dayTxtFld.text isEqualToString:@""]) {
		day=[[dayTxtFld text] intValue]*24*60;
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@days",dayTxtFld.text]];
	}
	if (![hourTxtFld.text isEqualToString:@""]) {
		hour=[[hourTxtFld text] intValue]*60;
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@hours",hourTxtFld.text]];
	}
	if (![minuteTxtFld.text isEqualToString:@""]) {
		minute = [[minuteTxtFld text] intValue];
		repeatTitleString=[repeatTitleString stringByAppendingString:[NSString stringWithFormat:@" %@minutes",minuteTxtFld.text]];
	}
	
	finalProd = year + month + week + day + hour + day;
	if (finalProd!=0) {
		NSString *prodString=[NSString stringWithFormat:@"%d",finalProd];
		
		NSMutableArray *tempCustomRepeatDataVal=[[NSMutableArray alloc] init];
		[tempCustomRepeatDataVal addObject:prodString];
		[tempCustomRepeatDataVal addObject:@"1"];
		[tempCustomRepeatDataVal addObject:repeatTitleString];
		
		BOOL isInserted = [AllInsertDataMethods insertCustomRepeatDataValues:tempCustomRepeatDataVal];
		[tempCustomRepeatDataVal release];
		
		if (isInserted==YES) {
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
	else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please input values in atleast one field to save a custom repeat." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}

}

#pragma mark -
#pragma mark TextField delegate Methods
#pragma mark -
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	if (textField == yearTxtFld) {
		[yearTxtFld resignFirstResponder];
		[monthTxtFld becomeFirstResponder];
	}
	
	else if (textField == monthTxtFld) {
		[monthTxtFld resignFirstResponder];
		[weekTxtFld becomeFirstResponder];
	}
	
	else if (textField == weekTxtFld) {
		[weekTxtFld resignFirstResponder];
		[dayTxtFld becomeFirstResponder];
	}
	
	else if (textField == dayTxtFld) {
		[dayTxtFld resignFirstResponder];
		[hourTxtFld becomeFirstResponder];
	}
	
	else if (textField == hourTxtFld) {
		[hourTxtFld resignFirstResponder];
		[minuteTxtFld becomeFirstResponder];
	}
	else {
		[textField resignFirstResponder];
	}
	return YES;
}


#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
	[yearTxtFld release];
	[monthTxtFld release];
	[weekTxtFld release];
	[dayTxtFld release];
	[hourTxtFld release];
	[minuteTxtFld release];
}


@end

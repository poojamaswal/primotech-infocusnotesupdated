//
//  AddEditStartDateEndDateVC.m
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddEditStartDateEndDateVC.h"

@interface AddEditStartDateEndDateVC()
-(void)addAdditionalControlsInAddEditStartEndDate;
-(void)setDatelabels;
@end

@implementation AddEditStartDateEndDateVC
@synthesize currentAppDataObject,currentToDoDataObject;

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withCurrentAppDataObject:(AppointmentsDataObject *) appDataObj {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.currentAppDataObject =  appDataObj;
    }
    return self;
}

-(id)initWithStartDateString:(NSString*) startDateStr endDateStr:(NSString*) endDateStr {
    self = [super initWithNibName:@"AddEditStartDateEndDateVC" bundle:[NSBundle mainBundle]];
    if (self) {
		strtDateString = [startDateStr copy];
		endDateString  = [endDateStr copy];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withCurrentToDoDataObject:(TODODataObject *) toDoDataObj{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.currentToDoDataObject =  toDoDataObj;
		
		if ([self.currentAppDataObject startDateTime]) {
			NSDate *startDate;
			NSString *dateStr;
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			if (strtDateString) {
				[strtDateString release];
				strtDateString = nil;
			}
			strtDateString = [[self.currentAppDataObject startDateTime] copy];
			[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			startDate = [[dateFormatter dateFromString:strtDateString] retain];
			[datePicker setDate:startDate animated:YES];
			
			[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
			[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
			dateStr = [dateFormatter stringFromDate:startDate];
			[strtDateLbl setText:dateStr];
			
			[dateFormatter release];
			[startDate release];
		}
		
		if ([self.currentAppDataObject dueDateTime]) {
			NSDate *endDate;
			NSString *dateStr;
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			if (endDateString) {
				[endDateString release];
				endDateString = nil;
			}
			endDateString = [[self.currentAppDataObject dueDateTime] copy];
			[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			endDate = [[dateFormatter dateFromString:endDateString] retain];
			[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
			[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
			dateStr = [dateFormatter stringFromDate:endDate];
			[endDateLbl setText:dateStr];
			[dateFormatter release];
			[endDate release];
		}
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self addAdditionalControlsInAddEditStartEndDate];
	[self setDatelabels];
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)setDatelabels { 
	if (strtDateString) {
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSDate *startDate = [dateFormatter dateFromString:strtDateString];
		[datePicker setDate:startDate animated:YES];
		
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		NSString *dateStr = [dateFormatter stringFromDate:startDate];
		[strtDateLbl setText:dateStr];
		
		[dateFormatter release];
	}
	
	if (endDateString) {
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSDate *endDate = [dateFormatter dateFromString:endDateString];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		NSString *dateStr = [dateFormatter stringFromDate:endDate];
		[endDateLbl setText:dateStr];
		[dateFormatter release];
	}
}

-(void)addAdditionalControlsInAddEditStartEndDate {
	[GlobalUtility setNavBarTitle:@"Start & End" forViewController:self];

	[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];

	UIImage *backButtonImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"back-btn" ofType:@"png"]];
	UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
	[backBtn setFrame:CGRectMake(0, 5, 52, 32)];
	[backBtn setImage:backButtonImage forState:UIControlStateNormal];
	[backBtn addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *cancelBarBtn=[[UIBarButtonItem alloc] initWithCustomView:backBtn];
	self.navigationItem.leftBarButtonItem=cancelBarBtn;
	[cancelBarBtn release];
	[backButtonImage release];
	
	UIBarButtonItem *doneBarBtn=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
	self.navigationItem.rightBarButtonItem = doneBarBtn;
	[doneBarBtn release];
	
	strtEndTblVw = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.navigationController.navigationBar.frame.size.width, 150) style:UITableViewStyleGrouped];
	[strtEndTblVw setDataSource:self];
	[strtEndTblVw setDelegate:self];
	[self.view addSubview:strtEndTblVw];

	strtDateLbl = [[UILabel alloc] init];
	[strtDateLbl setTextColor:[UIColor blueColor]];
	[strtDateLbl setTextAlignment:UITextAlignmentRight];
	[strtDateLbl setBackgroundColor:[UIColor clearColor]];
	
	endDateLbl = [[UILabel alloc] init];
	[endDateLbl setTextColor:[UIColor blueColor]];
	[endDateLbl setTextAlignment:UITextAlignmentRight];
	[endDateLbl setBackgroundColor:[UIColor clearColor]];

	datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+strtEndTblVw.frame.size.height+30, self.view.frame.size.width, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-strtEndTblVw.frame.size.height-20)];
	[datePicker setDate:[NSDate date] animated:YES];
	
	[datePicker addTarget:self action:@selector(updateDateLbl:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:datePicker];
	
	allDaySwitch = [[UISwitch alloc] init];
	[allDaySwitch addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventTouchUpInside];
	
	NSDate *date = datePicker.date;
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	if (!strtDateString) {
		strtDateString  = [[dateFormatter stringFromDate:date] retain];
	}

	[dateFormatter setDateFormat:@"MMM dd, yyyy H:mm a"];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	NSString *strtDateStr = [dateFormatter stringFromDate:date];
	[strtDateLbl setText:strtDateStr];
	[datePicker setDate:date animated:YES];
	
	NSTimeInterval secondsInAHour = 1 * 60 * 60;
	NSDate *dateEightHoursAhead = [date dateByAddingTimeInterval:secondsInAHour];
	
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	if (!endDateString) {
		endDateString  = [[dateFormatter stringFromDate:dateEightHoursAhead] retain];
	}

	[dateFormatter setDateFormat:@"MMM dd, yyyy H:mm a"];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	
	NSString *endDateStr = [dateFormatter stringFromDate:dateEightHoursAhead];
	[endDateLbl setText:endDateStr];
	
	[dateFormatter release];
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	if (cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"] autorelease];
	}
	
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	} 
	
	if (indexPath.row == 0) {

		UILabel *startLbl=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 60, 31)];
		[startLbl setBackgroundColor:[UIColor clearColor]];
		[startLbl setText:@"Starts"];
		[startLbl setTextColor:[UIColor blackColor]];
		[cell.contentView addSubview:startLbl];
		
		//Adding Description Labels		
		[strtDateLbl setFrame:CGRectMake(cell.frame.origin.x+startLbl.frame.size.width+30, 10,cell.frame.size.width - startLbl.frame.size.width-70, 31)];
		
		[cell.contentView addSubview:strtDateLbl];
		
		[startLbl release];
	}
	
	if (indexPath.row==1) {
		
		UILabel *endsLbl=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 60, 31)];
		[endsLbl setBackgroundColor:[UIColor clearColor]];
		[endsLbl setText:@"Ends"];
		[endsLbl setTextColor:[UIColor blackColor]];
		[cell.contentView addSubview:endsLbl];
		
		[endDateLbl setFrame:CGRectMake(cell.frame.origin.x+endsLbl.frame.size.width+30, 10,cell.frame.size.width-endsLbl.frame.size.width-70, 31)];
		[cell.contentView addSubview:endDateLbl];
		
		[endsLbl release];
	}
	if (indexPath.row==2) {
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		UILabel *allDayLbl=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 60, 31)];
		[allDayLbl setBackgroundColor:[UIColor clearColor]];
		[allDayLbl setText:@"All-day"];
		[allDayLbl setTextColor:[UIColor blackColor]];
		[cell.contentView addSubview:allDayLbl];
		
		[allDaySwitch setFrame:CGRectMake(cell.frame.size.width-allDayLbl.frame.size.width-70, 10, 100, 31)];
		
		[cell.contentView addSubview:allDaySwitch];
		[allDayLbl release];
		
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	selectedCellIndex = indexPath.row;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MMM dd, yyyy H:mm a"];
	
	if (indexPath.row == 0) {
		NSDate *startDate = [[formatter dateFromString:strtDateLbl.text] retain];
		[datePicker setDate:startDate animated:YES];
		[startDate release];
	}
	
	if (indexPath.row==1) {
		if (![endDateLbl.text isEqualToString:@""] && endDateLbl.text != nil) {
			NSDate *endDate = [[formatter dateFromString:endDateLbl.text] retain];
			[datePicker setDate:endDate animated:YES];
			[endDate release];
		}
	}
	[formatter release];
}

#pragma mark -
#pragma mark Bar Button Methods
#pragma mark -

-(IBAction)doneClicked:(id)sender {
	if ([strtDateLbl.text isEqualToString:@"Start Date"]||[endDateLbl.text isEqualToString:@"End Date"]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please input both of the dates." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	} else {
		NSDateFormatter *formater = [[NSDateFormatter alloc] init];
		
		// Add Other var Saving States.
		[formater setDateFormat:@"MMM dd, yyyy H:mm a"];
		NSDate *tempStartDate = [[formater dateFromString:strtDateLbl.text] retain];
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSString *tempStartStr = [[formater stringFromDate:tempStartDate] retain];
	
		[formater setDateFormat:@"MMM dd, yyyy H:mm a"];
		NSDate *tempDueDate = [[formater dateFromString:endDateLbl.text] retain];
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSString *tempDueStr = [[formater stringFromDate:tempDueDate] retain];
		
		NSComparisonResult result = [tempStartDate compare:tempDueDate];
		if (result == NSOrderedDescending) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"End date can't be a date before Starting Date. Please enter a date greater than starting date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];

			[formater release];
			[tempDueStr release];
			[tempDueDate release];
			[tempStartDate release];
			[tempStartStr release];
			return;
		} else {
			NSArray *cntrlrsArr = [[self.navigationController viewControllers] retain];

			id parentVC = [cntrlrsArr objectAtIndex:([cntrlrsArr count] - 2)];
			[parentVC setStartDateString:strtDateString];
			[parentVC setEndDateString:endDateString];
			
			[self.navigationController popViewControllerAnimated:YES];
			[cntrlrsArr release];
		}
		[formater release];
		[tempDueStr release];
		[tempDueDate release];
		[tempStartDate release];
		[tempStartStr release];
	}
}

-(IBAction)cancelClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Switch Button Method
#pragma mark -

-(void)valueChanged {
	NSDate *date = [datePicker.date retain];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSString *dateStr = [[dateFormatter stringFromDate:date] retain];
	
	if ([allDaySwitch isOn]) {
		[datePicker setDatePickerMode:UIDatePickerModeDate];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		if (dateStr) {
			[dateStr release];
			dateStr = nil;
		}
		dateStr = [[dateFormatter stringFromDate:date] retain];
	}else {
		[datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		if (dateStr) {
			[dateStr release];
			dateStr = nil;
		}
		dateStr = [[dateFormatter stringFromDate:date] retain];
	}

	switch (selectedCellIndex) {
		case 0:
			[strtDateLbl setText:dateStr];
			if (strtDateString) {
				[strtDateString release];
				strtDateString = nil;
			}
			strtDateString = [[NSString stringWithFormat:@"%@",datePicker.date] retain];
			break;
		case 1:
			[endDateLbl setText:dateStr];
			if (endDateString) {
				[endDateString release];
				endDateString = nil;
			}
			endDateString = [[NSString stringWithFormat:@"%@",datePicker.date] retain];
			break;

		default:
			break;
	}
	[dateFormatter release];
	[date release];
	[dateStr release];
}

#pragma mark -
#pragma mark Other Methods
#pragma mark -

-(IBAction)updateDateLbl:(NSInteger) withIndex {
	NSString *dateStr;
	NSDate *date = [datePicker.date retain];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	if ([allDaySwitch isOn] == YES) {
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	} else {
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	}

	dateStr = [[dateFormatter stringFromDate:date] retain];
	
	if (selectedCellIndex == 0) {
		NSDate *endDate = [[dateFormatter dateFromString:endDateLbl.text] retain];
		[strtDateLbl setText:dateStr];
		NSDate *pickerDate = [datePicker.date retain];
		[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		
		if (strtDateString) {
			[strtDateString release];
			strtDateString = nil;
		}
		strtDateString  = [[dateFormatter stringFromDate:pickerDate] retain];
		[endDate release];
		[pickerDate release];
	} else {
		NSDate *strtDate = [[dateFormatter dateFromString:strtDateLbl.text] retain];
		[endDateLbl setText:dateStr];
		NSDate *pickerDate = [datePicker.date retain];
		[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];

		if (endDateString) {
			 [endDateString release];
			 endDateString = nil;
		}
		endDateString  = [[dateFormatter stringFromDate:pickerDate] retain];
		[strtDate release];
		[pickerDate release];
	}
	
	[dateStr release];
	[dateFormatter release];
	[date release];
}

#pragma mark -
#pragma mark Memory Management Methods
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[datePicker release];
	[strtEndTblVw release];
	[strtDateLbl release];
	[allDaySwitch release];
	[endDateLbl release];
	[endDateString release];
	[strtDateString release];
	if (currentAppDataObject) [currentAppDataObject release];	
	if (currentToDoDataObject) [currentToDoDataObject release];
}

@end

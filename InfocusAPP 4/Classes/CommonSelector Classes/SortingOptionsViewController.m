//
//  SortingOptionsViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SortingOptionsViewController.h"


@interface SortingOptionsViewController()
-(void)addAdditionalControlsInSortingOptions;
@end


@implementation SortingOptionsViewController
@synthesize sortingComponentTable, parentVC;

-(id)initWithContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal andParentVC:(id) parentLocal fromCalenderMainScreen:(BOOL) calMainScreen {
    self = [super initWithNibName:@"SortingOptionsViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        parentVC = [parentLocal retain];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self addAdditionalControlsInSortingOptions];
}

#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	//	UITouch *touch = [touches anyObject];
	if (![touches containsObject:sortingComponentTable]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
		[self.view  setAlpha:0];
		[UIView commitAnimations];
		[self performSelector:@selector(removeView) withObject:nil afterDelay:0.3];
	}
}

-(void)removeView {
	[self.view removeFromSuperview];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}

#pragma mark -
#pragma mark - Private Methods 
#pragma mark -

-(void)addAdditionalControlsInSortingOptions {
	CGFloat frameX = contentFrame.origin.x;
	CGFloat frameY = contentFrame.origin.y; 
	CGFloat arrowPointX = arrowStartPoint.x;
	CGFloat arrowPointY = arrowStartPoint.y; 
	
	[self.view setBackgroundColor:[UIColor clearColor]];
	NSString *arrowImageName, *bgImageName;
	
	if ([arrowDirection isEqualToString:@"D"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowDown"];
		bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_Down"];  
	}else if ([arrowDirection isEqualToString:@"U"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowUp"];
		bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_UP"];  
	}else if ([arrowDirection isEqualToString:@"R"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowRight"];
	}else if ([arrowDirection isEqualToString:@"L"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowLeft"];
	}
	
	UIImage *tabelViewBgImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:bgImageName ofType:@"png"]];
	UIImageView *tabelViewBgImageView = [[UIImageView alloc] initWithImage:tabelViewBgImage];
	
	[tabelViewBgImageView  setFrame:CGRectMake(frameX, frameY, tabelViewBgImage.size.width, tabelViewBgImage.size.height)];
	
	sortingComponentTable = [[UITableView alloc] initWithFrame:CGRectMake(frameX, frameY, tabelViewBgImage.size.width, tabelViewBgImage.size.height) style:UITableViewStylePlain];
	
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(15, 5, 0, 0);
	[sortingComponentTable setContentInset:contentInsets];
	[sortingComponentTable setBackgroundColor:[UIColor lightGrayColor]];
//	[sortingComponentTable setBackgroundView:tabelViewBgImageView];
	[sortingComponentTable setScrollEnabled:NO];
	[sortingComponentTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[sortingComponentTable setDelegate:self];
	[sortingComponentTable setDataSource:self];
	[self.view addSubview:sortingComponentTable];
	
	
	UIImage *downArrowImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:arrowImageName ofType:@"png"]];
	
	UIImageView *downArrowImageView = [[UIImageView alloc] initWithImage:downArrowImage];
	[downArrowImageView  setFrame:CGRectMake(arrowPointX, arrowPointY, downArrowImage.size.width, downArrowImage.size.height)];
	[self.view addSubview:downArrowImageView];
	
	[arrowImageName release]; 
	[bgImageName release];
	[tabelViewBgImage release];
	[downArrowImage release];
	[downArrowImageView release];
	[tabelViewBgImageView release];
}

#pragma mark -
#pragma mark Tableview  Methods
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSInteger row;
	row = [indexPath row];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewcell"];
	
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35) reuseIdentifier:@"TableViewcell"] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone]; 
	}
	
	for ( UIView *subViews in [cell.contentView subviews]) {
		[subViews removeFromSuperview];
	}
	
	UILabel *lblCellText = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 100, 30)];
	[lblCellText setBackgroundColor:[UIColor clearColor]];
	[lblCellText setFont:[UIFont boldSystemFontOfSize:15]];
	[lblCellText setTextColor:[UIColor blackColor]];
	
	[cell.contentView addSubview:lblCellText];

	return cell;
}

#pragma mark -
#pragma mark MemoryManagement  Methods
#pragma mark -


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[sortingComponentTable release];
	[parentVC release];
}


@end

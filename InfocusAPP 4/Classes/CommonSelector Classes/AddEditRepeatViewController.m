//
//  AddEditRepeatViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddEditRepeatViewController.h"
#import "GetAllDataObjectsClass.h"
#import "RepeatDataObject.h"
#import "AddCustomRepeatViewController.h"

@interface AddEditRepeatViewController()
-(void)addAdditionalControlsInAddEditRepeat;
-(void)getValuesFromDatabase;
@end

@implementation AddEditRepeatViewController

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withRepeatID:(NSInteger) repeatIDLocal {
    self = [super initWithNibName:@"AddEditRepeatViewController" bundle:[NSBundle mainBundle]];
    if (self) {
		lastRepeatID = repeatIDLocal;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self getValuesFromDatabase];
	[self addAdditionalControlsInAddEditRepeat];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
	[[self.navigationController.navigationBar viewWithTag:12012] removeFromSuperview];
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)addAdditionalControlsInAddEditRepeat {
	[GlobalUtility setNavBarTitle:@"Repeat" forViewController:self];
	
	UIButton *backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
	[backBtn setFrame:CGRectMake(0, 5, 52, 32)];
	[backBtn setImage:[UIImage imageNamed:@"back-btn.png"] forState:UIControlStateNormal];
	[backBtn addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *cancelBarBtn=[[UIBarButtonItem alloc] initWithCustomView:backBtn];
	self.navigationItem.leftBarButtonItem=cancelBarBtn;
	[cancelBarBtn release];

	UIBarButtonItem *doneBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
	[doneBarBtn setEnabled:NO];
	self.navigationItem.rightBarButtonItem=doneBarBtn;
	[doneBarBtn release];
	
	UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[addBtn setTag:12012];
	[addBtn setFrame:CGRectMake(self.navigationController.navigationBar.frame.size.width-95, 5, 36, 31)];
	[addBtn setBackgroundImage:[UIImage imageNamed:@"header-1-icon.png"] forState:UIControlStateNormal];
	[addBtn setBackgroundColor: [UIColor clearColor]];
	[addBtn addTarget:self action:@selector(addCustomRepeat) forControlEvents:UIControlEventTouchUpInside];
	[self.navigationController.navigationBar addSubview:addBtn];
	

	repeatDetailTblVw = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
	[repeatDetailTblVw setDataSource:self];
	[repeatDetailTblVw setDelegate:self];
	[self.view addSubview:repeatDetailTblVw];
}

-(void)getValuesFromDatabase {
	repeatTimeArr = [[GetAllDataObjectsClass getRepeatTextData] retain];
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [repeatTimeArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	RepeatDataObject *repeatDataObjectForRow = [[repeatTimeArr objectAtIndex:indexPath.row] retain];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	if (cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"] autorelease];
	}
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	} 
	 
	NSString *cellTextStr = [[NSString alloc] initWithFormat:@"%@",[repeatDataObjectForRow repeatText]];
	[cell.textLabel setText:cellTextStr];
	[cellTextStr release];
	
	if (lastRepeatID == [repeatDataObjectForRow repeatID]) {
		if (tickImageView) {
			[tickImageView removeFromSuperview];
			[tickImageView release];
		}
		
		selectedObjIndex = [indexPath row];
		
		UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dark-blue-tick" ofType:@"jpg"]];
		tickImageView = [[UIImageView alloc] initWithImage:checkImage];
		[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
		[checkImage release];
		[cell.contentView addSubview:tickImageView];
	}
	[repeatDataObjectForRow release];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	if (tickImageView) {
		[tickImageView removeFromSuperview];
		[tickImageView release];
	}

	UIImage *checkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dark-blue-tick" ofType:@"jpg"]];
	tickImageView = [[UIImageView alloc] initWithImage:checkImage];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	[checkImage release];
	[Cell.contentView addSubview:tickImageView];
	
	selectedObjIndex = [indexPath row];
	
	[self.navigationItem.rightBarButtonItem setEnabled:YES];
}

#pragma mark -
#pragma mark Bar Button Methods
#pragma mark -

-(IBAction)doneClicked:(id)sender {
	// Add Other var Saving States.
	NSArray *cntrlrsArr = [[self.navigationController viewControllers] retain];
	
	AddEditCalendarViewController *tempVwCntrlr = (AddEditCalendarViewController*)[[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2] retain];

	[tempVwCntrlr.repeatDescrpLbl setTag:0];
	[tempVwCntrlr setAECalRepeatDataObject:[repeatTimeArr objectAtIndex:selectedObjIndex]];
	
	[tempVwCntrlr release];
	[cntrlrsArr release];
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)cancelClicked:(id)sender{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)addCustomRepeat {
	// Implement business logic to add CustomCell here..
	AddCustomRepeatViewController *addCustomRepeatViewController=[[AddCustomRepeatViewController alloc] initWithNibName:@"AddCustomRepeatViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:addCustomRepeatViewController animated:YES];
	[addCustomRepeatViewController release];
}

#pragma mark -
#pragma mark Memory Management 
#pragma mark -
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
	[tickImageView release];
 	[repeatDetailTblVw release];
	[repeatDescString release];
	[repeatTimeArr release];
} 

@end

//
//  SearchBarViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SearchBarViewController : UIViewController <UISearchBarDelegate> {
	UIView *searchbarContainerView;
	UISearchBar *searchAppBar;
	CGRect contentFrame; 
	NSString *arrowDirection;
	CGPoint arrowStartPoint;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal;

@property(nonatomic, retain) UIView *searchbarContainerView;
@property(nonatomic, retain) UISearchBar *searchAppBar; 
@property(nonatomic, assign) CGRect contentFrame; 
@property(nonatomic, retain) NSString *arrowDirection;
@property(nonatomic, assign) CGPoint arrowStartPoint;

@end

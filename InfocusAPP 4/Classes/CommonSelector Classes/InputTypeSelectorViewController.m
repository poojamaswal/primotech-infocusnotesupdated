//
//  InputTypeSelectorViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//
//  InputTypeSelectorViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InputTypeSelectorViewController.h"
#import "OrganizerAppDelegate.h"
#import "HandWritingViewController.h"
#import "VoiceToTextViewController.h"
#import "AddEditTitleViewController.h"
#import "AppointmentViewController .h"
#import "ToDoViewController.h"
#import "ToDoParentController.h"
#import "iSpeechViewControllerVTT.h"
#import "ListToDoViewController.h"

//#import "AddNewNotesViewCantroller.h"
//#import "NotesSingleViewCantroller.h"
//#import "NotesViewCantroller.h"

@interface InputTypeSelectorViewController()
-(void)addAdditionalControlsInInputType;
@end

@implementation InputTypeSelectorViewController
@synthesize tblInputTypeSelection;
@synthesize selectedInputType;
@synthesize contentFrame, arrowDirection, arrowStartPoint;
@synthesize parentVC;
//@synthesize disclosureTableView;  //steve added

#pragma mark -
#pragma mark - ViewController Life Cycle
#pragma mark -

-(id)initWithContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal andParentVC:(id) parentLocal fromMainScreen:(BOOL) mainScreen 
{
	if ((self = [super init])) 
	{
		self.parentVC = parentLocal;
		contentFrame = frame;	
		self.arrowDirection  = arrowDirectionLocal;
		arrowStartPoint = arrowStartPointLocal;
		isFromMainScreen = mainScreen;
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self addAdditionalControlsInInputType];
}

#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	if (![touches containsObject:tblInputTypeSelection]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
		[self.view  setAlpha:0];
		[UIView commitAnimations];
		[self performSelector:@selector(removeView) withObject:nil afterDelay:0.3];
	}
}

-(void)removeView {
	[self.view removeFromSuperview];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}

#pragma mark -
#pragma mark - Private Methods 
#pragma mark -

-(void)addAdditionalControlsInInputType {
	CGFloat frameX = contentFrame.origin.x;
	CGFloat frameY = contentFrame.origin.y; 
	CGFloat arrowPointX = arrowStartPoint.x;
	CGFloat arrowPointY = arrowStartPoint.y; 
	
	[self.view setBackgroundColor:[UIColor clearColor]];
	NSString *arrowImageName, *bgImageName;
    
    
	if ([arrowDirection isEqualToString:@"D"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowDown"];
        
        
        
		if ([self.parentVC isKindOfClass:[AppointmentViewController class]] || [self.parentVC isKindOfClass:[ToDoViewController class]])
		{
			
            bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_Down1"];
        }
       
		else {
            
            bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_Down"];
            /*   
             OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
             
             if(appDelegate.isVTTDevice ==YES){
             }
             else
             {
             NSLog(@"Parentclass->%@", [self.parentVC class]);
             ToDoViewController *todoViewController = [[ToDoViewController alloc] initWithNibName:@"ToDoViewController" bundle:[NSBundle mainBundle]];
             [todoViewController setTodoTitleType:@"T"];
             todoViewController.isAdd = YES;
             [appDelegate.navigationController pushViewController:todoViewController animated:NO];
             selectedInputType = @"H";
             HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:todoViewController];		
             [appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
             }   
             
             */
		}
        
	}else if ([arrowDirection isEqualToString:@"U"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowUp"];
		if ([self.parentVC isKindOfClass:[AppointmentViewController class]] || [self.parentVC isKindOfClass:[ToDoViewController class]])
		{
			bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_UP1"];
		}
		else {
            // bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_UP"]; //Steve commented
			bgImageName = [[NSString alloc] initWithString:@"InputSelectionImage_UP1"]; //Steve Added - All popups the same
		}
        
		
        
	}else if ([arrowDirection isEqualToString:@"R"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowRight"];
	}else if ([arrowDirection isEqualToString:@"L"]) {
		arrowImageName = [[NSString alloc] initWithString:@"PopOverArrowLeft"];
	}
	
	UIImage *tabelViewBgImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:bgImageName ofType:@"png"]];
	UIImageView *tabelViewBgImageView = [[UIImageView alloc] initWithImage:tabelViewBgImage];
    
    [tabelViewBgImageView  setFrame:CGRectMake(frameX, frameY, tabelViewBgImage.size.width, tabelViewBgImage.size.height)];
	
	tblInputTypeSelection = [[UITableView alloc] initWithFrame:CGRectMake(frameX, frameY, tabelViewBgImage.size.width, tabelViewBgImage.size.height) style:UITableViewStylePlain];
    
    
    ///////////////// Steve Added ///////////////////////
    
    if ([arrowDirection isEqualToString:@"D"])
    {
        tblInputTypeSelection.layer.shadowColor = [[UIColor grayColor] CGColor];
        tblInputTypeSelection.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        tblInputTypeSelection.layer.shadowOpacity = 0.5f;
        tblInputTypeSelection.layer.shadowRadius = 5.0f;
        tblInputTypeSelection.layer.masksToBounds = NO;
        tblInputTypeSelection.clipsToBounds = NO;
    }
    else {
        tblInputTypeSelection.layer.shadowColor = [[UIColor grayColor] CGColor];
        tblInputTypeSelection.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
        tblInputTypeSelection.layer.shadowOpacity = 0.5f;
        tblInputTypeSelection.layer.shadowRadius = 5.0f;
        tblInputTypeSelection.layer.masksToBounds = NO;
        tblInputTypeSelection.clipsToBounds = NO;
    }
    
    ///////////////// Steve Added End ///////////////////////
    
    
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(15, 5, 0, 0);
	[tblInputTypeSelection setContentInset:contentInsets];
	[tblInputTypeSelection setBackgroundColor:[UIColor clearColor]];
	[tblInputTypeSelection setBackgroundView:tabelViewBgImageView];
	[tblInputTypeSelection setScrollEnabled:NO];
	[tblInputTypeSelection setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[tblInputTypeSelection setDelegate:self];
	[tblInputTypeSelection setDataSource:self];
	[self.view addSubview:tblInputTypeSelection];
	
	
	UIImage *downArrowImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:arrowImageName ofType:@"png"]];
	
	UIImageView *downArrowImageView = [[UIImageView alloc] initWithImage:downArrowImage];
	[downArrowImageView  setFrame:CGRectMake(arrowPointX, arrowPointY, downArrowImage.size.width, downArrowImage.size.height)];
	[self.view addSubview:downArrowImageView];
	
}

#pragma mark -
#pragma mark Tableview  Methods
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSInteger row;
	//row = [indexPath row];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewcell"];
	
	if (cell == nil) {
        
		cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44) reuseIdentifier:@"TableViewcell"];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone]; 
	}
	
	UILabel *lblCellText = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 100, 35)];
	[lblCellText setBackgroundColor:[UIColor clearColor]];
	[lblCellText setFont:[UIFont boldSystemFontOfSize:15]];
	[lblCellText setTextColor:[UIColor blackColor]];
	
    //	if (row == 0) {
    //		[lblCellText setText: @"Hand Writing"];
    //	}else if (row == 1) {
    //		[lblCellText setText: @"Voice to Text"];
    //	}else if (row == 2) {
    //		[lblCellText setText: @"Keyboard"];
    //	}
	
	[cell.contentView addSubview:lblCellText];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSInteger row;
	row = [indexPath row];
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //Steve added
    if ([parentVC  isKindOfClass:[ListToDoViewController class]]) isPenIcon = YES; //Sets animation to Handwriting View
    else isPenIcon = NO;
    
    NSLog(@"isPenIcon from InputTypeSelector = %d",isPenIcon);
    
	
	if (isFromMainScreen)
	{
        
        if ([parentVC  isKindOfClass:[ToDoParentController class]]) {
            
            if (row == 0) {
                
                ToDoViewController *todoViewController = [[ToDoViewController alloc] initWithNibName:@"ToDoViewController" bundle:[NSBundle mainBundle]];
                [todoViewController setTodoTitleType:@"T"];
                todoViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:todoViewController animated:NO];
                
				selectedInputType = @"H";
				HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:todoViewController isPenIcon:NO];		
				[appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
			}else if (row == 1) {
				selectedInputType = @"V";
                //Alok Added for Lite verstion VTT Feature        
                if(IS_LITE_VERSION)
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Limited Service" message:@"Voice to text is limited for the Pro version." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Upgrade", nil];
                    [alert show];
                    return;
                }
                
                ToDoViewController *todoViewController = [[ToDoViewController alloc] initWithNibName:@"ToDoViewController" bundle:[NSBundle mainBundle]];
                [todoViewController setTodoTitleType:@"T"];
                todoViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:todoViewController animated:NO];  
                //Steve changed was: initWithNibName:@"VoiceToTextViewController"
                
				VoiceToTextViewController *voiceToTextViewController = [[VoiceToTextViewController alloc] initWithNibName:@"VoiceToTextViewController"  bundle:[NSBundle mainBundle] withParent:[[appDelegate.navigationController viewControllers] objectAtIndex:([[appDelegate.navigationController viewControllers] count] - 1)]];
                
				[appDelegate.navigationController pushViewController:voiceToTextViewController animated:NO];
			}else if (row == 2) {
                ToDoViewController *todoViewController = [[ToDoViewController alloc] initWithNibName:@"ToDoViewController" bundle:[NSBundle mainBundle]];
                [todoViewController setTodoTitleType:@"T"];
                todoViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:todoViewController animated:NO];
                
                
                selectedInputType = @"T";
				if ([parentVC respondsToSelector:@selector(appointmentType)]) {
					[parentVC setAppointmentType:@"T"];
					[[parentVC titleTxtFld] becomeFirstResponder];
					[[parentVC titleTxtFld] setBackground:nil];
					[[parentVC titleTxtFld] setPlaceholder:@"Title"];
					[parentVC setAppointmentBinary:nil];
				}else if([parentVC respondsToSelector:@selector(todoTitleType)]){
					[parentVC setTodoTitleType:@"T"];
					[[parentVC titleTextField] becomeFirstResponder];
					[[parentVC titleTextField] setBackground:nil];
					[[parentVC titleTextField] setPlaceholder:@"Title"];
					[parentVC setTodoTitleBinary:nil];
				}
			}
		}
		else {
			
			if (row == 0) {
                
                AppointmentViewController *addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController" bundle:[NSBundle mainBundle]];
                [addEditCalendarViewController setAppointmentType:@"T"];
                addEditCalendarViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:addEditCalendarViewController animated:NO];
				selectedInputType = @"H";
				HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:addEditCalendarViewController isPenIcon:isPenIcon];
				[appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
			}else if (row == 1) {
                
                //Alok Added for Lite verstion VTT Feature        
                if(IS_LITE_VERSION)
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Limited Service" message:@"Voice to text is limited for the Pro version." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Upgrade", nil];
                    [alert show];
                    return;
                }
                
                AppointmentViewController *addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController" bundle:[NSBundle mainBundle]];
                [addEditCalendarViewController setAppointmentType:@"T"];
                addEditCalendarViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:addEditCalendarViewController animated:NO];
                
				selectedInputType = @"V";
				VoiceToTextViewController *voiceToTextViewController = [[VoiceToTextViewController alloc] initWithNibName:@"VoiceToTextViewController"  bundle:[NSBundle mainBundle] withParent:[[appDelegate.navigationController viewControllers] objectAtIndex:([[appDelegate.navigationController viewControllers] count] - 1)]];		
				[appDelegate.navigationController pushViewController:voiceToTextViewController animated:NO];
			}else if (row == 2) {
                
                AppointmentViewController *addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController" bundle:[NSBundle mainBundle]];
                [addEditCalendarViewController setAppointmentType:@"T"];
                addEditCalendarViewController.isAdd = YES;
                [appDelegate.navigationController pushViewController:addEditCalendarViewController animated:NO];
                
				selectedInputType = @"T";
				if ([parentVC respondsToSelector:@selector(appointmentType)]) {
					[parentVC setAppointmentType:@"T"];
					[[parentVC titleTxtFld] becomeFirstResponder];
					[[parentVC titleTxtFld] setBackground:nil];
					[[parentVC titleTxtFld] setPlaceholder:@"Title"];
					[parentVC setAppointmentBinary:nil];
				}else if([parentVC respondsToSelector:@selector(todoTitleType)]){
					[parentVC setTodoTitleType:@"T"];
					[[parentVC titleTextField] becomeFirstResponder];
					[[parentVC titleTextField] setBackground:nil];
					[[parentVC titleTextField] setPlaceholder:@"Title"];
					[parentVC setTodoTitleBinary:nil];
				}
			}
            //			[addEditTitle release];
		}
	}else {
		if (row == 0) {
			selectedInputType = @"H";
			HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:parentVC isPenIcon:isPenIcon];
            NSLog(@"parentVC->%@",[parentVC description]);
                        
			[appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
		}else if (row == 1) {
            //Alok Added for Lite verstion VTT Feature        
            if(IS_LITE_VERSION)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Limited Service" message:@"Voice to text is limited for the Pro version." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Upgrade", nil];
                [alert show];
                return;
            }
            
            //steve added issue - no Nav controll
            NSLog(@"inputTypeSelector");
			selectedInputType = @"V";
			VoiceToTextViewController *voiceToTextViewController = [[VoiceToTextViewController alloc] initWithNibName:@"VoiceToTextViewController"  bundle:[NSBundle mainBundle] withParent:[[appDelegate.navigationController viewControllers] objectAtIndex:([[appDelegate.navigationController viewControllers] count] - 1)]];		
			[appDelegate.navigationController pushViewController:voiceToTextViewController animated:NO];
		}else if (row == 2) {
			selectedInputType = @"T";
			if ([parentVC respondsToSelector:@selector(appointmentType)]) {
				[parentVC setAppointmentType:@"T"];
				[[parentVC titleTxtFld] becomeFirstResponder];
				[[parentVC titleTxtFld] setBackground:nil];
				
				[[parentVC titleTxtFld].layer setCornerRadius:0.0];
				[parentVC titleTxtFld].layer.borderColor = [UIColor clearColor].CGColor;
				[parentVC titleTxtFld].layer.borderWidth = 0.0;
				
				[[parentVC titleTxtFld] setPlaceholder:@"Title"];
				[parentVC setAppointmentBinary:nil];
			}
            else if([parentVC respondsToSelector:@selector(todoTitleList)]){
				[parentVC setTodoTitleType:@"T"];
				[parentVC setTodoTitle:@""];
				[parentVC setTodoTitleBinary:nil];
                [parentVC setIsCreateViewOn:NO];
                
                [parentVC createAddView];
			}
            else if([parentVC respondsToSelector:@selector(todoTitleType)]){
				[parentVC setTodoTitleType:@"T"];
				[[parentVC titleTextField] becomeFirstResponder];
				[[parentVC titleTextField] setBackground:nil];
				
				[[parentVC titleTextField].layer setCornerRadius:0.0];
				[parentVC titleTextField].layer.borderColor = [UIColor clearColor].CGColor;
				[parentVC titleTextField].layer.borderWidth = 0.0;
				
				[[parentVC titleTextField] setPlaceholder:@"Title"];
				[parentVC setTodoTitleBinary:nil];
			}
		}
	}
	
	[self.view  setAlpha:0];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self.view removeFromSuperview];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PRO_VERSION_URL]];
    }
}



#pragma mark -
#pragma mark - Memory Management Methods 
#pragma mark -

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
	[super viewDidUnload];
}


@end

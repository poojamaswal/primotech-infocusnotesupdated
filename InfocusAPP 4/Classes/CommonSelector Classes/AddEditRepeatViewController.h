//
//  AddEditRepeatViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditCalendarViewController.h"
#import "AppointmentsDataObject.h"

@interface AddEditRepeatViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>{
	UITableView		*repeatDetailTblVw;
	NSMutableArray	*repeatTimeArr;
	NSString		*repeatDescString;
	UIImageView		*tickImageView;
	
	NSInteger		lastRepeatID;
	NSInteger		selectedObjIndex;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withRepeatID:(NSInteger) repeatIDLocal;


@end

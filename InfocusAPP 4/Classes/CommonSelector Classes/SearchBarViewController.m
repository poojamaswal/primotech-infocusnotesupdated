//
//  SearchBarViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SearchBarViewController.h"


@interface SearchBarViewController()
-(void)addAdditionalControlsInSearchBar;
@end

@implementation SearchBarViewController
@synthesize searchAppBar, contentFrame, arrowDirection, arrowStartPoint, searchbarContainerView;

#pragma mark -
#pragma mark - ViewController Life Cycle
#pragma mark -

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal {
	if ((self = [super init])) {
		contentFrame = frame;	
		self.arrowDirection  = arrowDirectionLocal;
		arrowStartPoint = arrowStartPointLocal;
	}
	return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
	[self addAdditionalControlsInSearchBar];
}

#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	//	UITouch *touch = [touches anyObject];
	if (![touches containsObject:searchAppBar] && ![touches containsObject:searchbarContainerView]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
		[self.view  setAlpha:0];
		[UIView commitAnimations];
		[searchAppBar resignFirstResponder];
	}
}	

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}

#pragma mark -
#pragma mark - Private Methods 
#pragma mark -

-(void)addAdditionalControlsInSearchBar {
	CGFloat frameX = contentFrame.origin.x;
	CGFloat frameY = contentFrame.origin.y; 
//	NSInteger frameWidth = contentFrame.size.width;
//	NSInteger frameHeight = contentFrame.size.height;
	CGFloat arrowPointX = arrowStartPoint.x;
	CGFloat arrowPointY = arrowStartPoint.y; 
	
	[self.view setBackgroundColor:[UIColor clearColor]];

	UIImage *searchBarContainerBgImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"SearchBGImage" ofType:@"png"]];
	
	searchbarContainerView = [[UIView alloc] initWithFrame:CGRectMake(frameX, frameY, searchBarContainerBgImage.size.width, searchBarContainerBgImage.size.height)];
	[searchbarContainerView setBackgroundColor:[UIColor colorWithPatternImage:searchBarContainerBgImage]];
	searchAppBar = [[UISearchBar alloc] initWithFrame:CGRectMake(7, 25, 236, 52)];
	[searchAppBar  setDelegate:self];
	[searchAppBar.layer setCornerRadius:5.0];
	[searchAppBar setPlaceholder:@"Search Appointments,Todo's"];
	[searchbarContainerView addSubview:searchAppBar];
//	UIEdgeInsets contentInsets = UIEdgeInsetsMake(15, 5, 0, 0);
	[self.view addSubview:searchbarContainerView];
	
	NSString *imageName;
	
	if ([arrowDirection isEqualToString:@"D"]) {
		imageName = @"PopOverArrowDown";
	}else if ([arrowDirection isEqualToString:@"U"]) {
		imageName = @"PopOverArrowUp";
	}else if ([arrowDirection isEqualToString:@"R"]) {
		imageName = @"PopOverArrowRight";
	}else if ([arrowDirection isEqualToString:@"L"]) {
		imageName = @"PopOverArrowLeft";
	}
	
	UIImage *downArrowImage = [[UIImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
	
	UIImageView *downArrowImageView = [[UIImageView alloc] initWithImage:downArrowImage];
	[downArrowImageView setTag:101];
    
	[downArrowImageView  setFrame:CGRectMake(arrowPointX, arrowPointY, downArrowImage.size.width, downArrowImage.size.height)];
	[self.view addSubview:downArrowImageView];
	
	[searchBarContainerBgImage release];
	[downArrowImage release];
	[downArrowImageView release];
}

#pragma mark -
#pragma mark SearchBar Delegate Methods
#pragma mark -

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	UIImageView *downRaaowImageViewTemp = [[self.view viewWithTag:101] retain];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[downRaaowImageViewTemp setFrame:CGRectMake(downRaaowImageViewTemp.frame.origin.x, downRaaowImageViewTemp.frame.origin.y - 172, downRaaowImageViewTemp.frame.size.width, downRaaowImageViewTemp.frame.size.height)];
	[searchbarContainerView setFrame:CGRectMake(searchbarContainerView.frame.origin.x, searchbarContainerView.frame.origin.y - 172, searchbarContainerView.frame.size.width, searchbarContainerView.frame.size.height)];
	[UIView commitAnimations];
	[downRaaowImageViewTemp release];
}                     

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	UIImageView *downRaaowImageViewTemp = [[self.view viewWithTag:101] retain];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[downRaaowImageViewTemp setFrame:CGRectMake(downRaaowImageViewTemp.frame.origin.x, downRaaowImageViewTemp.frame.origin.y + 172, downRaaowImageViewTemp.frame.size.width, downRaaowImageViewTemp.frame.size.height)];
	[searchbarContainerView setFrame:CGRectMake(searchbarContainerView.frame.origin.x, searchbarContainerView.frame.origin.y + 172, searchbarContainerView.frame.size.width, searchbarContainerView.frame.size.height)];
	[UIView commitAnimations];
	[downRaaowImageViewTemp release];
}                                            

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	
}                                          

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	
}                                         

#pragma mark -
#pragma mark - Memory Management Methods 
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
	[arrowDirection release];
	[searchAppBar release];
	[searchbarContainerView release];
}

@end

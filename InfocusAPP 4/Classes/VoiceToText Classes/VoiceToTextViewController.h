//
//  VoiceToTextViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpeechKit/SpeechKit.h>
#import "AddEditTitleViewController.h"
#import "GlobalUtility.h"
#import "TODODataObject.h"
#import "AddNewNotesViewCantroller.h"
//#import "AddEditToDoViewController.h"

@interface VoiceToTextViewController : UIViewController <SpeechKitDelegate, SKRecognizerDelegate, NSXMLParserDelegate,UITableViewDelegate,UITableViewDataSource>{
	IBOutlet UIButton* recordButton;
	//IBOutlet UITextView* alternativesDisplay;

	//UITextField* searchBox;
    IBOutlet UITextField* searchBox;
    
    
    IBOutlet UIImageView *bgImgView;
	//UITableView *titleTxtTblVw;         
    IBOutlet UITableView *titleTxtTblVw;  //Steve changed
    IBOutlet UILabel  *alternativeChoicesLabel;  //Steve added
	NSArray *titlesArray;
	
	NSString *eleName;
	NSString *responceString;
	
	id parentVC;

	SKRecognizer* voiceSearch;
    enum {
        TS_IDLE,
        TS_INITIAL,
        TS_RECORDING,
        TS_PROCESSING,
    } transactionState;
	
    
    TODODataObject *toDoBeingEdited;
    
    //Steve added
    //IBOutlet UIView* vuMeter;
    
    IBOutlet UIImageView *blackimage;
    //Steve added
    CALayer *vuMeterCenter;
    CALayer *vuMeterLeft1;
    CALayer *vuMeterLeft2;
    CALayer *vuMeterRight1;
    CALayer *vuMeterRight2;
    
    //Steve added
    int    counter;

    IBOutlet UINavigationBar *navBar;

}
@property(nonatomic,strong) IBOutlet UIButton* recordButton;
//@property(nonatomic,strong) UITextField* searchBox;
@property(nonatomic,strong) IBOutlet UITextField* searchBox;
@property (nonatomic, strong) TODODataObject *toDoBeingEdited;
//@property (nonatomic, strong)  IBOutlet UILabel  *alternativeChoicesLabel;  //Steve added

//@property(nonatomic, strong) IBOutlet UIView* vuMeter;
//Steve added
@property(nonatomic, strong)         SKRecognizer* voiceSearch;

-(IBAction)backBtn_Clicked:(id) sender;
- (IBAction)recordButtonAction: (id)sender;
-(IBAction)doneClicked:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal;
@end

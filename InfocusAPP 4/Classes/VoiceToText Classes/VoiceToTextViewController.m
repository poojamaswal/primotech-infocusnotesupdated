//
//  VoiceToTextViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "VoiceToTextViewController.h"
#import "ToDoViewController.h"
//Alok Added
#import "ListToDoViewController.h"
@interface VoiceToTextViewController()
-(void)addAdditionalControlsInVoicetoText;
-(void)adjustControls;
@end

//New SpeechKit Key
const unsigned char SpeechKitApplicationKey[] = {0xa4, 0xe1, 0x42, 0x0b, 0x03, 0xa3, 0x8c, 0x3a, 0x9f, 0xbc, 0xe4, 0x97, 0xf3, 0xe1, 0x58, 0x8f, 0x7d, 0xf1, 0xf7, 0x77, 0xc4, 0x70, 0x41, 0xbd, 0x69, 0xa3, 0xdd, 0x74, 0x94, 0x93, 0xc5, 0x38, 0x74, 0x07, 0x43, 0x77, 0xab, 0xd0, 0xb1, 0x4a, 0x8c, 0x95, 0xe1, 0x7f, 0x76, 0x39, 0x0f, 0xf3, 0x2b, 0x06, 0x6b, 0xef, 0x76, 0xac, 0xac, 0xdf, 0x50, 0x89, 0x54, 0xe4, 0x1c, 0x19, 0x1c, 0x90};


//const unsigned char SpeechKitApplicationKey[] = {0x57, 0xac, 0xe5, 0xb5, 0x37, 0x6b, 0x2, 0x7c, 0x1c, 0xb9, 0xa8, 0xe, 0x73, 0x67, 0xc7, 0xab, 0xc7, 0x3f, 0xf5, 0x2d, 0x1b, 0x3f, 0xef, 0xf6, 0x5, 0x6f, 0x1a, 0x96, 0x14, 0x33, 0xbe, 0x6b, 0xdf, 0xa3, 0x20, 0x16, 0xe, 0x22, 0x92, 0x2a, 0xdc, 0x3a, 0x39, 0x4f, 0xa1, 0x0, 0xfa, 0x69, 0x3f, 0xef, 0x96, 0x5a, 0x2e, 0x85, 0x51, 0x34, 0xf4, 0x6b, 0xca, 0x9, 0x9e, 0x88, 0x74, 0x7d};

@implementation VoiceToTextViewController
@synthesize recordButton,searchBox;
@synthesize toDoBeingEdited;
//Steve added
@synthesize voiceSearch;

#pragma mark -
#pragma mark ViewController LifeCycle
#pragma mark -

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal{
	if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		parentVC = parentVCLocal;
        toDoBeingEdited = nil;
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    //old Server info
    //[SpeechKit setupWithID:@"NMDPTRIAL_a1technology_tarunsharma20110308021750" host:@"65.124.114.176" port:443 useSSL:NO delegate:self];
    
    //Updated server info
    [SpeechKit setupWithID:@"NMDPPRODUCTION_Elixir_Software_Group_InFocus_Pro_20120703191027" host:@"qm.nmdp.nuancemobility.net" port:443 useSSL:NO delegate:self];
    
    
    
	//	[self addAdditionalControlsInVoicetoText];
}

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"]; 
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
	[titleTxtTblVw setHidden:YES];
    [bgImgView setHidden:YES];
    [alternativeChoicesLabel setHidden:YES];
}


-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:YES];
	[self addAdditionalControlsInVoicetoText];
}



#pragma mark -
#pragma mark Keyboard

/*
 - (void) textFieldShouldBeginEditing: (UITextField  *)textField{
 [textField becomeFirstResponder];
 [textField setKeyboardAppearance:UIReturnKeyDone]; 
 }
 */
- (void)textFieldDidEndEditing:(UITextField *)textField {
	[textField resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark -
#pragma mark VU Meter


- (void)setVUMeterWidth:(float)width :(float)width1 :(float)width2 
{
    // NSLog(@"setVUMeterWidth");
    //NSLog(@" voiceSearch.audioLevel = %f ", voiceSearch.audioLevel);
    
    if (width < 0)
        width = 0;
    
    if(width2 <=0)
    {
        width = 10;
        width1 = 5;
        width2 = 0;
    }
    
    ////////// Steve added //////////////
    //Sets a Maximum value & also varies values if even number or odd number
    
    if (width > 75 || width1 > 55 || width2 > 35)
    {
        counter = ++counter;
        //NSLog(@"Counter = %d", counter);
        
        if (counter %2 == 0) //Even Integer
        {
            width = 75;
            width1 = 55;
            width2 = 35;
        }
        else                  //Odd Integer
        {
            width = 55;
            width1 = 35;
            width2 = 15;
        }
        
    }
    ////////// Steve added End //////////////
    
    
    //NSLog(@"width = %f", width);
    // NSLog(@"width1 = %f", width1);
    // NSLog(@"width2 = %f", width2);
    
    
    
    //Steve commented out
    /*
     CGRect frame = vuMeter.frame;
     frame.size.width = width+10;
     vuMeter.frame = frame;
     */
    
    
    
    vuMeterCenter.bounds = CGRectMake(0, 0, 10, 30 + width);
    vuMeterLeft1.bounds = CGRectMake(0, 0, 10, 20 + width1);
    vuMeterLeft2.bounds = CGRectMake(0, 0, 10, 10 + width2);
    vuMeterRight1.bounds = CGRectMake(0, 0, 10, 20 + width1);
    vuMeterRight2.bounds = CGRectMake(0, 0, 10, 10 + width2);
    
    
    
    
    // NSLog(@"vuMeter.frame.size.height == %f", vuMeterCenter.frame.size.height);
    
    
}

- (void)updateVUMeter 
{
    //NSLog(@"updateVUMeter audio level %f ",voiceSearch.audioLevel);
    
    //Steve changed
    float width = (voiceSearch.audioLevel + 64)*1;  
    float width1 = (voiceSearch.audioLevel + 64)*1;
    float width2 = (voiceSearch.audioLevel + 64)*1;  
    
    //NSLog(@"width updateVUMeter= %f", width);
    // NSLog(@"width1 updateVUMeter= %f", width1);
    // NSLog(@"width2 updateVUMeter= %f", width2);
    
    //Steve commented out
    /*
     float width = (40 + voiceSearch.audioLevel)* (5/2); 
     float width1 = (30 + voiceSearch.audioLevel)* (5/2);
     float width2 = (20 + voiceSearch.audioLevel)* (5/2);  
     */
    
    
    
    if(voiceSearch.audioLevel <= -5) {
        // [self setVUMeterWidth:40:20:10];   //Steve commented out
        
        // return;
    }
    
    // NSLog(@" width = %f ", width);
    
    //Steve added to exit, otherwise loops forever
    if (voiceSearch.audioLevel ==  0.00) 
    {
        vuMeterCenter.bounds = CGRectMake(0, 0, 10, 40);
        vuMeterLeft1.bounds = CGRectMake(0, 0, 10, 20);
        vuMeterLeft2.bounds = CGRectMake(0, 0, 10, 10);
        vuMeterRight1.bounds = CGRectMake(0, 0, 10, 20);
        vuMeterRight2.bounds = CGRectMake(0, 0, 10, 10);
        
        return;
    }
    
    [self setVUMeterWidth:width:width1:width2];    
    [self performSelector:@selector(updateVUMeter) withObject:nil afterDelay:0.05];
}

#pragma mark -
#pragma mark Private methods
#pragma mark -
-(void)addAdditionalControlsInVoicetoText{
	[GlobalUtility setNavBarTitle:@"Voice To Text Input" forViewController:self];
	
	titlesArray = [[NSArray alloc] init];
	
	[self.navigationItem setHidesBackButton:YES];
	
    [recordButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];  //Steve added
    [alternativeChoicesLabel setHidden:YES];  //Steve added
	
	//searchBox = [[UITextField alloc] initWithFrame:CGRectMake(20, 50, 280, 31)];
	[searchBox setUserInteractionEnabled:NO]; 
	[searchBox setBorderStyle:UITextBorderStyleRoundedRect];
	[searchBox setBackgroundColor:[UIColor whiteColor]]; //Steve changed was: [UIColor clearColor]
    [searchBox setAutocapitalizationType:UITextAutocapitalizationTypeSentences]; //Steve added
    [searchBox setReturnKeyType:UIReturnKeyDone]; //Steve added
    
	[self.view addSubview:searchBox];
    
    // NSLog(@" searchbox is %@", searchBox.description);
	
    
	//titleTxtTblVw=[[UITableView alloc] initWithFrame:CGRectMake(20, 89, 280, 140) style:UITableViewStyleGrouped];  //Steve changed table height
	[titleTxtTblVw setDataSource:self];
	[titleTxtTblVw setDelegate:self];
	[titleTxtTblVw setHidden:YES];  //Steve changed
    [bgImgView setHidden:YES];
    titleTxtTblVw.layer.cornerRadius = 0.25;
    titleTxtTblVw.layer.borderColor = [[UIColor blackColor] CGColor];
	//[titleTxtTblVw setBackgroundColor:[UIColor whiteColor]];
    //[titleTxtTblVw.layer setBorderColor:[[UIColor blackColor] CGColor]]; //Steve added
	//[self.view addSubview:titleTxtTblVw];
    
    
    
    ///////////////  Steve added meter //////////////////////
    
    //Center Meter
    vuMeterCenter = [CALayer layer];
    vuMeterCenter.bounds = CGRectMake(0, 0, 10, 30);
    vuMeterCenter.position = CGPointMake(162, 315);
    vuMeterCenter.backgroundColor = [UIColor whiteColor].CGColor;
    vuMeterCenter.borderColor = [UIColor blackColor].CGColor;
    vuMeterCenter.opacity = 1.0f;
    [self.view.layer addSublayer:vuMeterCenter];
    
    //Left1 meter
    vuMeterLeft1 = [CALayer layer];
    vuMeterLeft1.bounds = CGRectMake(0, 0, 10, 20);
    vuMeterLeft1.position = CGPointMake(144, 315);
    vuMeterLeft1.backgroundColor = [UIColor whiteColor].CGColor;
    //vuMeter.borderColor = [UIColor blackColor].CGColor;
    vuMeterLeft1.opacity = 1.0f;
    [self.view.layer addSublayer:vuMeterLeft1];
    
    //Left2 meter
    vuMeterLeft2 = [CALayer layer];
    vuMeterLeft2.bounds = CGRectMake(0, 0, 10, 10);
    vuMeterLeft2.position = CGPointMake(125, 315);
    vuMeterLeft2.backgroundColor = [UIColor whiteColor].CGColor;
    //vuMeter.borderColor = [UIColor blackColor].CGColor;
    vuMeterLeft2.opacity = 1.0f;
    [self.view.layer addSublayer:vuMeterLeft2];
    
    
    
    //Right1 meter
    vuMeterRight1 = [CALayer layer];
    vuMeterRight1.bounds = CGRectMake(0, 0, 10, 20);
    vuMeterRight1.position = CGPointMake(180, 315);
    vuMeterRight1.backgroundColor = [UIColor whiteColor].CGColor;
    //vuMeter.borderColor = [UIColor blackColor].CGColor;
    vuMeterRight1.opacity = 1.0f;
    [self.view.layer addSublayer:vuMeterRight1];
    
    //Right2 meter
    vuMeterRight2 = [CALayer layer];
    vuMeterRight2.bounds = CGRectMake(0, 0, 10, 10);
    vuMeterRight2.position = CGPointMake(201, 315);
    vuMeterRight2.backgroundColor = [UIColor whiteColor].CGColor;
    //vuMeter.borderColor = [UIColor blackColor].CGColor;
    vuMeterRight2.opacity = 1.0f;
    [self.view.layer addSublayer:vuMeterRight2];
    [self.view bringSubviewToFront:blackimage];
    ///////////////  Steve added meter End //////////////////////
}

-(void)adjustControls{
}
#pragma mark -
#pragma mark SKRecognizerDelegate methods

- (void)recognizerDidBeginRecording:(SKRecognizer *)recognizer
{
    //NSLog(@"Recording started.");
    
    transactionState = TS_RECORDING;
    [recordButton setTitle:@" Recording..." forState:UIControlStateNormal];
    
    [recordButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [recordButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //recordButton.titleLabel.c
    [self performSelector:@selector(updateVUMeter) withObject:nil afterDelay:0.05];  //Steve added back. For meter.
}

- (void)recognizerDidFinishRecording:(SKRecognizer *)recognizer
{
    //NSLog(@"Recording finished.");
	
    transactionState = TS_PROCESSING;
    [recordButton setTitle:@" Processing..." forState:UIControlStateNormal];
    [recordButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    //Steve added
    [self setVUMeterWidth:40:20:10];
    [recordButton setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:129.0f/255.0f blue:0.0f/255.0f alpha:1.0] forState:UIControlStateNormal];//Steve added
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)results{
    // NSLog(@"Got results.");
    long numOfResults = [results.results count];
    
    transactionState = TS_IDLE;
    [recordButton setTitle:@"Record" forState:UIControlStateNormal];
    [searchBox setUserInteractionEnabled:YES];   //Steve added
    [searchBox setKeyboardAppearance:UIReturnKeyDone]; //Steve added
    
    if (numOfResults > 0){
        searchBox.text = [results firstResult];
	}
    
	if (numOfResults > 1){ 
		if (titlesArray) {
			titlesArray = nil;
		}
		titlesArray = [results results];
		[titleTxtTblVw reloadData];
		[titleTxtTblVw setHidden:NO];
        [bgImgView setHidden:NO];
        [alternativeChoicesLabel setHidden:NO]; //Steve added
		//alternativesDisplay.text = [[results.results subarrayWithRange:NSMakeRange(1, numOfResults-1)] componentsJoinedByString:@"\n"];
    }
    if (results.suggestion) {        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suggestion"
														message:results.suggestion delegate:nil
											  cancelButtonTitle:@"OK" otherButtonTitles:nil];        
		[alert show];
    }
	voiceSearch = nil;
    [recordButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];  //Steve added
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion{
    //NSLog(@"Got error.");
    transactionState = TS_IDLE;
    [recordButton setTitle:@"Record" forState:UIControlStateNormal];
	
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[error localizedDescription]delegate:nil cancelButtonTitle:@"OK"
										 otherButtonTitles:nil];        
    [alert show];
    
    if (suggestion) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suggestion"message:suggestion
													   delegate:nil cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];        
		[alert show];
    }
	voiceSearch = nil;
}

#pragma mark -
#pragma mark All Methods

- (IBAction)recordButtonAction: (id)sender {
    if (transactionState == TS_RECORDING) {
        [voiceSearch stopRecording];
    }
    else if (transactionState == TS_IDLE) {
        SKEndOfSpeechDetection detectionType;
        NSString* recoType=SKDictationRecognizerType;
        NSString* langType=@"en_US";;
        detectionType=SKShortEndOfSpeechDetection;
        transactionState = TS_INITIAL;
		
		//[searchBox resignFirstResponder];
		
        // NSLog(@"Recognizing type:'%@' Language Code: '%@'.", recoType, langType);
		
		voiceSearch = [[SKRecognizer alloc] initWithType:recoType detection:detectionType language:langType delegate:self];
    }
}

-(IBAction)doneClicked:(id)sender {
	//NSLog(@"Text string is: %@",searchBox.text);
    
 	if(![searchBox.text isEqualToString:@""] && searchBox.text != nil){
		if ([parentVC respondsToSelector:@selector(appointmentTitle)]) {
			NSArray *cntrlrsArr = [self.navigationController viewControllers];
			
			AddEditTitleViewController *tempVwCntrlr = (AddEditTitleViewController*)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2];
			[[tempVwCntrlr titleTxtFld] setText:searchBox.text];
			[[tempVwCntrlr titleTxtFld] setTextColor:[UIColor blueColor]];
			[tempVwCntrlr setAppointmentTitle:searchBox.text];
			[tempVwCntrlr setAppointmentType:@""];
			[tempVwCntrlr setAppointmentType:@"V"];
			[tempVwCntrlr setAppointmentBinary:nil];
		}  
        else if([parentVC respondsToSelector:@selector(todoTitleList)])
        {
            // NSLog(@"Anil");
            
            if(toDoBeingEdited  == nil)
            {
                [parentVC setTodoTitle:searchBox.text];
                [parentVC setTodoTitleType:@"V"];
                
            }
            else
            {
                // Update this to do
                
                [toDoBeingEdited setTodoTitle:searchBox.text];
                //BOOL isInserted = [AllInsertDataMethods updateToDoDataValues:toDoBeingEdited];
                // NSLog(@"%d",isInserted);
                toDoBeingEdited = nil;
                
            }
                  
        }
        
        
        else if([parentVC respondsToSelector:@selector(todoTitle)])  //ToDo title
        {
            
            NSArray *cntrlrsArr = [self.navigationController viewControllers];
            
            ToDoViewController *tempVwCntrlr = (ToDoViewController*)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2];
            [[tempVwCntrlr titleTextField] setText:searchBox.text];
            [[tempVwCntrlr titleTextField] setTextColor:[UIColor blueColor]];
            [tempVwCntrlr setTodoTitle:searchBox.text];
            [tempVwCntrlr setTodoTitleType:@""];
            [tempVwCntrlr setTodoTitleType:@"V"];
            [tempVwCntrlr setTodoTitleBinary:nil];
			
		}
        else if([parentVC respondsToSelector:@selector(notesTitle)])  //ToDo title
        {
            
            NSArray *cntrlrsArr = [self.navigationController viewControllers];
            
            AddNewNotesViewCantroller *tempVwCntrlr = (AddNewNotesViewCantroller*)[cntrlrsArr objectAtIndex:[cntrlrsArr count]-2];
            [[tempVwCntrlr titleTextField] setText:searchBox.text];
            [[tempVwCntrlr titleTextField] setTextColor:[UIColor blueColor]];
            [tempVwCntrlr setNotesTitle:searchBox.text];
            [tempVwCntrlr setNotesTitleType:@""];
            [tempVwCntrlr setNotesTitleType:@"V"];
            [tempVwCntrlr setNotesTitleBinary:nil];
			
		}
        
        
        
        
		
		[self.navigationController popViewControllerAnimated:YES];
        
	} 
    else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"No Voice is recorded to use as title." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}

-(IBAction)backBtn_Clicked:(id) sender 
{ 
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [titlesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	if (cell == nil){
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"];
	}
	
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	}
	
	[cell.textLabel setText:[titlesArray objectAtIndex:indexPath.row]];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	[searchBox setText:[titlesArray objectAtIndex:indexPath.row]];
	
}

#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    navBar = nil;
    blackimage = nil;
    bgImgView = nil;
    [super viewDidUnload];
}


@end

//
//  TKCalendarMonthView.m
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 */

#import "TKCalendarMonthView.h"
#import "NSDate+TKCategory.h"
#import "TKGlobal.h"
#import "UIImage+TKCategory.h"
#import "NSDate-Utilities.h"
#import "OrganizerAppDelegate.h"
#import "CalendarMonthViewController.h"

//#define kCalendImagesPath @"TapkuLibrary.bundle/Images/calendar/"

@class TKCalendarMonthTiles; //Steve iPad

@interface NSDate (calendarcategory) //Called Second
- (NSDate*) firstOfMonth;
- (NSDate*) nextMonth;
- (NSDate*) previousMonth;
- (NSDate*) monthWithDate:(NSDate*) date; 
-(NSDate*) afterMonth:(NSInteger) months;

@end

@implementation NSDate (calendarcategory)

- (NSDate*) firstOfMonth{
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day = 1;
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

-(NSDate*) afterMonth:(NSInteger) months {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	if (months > 0) {
	}else {
	}

	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

- (NSDate*) nextMonth {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

- (NSDate*) monthWithDate:(NSDate*) date {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) fromDate:date];
	info.month = [comp month];
	info.year = [comp year];
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
}

- (NSDate*) previousMonth{
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.month--;
	if(info.month<1){
		info.month = 12;
		info.year--;
	}
	
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

@end

@interface TKCalendarMonthTiles : UIView {
	UILabel		*dot;
	UILabel		*currentDay;
	UIImageView *selectedImageView;
	
	id		target;
	SEL		action;
	
	int		selectedDay, selectedPortion;
	int		firstWeekday, daysInMonth;
	int		firstOfPrev, lastOfPrev;
	int		today;
	
	BOOL	startOnSunday;
	BOOL	markWasOnToday;
	
	NSDate	*monthDate;
	NSArray *marks;
    
    
    
}

@property (readonly) NSDate *monthDate;
@property(nonatomic) float tileHeight;//Steve iPad

- (id) initWithMonth:(NSDate*)date marks:(NSArray*)marks startDayOnSunday:(BOOL)sunday;
- (void) setTarget:(id)target action:(SEL)action;

- (void) selectDay:(int) day;
- (NSDate *) dateSelected;
+ (NSArray *) rangeOfDatesInMonthGrid:(NSDate *)date startOnSunday:(BOOL)sunday;
- (CGRect) rectForButtonCellAtIndex:(int)index;//Steve

@end



//For classic Calendar
#define dotFontSize 18.0
#define dateFontSize 18.0

//For Picture Calendar
#define dotFontSizePictureCalendar 18.0
#define dateFontSizePictureCalendar 22.0

//For iPad
#define iPadDotFontSize  23.0
#define iPadDateFontSize 30.0


@interface TKCalendarMonthTiles (private)

@property (strong, nonatomic) UIImageView *selectedImageView;
@property (readonly) UILabel *currentDay;
@property (readonly) UILabel *dot;
@property(nonatomic, assign) float tileHeight; //Steve iPad
@end


@implementation TKCalendarMonthTiles
@synthesize monthDate;
@synthesize tileHeight;


+ (NSArray*) rangeOfDatesInMonthGrid:(NSDate*)date startOnSunday:(BOOL)sunday{
    //NSLog(@"TKCalendarMonthTiles rangeOfDatesInMonthGrid");
    
    
	NSDate *firstDate, *lastDate;
	
	TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day	= 1;
	info.hour	= 0;
	info.minute = 0;
	info.second = 0;
	
	NSDate *currentMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info = [currentMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSDate *previousMonth = [currentMonth previousMonth];
	NSDate *nextMonth = [currentMonth nextMonth];
	
	if(info.weekday > 1 && sunday) {
		TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		int preDayCnt = [previousMonth daysBetweenDate:currentMonth];		
		info2.day = preDayCnt - info.weekday + 2;
		firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	}else if(!sunday && info.weekday != 2) {
		TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		int preDayCnt = [previousMonth daysBetweenDate:currentMonth];
		
		if(info.weekday == 1){
			info2.day = preDayCnt - 5;
		}else{
			info2.day = preDayCnt - info.weekday + 3;
		}
		firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	}else{
		firstDate = currentMonth;
	}
	
	int daysInMonth = [currentMonth daysBetweenDate:nextMonth];		
	info.day = daysInMonth;
	NSDate *lastInMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	TKDateInformation lastDateInfo = [lastInMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];	
	
	if(lastDateInfo.weekday < 7 && sunday){
		
		lastDateInfo.day = 7 - lastDateInfo.weekday;
		lastDateInfo.month++;
		lastDateInfo.weekday = 0;
		if(lastDateInfo.month>12){
			lastDateInfo.month = 1;
			lastDateInfo.year++;
		}
		lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		
	}else if(!sunday && lastDateInfo.weekday != 1){
		
		
		lastDateInfo.day = 8 - lastDateInfo.weekday;
		lastDateInfo.month++;
		if(lastDateInfo.month>12){ lastDateInfo.month = 1; lastDateInfo.year++; }
		
		
		lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		
	}else{
		lastDate = lastInMonth;
	}
	return [NSArray arrayWithObjects:firstDate, lastDate, nil];
}

- (id) initWithMonth:(NSDate *) date marks:(NSArray *) markArray startDayOnSunday:(BOOL)sunday {
	// NSLog(@"TKCalendarMonthTiles initWithMonth");
    
    if(![super initWithFrame:CGRectZero]) return nil;
    
    self.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0];

    
	firstOfPrev = -1;
	marks = markArray;
	monthDate = date;
	startOnSunday = sunday;
	
	TKDateInformation dateInfo = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	firstWeekday = dateInfo.weekday;
	
	NSDate *prev = [monthDate previousMonth];
	daysInMonth = [[monthDate nextMonth] daysBetweenDate:monthDate];
	
	int row = (daysInMonth + dateInfo.weekday - 1);
	if(dateInfo.weekday==1&&!sunday) row = daysInMonth + 6;
	if(!sunday) row--;
	
	row = (row / 7) + ((row % 7 == 0) ? 0:1);
    
    
    CGFloat h ;
    
    //Steve iPad
    if(IS_IPAD)
    {
        float maxHeightAllowed=CalendarViewHeight-CalendarTopBarHeight;
        tileHeight= maxHeightAllowed/row;
        //NSLog(@"tileHeight = %f",tileHeight);
        h=tileHeight * row;
    }
    else
    {
        h = 38 * row;// h=44.0f * row;
    }
    

    
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	//finish
	
	TKDateInformation todayInfo = [destinationDate dateInformation];
	today = dateInfo.month == todayInfo.month && dateInfo.year == todayInfo.year ? todayInfo.day : 0;
	
	int preDayCnt = [prev daysBetweenDate:monthDate];
	
	if(firstWeekday > 1 && sunday) {
		firstOfPrev = preDayCnt - firstWeekday + 2;
		lastOfPrev = preDayCnt;
	}else if(!sunday && firstWeekday != 2){
		
		if(firstWeekday == 1) {
			firstOfPrev = preDayCnt - 5;
		}else{
			firstOfPrev = preDayCnt - firstWeekday+3;
		}
		lastOfPrev = preDayCnt;
	}
	
    
    //Steve iPad
    if (IS_IPAD) {
        self.frame = CGRectMake(0, 1.0, tileWidth*7 ,h+1);
    }
    else{
        self.frame = CGRectMake(0, 1, 320, h+1);
    }
    
   // NSLog(@"TKCalendarMonthTiles =%@",NSStringFromCGRect(self.frame));

	
	[self.selectedImageView addSubview:self.currentDay];
	[self.selectedImageView addSubview:self.dot];
	self.multipleTouchEnabled = NO;
    
	
	return self;
}

- (void) setTarget:(id)t action:(SEL)a{
	target = t;
	action = a;
}

- (CGRect) rectForCellAtIndex:(int)index{
	int row = index / 7;
	int col = index % 7;
    
    //Steve iPad
    if (IS_IPAD) {
        
        CGRect rect =CGRectMake(col*tileWidth, row*tileHeight+tileHeightAdjustment, tileWidth, tileHeight);// CGRectMake(col*92, row*88+29, 94, 90);
        //NSLog(@"rectForCellAtIndex = %@", NSStringFromCGRect(rect));
        
        return rect;

    }
    else{
        return CGRectMake(col*46, row*38+6, 47, 38);
    }

}


- (CGRect) rectForButtonCellAtIndex:(int)index{
	int row = index / 7;
	int col = index % 7;
    
    
    //Steve iPad
    if (IS_IPAD) {
        
        CGRect rect = CGRectMake(col*tileWidth, row*tileHeight+tileHeightAdjustment, tileWidth, tileHeight);// CGRectMake(col*92, row*88+29, 94, 90);
         //NSLog(@"rectForCellAtIndex = %@", NSStringFromCGRect(rect));
        return rect;
    }
    else{
        
        return CGRectMake(col*46 + 11, row*38 + 5, 26, 26);
    }
    
	
}


//Draws dots in Calendar
- (void) drawTileInRect:(CGRect) r day:(int) day mark:(BOOL) mark font:(UIFont *) f1 font2:(UIFont *) f2 {
	//NSLog(@"TKCalendarMonthTiles drawTileInRect");
    
    //UIFont *newFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];//Steve
    
    
	NSString *str = [NSString stringWithFormat:@"%d", day];
    
    if (IS_IPAD) {
        
        r.size.height -= 2;//2
        [str drawInRect: r
               withFont: f1
          lineBreakMode: NSLineBreakByWordWrapping
              alignment: NSTextAlignmentCenter];
        
        
        if(mark){
            r.size.height = 10;
            r.origin.y += 25;//25
            
            [@"•" drawInRect: r
                    withFont: f2
               lineBreakMode: NSLineBreakByWordWrapping
                   alignment: NSTextAlignmentCenter];
        }

    }
    else{ //iPhone
        
        r.size.height -= 2;
        [str drawInRect: r
               withFont: f1
          lineBreakMode: NSLineBreakByWordWrapping
              alignment: NSTextAlignmentCenter];
        
        
        if(mark){
            r.size.height = 10;
            r.origin.y += 15; //Steve - was 18, but too low with iOS 7 & system font change
            
            [@"•" drawInRect: r
                    withFont: f2
               lineBreakMode: NSLineBreakByWordWrapping
                   alignment: NSTextAlignmentCenter];
        }

    }
	
}


//Draws Date Tiles
- (void) drawRect:(CGRect)rect {
   // NSLog(@"TKCalendarMonthTiles drawRect");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Steve - fills the contex space with a clear color so you see the image
    CGContextSetRGBFillColor(context, 0, 0, 0, 0);
    CGContextFillRect(context, self.frame);
    
    CGRect r;
    
    //Steve iPad
    if (IS_IPAD) {
        r = CGRectMake(0, 0, tileWidth, tileHeight);
    }
    else{
        
        r = CGRectMake(0, 0, 46, 38);
    }

    
    
    NSUserDefaults *sharedDefualts = [NSUserDefaults standardUserDefaults];
    
    if ( [sharedDefualts boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7) {
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                //Fills with off white color
                CGContextSetRGBFillColor(context, 245.0/255.0, 245.0/255.0, 245.0/255.0, 1.0); //Calendar Background color
                CGContextFillRect(context, self.frame);
                
                //draws line below the Calendar for light theme only
                CGPoint point[2];
                point[0] = CGPointMake(0, self.frame.size.height - 1);
                point[1] = CGPointMake(320, self.frame.size.height - 1);
                
                UIColor *color = [UIColor lightGrayColor];
                CGContextSetStrokeColorWithColor(context, color.CGColor);
 
                CGContextAddLines(context, point, 2);
                CGContextStrokePath(context);
                CGContextFillPath(context);
                
            }
            else{ //Dark Theme
                
                //adss the tile image
                UIImage *tile = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile.png")];
                CGContextDrawTiledImage(context, r, tile.CGImage);
            }

        }
        else{ //iOS 6
            
            //adss the tile image
            UIImage *tile = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile.png")];
            CGContextDrawTiledImage(context, r, tile.CGImage);
        }
        
        
    }

	
    //////////////////// Today Tile /////////////////////////
    
	if(today > 0){
        
		int pre = firstOfPrev > 0 ? lastOfPrev - firstOfPrev + 1 : 0;
		int index = today +  pre-1;
        
		CGRect r =[self rectForCellAtIndex:index];
        
        //Steve iPad
        if (IS_IPAD) {
            r.origin.y -= tileHeightAdjustment;
        }
        else //iPhone
        {
            r.origin.y -= 7;
        }
        
        //Steve iPad
        if (IS_IPAD) {
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"])
            {
                //Dark Theme --> fills with square
                CGRect  newRect = CGRectMake(r.origin.x, r.origin.y, r.size.width, r.size.height);
                //NSLog(@"rect = %@", NSStringFromCGRect(r));
                
                //Fills the Today Tile with a Blue Color
                CGContextSetRGBFillColor(context, 52.0/255.0, 143.0/255.0, 255.0/255.0, 1);//blue
                CGContextFillRect(context, newRect);
            }
            else //Photo Calendar
            {
                // Steve Add circle under numbers of Today Calendar Date
                CGRect circlRect = [self rectForButtonCellAtIndex:index];
                
                //Adjust CircleRect to fit over number
                circlRect.origin.x = circlRect.origin.x + 14; //+14
                circlRect.origin.y = circlRect.origin.y - 18; //-18
                
                circlRect.size.width = 75;
                circlRect.size.height = 75;
                
                UIColor *colorOfCircle = [UIColor colorWithRed:52.0/255.0 green:143.0/255.0 blue:255.0/255.0 alpha:1];
                CGContextAddEllipseInRect(context, circlRect);
                CGContextSetFillColorWithColor(context, colorOfCircle.CGColor);
                CGContextFillEllipseInRect(context, circlRect);
                
                //Moves cirle so text looks centered
                CGContextTranslateCTM (context, -2, -1); //-2, -1
            }
            
        }
        else{ //iPhone
            
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
                
                if (IS_IOS_7) {
                    
                    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme --> circle
                        
                        
                        // Steve Add circle under numbers of Today Calendar Date
                        CGRect circlRect = [self rectForButtonCellAtIndex:index];
                        
                        //Adjust CircleRect to fir ovwe number
                        circlRect.origin.x = circlRect.origin.x - 7; //-7
                        circlRect.origin.y = circlRect.origin.y - 4; //-4
                        circlRect.size.width = 35; //35
                        circlRect.size.height = 35;//35
                        
                        //UIColor *colorOfCircle = [UIColor colorWithRed:52.0/255.0 green:143.0/255.0 blue:255.0/255.0 alpha:1]; //Light Blue
                        UIColor *colorOfCircle = [UIColor colorWithRed:0.0/255.0 green:51.0/255.0 blue:255.0/255.0 alpha:1]; //Dark Blue
                        CGContextAddEllipseInRect(context, circlRect);
                        CGContextSetFillColorWithColor(context, colorOfCircle.CGColor);
                        CGContextFillEllipseInRect(context, circlRect);
                        
                        //Moves cirle so text looks centered
                        CGContextTranslateCTM (context, -2, -1); //-3, -2
                        
                    }
                    else{ //Dark Theme --> fills with square
                        
                        //Adjust width slightly since it fills too much
                        CGRect  newRect = CGRectMake(r.origin.x, r.origin.y, r.size.width - 1, r.size.height);
                        
                        //Fills the Today Tile with a Blue Color
                        CGContextSetRGBFillColor(context, 52.0/255.0, 143.0/255.0, 255.0/255.0, 1);//blue
                        CGContextFillRect(context, newRect);
                    }
                    
                    
                }
                else{ //iOS 6 --> fills with square
                    
                    //Adjust width slightly since it fills too much
                    CGRect  newRect = CGRectMake(r.origin.x, r.origin.y, r.size.width - 1, r.size.height);
                    
                    //Fills the Today Tile with a Blue Color
                    CGContextSetRGBFillColor(context, 52.0/255.0, 143.0/255.0, 255.0/255.0, 1);
                    CGContextFillRect(context, newRect);
                }
                
                
            }
            else{ //Picture Calendar
                
                // Steve Add circle under numbers of Today Calendar Date
                CGRect circlRect = [self rectForButtonCellAtIndex:index];
                
                //Adjust CircleRect to make larger
                circlRect.origin.x = circlRect.origin.x - 7; //-7
                circlRect.origin.y = circlRect.origin.y - 4; //-4
                //Steve iPad ?
                circlRect.size.width = 35;
                circlRect.size.height = 35;
                
                UIColor *colorOfCircle = [UIColor colorWithRed:52.0/255.0 green:143.0/255.0 blue:255.0/255.0 alpha:1];
                CGContextAddEllipseInRect(context, circlRect);
                CGContextSetFillColorWithColor(context, colorOfCircle.CGColor);
                CGContextFillEllipseInRect(context, circlRect);
                
                //Moves cirle so text looks centered
                CGContextTranslateCTM (context, -2, -1); //-2, -1
                
            }
        }
        


	}
	
	int index = 0;
    
    UIFont *font;
    UIFont *font2;
    
    //Steve iPad
    if (IS_IPAD) {
        //font= [UIFont boldSystemFontOfSize:iPadDateFontSize];
        //font2=[UIFont boldSystemFontOfSize:iPadDotFontSize];
        
        font = [UIFont fontWithName:@"HelveticaNeue" size:iPadDateFontSize];//Steve
        font2 = [UIFont fontWithName:@"HelveticaNeue" size:iPadDotFontSize];//Steve
    }
    else{ //iPhone
        
        // Steve - adjust font for Classic vs Picture Calendar
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                font = [UIFont fontWithName:@"HelveticaNeue" size:dateFontSize];//Steve - Regular text
                font2 = [UIFont fontWithName:@"HelveticaNeue" size:dotFontSize]; //Steve - Regular text
            }
            else{ //Dark Theme
                
                font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dateFontSize];//Steve - Bold
                font2 = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dotFontSize]; //Steve - Bold
            }
            
            
        }
        else{ //Picture Calendar
            
            font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dateFontSizePictureCalendar];//Steve - Bold
            font2 = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dotFontSizePictureCalendar]; //Steve - Bold
        }
    }
    
    

     
	
    UIColor *color;
    
     if ( [sharedDefualts boolForKey:@"CalendarTypeClassic"]) {
         
         color = [UIColor grayColor];
     }
     else{ //Picture Calendar
         
         color = [UIColor lightGrayColor];
     }
	
	
	if(firstOfPrev>0){
        
		[color set];
        
		for(int i = firstOfPrev;i<= lastOfPrev;i++){ //draws the marks on Calendar that are in previous month??
            
			r = [self rectForCellAtIndex:index];
            
			if ([marks count] > 0)
				if ([marks count] > index) {
					[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
				}else {
					[self drawTileInRect:r day:i mark:NO font:font font2:font2];
				}
            else
                [self drawTileInRect:r day:i mark:NO font:font font2:font2];
			index++;
		}
	}
	
    
    if ( [sharedDefualts boolForKey:@"CalendarTypeClassic"]) {
        
        color = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];//Font color of numbers on Calendar
    }
    else{ //Picture Calendar
        
        color = [UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1];//Font color of numbers on Calendar
    }
    
    
	[color set];
    
    
    ///////////////////  Draws the marks on the Calendar for the month /////////////////////////
	for(int i=1; i <= daysInMonth; i++){
		
		r = [self rectForCellAtIndex:index];
        
        
		if(today == i) [[UIColor whiteColor] set];
    
		
		if ([marks count] > 0) 
			if ([marks count] > index) {
				[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
			}else {
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
			}
			else
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
		if(today == i) [color set];
		index++;
	}
	
    
	[[UIColor grayColor] set];
	int i = 1;
    
	while(index % 7 != 0){
        
		r = [self rectForCellAtIndex:index] ;
        
		if ([marks count] > 0) //Draws marks on next months
			if ([marks count] > index) {
				[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
			}else {
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
			}
			else
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
		i++;
		index++;
	}
    

    
}


//Highlights day with image
- (void) selectDay:(int)day {
       //NSLog(@"TKCalendarMonthTiles selectDay");
	
	int pre = firstOfPrev < 0 ?  0 : lastOfPrev - firstOfPrev + 1;
	
	int tot = day + pre;
	int row = tot / 7;
	int column = (tot % 7)-1;
	
	selectedDay = day;
	selectedPortion = 1;
	
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
	
	if(day == today){
        
        self.currentDay.shadowOffset = CGSizeMake(0, 1);
        self.dot.shadowOffset = CGSizeMake(0, 1);
		markWasOnToday = YES;
    
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245){ //Light Theme
                 
                    self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture.png"]; //Steve - Circle Image
                    //self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Black"]; //Steve - Black Circle Image
                }
                else{ //Dark Theme
                    
                    if (IS_IPAD) {
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_iPad.png"]; //Steve - Square Flatter image
                        CGRect r = self.selectedImageView.frame;
                    }
                    else{
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile.png"]; //Steve - Square Flatter image
                    }
                 
                }

            }
            else{ //iOS 6
                
                 self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile.png"]; //Steve - Square Flatter image
            }
            
            
        }
        else{ //Picture Calendar
            
            if (IS_IPAD) {
                  self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture_iPad.png"]; //Steve - Circle Image
            }
            else{
                  self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture.png"]; //Steve - Circle Image
            }
            
        }

        
	}
    else if(markWasOnToday)
    {
		self.dot.shadowOffset = CGSizeMake(0, -1);
		self.currentDay.shadowOffset = CGSizeMake(0, -1);
		markWasOnToday = NO;
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IPAD) {
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_iPad.png"];//Steve - Flatter image
            }
            else{
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"];//Steve - Flatter image
            }
        
    
        }
        else{ //Picture Calendar
            
            if (IS_IPAD) {
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture_iPad.png"]; //Steve - Circle Image
            }
            else{
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture.png"]; //Steve - Circle Image
            }
            
        }
        
	}
	
	[self addSubview:self.selectedImageView];
    
    
    
	self.currentDay.text = [NSString stringWithFormat:@"%d",day];
    
	
	if ([marks count] > 0){
		if([[marks objectAtIndex: row * 7 + column] boolValue]){
			[self.selectedImageView addSubview:self.dot];
		}else{
			[self.dot removeFromSuperview];
		}
	}else{
		[self.dot removeFromSuperview];
	}
	
	if(column < 0){
		column = 6;
		row--;
	}
	
    
    
	CGRect r = self.selectedImageView.frame;
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7) {
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                r.origin.x = (column*46) + 2;//Adjust circle position
                r.origin.y = (row*38)-1;
            }
            else{ //Dark Theme
                
                if (IS_IPAD) {
                    r.origin.x = (column*tileWidth);// +11  Adjust
                    r.origin.y = (row*tileHeight);// +8  Adjust
                }
                else{
                    r.origin.x = (column*46);
                    r.origin.y = (row*38)-1;
                }
            }
        }
        else{ //iOS 6
            
            r.origin.x = (column*46);
            r.origin.y = (row*38)-1;
        }
        
    }
    else{ //Photo Calendar
        
        //Steve iPad
        if (IS_IPAD)
        {
            r.origin.x = (column*tileWidth) + 11;// +11  Adjust circle position
            r.origin.y = (row*tileHeight) + 8;// +8  Adjust circle position
        }
        else //iPhone
        {
            r.origin.x = (column*46) + 2;//Adjust circle postion
            r.origin.y = (row*38)-1;

        }
        
    }

	self.selectedImageView.frame = r;
    
}

- (NSDate*) dateSelected
{
    //NSLog(@"TKCalendarMonthTiles dateSelected");
    
	if(selectedDay < 1 || selectedPortion != 1) return nil;
	
	TKDateInformation info = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	info.hour = 0;
	info.minute = 0;
	info.second = 0;
	info.day = selectedDay;
	
	
	NSDate *d = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	//NSLog(@"%@",d);
	return d;
}

- (void) reactToTouch:(UITouch*)touch down:(BOOL)down {
    //NSLog(@"TKCalendarMonthTiles reactToTouch");
    
	CGPoint p = [touch locationInView:self];
	if(p.y > self.bounds.size.height || p.y < 0) return;
    
    int column,row;
    
    //Steve iPad
    if (IS_IPAD) {
        column= p.x / tileWidth, row = p.y / tileHeight;
    }
    else//iPhone
    {
        column = p.x / 46, row = p.y / 38; //column = p.x / 46, row = p.y / 44; ???
    }
	
	
	int day = 1, portion = 0;
    
    //Steve iPad
    if (IS_IPAD) {
        if(row == (int) (self.bounds.size.height / tileHeight)) row --;
    }
    else
    {
        if(row == (int) (self.bounds.size.height / 38)) row --;//if(row == (int) (self.bounds.size.height / 44)) row --;???
    }
	
	
    
	int fir = firstWeekday - 1;
	if(!startOnSunday && fir == 0) fir = 7;
	if(!startOnSunday) fir--;
	
	
	if(row==0 && column < fir){
		day = firstOfPrev + column;
	}else{
		portion = 1;
		day = row * 7 + column  - firstWeekday+2;
		if(!startOnSunday) day++;
		if(!startOnSunday && fir==6) day -= 7;
		
	}
	if(portion > 0 && day > daysInMonth){
		portion = 2;
		day = day - daysInMonth;
	}
	
    
	NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
	if(portion != 1){ //For Gray Tiles in Next & Previous Months

		markWasOnToday = YES;
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) {//Light Theme
                    
                    self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Black.png"]; //Steve - Black Circle Image
                }
                else{ //Dark Theme
                    
                    if (IS_IPAD) {
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_iPad.png"]; //Steve - Square Image
                    }
                    else{
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"]; //Steve - Square Image
                    }

                }
            }
            else{//iOS 6
                
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"]; //Steve - Square Image
            }
            
        }
        else{ //Photo Calendar
            
            if (IS_IPAD) {
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture_iPad.png"]; //Steve - Circle Image
            }
            else{
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture.png"]; //Steve - Circle Image
            }
            
            
        }
        
        
	}else if(portion==1 && day == today){
		self.currentDay.shadowOffset = CGSizeMake(0, 1);
		self.dot.shadowOffset = CGSizeMake(0, 1);
		markWasOnToday = YES;
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) {//Light Theme
                    
                    self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture.png"]; //Steve - Circle Image
                }
                else{ //Dark Theme
                    
                    if (IS_IPAD) {
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_iPad.png"]; //Steve - Square Image
                    }
                    else{
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile.png"]; //Steve - Square Image
                    }

                }
            }
            else{//iOS 6
                
                self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile.png"]; //Steve - Square Image
            }

            
        }
        else{ //Picture Calendar
            
            if (IS_IPAD) {
                self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture_iPad.png"]; //Steve - Circle Image
            }
            else{
                self.selectedImageView.image = [UIImage imageNamed:@"MonthTodaySelectedTile_Picture.png"]; //Steve - Circle Image
            }
            
        }
        
        
	}else if(markWasOnToday){
		self.dot.shadowOffset = CGSizeMake(0, -1);
		self.currentDay.shadowOffset = CGSizeMake(0, -1);
		markWasOnToday = NO;
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) {//Light Theme
                    
                    self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Black.png"]; //Steve - Black Circle Image
                }
                else{ //Dark Theme
                    
                    if (IS_IPAD) {
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_iPad.png"]; //Steve - Square Image
                    }
                    else{
                        self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"]; //Steve - Square Image
                    }
                
                }
            }
            else{//iOS 6
                
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"]; //Steve - Square Image
            }

            
        }
        else{ //Picture Calendar
            
            if (IS_IPAD) {
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture_iPad.png"]; //Steve - Circle Image
            }
            else{
                self.selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture.png"]; //Steve - Circle Image
            }
            
            
        }
        
	}
	
	[self addSubview:self.selectedImageView];
	self.currentDay.text = [NSString stringWithFormat:@"%d",day];
	
	if ([marks count] > 0) {
		if([[marks objectAtIndex: row * 7 + column] boolValue])
			[self.selectedImageView addSubview:self.dot];
		else
			[self.dot removeFromSuperview];
	}else{
		[self.dot removeFromSuperview];
	}
    
    
//Updates Selected Image
///////////////////////
	CGRect r = self.selectedImageView.frame;

    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7) {
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                r.origin.x = (column*46) + 2; //Adjust rect by 2 for circles. Otherwise goes off screen.
                r.origin.y = (row*38)-1;
            }
            else{ //Dark Theme
                
                if (IS_IPAD) {
                    r.origin.x = (column*tileWidth);//11
                    r.origin.y = (row*tileHeight);//8
                }
                else{ //iPhone
                    r.origin.x = (column*46);
                    r.origin.y = (row*38)-1;
                }
                

            }
        }
        else{ //iOS 6
            
            r.origin.x = (column*46);
            r.origin.y = (row*38)-1;
        }
        
        
    }
    else{ //Photo Calendar
        
        if (IS_IPAD) {
            r.origin.x = (column*tileWidth) + 11;//11
            r.origin.y = (row*tileHeight) + 8;//8
        }
        else{  //iPhone
            r.origin.x = (column*46) + 2;//Adjust rect by 2 for circles. Otherwise goes off screen.
            r.origin.y = (row*38)-1;
        }
        

    }

    

	self.selectedImageView.frame = r;
////////////////////////
    
	
	if(day == selectedDay && selectedPortion == portion) return;
	
	if(portion == 1){
		selectedDay = day;
		selectedPortion = portion;
		[target performSelector:action withObject:[NSArray arrayWithObject:[NSNumber numberWithInt:day]]];
		
	}
	else if(down){
		[target performSelector:action withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:day],[NSNumber numberWithInt:portion],nil]];
		selectedDay = day;
		selectedPortion = portion;
	}
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	//[super touchesBegan:touches withEvent:event];
	[self reactToTouch:[touches anyObject] down:NO];
} 

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	[self reactToTouch:[touches anyObject] down:NO];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
	[self reactToTouch:[touches anyObject] down:YES];
}

- (UILabel *) currentDay {
    
	if(currentDay==nil){
        
		CGRect r = self.selectedImageView.bounds;
		r.origin.y -= 2;
		currentDay = [[UILabel alloc] initWithFrame:r];
		currentDay.text = @"1";
		currentDay.textColor = [UIColor whiteColor];
		currentDay.backgroundColor = [UIColor clearColor];
		currentDay.textAlignment = NSTextAlignmentCenter;
		currentDay.shadowColor = [UIColor darkGrayColor];
		currentDay.shadowOffset = CGSizeMake(0, -1);
        
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                currentDay.font = [UIFont fontWithName:@"HelveticaNeue" size:dateFontSize];//Steve
            }
            else{
                
                //Steve iPad
                if (IS_IPAD) {
                    
                    //currentDay.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:iPadDateFontSize];//Steve
                    currentDay.font = [UIFont fontWithName:@"HelveticaNeue" size:iPadDateFontSize];//Steve
                }
                else{
                    currentDay.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dateFontSize];//Steve
                }
                
            }
            
        }
        else{ //Picture Calendar
            
            currentDay.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dateFontSizePictureCalendar];//Steve
        }

	}

	return currentDay;
}

- (UILabel *) dot{
    
	if(dot==nil){
        
        //Steve iPad
        CGRect r = self.selectedImageView.bounds;
        
        if (IS_IPAD) {
            r.origin.y += (r.size.height/2)+10; //+25
            r.size.height = 12;
        }
        else //iPhone
        {
            r.origin.y += 29;
            r.size.height -= 31;
        }
        

		dot = [[UILabel alloc] initWithFrame:r];
		dot.text = @"•";
		dot.textColor = [UIColor whiteColor];
		dot.backgroundColor = [UIColor clearColor];
		//dot.font = [UIFont boldSystemFontOfSize:dotFontSize];
		dot.textAlignment = NSTextAlignmentCenter;
		dot.shadowColor = [UIColor darkGrayColor];
		dot.shadowOffset = CGSizeMake(0, -1);
        
        
        //Steve iPad
        if (IS_IPAD) {
            dot.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:iPadDotFontSize];
        }
        else{ //iPhone
            dot.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:dotFontSize];
        }
        
	}
	return dot;
}

//Date selected image
- (UIImageView *) selectedImageView {
    
	if(selectedImageView == nil){
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        

        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            //selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected"]];
            //selectedImageView = [[UIImageView alloc] init];
            
            
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //light Theme
                    
                    selectedImageView = [[UIImageView alloc] init];
                    selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Black.png"]; //Steve - Black Circle Image
                    
                    CGRect rect = CGRectZero;
                    rect = [selectedImageView frame];
                    rect.size.width = 40;
                    rect.size.height = 40;
                    [selectedImageView setFrame:rect];
                }
                else{ //Dark Theme
                    
                    if (IS_IPAD) {
                        selectedImageView = [[UIImageView alloc] init];
                        selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_iPad.png"];//Steve - Flatter Square image
                        
                        CGRect rect = CGRectZero;
                        rect = [selectedImageView frame];
                        rect.size.width = tileWidth;
                        rect.size.height = tileHeight;
                        [selectedImageView setFrame:rect];
                    }
                    else{
                        selectedImageView = [[UIImageView alloc] init];
                        selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"];//Steve - Flatter Square image
                        
                        CGRect rect = CGRectZero;
                        rect = [selectedImageView frame];
                        rect.size.width = 46;
                        rect.size.height = 38;
                        [selectedImageView setFrame:rect];
                    }
                }
            }
            else{//iOS 6
                
                selectedImageView = [[UIImageView alloc] init];
                selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected.png"];//Steve - Flatter Square image
                
                CGRect rect = CGRectZero;
                rect = [selectedImageView frame];
                rect.size.width = 46;
                rect.size.height = 38;
                [selectedImageView setFrame:rect];
            }
            
        }
        else{ //Picture Calendar
            
            if (IS_IPAD)
            {
                selectedImageView = [[UIImageView alloc] init];
                selectedImageView.frame = CGRectMake(0, 0, tileWidth, tileHeight);
                selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture_iPad.png"]; //Steve - Circle Image
                
                CGRect rect = CGRectZero;
                rect = [selectedImageView frame];
                rect.size.width = 80;
                rect.size.height = 80;
                [selectedImageView setFrame:rect];
            }
            else
            {
                selectedImageView = [[UIImageView alloc] init];
                selectedImageView.image = [UIImage imageNamed:@"MonthDateTileSelected_Picture.png"]; //Steve - Circle Image
                
                CGRect rect = CGRectZero;
                rect = [selectedImageView frame];
                rect.size.width = 40;
                rect.size.height = 40;
                [selectedImageView setFrame:rect];
            }
            
            
        }
    }



	return selectedImageView;
}


@end

@interface TKCalendarMonthView (private)  //Called First

@property (readonly) UIScrollView *tileBox;
@property (readonly) UIImageView *topBackground;
@property (readonly) UILabel *monthYear;
@property (readonly) UIButton *leftArrow;
@property (readonly) UIButton *rightArrow;
@property (readonly) UIImageView *shadow;



@end

@implementation TKCalendarMonthView
@synthesize delegate,dataSource;
@synthesize monthButton, yearButton;
@synthesize parentVC;

- (id) initWithCallingParent:(id) callingParent {
   // NSLog(@"TKCalendarMonthView initWithCallingParent");
    
    
    self.parentVC = callingParent;
    
    //Steve - determines fisrt day of week for international compatibility
    NSUInteger firstDayOfWeek  = [[NSCalendar currentCalendar] firstWeekday];
    
    
    if (firstDayOfWeek == 1)
        return [self initWithSundayAsFirst:YES];//Sunday
    else
        return [self initWithSundayAsFirst:NO];//Monday
    

    
}

- (id) initWithSundayAsFirst:(BOOL)s {
   //NSLog(@"TKCalendarMonthView initWithSundayAsFirst");
	
    
	if (!(self = [super initWithFrame:CGRectZero])) return nil;
	sunday = s;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7) {
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                self.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
            }
            else{ //Dark Theme
                
                self.backgroundColor = [UIColor clearColor];
            }
        }
        else{//iOS 6
            
            self.backgroundColor = [UIColor clearColor];
        }
    }
    else{ //Photo Calendar
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    
    
	
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    

	
	if ([[appDelegate currentVisibleDate] isEqualToString:@""] || [appDelegate currentVisibleDate] == nil) {
		
        //[appDelegate setCurrentVisibleDate:[[NSDate date] description]]; //Steve commented
        
        //******************* Steve updated so currentVisibleDate shows correct in current
        
        appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSDate *sourceDate = [NSDate date];
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
        
        // NSLog(@"destinationDate #1 = %@", destinationDate);
        
        if(!appDelegate.currentVisibleDate || [appDelegate.currentVisibleDate isEqualToString:@""]) {
            
            NSCalendar *calendarToRemoveTime = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            [calendarToRemoveTime setLocale:[NSLocale currentLocale]];
            
            //Steve commented - was changing to GMT Time, which throws off the correct date and time
            //[calendarToRemoveTime setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            
            //sets Time for 12AM
            NSDateComponents *setTimeToDayBeginning = [calendarToRemoveTime components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:destinationDate];
            [setTimeToDayBeginning setHour:0];
            [setTimeToDayBeginning setMinute:0];
            [setTimeToDayBeginning setSecond:0];
            
            NSDate  *currentVisibleDate_Date = [calendarToRemoveTime dateFromComponents:setTimeToDayBeginning];
            
            //Steve - new currentVisibleDate starting at 12AM (beginning of day)
            [appDelegate setCurrentVisibleDate:[formater stringFromDate:currentVisibleDate_Date]];
            
           // NSLog(@"appDelegate.currentVisibleDate #11 = %@", appDelegate.currentVisibleDate);
        }
       // NSLog(@"appDelegate.currentVisibleDate #2 = %@", appDelegate.currentVisibleDate);
	}
    
    
    
	NSDate *currVisibleDate = [formater	dateFromString:[appDelegate currentVisibleDate]]; 
	
	currentTile = [[TKCalendarMonthTiles alloc] initWithMonth:[currVisibleDate firstOfMonth] marks:nil startDayOnSunday:sunday];
	[currentTile setTarget:self action:@selector(tile:)];
    
    
    if (IS_IPAD) //Used for iPad only
    {
          tileHeight=[currentTile tileHeight];
    }

	
	[self performSelector:@selector(initialySelectDate) withObject:nil afterDelay:0.1];
	
	CGRect rect = CGRectZero;
	
	rect = [currentTile frame];
    
    //Steve iPad
    if (IS_IPAD) {
        rect.size.width = tileWidth;
    }
    else{//iPhone
        rect.size.width = 46;
    }

	[currentTile setFrame:rect];
	

    CGRect r;
    
    //Steve iPad
    if (IS_IPAD) {
        r= CGRectMake(0, 0, self.tileBox.bounds.size.width+4, self.tileBox.bounds.size.height + self.tileBox.frame.origin.y);
    }
    else //iPhone
    {
        r= CGRectMake(0, 0, self.tileBox.bounds.size.width, self.tileBox.bounds.size.height + self.tileBox.frame.origin.y);
    }
    
    self.frame = r;


    //NSLog(@"self.frame = %@", NSStringFromCGRect(self.frame));
    //NSLog(@"tileBox.frame = %@", NSStringFromCGRect(tileBox.frame));
    //NSLog(@"CurrentTile.frame = %@", NSStringFromCGRect(currentTile.frame));
    
 

    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {

        if (IS_IOS_7) {
            
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] != 245) { //Dark Theme
                
                [self addSubview:self.topBackground];
            }

        }
        else{ //iOS 6
            
            [self addSubview:self.topBackground];
        }
        
    }


	[self.tileBox addSubview:currentTile];
	[self addSubview:self.tileBox];
	
	NSDate *date = [NSDate date];
	self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[date month],[date year]];
	[self addSubview:self.monthYear];
	[self addSubview:self.leftArrow];
	[self addSubview:self.rightArrow];
	
	monthaArray = [[NSMutableArray alloc] initWithObjects:@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December", nil];
	yearsArray  = [[NSMutableArray alloc] init];
	
	for (int i = 1900; i < 2100; i++) {
		[yearsArray  addObject:[NSString stringWithFormat:@"%d", i]];
	}
	
    
    UIImage *monthButtonImage;
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7){
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                monthButtonImage = [UIImage imageNamed:@"MonthBox_Black.png"];
            }
            else{ //Dark theme
                
                monthButtonImage = [UIImage imageNamed:@"MonthBox.png"];
            }
            
        }
        else{ //iOS 6
            
            monthButtonImage = [UIImage imageNamed:@"MonthBox_Old.png"];
        }

    }
    else{ //Photo Calendar
        
        monthButtonImage = [UIImage imageNamed:@"MonthBox.png"];
    }
    
    
	
	
	monthButton = [[UIButton alloc] initWithFrame:CGRectMake(60, 5, monthButtonImage.size.width, monthButtonImage.size.height)];
	[monthButton setTitle:@"Month" forState:UIControlStateNormal];
	[monthButton setImage:monthButtonImage forState:UIControlStateNormal];
	[monthButton addTarget:self action:@selector(monthButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[monthButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	[monthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self addSubview:monthButton];

	
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	if ([appDelegate.currentVisibleDate isEqualToString:@""]) {
		appDelegate.currentVisibleDate = [formater stringFromDate:[NSDate date]];
	}
	NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
	[formater setDateFormat:@"MMMM"];
	
	UILabel *monthTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, monthButtonImage.size.width - 20, monthButtonImage.size.height - 4)];
	//[monthTitleLabel setFont:[UIFont boldSystemFontOfSize:28]];//12
    [monthTitleLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];//Steve
	[monthTitleLabel setTag:311];
	[monthTitleLabel setText:[formater stringFromDate:tempDate]];
	[monthTitleLabel setBackgroundColor:[UIColor clearColor]];
	[monthButton addSubview:monthTitleLabel];
	
    
    UIImage *yearButtonImage;
    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7){
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                yearButtonImage = [UIImage imageNamed:@"YearBox_Black.png"];
            }
            else{ //Dark theme
                
                yearButtonImage = [UIImage imageNamed:@"YearBox.png"];
            }
            
        }
        else{ //iOS 6
            
            yearButtonImage = [UIImage imageNamed:@"YearBox_Old.png"];
        }
        
    }
    else{ //Photo Calendar
        
        yearButtonImage = [UIImage imageNamed:@"YearBox.png"];
    }

    
	
	yearButton = [[UIButton alloc] initWithFrame:CGRectMake(320 - 130, 5, yearButtonImage.size.width, yearButtonImage.size.height)];
	[yearButton setTitle:@"Year" forState:UIControlStateNormal];
	[yearButton setImage:yearButtonImage forState:UIControlStateNormal];
	[yearButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	[yearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[yearButton addTarget:self action:@selector(yearButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:yearButton];
    
    
    //Steve iPad - Adjust Month & Year Dropdown Position
    if (IS_IPAD) {
        
        //Find Center with Month & Year
        int distance = 25;
        int x =  self.frame.size.width/2 - (monthButtonImage.size.width + yearButtonImage.size.width + distance)/2;
        
        monthButton.frame = CGRectMake(x , 5, monthButtonImage.size.width, monthButtonImage.size.height);
        yearButton.frame = CGRectMake(monthButton.frame.origin.x + monthButton.frame.size.width + distance, 5, yearButtonImage.size.width, yearButtonImage.size.height);
    }
    
	
	UILabel *yearTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, yearButtonImage.size.width - 20, yearButtonImage.size.height - 4)];
	[yearTitleLabel setTag:312];
	//[yearTitleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [yearTitleLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];//Steve
	[yearTitleLabel setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]];
	[yearTitleLabel setBackgroundColor:[UIColor clearColor]];
	[yearButton addSubview:yearTitleLabel];
	
	UIView *scrollOptBGView = [[ UIView alloc ] initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height)];
	[scrollOptBGView setBackgroundColor:[UIColor clearColor]];
	[scrollOptBGView setTag:411];
	[scrollOptBGView setAlpha:0];
    
    
    scrollOptTblView = [[UITableView alloc] initWithFrame:CGRectMake(monthButton.frame.origin.x, monthButton.frame.origin.y + monthButton.frame.size.height, monthButton.frame.size.width, 200) style:UITableViewStylePlain];
	[scrollOptTblView setDataSource:self];
	[scrollOptTblView setDelegate:self];
	[scrollOptTblView setRowHeight:25];
	[scrollOptTblView.layer setCornerRadius:5.0];
	scrollOptTblView.layer.masksToBounds = YES;
	scrollOptTblView.layer.borderColor = [UIColor grayColor].CGColor;  //Steve  [UIColor purpleColor].CGColor;
	scrollOptTblView.layer.borderWidth = 1.5;	
	[scrollOptBGView addSubview:scrollOptTblView];
    
    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if (IS_IOS_7){
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                //[monthTitleLabel setTextColor:[UIColor colorWithRed:50.0/255.0 green:125.0/255.0 blue:255.0/255.0 alpha:1]];
                //[yearTitleLabel setTextColor:[UIColor colorWithRed:50.0/255.0 green:125.0/255.0 blue:255.0/255.0 alpha:1]];
                [monthTitleLabel setTextColor:[UIColor blackColor]];
                [yearTitleLabel setTextColor:[UIColor blackColor]];
            }
            else{ //Dark theme
                
                [monthTitleLabel setTextColor:[UIColor whiteColor]];
                [yearTitleLabel setTextColor:[UIColor whiteColor]];
            }
            
        }
        else{ //iOS 6
            
            [monthTitleLabel setTextColor:[UIColor whiteColor]];
            [yearTitleLabel setTextColor:[UIColor whiteColor]];
        }
        
    }
    else{ //Photo Calendar
        
        [monthTitleLabel setTextColor:[UIColor whiteColor]];
        [yearTitleLabel setTextColor:[UIColor whiteColor]];
    }

    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        
        if (!IS_IOS_7) { // iOS 6
            
            [self addSubview:self.shadow];
            self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height + 21, self.shadow.frame.size.width, self.shadow.frame.size.height);

        }
  
    }
    
    //NSLog(@"self.frame 2 = %@", NSStringFromCGRect(self.frame));

	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"eee"];
	[dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	TKDateInformation sund;
	sund.day = 5;
	sund.month = 12;
	sund.year = 2010;
	sund.hour = 0;
	sund.minute = 0;
	sund.second = 0;
	
	NSTimeZone *tz = [NSTimeZone timeZoneForSecondsFromGMT:0];
	NSString * sun = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 6;
	NSString *mon = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 7;
	NSString *tue = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 8;
	NSString *wed = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 9;
	NSString *thu = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 10;
	NSString *fri = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 11;
	NSString *sat = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	
	NSArray *ar;
	if(sunday) ar = [NSArray arrayWithObjects:sun,mon,tue,wed,thu,fri,sat,nil];
	else ar = [NSArray arrayWithObjects:mon,tue,wed,thu,fri,sat,sun,nil];
	
    
    //Adds Day header (Sun, Mon, Tue, etc)
	int i = 0;
	for(NSString *s in ar){
        
        UILabel *label ;
        
        //Steve iPad
        if (IS_IPAD) {
            
            label= [[UILabel alloc] initWithFrame:CGRectMake(tileWidth * i, 62, tileWidth, 20)];
        }
        else
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(46 * i, 29, 46, 15)];
        }
        

		[self addSubview:label];
		label.text = s;
		label.textAlignment = NSTextAlignmentCenter;
        if (!IS_IOS_7) 	label.shadowColor = [UIColor whiteColor];
		label.shadowOffset = CGSizeMake(0, 0);
		label.backgroundColor = [UIColor clearColor];
        

        //Steve iPad
        if (IS_IPAD) {
              label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20];//Steve - changes Week day name on top of Calendar(Mon, Tues, etc)
        }
        else//iPhone
        {
              label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:11];//Steve - changes Week day name on top of Calendar(Mon, Tues, etc)
        }


		i++;
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    label.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
                }
                else{ //Dark Theme
                    
                    label.textColor = [UIColor whiteColor];
                }

            }
            else{ //iOS 6
                
                label.textColor = [UIColor whiteColor];
            }
            
            
        }
        else{ //Photo Calendar
            
              label.textColor = [UIColor whiteColor];
        }

        
        	}
	[self addSubview:scrollOptBGView];
    
    
    ////////////////////  Added swipeRight and Swipe Left gestures to change month //////////
    
    UISwipeGestureRecognizer *SwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightGestureRecognizer:)];
    SwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    SwipeRight.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:SwipeRight];
    SwipeRight.delegate = self;
    
    UISwipeGestureRecognizer *SwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLefttGestureRecognizer:)];
    SwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    SwipeLeft.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:SwipeLeft];
    SwipeLeft.delegate = self;
    
////////////////////////////////////////////////////////////////////////////////////
    
    
    
	return self;
}

- (void)swipeRightGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer 
{
   // NSLog(@"%s",__FUNCTION__);
    
    [self.leftArrow sendActionsForControlEvents:UIControlEventTouchUpInside];    
}
- (void)swipeLefttGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer 
{
    //NSLog(@"%s",__FUNCTION__);
    [self.rightArrow sendActionsForControlEvents:UIControlEventTouchUpInside];    

    
}



- (void) initialySelectDate {
    //NSLog(@"TKCalendarMonthView initialySelectDate");
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	NSDate *date = [formater dateFromString:appDelegate.currentVisibleDate]; 
	
	[currentTile selectDay:[date day]];
	[parentVC reloadTableView];
}


- (void) monthButton_Clicked:(UIButton *) sender {
    // NSLog(@"TKCalendarMonthView monthButton_Clicked");
    
	sender.tag = 211;
	isMonthSelected = YES;
	[scrollOptTblView setFrame:CGRectMake(monthButton.frame.origin.x, monthButton.frame.origin.y + monthButton.frame.size.height, monthButton.frame.size.width, 150)];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[[self viewWithTag:411] setAlpha:1];
	[UIView commitAnimations];
	[scrollOptTblView reloadData];
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] inSection:0];
	[scrollOptTblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	[self performSelector:@selector(selectedRow) withObject:nil afterDelay:0.25];
}

- (void) yearButton_Clicked:(UIButton *) sender {
	isMonthSelected = NO;
	[scrollOptTblView setFrame:CGRectMake(yearButton.frame.origin.x, yearButton.frame.origin.y + yearButton.frame.size.height, yearButton.frame.size.width, 150)];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[[self viewWithTag:411] setAlpha:1];
	[UIView commitAnimations];
	
	sender.tag = 212;
	[scrollOptTblView reloadData];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[yearsArray indexOfObject:[(UILabel*)[yearButton viewWithTag:312] text]] inSection:0];
	[scrollOptTblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	[self performSelector:@selector(selectedRow) withObject:nil afterDelay:0.5];
}

- (void) selectedRow {
	NSIndexPath *indexPath;
	
	if (isMonthSelected) {
		indexPath = [NSIndexPath indexPathForRow:[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] inSection:0];
	}else {
		indexPath = [NSIndexPath indexPathForRow:[yearsArray indexOfObject:[(UILabel*)[yearButton viewWithTag:312] text]] inSection:0];
	}
	
	[[scrollOptTblView cellForRowAtIndexPath:indexPath] setSelected:YES];
}

-(int)compairDateWithoutTimeDate1:(NSDate *) dateOne date2:(NSDate *) dateTwo {
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
	
	NSDateComponents *date1Components = [calendar components:comps 
													fromDate: dateOne];
	NSDateComponents *date2Components = [calendar components:comps 
													fromDate: dateTwo];
	
	dateOne = [calendar dateFromComponents:date1Components];
	dateTwo = [calendar dateFromComponents:date2Components];
	
	NSComparisonResult result = [dateOne compare:dateTwo];
	if (result == NSOrderedAscending) {
		return 1; // Date One is smaller then date two
	} else if (result == NSOrderedDescending) {
		return 3; // Date One is bigger then date two
	}  else {
		return 2; // Date One is equal date two
	}
}


- (void)animateMonth {
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	if ([self compairDateWithoutTimeDate1:[formater dateFromString:[appDelegate currentVisibleDate]] date2:destinationDate] == 2) 
	{
		return ;
	}
	
	
	TKDateInformation info = [[NSDate date] dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day	= 1;
	info.hour	= 0;
	info.minute = 0;
	info.second = 0;
	
	NSDateFormatter *abc = [[NSDateFormatter alloc]init];
	[abc setDateFormat:@"yyyy-MM"];
	NSDate *currentMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSString *curMon = [abc stringFromDate:currentMonth];
	
	NSDateFormatter *xyz = [[NSDateFormatter alloc]init];
	[xyz setDateFormat:@"yyyy-MM"];
	appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	NSDate *date = [formater dateFromString:appDelegate.currentVisibleDate];
	NSString *local = [xyz stringFromDate:date];
	
	
	if ([curMon isEqualToString:local])
	{
		appDelegate.currentVisibleDate = [formater stringFromDate:destinationDate];
		[currentTile selectDay:[[[[appDelegate currentVisibleDate] substringFromIndex:8] substringToIndex:2] intValue]];
		NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
		[formater setDateFormat:@"MMMM"];
		[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
		[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
		return;
	}
	
	appDelegate.currentVisibleDate = [formater stringFromDate:destinationDate];
	[self changeMonthAnimation:nil];
	[currentTile selectDay:[[[[appDelegate currentVisibleDate] substringFromIndex:8] substringToIndex:2] intValue]];

	//NSLog(@"********appdelegate.currentVisibleDate = %@",appDelegate.currentVisibleDate);
	
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)]){
		[delegate calendarMonthView:self monthDidChange:[NSDate date]];
	}
	NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
	
	[formater setDateFormat:@"MMMM"];
	[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
	[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
}

- (void) changeMonthAnimation:(UIView *) sender {
	BOOL isNext = (sender.tag == 1);
	NSDate *nextMonth;
	NSDateFormatter *formate = [[NSDateFormatter alloc] init];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	if (!sender) 
	{
		[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];                                       //Steve - changes time. Why???
        //[formate setTimeZone:[NSTimeZone systemTimeZone]]; //Steve added
		NSString *dateString =  [[appDelegate.currentVisibleDate substringToIndex:8] stringByAppendingFormat:@"01 18:30:0000 +0000"];
		
        //****** Steve changed - 
		NSDate *date = [formate dateFromString:dateString]; //Was: dateString
		nextMonth = [date monthWithDate:date];
	}
	else 
	{
		if (sender.tag == 211 || sender.tag == 212)
		{
			[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			NSDate *date = [formate dateFromString:appDelegate.currentVisibleDate];
			nextMonth = [date monthWithDate:date];
		}
		else 
		{
			nextMonth = isNext ? [currentTile.monthDate nextMonth] : [currentTile.monthDate previousMonth];
		}
	}
	TKDateInformation nextInfo = [nextMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	//	nextInfo.weekday =  [self weekDayForDate:appDelegate.currentVisibleDate];
	NSDate *localNextMonth = [NSDate dateFromDateInformation:nextInfo];
	
	NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:nextMonth startOnSunday:sunday];
	NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
	TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:nextMonth marks:ar startDayOnSunday:sunday];
	[newTile setTarget:self action:@selector(tile:)];
	
	int overlap =  0;
    
    
    //////////
 
    
    ////////////
	
	if(isNext){
        
        //Steve iPad
        if (IS_IPAD) {
            overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : tileHeight;
        }
        else
        {
            //Steve iPad
            if (IS_IPAD) {
                overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? tileHeight : 0;
            }
            else
            {
                 overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : 38;//overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? 44 : 0;
            }
           
        }
        
		
	}else{
		overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? 38 : 0;
	}
	
	float y = isNext ? currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap;
	
	newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);
	[self.tileBox addSubview:newTile];
	[self.tileBox bringSubviewToFront:currentTile];
	
	self.userInteractionEnabled = NO;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDidStopSelector:@selector(animationEnded)];
	[UIView setAnimationDuration:0.4];
	
	currentTile.alpha = 0.0;
	
	if(isNext) {
		currentTile.frame = CGRectMake(0, -1 * currentTile.bounds.size.height + overlap, currentTile.frame.size.width, currentTile.frame.size.height);
		newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
		
	}else {
		
		newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		currentTile.frame = CGRectMake(0,  newTile.frame.size.height - overlap, currentTile.frame.size.width, currentTile.frame.size.height);
		
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
	}
	
	[UIView commitAnimations];
	oldTile = currentTile;
	currentTile = newTile;
	
	monthYear.text = [NSString stringWithFormat:@"%@ %@",[localNextMonth month],[localNextMonth year]];
	
	[currentTile selectDay:[localNextMonth day]];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.2];
	[[parentVC dayDetailTableView] setFrame:CGRectMake(0, self.frame.size.height + 2, 320, [parentVC view].frame.size.height - self.frame.size.height)];
	[UIView commitAnimations];
}

- (int) weekDayForDate:(NSString *) date {
	NSDateFormatter *formater = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDateComponents *comps = [[NSCalendar currentCalendar] components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:[formater dateFromString:date]];
	int weekday = [comps weekday];
	return weekday;
}

#pragma mark -
#pragma mark Table view data source
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (isMonthSelected) {
		return [monthaArray count];
	}else {
		return [yearsArray count];
	}
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"TKCalendarMonthView cellForRowAtIndexPath");
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
	[cell.textLabel setTextAlignment:NSTextAlignmentCenter];
	//[cell.textLabel setFont:[UIFont boldSystemFontOfSize:15]]; //Steve - iOS 7 changed system font so old one too large
    [cell.textLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Bold" size:13]];//13
	
    
	if (isMonthSelected) {
		[cell.textLabel setText:[monthaArray objectAtIndex:[indexPath row]]];
		if ([[monthaArray objectAtIndex:[indexPath row]] isEqualToString:[(UILabel*)[monthButton viewWithTag:311] text]]) {
			//			[cell setSelected:YES];
		}
	}else {
		if ([[yearsArray objectAtIndex:[indexPath row]] isEqualToString:[(UILabel*)[monthButton viewWithTag:312] text]]) {
			//			[cell setSelected:YES];
		}
		[cell.textLabel setText:[yearsArray objectAtIndex:[indexPath row]]];
	}
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
#pragma mark -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NSString *dateStr;
	//	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	if (isMonthSelected) 
	{
		[(UILabel*)[monthButton viewWithTag:311] setText:[monthaArray objectAtIndex:[indexPath row]]];
		if ([indexPath row] + 1 < 10) {
            //Steve changed
        //dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 18:30:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
			dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 00:00:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
		}else {
            //Steve changed
        //dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 18:30:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
			dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 00:00:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
		}
		
	} 
	else {
		[(UILabel*)[yearButton viewWithTag:312] setText:[yearsArray objectAtIndex:[indexPath row]]];
		
		if ( 1+[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] < 10) {
           // dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 18:30:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]]; 
			dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 00:00:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]]; //Steve changed
		}else {
            //dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 18:30:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]];
			dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 00:00:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]]; //Steve changed
		}
	}
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.currentVisibleDate  = dateStr;
	NSDateFormatter *formate = [[NSDateFormatter alloc] init];
	[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDate *date = [formate dateFromString:appDelegate.currentVisibleDate];
	
	if (isMonthSelected) {
		[self changeMonthAnimation:monthButton];
	}else {
		[self changeMonthAnimation:yearButton];
	}
	
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)]){
		[delegate calendarMonthView:self monthDidChange:date];
	}
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.7];
	[[self viewWithTag:411] setAlpha:0];
	[UIView commitAnimations];
}

#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	if (![touches containsObject:scrollOptTblView]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.7];
		[[self viewWithTag:411] setAlpha:0];
		[UIView commitAnimations];
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}

- (void) changeMonth:(UIButton *)sender {
	//NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
//	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss  +0530"];
	
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	
	[self changeMonthAnimation:sender];
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
		[delegate calendarMonthView:self monthDidChange:currentTile.monthDate];
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	appDelegate.currentVisibleDate = [dateFormatter stringFromDate:currentTile.monthDate];
	
	
	
	NSDate *tempDate =  [dateFormatter dateFromString:appDelegate.currentVisibleDate];
	
	[dateFormatter setDateFormat:@"MMMM"];
	[(UILabel*)[monthButton viewWithTag:311] setText:[dateFormatter stringFromDate:tempDate]];
	[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
	
	//NSLog(@"Date: %@", currentTile.monthDate);
	
}

- (void) animationEnded {
	self.userInteractionEnabled = YES;
	oldTile = nil;
	[parentVC reloadTableView];
    
    //Steve commented since "Loading View" is no longer used
    //[parentVC removeView]; //Steve added to remove "Loading..." view
}

- (NSDate*) dateSelected {
	
	//NSLog(@"%@",currentTile.dateSelected);
	return [currentTile dateSelected];
}

- (NSDate*) monthDate {
	return [currentTile monthDate];
}

- (void) selectDate:(NSDate*)date{
   // NSLog(@"selectDate");
    
	TKDateInformation info = [date dateInformation];
	
	NSDate *month = [date firstOfMonth];
	
	if([month isEqualToDate:[currentTile monthDate]]){
		[currentTile selectDay:info.day];
		return;
	}else {
		
		//NSDate *month = [self firstOfMonthFromDate:date];
		NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:month startOnSunday:sunday];
		NSArray *data = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
		TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:month 
																			  marks:data 
																   startDayOnSunday:sunday];
		[newTile setTarget:self action:@selector(tile:)];
		[currentTile removeFromSuperview];
		currentTile = newTile;
		
		[self.tileBox addSubview:currentTile];
        
        
        //Steve iPad
        if (IS_IPAD) {
            
            self.tileBox.frame = CGRectMake(0, CalendarTopBarHeight, newTile.frame.size.width, newTile.frame.size.height);
        }
        else
        {
           self.tileBox.frame = CGRectMake(0, 38, newTile.frame.size.width, newTile.frame.size.height);// self.tileBox.frame = CGRectMake(0, 44, newTile.frame.size.width, newTile.frame.size.height);
        }
        
        
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
		self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[month month],[month year]];
		[currentTile selectDay:info.day];
	}
}

- (void) reload {
	NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:[currentTile monthDate] startOnSunday:sunday];
	NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
	
	TKCalendarMonthTiles *refresh = [[TKCalendarMonthTiles alloc] initWithMonth:[currentTile monthDate] marks:ar startDayOnSunday:sunday];
	[refresh setTarget:self action:@selector(tile:)];
	
	[self.tileBox addSubview:refresh];
	[currentTile removeFromSuperview];
	currentTile = refresh;
}

- (void) tile:(NSArray *) ar {
    
	if([ar count] < 2) {
        
		if ([delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
			[delegate calendarMonthView:self didSelectDate:[self dateSelected]];
	} else {
        
		int direction = [[ar lastObject] intValue];
		UIButton *b = direction > 1 ? self.rightArrow : self.leftArrow;
		[self changeMonthAnimation:b];
		
		int day = [[ar objectAtIndex:0] intValue];
		TKDateInformation info = [[currentTile monthDate] dateInformation];
		info.day = day;
		NSDate *dateForMonth = [NSDate dateFromDateInformation:info]; 
		[currentTile selectDay:day];
        
		if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
			[delegate calendarMonthView:self monthDidChange:dateForMonth];		
		
		OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
		NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		appDelegate.currentVisibleDate = [formater stringFromDate:dateForMonth];
		NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
		[formater setDateFormat:@"MMMM"];
		[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
		[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]];
	}
	[[parentVC dayDetailTableView] reloadData];
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	appDelegate.currentVisibleDate = [formater stringFromDate:[currentTile dateSelected]];
	
}

//Steve comments - background color or image of the bar above the month calendar
- (UIImageView *) topBackground{
    
	if(topBackground==nil){
        
        topBackground = [[UIImageView alloc] init];
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        //NSLog(@" DefaultAppThemeColorRed =  %f", [sharedDefaults floatForKey:@"DefaultAppThemeColorRed"]);
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] != 245) { //Dark Theme
                    
                    if (IS_IPAD) {
                        UIImage *myImage = [UIImage imageNamed:@"MonthHeader_iPad.png"];
                        topBackground = [[UIImageView alloc]initWithImage:myImage];
                    }
                    else{
                        UIImage *myImage = [UIImage imageNamed:@"MonthHeader.png"];
                        topBackground = [[UIImageView alloc]initWithImage:myImage];
                    }
                 
                }
  
            }
            else{   //iOS 6
                
                UIImage *myImage = [UIImage imageNamed:@"MonthHeader.png"];
                topBackground = [[UIImageView alloc]initWithImage:myImage];
            }
            
        }

	}
	
	CGRect rect = CGRectZero;
	
	rect = [topBackground frame];
	rect.size.width = rect.size.width;
	[topBackground setFrame:rect];
    topBackground.frame = CGRectMake(-1, -1, rect.size.width + 2, rect.size.height + 3); //Steve - adjust frame
	
	return topBackground;
}

- (UILabel *) monthYear {
    
	if(monthYear == nil) {
        
        //Steve iPad
        if (IS_IPAD) {
            monthYear = [[UILabel alloc] initWithFrame:CGRectInset(CGRectMake(0, 15, self.tileBox.frame.size.width, 38), 40, 6)];
        }
        else//iPhone
        {
            monthYear = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tileBox.frame.size.width, 38)];
            //monthYear = [[UILabel alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, self.tileBox.frame.size.width, 38), 40, 6)];
        }
        
		
		monthYear.textAlignment = NSTextAlignmentCenter;
		monthYear.backgroundColor = [UIColor clearColor];
		//monthYear.font = [UIFont boldSystemFontOfSize:22];
        monthYear.font =  [UIFont fontWithName:@"HelveticaNeue-Bold" size:22];//Steve
		monthYear.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:0];
	}
	return monthYear;
}

- (UIButton *) leftArrow{
    
	if(leftArrow==nil){
		leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		leftArrow.tag = 0;
		[leftArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
		leftArrow.frame = CGRectMake(0, 0, 48, 38);
		[leftArrow setShowsTouchWhenHighlighted:YES];
		[leftArrow setReversesTitleShadowWhenHighlighted:YES];
		[leftArrow setAdjustsImageWhenDisabled:YES];
		[leftArrow setAdjustsImageWhenHighlighted:YES];
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    [leftArrow setImage:[UIImage imageNamed:@"left-arrow_Blue.png"] forState:0];
                }
                else{ //All Others white
                    
                    [leftArrow setImage:[UIImage imageNamed:@"left-arrow.png"] forState:0]; //White Arrow
                }
                
            }
            else{ //iOS 6
                
                [leftArrow setImage:[UIImage imageNamed:@"left-arrow.png"] forState:0]; //White Arrow
            }
            
        }
        else{ //Photo Calendar
            
            [leftArrow setImage:[UIImage imageNamed:@"left-arrow.png"] forState:0];//White Arrow
        }
        
        
        
        //Steve iPad
        if (IS_IPAD) {
            
            leftArrow.frame = CGRectMake(24, 15, 48, 38);
        }

        

	}
	return leftArrow;
}

- (UIButton *) rightArrow {
	if(rightArrow==nil){
		rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		rightArrow.tag = 1;
		[rightArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
		rightArrow.frame = CGRectMake(320-45, 0, 48, 38);
		[rightArrow setShowsTouchWhenHighlighted:YES];
		[rightArrow setReversesTitleShadowWhenHighlighted:YES];
		[rightArrow setAdjustsImageWhenDisabled:YES];
		[rightArrow setAdjustsImageWhenHighlighted:YES];
        
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            if (IS_IOS_7) {
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    [rightArrow setImage:[UIImage imageNamed:@"right-arrow_Blue2.png"] forState:0];
                }
                else{ //All Others white
                    
                    [rightArrow setImage:[UIImage imageNamed:@"right-arrow.png"] forState:0];//White Arrow
                }
                
            }
            else{ //iOS 6
                
                [rightArrow setImage:[UIImage imageNamed:@"right-arrow.png"] forState:0];//White Arrow
            }
            
            
        }
        else{ //Photo Calendar
            
            [rightArrow setImage:[UIImage imageNamed:@"right-arrow.png"] forState:0];//White Arrow
        }
        
        
        //Steve iPad
        if (IS_IPAD) {
            rightArrow.frame = CGRectMake(self.tileBox.frame.size.width-74, 15, 48, 38);
        }

    
		
	}
	return rightArrow;
}


//Box which contains all the tiles
- (UIScrollView *) tileBox {
    
	if(tileBox==nil){
		
        //Steve iPad
        if (IS_IPAD) {
            
            tileBox = [[UIScrollView alloc] initWithFrame:CGRectMake(1, CalendarTopBarHeight, currentTile.frame.size.width*7, currentTile.frame.size.height)];
            //NSLog(@"tilebox method tileBox.frame = %@", NSStringFromCGRect(tileBox.frame));
        }
        else //iPhone
        {
            tileBox = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, 320, currentTile.frame.size.height)];
        }
        tileBox.clipsToBounds = YES;
	}
    
    
	return tileBox;
}

- (UIImageView *) shadow {

    if(shadow==nil){
        shadow = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Shadow.png")]];
    }
	return shadow;
}

@end

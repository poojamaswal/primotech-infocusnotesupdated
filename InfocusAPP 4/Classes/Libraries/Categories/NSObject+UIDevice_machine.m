//
//  NSObject+UIDevice_machine.m
//  Organizer
//
//  Created by alok gupta on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSObject+UIDevice_machine.h"

#include <sys/types.h>
#include <sys/sysctl.h>

@implementation UIDevice (UIDevice_machine)
- (NSString *)machine
{
    size_t size;
    
    // Set 'oldp' parameter to NULL to get the size of the data
    // returned so we can allocate appropriate amount of space
    sysctlbyname("hw.machine", NULL, &size, NULL, 0); 
    
    // Allocate the space to store name
    char *name = malloc(size);
    
    // Get the platform name
    sysctlbyname("hw.machine", name, &size, NULL, 0);
    
    // Place name into a string
    NSString *machine = [NSString stringWithUTF8String:name];
    // Done with this
    free(name);
    
    return machine;
}
@end

//
//  TKCalendarMonthView.h
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import <UIKit/UIKit.h>


/***** Macros for ipad calendar rendering *********/
#define tileWidth 109.42
#define tileHeightAdjustment 28
#define CalendarViewHeight 653
#define CalendarTopBarHeight  90
#define EnableLargeCalendarForIpad YES



@class CalendarMonthViewController;

@class TKCalendarMonthTiles;

@protocol TKCalendarMonthViewDelegate, TKCalendarMonthViewDataSource;

@interface TKCalendarMonthView : UIView <UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>{
	TKCalendarMonthTiles *currentTile, *oldTile;
	UIButton *leftArrow, *rightArrow, *monthButton, *yearButton;
	UIImageView *topBackground, *shadow;
	UILabel *monthYear;
	UIScrollView *tileBox;
	UITableView *scrollOptTblView;
	BOOL sunday;
	NSMutableArray *monthaArray, *yearsArray;
	BOOL isMonthSelected; 
	
	id <TKCalendarMonthViewDelegate> __unsafe_unretained delegate;
	id <TKCalendarMonthViewDataSource> __unsafe_unretained dataSource;
	
	CalendarMonthViewController *parentVC;
    
    float tileHeight; //Steve iPad
    
}

@property (nonatomic,strong) CalendarMonthViewController * parentVC;
@property (nonatomic,strong) UIButton *monthButton;
@property (nonatomic,strong) UIButton *yearButton;
@property (nonatomic,unsafe_unretained) id <TKCalendarMonthViewDelegate> delegate;
@property (nonatomic,unsafe_unretained) id <TKCalendarMonthViewDataSource> dataSource;

- (id) initWithCallingParent:(id) callingParent;
- (id) initWithSundayAsFirst:(BOOL)sunday; // or Monday

- (NSDate*) dateSelected;
- (NSDate*) monthDate;
- (void) selectDate:(NSDate*)date;
- (void) reload;
- (void) animateMonth;
- (void) changeMonthAnimation:(UIView * ) sender;
@end


@protocol TKCalendarMonthViewDelegate <NSObject>

@optional
- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)d;
- (void) calendarMonthView:(TKCalendarMonthView*)monthView monthDidChange:(NSDate*)d;
- (int) weekDayForDate:(NSString *) date;
@end

@protocol TKCalendarMonthViewDataSource <NSObject>
-(NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate;
@end
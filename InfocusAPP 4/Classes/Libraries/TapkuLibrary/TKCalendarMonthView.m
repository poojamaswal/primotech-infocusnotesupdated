//
//  TKCalendarMonthView.m
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 */

#import "TKCalendarMonthView.h"
#import "NSDate+TKCategory.h"
#import "TKGlobal.h"
#import "UIImage+TKCategory.h"
#import "NSDate-Utilities.h"
#import "OrganizerAppDelegate.h"
#import "CalendarMonthViewController.h"

//#define kCalendImagesPath @"TapkuLibrary.bundle/Images/calendar/"

@interface NSDate (calendarcategory)
- (NSDate*) firstOfMonth;
- (NSDate*) nextMonth;
- (NSDate*) previousMonth;
- (NSDate*) monthWithDate:(NSDate*) date; 
-(NSDate*) afterMonth:(NSInteger) months;
@end

@implementation NSDate (calendarcategory)

- (NSDate*) firstOfMonth{
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day = 1;
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

-(NSDate*) afterMonth:(NSInteger) months {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	if (months > 0) {
	}else {
	}

	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

- (NSDate*) nextMonth {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

- (NSDate*) monthWithDate:(NSDate*) date {
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) fromDate:date];
	info.month = [comp month];
	info.year = [comp year];
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
}

- (NSDate*) previousMonth{
	TKDateInformation info = [self dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.month--;
	if(info.month<1){
		info.month = 12;
		info.year--;
	}
	
	info.minute = 0;
	info.second = 0;
	info.hour = 0;
	return [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
}

@end

@interface TKCalendarMonthTiles : UIView {
	UILabel		*dot;
	UILabel		*currentDay;
	UIImageView *selectedImageView;
	
	id		target;
	SEL		action;
	
	int		selectedDay, selectedPortion;
	int		firstWeekday, daysInMonth;
	int		firstOfPrev, lastOfPrev;
	int		today;
	
	BOOL	startOnSunday;
	BOOL	markWasOnToday;
	
	NSDate	*monthDate;
	NSArray *marks;
}

@property (readonly) NSDate *monthDate;

- (id) initWithMonth:(NSDate*)date marks:(NSArray*)marks startDayOnSunday:(BOOL)sunday;
- (void) setTarget:(id)target action:(SEL)action;

- (void) selectDay:(int) day;
- (NSDate *) dateSelected;
+ (NSArray *) rangeOfDatesInMonthGrid:(NSDate *)date startOnSunday:(BOOL)sunday;

@end

#define dotFontSize 18.0
#define dateFontSize 18.0

@interface TKCalendarMonthTiles (private)

@property (readonly) UIImageView *selectedImageView;
@property (readonly) UILabel *currentDay;
@property (readonly) UILabel *dot;
@end

@implementation TKCalendarMonthTiles
@synthesize monthDate;

+ (NSArray*) rangeOfDatesInMonthGrid:(NSDate*)date startOnSunday:(BOOL)sunday{
	
	NSDate *firstDate, *lastDate;
	
	TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day	= 1;
	info.hour	= 0;
	info.minute = 0;
	info.second = 0;
	
	NSDate *currentMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info = [currentMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	NSDate *previousMonth = [currentMonth previousMonth];
	NSDate *nextMonth = [currentMonth nextMonth];
	
	if(info.weekday > 1 && sunday) {
		TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		int preDayCnt = [previousMonth daysBetweenDate:currentMonth];		
		info2.day = preDayCnt - info.weekday + 2;
		firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	}else if(!sunday && info.weekday != 2) {
		TKDateInformation info2 = [previousMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		int preDayCnt = [previousMonth daysBetweenDate:currentMonth];
		
		if(info.weekday == 1){
			info2.day = preDayCnt - 5;
		}else{
			info2.day = preDayCnt - info.weekday + 3;
		}
		firstDate = [NSDate dateFromDateInformation:info2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	}else{
		firstDate = currentMonth;
	}
	
	int daysInMonth = [currentMonth daysBetweenDate:nextMonth];		
	info.day = daysInMonth;
	NSDate *lastInMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	TKDateInformation lastDateInfo = [lastInMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];	
	
	if(lastDateInfo.weekday < 7 && sunday){
		
		lastDateInfo.day = 7 - lastDateInfo.weekday;
		lastDateInfo.month++;
		lastDateInfo.weekday = 0;
		if(lastDateInfo.month>12){
			lastDateInfo.month = 1;
			lastDateInfo.year++;
		}
		lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		
	}else if(!sunday && lastDateInfo.weekday != 1){
		
		
		lastDateInfo.day = 8 - lastDateInfo.weekday;
		lastDateInfo.month++;
		if(lastDateInfo.month>12){ lastDateInfo.month = 1; lastDateInfo.year++; }
		
		
		lastDate = [NSDate dateFromDateInformation:lastDateInfo timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		
	}else{
		lastDate = lastInMonth;
	}
	return [NSArray arrayWithObjects:firstDate, lastDate, nil];
}

- (id) initWithMonth:(NSDate *) date marks:(NSArray *) markArray startDayOnSunday:(BOOL)sunday {
	
    
    
    if(![super initWithFrame:CGRectZero]) return nil;
	
	firstOfPrev = -1;
	marks = markArray;
	monthDate = date;
	startOnSunday = sunday;
	
	TKDateInformation dateInfo = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	firstWeekday = dateInfo.weekday;
	
	NSDate *prev = [monthDate previousMonth];
	//NSDate *next = [monthDate nextMonth];
	
	daysInMonth = [[monthDate nextMonth] daysBetweenDate:monthDate];
	
	int row = (daysInMonth + dateInfo.weekday - 1);
	if(dateInfo.weekday==1&&!sunday) row = daysInMonth + 6;
	if(!sunday) row--;
	
	row = (row / 7) + ((row % 7 == 0) ? 0:1);
	float h = 38 * row;
	
	//added
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	//finish
	
	TKDateInformation todayInfo = [/*[NSDate date]*/destinationDate dateInformation];
	//
	//NSLog(@"*****todayInfo = %d", todayInfo);
	
	today = dateInfo.month == todayInfo.month && dateInfo.year == todayInfo.year ? todayInfo.day : 0;
	
	int preDayCnt = [prev daysBetweenDate:monthDate];
	
	if(firstWeekday > 1 && sunday) {
		firstOfPrev = preDayCnt - firstWeekday + 2;
		lastOfPrev = preDayCnt;
	}else if(!sunday && firstWeekday != 2){
		
		if(firstWeekday == 1) {
			firstOfPrev = preDayCnt - 5;
		}else{
			firstOfPrev = preDayCnt - firstWeekday+3;
		}
		lastOfPrev = preDayCnt;
	}
	
	self.frame = CGRectMake(0, 1, 320, h+1);
	
	[self.selectedImageView addSubview:self.currentDay];
	[self.selectedImageView addSubview:self.dot];
	self.multipleTouchEnabled = NO;
	
	return self;
}

- (void) setTarget:(id)t action:(SEL)a{
	target = t;
	action = a;
}

- (CGRect) rectForCellAtIndex:(int)index{
	int row = index / 7;
	int col = index % 7;
	return CGRectMake(col*46, row*38+6, 47, 38);
}

- (void) drawTileInRect:(CGRect) r day:(int) day mark:(BOOL) mark font:(UIFont *) f1 font2:(UIFont *) f2 {
	
	NSString *str = [NSString stringWithFormat:@"%d", day];
	
	r.size.height -= 2;
	[str drawInRect: r
		   withFont: f1
	  lineBreakMode: UILineBreakModeWordWrap 
		  alignment: UITextAlignmentCenter];
	
	if(mark){
		r.size.height = 10;
		r.origin.y += 18;
		
		[@"•" drawInRect: r
				withFont: f2
		   lineBreakMode: UILineBreakModeWordWrap 
			   alignment: UITextAlignmentCenter];
	}
}

- (void) drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	UIImage *tile = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile.png")];
	CGRect r = CGRectMake(0, 0, 46, 38);
	CGContextDrawTiledImage(context, r, tile.CGImage);
	
	if(today > 0){
		int pre = firstOfPrev > 0 ? lastOfPrev - firstOfPrev + 1 : 0;
		int index = today +  pre-1;
		CGRect r =[self rectForCellAtIndex:index];
		r.origin.y -= 7;
		[[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Today Tile.png")] drawInRect:r];
	}
	
	int index = 0;
	
	UIFont *font = [UIFont boldSystemFontOfSize:dateFontSize];
	UIFont *font2 =[UIFont boldSystemFontOfSize:dotFontSize];
	UIColor *color = [UIColor grayColor];
	
	if(firstOfPrev>0){
		[color set];
		for(int i = firstOfPrev;i<= lastOfPrev;i++){
			r = [self rectForCellAtIndex:index];
			if ([marks count] > 0)
				if ([marks count] > index) {
					[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
				}else {
					[self drawTileInRect:r day:i mark:NO font:font font2:font2];
				}
				else
					[self drawTileInRect:r day:i mark:NO font:font font2:font2];
			index++;
		}
	}
	
	color = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
	[color set];
	for(int i=1; i <= daysInMonth; i++){
		
		r = [self rectForCellAtIndex:index];
		if(today == i) [[UIColor whiteColor] set];
		
		if ([marks count] > 0) 
			if ([marks count] > index) {
				[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
			}else {
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
			}
			else
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
		if(today == i) [color set];
		index++;
	}
	
	[[UIColor grayColor] set];
	int i = 1;
	while(index % 7 != 0){
		r = [self rectForCellAtIndex:index] ;
		if ([marks count] > 0) 
			if ([marks count] > index) {
				[self drawTileInRect:r day:i mark:[[marks objectAtIndex:index] boolValue] font:font font2:font2];
			}else {
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
			}
			else
				[self drawTileInRect:r day:i mark:NO font:font font2:font2];
		i++;
		index++;
	}
}

- (void) selectDay:(int)day {
	
	int pre = firstOfPrev < 0 ?  0 : lastOfPrev - firstOfPrev + 1;
	
	int tot = day + pre;
	int row = tot / 7;
	int column = (tot % 7)-1;
	
	selectedDay = day;
	selectedPortion = 1;
	
	
	if(day == today){
		self.currentDay.shadowOffset = CGSizeMake(0, 1);
		self.dot.shadowOffset = CGSizeMake(0, 1);
		self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Today Selected Tile.png")];
		markWasOnToday = YES;
	}else if(markWasOnToday){
		self.dot.shadowOffset = CGSizeMake(0, -1);
		self.currentDay.shadowOffset = CGSizeMake(0, -1);
		
		self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected.png")];
		markWasOnToday = NO;
	}
	
	[self addSubview:self.selectedImageView];
	self.currentDay.text = [NSString stringWithFormat:@"%d",day];
	
	if ([marks count] > 0) {
		if([[marks objectAtIndex: row * 7 + column] boolValue]){
			[self.selectedImageView addSubview:self.dot];
		}else{
			[self.dot removeFromSuperview];
		}
	}else{
		[self.dot removeFromSuperview];
	}
	
	if(column < 0){
		column = 6;
		row--;
	}
	
	CGRect r = self.selectedImageView.frame;
	r.origin.x = (column*46);
	r.origin.y = (row*38)-1;
	self.selectedImageView.frame = r;
}

- (NSDate*) dateSelected 
{
	if(selectedDay < 1 || selectedPortion != 1) return nil;
	
	TKDateInformation info = [monthDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	info.hour = 0;
	info.minute = 0;
	info.second = 0;
	info.day = selectedDay;
	
	
	NSDate *d = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	//NSLog(@"%@",d);
	return d;
}

- (void) reactToTouch:(UITouch*)touch down:(BOOL)down {
	CGPoint p = [touch locationInView:self];
	if(p.y > self.bounds.size.height || p.y < 0) return;
	
	int column = p.x / 46, row = p.y / 38;
	int day = 1, portion = 0;
	
	if(row == (int) (self.bounds.size.height / 38)) row --;
	
	int fir = firstWeekday - 1;
	if(!startOnSunday && fir == 0) fir = 7;
	if(!startOnSunday) fir--;
	
	
	if(row==0 && column < fir){
		day = firstOfPrev + column;
	}else{
		portion = 1;
		day = row * 7 + column  - firstWeekday+2;
		if(!startOnSunday) day++;
		if(!startOnSunday && fir==6) day -= 7;
		
	}
	if(portion > 0 && day > daysInMonth){
		portion = 2;
		day = day - daysInMonth;
	}
	
	
	if(portion != 1){
		self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Gray.png")];
		markWasOnToday = YES;
	}else if(portion==1 && day == today){
		self.currentDay.shadowOffset = CGSizeMake(0, 1);
		self.dot.shadowOffset = CGSizeMake(0, 1);
		self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Today Selected Tile.png")];
		markWasOnToday = YES;
	}else if(markWasOnToday){
		self.dot.shadowOffset = CGSizeMake(0, -1);
		self.currentDay.shadowOffset = CGSizeMake(0, -1);
		self.selectedImageView.image = [UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected.png")];
		markWasOnToday = NO;
	}
	
	[self addSubview:self.selectedImageView];
	self.currentDay.text = [NSString stringWithFormat:@"%d",day];
	
	if ([marks count] > 0) {
		if([[marks objectAtIndex: row * 7 + column] boolValue])
			[self.selectedImageView addSubview:self.dot];
		else
			[self.dot removeFromSuperview];
	}else{
		[self.dot removeFromSuperview];
	}
	CGRect r = self.selectedImageView.frame;
	r.origin.x = (column*46);
	r.origin.y = (row*38)-1;
	self.selectedImageView.frame = r;
	
	if(day == selectedDay && selectedPortion == portion) return;
	
	if(portion == 1){
		selectedDay = day;
		selectedPortion = portion;
		[target performSelector:action withObject:[NSArray arrayWithObject:[NSNumber numberWithInt:day]]];
		
	}
	else if(down){
		[target performSelector:action withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:day],[NSNumber numberWithInt:portion],nil]];
		selectedDay = day;
		selectedPortion = portion;
	}
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	//[super touchesBegan:touches withEvent:event];
	[self reactToTouch:[touches anyObject] down:NO];
} 

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	[self reactToTouch:[touches anyObject] down:NO];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
	[self reactToTouch:[touches anyObject] down:YES];
}

- (UILabel *) currentDay {
	if(currentDay==nil){
		CGRect r = self.selectedImageView.bounds;
		r.origin.y -= 2;
		currentDay = [[UILabel alloc] initWithFrame:r];
		currentDay.text = @"1";
		currentDay.textColor = [UIColor whiteColor];
		currentDay.backgroundColor = [UIColor clearColor];
		currentDay.font = [UIFont boldSystemFontOfSize:dateFontSize];
		currentDay.textAlignment = UITextAlignmentCenter;
		currentDay.shadowColor = [UIColor darkGrayColor];
		currentDay.shadowOffset = CGSizeMake(0, -1);
	}
	return currentDay;
}

- (UILabel *) dot{
	if(dot==nil){
		CGRect r = self.selectedImageView.bounds;
		r.origin.y += 29;
		r.size.height -= 31;
		dot = [[UILabel alloc] initWithFrame:r];
		
		dot.text = @"•";
		dot.textColor = [UIColor whiteColor];
		dot.backgroundColor = [UIColor clearColor];
		dot.font = [UIFont boldSystemFontOfSize:dotFontSize];
		dot.textAlignment = UITextAlignmentCenter;
		dot.shadowColor = [UIColor darkGrayColor];
		dot.shadowOffset = CGSizeMake(0, -1);
	}
	return dot;
}

- (UIImageView *) selectedImageView {
	if(selectedImageView==nil){
		selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Date Tile Selected"]];
	}
	
	CGRect rect = CGRectZero;
	rect = [selectedImageView frame];
	rect.size.width = 46;
	rect.size.height = 38;
	[selectedImageView setFrame:rect];
	return selectedImageView;
}


@end

@interface TKCalendarMonthView (private)

@property (readonly) UIScrollView *tileBox;
@property (readonly) UIImageView *topBackground;
@property (readonly) UILabel *monthYear;
@property (readonly) UIButton *leftArrow;
@property (readonly) UIButton *rightArrow;
@property (readonly) UIImageView *shadow;

@end

@implementation TKCalendarMonthView
@synthesize delegate,dataSource;
@synthesize monthButton, yearButton;
@synthesize parentVC;

- (id) initWithCallingParent:(id) callingParent {
	self.parentVC = callingParent;
	return [self initWithSundayAsFirst:YES];
}

- (id) initWithSundayAsFirst:(BOOL)s {
	
	if (!(self = [super initWithFrame:CGRectZero])) return nil;
	sunday = s;
	
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	if ([[appDelegate currentVisibleDate] isEqualToString:@""] || [appDelegate currentVisibleDate] == nil) {
		[appDelegate setCurrentVisibleDate:[[NSDate date] description]];
	}
	NSDate *currVisibleDate = [formater	dateFromString:[appDelegate currentVisibleDate]]; 
	
	currentTile = [[TKCalendarMonthTiles alloc] initWithMonth:[currVisibleDate firstOfMonth] marks:nil startDayOnSunday:sunday];
	[currentTile setTarget:self action:@selector(tile:)];
	
	[self performSelector:@selector(initialySelectDate) withObject:nil afterDelay:0.1];
	
	CGRect rect = CGRectZero;
	
	rect = [currentTile frame];
	rect.size.width = 46;
	[currentTile setFrame:rect];
	
	CGRect r = CGRectMake(0, 0, self.tileBox.bounds.size.width , self.tileBox.bounds.size.height + self.tileBox.frame.origin.y);
	
	self.frame = r;
	
	
	[self addSubview:self.topBackground];
	[self.tileBox addSubview:currentTile];
	[self addSubview:self.tileBox];
	
	NSDate *date = [NSDate date];
	self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[date month],[date year]];
	[self addSubview:self.monthYear];
	[self addSubview:self.leftArrow];
	[self addSubview:self.rightArrow];
	
	monthaArray = [[NSMutableArray alloc] initWithObjects:@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December", nil];
	yearsArray  = [[NSMutableArray alloc] init];
	
	for (int i = 1900; i < 2100; i++) {
		[yearsArray  addObject:[NSString stringWithFormat:@"%d", i]];
	}
	
	UIImage *monthButtonImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MonthBox" ofType:@"png"]];
	
	monthButton = [[UIButton alloc] initWithFrame:CGRectMake(60, 5, monthButtonImage.size.width, monthButtonImage.size.height)];
	[monthButton setTitle:@"Month" forState:UIControlStateNormal];
	[monthButton setImage:monthButtonImage forState:UIControlStateNormal];
	[monthButton addTarget:self action:@selector(monthButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[monthButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	[monthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self addSubview:monthButton];
	
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	if ([appDelegate.currentVisibleDate isEqualToString:@""]) {
		appDelegate.currentVisibleDate = [formater stringFromDate:[NSDate date]];
	}
	NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
	[formater setDateFormat:@"MMMM"];
	
	UILabel *monthTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, monthButtonImage.size.width - 20, monthButtonImage.size.height - 4)];
	[monthTitleLabel setFont:[UIFont boldSystemFontOfSize:12]];
	[monthTitleLabel setTag:311];
	[monthTitleLabel setText:[formater stringFromDate:tempDate]];
	[monthTitleLabel setTextColor:[UIColor whiteColor]];
	[monthTitleLabel setBackgroundColor:[UIColor clearColor]];
	[monthButton addSubview:monthTitleLabel];
	
	UIImage *yearButtonImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"YearBox" ofType:@"png"]];
	yearButton = [[UIButton alloc] initWithFrame:CGRectMake(320 - 130, 5, yearButtonImage.size.width, yearButtonImage.size.height)];
	[yearButton setTitle:@"Year" forState:UIControlStateNormal];
	[yearButton setImage:yearButtonImage forState:UIControlStateNormal];
	[yearButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	[yearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[yearButton addTarget:self action:@selector(yearButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:yearButton];
	
	UILabel *yearTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, yearButtonImage.size.width - 20, yearButtonImage.size.height - 4)];
	[yearTitleLabel setTag:312];
	[yearTitleLabel setFont:[UIFont boldSystemFontOfSize:12]];
	[yearTitleLabel setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]];
	[yearTitleLabel setTextColor:[UIColor whiteColor]];
	[yearTitleLabel setBackgroundColor:[UIColor clearColor]];
	[yearButton addSubview:yearTitleLabel];
	
	UIView *scrollOptBGView = [[ UIView alloc ] initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height)];
	[scrollOptBGView setBackgroundColor:[UIColor clearColor]];
	[scrollOptBGView setTag:411];
	[scrollOptBGView setAlpha:0];
	
	scrollOptTblView = [[UITableView alloc] initWithFrame:CGRectMake(monthButton.frame.origin.x, monthButton.frame.origin.y + monthButton.frame.size.height, monthButton.frame.size.width, 200) style:UITableViewStylePlain];
	[scrollOptTblView setDataSource:self];
	[scrollOptTblView setDelegate:self];
	[scrollOptTblView setRowHeight:25];
	[scrollOptTblView.layer setCornerRadius:5.0];
	scrollOptTblView.layer.masksToBounds = YES;
	scrollOptTblView.layer.borderColor = [UIColor grayColor].CGColor;  //Steve  [UIColor purpleColor].CGColor;
	scrollOptTblView.layer.borderWidth = 1.5;	
	[scrollOptBGView addSubview:scrollOptTblView];
	
	[self addSubview:self.shadow];
	self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
	self.backgroundColor = [UIColor grayColor];
	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"eee"];
	[dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	TKDateInformation sund;
	sund.day = 5;
	sund.month = 12;
	sund.year = 2010;
	sund.hour = 0;
	sund.minute = 0;
	sund.second = 0;
	
	NSTimeZone *tz = [NSTimeZone timeZoneForSecondsFromGMT:0];
	NSString * sun = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 6;
	NSString *mon = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 7;
	NSString *tue = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 8;
	NSString *wed = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 9;
	NSString *thu = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 10;
	NSString *fri = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	sund.day = 11;
	NSString *sat = [dateFormat stringFromDate:[NSDate dateFromDateInformation:sund timeZone:tz]];
	
	
	NSArray *ar;
	if(sunday) ar = [NSArray arrayWithObjects:sun,mon,tue,wed,thu,fri,sat,nil];
	else ar = [NSArray arrayWithObjects:mon,tue,wed,thu,fri,sat,sun,nil];
	
	int i = 0;
	for(NSString *s in ar){
		
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(46 * i, 29, 46, 15)];
		[self addSubview:label];
		label.text = s;
		label.textAlignment = UITextAlignmentCenter;
		label.shadowColor = [UIColor whiteColor];
		label.shadowOffset = CGSizeMake(0, 0);
		label.font = [UIFont systemFontOfSize:11];
		label.backgroundColor = [UIColor clearColor];
		//		label.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
		label.textColor = [UIColor whiteColor];
		i++;
	}
	[self addSubview:scrollOptBGView];
    
    
    ////////////////////  Added swipeRight and Swipe Left gestures to change month //////////
    
    UISwipeGestureRecognizer *SwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightGestureRecognizer:)];
    SwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    SwipeRight.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:SwipeRight];
    SwipeRight.delegate = self;
    
    UISwipeGestureRecognizer *SwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLefttGestureRecognizer:)];
    SwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    SwipeLeft.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:SwipeLeft];
    SwipeLeft.delegate = self;
    
////////////////////////////////////////////////////////////////////////////////////
    
    
    
	return self;
}

- (void)swipeRightGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer 
{
    NSLog(@"%s",__FUNCTION__);
    
    [self.leftArrow sendActionsForControlEvents:UIControlEventTouchUpInside];    
}
- (void)swipeLefttGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer 
{
    NSLog(@"%s",__FUNCTION__);
    [self.rightArrow sendActionsForControlEvents:UIControlEventTouchUpInside];    

    
}



- (void) initialySelectDate {
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	NSDate *date = [formater dateFromString:appDelegate.currentVisibleDate]; 
	
	[currentTile selectDay:[date day]];
	[parentVC reloadTableView];
}


- (void) monthButton_Clicked:(UIButton *) sender {
	sender.tag = 211;
	isMonthSelected = YES;
	[scrollOptTblView setFrame:CGRectMake(monthButton.frame.origin.x, monthButton.frame.origin.y + monthButton.frame.size.height, monthButton.frame.size.width, 150)];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[[self viewWithTag:411] setAlpha:1];
	[UIView commitAnimations];
	[scrollOptTblView reloadData];
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] inSection:0];
	[scrollOptTblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	[self performSelector:@selector(selectedRow) withObject:nil afterDelay:0.25];
}

- (void) yearButton_Clicked:(UIButton *) sender {
	isMonthSelected = NO;
	[scrollOptTblView setFrame:CGRectMake(yearButton.frame.origin.x, yearButton.frame.origin.y + yearButton.frame.size.height, yearButton.frame.size.width, 150)];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[[self viewWithTag:411] setAlpha:1];
	[UIView commitAnimations];
	
	sender.tag = 212;
	[scrollOptTblView reloadData];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[yearsArray indexOfObject:[(UILabel*)[yearButton viewWithTag:312] text]] inSection:0];
	[scrollOptTblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	[self performSelector:@selector(selectedRow) withObject:nil afterDelay:0.5];
}

- (void) selectedRow {
	NSIndexPath *indexPath;
	
	if (isMonthSelected) {
		indexPath = [NSIndexPath indexPathForRow:[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] inSection:0];
	}else {
		indexPath = [NSIndexPath indexPathForRow:[yearsArray indexOfObject:[(UILabel*)[yearButton viewWithTag:312] text]] inSection:0];
	}
	
	[[scrollOptTblView cellForRowAtIndexPath:indexPath] setSelected:YES];
}

-(int)compairDateWithoutTimeDate1:(NSDate *) dateOne date2:(NSDate *) dateTwo {
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
	
	NSDateComponents *date1Components = [calendar components:comps 
													fromDate: dateOne];
	NSDateComponents *date2Components = [calendar components:comps 
													fromDate: dateTwo];
	
	dateOne = [calendar dateFromComponents:date1Components];
	dateTwo = [calendar dateFromComponents:date2Components];
	
	NSComparisonResult result = [dateOne compare:dateTwo];
	if (result == NSOrderedAscending) {
		return 1; // Date One is smaller then date two
	} else if (result == NSOrderedDescending) {
		return 3; // Date One is bigger then date two
	}  else {
		return 2; // Date One is equal date two
	}
}


- (void)animateMonth {
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	if ([self compairDateWithoutTimeDate1:[formater dateFromString:[appDelegate currentVisibleDate]] date2:destinationDate] == 2) 
	{
		return ;
	}
	
	
	TKDateInformation info = [[NSDate date] dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	info.day	= 1;
	info.hour	= 0;
	info.minute = 0;
	info.second = 0;
	
	NSDateFormatter *abc = [[NSDateFormatter alloc]init];
	[abc setDateFormat:@"yyyy-MM"];
	NSDate *currentMonth = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSString *curMon = [abc stringFromDate:currentMonth];
	
	NSDateFormatter *xyz = [[NSDateFormatter alloc]init];
	[xyz setDateFormat:@"yyyy-MM"];
	appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	NSDate *date = [formater dateFromString:appDelegate.currentVisibleDate];
	NSString *local = [xyz stringFromDate:date];
	
	
	if ([curMon isEqualToString:local])
	{
		appDelegate.currentVisibleDate = [formater stringFromDate:destinationDate];
		[currentTile selectDay:[[[[appDelegate currentVisibleDate] substringFromIndex:8] substringToIndex:2] intValue]];
		NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
		[formater setDateFormat:@"MMMM"];
		[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
		[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
		return;
	}
	
	appDelegate.currentVisibleDate = [formater stringFromDate:destinationDate];
	[self changeMonthAnimation:nil];
	[currentTile selectDay:[[[[appDelegate currentVisibleDate] substringFromIndex:8] substringToIndex:2] intValue]];

	//NSLog(@"********appdelegate.currentVisibleDate = %@",appDelegate.currentVisibleDate);
	
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)]){
		[delegate calendarMonthView:self monthDidChange:[NSDate date]];
	}
	NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
	
	[formater setDateFormat:@"MMMM"];
	[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
	[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
}

- (void) changeMonthAnimation:(UIView *) sender {
	BOOL isNext = (sender.tag == 1);
	NSDate *nextMonth;
	NSDateFormatter *formate = [[NSDateFormatter alloc] init];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	if (!sender) 
	{
		[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSString *dateString =  [[appDelegate.currentVisibleDate substringToIndex:8] stringByAppendingFormat:@"01 18:30:0000 +0000"];
		
		NSDate *date = [formate dateFromString:dateString];
		nextMonth = [date monthWithDate:date];
	}
	else 
	{
		if (sender.tag == 211 || sender.tag == 212)
		{
			[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			NSDate *date = [formate dateFromString:appDelegate.currentVisibleDate];
			nextMonth = [date monthWithDate:date];
		}
		else 
		{
			nextMonth = isNext ? [currentTile.monthDate nextMonth] : [currentTile.monthDate previousMonth];
		}
	}
	TKDateInformation nextInfo = [nextMonth dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	//	nextInfo.weekday =  [self weekDayForDate:appDelegate.currentVisibleDate];
	NSDate *localNextMonth = [NSDate dateFromDateInformation:nextInfo];
	
	NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:nextMonth startOnSunday:sunday];
	NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
	TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:nextMonth marks:ar startDayOnSunday:sunday];
	[newTile setTarget:self action:@selector(tile:)];
	
	int overlap =  0;
	
	if(isNext){
		overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : 38;
	}else{
		overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? 38 : 0;
	}
	
	float y = isNext ? currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap;
	
	newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);
	[self.tileBox addSubview:newTile];
	[self.tileBox bringSubviewToFront:currentTile];
	
	self.userInteractionEnabled = NO;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDidStopSelector:@selector(animationEnded)];
	[UIView setAnimationDuration:0.4];
	
	currentTile.alpha = 0.0;
	
	if(isNext) {
		currentTile.frame = CGRectMake(0, -1 * currentTile.bounds.size.height + overlap, currentTile.frame.size.width, currentTile.frame.size.height);
		newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
		
	}else {
		
		newTile.frame = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		currentTile.frame = CGRectMake(0,  newTile.frame.size.height - overlap, currentTile.frame.size.width, currentTile.frame.size.height);
		
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
	}
	
	[UIView commitAnimations];
	oldTile = currentTile;
	currentTile = newTile;
	
	monthYear.text = [NSString stringWithFormat:@"%@ %@",[localNextMonth month],[localNextMonth year]];
	
	[currentTile selectDay:[localNextMonth day]];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.2];
	[[parentVC dayDetailTableView] setFrame:CGRectMake(0, self.frame.size.height + 2, 320, [parentVC view].frame.size.height - self.frame.size.height)];
	[UIView commitAnimations];
}

- (int) weekDayForDate:(NSString *) date {
	NSDateFormatter *formater = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDateComponents *comps = [[NSCalendar currentCalendar] components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:[formater dateFromString:date]];
	int weekday = [comps weekday];
	return weekday;
}

#pragma mark -
#pragma mark Table view data source
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if (isMonthSelected) {
		return [monthaArray count];
	}else {
		return [yearsArray count];
	}
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	[cell.textLabel setTextAlignment:UITextAlignmentCenter];
	[cell.textLabel setFont:[UIFont boldSystemFontOfSize:15]];
	
	if (isMonthSelected) {
		[cell.textLabel setText:[monthaArray objectAtIndex:[indexPath row]]];
		if ([[monthaArray objectAtIndex:[indexPath row]] isEqualToString:[(UILabel*)[monthButton viewWithTag:311] text]]) {
			//			[cell setSelected:YES];
		}
	}else {
		if ([[yearsArray objectAtIndex:[indexPath row]] isEqualToString:[(UILabel*)[monthButton viewWithTag:312] text]]) {
			//			[cell setSelected:YES];
		}
		[cell.textLabel setText:[yearsArray objectAtIndex:[indexPath row]]];
	}
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
#pragma mark -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NSString *dateStr;
	//	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	if (isMonthSelected) 
	{
		[(UILabel*)[monthButton viewWithTag:311] setText:[monthaArray objectAtIndex:[indexPath row]]];
		if ([indexPath row] + 1 < 10) {
			dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 18:30:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
		}else {
			dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 18:30:00 +0000", [(UILabel*)[yearButton viewWithTag:312] text], [indexPath row] + 1];
		}
		
	} 
	else {
		[(UILabel*)[yearButton viewWithTag:312] setText:[yearsArray objectAtIndex:[indexPath row]]];
		
		if ( 1+[monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]] < 10) {
			dateStr =  [@"" stringByAppendingFormat:@"%@-0%d-01 18:30:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]];
		}else {
			dateStr =  [@"" stringByAppendingFormat:@"%@-%d-01 18:30:00 +0000", [yearsArray objectAtIndex:[indexPath row]], 1 + [monthaArray indexOfObject:[(UILabel*)[monthButton viewWithTag:311] text]]];
		}
	}
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.currentVisibleDate  = dateStr;
	NSDateFormatter *formate = [[NSDateFormatter alloc] init];
	[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDate *date = [formate dateFromString:appDelegate.currentVisibleDate];
	
	if (isMonthSelected) {
		[self changeMonthAnimation:monthButton];
	}else {
		[self changeMonthAnimation:yearButton];
	}
	
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)]){
		[delegate calendarMonthView:self monthDidChange:date];
	}
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.7];
	[[self viewWithTag:411] setAlpha:0];
	[UIView commitAnimations];
}

#pragma mark -
#pragma mark - Touch Methods 
#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	if (![touches containsObject:scrollOptTblView]) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.7];
		[[self viewWithTag:411] setAlpha:0];
		[UIView commitAnimations];
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//	UITouch *touch = [touches anyObject];   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
}

- (void) changeMonth:(UIButton *)sender {
	//NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
//	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss  +0530"];
	
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	
	[self changeMonthAnimation:sender];
	if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
		[delegate calendarMonthView:self monthDidChange:currentTile.monthDate];
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	appDelegate.currentVisibleDate = [dateFormatter stringFromDate:currentTile.monthDate];
	
/*	NSDate *dateString =[dateFormatter dateFromString:appDelegate.currentVisibleDate];
	
    //NSDate *tempDate = [dateFormatter stringFromDate:appDelegate.currentVisibleDate];
	
	
	//NSLog(@"%@",dateString);
	
	
	
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:dateString];
    NSDate *gmtDate = [dateString dateByAddingTimeInterval:-timeZoneOffset]; // NOTE the "-" sign!
    NSString *gmtdateString = [dateFormatter stringFromDate:gmtDate];
	
   //NSLog(@"%@",gmtdateString);
	
	
	
	
	//NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
	[dateFormatter setDateFormat:@"MMMM"];
    //NSLog(@"%@",[dateFormatter stringFromDate:gmtDate]);
	
	
	[(UILabel*)[monthButton viewWithTag:311] setText:[dateFormatter stringFromDate:gmtDate]];
	[(UILabel*)[yearButton viewWithTag:312] setText:[gmtdateString substringWithRange:NSMakeRange(0, 4)]];
	[dateFormatter release];
 */
	
	
	NSDate *tempDate =  [dateFormatter dateFromString:appDelegate.currentVisibleDate];
	
	[dateFormatter setDateFormat:@"MMMM"];
	[(UILabel*)[monthButton viewWithTag:311] setText:[dateFormatter stringFromDate:tempDate]];
	[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]]; 
	
	//NSLog(@"Date: %@", currentTile.monthDate);
	
}

- (void) animationEnded {
	self.userInteractionEnabled = YES;
	oldTile = nil;
	[parentVC reloadTableView];
}

- (NSDate*) dateSelected {
	
	//NSLog(@"%@",currentTile.dateSelected);
	return [currentTile dateSelected];
}

- (NSDate*) monthDate {
	return [currentTile monthDate];
}

- (void) selectDate:(NSDate*)date{
	TKDateInformation info = [date dateInformation];
	
	NSDate *month = [date firstOfMonth];
	
	if([month isEqualToDate:[currentTile monthDate]]){
		[currentTile selectDay:info.day];
		return;
	}else {
		
		//NSDate *month = [self firstOfMonthFromDate:date];
		NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:month startOnSunday:sunday];
		NSArray *data = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
		TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithMonth:month 
																			  marks:data 
																   startDayOnSunday:sunday];
		[newTile setTarget:self action:@selector(tile:)];
		[currentTile removeFromSuperview];
		currentTile = newTile;
		
		[self.tileBox addSubview:currentTile];
		self.tileBox.frame = CGRectMake(0, 38, newTile.frame.size.width, newTile.frame.size.height);
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		
		self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
		self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[month month],[month year]];
		[currentTile selectDay:info.day];
	}
}

- (void) reload {
	NSArray *dates = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:[currentTile monthDate] startOnSunday:sunday];
	NSArray *ar = [dataSource calendarMonthView:self marksFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
	
	TKCalendarMonthTiles *refresh = [[TKCalendarMonthTiles alloc] initWithMonth:[currentTile monthDate] marks:ar startDayOnSunday:sunday];
	[refresh setTarget:self action:@selector(tile:)];
	
	[self.tileBox addSubview:refresh];
	[currentTile removeFromSuperview];
	currentTile = refresh;
}

- (void) tile:(NSArray *) ar {
	if([ar count] < 2) {
		if ([delegate respondsToSelector:@selector(calendarMonthView:didSelectDate:)])
			[delegate calendarMonthView:self didSelectDate:[self dateSelected]];
	} else {
		int direction = [[ar lastObject] intValue];
		UIButton *b = direction > 1 ? self.rightArrow : self.leftArrow;
		[self changeMonthAnimation:b];
		
		int day = [[ar objectAtIndex:0] intValue];
		TKDateInformation info = [[currentTile monthDate] dateInformation];
		info.day = day;
		NSDate *dateForMonth = [NSDate dateFromDateInformation:info]; 
		[currentTile selectDay:day];
		if([delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:)])
			[delegate calendarMonthView:self monthDidChange:dateForMonth];		
		
		OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
		NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		appDelegate.currentVisibleDate = [formater stringFromDate:dateForMonth];
		NSDate *tempDate =  [formater dateFromString:appDelegate.currentVisibleDate];
		[formater setDateFormat:@"MMMM"];
		[(UILabel*)[monthButton viewWithTag:311] setText:[formater stringFromDate:tempDate]];
		[(UILabel*)[yearButton viewWithTag:312] setText:[[appDelegate currentVisibleDate] substringWithRange:NSMakeRange(0, 4)]];
	}
	[[parentVC dayDetailTableView] reloadData];
	
	NSDateFormatter *formater  = [[NSDateFormatter alloc] init];
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	appDelegate.currentVisibleDate = [formater stringFromDate:[currentTile dateSelected]];
	
}

- (UIImageView *) topBackground{
	if(topBackground==nil){
        
        topBackground = [[UIImageView alloc] init];
        
        topBackground.backgroundColor = [UIColor colorWithRed:135/255 green:135/255 blue:135/255 alpha:1.0];
    
        
		//topBackground = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Grid Top Bar.png")]];
	}
	
	CGRect rect = CGRectZero;
	
	rect = [topBackground frame];
	rect.size.width = rect.size.width;
	[topBackground setFrame:rect];
	
	return topBackground;
}

- (UILabel *) monthYear {
	if(monthYear == nil) {
		monthYear = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tileBox.frame.size.width, 38)];
		
		monthYear.textAlignment = UITextAlignmentCenter;
		monthYear.backgroundColor = [UIColor clearColor];
		monthYear.font = [UIFont boldSystemFontOfSize:22];
		monthYear.textColor = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:0];
	}
	return monthYear;
}

- (UIButton *) leftArrow{
	if(leftArrow==nil){
		leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		leftArrow.tag = 0;
		[leftArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
		
	//	[leftArrow setImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Left Arrow"] forState:0];
		[leftArrow setImage:[UIImage imageNamed:@"left-arrow.png"] forState:0];
		
		leftArrow.frame = CGRectMake(0, 0, 48, 38);
		
		[leftArrow setShowsTouchWhenHighlighted:YES];
		[leftArrow setReversesTitleShadowWhenHighlighted:YES];
		[leftArrow setAdjustsImageWhenDisabled:YES];
		[leftArrow setAdjustsImageWhenHighlighted:YES];
	}
	return leftArrow;
}

- (UIButton *) rightArrow {
	if(rightArrow==nil){
		rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		rightArrow.tag = 1;
		[rightArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
		rightArrow.frame = CGRectMake(320-45, 0, 48, 38);
		//[rightArrow setImage:[UIImage imageNamedTK:@"TapkuLibrary.bundle/Images/calendar/Month Calendar Right Arrow"] forState:0];
		[rightArrow setImage:[UIImage imageNamed:@"right-arrow.png"] forState:0];
		[rightArrow setShowsTouchWhenHighlighted:YES];
		[rightArrow setReversesTitleShadowWhenHighlighted:YES];
		[rightArrow setAdjustsImageWhenDisabled:YES];
		[rightArrow setAdjustsImageWhenHighlighted:YES];
		
	}
	return rightArrow;
}

- (UIScrollView *) tileBox {
	if(tileBox==nil){
		tileBox = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, 320, currentTile.frame.size.height)];
	}
	return tileBox;
}

- (UIImageView *) shadow {
	if(shadow==nil){
		shadow = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:TKBUNDLE(@"TapkuLibrary.bundle/Images/calendar/Month Calendar Shadow.png")]];
	}
	return shadow;
}

@end

//
//  NSObject+UIDevice_machine.h
//  Organizer
//
//  Created by alok gupta on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIDevice (UIDevice_machine)
- (NSString *)machine;
@end

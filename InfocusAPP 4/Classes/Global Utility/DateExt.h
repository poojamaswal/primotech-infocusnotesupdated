//
//  DateExt.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 5/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate (DateExt)

+ (NSDate *) dateWithSQLiteRepresentation: (NSString *) myString;
+ (NSDate *) dateWithSQLiteRepresentation: (NSString *) myString timeZone: (NSString *) myTimeZone;
+ (NSDateFormatter *) sqlLiteDateFormatter;
+ (NSDateFormatter *) sqlLiteDateFormatterWithTimezone;
- (NSString *) sqlLiteDateRepresentation;
- (NSTimeInterval) unixTime;
- (NSTimeInterval) julianDay;
+ (NSDate *) dateWithJulianDay: (NSTimeInterval) myTimeInterval;
+ (id)dateWithNaturalLanguageStringForString:(NSString *)string;
@end

//
//  LocalDisctionaryClass.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalDisctionaryClass : NSObject {
	id obj;
	NSString *key;
}

@property(nonatomic, strong) id obj;
@property(nonatomic, strong) NSString *key;

@end

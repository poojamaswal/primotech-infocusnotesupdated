//
//  GlobalUtility.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GlobalUtility.h"
#import "HandWritingViewController.h"
//#import "TODODataObject.h"

@implementation GlobalUtility

+(void)setNavBarTitle:(NSString*)title forViewController:(id) VC{
	UIViewController *viewCntr = VC;
	UINavigationController *navController = [viewCntr navigationController];
	//UINavigationItem *navIem = [viewCntr navigationItem];
	NSString *titleLocal = title;
	[[[navController navigationBar] viewWithTag:1011] removeFromSuperview];
	NSString *string = titleLocal; //26.9
	//CGSize rowSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:20.0] constrainedToSize:CGSizeMake(220,GENERAL_LABEL_HEIGHT) lineBreakMode:UILineBreakModeTailTruncation];
    CGSize rowSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:20.0] constrainedToSize:CGSizeMake(220,GENERAL_LABEL_HEIGHT) lineBreakMode:NSLineBreakByTruncatingTail];
	
	NSInteger xPos = navController.navigationBar.frame.size.width/2 - rowSize.width/2;
	NSInteger yPos = navController.navigationBar.frame.size.height/2 -  GENERAL_LABEL_HEIGHT/2;
	NSInteger width = rowSize.width;
	NSInteger height = rowSize.height;
	
	UILabel *titleLabel = [self getLabelNavTitleWithFrame: CGRectMake(xPos, yPos+3, width, height)];
	
	titleLabel.text = titleLocal;
	[navController.navigationBar addSubview:titleLabel];

//	UIImage *navImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bluestreep" ofType:@"png"]];
//	UIImageView *navBGImageView = [[UIImageView alloc] initWithImage:navImage];
//	[navBGImageView setFrame:navController.navigationBar.frame];
//	[navController.navigationBar insertSubview:navBGImageView atIndex:0];
//	[navImage release];
//	[navBGImageView release];
	
}

+(UILabel*)getLabelNavTitleWithFrame:(CGRect) frame{
	UILabel *label = [[UILabel alloc] initWithFrame:frame];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1]];
	[label setTextAlignment:NSTextAlignmentCenter];
	[label setTag:1011];
	[label setFont:[UIFont fontWithName:@"HelveticaNeue" size:20.0]];
	return label;
}

+(void)createBackNavigationButtonWithImageName:(NSString*) imageNameLocal forViewController:(id)VC { 
	UIViewController *viewCntr = VC;
	NSString *imageName = imageNameLocal;
	
	UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[leftButton setImage:[UIImage imageNamed:imageName]forState:UIControlStateNormal];
	[leftButton addTarget:viewCntr action:@selector(backBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	
	[leftButton setFrame:CGRectMake(5, 7, [[UIImage imageNamed:imageName] size].width-5, [[UIImage imageNamed:imageName] size].height)];
//	UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
//	[viewCntr.navigationItem setLeftBarButtonItem:leftBarButtonItem];
//	[leftBarButtonItem release];
//	leftBarButtonItem=nil;
	[viewCntr.navigationController.navigationBar addSubview:leftButton];
} 

+(void)createRightNavigationButtonWithImageName:(NSString*) imageNameLocal forViewController:(id)VC {
	
	UIViewController *viewCntr = VC;
	NSString *imageName = imageNameLocal;
	
	UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[rightButton setImage:[UIImage imageNamed:imageName]forState:UIControlStateNormal];
	[rightButton addTarget:viewCntr action:@selector(mapBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[rightButton setFrame:CGRectMake(240, 7, [[UIImage imageNamed:imageName] size].width, [[UIImage imageNamed:imageName] size].height)];
	
	UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
	[viewCntr.navigationItem setRightBarButtonItem:rightBarButtonItem];
}

+(NSString *)getDocumentsDirectory{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+(void)getUnderImplementationAlert {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Under Implementation !!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}

//+(NSMutableArray *)sortCommonDisctionary:(NSMutableDictionary *)commonDisc inAscendingOrder: (BOOL) ascendingOrder {
//	NSMutableDictionary *commonDiscLocal = [commonDisc mutableCopy];
//	NSMutableArray *allKeys = [[commonDisc allKeys] mutableCopy];
//	NSMutableArray *keysArray = [[NSMutableArray alloc] init];
//	NSMutableArray *identArray = [[NSMutableArray alloc] init];
//	
//	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//	[formatter setDateFormat:@"dd-MM-yyyy"];
//
//	for (NSString *key in allKeys) {
//		[keysArray addObject:[[key componentsSeparatedByString:@"##"] objectAtIndex:0]] ;
//		[identArray addObject:[[key componentsSeparatedByString:@"##"] objectAtIndex:1]];
//	}
//	
//	
//	for(int j = 0; j < [keysArray count]; j++ ) {
//		for (int i = j; i < [keysArray count]; i++) {
//			NSDate *currentDate = [formatter dateFromString:[keysArray objectAtIndex:j]];
//			NSDate *nextDate = [formatter dateFromString:[keysArray objectAtIndex:i]];
////			NSLog(@"Current Date %@, Next Date %@", currentDate, nextDate);
//			if (ascendingOrder) {
//				if ([currentDate compare:nextDate] == NSOrderedAscending) {
//					[keysArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					[allKeys exchangeObjectAtIndex:j withObjectAtIndex:i];
//				}
//			}else {
//				if ([currentDate compare:nextDate] == NSOrderedDescending) {
//					[keysArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					[allKeys exchangeObjectAtIndex:j withObjectAtIndex:i];
//				}
//			}
//		}
//	}
//	
//	NSMutableArray *temp = [[NSMutableArray alloc] init];
//	for (int i = 0; i< [allKeys count]; i++) {
//		LocalDisctionaryClass *localDisctionary = [[LocalDisctionaryClass alloc] init];
//		NSString *key = [allKeys objectAtIndex:i];
//		id obj = [[commonDiscLocal objectForKey:key] copy];
//		localDisctionary.obj = obj; 
//		localDisctionary.key = key;
//
////		NSLog(@"Key: %@, Obj: %@", key, obj);
//		[temp addObject:localDisctionary];
//		[localDisctionary release];
//	}
//	
//	[allKeys release];
//	[keysArray release];
//	[identArray release];
//	[formatter release];
//	[commonDiscLocal release];
//
//	return [temp autorelease];
//}

//+(NSMutableDictionary *)sortTODODisctionaryByPriority:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder{ 
//	NSMutableDictionary *discLocal = [Disc mutableCopy];
//	NSMutableArray *allKeys = [[discLocal allKeys] mutableCopy];
//	NSMutableArray *allValues = [[discLocal allValues] mutableCopy];
//	
//	for (int k = 0; k < [allValues count]; k++) {
//		NSMutableArray *currentObjArray  = [[allValues objectAtIndex:k] retain];
//		if ([currentObjArray count] == 1) {
//			[currentObjArray  release];
//			continue;
//		}
//		
//		for(int j = 0; j < [currentObjArray count]; j++ ) {
//			for (int i = j; i < [currentObjArray count]; i++) {
//				TODODataObject  *currentTODODataObject = (TODODataObject*)[currentObjArray objectAtIndex:j];
//				TODODataObject  *nextTODODataObject = (TODODataObject*)[currentObjArray objectAtIndex:i];
//				
//				NSString *currentPriority = [currentTODODataObject priority];
//				NSString *nextPriority = [nextTODODataObject priority];
//				
//				if (ascendingOrder) {
//					if ([currentPriority compare:nextPriority] == NSOrderedAscending) {
//						[currentObjArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					}
//				}else {
//					if ([currentPriority compare:nextPriority] == NSOrderedDescending) {
//						[currentObjArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					}
//				}
//			}
//		}
//		
//		[allValues replaceObjectAtIndex:k withObject:currentObjArray];
//		[currentObjArray release];
//	}
//	
//	NSMutableDictionary *finalDics = [[NSDictionary alloc] initWithObjects:allValues forKeys:allKeys];
//	
//	[discLocal release];
//	[allKeys release];
//	[allValues release];
//	return [finalDics autorelease];
//}
//
//+(NSMutableDictionary *)sortTODODisctionaryByTitle:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder { 
//	NSMutableDictionary *discLocal = [Disc mutableCopy];
//	NSMutableArray *allKeys = [[discLocal allKeys] mutableCopy];
//	NSMutableArray *allValues = [[discLocal allValues] mutableCopy];
//	
//	for (int k = 0; k < [allValues count]; k++) {
//		NSMutableArray *currentObjArray  = [[allValues objectAtIndex:k] retain];
//		if ([currentObjArray count] == 1) {
//			[currentObjArray  release];
//			continue;
//		}
//		
//		for(int j = 0; j < [currentObjArray count]; j++ ) {
//			for (int i = j; i < [currentObjArray count]; i++) {
//				TODODataObject  *currentTODODataObject = (TODODataObject*)[currentObjArray objectAtIndex:j];
//				TODODataObject  *nextTODODataObject = (TODODataObject*)[currentObjArray objectAtIndex:i];
//				
//				NSString *currentTitle = [currentTODODataObject todoTitle];
//				NSString *nextTitle = [nextTODODataObject todoTitle];
//				
//				if (ascendingOrder) {
//					if ([currentTitle compare:nextTitle] == NSOrderedAscending) {
//						[currentObjArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					}
//				}else {
//					if ([currentTitle compare:nextTitle] == NSOrderedDescending) {
//						[currentObjArray exchangeObjectAtIndex:j withObjectAtIndex:i];
//					}
//				}
//			}
//		}
//		[allValues replaceObjectAtIndex:k withObject:currentObjArray];
//		[currentObjArray release];
//	}
//	
//	NSMutableDictionary *finalDics = [[NSDictionary alloc] initWithObjects:allValues forKeys:allKeys];
//	
//	[discLocal release];
//	[allKeys release];
//	[allValues release];
//	return [finalDics autorelease];
//}

+(NSMutableDictionary *)sortTODODisctionaryByStartDate:(NSMutableDictionary *)Disc inAscendingOrder:(BOOL) ascendingOrder {
	return Disc;
}

+(NSMutableDictionary *)sortTODODisctionaryByDateCreated:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder {
	return nil;
}

+(NSMutableDictionary *)sortTODODisctionaryByDateModified:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder {
	return nil;
}

@end










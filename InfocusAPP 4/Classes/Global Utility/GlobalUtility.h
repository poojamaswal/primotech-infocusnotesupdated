//
//  GlobalUtility.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "TKCalendarMonthView.h"

@interface GlobalUtility : NSObject {
}
+(void)setNavBarTitle:(NSString*)title forViewController:(id) VC; 
+(UILabel*)getLabelNavTitleWithFrame:(CGRect) frame;
+(void)createBackNavigationButtonWithImageName:(NSString*) imageNameLocal forViewController:(id)VC;
+(void)createRightNavigationButtonWithImageName:(NSString*) imageNameLocal forViewController:(id)VC ;
+(NSString *)getDocumentsDirectory;
+(void)getUnderImplementationAlert; 

//+(NSMutableArray *)sortCommonDisctionary:(NSMutableDictionary *)commonDisc inAscendingOrder: (BOOL) ascendingOrder;

//+(NSMutableDictionary *)sortTODODisctionaryByPriority:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder;
+(NSMutableDictionary *)sortTODODisctionaryByStartDate:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder;
//+(NSMutableDictionary *)sortTODODisctionaryByTitle:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder;
+(NSMutableDictionary *)sortTODODisctionaryByDateCreated:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder;
+(NSMutableDictionary *)sortTODODisctionaryByDateModified:(NSMutableDictionary *)Disc inAscendingOrder: (BOOL) ascendingOrder;


@end

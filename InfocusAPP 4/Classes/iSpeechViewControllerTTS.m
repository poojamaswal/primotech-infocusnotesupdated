//
//  iSpeechViewControllerTTS.m
//  Organizer
//
//  Created by Aman Gupta on 5/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "iSpeechViewControllerTTS.h"
#import "OrganizerAppDelegate.h"
@implementation iSpeechViewControllerTTS
static iSpeechViewControllerTTS *_sharedMySingleton = nil;

@synthesize iSpeech=_iSpeech;
@synthesize parentVC;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal
//
//{
//    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) 
//    {
//        self.parentVC =  parentVCLocal;
//    }
//    return self;
//}

+(iSpeechViewControllerTTS *)sharedApplication
{
    _sharedMySingleton = nil;
    
    @synchronized([iSpeechViewControllerTTS class])
	{
		if (!_sharedMySingleton)
            
            
			return _sharedMySingleton =[[self alloc] init];
        
    }
    
	return nil;
    
    
    
}

+(id)alloc
{
    @synchronized([iSpeechViewControllerTTS class])
	{
		NSAssert(_sharedMySingleton == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedMySingleton = [super alloc];
		return _sharedMySingleton;
	}
    
	return nil;
    
}
-(id)init {
	self = [super init];
	if (self != nil) {
		// initialize stuff here
	}
    
	return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    NSLog(@"ParentClass %@",[parentVC description]);
    
    //  [[[OrganizerAppDelegate appDelegate] iSpeech] ISpeechSetSpeakingDone:self];
    
    [super viewDidLoad];
    
}
-(void)readText:(NSString *)textToSpeak
{
    //    [[[OrganizerAppDelegate appDelegate] iSpeech]ISpeechSetSpeed:-5];
    //
    //    [[[OrganizerAppDelegate appDelegate] iSpeech]ISpeechSpeak:textToSpeak];
    
    ISSpeechSynthesis *synthesis = [[ISSpeechSynthesis alloc] initWithText:textToSpeak];
    [synthesis setSpeed:-10];
    
    [synthesis setDelegate:self];
    [synthesis setVoice:ISVoiceUSEnglishFemale];
    NSError *err;
    
    if(![synthesis speak:&err]) {
        NSLog(@"ERROR: %@", err);
    }
    
}
- (void)synthesis:(ISSpeechSynthesis *)speechSynthesis didFailWithError:(NSError *)error
{
    if([error code] == kISpeechErrorCodeNoInternetConnection) 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You are not connected to the Internet. Please double check your connection settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    } 
    
}



- (BOOL) ISpeechSpeak:(NSString *)text
{
    return YES;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

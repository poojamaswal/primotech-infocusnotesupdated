//
//  HomeViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h> //Steve
//#import <CoreLocation/CoreLocation.h> //Steve
//#import <SystemConfiguration/SystemConfiguration.h> //Steve

#import "CalendarParentController.h"
#import "ToDoParentController.h"

#import "NotesViewCantroller.h"
//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"
#import "SettingsViewController.h"

@interface HomeViewController  : UIViewController< UIWebViewDelegate>
{
	NSUserDefaults *userDefaults;
	NSInteger viewStatus;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIButton *aboutButton;  //Steve 
    IBOutlet UIView *aboutView;        //Steve 
    IBOutlet UIButton *aboutDoneButton; //Steve 
    IBOutlet UIImageView *nuanceImageView; //Steve
    IBOutlet UIView *helpView;          //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;  //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView;
    //AdWhirlView *adView;
    IBOutlet UILabel *appVersionNumberLabel; //Steve

    //Steve added
    IBOutlet UIView *infoButtonView;
    IBOutlet UIButton *VTTinfoButton;
    IBOutlet UIButton *okButtonVTTAuth;
    IBOutlet UIButton *cancelButtonVTTAuth;
    IBOutlet UITextField *userNameVTTAuthTextField;
    IBOutlet UITextField *passwordVTTAuthTextField;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UILabel *passwordLabel;
    IBOutlet UILabel *youAreAuthLabel;
    IBOutlet UILabel *notAvailableOnFREELabel;
    UITapGestureRecognizer *tap;
    
}
@property(readwrite)NSInteger viewStatus;
@property(nonatomic,strong)NSUserDefaults *userDefaults;
//@property(nonatomic,retain)AdWhirlView *adView;

-(IBAction) calendarButton_Cliked:(id)sender;
-(IBAction) toDoButton_Cliked:(id)sender;
-(IBAction) projectsButton_Cliked:(id)sender;
-(IBAction) notesButton_Cliked:(id)sender;
-(IBAction) graphButton_Cliked:(id)sender;
-(IBAction) settingsButton_Cliked:(id)sender;
- (IBAction)aboutButton_Clicked:(id)sender;  //Steve 
- (IBAction)aboutDone_Clicked:(id)sender;  //Steve
- (IBAction)elixirWebsiteURL_Clicked:(id)sender; //Steve
- (IBAction)helpView_Tapped:(id)sender; //Steve
- (IBAction)VTTinfoButton_Clicked:(UIButton *)sender; //Steve
- (IBAction)okBuutonVTTAuth_Clicked:(UIButton *)sender; //Steve
- (IBAction)cancelButtonVTTAuth_Clicked:(UIButton *)sender; //Steve

@end

//
//  HomeViewController.m
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "CalendarParentController.h"
//#import "NotesSingleViewCantroller.h"
#import "ProjectMainViewCantroller.h"
#import "ListToDoViewController.h"
#import "LIstToDoProjectViewController.h"
#import "Flurry.h" //Steve

//#import "AdWhirlView.h"
#define kSampleAppkey = @"2e2ab4db4452454ba676478bdfgffgedbe36"

#import "QuantcastMeasurement.h"
#import <AdSupport/AdSupport.h> //Steve
//#import <Foundation/Foundation.h> //Steve
//#import <CoreLocation/CoreLocation.h> //Steve
//#import <SystemConfiguration/SystemConfiguration.h> //Steve

#import "NotesViewCantroller.h"


@implementation HomeViewController
@synthesize userDefaults,viewStatus;
//@synthesize adView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Home" withLabels:Nil]; //Steve
    [Flurry logEvent:@"Home"]; //Steve
    
    if (!IS_IPHONE_5) { //Not iPhone5
        aboutButton.frame = CGRectMake(282, 380, 18, 19);
    }

}

- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}



/*
-(void)reportMdotMAppEvent:(NSString *)appid userToken:(NSString*)usertoken eventID:(NSString*)eventid transactionID:(NSString*)transactionid
{

    
    UIWebView *aWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [aWebView setDelegate:self];
    
    // replace TRUE with FALSE below for deviceid-less, privacy-compliant tracking
    NSString *deviceid = TRUE ? [[UIDevice currentDevice] uniqueIdentifier] : @"";
    NSString *odin = ODIN1(); // For details on generating ODIN1, see: http://code.google.com/p/odinmobile/wiki/ODIN1
    NSString *aid = [[ASIdentifierManager advertisingIdentifier] UUIDString];
    NSString *ate = [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled] ? @"1" : @"0";
    NSString *appOpenEndpoint =
    [NSString stringWithFormat:@"http://ads.mdotm.com/ads/event.php?appid=%@&usertoken=%@&aid=%@&ate=%@&odin=%@&eventid=%@&transactionid=%@&deviceid=%@",
     appid, usertoken, aid, ate, odin, eventid, transactionid, deviceid];
    NSURL *url = [NSURL URLWithString:appOpenEndpoint];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [aWebView loadRequest:requestObj];
    [self.view addSubview:aWebView];
    //[aWebView release],
    aWebView = nil;
}
*/


-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear HomeViewController");
    //Steve - change color back to white otherwise all other icons will be gray from Settings Nvigation
     //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

    
    //[self reportMdotMAppEvent:@"[APPID]" userToken:@"abcd1234" eventID:@"registration" transactionID:@""];
    
    //Steve added - Shows info button if IsVTTDevice
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	if(appdelegate.IsVTTDevice && !IS_LIST_VERSION && !IS_LITE_VERSION){
        VTTinfoButton.hidden = NO;
    }
    else
        VTTinfoButton.hidden = YES;

    
    if(IS_LITE_VERSION)
    {
        //AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
        //[adWhirlView setFrame:CGRectMake(0, 410, 320, 50)];
        
        //[self.view addSubview:adWhirlView];
        [super viewDidLoad];
    }
    ///Alok Gupta Added for Navigation bar Title of Lite version
    if(IS_LITE_VERSION)
        [[[navBar items]objectAtIndex:0]setTitle:@"InFocus Lite"];

     self.view.backgroundColor = [UIColor underPageBackgroundColor];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"]; 
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
	
	userDefaults = [NSUserDefaults standardUserDefaults];
	
	
	if ([[NSUserDefaults standardUserDefaults] integerForKey:@"optionVal"] == 0)
	{
		[userDefaults setInteger:1 forKey:@"optionVal"];
		[userDefaults synchronize];
		
		
	}
	if([[NSUserDefaults standardUserDefaults] integerForKey:@"optionVal"] == 2)
	{
		viewStatus=0;
	}
	else if ([[NSUserDefaults standardUserDefaults] integerForKey:@"optionVal"] == 1)
	{
		viewStatus=1;
	}
	
	aboutView.hidden = YES;  //Steve
    helpView.hidden = YES;  //Steve


    ////////////// Steve Added /////////////////////////
    //Sets the About page "Done" button
    [aboutDoneButton setTitle:@"Done" forState:UIControlStateNormal]; 
    [aboutDoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [aboutDoneButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
    aboutDoneButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    aboutDoneButton.layer.cornerRadius = 10.0;
    aboutDoneButton.layer.borderWidth = 1.0;
    aboutDoneButton.layer.borderColor = [[UIColor grayColor]CGColor];
    aboutDoneButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    ////////////// Steve Added End /////////////////////////
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_Home"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        [helpView addGestureRecognizer:tapHelpView];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];

        helpView.hidden = NO;
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 0.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_Home"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
    }

    [super viewWillAppear:YES];
}

//Steve added for Help views
-(void)viewDidAppear:(BOOL)animated{
 
}

-(IBAction)calendarButton_Cliked:(id)sender { 
	CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:calendarParentController animated:YES];
}

-(IBAction) toDoButton_Cliked:(id)sender {
	ToDoParentController *toDoParentController = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:toDoParentController animated:YES];
}

-(IBAction) projectsButton_Cliked:(id)sender 
{

	ProjectMainViewCantroller *projectsViewController = [[ProjectMainViewCantroller alloc] initWithNibName:@"ProjectMainViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:projectsViewController animated:YES];
	
}

-(IBAction)notesButton_Cliked:(id)sender{
    
		NotesViewCantroller *notesViewCantroller =[[NotesViewCantroller alloc]initWithNibName:@"NotesViewCantroller" bundle:[NSBundle mainBundle]];
		[self.navigationController pushViewController:notesViewCantroller animated:YES];
			
	
}
-(IBAction) graphButton_Cliked:(id)sender{

    //Alok Upates This For testing List Projects
   LIstToDoProjectViewController *ptr_ListToDoViewController = [[LIstToDoProjectViewController alloc]initWithNibName:@"LIstToDoProjectViewController" bundle:nil];
    
    //ListToDoViewController *ptr_ListToDoViewController = [[ListToDoViewController alloc]initWithNibName:@"ListToDoViewController" bundle:nil];
    
    [self.navigationController pushViewController:ptr_ListToDoViewController animated:YES];
    

}
-(IBAction) settingsButton_Cliked:(id)sender{
    
    /*
    SettingsViewController *settingsViewController = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:settingsViewController animated:YES];
     */
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SettingsTableViewController" bundle:nil];
    UIViewController *settingsTableViewController = [sb instantiateViewControllerWithIdentifier:@"SettingsTableViewController"];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    [self presentViewController:settingsTableViewController animated:YES completion:nil];
    
}

//Steve added
//About Button with company info
- (IBAction)aboutButton_Clicked:(id)sender {
    
    //Steve added - adds app version number (in String format)
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSLog(@"********** appVersionString =  %@",appVersionString);
    
    appVersionNumberLabel.text = appVersionString;
    
    nuanceImageView.hidden = YES;
    aboutView.hidden = NO;
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *) [[UIApplication sharedApplication] delegate];
	if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized && !IS_LITE_VERSION)
    {
        nuanceImageView.hidden = NO;
    }
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.50;
    [aboutView.layer addAnimation:transition forKey:kCATransitionFromTop];
    [self.view addSubview:aboutView];
    
    
}

//Steve added
//about button "Done" clicked - removes view
- (IBAction)aboutDone_Clicked:(id)sender {

    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.50;
    [aboutView.layer addAnimation:transition forKey:kCATransitionFromTop];
 
    aboutView.hidden = YES;
}

//Steve added - button Clicked under URL Label
- (IBAction)elixirWebsiteURL_Clicked:(id)sender {
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http:ElixirSoftwareGroup.com"]];
}


- (IBAction)helpView_Tapped:(id)sender {
   
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];

    helpView.hidden = YES;
    
}

-(IBAction)VTTinfoButton_Clicked:(id)sender {
    NSLog(@"VTTinfoButton_Clicked");
    
    infoButtonView.userInteractionEnabled = YES;
    aboutDoneButton.userInteractionEnabled = NO;

    
    //Steve added - sets up "infoButtonView" view
    infoButtonView.hidden = YES;
    infoButtonView.layer.borderWidth = 0.0f;
    infoButtonView.layer.cornerRadius = 8.0f;
    infoButtonView.layer.shadowColor = [[UIColor grayColor] CGColor];
    infoButtonView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    infoButtonView.layer.shadowOpacity = 1.0f;
    infoButtonView.layer.shadowRadius = 5.0f;
    infoButtonView.layer.masksToBounds = NO;
    infoButtonView.clipsToBounds = NO;
    
    //Sets the About page "OK" button on "infoButtonView" view
    [okButtonVTTAuth setTitle:@"OK" forState:UIControlStateNormal];
    [okButtonVTTAuth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okButtonVTTAuth setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
    
    okButtonVTTAuth.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    okButtonVTTAuth.layer.cornerRadius = 10.0;
    okButtonVTTAuth.layer.borderWidth = 1.0;
    //okButtonVTTAuth.layer.borderColor = [[UIColor grayColor]CGColor];
    okButtonVTTAuth.layer.borderColor = [[UIColor whiteColor]CGColor];
    okButtonVTTAuth.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    //Sets the About page "Cancel" button on "infoButtonView" view
    [cancelButtonVTTAuth setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButtonVTTAuth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButtonVTTAuth setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState:UIControlStateHighlighted];
    cancelButtonVTTAuth.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    
    cancelButtonVTTAuth.layer.cornerRadius = 10.0;
    cancelButtonVTTAuth.layer.borderWidth = 1.0;
    //cancelButtonVTTAuth.layer.borderColor = [[UIColor grayColor]CGColor];
    //cancelButtonVTTAuth.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    cancelButtonVTTAuth.layer.borderColor = [[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]CGColor];
    cancelButtonVTTAuth.backgroundColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    
    
    
    [UIView beginAnimations:nil context:nil];
    infoButtonView.hidden = NO;
    infoButtonView.alpha = 0.0f;
    //[self.view addSubview:loadingView];
    
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:0.7];
    [infoButtonView  setAlpha:1];
    [UIView commitAnimations];
    
    [aboutView addSubview:infoButtonView];
    
    
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //if is VTT authorized, no need to offer username & password to reset.
	if(appDelegate.isVTTDeviceAuthorized) {
        
        infoButtonView.frame = CGRectMake(20, 70, 280, 85);
        
        youAreAuthLabel.textColor = [UIColor blueColor];
        youAreAuthLabel.hidden = NO;
        
        notAvailableOnFREELabel.hidden = YES;
        
        okButtonVTTAuth.hidden = YES;
        cancelButtonVTTAuth.hidden = YES;
        userNameVTTAuthTextField.hidden = YES;
        passwordVTTAuthTextField.hidden = YES;
        userNameLabel.hidden = YES;
        passwordLabel.hidden = YES;
        
        tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelButtonVTTAuth_Clicked:)];
        [self.view addGestureRecognizer:tap];
        
    }
    else{
        infoButtonView.frame = CGRectMake(20, 70, 280, 170);
        
        youAreAuthLabel.hidden = YES;
        
        notAvailableOnFREELabel.hidden = NO;
        notAvailableOnFREELabel.textColor = [UIColor blueColor];
        
        okButtonVTTAuth.hidden = NO;
        cancelButtonVTTAuth.hidden = NO;
        userNameVTTAuthTextField.hidden = NO;
        passwordVTTAuthTextField.hidden = NO;
        userNameLabel.hidden = NO;
        passwordLabel.hidden = NO;
    }
    
}


- (IBAction)okBuutonVTTAuth_Clicked:(UIButton *)sender {
    NSLog(@"okBuutonVTTAuth_Clicked");
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *fileNameForUser = @"VTTfile";
    NSString *content = @"FFF5553Aa";
    NSError  *error;
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Checks username & password with text fields to see if correct
    if ([userNameVTTAuthTextField.text isEqualToString:fileNameForUser] && [passwordVTTAuthTextField.text isEqualToString:content] ) {
        
        appDelegate.isVTTDeviceAuthorized = YES;
        
        //get the documents directory:
        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        
        //make a directory & file name to write the data to using the documents directory:
        NSString *directoryName = [NSString stringWithFormat:@"%@/VoiceToText",documentsDirectory];
        NSString *fileName = [NSString stringWithFormat:@"%@/VTTfile.txt",directoryName];
        
        //Create Directory Folder & then write content to file
        [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
        [content writeToFile:fileName atomically:NO  encoding:NSStringEncodingConversionAllowLossy  error:&error];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authorized" message:@"You are authorized to use Voice To Text." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
        
        
    }
    else{
        appDelegate.isVTTDeviceAuthorized = NO;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Your user name and/or password is not correct. If you downloaded the FREE version of this app you will not have Voice To Text. Please contact customer support if you feel this is an error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
    }
    
    infoButtonView.hidden = YES;
    infoButtonView.userInteractionEnabled = NO;
       aboutDoneButton.userInteractionEnabled = YES;
    [infoButtonView removeFromSuperview];
    
    
    ///////////////////////////////////////////
    //----- TEST --> LIST ALL FILES -----
    ///////////////////////////////////////////
    
    NSLog(@"LISTING ALL FILES FOUND");
    
    NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    //make a directory & file name to write the data to using the documents directory:
    NSString *directoryName = [NSString stringWithFormat:@"%@/VoiceToText",documentsDirectory];
    
    int Count;
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:directoryName error:&error];
    
    for (Count = 0; Count < (int)[directoryContent count]; Count++)
    {
        NSLog(@"File %d: %@", (Count + 1), [directoryContent objectAtIndex:Count]);
    }
    
    
}
- (IBAction)cancelButtonVTTAuth_Clicked:(UIButton *)sender {
    NSLog(@"cancelButtonVTTAuth_Clicked");
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isVTTDeviceAuthorized) {
        [self.view removeGestureRecognizer:tap];
    }
    
    infoButtonView.hidden = YES;
    infoButtonView.userInteractionEnabled = NO;
    aboutDoneButton.userInteractionEnabled = YES;
    [infoButtonView removeFromSuperview];
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    navBar = nil;
    aboutButton = nil;
    aboutView = nil;
   // [self setAboutDone_Clicked:nil];
   // aboutDone_Clicked = nil;
    nuanceImageView = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    appVersionNumberLabel = nil;
    infoButtonView = nil;
    [super viewDidUnload];
    
    //aboutDoneButton
}




@end










//
//  ColorSelectorViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorSelectorViewController : UIViewController {
	UIView *aView;
	UISlider *redSlider;
	UISlider *greenSlider;
	UISlider *blueSlider;
	id        VC;
	CGFloat red;
	CGFloat green;
	CGFloat blue;
	CGFloat alpha;
}

@property(nonatomic , readonly)float red;
@property(nonatomic , readonly)float green;
@property(nonatomic , readonly)float blue;
@property(nonatomic , readonly)float alpha;

- (UIColor*)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy;

@end

//
//  ColorSelectorViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ColorSelectorViewController.h"
#import "GlobalUtility.h"

@implementation ColorSelectorViewController
//@synthesize red,green,blue,alpha;
#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -
/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	int x=5,y=5,j;
	
//	NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"black.png",@"blue red.png",@"blue.png",
//							 @"dark yellow.png",@"green.png",@"Grey.png",@"light blue.png",@"light blue1.png",
//							 @"light green.png",@"light grey.png",@"light red.png",@"Pink.png",@"red.png",
//							 @"voilet.png",@"White.png",@"yellow.png",nil];
	
	NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"black1.png", @"charcoal1.png", @"darkblue1.png", @"blue1.png",
							 @"purple1.png", @"pink1.png", @"darkgreen1.png", @"green1.png", @"olive1.png", @"red1.png",
							 @"darkred1.png", @"teal1.png", @"blueviolet1.png", @"saddlebrown1.png", @"indigo1.png", @"dimgray1.png",
							 nil];
	
	for (int i=0; i<[imgNameArray count]; i++) {
		for (j=0; j<4; j++) {
			UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
			[btn setFrame:CGRectMake(x, y, 50, 50)];
			[btn setBackgroundImage:[UIImage imageNamed:[imgNameArray objectAtIndex:i]] forState:UIControlStateNormal];
			[btn setTag:i];
			[btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
			[self.view addSubview:btn];
			x=x+63+10;
			i++;
		}
		i--;
		//j = 0;
		x = 5;
		y = y+63+5;
		
	}
	
	aView=[[UIView alloc] initWithFrame:CGRectMake(200,y+10 , 80, 80)];
	[aView setBackgroundColor:[UIColor redColor]];
    //self.view.backgroundColor=[UIColor blueColor];
	
	redSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y+10, 150, 23)];
	[redSlider setMaximumValue:1.0]; 
	[redSlider setMinimumValue:0.0];
	[redSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:redSlider];
	
	greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y+43, 150, 23)];
	[greenSlider setMaximumValue:1.0]; 
	[greenSlider setMinimumValue:0.0];
	[greenSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:greenSlider];
	
	blueSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y+83, 150, 23)];
	[blueSlider setMaximumValue:1.0]; 
	[blueSlider setMinimumValue:0.0];
	[blueSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:blueSlider];
	
	[self.view addSubview:aView];
	
	UIButton *okButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
	[okButton setTitle:@"Save Color" forState:UIControlStateNormal];
	[okButton setFrame:CGRectMake(100, y+116, 150, 31)];
	[okButton addTarget:self action:@selector(colorSelected:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:okButton];
}

#pragma mark -
#pragma mark All Methods
#pragma mark -
-(void)btnClicked:(id)sender{
	UIImage *image=[sender backgroundImageForState:UIControlStateNormal];
	
	[self getRGBAsFromImage:image atX:10 andY:10];
}


- (UIColor*)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy
{
	// First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,bitsPerComponent, bytesPerRow, colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
	
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
	
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
	red = (rawData[byteIndex] * 1.0) / 255.0;
	green = (rawData[byteIndex + 1] * 1.0) / 255.0;
	blue = (rawData[byteIndex + 2] * 1.0) / 255.0;
	alpha = 1.0;//(rawData[byteIndex + 3] * 1.0) / 255.0;
	//byteIndex += 4;
	
	[redSlider setValue:red animated:YES];
	[greenSlider setValue:green animated:YES];
	[blueSlider setValue:blue animated:YES];
	UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
	
	[aView setBackgroundColor:acolor];
    free(rawData);
	
    return acolor;
	
}
-(void)valueChanged:(id)sender{
	
	UIColor *acolor = [UIColor colorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value alpha:1.0];
	red = redSlider.value;
	green = greenSlider.value;
	blue = blueSlider.value;
	
	[aView setBackgroundColor:acolor];
	
}

-(void)colorSelected:(id)sender{
	id VC1 = [[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] - 2)];
	
	[VC1 setColor:[UIColor colorWithRed:red green:green blue:blue alpha:1]];
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark -
#pragma mark Memory Management
#pragma mark -
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}



@end

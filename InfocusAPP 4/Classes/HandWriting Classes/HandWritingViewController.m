//
//  HandWritingViewController.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HandWritingViewController.h"
#import "GlobalUtility.h"
#import "AppointmentViewController .h"
#import "ColorSelectorViewController.h"
#import "OrganizerAppDelegate.h"
#import "UIImage+Resize.h"
#import "AddNewNotesViewCantroller.h"
//Steve added
#import "ListToDoViewController.h"
#import "ToDoParentController.h"
#import "ToDoViewController.h"


@interface HandWritingViewController()

@property (strong, nonatomic) IBOutlet UIButton *doneButton_iOS7; //Steve
@property (strong, nonatomic) IBOutlet UIButton *previewCancelButton; //Steve
@property (strong, nonatomic) IBOutlet UILabel *previewLabel_iOS7; //Steve
@property (strong, nonatomic) IBOutlet UIButton *previewCancelButton_iOS7; //Steve
@property (strong, nonatomic) IBOutlet UIButton *previewClearButton_iOS7; //Steve
@property (strong, nonatomic) IBOutlet UIButton *previewDoneButton_iOS7;
@property (strong, nonatomic) NSString *viewControllerName;

-(void)addAdditionalControlsInHandWriting;
-(void)setLandscapeMode;
- (void) removeFromSuperviewWithDelay; //Steve added

@end

@implementation HandWritingViewController
@synthesize parentVC,delegate,whatVAL, lineCount, posIndLabel;
@synthesize nextLineButton, nextLineButtonSideBar;
@synthesize previousLineButton;
@synthesize scrollLeftButton;
@synthesize scrollRightButton, scrollRightButtonSideBar;
@synthesize isEditingToDo;
@synthesize toDoBeingEdited;
@synthesize isSettingNotesHWObject;
@synthesize previewCancelButton; //Steve
//@synthesize drawImage;
//@synthesize playButton;

#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

//Steve added: sPenIcon:(BOOL) isPendIcon
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal isPenIcon:(BOOL) isPenIcon{
    
    if (IS_IPAD)
    {
        self=[super initWithNibName:@"HandWritingViewController_iPad" bundle:[NSBundle mainBundle]];
    }
    else
    {
        self=[super initWithNibName:@"HandWritingViewController" bundle:[NSBundle mainBundle]];
    }
    
    
    //NSLog(@"initWithNibName");
    self.parentVC =  parentVCLocal;
    self.isEditingToDo = NO; // Added by anil
    toDoBeingEdited = nil; // Added by Anil
    isSettingNotesHWObject = NO; // Added by Anil
    isComingFromPenIcon_ListView = isPenIcon; //Steve added
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //NSLog(@"viewDidLoad");
    
    
    
    //Steve - protocol to scroll from PaintingView
    drawImage.scrollDelegate = self;
    isAutoScrollOn = YES;
    
    
    //drawImage = [[PaintingView alloc]init]; //Steve TEST
    
    
    //////// Steve - removes Stop Button /////////////////
    _savedNavBarItems = [navBar.items mutableCopy];
    
    NSMutableArray     *items = [navBar.items mutableCopy];
    [items removeObjectAtIndex:1];
    navBar.items = items;
    
    ////////////// Steve End ///////////////////////
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    //Steve commented
    if (IS_IPAD) {
        //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    }
    
    NSUserDefaults *controller = [NSUserDefaults standardUserDefaults];
    
    _viewControllerName=[NSString stringWithFormat:@"%@",[controller valueForKey:@"Viewcontroller"]];
    
    
    if (IS_IPAD)
    {
        
        if([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"ToDo"])
        {
            self.view.frame = CGRectMake(0, 704, self.view.frame.size.width, 320); //was 0, 704, 568, 320
            self.view.backgroundColor = [UIColor grayColor];
            navBar.frame = CGRectMake(0, 20, self.view.frame.size.width, 44);
            
            previewClearButton.frame = CGRectMake(300, 8, 51, 29);
            previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
            
        }
        else if([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"List"])
        {
            self.view.frame = CGRectMake(0, 704, 459, 320); //was 0, 704, 568, 320
            self.view.backgroundColor = [UIColor clearColor];
            navBar.frame = CGRectMake(0, 20, 459, 44);
            
            previewClearButton.frame = CGRectMake(300, 8, 51, 29);
            previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
        }
        else if([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Projects"])
        {
            self.view.frame = CGRectMake(0, 704, 459, 320); //was 0, 704, 568, 320
            self.view.backgroundColor = [UIColor clearColor];
            navBar.frame = CGRectMake(0, 20, 459, 44);
            
            previewClearButton.frame = CGRectMake(300, 8, 51, 29);
            previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
        }
        else
        {
            self.view.frame = CGRectMake(0, 704, 768, 320); //was 0, 704, 568, 320
            self.view.backgroundColor = [UIColor clearColor];
            navBar.frame = CGRectMake(0, 0, 768, 44);
            previewClearButton.frame = CGRectMake(351, 8, 51, 29);
            previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
            
        }
        
        
        //Steve commented.  Causes NavBar to look larger & ackward.
        /*
        if (![[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Calender"])
        {
            anthVw.frame = CGRectMake(anthVw.frame.origin.x,anthVw.frame.origin.y+20,anthVw.frame.size.width,anthVw.frame.size.height+20);
            UIView * fakeView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
            fakeView.backgroundColor=[UIColor colorWithRed:0.2980 green:0.2980 blue:0.2980 alpha:1.0];
            [self.view addSubview:fakeView];
            navBar.frame = CGRectMake(0, 20, self.view.frame.size.width, 44);
            // previewNavBar.frame=CGRectMake(0, 20, 459, 44);
            forwardLabelView.hidden=YES;
        }
         */
    }
    
    else{  //iPhone
        
        self.view.frame = [[UIScreen mainScreen] applicationFrame];
        CGRect originalFrame = self.view.frame;
        originalFrame.origin.x = 20.0;
        self.view.frame = originalFrame;
        previewClearButton.frame = CGRectMake(351, 8, 51, 29);
        previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
        
    }
    
    //NSLog(@" self.view.frame #1 = %@ ", NSStringFromCGRect(self.view.frame));
    //NSLog(@" self.navigationController.view.frame #1 = %@ ", NSStringFromCGRect(self.navigationController.view.frame));
    //NSLog(@" ****UIScreen.mainScreen.applicationFrame #1 = %@ ", NSStringFromCGRect(UIScreen.mainScreen.applicationFrame));
    
    //Steve - set up autoScroll View that displays message "Auto Scroll ON/OFF"
    
    _autoScrollMessageView.hidden = YES;
    _autoScrollLabel_OFF.hidden = YES;
    _autoScrollLabel_ON.hidden = YES;
    _autoScrollMessageView.userInteractionEnabled = NO;
    
    _autoScrollMessageView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _autoScrollMessageView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    _autoScrollMessageView.layer.shadowOpacity = 0.5f;
    _autoScrollMessageView.layer.shadowRadius = 5.0f;
    _autoScrollMessageView.layer.cornerRadius = 8.0;
    
    
    // previewClearButton.frame = CGRectMake(351, 8, 51, 29);
    // previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
    
    
    //    previewClearButton.frame = CGRectMake(351, 8, 51, 29);
    //    previewSaveButton.frame = CGRectMake(420, 8, 51, 29);
    
    helpHandwriteImage_iPhone5.hidden = NO;
    helpHandwriteImage.hidden = YES;
    helpHandwriteImage_iOS5.hidden = YES;
    
#pragma mark -- IPad Primotech
    
    //pooja-iPad
    if(IS_IPAD)
    {
        // Update  (Karan Changes)
        if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"ToDo"])
        {
            anthImgVw.frame = CGRectMake(768/4 - 280/4, 64, 280, 182);
            anthImgVw.backgroundColor = [UIColor redColor];
            drawImage.frame = CGRectMake(0, 64, 1200, 840); //0,44,1600,840
            //NSLog(@"drawImage.frame = %@", NSStringFromCGRect(drawImage.frame));
            
            self.lineView.frame = CGRectMake(10, 350, 500, 1); //80+68,220,280,1
            _autoScrollMessageView.frame = CGRectMake(768/4-160/4, 113, 160, 50); //204, 113, 160, 50
            self.scrollZoneView.frame = CGRectMake(326 + 88 + 200, 44, 0, 768-128); //326 + 88, 44, 0, 440
            //self.scrollZoneView.backgroundColor = [UIColor blueColor];
            
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88 + 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88 + 88, 264, 50, 60);//Steve
            
        }
        else if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"List"])
        {
            anthImgVw.frame = CGRectMake(768/4 - 280/4, 64, 280, 182);
            anthImgVw.backgroundColor = [UIColor whiteColor];
            drawImage.frame = CGRectMake(0, 64, 1200, 840); //0,44,1600,840
            //NSLog(@"drawImage.frame = %@", NSStringFromCGRect(drawImage.frame));
            
            // self.lineView.frame = CGRectMake(70, 330, 300, 1); //80+68,220,280,1
            self.lineView.frame = CGRectMake(10, 350, 500, 1);
            _autoScrollMessageView.frame = CGRectMake(768/2-160/2, 113, 160, 50); //204, 113, 160, 50
            self.scrollZoneView.frame = CGRectMake(326 + 88 + 200, 44, 0, 768-128); //326 + 88, 44, 0, 440
            //self.scrollZoneView.backgroundColor = [UIColor blueColor];
            
            // NSLog(@"Frame is:  %f --  %f --   %f -- %f", anthVw.frame.origin.x,anthVw.frame.origin.y,anthVw.frame.size.width,anthVw.frame.size.height);
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88 + 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88 + 88, 264, 50, 60);//Steve
            
            
        }
        else if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Projects"])
        {
            anthImgVw.frame = CGRectMake(768/4 - 280/4, 64, 280, 182);
            anthImgVw.backgroundColor = [UIColor whiteColor];
            drawImage.frame = CGRectMake(0, 64, 1200, 840); //0,44,1600,840
            //NSLog(@"drawImage.frame = %@", NSStringFromCGRect(drawImage.frame));
            
            // self.lineView.frame = CGRectMake(768/4-500/4, 220, 500, 1); //80+68,220,280,1
            self.lineView.frame = CGRectMake(0, 345, 500, 1);
            _autoScrollMessageView.frame = CGRectMake(768/2-160/2, 113, 160, 50); //204, 113, 160, 50
            self.scrollZoneView.frame = CGRectMake(326 + 88 + 200, 44, 0, 768-128); //326 + 88, 44, 0, 440
            //self.scrollZoneView.backgroundColor = [UIColor blueColor];
            
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88 + 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88 + 88, 264, 50, 60);//Steve
            
            
        }
        else
        {
            anthImgVw.frame = CGRectMake(768/2 - 280/2, 64, 280, 182);
            anthImgVw.backgroundColor = [UIColor whiteColor];
            drawImage.frame = CGRectMake(0, 44, 1536, 840); //0,44,1600,840
            //NSLog(@"drawImage.frame = %@", NSStringFromCGRect(drawImage.frame));
            
            self.lineView.frame = CGRectMake(80, 310, self.view.frame.size.width-160, 1); //80+68,220,280,1
            _autoScrollMessageView.frame = CGRectMake(768/2-160/2, 113, 160, 50); //204, 113, 160, 50
            self.scrollZoneView.frame = CGRectMake(326 + 88 + 200, 44, 0, 768-128); //326 + 88, 44, 0, 440
            //self.scrollZoneView.backgroundColor = [UIColor blueColor];
            
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88 + 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88 + 88, 264, 50, 60);//Steve
        }
        
    }
    else
    {
        
        if (IS_IPHONE_5) {
            anthImgVw.frame = CGRectMake(90, 64, 280, 182);
            drawImage.frame = CGRectMake(0, 44, 1600, 840); // Steve TEST was: CGRectMake(0, 44, 1600, 840);
            
            self.lineView.frame = CGRectMake(80 + 68, 220, 280, 1); //Steve
            _autoScrollMessageView.frame = CGRectMake(204, 113, 160, 50);
            self.scrollZoneView.frame = CGRectMake(326 + 88, 44, 0, 440); //Steve
            
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88 + 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88 + 88, 264, 50, 60);//Steve
        }
        else{
            
            helpHandwriteImage.hidden = NO;
            helpHandwriteImage_iPhone5.hidden = YES;
            helpHandwriteImage_iOS5.hidden = YES;
            
            self.lineView.frame = CGRectMake(80, 220, 280, 1); //Steve
            _autoScrollMessageView.frame = CGRectMake(160, 113, 160, 50);
            self.scrollZoneView.frame = CGRectMake(326, 44, 0, 440); //Steve
            
            scrollRightButtonSideBar.frame = CGRectMake(504 - 88, 264, 50, 60); //Steve
            nextLineButtonSideBar.frame = CGRectMake(504-88, 264, 50, 60);//Steve
            
        }
    }
    
    lineNumber = 1;
    self.lineCount.text = [NSString stringWithFormat:@"Line %d", lineNumber];
    self.lineCount.textColor = [UIColor colorWithRed:193.0f/255.0f green:136.0f/255.0f blue:12.0f/255.0f alpha:1]; //Steve added
    posIndLabel.text = @"Scroll fwd";
    
    //NSLog(@"lineCount.frame =  %@", NSStringFromCGRect(self.lineCount.frame));
    
    ////////////// Steve added  //////////////////////
    
    forwardView.backgroundColor = [UIColor clearColor];
    forwardLabelView.backgroundColor = [UIColor clearColor];
    forwardView.userInteractionEnabled = YES;
    forwardLabelView.userInteractionEnabled = NO;
    
    ////////////// Steve added  End ////////////////////
    
    
    [previousLineButton setEnabled:NO];
    [scrollLeftButton setEnabled:NO];
    
    [scrollRightButtonSideBar setEnabled:YES];
    [nextLineButton setEnabled:YES];
    
    
    [scrollRightButtonSideBar setHidden:NO];
    [nextLineButtonSideBar setHidden:YES];
    
    [anthImgVw.layer setCornerRadius:10.0];
    anthImgVw.layer.masksToBounds = YES;
    anthImgVw.layer.borderColor = [UIColor grayColor].CGColor;
    anthImgVw.layer.borderWidth = 1.5;
}

#pragma mark -- IPad Primotech
#pragma mark -- Hide TabBar
- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}


-(void)viewWillAppear:(BOOL)animated{
    //NSLog(@"viewWillAppear");
    [super viewWillAppear:YES];
    
    //    NSArray *controllerArray=[parentVC viewControllers];
    //
    //    for (UIViewController *controller in controllerArray)
    //    {
    //        //Code here.. e.g. print their titles to see the array setup;
    //        NSLog(@"%@",controller.title);
    //    }
    
    
    
    if (IS_IPAD) {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    
    if (IS_IOS_7){
        drawImage.backgroundColor = [UIColor whiteColor];
        //drawImage.backgroundColor = [UIColor yellowColor];
    }else //iOS 6
        drawImage.backgroundColor = [UIColor colorWithRed:244.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0];
    
    
    self.navigationController.navigationBarHidden = YES;
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (IS_IOS_7) {
        
        
        self.edgesForExtendedLayout = UIRectEdgeNone;//UIRectEdgeNone
        self.view.backgroundColor = [UIColor whiteColor];
        
        
        navBar.barTintColor = [UIColor blackColor];
        navBar.tintColor = [UIColor whiteColor];
        navBar.translucent = NO;
        navBar.alpha = 0.7;
        
        
        //done Button - Floating in NIB
        if (!IS_IPAD)
        {
            doneButton.hidden = NO;
            _doneButton_iOS7.tintColor = [UIColor whiteColor];
            _doneButton_iOS7.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue"  size:18];
        }else
        {
            _doneButton_iOS7.hidden=YES;
        }
        
        
        //pooja-iPad
        
        NSUserDefaults *controller=[NSUserDefaults standardUserDefaults];
        if(IS_IPAD)
        {
            if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"ToDo"])
            {
                _doneButton_iOS7.frame = CGRectMake(400,25, 51, 29);
                
            }else if([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"List"])
            {
                _doneButton_iOS7.frame = CGRectMake(400, 7, 51, 29);//Steve}
            }else if([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Projects"])
            {
                _doneButton_iOS7.frame = CGRectMake(400, 7, 51, 29);//Steve}
            }
            else
            {
                _doneButton_iOS7.frame = CGRectMake(700, 7, 51, 29);
            }
        }
        else
        {
            if (IS_IPHONE_5) {
                
                _doneButton_iOS7.frame = CGRectMake(513, 7, 51, 29);
            }
            else{
                
                _doneButton_iOS7.frame = CGRectMake(425, 7, 51, 29);
            }
            
        }
        [self.view addSubview:_doneButton_iOS7];
        
        
        
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
        
        
    }
    else{ //iOS 6
        
        _previewCancelButton_iOS7.hidden = YES;
        _previewClearButton_iOS7.hidden = YES;
        
        //Steve added - Keeps ToolBar items white, otherwise after going to "Settings", toobar items turns gray.
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
        
        
        //Sets Navigation item color
        [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        /*
         UINavigationBar *navBarNew = [[self navigationController]navigationBar];
         UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
         [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
         */
        
        [navBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    }
    
    
    
    self.penBarButton.tintColor = [UIColor colorWithRed:103.0/255.0f green:150.0/255.0f blue:237.0/255.0f alpha:1.0];
    self.eraserBarButton.tintColor = [UIColor whiteColor];
    
    
    //Steve
    if (!IS_IPAD) { //iPhone
        [self setLandscapeMode];
    }
    
    
    //NSLog(@"self.view.frame = %@",NSStringFromCGRect(self.view.frame));
    //NSLog(@"  self.navigationController.view.bounds = %@",NSStringFromCGRect(self.navigationController.view.bounds));
    //NSLog(@"  self.navigationController.view.frame = %@",NSStringFromCGRect(self.navigationController.view.frame));
    
    
    
    //Steve added
    //sets background color
    if([parentVC isKindOfClass:[AddNewNotesViewCantroller class]] && isSettingNotesHWObject)  //isSettingNotesHWObject is in body of Notes (Not Title)
    {
        [drawImage setBackgroundColorWithRed:255.0f/255.0f green:250.0f/255.0f blue:205.0f/255.0f alpha:1.0f]; // Lemon
    }
    else {
        //[drawImage setBackgroundColorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f]; //Off White
        [drawImage setBackgroundColorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.0f]; //transparent
    }
    
    
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_HandWriting_3"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];
        [helpView addGestureRecognizer:tapHelpView];
        
        helpView.hidden = NO;
        
        /*
         CATransition  *transition = [CATransition animation];
         transition.type = kCATransitionPush;
         transition.subtype = kCATransitionFromTop;
         transition.duration = 1.0;
         [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_HandWriting_3"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTime"]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isFirstTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:NO];
        
    }
    
}


-(void)setLandscapeMode {
    
    //Steve added if & else statement
    //removes animation if coming from ListToDoViewController Class
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([parentVC isKindOfClass:[ListToDoViewController class]] )
    {
        
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait && !isComingFromPenIcon_ListView) //Not coming from Pen Icon (2 finger swipe down)
        {
            //NSLog(@" !isComingFromPenIcon_ListView");
            //pooja-iPad
            if(IS_IPAD)
            {
                //Steve
                self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                self.navigationController.view.frame = CGRectMake(0, 0, 568, 320);
                /*
                 [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                 self.navigationController.view.transform = CGAffineTransformIdentity;
                 oldViewTransform = self.view.transform;
                 self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                 
                 if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                 
                 if (IS_IOS_7) {
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
                 }
                 else{ //iOS 6
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, -20, 768, 1024);
                 }
                 
                 }
                 else{
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
                 }
                 */
                
            }else
            {
                if (IS_IPHONE_5) {
                    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                        }
                        else{ //iOS 6
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, -20, 320, 568);
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                    }
                    
                }
                else{ // Not iPhone 5
                    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                        }
                        else{
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                    }
                    
                }
            }
            
        }
        //Steve added to animate if coming from Pen Icon on List To-do view only
        else if (self.interfaceOrientation == UIInterfaceOrientationPortrait && isComingFromPenIcon_ListView) //isPen do animation
        {
            //NSLog(@" isComingFromPenIcon_ListView");
            //pooja-iPad
            if(IS_IPAD)
            {
                //Steve
                self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                self.navigationController.view.frame = CGRectMake(0, 0, 568, 320);
                /*
                 [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                 self.navigationController.view.transform = CGAffineTransformIdentity;
                 oldViewTransform = self.view.transform;
                 self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                 
                 if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                 
                 if (IS_IOS_7) {
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
                 }
                 else{ //iOS 6
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, -20, 768, 1024);
                 }
                 
                 }
                 else{
                 self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
                 self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
                 }
                 */
                
            }else
            {
                
                if (IS_IPHONE_5) {
                    [UIView beginAnimations:@"View Flip" context:nil];
                    [UIView setAnimationDuration:0.4];
                    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                    
                    // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                        }
                        else{ //iOS 6
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, -20, 320, 568);
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                    }
                    
                    
                    [UIView commitAnimations];
                }
                
                else{ //Not iPhone 5
                    [UIView beginAnimations:@"View Flip" context:nil];
                    [UIView setAnimationDuration:0.4];
                    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                    
                    // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                        }
                        else{ //iOS 6
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, -20, 320, 480);
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                    }
                    
                    
                    [UIView commitAnimations];
                }
            }
            
        }
        
    }
    else //Coming from other classes besides ListViewController class
    {
        //NSLog(@"************ !!!!!!![parentVC isKindOfClass:[ListToDoViewController class]]");
        //pooja-iPad
        if(IS_IPAD)
        {
            //Steve
            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
            self.navigationController.view.frame = CGRectMake(0, 0, 568, 320);
            /*
             [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
             self.navigationController.view.transform = CGAffineTransformIdentity;
             oldViewTransform = self.view.transform;
             self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
             
             if ([sharedDefaults boolForKey:@"NavigationNew"]) {
             
             if (IS_IOS_7) {
             self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
             self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
             }
             else{ //iOS 6
             self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
             self.navigationController.view.frame = CGRectMake(0, -20, 768, 1024);
             }
             
             }
             else{
             self.navigationController.view.bounds = CGRectMake(0, 0, 1024, 768);
             self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
             }
             */
            
        }else
        {
            if (IS_IPHONE_5) {
                [UIView beginAnimations:@"View Flip" context:nil];
                [UIView setAnimationDuration:0.4];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                
                if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
                {
                    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                        }
                        else{ //iOS 6
                            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                            self.navigationController.view.frame = CGRectMake(0, -20, 320, 568); //Steve - offset to adjust for white space on Statusbar
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                    }
                    
                }
                
                [UIView commitAnimations];
            }
            else{ //Not iPhone 5
                [UIView beginAnimations:@"View Flip" context:nil];
                [UIView setAnimationDuration:0.4];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                
                if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
                {
                    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    oldViewTransform = self.view.transform;
                    self.navigationController.view.transform = CGAffineTransformMakeRotation(degreesToRadian(270));
                    
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        
                        if (IS_IOS_7) {
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);                     }
                        else{
                            self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                            self.navigationController.view.frame = CGRectMake(0, -20, 320, 480); //Steve - offset to adjust for white space on StatusBar
                        }
                        
                    }
                    else{
                        self.navigationController.view.bounds = CGRectMake(0, 0, 480, 320);
                        self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                    }
                    
                    
                }
                
                [UIView commitAnimations];
            }
        }
        
    }
    
    
}


-(void)viewDidAppear:(BOOL)animated {
    //NSLog(@"viewDidAppear");
    [super viewDidAppear:YES];
    
    [self addAdditionalControlsInHandWriting];
    
    if (previousColor) {
        previousColor = nil;
    }
    
    //////////////////////////////////////////////////////////////////////
    ////////////////// Steve added default for HW color ///////////////////
    ///////////////////////////////////////////////////////////////////////
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    float redColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorRed"];
    float greenColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorGreen"];
    float blueColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorBlue"];
    
    previousColor  = [UIColor colorWithRed:redColorDefault green:greenColorDefault blue:blueColorDefault alpha:1];
    [drawImage setBrushColorWithRed:redColorDefault green:greenColorDefault blue:blueColorDefault];
    
}

-(void)viewWillDisappear:(BOOL) animated {
    [super viewWillDisappear:YES];
    
    
    //Steve - Do not need this code for iPad.  Also causes StatusBar to re-appear when we don't want it to on a Modal View
    if (IS_IPAD) {
        return;
    }
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    }
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
        //pooja-iPad
        if(IS_IPAD)
        {
            //STeve
            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
            self.navigationController.view.frame = CGRectMake(0, 0, 568, 320);
            /*
             if (IS_IOS_7) {
             self.navigationController.view.transform = CGAffineTransformIdentity;
             self.navigationController.view.transform = oldViewTransform;
             self.navigationController.view.bounds = CGRectMake(0, 0, 768, 1024);
             self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
             }
             */
            
        }
        else
        {
            if (IS_IPHONE_5) {
                //NSLog(@"IS_IPHONE_5");
                
                if (IS_IOS_7) {
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    self.navigationController.view.transform = oldViewTransform;
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 568);
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
                }
                else{ //iOS 6
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    self.navigationController.view.transform = oldViewTransform;
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 548); //Was 568
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 548); //Was 568
                }
                
            }
            else{ //Not iPhone 5
                
                if (IS_IOS_7) {
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    self.navigationController.view.transform = oldViewTransform;
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 480);
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
                }
                else{ //iOS 6
                    self.navigationController.view.transform = CGAffineTransformIdentity;
                    self.navigationController.view.transform = oldViewTransform;
                    self.navigationController.view.bounds = CGRectMake(0, 0, 320, 460);
                    self.navigationController.view.frame = CGRectMake(0, 0, 320, 460);
                }
                
            }
        }
    }
    else{ //Old Nav
        
        //pooja-iPad
        if(IS_IPAD)
        {
            //Steve
            self.navigationController.view.bounds = CGRectMake(0, 0, 568, 320);
            self.navigationController.view.frame = CGRectMake(0, 0, 568, 320);
            /*
             self.navigationController.view.transform = CGAffineTransformIdentity;
             self.navigationController.view.transform = oldViewTransform;
             self.navigationController.view.bounds = CGRectMake(0, 0, 768, 1024);
             self.navigationController.view.frame = CGRectMake(0, 0, 768, 1024);
             */
        }
        else
        {
            if (IS_IPHONE_5) {
                //NSLog(@"IS_IPHONE_5");
                self.navigationController.view.transform = CGAffineTransformIdentity;
                self.navigationController.view.transform = oldViewTransform;
                self.navigationController.view.bounds = CGRectMake(0, 0, 320, 568);
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 568);
            }
            else{
                self.navigationController.view.transform = CGAffineTransformIdentity;
                self.navigationController.view.transform = oldViewTransform;
                self.navigationController.view.bounds = CGRectMake(0, 0, 320, 480);
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 480);
            }
        }
    }
    
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)addAdditionalControlsInHandWriting
{
    //NSLog(@"addAdditionalControlsInHandWriting");
    
    [self.navigationItem setHidesBackButton:YES];
    [GlobalUtility setNavBarTitle:@"" forViewController:self];
    
    UIView *lineChangeView = [[UIView alloc] initWithFrame:CGRectMake(drawImage.frame.size.width - 60, 0, 80, drawImage.frame.size.width)];
    [lineChangeView setBackgroundColor:[UIColor redColor]];
    [lineChangeView setAlpha:0.3];
    
    UILabel *newLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 50, 30)];
    [newLineLabel setText:@"New Line"];
    [newLineLabel setBackgroundColor:[UIColor clearColor]];
    [newLineLabel setFont:[UIFont boldSystemFontOfSize:11]];
    [newLineLabel setTextColor:[UIColor blackColor]];
    [newLineLabel setTextAlignment:NSTextAlignmentCenter];
    [lineChangeView addSubview:newLineLabel];
    [self.view bringSubviewToFront:newLineLabel];
    //	[drawImage addSubview:lineChangeView];
    
    [self.view addSubview:anthVw];
    [anthVw setAlpha:0];
}

#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -

-(IBAction)doneButton_Clicked:(id) sender
{
    self.tabBarController.tabBar.userInteractionEnabled =YES;
    [drawImage removeFromSuperview]; //Steve - removes for iOS7, otherwise HW will flash
    
    [anthVw removeFromSuperview];
    [anthImgVw setImage:[drawImage getImage]];  //Steve TEST
    //[anthImgVw setImage:[drawImage getImage111]];
    
    
    UIImage *imageFull = [anthImgVw.image copy];
    NSData *dataObj1 = UIImageJPEGRepresentation(imageFull, 1.0);
    
    /*
     NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *path = [[searchPath objectAtIndex:0]  stringByAppendingString:[NSString stringWithFormat:@"/%@",@"ToDoTitalBinaryLarge.png"]];
     NSData *imageData = UIImagePNGRepresentation(imageFull);
     
     [imageData writeToFile:path atomically:YES];
     */
    
    UIImage *tempImage = [UIImage imageNamed:@"ToDoTitalBinaryLarge.png"];
    
    NSData *dataObj2 = UIImageJPEGRepresentation(tempImage, 1.0);
    
    CGImageRef testImage;
    
    //pooja-iPad
    if(IS_IPAD)
    {
        //Steve
        //testImage = CGImageCreateWithImageInRect([anthImgVw.image CGImage],CGRectMake(0.0, 0.0, 2550, 500));
        testImage = CGImageCreateWithImageInRect([anthImgVw.image CGImage],CGRectMake(0.0, 0.0, 1600, 280));//1600, 280 //Steve
    }
    else
    {
        if (IS_IPHONE_5) {
            testImage = CGImageCreateWithImageInRect([anthImgVw.image CGImage],CGRectMake(0.0, 0.0, 1600, 280)); //Steve - Was CGRectMake(0.0, 0.0, 1580, 232)
        }
        else
            testImage = CGImageCreateWithImageInRect([anthImgVw.image CGImage],CGRectMake(0.0, 0.0, 1600, 280)); //Steve - CGRectMake(0.0, 0.0, 1580, 232)
    }
    
    
    
    
    if(![dataObj1 isEqualToData:dataObj2])
    {
        
        UIImage* imagetoSave = [UIImage imageWithCGImage:testImage];
        [anthImgVw setImage:imagetoSave];
        imagetoSave =  [imagetoSave resizedImage:CGSizeMake(100, 80) interpolationQuality:1]; //4
        
        
        if ([parentVC respondsToSelector:@selector(appointmentTitle)])
        {
            
            [parentVC setAppointmentBinary:UIImagePNGRepresentation(anthImgVw.image)]; //Steve
            [parentVC setAppointmentBinaryLarge:UIImagePNGRepresentation(imageFull)]; //Steve
            
            [parentVC setAppointmentType:@"H"];
            [parentVC setAppointmentTitle:@""];
            
        }
        else if( [parentVC respondsToSelector:@selector(todoTitle)])
        {
            //Steve changed
            [parentVC setTodoTitleBinary:UIImagePNGRepresentation(anthImgVw.image)]; //Steve
            [parentVC setTodoTitleBinaryLarge:UIImagePNGRepresentation(imageFull)]; //Steve
            
            [parentVC setTodoTitleType:@"H"];
            [parentVC setTodoTitle:@""];
            
            
        }
        else if([parentVC isKindOfClass:[AddNewNotesViewCantroller class]] )
        {
            
            if(!isSettingNotesHWObject)
            {
                // NSLog(@"Add Notes view controller");
                
                //Adding HW to tite
                
                [parentVC setNotesTitleBinary:UIImagePNGRepresentation(anthImgVw.image)]; //Steve
                [parentVC setNotesTitleBinaryLarge:UIImagePNGRepresentation(imageFull)]; //Steve
                
                
                [parentVC setNotesTitle:@""];
                [parentVC setNotesTitleType:@"H"];
            }
            else
            {
                // We are adding handwriting objects to note body
                
                [[self.parentVC tempHWRTArray] addObject:UIImagePNGRepresentation(imageFull)]; //Steve
                
            }
        }
        
        else if([parentVC respondsToSelector:@selector(titleTextField)])
        {
            
            //		if(whatVAL)
            //
            //		{
            ////			[(AddNewNotesViewCantroller*)parentVC setNotesTitle:@""];
            //			[[self.parentVC tempHWRTArray] addObject:UIImageJPEGRepresentation(imageFull, 1.0)];
            //		}
            //		else
            //		{
            //		[parentVC setNotesTitleBinary:UIImageJPEGRepresentation(anthImgVw.image, 1.0)];
            //		[parentVC setNotesTitleBinaryLarge:UIImageJPEGRepresentation(imageFull, 1.0)];
            //		[parentVC setNotesTitle:nil];
            //		[parentVC setNotesTitleType:@"H"];
            //		}
            //
        }
        else if([parentVC respondsToSelector:@selector(todoTitleBinary)])
        {
            
            
            if(toDoBeingEdited  == nil)
            {
                // Save to DB
                //*****************************************
                //Steve added
                //Inserts new Data (user typed into Database
                //******PENDING CODE - Need to add if statement to test if "Done" button was clicked.
                //Also, if black image was clicked
                //*****************************************
                
                TODODataObject *addToDoObj = [[TODODataObject alloc] init];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                // Sets start & End Date on current time and date
                UILabel *startsLabel = [[UILabel alloc] init];
                UILabel *endsLabel = [[UILabel alloc] init];
                NSDate *sourceDate = [NSDate date];
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                [startsLabel setText:[dateFormatter stringFromDate:sourceDate]];
                [endsLabel setText:[dateFormatter stringFromDate:[sourceDate dateByAddingHours:1]]];
                
                [addToDoObj setStartDateTime:startsLabel.text];
                [addToDoObj setDueDateTime:endsLabel.text];
                
                [addToDoObj setCreateDateTime:startsLabel.text];
                
                //////  Added by anil
                NSDateFormatter *newdateFormatter = [[NSDateFormatter alloc] init];
                
                
                NSDate *ModDate  = [NSDate date];
                [newdateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
                [addToDoObj setModifyDateTime:[newdateFormatter stringFromDate:ModDate]];
                
                
                //   [addToDoObj setModifyDateTime:startsLabel.text];
                
                ListToDoViewController *ab = (ListToDoViewController *)self.parentVC;
                
                [addToDoObj setProjID:ab.projectIDFromProjMod];
                [addToDoObj setProjPrntID:ab.projectIDFromProjMod];
                [addToDoObj setToDoProgress: 0.0];
                [addToDoObj setPriority:@""];
                [addToDoObj setIsStarred:NO];
                [addToDoObj setTodoTitleType:@"H"];
                
                [addToDoObj setTodoTitleBinary:UIImageJPEGRepresentation(anthImgVw.image, 1.0)];
                [addToDoObj setTodoTitleBinaryLarge:UIImageJPEGRepresentation(imageFull, 1.0)];
                [addToDoObj setAlert:@"None"];
                [addToDoObj setAlertSecond:@"None"];
                [addToDoObj setIsAlertOn:NO];
                
                //***********************************************************
                //***********************************************************
                //Add new Todo from inputTextField
                //***PENDING*** bring over inputTextField from another method?????
                //[addToDoObj setTodoTitle:inputTextField.text];
                //  [addToDoObj setTodoTitle:@""];
                // NSLog(@"TodoTitle = %@", addToDoObj.todoTitle);
                //***********************************************************
                //***********************************************************
                
                
                //   [todoDateObjArray replaceObjectAtIndex:indexPath.row withObject:addToDoObj];
                //  [AllInsertDataMethods insertToDoDataValues:addToDoObj];
                
                //*****************************************
                //END  - Inserts new Data (user typed into Database
                //*****************************************
                
                
                
            }
            else
            {
                // Update this to do
                
                [toDoBeingEdited setTodoTitleBinary:UIImageJPEGRepresentation(anthImgVw.image, 1.0)];
                [toDoBeingEdited setTodoTitleBinaryLarge:UIImageJPEGRepresentation(imageFull, 1.0)];
                
                BOOL isInserted = [AllInsertDataMethods updateToDoDataValues:toDoBeingEdited];
                NSLog(@"%d",isInserted);
                toDoBeingEdited = nil;
                
            }
            
        }
        else if([parentVC isKindOfClass:[CalendarParentController class]] )
        {
            
            CalendarDataObject *calObj = [GetAllDataObjectsClass getDefaultCalendarDataObject];
            OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
            //NSLog(@"Handwriting view being called from CalenderDailyView");
            
            
            AppointmentsDataObject* appointmentobj = [[AppointmentsDataObject alloc] init];
            
            
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            NSDateFormatter *xyz = [[NSDateFormatter alloc]init];
            [xyz setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [format dateFromString:appDelegate.currentVisibleDate];
            NSString *localDateString1 = [xyz stringFromDate:date];
            
            NSDateFormatter *time = [[NSDateFormatter alloc] init];
            [time setDateFormat:@"HH:mm:ss +0000"];
            
            NSDate *dt = [NSDate date];
            NSTimeZone *destination1 = [NSTimeZone systemTimeZone];
            NSTimeInterval timeZoneOffset1 = [destination1 secondsFromGMTForDate:dt];
            NSDate *localDate1 = [dt dateByAddingTimeInterval:timeZoneOffset1];
            NSString *localTime = [time stringFromDate:localDate1];
            
            NSString *finalDate = [localDateString1 stringByAppendingFormat:[NSString stringWithFormat:@"%@", localTime]];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            //dateFromString ;//= [[NSDate alloc] init];
            NSDate *dateFromString = [dateFormatter dateFromString:finalDate];
            
            //		NSTimeZone *destination = [NSTimeZone systemTimeZone];
            //		NSTimeInterval timeZoneOffset = [destination secondsFromGMTForDate:dateFromString];
            //		NSDate *localDate = [dateFromString dateByAddingTimeInterval:timeZoneOffset];
            //		NSLog(@"local Date %@", localDate);
            [format setDateFormat:@"MMM dd, yyyy hh:mm a"];
            NSString *localDateString = [format stringFromDate:dateFromString];
            
            
            
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
            NSDate *strtDate = [dateFormatter1 dateFromString:localDateString];
            NSDate *endDate = [dateFormatter1 dateFromString:[format stringFromDate:[dateFromString dateByAddingHours:1]]];
            
            
            // NSLog(@"Start ==> %@ End %@",strtDate,endDate);
            
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            NSString* strStrtDate = [dateFormatter1 stringFromDate:strtDate];
            NSString* strEndDate = [dateFormatter1 stringFromDate:endDate];
            
            
            
            
            [appointmentobj setStartDateTime:strStrtDate];
            [appointmentobj setDueDateTime:strEndDate];
            
            [appointmentobj setShortNotes:@""];
            [appointmentobj setCalID:[calObj calendarID]];
            [appointmentobj setTextLocName:@""];
            [appointmentobj setRepeat:@""];
            [appointmentobj setAlert:@""];
            [appointmentobj setAlertSecond:@""];//Anils Added///
            
            
            [appointmentobj setAppointmentTitle:@""];
            [appointmentobj setAppointmentType:@"H"];
            
            [appointmentobj setAppointmentBinary:UIImageJPEGRepresentation(anthImgVw.image, 1.0)];
            [appointmentobj setAppointmentBinaryLarge:UIImageJPEGRepresentation(imageFull, 1.0)];
            
            
            //           [appointmentobj setIsAllDay:NO];
            
            
            BOOL isInserted = [AllInsertDataMethods insertAppointmentDataValues:appointmentobj];
            
            
            //NSLog(@"%d",isInserted);
            
            [self.navigationController popViewControllerAnimated:NO];
            // [self dismissViewControllerAnimated:NO completion:nil];
            //[self.navigationController pushViewController:parentVC animated:NO];
            [appDelegate.navigationController popViewControllerAnimated:YES];
            
            
            return;
        }
        
        
    }
    
    CGImageRelease(testImage);
    
    // NSLog(@"parentVC->%@",[parentVC description]);
    
    
    //Steve
    if (IS_IPAD)
    {
        
        if([parentVC isKindOfClass:[ToDoViewController class]] || [parentVC isKindOfClass:[ToDoParentController class]] ){
            
            [self.handwritingForiPadDelegate hideBluredThings];
            
        }
        else if( [parentVC isKindOfClass:[ListToDoViewController class]] ){
            
            //Steve - Calls method to show HW in AppointmentViewController by calling AppointmentViewController viewWillAppear
            [self.handwritingForiPadDelegate showHandwritingForiPad];

        }else
        {
            [self.handwritingForiPadDelegate showHandwritingForiPad];
        }
       
        
         [self.view removeFromSuperview];
         [self.navigationController popToViewController:parentVC animated:NO];
    }
    
    else{
        
        
        [self.navigationController popToViewController:parentVC animated:NO];
        
        
    }
    
    
}

-(IBAction)backButton_Clicked:(id) sender {
    [self.navigationController popToViewController:parentVC animated:NO];
}

-(IBAction)drawButton_Clicked:(id) sender {
    
    [drawImage eraserOn:NO]; //Steve - tells PaintingView Class if eraser is ON/OFF
    
    //self.penBarButton.tintColor = [UIColor colorWithRed:103.0/255.0f green:150.0/255.0f blue:237.0/255.0f alpha:1.0]; //Steve - changes pen color so it appears selected
    self.penBarButton.tintColor = [UIColor colorWithRed:103.0/255.0f green:150.0/255.0f blue:237.0/255.0f alpha:1.0];//Steve - blue
    //self.penBarButton.tintColor = [UIColor colorWithRed:191.0/255.0f green:178.0/255.0f blue:143.0/255.0f alpha:1.0];//Steve - brown
    self.eraserBarButton.tintColor = [UIColor whiteColor];
    
    const CGFloat* colors = CGColorGetComponents(previousColor.CGColor);
    [drawImage setBrushColorWithRed:colors[0] green:colors[1] blue:colors[2]];
    //[drawImage setBrushColorWithRed:colors[0] green:colors[1] blue:colors[2] alpha:1.0]; //STeve TEST
    
    //Steve Added - only shows colors if ereasr button is not ON.
    if (!isEraserOn) {
        [self colorButton_Clicked:self];
        return;
    }
    
    
    isEraserOn = NO;
    
    /*
     // Steve - reverses isAutoScroll, since it will get changed back on the method playButton_clicked
     if (isAutoScrollOn)
     isAutoScrollOn = NO;
     else
     isAutoScrollOn = YES;
     */
    
    if (isAutoScrollOn){
        navBar.items = _savedNavBarItems; //restores all the navBar items
        
        NSMutableArray     *items = [_savedNavBarItems mutableCopy];
        [items removeObjectAtIndex:1]; //Removes Play Button
        navBar.items = items;
        
        _playButton.userInteractionEnabled = YES;
        _playButton.alpha = 1.0;
        _stopButton.userInteractionEnabled = YES;
        _stopButton.alpha = 1.0;
        
        isAutoScrollOn = YES;
        
        _autoScrollLabel_OFF.hidden = YES;
        _autoScrollLabel_ON.hidden = NO;//Shows AutoScroll ON Label
        
        [drawImage setAutoScrollOn:YES];
    }
    else{
        navBar.items = _savedNavBarItems; //restores all the navBar items
        
        NSMutableArray     *items = [_savedNavBarItems mutableCopy];
        [items removeObjectAtIndex:2]; //Removes Eraser Button
        navBar.items = items;
        
        _playButton.userInteractionEnabled = YES;
        _playButton.alpha = 1.0;
        _stopButton.userInteractionEnabled = YES;
        _stopButton.alpha = 1.0;
        
        isAutoScrollOn = NO;
        
        _autoScrollLabel_OFF.hidden = NO; //Shows AutoScroll OFF Label
        _autoScrollLabel_ON.hidden = YES;
        
        [drawImage setAutoScrollOn:NO];
    }
    
    
    // [self playButton_Clicked:self]; //sets AutoScroll to Previous State
}

-(IBAction)eraseButton_Clicked:(id) sender {
    
    isEraserOn = YES; //Steve
    [drawImage eraserOn:YES]; //Steve - tells PaintingView Class if eraser is ON/OFF
    
    self.penBarButton.tintColor = [UIColor whiteColor];
    self.eraserBarButton.tintColor = [UIColor colorWithRed:191.0/255.0f green:178.0/255.0f blue:143.0/255.0f alpha:1.0]; //changes eraser color so it appears selected
    
    //[drawImage setBrushColorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f]; //Steve commented
    
    
    //[drawImage setBackgroundColorWithRed:255.0f/255.0f green:250.0f/255.0f blue:205.0f/255.0f alpha:1.0f]; // Lemon
    
    //STeve TEST Commented
    
    //Steve added
    //sets the Brush Color depending on the Class it came from
    if([parentVC isKindOfClass:[AddNewNotesViewCantroller class]] && isSettingNotesHWObject)  //isSettingNotesHWObject is in body of Notes (Not Title)
    {
        [drawImage noteClassAndSticky:YES];
        [drawImage setBrushColorWithRed:255.0f/255.0f green:250.0f/255.0f blue:205.0f/255.0f]; // Lemon for Note Sticky
    }
    else
    {
        [drawImage noteClassAndSticky:NO];
        //[drawImage setBrushColorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f]; //Off White
        [drawImage setBrushColorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f]; //transparant
    }
    
    
    [self playButton_Clicked:self]; //changes buttons & turns AutoScroll off.
    
}

-(IBAction)colorButton_Clicked:(id) sender
{
    //NSLog(@"colorButton_Clicked");
    
    [drawImage setUserInteractionEnabled:NO];
    
    UIView *colorPanelView;
    
#pragma mark -- IPad Primotech
    
    //pooja-iPad
    if(IS_IPAD)
    {
        //Steve
        //colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
        colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 568, 320)];
    }
    else
    {
        if (IS_IPHONE_5)
            colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 568, 320)];
        else
            colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 320)];
    }
    
    
    [colorPanelView setBackgroundColor:[UIColor whiteColor]]; //lightGrayColor
    //[colorPanelView setBackgroundColor:[UIColor colorWithRed:244.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0f]];
    [colorPanelView setAlpha:0.0];
    [colorPanelView setTag:1029];
    
    
    int x, y;
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        x = 7 + 44;
        y = 15;
    }
    else
    {
        if (IS_IPHONE_5)
            x = 7 + 44, y = 15;
        else
            x = 7, y = 15;
    }
    
    NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"black1.png", @"charcoal1.png", @"darkblue1.png", @"blue1.png",
                             @"purple1.png", @"pink1.png", @"darkgreen1.png", @"green1.png", @"olive1.png", @"red1.png",
                             @"darkred1.png", @"teal1.png", @"blueviolet1.png", @"saddlebrown1.png", @"indigo1.png", @"dimgray1.png",
                             nil];
    
    for(int i = 0; i < [imgNameArray count]; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(x, y, 50, 50)];
        [btn setBackgroundImage:[UIImage imageNamed:[imgNameArray objectAtIndex:i]] forState:UIControlStateNormal];
        [btn setTag:i];
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [colorPanelView addSubview:btn];
        
        x = x + btn.frame.size.width + 7;
        
#pragma mark -- IPad Primotech
        //pooja-iPad
        if(IS_IPAD)
        {
            if (x > 425 + 44) {
                x = 7 + 44;
                y = 25 + btn.frame.size.height;
            }
        }
        else
        {
            if (IS_IPHONE_5) {
                if (x > 425 + 44) {
                    x = 7 + 44;
                    y = 25 + btn.frame.size.height;
                }
            }
            else{
                if (x > 425) {
                    x = 7;
                    y = 25 + btn.frame.size.height;
                }
            }
        }
    }
    
    if (aView) {
        aView = nil;
    }
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        aView = [[UIView alloc] initWithFrame:CGRectMake(200 + 44, y + 75, 80, 80)];
    }
    else
    {
        if (IS_IPHONE_5) {
            aView = [[UIView alloc] initWithFrame:CGRectMake(200 + 44, y + 75, 80, 80)];
        }
        else
            aView = [[UIView alloc] initWithFrame:CGRectMake(200, y + 75, 80, 80)];
    }
    
    //////////////////////////////////////////////////////////////////////
    ////////////////// Steve added default for HW color ///////////////////
    ///////////////////////////////////////////////////////////////////////
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    float redColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorRed"];
    float greenColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorGreen"];
    float blueColorDefault = [sharedDefaults floatForKey:@"DefaultHandwritingColorBlue"];
    
    [aView setBackgroundColor:[UIColor colorWithRed:redColorDefault green:greenColorDefault blue:blueColorDefault alpha:1.0]];
    
    ///////////////////////////// Steve End /////////////////////////////////
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        redSlider = [[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 80, 150, 23)];
        redSlider.minimumTrackTintColor = [UIColor blueColor];
    }
    else
    {
        if (IS_IPHONE_5)
            redSlider = [[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 80, 150, 23)];
        else
            redSlider = [[UISlider alloc] initWithFrame:CGRectMake(15, y + 80, 150, 23)];
    }
    
    [redSlider setMaximumValue:1.0];
    [redSlider setMinimumValue:0.0];
    [redSlider setValue:redColorDefault]; //Steve added
    [redSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [colorPanelView addSubview:redSlider];
    
#pragma mark -- IPad Primotech
    
    //pooja-iPad
    if(IS_IPAD)
    {
        greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 105, 150, 23)];
        greenSlider.minimumTrackTintColor = [UIColor blueColor];
    }
    else
    {
        
        if (IS_IPHONE_5)
            greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 105, 150, 23)];
        else
            greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 105, 150, 23)];
    }
    
    
    [greenSlider setMaximumValue:1.0];
    [greenSlider setMinimumValue:0.0];
    [greenSlider setValue:greenColorDefault]; //Steve added
    [greenSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [colorPanelView addSubview:greenSlider];
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        blueSlider =[[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 130, 150, 23)];
        blueSlider.minimumTrackTintColor = [UIColor blueColor];
    }
    else
    {
        if (IS_IPHONE_5)
            blueSlider=[[UISlider alloc] initWithFrame:CGRectMake(15 + 44, y + 130, 150, 23)];
        else
            blueSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 130, 150, 23)];
    }
    
    [blueSlider setMaximumValue:1.0];
    [blueSlider setMinimumValue:0.0];
    [blueSlider setValue:blueColorDefault]; //Steve added
    [blueSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [colorPanelView addSubview:blueSlider];
    
    [colorPanelView addSubview:aView];
    
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [okButton setTitle:@"Save" forState:UIControlStateNormal];
    
    [okButton setBackgroundImage:[UIImage imageNamed:@"Done_btn.png"] forState:UIControlStateNormal];
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        [okButton setFrame:CGRectMake(300 + 44, 180, 53, 32)];
    }
    else
    {
        if (IS_IPHONE_5)
            [okButton setFrame:CGRectMake(300 + 44, 180, 53, 32)];
        else
            [okButton setFrame:CGRectMake(300, 180, 53, 32)];
    }
    
    
    //Steve - adds another view to center colorPanelView with new iPad width
    if (IS_IPAD) {
        
        //New view for iPad
        UIView *newViewToCenteriPad = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 320)];
        newViewToCenteriPad.tag = 9999;
        newViewToCenteriPad.alpha = 0.0;
        newViewToCenteriPad.backgroundColor = [UIColor whiteColor];
        
        //Center colorPanelView to newViewToCenteriPad
        CGRect  screeSize = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screeSize.size.width; //Finds screen width
        NSUserDefaults *controller=[NSUserDefaults standardUserDefaults];
        
        NSLog(@"%@",[controller valueForKey:@"Viewcontroller"]);
        
        if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Calender"] || [[controller valueForKey:@"Viewcontroller"] isEqualToString:@"TODO"] )
        {
            colorPanelView.frame = CGRectMake(screenWidth/2 - colorPanelView.frame.size.width/2, 0, colorPanelView.frame.size.width, colorPanelView.frame.size.height);
        }else
        {
            
            
            colorPanelView.frame = CGRectMake(screenWidth/8 - colorPanelView.frame.size.width/4, 0, colorPanelView.frame.size.width, colorPanelView.frame.size.height);
            
            
        }
        
        
        [okButton addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
        [colorPanelView addSubview:okButton];
        [newViewToCenteriPad addSubview:colorPanelView];
        [self.view addSubview:newViewToCenteriPad];
        
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [colorPanelView setAlpha:1.0];
        [newViewToCenteriPad setAlpha:1.0];
        [UIView commitAnimations];
    }
    else{ //iPhone
        
        [okButton addTarget:self action:@selector(colorChangeSaved:) forControlEvents:UIControlEventTouchUpInside];
        [colorPanelView addSubview:okButton];
        [self.view addSubview:colorPanelView];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [colorPanelView setAlpha:1.0];
        [UIView commitAnimations];
    }
}


-(IBAction)clearButton_Clicked:(id) sender {
    //[drawImage erase]; //Steve commented
    
    /*
     //Steve added
     //erases image & makes sure it's the proper background color
     if([parentVC isKindOfClass:[AddNewNotesViewCantroller class]] && isSettingNotesHWObject)  //isSettingNotesHWObject is in body of Notes (Not Title)
     {
     [drawImage setBackgroundColorWithRed:255.0f/255.0f green:250.0f/255.0f blue:205.0f/255.0f]; // Lemon
     }
     else {
     [drawImage setBackgroundColorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f]; //Off White
     }
     
     */
    
    //sets the Brush Color depending on the Class it came from
    if([parentVC isKindOfClass:[AddNewNotesViewCantroller class]] && isSettingNotesHWObject)  //isSettingNotesHWObject is in body of Notes (Not Title)
    {
        [drawImage noteClassAndSticky:YES];
        [drawImage setBackgroundColorWithRed:255.0/255.0f green:250.0/255.0f blue:205.0/255.0f alpha:1.0f]; //Lemon
    }
    else
    {
        [drawImage noteClassAndSticky:NO];
        //[drawImage setBrushColorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f]; //Off White
        [drawImage setBackgroundColorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:0.0f];//transparant
    }
    
    
    //Steve changed Y to 44
    [drawImage setFrame:CGRectMake(0, 44, drawImage.frame.size.width, drawImage.frame.size.height)];
    
    // NSLog(@" drawImage.frame.origin.x = %f", drawImage.frame.origin.x);
    // NSLog(@" drawImage.frame.origin.y = %f", drawImage.frame.origin.y );
    
    
    ///////////// Steve added ///////////////////
    // resets all the buttons
    posIndLabel.text = @"Scroll fwd";
    
    lineNumber = 1;
    self.lineCount.text = [NSString stringWithFormat:@"Line %d", lineNumber];
    [previousLineButton setEnabled:NO];
    [scrollLeftButton setEnabled:NO];
    
    [scrollRightButtonSideBar setHidden:NO];
    [scrollRightButtonSideBar setEnabled:YES];
    
    [nextLineButtonSideBar setEnabled:YES];
    [nextLineButtonSideBar setHidden:YES];
    
    [scrollRightButton setEnabled: YES];
    [nextLineButton setEnabled:YES];
    
    
    ///////////// Steve added End ///////////////////
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.3];
    [anthVw setAlpha:0];
    [UIView commitAnimations];
}

-(IBAction)cancelPreviewButton_Clicked:(id)sender {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7]; //1.3
    [anthVw setAlpha:0];
    [UIView commitAnimations];
    
    if (IS_IOS_7) {
        _previewLabel_iOS7.hidden = YES;
    }
}

- (IBAction)helpView_Tapped:(id)sender {
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}


-(IBAction)previewButton_Clicked:(id) sender {
    
    if (IS_IOS_7) {
        
        //anthVw.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
        anthImgVw.backgroundColor = [UIColor whiteColor];
        _previewLabel_iOS7.hidden = NO;
        _previewLabel_iOS7.textColor = [UIColor whiteColor];
        previewNavBar.items = nil; //removes the "Preview" title in black that's for iOS 6
        
        
#pragma mark -- IPad Primotech
        //pooja-iPad
        
        //Ste
        CGRect screenSize = [[UIScreen mainScreen]bounds];
        CGFloat width = screenSize.size.width;
        CGFloat height = screenSize.size.height;
        
        NSUserDefaults *controller=[NSUserDefaults standardUserDefaults];
        
        if(IS_IPAD)
        {
            if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"ToDo"])
            {
                anthImgVw.frame = CGRectMake(width/4 - anthImgVw.frame.size.width/4, (320 + 44)/4 - anthImgVw.frame.size.height/4, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
                _previewLabel_iOS7.frame = CGRectMake(width/4 - 100/4, 7, 100, 35);
            }
            else if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"List"])
            {
                anthImgVw.frame = CGRectMake(width/4 - anthImgVw.frame.size.width/4-15, (320 + 44)/4 - anthImgVw.frame.size.height/4+15, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
                
                _previewLabel_iOS7.frame = CGRectMake(width/4 - 100/4, 7, 100, 35);
                
                NSLog(@"Frame is:  %f --  %f --   %f -- %f", _lineView.frame.origin.x,_lineView.frame.origin.y,_lineView.frame.size.width,_lineView.frame.size.height);
            }
            else if ([[controller valueForKey:@"Viewcontroller"] isEqualToString:@"Projects"])
            {
                anthVw.frame = CGRectMake(anthVw.frame.origin.x,anthVw.frame.origin.y,anthVw.frame.size.width,anthVw.frame.size.height+20);
                
                
                //  anthImgVw.frame = CGRectMake(width/4 - anthImgVw.frame.size.width/4, (320 + 44)/4 - anthImgVw.frame.size.height/4, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
                _previewLabel_iOS7.frame = CGRectMake(width/4 - 100/4, 7, 100, 35);
            }
            
            else
            {
                
                anthImgVw.frame = CGRectMake(width/2 - anthImgVw.frame.size.width/2, (320 + 44)/2 - anthImgVw.frame.size.height/2, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
                _previewLabel_iOS7.frame = CGRectMake(width/2 - 100/2, 7, 100, 35);
            }
        }
        else
        {
            
            if (!IS_IPHONE_5) {
                _previewLabel_iOS7.frame = CGRectMake(height/2 - 78/2, 7, 78, 29);
                anthImgVw.frame = CGRectMake(height/2 - anthImgVw.frame.size.width/2, anthImgVw.frame.origin.y, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
            }
            else{ //Steve updated
                _previewLabel_iOS7.frame = CGRectMake(height/2 - 78/2, 7, 78, 29);
                anthImgVw.frame = CGRectMake(height/2 - anthImgVw.frame.size.width/2, anthImgVw.frame.origin.y, anthImgVw.frame.size.width, anthImgVw.frame.size.height);
            }
            
        }
        
        
        
        previewCancelButton.hidden = YES;
        previewSaveButton.hidden = YES;
        previewClearButton.hidden = YES;
        
        _previewCancelButton_iOS7.hidden = NO;
        _previewDoneButton_iOS7.hidden = YES;
        _previewClearButton_iOS7.hidden = NO;
        
        [_previewClearButton_iOS7 setTintColor:[UIColor whiteColor]];
        _previewDoneButton_iOS7.tintColor=[UIColor whiteColor];
        _previewCancelButton_iOS7.tintColor=[UIColor whiteColor];
        
#pragma mark -- IPad Primotech
        //pooja-iPad
        if(IS_IPAD)
        {
            //Steve changed
            _previewDoneButton_iOS7.frame = CGRectMake(_doneButton_iOS7.frame.origin.x, 5, 60, 40);
            _previewClearButton_iOS7.frame = CGRectMake(_doneButton_iOS7.frame.origin.x - _previewClearButton_iOS7.frame.size.width - 15, 5, 60, 40);
            
            [previewNavBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            previewNavBar.barTintColor = [UIColor colorWithRed:0.2980 green:0.2980 blue:0.2980 alpha:1.0];
        }
        else
        {
            _previewDoneButton_iOS7.frame = CGRectMake(_doneButton_iOS7.frame.origin.x, _doneButton_iOS7.frame.origin.y, 46, 30);
            _previewClearButton_iOS7.frame = CGRectMake(_doneButton_iOS7.frame.origin.x - _previewClearButton_iOS7.frame.size.width - 15, 7, 46, 30);
            previewNavBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
        }
        
        
        //previewNavBar.tintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
        previewNavBar.translucent = NO;
        
        //[anthVw addSubview:_doneButton_iOS7];
    }
    
    [anthImgVw setImage:[drawImage getImage]];
    
    [anthVw addSubview:anthImgVw];
    
    
    [self.view bringSubviewToFront:anthVw];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7]; //1.3
    
    [anthVw setAlpha:1];
    
    [UIView commitAnimations];
}

-(IBAction)nextLineButton_Clicked:(id) sender {
    
    //	adjust Handwriting, like increasing the Handwriting range
    //NSLog(@" drawImage.frame.origin.x = %f", drawImage.frame.origin.x);
    //NSLog(@" drawImage.frame.origin.y = %f", drawImage.frame.origin.y);
    
    posIndLabel.text = @"Scroll fwd";
    endLimitView.hidden = YES;
    
    [scrollRightButtonSideBar setHidden:NO];
    [nextLineButtonSideBar setHidden:YES];
    [previousLineButton setEnabled:YES];
    
    //-456
    if (drawImage.frame.origin.y != -516)  /* -456 (Shows if Not on Line 3)*/{
        lineNumber++;
        self.lineCount.text = [NSString stringWithFormat:@"Line %d", lineNumber];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2]; //0.5                    //-250
        [drawImage setFrame:CGRectMake(0, drawImage.frame.origin.y - 280, drawImage.frame.size.width, drawImage.frame.size.height)];
        /* -250 (Changes the scroll in "y" direction)*/
        [UIView commitAnimations];
        
        //-456
        if (drawImage.frame.origin.y == -516) /* -456 (Shows if on Line 3) */{
            [nextLineButton setEnabled:NO];
            [nextLineButtonSideBar setEnabled:NO];
        }
        [scrollLeftButton setEnabled:NO];
        [scrollRightButton setEnabled:YES];
        [scrollRightButtonSideBar setEnabled:YES];
    }else {
        [nextLineButton setEnabled:NO];
        [nextLineButtonSideBar setEnabled:NO];
    }
}

-(IBAction)previousLineButton_Clicked:(id) sender {
    
    posIndLabel.text = @"Scroll fwd";
    endLimitView.hidden = YES;
    
    [nextLineButtonSideBar setHidden:YES]; //Steve added
    [scrollRightButtonSideBar setHidden:NO];
    [nextLineButton setEnabled:YES];
    [nextLineButtonSideBar setEnabled:YES];
    if (drawImage.frame.origin.y != 44) {
        lineNumber--;
        self.lineCount.text = [NSString stringWithFormat:@"Line %d", lineNumber];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2]; //0.5                    //250
        [drawImage setFrame:CGRectMake(0, drawImage.frame.origin.y + 280, drawImage.frame.size.width, drawImage.frame.size.height)];
        /* 250 (Changes the scroll in "y" direction) */
        [UIView commitAnimations];
        if (drawImage.frame.origin.y == 44) {
            [previousLineButton setEnabled:NO];
        }
        [scrollLeftButton setEnabled:NO];
        [scrollRightButton setEnabled:YES];
        [scrollRightButtonSideBar setEnabled:YES];
    }else {
        [previousLineButton setEnabled:NO];
    }
}

-(IBAction)scrollLeftButton_Clicked:(id) sender {
    
    posIndLabel.text = @"Scroll fwd";
    endLimitView.hidden = YES;
    
    [nextLineButtonSideBar setHidden:YES]; //Steve added
    [scrollRightButtonSideBar setHidden:NO];
    [scrollRightButton setEnabled:YES];
    [scrollRightButtonSideBar setEnabled:YES];
    if (drawImage.frame.origin.x == 0) {
        [scrollLeftButton setEnabled:NO];
        return;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2]; //0.5
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        //Steve
        //[drawImage setFrame:CGRectMake(drawImage.frame.origin.x + 850, drawImage.frame.origin.y, drawImage.frame.size.width,drawImage.frame.size.height)];
        [drawImage setFrame:CGRectMake(drawImage.frame.origin.x + 698, drawImage.frame.origin.y, drawImage.frame.size.width,drawImage.frame.size.height)];
    }
    else
    {
        if (IS_IPHONE_5)                                              //508
            [drawImage setFrame:CGRectMake(drawImage.frame.origin.x + 498, drawImage.frame.origin.y, drawImage.frame.size.width,drawImage.frame.size.height)];
        else                                                           //360
            [drawImage setFrame:CGRectMake(drawImage.frame.origin.x + 370, drawImage.frame.origin.y, drawImage.frame.size.width,drawImage.frame.size.height)];
    }
    /* 360 (Changes the scroll in "x" direction)*/
    
    
    [UIView commitAnimations];
    
    if (drawImage.frame.origin.x == 0) {
        [scrollLeftButton setEnabled:NO];
    }
}

-(IBAction)scrollRightButton_Clicked:(id) sender {
    //NSLog(@"scrollRightButton_Clicked");
    
    
    
    endLimitView.hidden = YES;
    [scrollLeftButton setEnabled:YES];
    
    //Steve - sets scroll amount
    int     scrollAmount;
    
#pragma mark -- IPad Primotech
    //pooja-iPad
    if(IS_IPAD)
    {
        //scrollAmount = 850;
        scrollAmount = 698;//Steve
    }
    else
    {
        if (IS_IPHONE_5)        scrollAmount = 498;
        else if (IS_IOS_5)      scrollAmount = 370;
        else                    scrollAmount = 370; //Not iPhone 5, but iOS 6 and greater
    }
    
    //NSLog(@"drawImage.frame.origin.x = %f", drawImage.frame.origin.x);
    //NSLog(@"drawImage.frame.origin.y = %f", drawImage.frame.origin.y);
    //NSLog(@"scrollAmount = %d", scrollAmount);
    
    
    if (IS_IPAD && drawImage.frame.origin.x == -scrollAmount) // 2 Scrolls)
    {
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
        return;
    }
    else if (!IS_IOS_5 && drawImage.frame.origin.x == -1110) // 4 Scrolls
    {
        //NSLog(@"HERE #1");
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
        return;
    }
    else if (IS_IOS_5 && drawImage.frame.origin.x == -(2 * scrollAmount)) //3 scrolls
    {
        //NSLog(@"HERE #1");
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
        return;
    }
    
    
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2]; //0.5
    
    [drawImage setFrame:CGRectMake(drawImage.frame.origin.x - scrollAmount, drawImage.frame.origin.y, drawImage.frame.size.width,drawImage.frame.size.height)];
    
    
    [UIView commitAnimations];
    
    if (IS_IPAD && drawImage.frame.origin.x == -scrollAmount) // 2 Scrolls)
    {
        posIndLabel.text = @"New Line";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:NO];
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
    }
    if (IS_IOS_5 && drawImage.frame.origin.x == -(2 * scrollAmount)) { //iOS 5 scrolls 3 times
        //NSLog(@"HERE #2");
        posIndLabel.text = @"New Line";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:NO];
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
    }
    //-720
    else if (!IS_IOS_5 && drawImage.frame.origin.x == -(3 * scrollAmount)) { //iOS 6 scrolls 4 times
        //NSLog(@"HERE #2");
        posIndLabel.text = @"New Line";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:NO];
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
    }
    else if (IS_IPHONE_5 && drawImage.frame.origin.x == -(2 * scrollAmount)){ //iPhone 5 scrolls 3 times
        //NSLog(@"HERE #3");
        posIndLabel.text = @"New Line";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:NO];
        [scrollRightButton setEnabled:NO];
        [scrollRightButtonSideBar setEnabled:NO];
    }
    //Scrolls 4 times - Not iPhone5 and is iOS 6 or greater
    if ((!IS_IOS_5 && drawImage.frame.origin.x == -(3 * scrollAmount)) && (drawImage.frame.origin.y == -516)){
        //NSLog(@"HERE #4");
        posIndLabel.text = @"End Input";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:YES];
        
        endLimitView = [[UIView alloc]initWithFrame:CGRectMake(66, 0, 100, 440)];
        endLimitView.backgroundColor = [UIColor redColor];
        endLimitView.alpha = 0.5;
        [self.scrollZoneView addSubview:endLimitView];
        [self.scrollZoneView bringSubviewToFront:posIndLabel];
    }
    //iPad & scrolls 2 times & y position ==-516
    else if (IS_IPAD && drawImage.frame.origin.x == -scrollAmount && drawImage.frame.origin.y == -516)
    {
        posIndLabel.text = @"End Input";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:YES];
        
        endLimitView = [[UIView alloc]initWithFrame:CGRectMake(66, 0, 100, 440)];
        endLimitView.backgroundColor = [UIColor redColor];
        endLimitView.alpha = 0.5;
        [self.scrollZoneView addSubview:endLimitView];
        [self.scrollZoneView bringSubviewToFront:posIndLabel];
    }
    //iPhone 5  or iOS 5 scrolls 3 times
    else if (( (IS_IOS_5 || IS_IPHONE_5) && drawImage.frame.origin.x == -(2 * scrollAmount)) && (drawImage.frame.origin.y == -516)){
        //NSLog(@"HERE #5");
        posIndLabel.text = @"End Input";
        [scrollRightButtonSideBar setHidden:YES];
        [nextLineButtonSideBar setHidden:YES];
        
        endLimitView = [[UIView alloc]initWithFrame:CGRectMake(66, 0, 100, 440)];
        endLimitView.backgroundColor = [UIColor redColor];
        endLimitView.alpha = 0.5;
        [self.scrollZoneView addSubview:endLimitView];
        [self.scrollZoneView bringSubviewToFront:posIndLabel];
    }
}


//Steve - adds a play button to turn Autoscroll ON/OFF
- (IBAction)playButton_Clicked:(id)sender {
    
    
    //Switches ON/OFF
    if (isEraserOn) {  //turns off autoScroll if eraser is on.
        
        _playButton.userInteractionEnabled = NO;
        _playButton.alpha = 0.25;
        _stopButton.userInteractionEnabled = NO;
        _stopButton.alpha = 0.25;
        
        _autoScrollLabel_OFF.hidden = NO; //Shows AutoScroll OFF Label
        _autoScrollLabel_ON.hidden = YES;
        
        //if (isAutoScrollOn)
        //     [self animateAutoScrollViewFadeIn];
        
        
        [drawImage setAutoScrollOn:NO];
        
    }else if (isAutoScrollOn) {
        
        navBar.items = _savedNavBarItems; //restores all the navBar items
        
        NSMutableArray     *items = [_savedNavBarItems mutableCopy];
        [items removeObjectAtIndex:2]; //Removes Eraser Button
        navBar.items = items;
        
        _playButton.userInteractionEnabled = YES;
        _playButton.alpha = 1.0;
        _stopButton.userInteractionEnabled = YES;
        _stopButton.alpha = 1.0;
        
        isAutoScrollOn = NO;
        
        _autoScrollLabel_OFF.hidden = NO; //Shows AutoScroll OFF Label
        _autoScrollLabel_ON.hidden = YES;
        
        [self animateAutoScrollViewFadeIn];
        [drawImage setAutoScrollOn:NO];
        
    }else{
        
        navBar.items = _savedNavBarItems; //restores all the navBar items
        
        NSMutableArray     *items = [_savedNavBarItems mutableCopy];
        [items removeObjectAtIndex:1]; //Removes Play Button
        navBar.items = items;
        
        _playButton.userInteractionEnabled = YES;
        _playButton.alpha = 1.0;
        _stopButton.userInteractionEnabled = YES;
        _stopButton.alpha = 1.0;
        
        isAutoScrollOn = YES;
        
        _autoScrollLabel_OFF.hidden = YES;
        _autoScrollLabel_ON.hidden = NO;//Shows AutoScroll ON Label
        
        [self animateAutoScrollViewFadeIn];
        [drawImage setAutoScrollOn:YES];
    }
    
}


//Steve - animates a message showing AutoScroll Status (On/Off)
-(void) animateAutoScrollViewFadeIn{
    
    // [_autoScrollMessageView bringSubviewToFront:self.view];
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [_autoScrollMessageView.layer addAnimation:transition forKey:kCATransitionFade];
    [self.view addSubview:_autoScrollMessageView];
    
    _autoScrollMessageView.hidden = NO;
    
    [self performSelector:@selector(animateAutoScrollViewFadeOut) withObject:nil afterDelay:1.0];
    
}

-(void) animateAutoScrollViewFadeOut{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [_autoScrollMessageView.layer addAnimation:transition forKey:kCATransitionFade];
    
    _autoScrollMessageView.hidden = YES;
}


#pragma mark -
#pragma mark Class Methods
#pragma mark -

- (UIColor *)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy {
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,bitsPerComponent, bytesPerRow, colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
    byteIndex += 4;
    
    if (previousColor) {
        previousColor = nil;
    }
    
    previousColor  = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
    
    [drawImage setBrushColorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0];
    
    [redSlider setValue:(rawData[byteIndex] * 1.0) / 255.0 animated:YES];
    [greenSlider setValue:(rawData[byteIndex + 1] * 1.0) / 255.0 animated:YES];
    [blueSlider setValue:(rawData[byteIndex + 2] * 1.0) / 255.0 animated:YES];
    
    UIColor *acolor = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
    
    [aView setBackgroundColor:acolor];
    
    free(rawData);
    return acolor;
}

-(void)colorChangeSaved: (id) sender {
    
    ///////////////////////////////////////////////////////////////
    /////////////// Steve added to save default color ////////////////
    ////////////////////////////////////////////////////////////////
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setFloat:redSlider.value forKey:@"DefaultHandwritingColorRed"];
    [sharedDefaults setFloat:greenSlider.value forKey:@"DefaultHandwritingColorGreen"];
    [sharedDefaults setFloat:blueSlider.value forKey:@"DefaultHandwritingColorBlue"];
    
    //////////////////////// Steve end /////////////////////////////////
    
    if (IS_IPAD) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [[self.view viewWithTag:9999] setAlpha:0.0]; //Steve - removes "newViewToCenteriPad"
        [UIView commitAnimations];
    }
    else{ //iPhone
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [[self.view viewWithTag:1029] setAlpha:0.0];
        [UIView commitAnimations];
    }
    
    
    
    [drawImage setUserInteractionEnabled:YES];
    
    //Steve added - gives time for animation to display. Otherwise it removes view too quickly, so it doesn't appear animated
    [self performSelector:@selector(removeFromSuperviewWithDelay) withObject:nil afterDelay:0.5];
}

- (void)removeFromSuperviewWithDelay{
    
    //Steve
    if (IS_IPAD) {
        
        [[self.view viewWithTag:9999] removeFromSuperview];
    }
    else{ //iPhone
        
        [[self.view viewWithTag:1029] removeFromSuperview];
    }
    
}

-(void)valueChanged:(id)sender{
    //NSLog(@"valueChanged");
    
    if (previousColor) {
        previousColor = nil;
    }
    
    previousColor = [UIColor colorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value alpha:1.0];
    [drawImage setBrushColorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value];
    [aView setBackgroundColor: previousColor];
    
}

-(void)btnClicked:(id)sender {
    UIImage *image = [sender backgroundImageForState:UIControlStateNormal];
    [self getRGBAsFromImage:image atX:10 andY:10];
}


#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    navBar = nil;
    bottomBar = nil;
    previewNavBar = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    
    previewClearButton = nil;
    previewSaveButton = nil;
    helpHandwriteImage = nil;
    helpHandwriteImage_iPhone5 = nil;
    backButton = nil;
    doneButton = nil;
    scrollZoneView = nil;
    [self setLineView:nil];
    helpHandwriteImage_iOS5 = nil;
    [self setPlayButton:nil];
    _playButton = nil;
    _stopButton = nil;
    savedNavBarItems = nil;
    [self setStopButton:nil];
    self.savedNavBarItems = nil;
    [self setAutoScrollMessageView:nil];
    [self setAutoScrollLabel_ON:nil];
    [self setAutoScrollLabel_OFF:nil];
    [self setPenBarButton:nil];
    [self setEraserBarButton:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    
    if (redSlider) {
        redSlider = nil;
    }
    
    if (greenSlider) {
        greenSlider = nil;
    }
    if (blueSlider) {
        blueSlider = nil;
    }
    if (aView) {
        aView = nil;
    }
    
}


@end

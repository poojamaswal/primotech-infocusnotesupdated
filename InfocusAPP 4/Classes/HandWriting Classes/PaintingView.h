
#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

//CONSTANTS:
// (Steve)
#define kBrushOpacity	 (1.0 / 1.0)
#define kBrushPixelStep	 0.10
#define kBrushScale	 3.5
#define kLuminosity	 1.0
#define kSaturation	 8.0



//#define kBrushOpacity		(1.0 / 1.0)
//#define kBrushPixelStep		1
//#define kBrushScale			3
//#define kLuminosity			0.75
//#define kSaturation			1.0

//CLASS INTERFACES:

@protocol ScrollDelegate <NSObject> //Steve

-(IBAction)scrollRightButton_Clicked:(id) sender;
-(IBAction)nextLineButton_Clicked:(id) sender;

@end



@interface PaintingView : UIView {
@private
	// The pixel dimensions of the backbuffer
	GLint backingWidth;
	GLint backingHeight;
	
	EAGLContext *context;
	
	// OpenGL names for the renderbuffer and framebuffers used to render to this view
	GLuint viewRenderbuffer, viewFramebuffer;
	
	// OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist)
	GLuint depthRenderbuffer;
	
	GLuint	brushTexture;
	CGPoint	location;
	CGPoint	previousLocation;
	Boolean	firstTouch;
	Boolean needsErase;	
    
    BOOL isTouchAgain;
    CGFloat verticleDrowLimit;
    CGFloat horizontalDrawLimit;
    BOOL mouseSwiped;
    
    BOOL isReadyToScroll; //Steve
    BOOL isAutoScroll; //Steve - turns on/off AutoScroll
    BOOL isEraserOn; //Steve
    BOOL isNoteClassSticky; //Steve
    id   parentVC; //Steve

}

@property(nonatomic, readwrite) CGPoint location;
@property(nonatomic, readwrite) CGPoint previousLocation;
@property(nonatomic, weak) id<ScrollDelegate> scrollDelegate; //Steve  protocol
@property(nonatomic, strong)id parentVC; //Steve


//- (void)erase;  //Steve commented
- (UIImage *)getImage;
- (UIImage *)getImage111; //Steve TEST

- (void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

//Steve added
//STeve TEST - added alpha
-(void) setBackgroundColorWithRed: (CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
-(void) setAutoScrollOn: (BOOL) isAutoScrollOn; //Steve - tells this class if Erase button is ON and therefore shutoff autoscroll
-(void) eraserOn: (BOOL)isEraserOnOrOff; //Steve added
-(void) noteClassAndSticky:(BOOL) isNoteClassAndStickBothTrue; //Steve

@end










#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#import "PaintingView.h"
//#import "AddNewNotesViewCantroller.h" //Steve

//CLASS IMPLEMENTATIONS:

// A class extension to declare private methods
@interface PaintingView (private)

- (BOOL)createFramebuffer;
- (void)destroyFramebuffer;
- (void)scroll_screen;



@end

@implementation PaintingView

@synthesize  location;
@synthesize  previousLocation;

// Implement this to override the default layer class (which is [CALayer class]).
// We do this so that our view will be backed by a layer that is capable of OpenGL ES rendering.
+ (Class) layerClass 
{
    return [CAEAGLLayer class];
}

// The GL view is stored in the nib file. When it's unarchived it's sent -initWithCoder:
- (id)initWithCoder:(NSCoder*)coder {
    NSLog(@"initWithCoder");
    
    isAutoScroll = YES;
    
	CGImageRef		brushImage;
	CGContextRef	brushContext;
	GLubyte			*brushData;
	size_t			width, height;
    
    if ((self = [super initWithCoder:coder])) {
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
		
		//Steve TEST - Transparent background
        //eaglLayer.opaque = YES;
        eaglLayer.opaque = NO;

        //***** Steve TEST 
		// In this application, we want to retain the EAGLDrawable contents after a call to presentRenderbuffer.
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:YES], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        //eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
		
		context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
		
		if (!context || ![EAGLContext setCurrentContext:context]) {
			return nil;
		}
		
		// Create a texture from an image
		// First create a UIImage object from the data in a image file, and then extract the Core Graphics image
		brushImage = [UIImage imageNamed:@"Particle.png"].CGImage;
		
		// Get the width and height of the image
		width = CGImageGetWidth(brushImage);
		height = CGImageGetHeight(brushImage);
		
		// Texture dimensions must be a power of 2. If you write an application that allows users to supply an image,
		// you'll want to add code that checks the dimensions and takes appropriate action if they are not a power of 2.
		
		// Make sure the image exists
		if(brushImage) {
            //NSLog(@"butdhImage exists");
			// Allocate  memory needed for the bitmap context
			brushData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
			// Use  the bitmatp creation function provided by the Core Graphics framework. 
			brushContext = CGBitmapContextCreate(brushData, width, height, 8, width * 4, CGImageGetColorSpace(brushImage), kCGImageAlphaPremultipliedLast);
			// After you create the context, you can draw the  image to the context.
			CGContextDrawImage(brushContext, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), brushImage);
			// You don't need the context at this point, so you need to release it to avoid memory leaks.
			CGContextRelease(brushContext);
			// Use OpenGL ES to generate a name for the texture.
			glGenTextures(1, &brushTexture);
			// Bind the texture name. 
			glBindTexture(GL_TEXTURE_2D, brushTexture);
			// Set the texture parameters to use a minifying filter and a linear filer (weighted average)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			// Specify a 2D texture image, providing the a pointer to the image data in memory
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, brushData);
			// Release  the image data; it's no longer needed
            free(brushData);
			
			// Enable use of the texture
			glEnable(GL_TEXTURE_2D);
			// Set a blending function to use
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			// Enable blending
			glEnable(GL_BLEND);
		}
		
		// Set the view's scale factor
		self.contentScaleFactor = 1.0;
        
		// Setup OpenGL states
		glMatrixMode(GL_PROJECTION);
		CGRect frame = self.bounds;
		CGFloat scale = self.contentScaleFactor;
		// Setup the view port in Pixels
		glOrthof(0, frame.size.width * scale, 0, frame.size.height * scale, -1, 1);
		glViewport(0, 0, frame.size.width * scale, frame.size.height * scale);
		glMatrixMode(GL_MODELVIEW);
		
		glDisable(GL_DITHER);
		glEnable(GL_TEXTURE_2D);
		glEnableClientState(GL_VERTEX_ARRAY);
		
	    glEnable(GL_BLEND);
		// Set a blending function appropriate for premultiplied alpha pixel data
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        //glBlendFunci(GL_ONE, GL_ONE_MINUS_SRC_ALPHA,GL_ONE_MINUS_DST_ALPHA); //Steve TEST - bug
        
        //Steve TEST
        //glBlendEquation(GL_FUNC_ADD); //Steve TEST - Add - ADDITIVE BLENDING (default)
        //glBlendEquation(GL_FUNC_REVERSE_SUBTRACT); //erase SUBTRACTIVE BLENDING
        //glBlendEquation(GL_FUNC_REVERSE_SUBTRACT_OES); //Steve - for iOS 8
        glBlendEquationOES(GL_FUNC_REVERSE_SUBTRACT_OES); //Steve - for iOS 8 & arm64
        
		
		glEnable(GL_POINT_SPRITE_OES);
		glTexEnvf(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
		glPointSize(width / kBrushScale);
        //		glPointSize(10.0);
		
		// Make sure to start with a cleared buffer
		needsErase = YES;
		
		// Playback recorded path, which is "Shake Me"
        //		recordedPaths = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Recording" ofType:@"data"]];
        //		if([recordedPaths count])
        //			[self performSelector:@selector(playback:) withObject:recordedPaths afterDelay:0.2];
	}
    //	UIImageView *maskLayer=[[UIImageView alloc] initWithFrame:CGRectMake(0,00,480, 320)];
    //	maskLayer.image =[UIImage imageNamed:@"tracingscreen-a.png"];
    //	[self addSubview:maskLayer];
	
	return self;
}

// If our view is resized, we'll be asked to layout subviews.
// This is the perfect opportunity to also update the framebuffer so that it is
// the same size as our display area.
//Steve commented out - using setBackgroundColorWithRed method to clear buffer & set background color
// If this method is here, it gets called after HW methods & therefore overrides the color changes made
/*
-(void)layoutSubviews {
    NSLog(@"layoutSubviews");                   
	[EAGLContext setCurrentContext:context];     //Moved to setBackgroundColorWithRed
	[self destroyFramebuffer];                   //Moved to setBackgroundColorWithRed
	[self createFramebuffer];                    //Moved to setBackgroundColorWithRed
	
	// Clear the framebuffer the first time it is allocated
	if (needsErase) {
		[self erase]; //Steve removed to allow for custom colors set in Handwriting Class
        //[self setBackgroundColorWithRed:0.0 green:255.0 blue:0.0];
    
		needsErase = NO;
	}
}
 */

-(void) setNeedsLayout
{
    //NSLog(@"setNeedsLayout");
}

- (BOOL)createFramebuffer {
	// Generate IDs for a framebuffer object and a color renderbuffer
	glGenFramebuffersOES(1, &viewFramebuffer);
	glGenRenderbuffersOES(1, &viewRenderbuffer);
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
	// This call associates the storage for the current render buffer with the EAGLDrawable (our CAEAGLLayer)
	// allowing us to draw into a buffer that will later be rendered to screen wherever the layer is (which corresponds with our view).
	[context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)self.layer];
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer);
	
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
	
	// For this sample, we also need a depth buffer, so we'll create and attach one via another renderbuffer.
	glGenRenderbuffersOES(1, &depthRenderbuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
	glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
	
	if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
	{
		//NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
		return NO;
	}
	
	return YES;
}

// Clean up any buffers we have allocated.
- (void)destroyFramebuffer
{
	glDeleteFramebuffersOES(1, &viewFramebuffer);
	viewFramebuffer = 0;
	glDeleteRenderbuffersOES(1, &viewRenderbuffer);
	viewRenderbuffer = 0;
	
	if(depthRenderbuffer)
	{
		glDeleteRenderbuffersOES(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}
}

// Releases resources when they are not longer needed.
- (void) dealloc
{
	if (brushTexture)
	{
		glDeleteTextures(1, &brushTexture);
		brushTexture = 0;
	}
	
	if([EAGLContext currentContext] == context)
	{
		[EAGLContext setCurrentContext:nil];
	}
	
}

//Steve commented - Not used anymore.  Using setBackgroundColorWithRed instead
// Erases the screen
/*
- (void) erase
{
    
    //NSLog(@"erase");
    
	[EAGLContext setCurrentContext:context];
	
	// Clear the buffer
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
    
    //Steve change to off white
	glClearColor(244.0f/255.0f, 243.0f/255.0f, 243.0f/255.0f, 1.0);
    
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Display the buffer
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
	[context presentRenderbuffer:GL_RENDERBUFFER_OES];
}
*/


//Steve added
//allows user to change the  color
-(void) setBackgroundColorWithRed: (CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha
{
     //NSLog(@"setBackgroundColorWithRed");
  
    
    //Taken from layoutSubview Methods
    [EAGLContext setCurrentContext:context];
	[self destroyFramebuffer];
	[self createFramebuffer];
    
	
	// Clear the buffer
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
    
    //Steve TEST
    glClearColor(red, green, blue, alpha);  //Steve added for custom color selection

    
    
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Display the buffer
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
	[context presentRenderbuffer:GL_RENDERBUFFER_OES];
    
}


// Drawings a line onscreen based on where the user touches
- (void) renderLineFromPoint:(CGPoint)start toPoint:(CGPoint)end
{
    //NSLog(@"renderLineFromPoint");
    
	static GLfloat*		vertexBuffer = NULL;
	static NSUInteger	vertexMax = 64;
	NSUInteger			vertexCount = 0,
    count,
    i;
	
	[EAGLContext setCurrentContext:context];
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
	
	// Convert locations from Points to Pixels
	CGFloat scale = self.contentScaleFactor;
	start.x *= scale;
	start.y *= scale;
	end.x *= scale;
	end.y *= scale;
	
	// Allocate vertex array buffer
	if(vertexBuffer == NULL)
		vertexBuffer = malloc(vertexMax * 2 * sizeof(GLfloat));
	
	// Add points to the buffer so there are drawing points every X pixels
	count = MAX(ceilf(sqrtf((end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y)) / kBrushPixelStep), 1);
    
	for(i = 0; i < count; ++i) {
		if(vertexCount == vertexMax) {
			vertexMax = 2 * vertexMax;
			vertexBuffer = realloc(vertexBuffer, vertexMax * 2 * sizeof(GLfloat));
		}
		
		vertexBuffer[2 * vertexCount + 0] = start.x + (end.x - start.x) * ((GLfloat)i / (GLfloat)count);
		vertexBuffer[2 * vertexCount + 1] = start.y + (end.y - start.y) * ((GLfloat)i / (GLfloat)count);
		vertexCount += 1;
	}
    
	
	// Render the vertex array
	glVertexPointer(2, GL_FLOAT, 0, vertexBuffer);
	glDrawArrays(GL_POINTS, 0, vertexCount);
	
	// Display the buffer
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
	[context presentRenderbuffer:GL_RENDERBUFFER_OES];
}


//Steve TEST
-(void) eraserOn: (BOOL)isEraserOnOrOff{
    
    isEraserOn = isEraserOnOrOff;
}

//Steve TEST
-(void) noteClassAndSticky:(BOOL) isNoteClassAndStickBothTrue{
    
     isNoteClassSticky = isNoteClassAndStickBothTrue;
}


// Reads previously recorded points and draws them onscreen. This is the Shake Me message that appears when the application launches.
- (void) playback:(NSMutableArray*)recordedPaths
{
	NSData*				data = [recordedPaths objectAtIndex:0];
	CGPoint*			point = (CGPoint*)[data bytes];
	NSUInteger			count = [data length] / sizeof(CGPoint),
    i;
	
	// Render the current path
	for(i = 0; i < count - 1; ++i, ++point)
		[self renderLineFromPoint:*point toPoint:*(point + 1)];
	
	// Render the next path after a short delay 
	[recordedPaths removeObjectAtIndex:0];
	if([recordedPaths count])
		[self performSelector:@selector(playback:) withObject:recordedPaths afterDelay:0.01];
}

// Handles the start of a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesBegan");
    
    //Steve - cancels Autoscrolls if view is touched again. 
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
	isTouchAgain = YES;
    mouseSwiped = NO;
    
    isReadyToScroll = NO; //Steve
	
	if (self.frame.origin.y == 44) {
		verticleDrowLimit = 280; //232 //270
	}else {
		verticleDrowLimit = sqrt(self.frame.origin.y * self.frame.origin.y) + 44 + 280; //272
	}
    
    
	if (self.frame.origin.x == 0) 
	{
        if (IS_IPAD)
            horizontalDrawLimit = 768; //was 600 Steve changed
        else if (IS_IPHONE_5)
            horizontalDrawLimit = 568;
		else
            horizontalDrawLimit = 480;
	}
	else 
	{
        //NSLog(@"************* horizontalDrawLimit ************ ");
		
        if (IS_IPAD)
            horizontalDrawLimit = sqrt(self.frame.origin.x * self.frame.origin.x) + 768; //Steve was 600
        else if (IS_IPHONE_5)
             horizontalDrawLimit = sqrt(self.frame.origin.x * self.frame.origin.x) + 568;
        else
            horizontalDrawLimit = sqrt(self.frame.origin.x * self.frame.origin.x) + 480;
        
	}
	
	
    //NSLog(@"self.frame.origin.x = %f",self.frame.origin.x);
    //NSLog(@"horizontalDrawLimit = %f",horizontalDrawLimit);
    
    //NSLog(@"self.frame.origin.y = %f",self.frame.origin.y);
	//NSLog(@"verticleDrowLimit %f", verticleDrowLimit);

	
	CGRect bounds = [self bounds];
    UITouch*	touch = [[event touchesForView:self] anyObject];
    CGPoint currentPoint = [touch locationInView: self];
    
    
	// Convert touch point from UIView referential to OpenGL one (upside-down flip)
	location = [touch locationInView:self];
	location.y = bounds.size.height - location.y;
    
    
    if (currentPoint.y > verticleDrowLimit || currentPoint.x > horizontalDrawLimit){
		return;
    }

    
    //Steve
    if (IS_IPAD) {
        
        // Detects when view should scroll by testing HorizonatalDrawLimit - 220 for dotting "i"
        if (currentPoint.x > horizontalDrawLimit - 220) {
            isReadyToScroll = YES;
        }
    }
    else{ //iPhone
        
        // Detects when view should scroll by testing HorizonatalDrawLimit - 160 for dotting "i"
        if (currentPoint.x > horizontalDrawLimit - 160) {
            isReadyToScroll = YES;
        }
    }
    
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event { 
    //NSLog(@"touchesMoved");
    
	mouseSwiped =       YES; //YES
	CGRect				bounds = [self bounds];
	UITouch*			touch = [[event touchesForView:self] anyObject];
	CGPoint currentPoint = [touch locationInView: self];
    
     //NSLog(@"currentPoint =  %@",NSStringFromCGPoint(currentPoint));
	
	if (currentPoint.y > verticleDrowLimit || currentPoint.x > horizontalDrawLimit) 
	{
         //NSLog(@"******* TouchesMoved Limits *************");
		return;
	}
	else 
	{
        
        //Steve
        if (IS_IPAD) {
            
            // Detects when view should scroll by testing HorizonatalDrawLimit - 220
            if (currentPoint.x > horizontalDrawLimit - 220) {
                isReadyToScroll = YES;
            }
        }
        else{ //iPhone
         
            // Detects when view should scroll by testing HorizonatalDrawLimit - 160
            if (currentPoint.x > horizontalDrawLimit - 160) {
                isReadyToScroll = YES;
            }
        }
        
        
  
		// Convert touch point from UIView referential to OpenGL one (upside-down flip)
		if (firstTouch) {
			firstTouch = NO;
			previousLocation = [touch previousLocationInView:self];
			previousLocation.y = bounds.size.height - previousLocation.y;
		} else {
			location = [touch locationInView:self];
			location.y = bounds.size.height - location.y;
			previousLocation = [touch previousLocationInView:self];
			previousLocation.y = bounds.size.height - previousLocation.y;
		}
        
		// Render the stroke
		[self renderLineFromPoint:previousLocation toPoint:location];
	}
}

// Handles the end of a touch event when the touch is a tap.
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesEnded");
    
	//CGRect				bounds = [self bounds];
    UITouch*	touch = [[event touchesForView:self] anyObject];
	CGPoint currentPoint = [touch locationInView: self];
    
    //NSLog(@"currentPoint =  %@",NSStringFromCGPoint(currentPoint));
    
    //Steve changed was: if (currentPoint.y > verticleDrowLimit) {
	//if (currentPoint.x > horizontalDrawLimit) { //Steve commented
    
    //Steve - used for dotting "i"
    if (!mouseSwiped) {
        CGPoint locationDot = CGPointMake(location.x, location.y+1);
        [self renderLineFromPoint:location toPoint:locationDot];  // Render the stroke
    }
    
    if (currentPoint.y > verticleDrowLimit) {
		isReadyToScroll = NO;
		return;
	}
    
	isTouchAgain = NO;
    
    
    //NSLog(@" horizontalDrawLimit = %f", horizontalDrawLimit);
    //NSLog(@" verticleDrowLimit = %f", verticleDrowLimit);
    //NSLog(@" isReadyToScroll = %d", isReadyToScroll);
    
    
    //Steve - checks to see if autoscroll is on first.  Erasing, for example, shuts autScroll off.
    if (isAutoScroll) {
        
        //Steve - Sends message to scroll
        if (isReadyToScroll && horizontalDrawLimit <= 880 + 426) {
            [self performSelector:@selector(scroll_screen) withObject:nil afterDelay:0.5];
        }
        
        //iPad - scroll 2 times
        if (IS_IPAD && isReadyToScroll && horizontalDrawLimit > 1220 && verticleDrowLimit < 700){
            [self performSelector:@selector(moveToNextLine:) withObject:nil afterDelay:0.5];
        }
        //iOS 5 - only scrolls 3 times
        else if (IS_IOS_5 && isReadyToScroll && horizontalDrawLimit > 880 && verticleDrowLimit < 700){
            [self performSelector:@selector(moveToNextLine:) withObject:nil afterDelay:0.5];
        }
        //iOS 6 - scrolls 4 times
        else if (!IS_IOS_5 && isReadyToScroll && horizontalDrawLimit > 1220 && verticleDrowLimit < 700){
            [self performSelector:@selector(moveToNextLine:) withObject:nil afterDelay:0.5];
        }
        

    }
    
    
}

// Handles the end of a touch event.
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	// If appropriate, add code necessary to save the state of the application.
	// This application is not saving state.
}

- (void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue
{
	// Set the brush color using premultiplied alpha values
	glColor4f(red	* kBrushOpacity,
			  green * kBrushOpacity,
			  blue	* kBrushOpacity,
			  kBrushOpacity);
    
    
    //////////////////////// Steve TEST //////////////////////////////
    
    // Display the buffer
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
    
    
    if (isEraserOn && !isNoteClassSticky){ //Don't want to use subtractive for Note Sticky
        //glBlendEquation(GL_FUNC_REVERSE_SUBTRACT); //erase SUBTRACTIVE BLENDING
        //glBlendEquation(GL_FUNC_REVERSE_SUBTRACT_OES); //Steve - iOS 8 fix
        glBlendEquationOES(GL_FUNC_REVERSE_SUBTRACT_OES); //Steve - iOS 8 fix & arm64
    }
    else{
        //glBlendEquation(GL_FUNC_ADD); //Add - ADDITIVE BLENDING (default)
        //glBlendEquation(GL_FUNC_ADD_OES); //Steve - iOS 8 fix
        glBlendEquationOES(GL_FUNC_ADD_OES); //Steve - iOS 8 fix & arm 64
    }
    
    [self setNeedsDisplay];
	[context presentRenderbuffer:GL_RENDERBUFFER_OES];
    
    //////////////////////// Steve End //////////////////////////////

}


-(void)moveToNextLine:(id) sender {
	//NSLog(@"moveToNextLine");
  
    //nextLineButton_Clicked
    if([self.scrollDelegate respondsToSelector:@selector(nextLineButton_Clicked:)]) {
        [self.scrollDelegate nextLineButton_Clicked:self];
    }
    
    //Steve commented. Now sends message to ViewController through a protocol
    /*
    if (self.frame.origin.y != -420) { //420
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2]; //0.2               //232
        [self setFrame:CGRectMake(0, self.frame.origin.y - 280, self.frame.size.width, self.frame.size.height)];
        [UIView commitAnimations];
    }
     */
}


-(void)scroll_screen {
    //NSLog(@"scroll_screen");
    
    if([self.scrollDelegate respondsToSelector:@selector(scrollRightButton_Clicked:)]) {
        [self.scrollDelegate scrollRightButton_Clicked:self];
    }
    
    //Steve commented. Now sends message to ViewController through a protocol
    /*
    if (IS_IPHONE_5) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2]; //0.3
        [self setUserInteractionEnabled:NO];            
        [self setFrame:CGRectMake(self.frame.origin.x - 410 - 88, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
        [self setUserInteractionEnabled:YES];
        [UIView commitAnimations];
    }
    else{
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2]; //0.3
        [self setUserInteractionEnabled:NO];            //400
        [self setFrame:CGRectMake(self.frame.origin.x - 370, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
        [self setUserInteractionEnabled:YES];
        [UIView commitAnimations];
    }
     */
    
}


- (UIImage *)getImage111 {
   // NSLog(@"getImage111");
    
	int width = CGRectGetWidth([self bounds]);
	int height = CGRectGetHeight([self bounds]);
	
    NSInteger myDataLength = width * height * 4;
	
    // allocate array and read pixels into it.
    GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	
    // gl renders "upside down" so swap top to bottom into new array.
    // there's gotta be a better way, but this works.
    GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
    for(int y = 0; y <height; y++)
    {
        for(int x = 0; x <width * 4; x++)
        {
            buffer2[(height - 1 - y) * width * 4 + x] = buffer[y * 4 * width + x];
        }
    }
	
    // make data provider with data.
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer2, myDataLength, NULL);
	
    // prep the ingredients
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * width;
    
    // Steve TEST
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
    
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
    eaglLayer.opaque = NO;
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    const CGFloat myColor[] = {0.0, 0.0, 0.0, 0.0};
    eaglLayer.backgroundColor = CGColorCreate(rgb, myColor);
    CGColorSpaceRelease(rgb);

	
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
	
    // make the cgimage
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
	CGColorSpaceRelease(colorSpaceRef);
	CGDataProviderRelease(provider);
	
    // then make the uiimage from that
    UIImage *myImage = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
    
    //CGDataProviderRelease(provider);
    
    return myImage;
}




void releaseScreenshotData(void *info, const void *data, size_t size) {
	//NSLog(@"releaseData\n");
	free((void *)data);
};

- (UIImage *)getImage {
    //NSLog(@"getImage");
    
    //	int width = CGRectGetWidth([self bounds]);
    //	int height = CGRectGetHeight([self bounds]);
    //	NSInteger myDataLength = width * height * 4;
    //	
    //	// allocate array and read pixels into it.
    //	GLuint *buffer = (GLuint *) malloc(myDataLength);
    //	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    //	
    //	// gl renders “upside down” so swap top to bottom into new array.
    //	for(int y = 0; y < height / 2; y++) {
    //		for(int x = 0; x < width; x++) {
    //			//Swap top and bottom bytes
    //			GLuint top = buffer[y * width + x];
    //			GLuint bottom = buffer[(height - 1 - y) * width + x];
    //			buffer[(height - 1 - y) * width + x] = top;
    //			buffer[y * width + x] = bottom;
    //		}
    //	}
    //	
    //	// make data provider with data.
    //	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, myDataLength, releaseScreenshotData);
    //	
    //	// prep the ingredients
    //	const int bitsPerComponent = 8;
    //	const int bitsPerPixel = 4 * bitsPerComponent;
    //	const int bytesPerRow = 4 * width;
    //	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    //	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    //	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    //	
    //	// make the cgimage
    //	CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    //	
    //	CGColorSpaceRelease(colorSpaceRef);
    //	CGDataProviderRelease(provider);
    //	// then make the UIImage from that
    //	UIImage *myImage = [UIImage imageWithCGImage:imageRef];
    //	CGImageRelease(imageRef);
    ////	UIImageWriteToSavedPhotosAlbum(myImage, self, nil, nil);
    //	return myImage;
	
    //	CGRect rect = [[UIScreen mainScreen] bounds];
    
    
	int width = CGRectGetWidth([self bounds]);
	int height = CGRectGetHeight([self bounds]);
	
	NSInteger myDataLength = width * height * 4;
	GLubyte *buffer = (GLubyte *) malloc(myDataLength);
	GLubyte *bufferFlipped = (GLubyte *) malloc(myDataLength);
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	for(int y = 0; y <height; y++) {
		for(int x = 0; x <width * 4; x++) {
			bufferFlipped[(height - 1 - y) * width * 4 + x] = buffer[y * 4 * width + x];
		}
	}
	free(buffer);	// free original buffer
    
	
	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, bufferFlipped, myDataLength, releaseScreenshotData);
	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    
    //Steve changed to  kCGImageAlphaPremultipliedLast                              Was: kCGBitmapByteOrderDefault
	CGImageRef imageRef = CGImageCreate(width, height, 8, 32, 4 * width, colorSpaceRef, kCGImageAlphaPremultipliedLast, provider, NULL, NO, kCGRenderingIntentDefault);
    

	CGColorSpaceRelease(colorSpaceRef);
	CGDataProviderRelease(provider);
	
	UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
	CGImageRelease(imageRef);
    
	
	return image;
}


//Steve - tells this class if Erase button is ON and therefore shutoff autoscroll
-(void) setAutoScrollOn: (BOOL) isAutoScrollOn{
    
    isAutoScroll = isAutoScrollOn;
}


@end

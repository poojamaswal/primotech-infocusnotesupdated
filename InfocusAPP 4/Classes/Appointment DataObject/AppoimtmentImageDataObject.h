//
//  AppoimtmentImageDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 22/05/11.
//  Copyright 2011 DAVV. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppoimtmentImageDataObject : NSObject {
	NSInteger appointmentID;
	UIImage *displayImage;
	UIImage *largeImage;
}

@property(nonatomic, assign) NSInteger appointmentID;
@property(nonatomic, retain) UIImage *displayImage;
@property(nonatomic, retain) UIImage *largeImage;

@end

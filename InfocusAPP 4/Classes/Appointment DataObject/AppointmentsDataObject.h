//
//  AppointmentsDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppointmentsDataObject : NSObject 
{
	NSInteger  appointmentID;
	NSString  *startDateTime;
	NSString  *dueDateTime; 
	NSString  *shortNotes; 
	NSString  *repeat;
	NSString  *alert;
    NSString *alertSecond;
	NSInteger locID;
	NSInteger calID;
	NSString  *textLocName;
	NSString  *appointmentTitle;
	NSString  *appointmentType; 
	NSData    *appointmentBinary;
	NSData    *appointmentBinaryLarge;
	NSString  *calendarName;
	UIColor   *calendarColor;
	BOOL	  isAllDay;	
	NSInteger eventValue;
    NSString *relation_Field1;
    BOOL      isDontShowStartDate; //Steve - used for long events > 1 day
    BOOL      isDontShowEndDate; //Steve - used for long events > 1 day
    BOOL      isLongEventAndAllDay; //Steve - used for long events > 2 day
    
}

@property(nonatomic,strong)NSString     *textLocName;
@property(nonatomic, assign) NSInteger appointmentID;
@property(nonatomic, strong) NSString  *startDateTime;
@property(nonatomic, strong) NSString  *dueDateTime; 
@property(nonatomic, strong) NSString  *shortNotes; 
@property(nonatomic, strong) NSString  *repeat;
@property(nonatomic, strong) NSString  *alert;
@property(nonatomic, strong) NSString  *alertSecond;
@property(nonatomic, assign) NSInteger locID;
@property(nonatomic, assign) NSInteger calID;
@property(nonatomic, strong) NSString  *appointmentTitle;
@property(nonatomic, strong) NSString  *appointmentType; 
@property(nonatomic, strong) NSData    *appointmentBinary;
@property(nonatomic, strong) NSData    *appointmentBinaryLarge;
@property(nonatomic, strong) NSString  *calendarName;
@property(nonatomic, strong) UIColor   *calendarColor;
@property(nonatomic, assign) BOOL	  isAllDay;	
@property(nonatomic, assign) NSInteger eventValue;
@property(nonatomic, strong) NSString *relation_Field1;
@property(nonatomic, assign) BOOL isDontShowStartDate; //Steve - used for long events > 1 day
@property(nonatomic, assign) BOOL isDontShowEndDate; //Steve - used for long events > 1 day
@property(nonatomic, assign) BOOL isLongEventAndAllDay; //Steve - used for long events > 2 day
@property(nonatomic, strong) NSTimeZone  *timeZoneName; //Steve - Calendar time zone



@end
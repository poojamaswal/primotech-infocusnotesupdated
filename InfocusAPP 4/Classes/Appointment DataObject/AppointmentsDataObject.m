//
//  AppointmentsDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppointmentsDataObject.h"

@implementation AppointmentsDataObject

@synthesize appointmentID;
@synthesize startDateTime, dueDateTime; 
@synthesize appointmentTitle, appointmentType, appointmentBinary, appointmentBinaryLarge;
@synthesize shortNotes; 
@synthesize alert, repeat;
@synthesize locID, calID;
@synthesize calendarName, calendarColor, isAllDay,textLocName, eventValue;
@synthesize alertSecond; //Anils's Addition///
@synthesize relation_Field1;//Alok's Added
@synthesize isDontShowEndDate; //Steve
@synthesize isDontShowStartDate; //Steve
@synthesize isLongEventAndAllDay; //Steve



@end

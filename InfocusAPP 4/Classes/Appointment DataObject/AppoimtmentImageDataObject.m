//
//  AppoimtmentImageDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 22/05/11.
//  Copyright 2011 DAVV. All rights reserved.
//

#import "AppoimtmentImageDataObject.h"

@implementation AppoimtmentImageDataObject
@synthesize	 displayImage, largeImage;

- (id)initWithCoder:(NSCoder *)coder { 
	NSData *displayImageData = [[coder decodeObjectForKey:@"displayImage"] retain];
	NSData *largeImageData = [[coder decodeObjectForKey:@"largeImage"] retain];
	UIImage *displayImageLocal = [UIImage imageWithData:displayImageData];
	UIImage *largeImageLocal = [UIImage imageWithData:largeImageData];

	appointmentID = [coder decodeIntegerForKey:@"appointmentID"];
	displayImage = [displayImageLocal retain];
	largeImage = [largeImageLocal  retain];

	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	NSData *displayImageData = UIImageJPEGRepresentation(displayImage, 0);   
	NSData *largeImageData = UIImageJPEGRepresentation(largeImage, 0);   

	[coder encodeInteger:appointmentID forKey:@"appointmentID"];
	[coder encodeObject:displayImageData forKey:@"displayImage"];
	[coder encodeObject:largeImageData forKey:@"largeImage"];
}

-(void) dealloc {
	[super dealloc];
	[displayImage release];
	[largeImage release];
}

@end

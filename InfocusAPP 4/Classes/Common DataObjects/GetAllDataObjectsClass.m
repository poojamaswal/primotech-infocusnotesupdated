//
//  GetAllDataObjectsClass.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GetAllDataObjectsClass.h"
#import "AppointmentsDataObject.h"
#import "OrganizerAppDelegate.h"


@implementation GetAllDataObjectsClass
#pragma mark -  
#pragma mark -  Appointment Methods 
#pragma mark -  

+(void) getAppointmentsArrayOrderByDueDate {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
//    NSString* prevString = @"";
    NSMutableDictionary *tempDic = nil;
    NSString *previousDate = nil;
	
	[appDelegate.appointmentsArray removeAllObjects];
    [appDelegate.shortStartDateArray removeAllObjects];
	
	sqlite3_stmt *selectAppointment_Stmt = nil; 
	NSMutableArray* aSubArray=[[NSMutableArray alloc]init];
	if(selectAppointment_Stmt == nil && appDelegate.database) {
		NSString *Sql = @"Select AppointmentID, CalendarID, LocationID, StartDateTime, DueDateTime, Repeat, Alert, ShortNotes, AppointmentTitle, AppointmentType, AppointmentBinary, AppointmentBinaryLarge, textLocName, subStr(StartDateTime,0,11)  as shortStartDate From AppointmentsTable order by subStr(StartDateTime,0,11)";
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK) {
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
        else 
        {
			while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW) 
            {
				AppointmentsDataObject* appDataObject = [[AppointmentsDataObject alloc]init];
				
				[appDataObject setAppointmentID: sqlite3_column_int(selectAppointment_Stmt, 0)];
				[appDataObject setCalID:sqlite3_column_int(selectAppointment_Stmt, 1)];
				[appDataObject setLocID:sqlite3_column_int(selectAppointment_Stmt, 2)];
                const char* string3 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 3);
                NSString* str3= string3 == NULL ? nil : [NSString stringWithUTF8String:string3];
				[appDataObject setStartDateTime:str3];
                const char* string4 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 4);
                NSString* str4= string4 == NULL ? nil : [NSString stringWithUTF8String:string4];
				[appDataObject setDueDateTime:str4];
				[appDataObject setShortNotes:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 7)]];
				[appDataObject setRepeat:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 5)]];
				[appDataObject setAlert:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 6)]];
				[appDataObject setAppointmentTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 8)]];
				[appDataObject setAppointmentType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 9)]];
                NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 10);
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 10) length:blobLength];
                
				blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 11);
				NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 11) length:blobLength];
                const char* string12 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 12);
                NSString* str12 = string12 == NULL ? nil : [NSString stringWithUTF8String:string12];
//				[appDataObject setShortStartDate:str12];
				[appDataObject setAppointmentBinary:binaryData];
				[appDataObject setAppointmentBinaryLarge:binaryDataLarge];
//				[appDelegate.appointmentsArray addObject:appDataObject];
				[appDataObject setTextLocName:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 13)]];

                if(![previousDate isEqualToString:str12] || previousDate == nil)
                {
                    if (tempDic != nil) {
                        [tempDic setValue:[aSubArray mutableCopy] forKey:@"Details"];
                        [appDelegate.appointmentsArray addObject:[tempDic mutableCopy]];
                        //NSLog(@"%@", [appDelegate.appointmentsArray objectAtIndex:0]);
                        [aSubArray removeAllObjects];
                        tempDic=nil;
                    }
                    tempDic=[[NSMutableDictionary alloc]initWithCapacity:0];
                    [tempDic setValue:str12 forKey:@"Date"];
                    previousDate=str12;
                    [aSubArray addObject:appDataObject];
                }
                else
                {
                    [aSubArray addObject:appDataObject];
                }
				
            }//while end
            if (tempDic!=nil) {
                [tempDic setValue:[aSubArray mutableCopy] forKey:@"Details"];
                [appDelegate.appointmentsArray addObject:[tempDic mutableCopy]];
                //NSLog(@"%d", [appDelegate.appointmentsArray count]);
                [aSubArray removeAllObjects];
                tempDic=nil;
            }
        }//else end
        
        sqlite3_finalize(selectAppointment_Stmt);		
        selectAppointment_Stmt = nil;
    }
	else 
	{
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		//NSAssert1(0, @"Error: Error in Statement Or Database");
	}
	return;
}

+(void) getAppointmentsArrayForDay:(NSDate*)selectedDate
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	//database = [appDelegate database];
    [appDelegate.dayApp_Array removeAllObjects];
	
    sqlite3_stmt *selectAppointment_Stmt = nil; 
	
	if(selectAppointment_Stmt == nil && appDelegate.database) 
    {
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        
        // Add Other var Saving States.
        [formater setDateFormat:@"yyyy-MM-dd"];
        // NSString* astr=strtDateLbl.text;
        //[formater setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
        NSString *tempStartDate = [formater stringFromDate:selectedDate];
        
        NSString *Sql1 = [[NSString alloc] initWithFormat: @"Select a.AppointmentID, a.CalendarID, a.LocationID, a.StartDateTime, a.DueDateTime, a.Repeat, a.Alert, a.ShortNotes, a.AppointmentTitle, a.AppointmentType, a.AppointmentBinary, a.AppointmentBinaryLarge, a.textLocName  From AppointmentsTable a  where a.StartDateTime like '%%%@%%'   Order By a.StartDateTime Asc", tempStartDate];
        //NSLog(@"Day query : %@", Sql1);
        //, substr(a.StartDateTime, 14,20) as 'Time'
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql1 UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK) 
        {
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
        else 
        {
 			while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW) {
				AppointmentsDataObject* appDataObject = [[AppointmentsDataObject alloc]init];
				
				[appDataObject setAppointmentID: sqlite3_column_int(selectAppointment_Stmt, 0)];
				[appDataObject setCalID:sqlite3_column_int(selectAppointment_Stmt, 1)];
				[appDataObject setLocID:sqlite3_column_int(selectAppointment_Stmt, 2)];
                const char* string3 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 3);
                NSString* str3= string3 == NULL ? nil : [NSString stringWithUTF8String:string3];
				[appDataObject setStartDateTime:str3];
                const char* string4 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 4);
                NSString* str4= string4 == NULL ? nil : [NSString stringWithUTF8String:string4];
				[appDataObject setDueDateTime:str4];
				[appDataObject setShortNotes:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 7)]];
				[appDataObject setRepeat:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 5)]];
				[appDataObject setAlert:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 6)]];
				[appDataObject setAppointmentTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 8)]];
				[appDataObject setAppointmentType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 9)]];
                NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 10);
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 10) length:blobLength];
                
				blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 11);
				NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 11) length:blobLength];
				[appDataObject setAppointmentBinary:binaryData];
				[appDataObject setAppointmentBinaryLarge:binaryDataLarge];
				[appDataObject setTextLocName:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 12)]];

				[appDelegate.dayApp_Array addObject:appDataObject]; 
				
			}
			sqlite3_finalize(selectAppointment_Stmt);		
			selectAppointment_Stmt = nil;
		}
	}
	else 
	{
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
 	}
	return;
}

+(void) getAppointmentsArrayOrderByStartDate {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate.appointmentsArray removeAllObjects];
	
	sqlite3_stmt *selectAppointment_Stmt = nil; 
	
	if(selectAppointment_Stmt == nil && appDelegate.database) {
		NSString *Sql = @"Select a.AppointmentID, a.CalendarID, a.LocationID, a.StartDateTime, a.DueDateTime, a.Repeat, a.Alert, a.ShortNotes, a.AppointmentTitle, a.AppointmentType, a.AppointmentBinary, a.AppointmentBinaryLarge, a.textLocName, b.numberOfRows From AppointmentsTable a Left Outer Join (Select count(1) as numberOfRows from AppointmentsTable) b Order By a.StartDateTime Asc";
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK) {
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}else {
			while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW) {
				AppointmentsDataObject* appDataObject = [[AppointmentsDataObject alloc]init];
				NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 10);
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 10) length:blobLength];
				
				blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 11);
				NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 11) length:blobLength];
				
				[appDataObject setAppointmentID: sqlite3_column_int(selectAppointment_Stmt, 0)];
				[appDataObject setCalID:sqlite3_column_int(selectAppointment_Stmt, 1)];
				[appDataObject setLocID:sqlite3_column_int(selectAppointment_Stmt, 2)];
				[appDataObject setStartDateTime:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 3)]];
				[appDataObject setDueDateTime:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 4)]];
				[appDataObject setShortNotes:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 7)]];
				[appDataObject setRepeat:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 5)]];
				[appDataObject setAlert:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 6)]];
				[appDataObject setAppointmentTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 8)]];
				[appDataObject setAppointmentType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 9)]];
				[appDataObject setAppointmentBinary:binaryData];
				[appDataObject setAppointmentBinaryLarge:binaryDataLarge];
				[appDataObject setTextLocName:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 10)]];

				[appDelegate.appointmentsArray  addObject:appDataObject]; 
			}
			
			sqlite3_finalize(selectAppointment_Stmt);		
			selectAppointment_Stmt = nil;
			//return [appDataObject autorelease];
		}
	}else {
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
	}
	return;
}







//+(void) getAllLocationData 
//{
//	
//	NSMutableArray *locationList = [[NSMutableArray alloc] init];
//	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
//	//database = [appDelegate database];
//	
//	sqlite3_stmt *selectAllLocationData_stmt = nil;
//	
//	if(selectAllLocationData_stmt == nil && appDelegate.database) {
//		const char* Sql = "Select LocationID, LocationPersonName, LocationAddress,LocationCity, LocationState, LocationZIP,IsVerified From LocationTable";
//		
//		if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectAllLocationData_stmt, NULL) == SQLITE_OK) {
//			while (sqlite3_step(selectAllLocationData_stmt) == SQLITE_ROW) {
//				LocationDataObject *locationDataObject = [[LocationDataObject alloc] initWithlocationID:sqlite3_column_int(selectAllLocationData_stmt, 0) locationPersonName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllLocationData_stmt, 1)] locationAddress:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllLocationData_stmt, 2)] locationCity:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllLocationData_stmt, 3)] locationState:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllLocationData_stmt, 4)] locationZip:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllLocationData_stmt, 5)] isVerified:sqlite3_column_int(selectAllLocationData_stmt, 6)];
//				[locationList addObject:locationDataObject];
//				[locationDataObject release];
//			}
//			sqlite3_finalize(selectAllLocationData_stmt);		
//			selectAllLocationData_stmt = nil;
//			return [locationList autorelease];
//		}
//	}
//	return nil;
//}
//
//+(void) getAllCalendarData {
//	NSMutableArray *calNameList = [[NSMutableArray alloc] init];
//	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
//	//database = [appDelegate database];
//	sqlite3_stmt *selectAllCalNameData_stmt = nil;
//	
//	if(selectAllCalNameData_stmt == nil && appDelegate.database) {
//		NSString* Sql = [[NSString alloc] initWithString:@"Select CalendarID, CalendarName, CalendarColorRed, CalendarColorGreen, CalendarColorBlue, isOrganizerCalendar, isDefaultCalendar From CalendarTable"];
//		
//		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAllCalNameData_stmt, NULL) == SQLITE_OK) {
//			while (sqlite3_step(selectAllCalNameData_stmt) == SQLITE_ROW) {
//				CalendarDataObject *calendarDataObject = [[CalendarDataObject alloc] initWithCalendarID:sqlite3_column_int(selectAllCalNameData_stmt, 0) calendarName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllCalNameData_stmt, 1)] calendarColorRed:sqlite3_column_double(selectAllCalNameData_stmt, 2) calendarColorGreen:sqlite3_column_double(selectAllCalNameData_stmt, 3) calendarColorBlue:sqlite3_column_double(selectAllCalNameData_stmt, 4) isOrganizerCalendar:sqlite3_column_int(selectAllCalNameData_stmt, 5) isDefaultCalendar:sqlite3_column_int(selectAllCalNameData_stmt, 6)];
//				[calNameList addObject:calendarDataObject];
//				
//				[calendarDataObject release];
//			}
//			sqlite3_finalize(selectAllCalNameData_stmt);		
//			selectAllCalNameData_stmt = nil;
//			return [calNameList autorelease];
//		}
//		[Sql release];
//	}
//	[calNameList release];
//	return nil;
//}
//

+(CalendarDataObject *) getDefaultCalendarDataObject {
	CalendarDataObject *calendarDataObject = nil;
	
	sqlite3_stmt *selectDefaultCalendarData_stmt = nil;
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	if(selectDefaultCalendarData_stmt == nil && appDelegate.database) {
		NSString *tempString = [[NSString alloc] initWithFormat:@"Select CalendarID, CalendarName, CalendarColorRed, CalendarColorGreen, CalendarColorBlue, isOrganizerCalendar, isDefaultCalendar From CalendarTable Where isDefaultCalendar = 1"];
        
		if(sqlite3_prepare_v2(appDelegate.database, [tempString UTF8String], -1, &selectDefaultCalendarData_stmt, NULL) == SQLITE_OK) {
			if(sqlite3_step(selectDefaultCalendarData_stmt) == SQLITE_ROW) {
				calendarDataObject = [[CalendarDataObject alloc] initWithCalendarID:sqlite3_column_int(selectDefaultCalendarData_stmt, 0) calendarName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectDefaultCalendarData_stmt, 1)] calendarColorRed:sqlite3_column_double(selectDefaultCalendarData_stmt, 2) calendarColorGreen:sqlite3_column_double(selectDefaultCalendarData_stmt, 3) calendarColorBlue:sqlite3_column_double(selectDefaultCalendarData_stmt, 4) isOrganizerCalendar:sqlite3_column_int(selectDefaultCalendarData_stmt, 5) isDefaultCalendar:sqlite3_column_int(selectDefaultCalendarData_stmt, 6)];
			}
			sqlite3_finalize(selectDefaultCalendarData_stmt);		
			selectDefaultCalendarData_stmt = nil;
		}
	}
	return calendarDataObject;
}

+(CalendarDataObject *)getCalendarDataObjectForCalendarID:(NSInteger) calendarIDLocal {
	
	CalendarDataObject *calendarDataObject;
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	//database = [appDelegate database];
	sqlite3_stmt *selectCalendarData_stmt = nil;
	
	if(selectCalendarData_stmt == nil && appDelegate.database) {
		NSString *tempString = [[NSString alloc] initWithFormat:@"Select CalendarID, CalendarName, CalendarColorRed, CalendarColorGreen, CalendarColorBlue, isOrganizerCalendar, isDefaultCalendar From CalendarTable Where CalendarID = %d", calendarIDLocal];
        
		if(sqlite3_prepare_v2(appDelegate.database, [tempString UTF8String], -1, &selectCalendarData_stmt, NULL) == SQLITE_OK) {
			if(sqlite3_step(selectCalendarData_stmt) == SQLITE_ROW) {
				calendarDataObject = [[CalendarDataObject alloc] initWithCalendarID:sqlite3_column_int(selectCalendarData_stmt, 0) calendarName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectCalendarData_stmt, 1)] calendarColorRed:sqlite3_column_double(selectCalendarData_stmt, 2) calendarColorGreen:sqlite3_column_double(selectCalendarData_stmt, 3) calendarColorBlue:sqlite3_column_double(selectCalendarData_stmt, 4) isOrganizerCalendar:sqlite3_column_int(selectCalendarData_stmt, 5) isDefaultCalendar:sqlite3_column_int(selectCalendarData_stmt, 6)];
			}
			sqlite3_finalize(selectCalendarData_stmt);		
			selectCalendarData_stmt = nil;
		}
	}
	return calendarDataObject;
}

//added
/*
+(BOOL)deleteCalenderWithID :(NSInteger)CalenderID 
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *deleteStmt = nil;
	
	if(deleteStmt == nil) {
		const char *sql = "delete from CalendarTable where CalendarID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteStmt, 1, CalenderID);
	
	if (SQLITE_DONE != sqlite3_step(deleteStmt))
    {
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_reset(deleteStmt);
		return NO;
	}
    else 
    {
		sqlite3_reset(deleteStmt);
		
		return YES;
	}
	return YES;
}
*/

//finish

+(LocationDataObject *)getLocationDataObjectForLocationID:(NSInteger) locationID {
	
	LocationDataObject *locationDataObject;
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectLocationData_stmt = nil;
	
	NSString *tempString = [[NSString alloc] initWithFormat:@"Select LocationID, LocationPersonName, LocationAddress, LocationCity, LocationState,  LocationZIP, IsVerified From LocationTable Where LocationID = %d", locationID];
	
	if (!locationID) {
		//NSLog( @"Location ID :%d,", locationID);
	}
	
	if(sqlite3_prepare_v2(appDelegate.database, [tempString UTF8String], -1, &selectLocationData_stmt, NULL) == SQLITE_OK) {
		if(sqlite3_step(selectLocationData_stmt) == SQLITE_ROW) {
			locationDataObject = [[LocationDataObject alloc] initWithlocationID:sqlite3_column_int(selectLocationData_stmt, 0) locationPersonName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectLocationData_stmt, 1)] locationAddress:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectLocationData_stmt, 2)] locationCity:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectLocationData_stmt, 3)] locationState:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectLocationData_stmt, 4)] locationZip:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectLocationData_stmt, 5)] isVerified:sqlite3_column_int(selectLocationData_stmt, 6)];
		}
		sqlite3_finalize(selectLocationData_stmt);		
		selectLocationData_stmt = nil;
	}
	return locationDataObject;	
}

//
//+(UIImage *) getImageForAppointmentID:(NSInteger ) appointmentIDLocal getDisplayImage:(BOOL) isDisplayImage {
//	UIImage *image;
//	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
//	//database = [appDelegate database];
//	
//	sqlite3_stmt *selectAppointmentImage_stmt = nil;
//	
//	if (isDisplayImage) {
//		if(selectAppointmentImage_stmt == nil && appDelegate.database) {
//			NSString *tempString  = [[NSString alloc] initWithFormat:@"Select AppointmentBinary From AppointmentsTable Where AppointmentID = %d", appointmentIDLocal];
//			
//			const char* Sql = [tempString UTF8String];
//			NSLog(@"Retain Count %d" , [tempString retainCount]);        
//			[tempString release];
//			
//			if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectAppointmentImage_stmt, NULL) == SQLITE_OK) {
//				if(sqlite3_step(selectAppointmentImage_stmt) == SQLITE_ROW) {
//					
//					NSUInteger blobLength = sqlite3_column_bytes(selectAppointmentImage_stmt, 0);
//					NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointmentImage_stmt, 0) length:blobLength];
//					
//					image = [[UIImage alloc] initWithData:binaryData];
//					
//				}
//				sqlite3_finalize(selectAppointmentImage_stmt);		
//				selectAppointmentImage_stmt = nil;
//				return [image autorelease];
//			}else {
//				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
//			}
//		}
//	}else {
//		if(selectAppointmentImage_stmt == nil && appDelegate.database) {
//			NSString *tempString  = [[NSString alloc] initWithFormat:@"Select AppointmentBinaryLarge From AppointmentsTable Where AppointmentID = %d", appointmentIDLocal];
//			
//			const char* Sql = [tempString UTF8String];
//			NSLog(@"Retain Count %d" , [tempString retainCount]);        
//			[tempString release];
//			
//			if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectAppointmentImage_stmt, NULL) == SQLITE_OK) {
//				if(sqlite3_step(selectAppointmentImage_stmt) == SQLITE_ROW) {
//					
//					NSUInteger blobLength = sqlite3_column_bytes(selectAppointmentImage_stmt, 0);
//					NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointmentImage_stmt, 0) length:blobLength];
//					
//					image = [[UIImage alloc] initWithData:binaryData];
//					
//				}
//				sqlite3_finalize(selectAppointmentImage_stmt);		
//				selectAppointmentImage_stmt = nil;
//				return [image autorelease];
//			}else {
//				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
//			}
//		}
//	}
//	return nil;
//}
//
+(NSInteger)getCalendarId:(EKCalendar *)CalObj
{
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    
    if (selected_stmt == nil && appDelegate.database)
        // CalObj.CGColor
    {
       
        NSString *CalName=  [NSString stringWithFormat:@"%@~%@",CalObj.title,CalObj.calendarIdentifier];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT * from CalendarTable WHERE CalendarName = \"%@\"",CalName];
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW)
            {
                return  sqlite3_column_int(selected_stmt, 0);
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
       /////
       sqlite3_stmt *insert_statement = nil;
       const CGFloat* components = CGColorGetComponents(CalObj.CGColor);
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO CalendarTable (CalendarName,CalendarColorRed,CalendarColorGreen,CalendarColorBlue,isOrganizerCalendar,isDefaultCalendar) VALUES (?,?,?,?,\"1\",\"0\")"];
        if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String], -1, &insert_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating statement. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_bind_text(insert_statement, 1, [CalName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_double(insert_statement, 2, components[0]);
        sqlite3_bind_double(insert_statement, 3, components[1]);
        sqlite3_bind_double(insert_statement, 4, components[2]);
        if(SQLITE_DONE != sqlite3_step(insert_statement)){
            NSAssert1(0, @"Error while inserting record. '%s'", sqlite3_errmsg(appDelegate.database));
            sqlite3_finalize(insert_statement);
            insert_statement=nil;
            //return NO;
        } else {
             return sqlite3_last_insert_rowid(appDelegate.database);sqlite3_finalize(insert_statement);
           insert_statement=nil;
        }
        
    }
    return 0;
	
}

+(BOOL)AddUpdateAppointmentData:(EKEvent *) EKEventDataObjLocal {
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	EKEvent *appDataObj = EKEventDataObjLocal;
	sqlite3_stmt *updateAppointmentData = nil;
    BOOL  IsAdd=NO;
    
    NSString *sqlquery =@"";
    sqlquery = [[NSString alloc]initWithFormat:@"select AppointmentID from AppointmentsTable where Relation_Field1 = \"%@\"",appDataObj.eventIdentifier];
    if(sqlite3_prepare_v2(appDelegate.database,[sqlquery UTF8String ], -1, &updateAppointmentData, NULL) != SQLITE_OK)
    {
        NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
    }
    else{
        int counter =0;
        while(sqlite3_step(updateAppointmentData) == SQLITE_ROW)counter=counter+1;
         if(counter>0)IsAdd =NO;
        else IsAdd =YES;
        sqlite3_finalize(updateAppointmentData);		
        updateAppointmentData = nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSString* strStrtDate = [dateFormatter stringFromDate:appDataObj.startDate];
	NSString* strEndDate = [dateFormatter stringFromDate:appDataObj.endDate];
    
    if(appDataObj.location == nil)appDataObj.location = @"";
    if(appDataObj.notes == nil)appDataObj.notes = @"";
   
       
    //NSLog(@"\n********* Event Data To update *************");
    /*NSLog(@"\nEvent Title: %@",appDataObj.title);
    NSLog(@"\nEvent Identifire: %@",appDataObj.eventIdentifier);
    NSLog(@"\nEvent StartDate: %@",appDataObj.startDate);
    NSLog(@"\nEvent UUID: %@",appDataObj.UUID);
    NSLog(@"\nEvent calendar: %@",appDataObj.calendar);
    //NSLog(@"\nEvent calendar Identifier: %@",appDataObj.calendarItemIdentifier);
    // NSLog(@"\nEvent ExternalIdentifier : %@",appDataObj.calendarItemExternalIdentifier);
    NSLog(@"\nConverted StartDate: %@",strStrtDate);
    NSLog(@"\nlocation: %@",appDataObj.location);
    NSLog(@"Event Notes: %@",appDataObj.notes);
    */
   int CalenderID= [self getCalendarId:appDataObj.calendar];
        NSLog(@"CalendarId->%d",CalenderID);
    
	const char *sql;
	if(updateAppointmentData == nil) {
		
        if(IsAdd)
            sql = "INSERT INTO AppointmentsTable (CalendarID,StartDateTime,DueDateTime,LocationID,Repeat,Alert,ShortNotes,AppointmentTitle,AppointmentType,AlertSecond,textLocName,Relation_Field1) VALUES (?,?,?,\"0\",\" \",\" \",?,?,\"T\",\" \",?,?)";
            //sql = "insert into AppointmentsTable(CalendarID, StartDateTime, DueDateTime, ShortNotes, AppointmentTitle, textLocName, Relation_Field1) Values(?, ?, ?, ?, ?, ?, ?)";
     
        else
            sql = "Update AppointmentsTable Set CalendarID = ?, StartDateTime = ?, DueDateTime = ?, ShortNotes = ?, AppointmentTitle = ?, textLocName= ? Where Relation_Field1 = ?";
        
        //  , isAllDay = ?sql = "Update AppointmentsTable Set AppointmentTitle = ? Where Relation_Field1 = \"EEC867DF-7C36-4D6D-9C86-AFDD8A0676EB:16E8A8FF-9242-4259-9C17-5A09D1D0D858\"";
        
        ///	NSString* StrSql = [[NSString alloc] initWithFormat:@"Update AppointmentsTable Set CalendarID = %d, StartDateTime = '%@', DueDateTime = '%@', ShortNotes = '%@', AppointmentTitle = '%@', textLocName= '%@' , isAllDay = '%c' Where Relation_Field1 = '%@'",1,strStrtDate,strEndDate,appDataObj.notes,appDataObj.title,appDataObj.location,appDataObj.isAllDay,appDataObj.eventIdentifier];
        //LocationID = ?, Repeat = ?, Alert = ?,AppointmentType = ?,AlertSecond = ?, AppointmentBinary = ?,
		//if(sqlite3_prepare_v2(appDelegate.database, [StrSql UTF8String], -1, &updateAppointmentData, NULL) == SQLITE_OK)
        
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateAppointmentData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
    sqlite3_bind_int(updateAppointmentData, 1, CalenderID);
	
	sqlite3_bind_text(updateAppointmentData, 2, [strStrtDate UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 3, [strEndDate UTF8String], -1, SQLITE_TRANSIENT);
	
	//sqlite3_bind_int(updateAppointmentData, 4, 0);// appDataObj.locID);
	
	//sqlite3_bind_text(updateAppointmentData, 5, [appDataObj.repeat  UTF8String], -1, SQLITE_TRANSIENT);
	
	//sqlite3_bind_text(updateAppointmentData, 6, [appDataObj.alert UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 4, [appDataObj.notes UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 5, [appDataObj.title UTF8String], -1, SQLITE_TRANSIENT);
	/*
     //sqlite3_bind_text(updateAppointmentData, 9, [appDataObj.appointmentType UTF8String], -1, SQLITE_TRANSIENT);
     
     sqlite3_bind_blob(updateAppointmentData, 10, [appDataObj.appointmentBinary bytes], [appDataObj.appointmentBinary length], SQLITE_TRANSIENT);
     */
	sqlite3_bind_text(updateAppointmentData, 6, [appDataObj.location UTF8String], -1, SQLITE_TRANSIENT);
    
	
    //int s7=		sqlite3_bind_int(updateAppointmentData, 7, [appDataObj isAllDay]);
    //sqlite3_bind_text(updateAppointmentData, 13, [appDataObj.alertSecond UTF8String], -1, SQLITE_TRANSIENT);*/
    sqlite3_bind_text(updateAppointmentData, 7, [appDataObj.eventIdentifier UTF8String], -1, SQLITE_TRANSIENT);
    
    
	if(SQLITE_DONE != sqlite3_step(updateAppointmentData)){
		NSAssert1(0, @"Error while inserting/Updating Events records. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
		return NO;
	} else {
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		// NSInteger locationID = sqlite3_last_insert_rowid(database);
		sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
		return YES;
	}
	return YES;
}
+(BOOL)updateAppointmentData:(EKEvent *) EKEventDataObjLocal {
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	EKEvent *appDataObj = EKEventDataObjLocal;
	sqlite3_stmt *updateAppointmentData = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
	
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSString* strStrtDate = [dateFormatter stringFromDate:appDataObj.startDate];
	NSString* strEndDate = [dateFormatter stringFromDate:appDataObj.endDate];
    
    
    //NSLog(@"\n********* Event Data To update *************");
    //NSLog(@"\nEvent Title: %@",appDataObj.title);
    //NSLog(@"\nEvent Identifire: %@",appDataObj.eventIdentifier);
    //NSLog(@"\nEvent StartDate: %@",appDataObj.startDate);
    //NSLog(@"\nEvent UUID: %@",appDataObj.UUID);
    //NSLog(@"\nEvent calendar: %@",appDataObj.calendar);
    //NSLog(@"\nEvent calendarIdentifier: %@",appDataObj.calendar.calendarIdentifier);
    //NSLog(@"\nEvent calendar Identifier: %@",appDataObj.calendarItemIdentifier);
   // NSLog(@"\nEvent ExternalIdentifier : %@",appDataObj.calendarItemExternalIdentifier);
    //NSLog(@"\nConverted StartDate: %@",strStrtDate);
    //NSLog(@"\nlocation: %@",appDataObj.location);
    

    if(appDataObj.location == nil)appDataObj.location = @"";
   if(appDataObj.notes == nil)appDataObj.notes = @"";
    
	const char *sql;
	if(updateAppointmentData == nil) {
		
        sql = "Update AppointmentsTable Set CalendarID = ?, StartDateTime = ?, DueDateTime = ?, ShortNotes = ?, AppointmentTitle = ?, textLocName= ? Where Relation_Field1 = ?";
 
        //  , isAllDay = ?sql = "Update AppointmentsTable Set AppointmentTitle = ? Where Relation_Field1 = \"EEC867DF-7C36-4D6D-9C86-AFDD8A0676EB:16E8A8FF-9242-4259-9C17-5A09D1D0D858\"";

	///	NSString* StrSql = [[NSString alloc] initWithFormat:@"Update AppointmentsTable Set CalendarID = %d, StartDateTime = '%@', DueDateTime = '%@', ShortNotes = '%@', AppointmentTitle = '%@', textLocName= '%@' , isAllDay = '%c' Where Relation_Field1 = '%@'",1,strStrtDate,strEndDate,appDataObj.notes,appDataObj.title,appDataObj.location,appDataObj.isAllDay,appDataObj.eventIdentifier];
					//LocationID = ?, Repeat = ?, Alert = ?,AppointmentType = ?,AlertSecond = ?, AppointmentBinary = ?, 
		//if(sqlite3_prepare_v2(appDelegate.database, [StrSql UTF8String], -1, &updateAppointmentData, NULL) == SQLITE_OK)
        
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateAppointmentData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
    sqlite3_bind_int(updateAppointmentData, 1, 1);
	
	sqlite3_bind_text(updateAppointmentData, 2, [strStrtDate UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 3, [strEndDate UTF8String], -1, SQLITE_TRANSIENT);
	
	//sqlite3_bind_int(updateAppointmentData, 4, 0);// appDataObj.locID);
	
	//sqlite3_bind_text(updateAppointmentData, 5, [appDataObj.repeat  UTF8String], -1, SQLITE_TRANSIENT);
	
	//sqlite3_bind_text(updateAppointmentData, 6, [appDataObj.alert UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 4, [appDataObj.notes UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 5, [appDataObj.title UTF8String], -1, SQLITE_TRANSIENT);
	/*
	//sqlite3_bind_text(updateAppointmentData, 9, [appDataObj.appointmentType UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_blob(updateAppointmentData, 10, [appDataObj.appointmentBinary bytes], [appDataObj.appointmentBinary length], SQLITE_TRANSIENT);
	*/
	sqlite3_bind_text(updateAppointmentData, 6, [appDataObj.location UTF8String], -1, SQLITE_TRANSIENT);
    
	
//int s7=		sqlite3_bind_int(updateAppointmentData, 7, [appDataObj isAllDay]);
		//sqlite3_bind_text(updateAppointmentData, 13, [appDataObj.alertSecond UTF8String], -1, SQLITE_TRANSIENT);*/
    sqlite3_bind_text(updateAppointmentData, 7, [appDataObj.eventIdentifier UTF8String], -1, SQLITE_TRANSIENT);
		        
   
	if(SQLITE_DONE != sqlite3_step(updateAppointmentData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
		return NO;
	} else {
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		// NSInteger locationID = sqlite3_last_insert_rowid(database);
		sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
		return YES;
	}
	return YES;
}
+(BOOL)deleteAppointmentWithEventIdentifier :(EKEvent *)EventIdentifier {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    EKEvent *appDataObj = EventIdentifier;
   /* NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
	
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSString* strStrtDate = [dateFormatter stringFromDate:appDataObj.startDate];
*/
    
    sqlite3_stmt *deleteStmt = nil;
    
	
	if(deleteStmt == nil  && appDelegate.database) {
		const char *sql = "delete from AppointmentsTable where AppointmentTitle =?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
        
    sqlite3_bind_text(deleteStmt, 1, [appDataObj.title UTF8String], -1,SQLITE_TRANSIENT);
    //sqlite3_bind_text(deleteStmt, 2, [strStrtDate UTF8String], -1,SQLITE_TRANSIENT);
	}
    else {
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
	}
	
	if (SQLITE_DONE != sqlite3_step(deleteStmt))
    {
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_reset(deleteStmt);
		return NO;
	}
    else
    {
		sqlite3_reset(deleteStmt);
		
		return YES;
	}
	return YES;
}

+(BOOL)deleteAppointmentWithID :(NSInteger )appointmentsID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *deleteStmt = nil;
	
	if(deleteStmt == nil) {
		const char *sql = "delete from AppointmentsTable where AppointmentID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteStmt, 1, appointmentsID);
	
	if (SQLITE_DONE != sqlite3_step(deleteStmt))
    {
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_reset(deleteStmt);
		return NO;
	}
    else 
    {
		sqlite3_reset(deleteStmt);
		
		return YES;
	}
	return YES;
}


+(ProjectsDataObject *)getProjectDataObjectForProjectID:(NSInteger) projectID {
	ProjectsDataObject *projectDataObject;
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *selectProjectData = nil;
	
	if(selectProjectData == nil && appDelegate.database) {
		NSString *tempString  = [[NSString alloc] initWithFormat:@"Select ProjectID, ProjectName From ProjectMaster Where ProjectID = %d", projectID];
		
		const char* Sql = [tempString UTF8String];
             
		
		if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectProjectData, NULL) == SQLITE_OK) {
			if(sqlite3_step(selectProjectData) == SQLITE_ROW) {
				projectDataObject = [[ProjectsDataObject alloc] initWithProjectId:sqlite3_column_int(selectProjectData, 0) projectTitle:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectProjectData, 1)] ];
			}
			sqlite3_finalize(selectProjectData);		
			selectProjectData = nil;
		}
	}
	return projectDataObject;
}
+(int)getCountRecords:(NSString*)_query
{
	OrganizerAppDelegate * appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *query;
	query =_query;
	//query = [NSString stringWithFormat:@"select ID from %@ where NAME='\%@\'",table,name];  
	const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
	sqlite3_stmt *statement = nil;
	int retID=-1;
	
    if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &statement, NULL)!= SQLITE_OK)
	{
		NSAssert1(0,@"error preparing statement",sqlite3_errmsg(appDelegate.database));
	}
	else
	{
		
		while (sqlite3_step(statement) == SQLITE_ROW)
		{
			retID=(int)sqlite3_column_int(statement,0);
			
		}
	}
	sqlite3_finalize(statement);
	//[self closeDatabaseConnection];
	return retID;
}
/*+(NSInteger *)getProjectDataCount {
	
    ProjectsDataObject *projectDataObject;
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *selectProjectData = nil;
	
	if(selectProjectData == nil && appDelegate.database) {
		NSString *tempString  = [[NSString alloc] initWithFormat:@"Select Count(ProjectID) As CountPro  From ProjectMaster"];
		
		const char* Sql = [tempString UTF8String];
        
		
		if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectProjectData, NULL) == SQLITE_OK) {
			if(sqlite3_step(selectProjectData) == SQLITE_ROW) {
				projectDataObject = [[ProjectsDataObject alloc] initWithProjectId:sqlite3_column_int(selectProjectData, 0) projectTitle:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectProjectData, 1)] ];
			}
			sqlite3_finalize(selectProjectData);		
			selectProjectData = nil;
		}
	}
	return projectDataObject;
}
*/

@end





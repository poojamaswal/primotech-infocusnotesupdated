//
//  RepeatDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RepeatDataObject : NSObject {

	NSInteger repeatID;
	float repeatInMinutes;
	BOOL neverRepeat;
	NSString *repeatText;
}

@property(nonatomic, assign) NSInteger repeatID;
@property(nonatomic, assign) float repeatInMinutes;
@property(nonatomic, assign) BOOL neverRepeat;
@property(nonatomic, retain) NSString *repeatText;

-(id)initWithrepeatID:(NSInteger) repeatIDLocal  repeatInMinutes:(float)repeatInMinutesLocal neverRepeat:(BOOL)neverRepeatLocal repeatText:(NSString *)repeatTextLocal;

@end

//
//  AlertDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AlertDataObject : NSObject {
	NSInteger alertID;
	NSString *alertType;
	NSString *alertRingtone;
	float alertBeforeMinutes;
	NSString *alertText;
}
@property(nonatomic, assign) NSInteger alertID;
@property(nonatomic, retain) NSString *alertType;
@property(nonatomic, retain) NSString *alertRingtone;
@property(nonatomic, assign) float alertBeforeMinutes;
@property(nonatomic, retain) NSString *alertText;

-(id)initWithalertID:(NSInteger) alertIDLocal  alertType:(NSString *)alertTypeLocal alertRingtone:(NSString *)alertRingtoneLocal alertBeforeMinutes:(float)alertBeforeMinutesLocal alertText:(NSString *)alertTextLocal;

@end

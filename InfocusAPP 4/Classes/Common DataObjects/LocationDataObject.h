//
//  LocationDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LocationDataObject : NSObject {
	NSInteger locationID;
	NSString *locationPersonName;
	NSString *locationAddress;
	NSString *locationCity;
	NSString *locationState;
	NSString *locationZip;
	BOOL isVerified;
}

@property(nonatomic, assign) NSInteger locationID;
@property(nonatomic, strong) NSString *locationPersonName;
@property(nonatomic, strong) NSString *locationAddress;
@property(nonatomic, strong) NSString *locationCity;
@property(nonatomic, strong) NSString *locationState;
@property(nonatomic, strong) NSString *locationZip;
@property(nonatomic, assign) BOOL isVerified;

-(id)initWithlocationID:(NSInteger) locationIDLocal  locationPersonName:(NSString *)locationPersonNameLocal  locationAddress:(NSString *)locationAddressLocal locationCity:(NSString *)locationCityLocal locationState:(NSString *)locationStateLocal locationZip:(NSString *)locationZipLocal isVerified:(BOOL)isVerifiedLocal;

@end

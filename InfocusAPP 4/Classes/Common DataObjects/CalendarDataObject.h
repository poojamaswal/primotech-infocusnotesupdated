//
//  CalendarDataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarDataObject : NSObject {
	NSInteger calendarID;
	NSString *calendarName;
	float calendarColorRed;
	float calendarColorGreen;
	float calendarColorBlue;
	BOOL isOrganizerCalendar;
	BOOL isDefaultCalendar;
    // ALok Gupta Added 
    NSString *calendarIdentifire;
    BOOL isCalendarImmutable;
    BOOL isAllowsContentModifications;
}

@property(nonatomic, assign) NSInteger calendarID;
@property(nonatomic, strong) NSString *calendarName;
@property(nonatomic, assign) float calendarColorRed;
@property(nonatomic, assign) float calendarColorGreen;
@property(nonatomic, assign) float calendarColorBlue;
@property(nonatomic, assign) BOOL isOrganizerCalendar; 
@property(nonatomic, assign) BOOL isDefaultCalendar;
//ALOK Gupta Added
@property(nonatomic, strong) NSString *calendarIdentifire;
@property(nonatomic, assign) BOOL isCalendarImmutable;
@property(nonatomic,assign) BOOL isAllowsContentModifications;
@property(nonatomic, assign)BOOL isCalendarShowing;//Steve - used so user can select calendars to show
@property(nonatomic, assign) NSString *calendarSource; //Steve - used to find the Calendar Source. i.e. "iCloud"

-(id)initWithCalendarID:(NSInteger) calendarIDLocal calendarName:(NSString*) calendarNameLocal calendarColorRed:(float) calendarColorRedLocal calendarColorGreen:(float) calendarColorGreenLocal calendarColorBlue:(float )calendarColorBlueLocal isOrganizerCalendar:(BOOL)isOrganizerCalendarLocal isDefaultCalendar:(BOOL)isDefaultCalendarLocal;


@end

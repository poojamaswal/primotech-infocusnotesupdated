//
//  CalendarDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarDataObject.h"

@implementation CalendarDataObject
@synthesize calendarID;
@synthesize calendarName;
@synthesize calendarColorRed;
@synthesize calendarColorGreen;
@synthesize calendarColorBlue;
@synthesize isOrganizerCalendar;
@synthesize isDefaultCalendar;
//Alok Gupta Added 
@synthesize calendarIdentifire;
@synthesize isCalendarImmutable;
@synthesize isAllowsContentModifications;


-(id)initWithCalendarID:(NSInteger) calendarIDLocal calendarName:(NSString*) calendarNameLocal calendarColorRed:(float) calendarColorRedLocal calendarColorGreen:(float) calendarColorGreenLocal calendarColorBlue:(float )calendarColorBlueLocal isOrganizerCalendar:(BOOL)isOrganizerCalendarLocal isDefaultCalendar:(BOOL)isDefaultCalendarLocal {

	if (self = [super init]) {
		self.calendarID = calendarIDLocal;
		self.calendarName = calendarNameLocal;
		self.calendarColorRed = calendarColorRedLocal;
		self.calendarColorGreen = calendarColorGreenLocal;
		self.calendarColorBlue = calendarColorBlueLocal;
		self.isOrganizerCalendar = isOrganizerCalendarLocal;
		self.isDefaultCalendar = isDefaultCalendarLocal;
	}
	return self;
}


@end

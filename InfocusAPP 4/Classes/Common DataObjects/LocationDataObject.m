//
//  LocationDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocationDataObject.h"

@implementation LocationDataObject

@synthesize locationID,locationPersonName,locationAddress;
@synthesize locationCity,locationState,locationZip;
@synthesize isVerified;

-(id)initWithlocationID:(NSInteger) locationIDLocal  locationPersonName:(NSString *)locationPersonNameLocal  locationAddress:(NSString *)locationAddressLocal locationCity:(NSString *)locationCityLocal locationState:(NSString *)locationStateLocal locationZip:(NSString *)locationZipLocal isVerified:(BOOL)isVerifiedLocal{
	if (self=[super init]) {
		self.locationID = locationIDLocal;
		self.locationPersonName = locationPersonNameLocal;
		self.locationAddress = locationAddressLocal;
		self.locationCity = locationCityLocal;
		self.locationState = locationStateLocal;
		self.locationZip = locationZipLocal;
		self.isVerified = isVerifiedLocal;
	}
	return self;
}

//-(id) copyWithZone: (NSZone *) zone {
//	LocationDataObject *locDataObject  = [[LocationDataObject allocWithZone:zone] init];
//	locDataObject.locationID = locationID;
//	
//    NSString *locationPersonNameTemp = [locationPersonName copy];
//	locDataObject.locationPersonName = locationPersonNameTemp;
//	[locationPersonNameTemp release];
//	
//    NSString *locationAddressTemp = [locationAddress copy];
//    locDataObject.locationAddress = locationAddressTemp;
//	[locationAddressTemp release];
//	
//	NSString *locationCityTemp = [locationCity copy];
//	locDataObject.locationCity = locationCityTemp;
//	[locationCityTemp release];
//	
//	NSString *locationStateTemp = [locationState copy]; 
//	locDataObject.locationState = locationStateTemp;
//	[locationStateTemp release];
//	
//	NSString *locationZipTemp = [locationZip copy];
//	locDataObject.locationZip = locationZipTemp;
//	[locationZipTemp release];
//	
//	locDataObject.isVerified = isVerified;
//	
//	return locDataObject;
//}


@end

//
//  AllInsertDataMethods.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
#import <Foundation/Foundation.h>
#import "LocationDataObject.h"
#import "AppointmentsDataObject.h"
#import "TODODataObject.h"
#import "ProjectsDataObject.h"
#import "ProjectsDataObject.h"
//Steve added back. It was commented out
#import "NotesDataObject.h"
#import "AddProjectDataObject.h"


@interface AllInsertDataMethods : NSObject
{
	
}

-(id)initWithDatabase:(sqlite3 *)databaseLocal;

+(BOOL)insertLocationDataValues:(LocationDataObject *) locObj;
+(BOOL)insertAppointmentDataValues:(AppointmentsDataObject *) appointmentDataObjLocal;
//+(BOOL)insertCustomRepeatDataValues:(NSMutableArray *) repeatValArray;
+(BOOL)updateAppointmentDataValues:(AppointmentsDataObject *) appointmentDataObjLocal UpdateLargeImage:(BOOL) isLargeImageUpdate; 

//+(BOOL)insertCustomCalendarDataValues:(CalendarDataObject *)calDataObjLocal;

+(BOOL)insertToDoDataValues:(TODODataObject *) toDoDataObjLocal;
+(BOOL)updateToDoDataValues:(TODODataObject *) toDoDataObjLocal;
+(BOOL)updateToDoStarred:(TODODataObject *) toDoDataObjLocal;
+(BOOL)deleteToDoWithID :(NSInteger ) todoID;
+(BOOL)updateToDoTitleOnly:(TODODataObject *) toDoDataObjLocal;

//+(BOOL)insertNotesDataValues:(NotesDataObject *) NotesDataObjLocal;
//+(int)selectMaxNotesID;
//+(BOOL)insertNotesDataMAP:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID;
//+(BOOL)insertNotesDataSKT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID;
//+(BOOL)insertNotesDataIMG:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID;
//+(BOOL)insertNotesDataTXT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID;
//+(BOOL)insertNotesDataHWRT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID;

+(BOOL)insertProjectDataValues:(ProjectsDataObject *)ProjectDataObjLocal;
+(BOOL)updateProjectTitleOnly:(AddProjectDataObject *)ProjectDataObjLocal;
+(BOOL)addProjectOnly:(AddProjectDataObject *)ProjDataObjLocal;
+(BOOL)deleteProject:(NSInteger)fval;
//Steve added for ARC
+(BOOL)insertNotesDataValues:(NotesDataObject *) NotesDataObjLocal;
+(BOOL)deleteNoteWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteHandwritingObjectsWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteImageObjectsWithID :(NSInteger ) noteID ;
+(BOOL)deleteNoteTextObjectsWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteWebObjectsWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteMapObjectsWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteBMObjectsWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteSketchObjectsWithID :(NSInteger ) noteID;

/// Delete Single Image from database
+(BOOL)deleteSingleImageWithID :(NSInteger ) noteID;
+(BOOL)deleteNoteWithTittle :(NSString* ) noteTittle;

+(BOOL)updateNotesTitleOnly:(NSString *)newTittle oldTittle:(NSString *)oldTitle;

@end

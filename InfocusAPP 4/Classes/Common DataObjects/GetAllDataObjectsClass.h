//
//  GetAllDataObjectsClass.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalendarDataObject.h"
#import "LocationDataObject.h"
#import "AppointmentsDataObject.h"
#import "ProjectsDataObject.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
@interface GetAllDataObjectsClass : NSObject  
{

}

+(void) getAppointmentsArrayOrderByDueDate;
+(void) getAppointmentsArrayOrderByStartDate;
//+(NSArray*) getAllLocationData; 
//+(NSArray*) getAllCalendarData;

+(CalendarDataObject*) getCalendarDataObjectForCalendarID:(NSInteger) calendarIDLocal;
//+(BOOL)deleteCalenderWithID :(NSInteger)CalenderID;
+(LocationDataObject*) getLocationDataObjectForLocationID:(NSInteger) locationID;  

//+(UIImage*) getImageForAppointmentID:(NSInteger ) appointmentIDLocal getDisplayImage:(BOOL) isDisplayImage;
+(CalendarDataObject *)getDefaultCalendarDataObject;

+(BOOL)deleteAppointmentWithID :(NSInteger )appointmentsID;
+(BOOL)deleteAppointmentWithEventIdentifier :(EKEvent *)EventIdentifier;
+(BOOL)updateAppointmentData:(EKEvent *) EKEventDataObjLocal;
+(BOOL)AddUpdateAppointmentData:(EKEvent *) EKEventDataObjLocal;
+(NSInteger)getCalendarId:(EKCalendar *)CalObj;

+(ProjectsDataObject *)getProjectDataObjectForProjectID:(NSInteger) projectID;
+(int)getCountRecords:(NSString*)_query;

+(void) getAppointmentsArrayForDay:(NSDate*)selectedDate;

//+(void)selectNotesData;

@end

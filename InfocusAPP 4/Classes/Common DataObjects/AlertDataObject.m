//
//  AlertDataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AlertDataObject.h"


@implementation AlertDataObject
@synthesize alertID;
@synthesize alertType;
@synthesize alertRingtone;
@synthesize alertBeforeMinutes,alertText;

-(id)initWithalertID:(NSInteger) alertIDLocal  alertType:(NSString *)alertTypeLocal alertRingtone:(NSString *)alertRingtoneLocal alertBeforeMinutes:(float)alertBeforeMinutesLocal alertText:(NSString *)alertTextLocal{
	if (self=[super init]) {
	self.alertID=alertIDLocal;
	self.alertRingtone=alertRingtoneLocal;
	self.alertType=alertTypeLocal;
	self.alertBeforeMinutes=alertBeforeMinutesLocal;
	self.alertText=alertTextLocal;
	}
	
	return self;
}
	
//-(id) copyWithZone: (NSZone *) zone {	
//	AlertDataObject *alrtDataObject = [[AlertDataObject allocWithZone:zone] init];
//	alrtDataObject.alertID = alertID;
//	
//	NSString *alertRingtoneTemp = [alertRingtone copy];
//	alrtDataObject.alertRingtone = alertRingtoneTemp;
//	[alertRingtoneTemp release];
//	
//	NSString *alertTypeTemp = [alertType copy];
//	alrtDataObject.alertType = alertType;
//	[alertTypeTemp release];
//	
//	alrtDataObject.alertBeforeMinutes = alertBeforeMinutes;
//	
//	NSString *alertTextTemp = [alertText copy] ; 
// 	alrtDataObject.alertText = alertTextTemp;
//	[alertTextTemp release];
//	
//    return alrtDataObject;
//}

-(void)dealloc{
	[super dealloc];
	[alertType release];
	[alertRingtone release];
	[alertText release];
}
@end


//  AllInsertDataMethods.m
//  Organizer
//  Created by Nilesh Jaiswal on 4/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.


#import "AllInsertDataMethods.h"
#import "OrganizerAppDelegate.h"
@implementation AllInsertDataMethods

-(id)initWithDatabase:(sqlite3 *)databaseLocal {
	if((self = [super init])) {
	}
	return self;
}

+(BOOL)insertLocationDataValues:(LocationDataObject *)locObj{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *insertLocationData=nil;
	if(insertLocationData == nil && appDelegate.database) {
		const char *sql = "insert into LocationTable(LocationPersonName, LocationAddress, LocationCity, LocationState, LocationZIP, IsVerified) Values(?, ?, ?, ?, ?, ?)";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertLocationData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	
	sqlite3_bind_text(insertLocationData, 1, [locObj.locationPersonName UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertLocationData, 2, [locObj.locationAddress UTF8String], -1, SQLITE_TRANSIENT);
    
	sqlite3_bind_text(insertLocationData, 3, [locObj.locationCity UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertLocationData, 4, [locObj.locationState UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertLocationData, 5, [locObj.locationZip UTF8String], -1, SQLITE_TRANSIENT);
	NSString* aVertified;
    if(locObj.isVerified==YES)
    {
        aVertified=@"YES";
    }
    else
    {
        aVertified=@"NO";
    }
	sqlite3_bind_text(insertLocationData, 6, [aVertified UTF8String] , -1, SQLITE_TRANSIENT);
	
	
	if(SQLITE_DONE != sqlite3_step(insertLocationData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertLocationData);
		insertLocationData=nil;
		return NO;
	}
	else{
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
        // NSInteger locationID = sqlite3_last_insert_rowid(database);
		sqlite3_finalize(insertLocationData);
		insertLocationData=nil;
	    return YES;
    }
    return YES;
}

+(BOOL)insertAppointmentDataValues:(AppointmentsDataObject *)appointmentDataObjLocal {	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertAppointmentData=nil;
	AppointmentsDataObject *appDataObj = appointmentDataObjLocal;
    
    NSLog(@"appDataObj.alert  --> %@", appDataObj.alert );
	
	if(insertAppointmentData == nil && appDelegate.database) 
    {
		//		CalendarID 1, StartDateTime 2, DueDateTime 3, LocationID 4, Repeat 5, Alert 6, ShortNotes 7, AppointmentTitle 8, AppointmentType 9, AppointmentBinary 10, AppointmentBinaryLarge 11
		
        //Steve added relation_Field1
		const char *sql = "insert into AppointmentsTable(CalendarID, StartDateTime, DueDateTime, LocationID, Repeat, Alert, ShortNotes, AppointmentTitle, AppointmentType, AppointmentBinary, AppointmentBinaryLarge, isAllDay, textLocName,AlertSecond, relation_Field1) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";//Anils Added//
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertAppointmentData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	sqlite3_bind_int(insertAppointmentData, 1, appDataObj.calID);
	
	sqlite3_bind_text(insertAppointmentData, 2, [appDataObj.startDateTime UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertAppointmentData, 3, [appDataObj.dueDateTime UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_int(insertAppointmentData, 4, appDataObj.locID);
	
	sqlite3_bind_text(insertAppointmentData, 5, [appDataObj.repeat  UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertAppointmentData, 6, [appDataObj.alert UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertAppointmentData, 7, [appDataObj.shortNotes UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertAppointmentData, 8, [appDataObj.appointmentTitle UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(insertAppointmentData, 9, [appDataObj.appointmentType UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_blob(insertAppointmentData, 10, [appDataObj.appointmentBinary bytes], [appDataObj.appointmentBinary length], SQLITE_TRANSIENT);
	
	sqlite3_bind_blob(insertAppointmentData, 11, [appDataObj.appointmentBinaryLarge bytes], [appDataObj.appointmentBinaryLarge length], SQLITE_TRANSIENT);
	
	sqlite3_bind_int(insertAppointmentData, 12, appDataObj.isAllDay);
	sqlite3_bind_text(insertAppointmentData, 13, [appDataObj.textLocName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertAppointmentData, 14, [appDataObj.alertSecond UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertAppointmentData, 15, [appDataObj.relation_Field1 UTF8String], -1, SQLITE_TRANSIENT); //Steve added
	
	if(SQLITE_DONE != sqlite3_step(insertAppointmentData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertAppointmentData);
		insertAppointmentData =nil;
		return NO;
	} else {
		// SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		
		sqlite3_finalize(insertAppointmentData);
		[appDelegate.appointmentsArray addObject:appDataObj];
		insertAppointmentData =nil;
		return YES;
	}
	return YES;
}


+(BOOL)updateAppointmentDataValues:(AppointmentsDataObject *) appointmentDataObjLocal UpdateLargeImage:(BOOL) isLargeImageUpdate{
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	AppointmentsDataObject *appDataObj = appointmentDataObjLocal;
	
	sqlite3_stmt *updateAppointmentData = nil;	
	
	if(updateAppointmentData == nil) {
        
		const char *sql;
        
		if (isLargeImageUpdate) {
			sql = "Update AppointmentsTable Set CalendarID = ?, StartDateTime = ?, DueDateTime = ?, LocationID = ?, Repeat = ?, Alert = ?, ShortNotes = ?, AppointmentTitle = ?, AppointmentType = ?, AppointmentBinary = ?, textLocName= ?, AppointmentBinaryLarge = ? , isAllDay = ?, AlertSecond = ? Where relation_Field1 = ?";//AppointmentID
		}else {
			sql = "Update AppointmentsTable Set CalendarID = ?, StartDateTime = ?, DueDateTime = ?, LocationID = ?, Repeat = ?, Alert = ?, ShortNotes = ?, AppointmentTitle = ?, AppointmentType = ?, AppointmentBinary = ?, textLocName= ? , isAllDay = ?, AlertSecond = ?  Where relation_Field1 = ?";
		}
		
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateAppointmentData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
    
    
	
	sqlite3_bind_int(updateAppointmentData, 1, appDataObj.calID);
	
	sqlite3_bind_text(updateAppointmentData, 2, [appDataObj.startDateTime UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 3, [appDataObj.dueDateTime UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_int(updateAppointmentData, 4, appDataObj.locID);
	
	sqlite3_bind_text(updateAppointmentData, 5, [appDataObj.repeat  UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 6, [appDataObj.alert UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 7, [appDataObj.shortNotes UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 8, [appDataObj.appointmentTitle UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 9, [appDataObj.appointmentType UTF8String], -1, SQLITE_TRANSIENT);
	
	sqlite3_bind_blob(updateAppointmentData, 10, [appDataObj.appointmentBinary bytes], [appDataObj.appointmentBinary length], SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateAppointmentData, 11, [appDataObj.textLocName UTF8String], -1, SQLITE_TRANSIENT);
    
	if (isLargeImageUpdate) {
		sqlite3_bind_blob(updateAppointmentData, 12, [appDataObj.appointmentBinaryLarge bytes], [appDataObj.appointmentBinaryLarge length], SQLITE_TRANSIENT);
		
		sqlite3_bind_int(updateAppointmentData, 13, [appDataObj isAllDay]);
		sqlite3_bind_text(updateAppointmentData, 14, [appDataObj.alertSecond UTF8String], -1, SQLITE_TRANSIENT);
		//sqlite3_bind_int(updateAppointmentData, 15, [appDataObj appointmentID]);
        
        sqlite3_bind_text(updateAppointmentData, 15, [appDataObj.relation_Field1 UTF8String], -1, SQLITE_TRANSIENT);
        
	}else {
		sqlite3_bind_int(updateAppointmentData, 12, [appDataObj isAllDay]);
		sqlite3_bind_text(updateAppointmentData, 13, [appDataObj.alertSecond UTF8String], -1, SQLITE_TRANSIENT);
		//sqlite3_bind_int(updateAppointmentData, 14, [appDataObj appointmentID]);
        
        sqlite3_bind_text(updateAppointmentData, 14, [appDataObj.relation_Field1 UTF8String], -1, SQLITE_TRANSIENT);
        
	}
    
	if(SQLITE_DONE != sqlite3_step(updateAppointmentData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
		return NO;
	} else {
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		// NSInteger locationID = sqlite3_last_insert_rowid(database);
        
        int rowEffected =  sqlite3_changes(appDelegate.database);
        sqlite3_finalize(updateAppointmentData);
		updateAppointmentData=nil;
        if(rowEffected ==0){
               [self insertAppointmentDataValues:appDataObj];
        }
        
		return YES;
	}
	return YES;
}


#pragma mark -
#pragma mark ToDo Methods
#pragma mark -

+(BOOL)insertToDoDataValues:(TODODataObject *)toDoDataObjLocal {
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *insertToDoData = nil;
    
	if(insertToDoData == nil && appDelegate.database) {
		const char *sql = "insert into ToDoTable(Title, TitleTextType, TitleBinary, BelongstoCalenderID, BelongstoProjectID, DateStarted, DueDate, DateCreated, DateModified, Progress, Priority, IsStarred, ManuallySortTodoID, TitleBinaryLarge,PerntID, Alert, AlertSecond, isAlertOn) Values(?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?)";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertToDoData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	sqlite3_bind_text(insertToDoData, 1, [[toDoDataObjLocal todoTitle] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertToDoData, 2, [[toDoDataObjLocal todoTitleType] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_blob(insertToDoData, 3, [[toDoDataObjLocal todoTitleBinary] bytes], [[toDoDataObjLocal todoTitleBinary] length], SQLITE_TRANSIENT);
	sqlite3_bind_int(insertToDoData, 4, 1);
	sqlite3_bind_int(insertToDoData, 5,	 [toDoDataObjLocal projID]);
	sqlite3_bind_text(insertToDoData, 6, [[toDoDataObjLocal startDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertToDoData, 7, [[toDoDataObjLocal dueDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertToDoData, 8, [[toDoDataObjLocal createDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertToDoData, 9, [[toDoDataObjLocal modifyDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insertToDoData, 10, [toDoDataObjLocal toDoProgress]);
	sqlite3_bind_text(insertToDoData, 11,[[toDoDataObjLocal priority] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insertToDoData, 12, [toDoDataObjLocal isStarred]);
	sqlite3_bind_int(insertToDoData, 13, [toDoDataObjLocal manuallySortedTodoID]);
	sqlite3_bind_blob(insertToDoData, 14,[[toDoDataObjLocal todoTitleBinaryLarge] bytes], [[toDoDataObjLocal todoTitleBinaryLarge] length], SQLITE_TRANSIENT);
	sqlite3_bind_int(insertToDoData, 15, [toDoDataObjLocal projPrntID]);
	sqlite3_bind_text(insertToDoData, 16,[[toDoDataObjLocal alert] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertToDoData, 17,[[toDoDataObjLocal alertSecond] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(insertToDoData, 18, [toDoDataObjLocal isAlertOn]);
    
    
    //NSLog(@"[toDoDataObjLocal alert] = %@", [toDoDataObjLocal alert]);
    //NSLog(@"[toDoDataObjLocal alertSecond] = %@", [toDoDataObjLocal alertSecond]);
    
    
	if(SQLITE_DONE != sqlite3_step(insertToDoData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertToDoData);
		insertToDoData=nil;
		return NO;
	}
	else{
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		// NSInteger locationID = sqlite3_last_insert_rowid(appDelegate.database);
		sqlite3_finalize(insertToDoData);
		insertToDoData = nil;
		return YES;
	}
	return YES;
}
///Alok added for starred values
+(BOOL)updateToDoStarred:(TODODataObject *) toDoDataObjLocal {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *updateToDoData = nil;
	
	if(updateToDoData == nil) {
		
		const char *sql = "Update ToDoTable Set IsStarred = ? Where TODOID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateToDoData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	
	sqlite3_bind_int(updateToDoData,  1, [toDoDataObjLocal isStarred]);
    sqlite3_bind_int(updateToDoData,  2, [toDoDataObjLocal todoID]);
    
    if(SQLITE_DONE != sqlite3_step(updateToDoData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return NO;
	}
	else{
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return YES;
	}
	return YES;
	
}

+(BOOL)updateToDoDataValues:(TODODataObject *) toDoDataObjLocal {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *updateToDoData = nil;
	
	if(updateToDoData == nil) {
		
		const char *sql = "Update ToDoTable Set Title = ?, TitleTextType = ?, TitleBinary = ?, BelongstoCalenderID = ?, BelongstoProjectID = ?, DateStarted = ?, DueDate = ?, DateCreated = ?, DateModified = ?, Progress = ?, Priority = ?, IsStarred = ?, ManuallySortTodoID = ?, TitleBinaryLarge = ?, Alert = ?, AlertSecond = ?, isAlertOn = ?,PerntID = ? Where TODOID = ?";
        //
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateToDoData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	sqlite3_bind_text(updateToDoData, 1, [[toDoDataObjLocal todoTitle] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(updateToDoData, 2, [[toDoDataObjLocal todoTitleType] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_blob(updateToDoData, 3, [[toDoDataObjLocal todoTitleBinary] bytes], [[toDoDataObjLocal todoTitleBinary] length], SQLITE_TRANSIENT);
	sqlite3_bind_int(updateToDoData,  4, 1);
	sqlite3_bind_int(updateToDoData,  5, [toDoDataObjLocal projID]);
	sqlite3_bind_text(updateToDoData, 6, [[toDoDataObjLocal startDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(updateToDoData, 7, [[toDoDataObjLocal dueDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(updateToDoData, 8, [[toDoDataObjLocal createDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(updateToDoData, 9, [[toDoDataObjLocal modifyDateTime] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(updateToDoData,  10, [toDoDataObjLocal toDoProgress]);
	sqlite3_bind_text(updateToDoData, 11, [[toDoDataObjLocal priority] UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(updateToDoData,  12, [toDoDataObjLocal isStarred]);
	sqlite3_bind_int(updateToDoData,  13, [toDoDataObjLocal manuallySortedTodoID]);
	sqlite3_bind_blob(updateToDoData, 14, [[toDoDataObjLocal todoTitleBinaryLarge] bytes], [[toDoDataObjLocal todoTitleBinaryLarge] length], SQLITE_TRANSIENT);
	
	sqlite3_bind_text(updateToDoData, 15, [[toDoDataObjLocal alert] UTF8String], -1, SQLITE_TRANSIENT); // Anil's Addition
    sqlite3_bind_text(updateToDoData, 16, [[toDoDataObjLocal alertSecond] UTF8String], -1, SQLITE_TRANSIENT); // Anil's Addition
    sqlite3_bind_int(updateToDoData,  17, [toDoDataObjLocal isAlertOn]); // Anil's Addition
    sqlite3_bind_int(updateToDoData,  18, [toDoDataObjLocal projPrntID]); // Anil's Addition
    
    sqlite3_bind_int(updateToDoData,  19, [toDoDataObjLocal todoID]);
    
    if(SQLITE_DONE != sqlite3_step(updateToDoData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return NO;
	}
	else{
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return YES;
	}
	return YES;
	
}

+(BOOL)updateToDoTitleOnly:(TODODataObject *) toDoDataObjLocal {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *updateToDoData = nil;
	
	if(updateToDoData == nil) {
		
		const char *sql = "Update ToDoTable Set Title = ?, DateModified = ?,Progress = ?, Alert = ?, AlertSecond = ?, isAlertOn = ?,TitleBinary = ?, TitleBinaryLarge = ?, TitleTextType = ? Where TODOID = ?";
        //
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateToDoData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	sqlite3_bind_text(updateToDoData, 1, [[toDoDataObjLocal todoTitle] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateToDoData, 2, [[toDoDataObjLocal modifyDateTime] UTF8String], -1, SQLITE_TRANSIENT);	
    sqlite3_bind_int(updateToDoData, 3, [toDoDataObjLocal toDoProgress]);
    
    
    sqlite3_bind_text(updateToDoData, 4, [[toDoDataObjLocal alert] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateToDoData, 5, [[toDoDataObjLocal alertSecond]UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(updateToDoData, 6, [toDoDataObjLocal isAlertOn]);
    
    //NSLog(@"[toDoDataObjLocal alert] = %@", [toDoDataObjLocal alert]);
    //NSLog(@"[toDoDataObjLocal alertSecond] = %@", [toDoDataObjLocal alertSecond]);
    
    
    sqlite3_bind_blob(updateToDoData, 7, [[toDoDataObjLocal todoTitleBinary] bytes], [[toDoDataObjLocal todoTitleBinary] length], SQLITE_TRANSIENT);
    sqlite3_bind_blob(updateToDoData, 8, [[toDoDataObjLocal todoTitleBinaryLarge] bytes], [[toDoDataObjLocal todoTitleBinaryLarge] length], SQLITE_TRANSIENT);
    sqlite3_bind_text(updateToDoData, 9, [[toDoDataObjLocal todoTitleType] UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_int(updateToDoData,  10, [toDoDataObjLocal todoID]);
    
    if(SQLITE_DONE != sqlite3_step(updateToDoData)){
		NSAssert1(0, @"Error while Updating Todo Title data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return NO;
	}
	else{
		sqlite3_finalize(updateToDoData);
		updateToDoData = nil;
		return YES;
	}
	return YES;
	
}

+(BOOL)updateProjectTitleOnly:(AddProjectDataObject *)ProjDataObjLocal {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *updateProjData = nil;
	
	if(updateProjData == nil) {
		
		const char *sql = "Update ProjectMaster Set projectName = ? , modifyDate = ? Where projectID = ?";
        //
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateProjData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	sqlite3_bind_text(updateProjData, 1, [[ProjDataObjLocal projectname] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateProjData, 2, [[ProjDataObjLocal modifyDate] UTF8String], -1, SQLITE_TRANSIENT);	
    
    sqlite3_bind_int(updateProjData,  3, [ProjDataObjLocal projectid]);
    
    if(SQLITE_DONE != sqlite3_step(updateProjData)){
		NSAssert1(0, @"Error while Updating Project data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateProjData);
		updateProjData = nil;
		return NO;
	}
	else{
		sqlite3_finalize(updateProjData);
		updateProjData = nil;
		return YES;
	}
	return YES;
	
}
+(BOOL)addProjectOnly:(AddProjectDataObject *)ProjDataObjLocal {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *updateProjData = nil;
    NSDate *today1 = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString11 = [dateFormat stringFromDate:today1];
    NSLog(@"date: %@", dateString11);
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
    NSDate *_date = [NSDate date];
    NSString *modifydateStr = [dateFormatter stringFromDate:_date];
    
	if(updateProjData == nil) {
		
		const char *sql = "insert into ProjectMaster(projectName,projectPriority,creationDate,belongstoID,level,folderImage,modifyDate)Values(?,?,?,?,?,?,?)";
        //, DateModified = ?
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateProjData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while update statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	sqlite3_bind_text(updateProjData, 1, [[ProjDataObjLocal projectname] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateProjData,2,[[NSString stringWithFormat:@"%@", @"None"] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateProjData,3,[dateString11 UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(updateProjData,  4, 0);
    sqlite3_bind_int(updateProjData, 5, 0);
    sqlite3_bind_text(updateProjData,6,[[NSString stringWithFormat:@"%@", @"FolderColor2.png"] UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(updateProjData,7,[modifydateStr UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
    if(SQLITE_DONE != sqlite3_step(updateProjData)){
		NSAssert1(0, @"Error while Adding Project Name data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(updateProjData);
		updateProjData = nil;
		return NO;
	}
	else{
		sqlite3_finalize(updateProjData);
		updateProjData = nil;
		return YES;
	}
	return YES;
	
}
+(BOOL)deleteProject:(NSInteger)fval
{
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *deleteproject= nil;
    NSString *sql =@"";
	if(deleteproject ==nil)
	{   sql = [[NSString alloc]initWithFormat:@"select projectID from ProjectMaster where belongstoId = %d or projectID =%d ",fval,fval];
		
        
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
		{
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		else{
			
			while(sqlite3_step(deleteproject) == SQLITE_ROW) 
			{
				NSInteger Bval=sqlite3_column_int(deleteproject, 0);
				NSLog(@"\nBval--->%d",Bval);
                sqlite3_stmt *queryStmt= nil;
                sql = [[NSString alloc]initWithFormat:@"delete from ProjectMaster where projectID = %d",Bval];
                if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &queryStmt, NULL) == SQLITE_OK)
                    sqlite3_step(queryStmt);
                queryStmt= nil;
                //Alok Added for Delete Notes 
                sql = [[NSString alloc]initWithFormat:@"delete from NotesMaster where BelongstoProjectID = %d",Bval];
                if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &queryStmt, NULL) == SQLITE_OK)
                    sqlite3_step(queryStmt);
                queryStmt= nil;  
                //Alok Added for Delete TODOs 
                sql = [[NSString alloc]initWithFormat:@"delete from ToDoTable where BelongstoProjectID = %d",Bval];
                if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &queryStmt, NULL) == SQLITE_OK)
                    sqlite3_step(queryStmt);
                queryStmt= nil; 
                
                //[self deleteProject:Bval];
			}
			
			sqlite3_finalize(deleteproject);		
			deleteproject = nil;        
		}
        
	}
    return YES;
}

+(BOOL)deleteToDoWithID :(NSInteger ) todoID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteToDoData = nil;
	
	if(deleteToDoData == nil) {
		const char *sql = "delete from ToDoTable where TODOID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteToDoData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteToDoData, 1, todoID);
	
	if (SQLITE_DONE != sqlite3_step(deleteToDoData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteToDoData);
		return NO;
	} else {
		sqlite3_finalize(deleteToDoData);
		return YES;
	}
	return YES;
}

#pragma mark Delete Note methods

+(BOOL)deleteNoteWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesMaster where NoteID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

+(BOOL)deleteNoteHandwritingObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesHwrtInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}


+(BOOL)deleteNoteImageObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesImgInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}


+(BOOL)deleteNoteTextObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesTextInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

+(BOOL)deleteNoteWebObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesWebInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

+(BOOL)deleteNoteMapObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesMapInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

+(BOOL)deleteNoteBMObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesBMInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}
+(BOOL)deleteNoteSketchObjectsWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesSktInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

+(BOOL)deleteSingleImageWithID :(NSInteger ) noteID {
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deletenoteData = nil;
	
	if(deletenoteData == nil) {
		const char *sql = "delete from NotesImgInfo where ImgID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deletenoteData, 1, noteID);
	
	if (SQLITE_DONE != sqlite3_step(deletenoteData)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deletenoteData);
		return NO;
	} else {
		sqlite3_finalize(deletenoteData);
		return YES;
	}
	return YES;
}

#pragma mark Delete Note methods end


+(int)selectMaxNotesID
{
	NSInteger nID;
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		NSString *Sql = [[NSString alloc] initWithString:@"SELECT MAX(NoteID)  FROM  NotesMaster"];
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			if(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				
				nID=sqlite3_column_int(selectNotes_Stmt, 0);
				
			}			//				        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
			//[aSubArray release];
		}
		//[Sql release];
	}
	
	return nID;
}



+(BOOL)insertNotesDataMAP:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
	sqlite3_stmt *insertNotesData = nil;
	
	NSLog(@"ID === %d", ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal ;
    
	int total=[[notesDataObj  notesMapDataArray] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesMapInfo(NotesID,MapInfo)Values(?,?) ";
			
		    if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		
		sqlite3_bind_blob(insertNotesData, 2, [[notesDataObj.notesMapDataArray objectAtIndex:i] bytes], [[notesDataObj.notesMapDataArray objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		
		//sqlite3_bind_text(insertNotesData, 2,[[notesDataObj.notesMapDataArray objectAtIndex:i]UTF8String], -1, SQLITE_TRANSIENT);
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
			//[notesDataObj release];
			return NO;
		} 
		else 
		{
			sqlite3_reset(insertNotesData);
			insertNotesData =nil;
		}
	}
	//[notesDataObj release];
	return YES;
}


/* Image Insert Function */

+(BOOL)insertNotesDataIMG:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	NSLog(@"ID=== %d",ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal;
	
	int total=[[notesDataObj  notesPicDataArray] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesImgInfo(NotesID,ImgInfo)Values(?,?) ";
			
		    if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		
		sqlite3_bind_blob(insertNotesData, 2, [[notesDataObj.notesPicDataArray objectAtIndex:i] bytes], [[notesDataObj.notesPicDataArray objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		
		//sqlite3_bind_text(insertNotesData, 2,[[notesDataObj.notesMapDataArray objectAtIndex:i]UTF8String], -1, SQLITE_TRANSIENT);
		
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
			//[notesDataObj release];
			return NO;
		} 
		else 
		{
			
			sqlite3_reset(insertNotesData);
			
			insertNotesData =nil;
			
		}
		
	}
	
	//[notesDataObj release];
	
	return YES;
}



/* end of Img Insert Function */




/* Sketch Insert Function */

+(BOOL)insertNotesDataSKT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	NSLog(@"ID=== %d",ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal;
	
	int total=[[notesDataObj  notessktchimageArrsy] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesSktInfo(NotesID,SktInfo)Values(?,?) ";
			
		    if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		
		sqlite3_bind_blob(insertNotesData, 2, [[notesDataObj.notessktchimageArrsy objectAtIndex:i] bytes], [[notesDataObj.notessktchimageArrsy objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		
		//sqlite3_bind_text(insertNotesData, 2,[[notesDataObj.notesMapDataArray objectAtIndex:i]UTF8String], -1, SQLITE_TRANSIENT);
		
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
			//[notesDataObj release];
			return NO;
		} 
		else 
		{
			
			sqlite3_reset(insertNotesData);
			
			insertNotesData =nil;
			
		}
		
	}
	
	//[notesDataObj release];
	
	return YES;
}



/* end of Sketch Insert Function */
/*------------------------------------------------*/
/* Insert Web URL */


+(BOOL)insertNotesDataweb:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	NSLog(@"ID=== %d",ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal ;
	
	int total=[[notesDataObj  notesWebURArray] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesWebInfo(NotesID,WebClipInfo)Values(?,?)";
			
			if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		//sqlite3_bind_text(insertNotesData, 2,[[notesDataObj.notesWebURArray objectAtIndex:i]UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_blob(insertNotesData, 2, [[notesDataObj.notesWebURArray objectAtIndex:i] bytes], [[notesDataObj.notesWebURArray objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
            //	[notesDataObj release];
			return NO;
		} 
		else 
		{
			
			sqlite3_reset(insertNotesData);
			
			insertNotesData =nil;
			
		}
		
	}
	
    //	[notesDataObj release];
	
	return YES;
}











/* End of method for web URL*/


//------------------------------------------------

/* Insert text Data into textInfo Table Method */
+(BOOL)insertNotesDataTXT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	NSLog(@"ID=== %d",ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal ;
	
	int total=[[notesDataObj notesTextArray] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesTextInfo(NotesID,TextInfo)Values(?,?)";
			
			if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		sqlite3_bind_text(insertNotesData, 2,[[notesDataObj.notesTextArray objectAtIndex:i]UTF8String], -1, SQLITE_TRANSIENT);
		
		//sqlite3_bind_blob(insertNotesData, 2, [[notesDataObj.notessktchimageArrsy objectAtIndex:i] bytes], [[notesDataObj.notessktchimageArrsy objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		
		
		
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
			//[notesDataObj release];
			return NO;
		} 
		else 
		{
			
			sqlite3_reset(insertNotesData);
			
			insertNotesData =nil;
			
		}
		
	}
	
	//[notesDataObj release];
	
	return YES;
}

/* End of the Method*/

//----------------------------------------------

+(BOOL)insertNotesDataHWRT:(NotesDataObject *) NotesDataObjLocal withNID:(int)ntID
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	NSLog(@"ID=== %d",ntID);
	
	NotesDataObject *notesDataObj = NotesDataObjLocal ;
	
	int total=[[notesDataObj notesHwrtDataArray] count];
	
	NSLog(@"Total Value Is = %d",total);
	
	
	for(int i=0;i<total;i++)
	{	
		NSLog(@"%d",i);
		
		if(insertNotesData == nil && appDelegate.database) 
		{
			
			const char *sql = "insert into NotesHwrtInfo(NotesID,HwertInfo)Values(?,?)";
			
			if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		
		sqlite3_bind_int(insertNotesData, 1,ntID);	
		
		
		
		sqlite3_bind_blob(insertNotesData, 2,[[notesDataObj.notesHwrtDataArray objectAtIndex:i] bytes], [[notesDataObj.notesHwrtDataArray objectAtIndex:i] length], SQLITE_TRANSIENT);
		
		
		
		
		
		if(SQLITE_DONE != sqlite3_step(insertNotesData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertNotesData);
			insertNotesData =nil;
			//[notesDataObj release];
			return NO;
		} 
		else 
		{
			
			sqlite3_reset(insertNotesData);
			
			insertNotesData =nil;
			
		}
		
	}
	
	//[notesDataObj release];
	return YES;
}




/*   insertNotesBMdata     */





+(BOOL)insertNotesDataValues:(NotesDataObject *) NotesDataObjLocal
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *insertNotesData=nil;
	
	NotesDataObject *notesDataObj = NotesDataObjLocal;
	if(insertNotesData == nil && appDelegate.database) 
    {
		
		const char *sql = "insert into NotesMaster(NoteTitle,NotesTitleType,NotesTitleBinary,NotesTitleBinaryLarge,BelongstoProjectID)Values(?,?,?,?,?)";
		
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &insertNotesData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	//NSLog(@"%@",notesDataObj.notesPicBinaryimg);
	sqlite3_bind_text(insertNotesData, 1, [notesDataObj.notesTitle UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertNotesData, 2, [notesDataObj.notesTitleType UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_blob(insertNotesData, 3,[notesDataObj.notesTitleBinary  bytes], [notesDataObj.notesTitleBinary length], SQLITE_TRANSIENT);
	sqlite3_bind_blob(insertNotesData, 4,[notesDataObj.notesTitleBinaryLarge  bytes], [notesDataObj.notesTitleBinaryLarge length], SQLITE_TRANSIENT);
	sqlite3_bind_int(insertNotesData, 5, notesDataObj.belongsToProjectID);
    
	if(SQLITE_DONE != sqlite3_step(insertNotesData)){
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertNotesData);
		insertNotesData =nil;
		//[notesDataObj release];
		return NO;
	} else {
		
		sqlite3_finalize(insertNotesData);
		[appDelegate.appointmentsArray addObject:notesDataObj];
		insertNotesData =nil;
		//[notesDataObj release];
		
		NSInteger nodeVal = [AllInsertDataMethods selectMaxNotesID];
		
		NSLog(@"=====%d",nodeVal);
		
		
        NSInteger AA = [AllInsertDataMethods insertNotesDataMAP:notesDataObj withNID:nodeVal];
		NSLog(@"%d",AA);
		
        NSInteger bb = [AllInsertDataMethods insertNotesDataIMG:notesDataObj withNID:nodeVal];
		NSLog(@"%d",bb);
		
		
		
        NSInteger cc = [AllInsertDataMethods insertNotesDataSKT:notesDataObj withNID:nodeVal];
		NSLog(@"%d",cc);
		
		NSInteger dd = [AllInsertDataMethods insertNotesDataweb:notesDataObj withNID:nodeVal];
		NSLog(@"%d",dd);
		
		NSInteger ee = [AllInsertDataMethods insertNotesDataTXT:notesDataObj withNID:nodeVal];
		NSLog(@"%d",ee);
		
		NSInteger ff = [AllInsertDataMethods insertNotesDataHWRT:notesDataObj withNID:nodeVal];
		
		NSLog(@"%d",ff);
		
		
		
		return YES;
	}	
	
	//[notesDataObj release];
	
	
	return YES;	
	
	
}


+(BOOL)insertProjectDataValues:(ProjectsDataObject *)ProjectDataObjLocal
{
	
	
	
	//NSLog(@"%@",notesDataObj.notesPicBinaryimg);
	/*	sqlite3_bind_text(insertProjectData, 1, [projectDataObj.projectName UTF8String], -1, SQLITE_TRANSIENT);
	 sqlite3_bind_text(insertProjectData, 2, [projectDataObj. UTF8String], -1, SQLITE_TRANSIENT);
	 
	 sqlite3_bind_text(insertProjectData, 3, [projectDataObj. UTF8String], -1, SQLITE_TRANSIENT);
	 sqlite3_bind_text(insertProjectData, 4, [projectDataObj.notesTitleType UTF8String], -1, SQLITE_TRANSIENT);
	 sqlite3_bind_text(insertProjectData, 5, [projectDataObj.notesTitleType UTF8String], -1, SQLITE_TRANSIENT);
	 
	 
	 if(SQLITE_DONE != sqlite3_step(insertProjectData)){
	 NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
	 sqlite3_finalize(insertProjectData);
	 insertProjectData =nil;
	 [notesDataObj release];
	 return NO;
	 } else {
	 
	 sqlite3_finalize(insertProjectData);
	 [appDelegate.appointmentsArray addObject:notesDataObj];
	 insertProjectData =nil;
	 [notesDataObj release];
	 
	 
	 return YES;
	 }	
	 
	 [notesDataObj release];
	 
	 
	 return YES;	
	 
	 */
	return YES;
}


+(BOOL)updateNotesTitleOnly:(NSString *)newTittle oldTittle:(NSString* )oldTitle {
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    sqlite3_stmt *updateProjData = nil;
    
    if(updateProjData == nil) {
        
        const char *sql = "Update NotesMaster Set NoteTitle = ? Where NoteTitle = ?";
        //
        if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &updateProjData, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while update statement. '%s'", sqlite3_errmsg(appDelegate.database));
    }
    
    sqlite3_bind_text(updateProjData, 1, [newTittle UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_bind_text(updateProjData, 2, [oldTitle UTF8String], -1, SQLITE_TRANSIENT);
    
    
    if(SQLITE_DONE != sqlite3_step(updateProjData)){
        NSAssert1(0, @"Error while Updating Project data. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_finalize(updateProjData);
        updateProjData = nil;
        return NO;
    }
    else{
        sqlite3_finalize(updateProjData);
        updateProjData = nil;
        return YES;
    }
    return YES;
    
}
+(BOOL)deleteNoteWithTittle :(NSString* ) noteTittle
{
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    sqlite3_stmt *deletenoteData = nil;
    
    if(deletenoteData == nil) {
        const char *sql = "Delete from NotesMaster where NoteTitle = ?";
        if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deletenoteData, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
    }
    //When binding parameters, index starts from 1 and not zero.
    sqlite3_bind_text(deletenoteData, 1, [noteTittle UTF8String], -1, SQLITE_TRANSIENT);
    
    if (SQLITE_DONE != sqlite3_step(deletenoteData)){
        NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_finalize(deletenoteData);
        return NO;
    } else {
        sqlite3_finalize(deletenoteData);
        return YES;
    }
    return YES;
}


@end

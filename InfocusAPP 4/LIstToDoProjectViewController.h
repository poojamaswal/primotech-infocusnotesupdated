//
//  LIstToDoProjectViewController.h
//  Organizer
//
//  Created by alok gupta on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectListViewController.h"

//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"
///Alok Added
#import "AddProjectDataObject.h"

#import "ProjectSelectionDelegate.h"

@class Cal_Todo_OptionsVC;
@class OrganizerAppDelegate;
@class HomeViewController;
@class ProjectListViewController;

//Steve changed was: @protocol ToDoParentControllerDelegate
@protocol LIstToDoProjectViewController
-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId todoID:(NSInteger) todoID prentID:(NSInteger)prentID; 
-(void)setStartDate:(NSString*)aStartDate todoID:(NSInteger)todoid;
-(void)setEndDate:(NSString*)aEndDate todoID:(NSInteger)todoid;


@end

@interface LIstToDoProjectViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>
{
	IBOutlet UITableView  *toDoTableView;

	NSInteger		projectIDFromProjMod;
	int ProjId;
	NSString *ProjTitle;
    
    id delegate;
    NSInteger retVal;
    NSInteger y;
    NSInteger x;
    NSInteger u;
    NSInteger v;
    
    NSString *yy;
    NSString *xx;
    NSString *uu;
    NSString *vv;
    NSString *Pname;
    NSString *Fstr;
    NSMutableArray *myArray;
    
    //Steve added 4 items
    UIColor     *myBackgroundColor;
    UIColor     *myFontColor;
    UIFont      *myFontName;
    UIColor     *myCellBackgroundColor;
    UIColor     *myBadgeColor;
    BOOL        isKeyBoardDone;
     
    int indexWhereCellIsBeingInserted;  // This saves the index where cell gets creating with pinch effect
    
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIView *helpView; //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;
    //AdWhirlView *adView;
    
    ///ALok Added
    NSMutableArray *getBIDArray;
    NSString *sql;
    NSMutableArray	*ProjectDataObjArray;
    NSMutableArray *arrayM_OriginalData;
    AddProjectDataObject *projDataObj;
    NSInteger totalTODO;
    BOOL isEditingProj;
    IBOutlet UIToolbar * bottomBar; 
    SearchBarViewController			*searchBarViewController;
    IBOutlet UIButton *homeBtn;
    IBOutlet UIButton *noteBtn;
    IBOutlet UIButton *todoBtn;
    IBOutlet UIButton *projectBtn;
    
    IBOutlet UIView *upgradeView; //Steve
    IBOutlet UIScrollView *upgradeScrollView; //Steve
    IBOutlet UINavigationBar *navBarUpgradeView; //Steve
    
    BOOL                    isMenuClosed; //Steve
    IBOutlet UIView *ListHelpDragView;


    
}
//Alok Added
@property(nonatomic,strong)AddProjectDataObject *projectdataobj ;
@property(nonatomic,strong)NSMutableArray * ProjectDataObjArray;
-(NSInteger)findTODOTotal:(NSInteger)valueTODO;
-(void)delete_Project:(NSInteger)fval;
-(void)dum:(NSMutableArray *)faa;
-(void)todo_Find:(NSInteger)bValue;
-(void)update_todo:(NSInteger)CValue;
-(void)delete_notes:(NSInteger)CValue;


- (IBAction)notesButtonClicked:(id)sender;
//@property(nonatomic,retain)AdWhirlView *adView;
//Steve added 4 items
@property(nonatomic,strong)UIColor *myBackgroundColor; 
@property(nonatomic,strong)UIColor *myFontColor; 
@property(nonatomic, strong) UIFont *myFontName;
@property(nonatomic, strong)  UIColor     *myCellBackgroundColor;


@property(nonatomic, strong) UITableView *toDoTableView;
//@property(assign)int  selectedButtonIndex; 
//@property (nonatomic, strong) NSData *todoTitleBinary;
//@property (nonatomic, strong) NSData *todoTitleBinaryLarge;

//Steve added
- (void) keyBoardDone: (id)sender;

- (IBAction)addToDoButton_Clicked:(id)sender;
- (IBAction)calendarButton_Clicked:(id)sender;
- (IBAction)homeButton_Clicked:(id)sender;
- (IBAction)ProjectButton_click:(id)sender;


//-(IBAction)	project_buttonClickedNow:(id)sender;
- (IBAction)helpView_Tapped:(id)sender;  //Steve


//- (id)initWithProjectID:(NSInteger ) projId;
//-(IBAction) startsEnds_buttonClicked:(id)sender;
//-(NSDate *)convertStringToDate:(NSString *)str_Date;
-(void)goToHandwritingView;
-(void)createAddView;

- (IBAction)searchBarButton_Clicked:(id)sender;
- (IBAction)mailBarButton_Clicked:(id)sender;
- (IBAction)settingBarButton_Clicked:(id)sender;
- (IBAction)infoBarButton_Clicked:(id)sender;

- (IBAction)cancelUpgradeButton_Clicked:(id)sender; //Steve
- (IBAction)upgradeButton_Clicked:(id)sender; //Steve


@property (nonatomic, assign) id<ProjectSelectionDelegate> delegate;

@end

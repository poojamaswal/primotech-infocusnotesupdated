
#import <UIKit/UIKit.h>

@interface RichTextEditorToggleButton : UIButton

@property (nonatomic, assign) BOOL on;

- (id)init;

@end

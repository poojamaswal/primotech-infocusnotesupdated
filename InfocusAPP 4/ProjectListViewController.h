//
//  ProjectListViewController.h
//  Organizer
//
//  Created by Tarun on 6/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectsDataObject.h"
#import "MyTreeNode.h"
#import "ToDoViewController.h"
#import "ToDoParentController.h"

@interface ProjectListViewController : UIViewController { //<ToDoViewControllerDelegate>{
	IBOutlet UITableView	*projectTblVw;
    UIImageView				*tickImageView;
	
    NSMutableArray			*projectArray;
    NSInteger				lastProjectID;
    id						__unsafe_unretained delegate;
	ProjectsDataObject *projectDataObject;
	
	MyTreeNode *treeNode;	
	MyTreeNode *node1;
	MyTreeNode *node2;
	MyTreeNode *node3;
	MyTreeNode *node4;
	
	IBOutlet UIButton *checkMe;
	
	NSMutableArray *getLevelRootArray;
	NSMutableArray *getLevelRootNameArray;
	NSMutableArray *getLevelRootLevelArray;
	NSMutableArray *getLevelRootBelongsToArray;
	NSMutableArray *pidleveloneArray;
	NSMutableArray *pidleveloneNameArray;
	NSMutableArray *pidleveloneLevelArray;
	NSMutableArray *pidleveloneBelongsToArray;
	NSMutableArray *pidleveltwoArray;
	NSMutableArray *pidleveltwoNameArray;
	NSMutableArray *pidleveltwoLevelArray;
	NSMutableArray *pidleveltwoBelongsTolArray;
	NSMutableArray *pidlevelthreeArray;
	NSMutableArray *pidlevelthreeLevelArray;
	NSMutableArray *pidlevelthreeBelongsToArray;
	NSMutableArray *pidlevelthreeNameArray;
	NSMutableArray *getLevelRootimageArray; 
	NSMutableArray *getLevelOneimageArray; 
	NSMutableArray *getLevelTwoimageArray; 
	NSMutableArray *getLevelThreetimageArray;
    
    NSMutableArray *pidRootInclusiveArray; //Steve
    NSMutableArray *pidleveloneInclusiveArray; //Steve
    NSMutableArray *pidleveltwoInclusiveArray; //Steve
    NSMutableArray *pidlevelthreeInclusiveArray; //Steve
    
	NSString *Sql;
	UIButton *buttonLevelone;
	UIButton  *buttonLeveltwo;
	UIButton  *buttonLevelthree;
	UIButton  *buttonLevelfour;
	
	IBOutlet UIButton *noneButton;
    
    NSInteger getTodoID;
    BOOL setVal;
    
    NSInteger y;
    NSInteger x;
    NSInteger u;
    NSInteger v;
    
    NSString *yy;
    NSString *xx;
    NSString *uu;
    NSString *vv;
    NSString *Pname;
    NSMutableArray *myArray;	
    NSInteger retVal;
    
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIView *helpView;  //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView; //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView; //Steve
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *addProjectButton;
    IBOutlet UILabel *projectLabelNone;
    
    
}
@property(assign)BOOL setVal;
@property(nonatomic,assign)NSInteger getTodoID;
@property(nonatomic,strong)NSMutableArray *projectArray;
@property (unsafe_unretained) id delegate;
@property(nonatomic,strong)NSMutableArray *getLevelRootArray;
@property(nonatomic,strong)NSMutableArray *getLevelRootNameArray;
@property(nonatomic,strong)NSMutableArray *getLevelRootLevelArray;
@property(nonatomic,strong)NSMutableArray *getLevelRootBelongsToArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneNameArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneLevelArray;
@property(nonatomic,strong)NSMutableArray *pidleveloneBelongsToArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoNameArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoLevelArray;
@property(nonatomic,strong)NSMutableArray *pidleveltwoBelongsTolArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeLevelArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeBelongsToArray;
@property(nonatomic,strong)NSMutableArray *pidlevelthreeNameArray;
@property(nonatomic,strong)NSMutableArray *getLevelRootimageArray; 
@property(nonatomic,strong)NSMutableArray *getLevelOneimageArray; 
@property(nonatomic,strong)NSMutableArray *getLevelTwoimageArray; 
@property(nonatomic,strong)NSMutableArray *getLevelThreetimageArray;
@property(nonatomic,strong)NSMutableArray *pidRootInclusiveArray; // Steve
@property(nonatomic,strong)NSMutableArray *pidleveloneInclusiveArray; //Steve
@property(nonatomic,strong) NSMutableArray *pidleveltwoInclusiveArray; //Steve
@property(nonatomic,strong) NSMutableArray *pidlevelthreeInclusiveArray; //Steve

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withProjectID:(NSInteger) projectId;
-(IBAction)backClicked:(id)sender;

-(void)FindRecored;
-(void)getLevelOne:(NSInteger)lONE;
-(void)getLevelTwo:(NSInteger)lTWO;
-(void)getLevelThree:(NSInteger)lThree;
-(IBAction)ShowNext:(id)sender;
-(void)noneButton:(id)sender;
- (IBAction)ProjectButton_click:(id)sender;
-(NSInteger)fetch_level_one:(NSInteger)projid;
- (IBAction)helpView_Tapped:(id)sender;  //Steve

@end

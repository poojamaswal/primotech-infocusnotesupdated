//
//  CustomEventButton.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 26/07/11.
//  Copyright 2011 DAVV. All rights reserved.
//

#import "CustomEventButton.h"


@interface CustomEventButton()
-(void)addControls;
-(void)setFrames; 
@end

@implementation CustomEventButton
@synthesize timeLabel;
@synthesize evntTitleLabel;
@synthesize notesLabel;
@synthesize titleImage;
@synthesize appTitleType;


-(id)initWithFrame:(CGRect) frame andAppointmentType:(NSString*) appType{
    //NSLog(@"initWithFrame");
    
	if (self = [super initWithFrame:frame]) {
		[self addControls];
		self.appTitleType = appType;
        
        // NSLog(@"self.appTitleType =  %@", self.appTitleType);
	}
    
   // NSLog(@"CustomButton self.frame =  %@", NSStringFromCGRect(self.frame));
	return self;
}

-(void)addControls {
   // NSLog(@"initWithFrame");
    
//	CGRect btnRect = [self frame];
	titleImage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
	[titleImage setUserInteractionEnabled:NO];
	
	evntTitleLabel = [[UILabel alloc] init];
	[evntTitleLabel setBackgroundColor:[UIColor clearColor]];
	[evntTitleLabel  setTextColor:[UIColor whiteColor]];
	[evntTitleLabel setFont:[UIFont boldSystemFontOfSize:12]];
	[evntTitleLabel setLineBreakMode:NSLineBreakByClipping];//UILineBreakModeClip
	
	notesLabel = [[UILabel alloc] init];
	[notesLabel setBackgroundColor:[UIColor clearColor]];
	[notesLabel setTextColor:[UIColor whiteColor]];
	[notesLabel setFont:[UIFont boldSystemFontOfSize:12]];
	[notesLabel setLineBreakMode:NSLineBreakByWordWrapping];//UILineBreakModeWordWrap
	[notesLabel setNumberOfLines:0];
	
	
	timeLabel = [[UILabel alloc] init];
	[timeLabel setBackgroundColor:[UIColor clearColor]];
	[timeLabel setTextColor:[UIColor whiteColor]];
	[timeLabel setFont:[UIFont boldSystemFontOfSize:12]];
	[timeLabel setLineBreakMode:NSLineBreakByClipping]; //UILineBreakModeClip
	
	UILabel *timeLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 33, 100, 40)];
	[timeLabel1 setBackgroundColor:[UIColor clearColor]];
	[timeLabel1 setTextColor:[UIColor clearColor]];
	[timeLabel1 setFont:[UIFont boldSystemFontOfSize:12]];
	[timeLabel1 setLineBreakMode:NSLineBreakByClipping];//UILineBreakModeClip
	[timeLabel1 setTag:1101];
	[timeLabel1 setText:@""];
	[self addSubview:timeLabel1];
	
    
    //Steve - added a white box since HW is now transparant
    self.whiteBoxForTitleImage = [UIButton buttonWithType:UIButtonTypeCustom];
    self.whiteBoxForTitleImage.userInteractionEnabled = NO;
    //self.whiteBoxForTitleImage.layer.cornerRadius = 10.0;
    //self.whiteBoxForTitleImage.layer.borderWidth = 1.0;
    //self.whiteBoxForTitleImage.layer.borderColor = [[UIColor grayColor]CGColor];
    self.whiteBoxForTitleImage.backgroundColor = [UIColor colorWithRed:244.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0];
    
    [self addSubview:self.whiteBoxForTitleImage];
	[self addSubview:timeLabel];
    [self addSubview:titleImage];
	[self addSubview:evntTitleLabel]; 
	[self addSubview:notesLabel];
    
}

-(void)setFrames 
{
    //NSLog(@"initWithFrame");
    
	CGRect btnRect = [self frame];
	CGFloat minHWImageWidth = 272.5/3.0;
	[timeLabel setFrame:CGRectMake(2, 0, btnRect.size.width - 4, 15)];
    
	if (btnRect.size.height < 30)
	{
		[timeLabel setFrame:CGRectMake(2, 0, btnRect.size.width - 4, 15)];
	}
	else 
	{
		if (btnRect.size.height > 30 && btnRect.size.height < 45) 
		{
			if ([appTitleType isEqualToString:@"H"])
			{
				if (self.frame.size.width <= minHWImageWidth)
				{
                    self.whiteBoxForTitleImage.frame = CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width  - 4, 0);
                    [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width  - 4, 0)];
				}
				else
				{
                    self.whiteBoxForTitleImage.frame =
                                    CGRectMake(1, timeLabel.frame.size.height + 1, btnRect.size.width  - 2, btnRect.size.height - timeLabel.frame.size.height - 1);
                    
                    
                    //Steve - limits width to 200 so it doesn't look distorted
                    if (IS_IPAD) {
                         [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, 200, btnRect.size.height - timeLabel.frame.size.height - 2)];
                    }
                    else{ //iPhone
                        [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width  - 4, btnRect.size.height - timeLabel.frame.size.height - 2)];
                    }
                    

                    
				}
			}
			else
			{
				[evntTitleLabel setFrame:CGRectMake(2, timeLabel.frame.size.height, btnRect.size.width  - 4, 15)];
			}
		}
		else if (btnRect.size.height >= 45) 
		{
			if ([appTitleType isEqualToString:@"H"])
			{
				if (btnRect.size.height < 60) 
				{
					if (self.frame.size.width <= minHWImageWidth)
					{
                        self.whiteBoxForTitleImage.frame = CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 0);
						[titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 0)];
					}
					else
					{
                        self.whiteBoxForTitleImage.frame =CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, btnRect.size.height - timeLabel.frame.size.height - 2);
                        
                        
                        //Steve - limits width to 200 so it doesn't look distorted
                        if (IS_IPAD) {
                            [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, 200, btnRect.size.height - timeLabel.frame.size.height - 2)];
                        }
                        else{ //iPhone
                            [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width  - 4, btnRect.size.height - timeLabel.frame.size.height - 2)];
                        }
					}
				}
				else 
				{
					if (self.frame.size.width <= minHWImageWidth) 
					{
                        self.whiteBoxForTitleImage.frame = CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 0);
						[titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 0)];
					}
					else 
					{
                        self.whiteBoxForTitleImage.frame = CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 30);
                        
                        
                        //Steve - limits width to 200 so it doesn't get distored
                        if (IS_IPAD) {
                            
                            [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, 200, 30)];
                        }
                        else{ //iPhone
                            
                            [titleImage setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 30)];
                        }
						
					}
					
					CGSize strSize = [notesLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(btnRect.size.width - 4, 9999) lineBreakMode:NSLineBreakByWordWrapping];
					
					[notesLabel setFrame:CGRectMake(2, titleImage.frame.origin.y + titleImage.frame.size.height+ 2, btnRect.size.width - 4, strSize.height)];
				}
			}
			else 
			{
				[evntTitleLabel setFrame:CGRectMake(2, timeLabel.frame.size.height + 1, btnRect.size.width - 4, 15)];
				CGSize strSize = [notesLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(btnRect.size.width - 4, 9999) lineBreakMode:NSLineBreakByWordWrapping];
				
				[notesLabel setFrame:CGRectMake(2, evntTitleLabel.frame.origin.y + evntTitleLabel.frame.size.height + 2, btnRect.size.width - 4, strSize.height)];
			}
		}
	} 
}


@end
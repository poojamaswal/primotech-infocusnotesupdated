//
//  LocationListViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationDataObject.h"



@interface LocationListViewController : UIViewController {
    IBOutlet UITableView	*locationTblVw;
    UIImageView				*tickImageView;

    NSMutableArray			*locationArray;
    LocationDataObject		*selectedObject;
    NSInteger				lastLocationID;
    id						__unsafe_unretained delegate;
}

@property(nonatomic,strong)NSMutableArray *locationArray;
@property (unsafe_unretained) id delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withLocationID:(NSInteger) locationId;
-(IBAction)backClicked:(id)sender;
-(IBAction)addClicked:(id)sender;

@end

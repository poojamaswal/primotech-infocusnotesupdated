//
//  PasswordProtectAppViewController.h
//  Organizer
//
//  Created by Steven Abrams on 7/4/13.
//
//

#import <UIKit/UIKit.h>

@interface PasswordProtectAppViewController : UIViewController<UIAlertViewDelegate>{
    
    BOOL    isChangePassword;
    UIPanGestureRecognizer *pan;
    
}


@property (strong, nonatomic) IBOutlet UIView *passwordProtectSetupView;
@property (strong, nonatomic) IBOutlet UIButton *passwordNOButton;
@property (strong, nonatomic) IBOutlet UIButton *passwordYESButton;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField_1;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField_2;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UILabel *reenterpasswordLabel_1;
@property (strong, nonatomic) IBOutlet UILabel *reenterpasswordLabel_2;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) id parentVC;

@property (strong, nonatomic) IBOutlet UIView *passwordChangeView;
@property (strong, nonatomic) IBOutlet UIButton *passwordChangeNOButton;
@property (strong, nonatomic) IBOutlet UIButton *passwordChangeYESButton;

@property (strong, nonatomic) IBOutlet UILabel *passwordONOFFLabel;
@property (strong, nonatomic) IBOutlet UISwitch *passwordONOFFSwitch;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *touchIDLabel;
@property (strong, nonatomic) IBOutlet UISwitch *touchIDSwitch;
@property (strong, nonatomic) IBOutlet UITextView *touchIDTextView;


- (IBAction)passwordNO_Clicked:(id)sender;
- (IBAction)passwordYES_Clicked:(id)sender;

- (IBAction)passwordChangeNO_Clicked:(id)sender;
- (IBAction)passwordChangeYES_Clicked:(id)sender;

- (IBAction)passwordSaveButtonClicked:(id)sender;
- (IBAction)passwordONOFF_Clicked:(id)sender;

- (IBAction)touchIDSwithChanged:(id)sender;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentViewController;


@end

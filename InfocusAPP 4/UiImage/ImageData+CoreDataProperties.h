//
//  ImageData+CoreDataProperties.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ImageData.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) NSString *imageCoordinates;
@property (nullable, nonatomic, retain) NotesID *notesID;
@property (nullable, nonatomic, retain) NSDate *savedDate;

@end

NS_ASSUME_NONNULL_END

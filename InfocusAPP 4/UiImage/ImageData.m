//
//  ImageData.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "ImageData.h"
#import "NotesID.h"
#import "DrawingDefaults.h"

@implementation ImageData

// Insert code here to add functionality to your managed object subclass

+(ImageData*)addImageData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context
{
    NSEntityDescription *desc = [NSEntityDescription entityForName:@"ImageData"
                                            inManagedObjectContext:context];
    
    ImageData *aImageData = [[ImageData alloc] initWithEntity:desc insertIntoManagedObjectContext:nil];
    
    if([dictData valueForKey:KeyData])
        aImageData.imageData = [dictData valueForKey:KeyData];
    if([dictData valueForKey:KeyFrames])
        aImageData.imageCoordinates = [dictData valueForKey:KeyFrames];
    
    
    //    aImageData.stickyID = [NSString stringWithFormat:@"%@",[dictData valueForKey:KeyID]];
    
    
    aImageData.savedDate = [DrawingDefaults sharedObject].dateStored;
    
    NSLog(@"%@",aImageData.savedDate);
    
    
    [context insertObject:aImageData];
    
    return aImageData;//risk
    
}

@end

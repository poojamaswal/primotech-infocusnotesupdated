//

//

#import <UIKit/UIKit.h>

@interface UIImage (Drawing)

+ (UIImage *)imageWithView:(UIView *)view fromPoint:(CGPoint)point;
+ (UIImage *)imageWithView:(UIView *)view fromRect:(CGRect)rect;
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
@end

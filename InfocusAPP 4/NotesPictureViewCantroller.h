//
//  NotesPictureViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 8/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgDataObject.h"


@interface NotesPictureViewCantroller : UIViewController 
{

	IBOutlet UITableView *theNewTable;
	IBOutlet UIButton *bkbutton;
	NSInteger GetnotesID;
	NSMutableArray *getAllImageArray;
	

	
	
}

//Steve commented for ARC
/*
@property(readwrite)NSInteger GetnotesID;
@property(nonatomic,retain)UITableView *newTable;
@property(nonatomic,retain)UIButton *bkbutton;
@property(nonatomic,retain)	ImgDataObject *GETNotesObject;
@property(nonatomic,retain)NSMutableArray *getAllImageArray;
*/

@property(readwrite)NSInteger GetnotesID;
@property(nonatomic,strong)UITableView *theNewTable;
@property(nonatomic,strong)UIButton *bkbutton;
@property(nonatomic,strong)	ImgDataObject *GETNotesObject;
@property(nonatomic,strong)NSMutableArray *getAllImageArray;


-(IBAction)BackButton_Clicked:(id)sender ;
-(void)SelectImgNotes:(NSInteger)NodID;
@end

//
//  TextData.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/12/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "TextData.h"
#import "NotesID.h"
#import "DatabaseManager.h"
#import "DrawingDefaults.h"

@implementation TextData

// Insert code here to add functionality to your managed object subclass
+(TextData*)addTextData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context
{
    
    
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"TextData"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    array = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    TextData *aTextData;
    NSMutableArray *arrData  = [[NSMutableArray alloc]init];
    for(int i=0;i<array.count;i++)
    {
        aTextData = [array objectAtIndex:i];
        if([aTextData.notesID.pageID isEqualToString:[DrawingDefaults sharedObject].strNoteName])
        {
            [arrData addObject:aTextData];
        }
    }
    
    if([DrawingDefaults sharedObject].intNotesSubNotesCounter-1 == arrData.count)
    {
        NSEntityDescription *desc = [NSEntityDescription entityForName:@"TextData"
                                                inManagedObjectContext:context];
        
        aTextData = [[TextData alloc] initWithEntity:desc insertIntoManagedObjectContext:nil];
        
        if([dictData valueForKey:@"data"])
            aTextData.textData = [dictData valueForKey:@"data"];
        
        aTextData.savedDate = [NSDate date];
        
        [context insertObject:aTextData];
    }
    else
    {
        NSLog(@"NO ------ EQUAL------------------");
        aTextData = [arrData objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
        
        NSLog(@"%@",aTextData.notesID);
        
        if([dictData valueForKey:@"data"])
            aTextData.textData = [dictData valueForKey:@"data"];
        
        aTextData.savedDate = [NSDate date];
        
        
    }
    
    
    
    
    
    
    return aTextData;//risk
    
}
@end

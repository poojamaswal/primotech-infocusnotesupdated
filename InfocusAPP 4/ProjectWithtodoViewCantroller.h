//
//  ProjectWithtodoViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputTypeSelectorViewController.h"


@interface ProjectWithtodoViewCantroller :UIViewController<MFMailComposeViewControllerDelegate> 
{
	NSInteger getpID;
	 IBOutlet UITableView *todoprojectTable;
	NSMutableArray *ProjectTodoeDataArray;
	
	InputTypeSelectorViewController *inputTypeSelectorViewController;
	//edit by tarun........
	NSString *pname;
	//end editing..........

}

@property(nonatomic,assign)NSInteger getpID;
@property(nonatomic,strong)NSMutableArray *ProjectTodoeDataArray;
//edit by tarun........
@property (nonatomic, strong)NSString *pname;
//end edit.........
-(IBAction)cancel_click:(id)sender;
-(void)getTodoProject:(NSInteger)pid;
-(IBAction)add_todo:(id)sender;
-(IBAction)inputOptionButton_Clicked:(id)sender;
- (IBAction)mailBarButton_Clicked:(id)sender;

@end

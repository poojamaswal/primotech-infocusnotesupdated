//
//  AddAlertViewController.m
//  Organizer
//
//  Created by Priyanka Taneja on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddAlertViewController.h"
//#import "OrganizerAppDelegate.h"

@implementation AddAlertViewController

@synthesize alertString, delegate;


//- (id)initWithAlertString:(NSString *) alert {
- (id)initWithAlertString:(NSString *) alert alertDate:(NSDate*) alertDateFormat{
    

    self = [super initWithNibName:@"AddAlertViewController" bundle:[NSBundle mainBundle]];

    if (self) {
        
		self.alertString = [alert isEqualToString:@"None"] ? @"Select Alert Type":alert;
        self.alertDate_Date = alertDateFormat;
    }
    
    
    //NSLog(@"self.alertString = %@  ", self.alertString);
    //NSLog(@"self.alertDate_Date = %@  ", self.alertDate_Date);
    //NSLog(@"self.alertDate_Date = %@  ", [self.alertDate_Date descriptionWithLocale:@"GMT"]);
    
    return self;
}

- (void)viewDidLoad {
    
    //Steve added
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    lastSelected =0;

    /*
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	*/
    
    //[datePicker setDate:destinationDate animated:YES];
    [datePicker setDate:self.alertDate_Date animated:YES];//Steve

	
	valuePicker.hidden  = NO;
	datePicker.hidden   = YES;
	
	pickerviewarray = [[NSMutableArray alloc] initWithObjects:@"None",@"5 Minutes Before",@"15 Minutes Before",@"30 Minutes Before",@"1 Hour Before",
					   @"2 Hour Before",@"1 Day Before",@"2 Day Before",
					   @"On Date of Event", nil];
    
    
    //Steve - ************ Temporary fix - set alertString to "None" each time *********
   
    alertString = @"None";
	// NSLog(@"self.alertString = %@  ", self.alertString);
    
    /*
	if (alertString) {
        
		NSInteger length = [alertString length];
		NSString *str =  [alertString substringFromIndex:length-2];

		if ([str isEqualToString:@"AM"] || [str isEqualToString:@"PM"]) {
            
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
			NSDate *startDate = [formatter dateFromString:alertString];
            
			[datePicker setDate:startDate animated:YES];
			
			valuePicker.hidden  = YES;
			datePicker.hidden   = NO;
			[segDateTime setSelectedSegmentIndex:0];
         
			
		}else {
			NSString *valStr = @"";
            
			for (int i = 0; i < [pickerviewarray count]; i++)  {
                
				//valStr = @"";
				valStr = [pickerviewarray objectAtIndex:i];
				if ([valStr isEqualToString:alertString]) {
					[valuePicker selectRow:i inComponent:0 animated:YES];
					valuePicker.hidden  = NO;
					datePicker.hidden   = YES;
					[segDateTime setSelectedSegmentIndex:0];
				}
			}
		}
	}
     
     */
    
    //Steve Changed
	//if ([alertString isEqualToString:@""] || !alertString ||[alertString isEqualToString:@"Alert"]) {
    if ([alertString isEqualToString:@"None"]) {
        
		lblDateTime.text = @"None";
        
	}else {
        
        //Steve added for international changes
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        lblDateTime.text = [dateFormatter stringFromDate:self.alertDate_Date];
        alertString = [dateFormatter stringFromDate:self.alertDate_Date];
        
		//lblDateTime.text = alertString;
	}
    
  
    [segDateTime setSelectedSegmentIndex:0]; // Anil's Addition
    [self segment_Clicked]; // Anil's Addition
    
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        cancelButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        lblDateTime.frame = CGRectMake(7, 123 - 44, 307, 37);
        segDateTime.frame = CGRectMake(20, 67 - 44, 280, 30);
        segDateTime.frame = CGRectMake(20, 67 - 44, 280, 30);
        datePicker.frame = CGRectMake(0, 244 - 44, 320, 216);
        valuePicker.frame = CGRectMake(0, 244 - 44, 320, 216);
        
        
        if (IS_IPHONE_5) {
            //repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
            
        }
        else {
            //repTblView.frame = CGRectMake(7, 49 - 44, 307, 406 + 44 + 88);
            
        }
        
        self.title =@"Alert";
        
        [self.navigationController.navigationBar addSubview:doneButton];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        //Back Button
        /*
        UIImage *backButtonImage = [UIImage imageNamed:@"CancelSmallBtn.png"];
        UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
        [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [backButtonNewNav addTarget:self action:@selector(Back_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
        
        [[self navigationItem] setLeftBarButtonItem:barButton];
        */
        
    }
    else{
        
        if (!IS_IPHONE_5) { //Not iPhone5
            //shareButton.frame = CGRectMake(105, 416, 111, 37);
        }
        
        self.navigationController.navigationBarHidden = YES;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    

    
}

-(IBAction) segment_Clicked {
    
	switch (segDateTime.selectedSegmentIndex) 
	{
		case 0:
                lblDateTime.text=[pickerviewarray objectAtIndex:lastSelected];//Anil's Added
				datePicker.hidden = YES;
				valuePicker.hidden = NO;
			break;
            
		case 1:
            
                //Steve 
                datePicker.timeZone = [NSTimeZone localTimeZone];
                datePicker.locale = [NSLocale currentLocale];
                datePicker.minuteInterval = 5;
                datePicker.calendar = [NSCalendar currentCalendar];
           
                [self dateChanged];//Anil's Added
				datePicker.hidden = NO;
				valuePicker.hidden = YES;
            break;
            
		default:
			break;
    }	
}

-(IBAction) dateChanged {
	
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
	
	if (alertString) {
		alertString = nil;
	}
    
    self.alertDate_Date = datePicker.date; //Steve
    
	alertString = [formatter stringFromDate:[datePicker date]];
	lblDateTime.text = alertString;
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thepickerView
{
	return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component 
{
    return [pickerviewarray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component 
{
	return [pickerviewarray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (alertString)
	{
		alertString = nil;
	}
	alertString = [pickerviewarray objectAtIndex:row];
    lastSelected=row;//Anil's Added
	lblDateTime.text = alertString;
}


-(IBAction)doneClicked:(id)sender
{
	if ([alertString isEqualToString:@"Select Alert Type"] || [alertString isEqualToString:@""] || !alertString || [alertString isEqualToString:@"Alert"]) {
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select the Alert Type...!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
	}
    
   // NSLog(@"appDelegate.alertStr = %@",appDelegate.alertStr);
    //NSLog(@"alertString = %@",alertString);
    
     if([appDelegate.alertStr isEqual: @"firstClicked"]) //Steve changed to remove warning
     {
        // NSLog(@"firstClicked");
         [[self delegate] setAlertString:alertString];
     }
     else
     {
         [[self delegate] setSecondAlertString:alertString];
     }
    
	[self.navigationController popViewControllerAnimated:YES];
    
}


-(IBAction) Back_buttonClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [cancelButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }

}

- (void)viewDidUnload {
    navBar = nil;
    cancelButton = nil;
    doneButton = nil;
    [super viewDidUnload];
}


@end

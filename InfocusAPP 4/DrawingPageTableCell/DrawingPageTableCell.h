//
//  DrawingPageTableCell.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PageTableViewCellDelegate <NSObject>

- (void)addPageAtIndex:(NSInteger)index;
- (void)deletePageAtIndex:(NSInteger)index;
- (void)rowSelectedAtIndex:(NSInteger)index;
@end

@interface DrawingPageTableCell : UITableViewCell


- (UIImage*)getCustomImage;
- (void)setCustomImage :(UIImage*)image;


@property (nonatomic, weak) IBOutlet UIButton *addPageButton;
@property (nonatomic, weak) IBOutlet UIButton *deletePageButton;
@property (nonatomic, weak) IBOutlet UIButton *rowSelectionButton;
@property (nonatomic, assign) id<PageTableViewCellDelegate> delegate;


@end

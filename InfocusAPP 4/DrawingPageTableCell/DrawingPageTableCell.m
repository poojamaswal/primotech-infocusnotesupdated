//
//  DrawingPageTableCell.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DrawingPageTableCell.h"
#import "DrawingDefaults.h"

@interface DrawingPageTableCell ()
{
    IBOutlet UIImageView *_imageView;
}

@end
@implementation DrawingPageTableCell


#pragma  mark - Getter / Setter

- (UIImage*)getCustomImage
{
    return _imageView.image;
}

- (void)setCustomImage :(UIImage*)image
{
    _imageView.image = image;
}

#pragma mark - Defaults

- (void)awakeFromNib
{
    // Initialization code
    
    _imageView.layer.borderColor = [UIColor grayColor].CGColor;
    _imageView.layer.borderWidth = 1.0f;
    _imageView.layer.cornerRadius = 20.0f;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)rowSelected:(UIButton*)sender
{
    [self.delegate rowSelectedAtIndex:sender.tag];
}

- (IBAction)addPageButtonClicked:(UIButton*)sender {
    
    [[DrawingDefaults sharedObject].dictResizableSticky removeAllObjects];
    [[DrawingDefaults sharedObject].dictResizableData removeAllObjects];
    
    [self.delegate addPageAtIndex:sender.tag];
    
}

- (IBAction)deletePageButtonClicked:(UIButton*)sender {
    
    [self.delegate deletePageAtIndex:sender.tag];
    
}


@end

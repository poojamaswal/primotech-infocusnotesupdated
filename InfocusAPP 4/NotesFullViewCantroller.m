//
//  NotesFullViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesFullViewCantroller.h"
#import "OrganizerAppDelegate.h"
#import "SktDataObject.h"
#import "ImgDataObject.h"
#import "TextDataObject.h"
#import "HwrtDataObject.h"
#import "NotesDetailViewCantroller.h"
#import "AddNewNotesViewCantroller.h"
#import "UIImage+UIImage_Extensions.h"
#import "UIImage+Resize.h"
#import "TTTAttributedLabel.h"


@interface NotesFullViewCantroller()

@property (strong, nonatomic) IBOutlet UIButton     *editButtoniOS7;
@property (strong, nonatomic) IBOutlet UIButton     *exportButton; //Steve
@property (strong, nonatomic) UIButton              *exportButton_iOS6;//steve


@end

@implementation NotesFullViewCantroller;
@synthesize fullTable;
@synthesize showValue;
@synthesize fetchNotesObject, fetchNotesObjectInfo;
@synthesize FindNoteValueArray, FindTitleArray;
@synthesize FindSktValueArray;
@synthesize FindImgValueArray;
@synthesize FindWebValueArray;
@synthesize FindTextValueArray;
@synthesize FindHwrtValueArray,k;
@synthesize fullInfoView;
@synthesize fullscrollview,contSize;
@synthesize ret6,ret5,ret4,ret3,ret2,ret1,ret0,ret7;
@synthesize newVal;
@synthesize currentImage;


//Steve added
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define Action_Sheet_Tag 123

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad
{
    [super viewDidLoad];

    if(IS_LITE_VERSION)
    {

        [super viewDidLoad];
    }
    
    if (!IS_IOS_7) { //Not iOS 7
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background-paper2"]];
        backgroundView.frame = self.view.bounds;
        [[self view] addSubview:backgroundView];
    }
    

}


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}



-(void)viewWillAppear:(BOOL)animated{
    //    NSLog(@"%s",__FUNCTION__);
    //    NSLog(@"%@",notesDataObject);
    
    [super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    

    navBar.hidden = YES;
    backButton.hidden = YES;
    self.navigationController.navigationBarHidden = NO;
    fullInfoView.frame = CGRectMake(0, 0, 320, 504 + 64);
    self.title =@"Note Info";
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
    self.view.backgroundColor = [UIColor blackColor];
    fullInfoView.backgroundColor = [UIColor whiteColor];
    
    
    //export Button from NIB
    [self.navigationController.navigationBar addSubview:_exportButton];
    
    
    
    //Edit Button made in NIB. Note - floating on page, not on view in NIB
    _editButtoniOS7.frame = CGRectMake(255, 8, 51, 29); //CGRectMake(207, 7, 51, 29);
    [_editButtoniOS7 addTarget:self
                        action:@selector(editNotes:)
              forControlEvents:UIControlEventTouchUpInside];
    
    _editButtoniOS7.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:19];
    [self.navigationController.navigationBar addSubview:_editButtoniOS7];
    
    
    ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
        
        _editButtoniOS7.tintColor = [UIColor whiteColor];
        _exportButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        
    }
    else{ //Dark Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
        _editButtoniOS7.tintColor = [UIColor whiteColor];
        _exportButton.tintColor = [UIColor whiteColor];
        
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        
        _exportButton.hidden = YES;
        
        _editButtoniOS7.frame = CGRectMake(390, 8, 51, 29);
    }
    
    ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
    



    
    // ***************** Steve End ***************************

    

    notesDataObject = [[NotesDataObject alloc] init];
    
    [fullscrollview removeFromSuperview];
    
    localImages = [[NSArray alloc] init];
   // FindNoteValueArray = [[NSMutableArray alloc]init]; //Steve commented
    FindSktValueArray  = [[NSMutableArray alloc]init];
    FindImgValueArray  = [[NSMutableArray alloc]init];
   // FindWebValueArray  = [[NSMutableArray alloc]init]; //Steve commented
    FindTextValueArray = [[NSMutableArray alloc]init];
    FindHwrtValueArray = [[NSMutableArray alloc]init];
    FindTitleArray =    [[NSMutableArray alloc]init];
    
    
   // [self SelectMapNotes:fetchNotesObject]; //Steve commented
    [self SelectSktNotes:fetchNotesObject];
    [self SelectImgNotes:fetchNotesObject];
    //[self SelectWebNotes:fetchNotesObject];  //STeve commented
    [self SelectTextNotes:fetchNotesObject];
    [self SelectHwrtNotes:fetchNotesObject];
    [self SelectTitleNotes:fetchNotesObject];
    
    NSString *tempString = [self getFolderName];
    
    contSize=40; //Steve changed: 200
    
    //   NSLog(@"contSize->%d",contSize);
    
    if (IS_IPHONE_5) {
        
        if (IS_IOS_7) {
        
            fullscrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 0, 300, 528 + 64)];
            
            UIEdgeInsets insets = UIEdgeInsetsMake(64, 0, 0, 0);
            fullscrollview.contentInset = insets;
        }
        else{
            
            fullscrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(30, 0, 290, 528)];
        }
        
    }
    else{
        if (IS_IOS_7) {
            if (!IS_IPAD)
            {
                 fullscrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 0, 300, 440 + 64)];
            }else
            {
                 fullscrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, self.view.frame.size.height-(64+44))];
            }
          
            
            UIEdgeInsets insets = UIEdgeInsetsMake(64, 0, 0, 0);
            fullscrollview.contentInset = insets;
        }
        else{
            
            fullscrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(30, 0, 290, 440)];
        }
    }
    
    
    [fullscrollview setBackgroundColor:[UIColor clearColor]];
    
    
    /* For Title */
    if([FindTitleArray count]>0)
    {
        
        //[ret2 addTarget:self action:@selector(flipAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // [fullscrollview addSubview:ret0];
        NotesDataObject *notedataobj = [FindTitleArray objectAtIndex:0];
        
        if ([notedataobj.notesTitleType isEqualToString: @"T"] || [notedataobj.notesTitleType isEqualToString: @"V"]) {
            if (notedataobj.notesTitle && (![notedataobj.notesTitle isEqualToString:@""])) {
                
                
                ///////////// Steve added //////////////////////
                //Calculate the expected size based on the font and linebreak mode of your label
                
                int labelWidthSize;
                
                if (IS_IOS_7)
                     labelWidthSize = 320;
                else
                    labelWidthSize = 270;
                
                    
                CGSize maximumLabelSize = CGSizeMake(labelWidthSize,9999);
                CGSize expectedSize = [notedataobj.notesTitle sizeWithFont:[UIFont fontWithName:@"Noteworthy-Bold" size:24.0] constrainedToSize:maximumLabelSize lineBreakMode:notedataobj.notesTitle.length]; 
                //     NSLog(@"expectedLabelSize Title = %@", NSStringFromCGSize(expectedSize));
                ///////////// Steve added End //////////////////////
                
                //Steve moved here
                ret0 = [UIButton buttonWithType:UIButtonTypeCustom];
                ret0.frame = CGRectMake(15,15, labelWidthSize, expectedSize.height);  
                ret0.layer.borderWidth = 0;
                ret0.userInteractionEnabled =NO;

                
                UITextView *detailLabel = [[UITextView alloc] initWithFrame:CGRectMake(15, 15, labelWidthSize, expectedSize.height + 10)];
                
                if (IS_IOS_7) {
                    
                    detailLabel.frame = CGRectMake(0, 15, labelWidthSize, expectedSize.height + 20);
                }

                //[ret5 addSubview:detailLabel];
                detailLabel.layer.borderWidth = 0; //Steve changed for testing
                detailLabel.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:1.0 alpha:0.9] CGColor];
                detailLabel.backgroundColor = [UIColor clearColor];
                detailLabel.layer.cornerRadius = 0;
                detailLabel.font = [UIFont fontWithName:@"Noteworthy-Bold" size:24.0];  //Steve changed
                NSString *cellValue = notedataobj.notesTitle;
                detailLabel.text=cellValue;
                detailLabel.textAlignment =  NSTextAlignmentLeft; //Steve changed was UITextAlignmentCenter
                detailLabel.editable=NO;
                detailLabel.scrollEnabled = NO;  //Steve added
                detailLabel.dataDetectorTypes = UIDataDetectorTypeAll; //Steve - detects phone, web, etc links
                [fullscrollview addSubview:detailLabel];
            
                
                newVal = expectedSize.height + 20;  //Steve added
                contSize = contSize + newVal; //Steve added
                
                //NSLog(@"Title Text ContentSize = %d", contSize);
                
            }  
        }
        else if ([notedataobj.notesTitleType isEqualToString: @"H"] && notedataobj.notesTitleBinary) {
            UIImage *titleImage = [[UIImage alloc] initWithData:
                                   notedataobj.notesTitleBinary];
            
            //Steve moved here
            ret0 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret0.frame = CGRectMake(15,15, 290, 75);  
            ret0.layer.borderWidth = 0;  //Steve changed for testing
            ret0.userInteractionEnabled =NO;
            
            
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 15, 265, 43)];
            
            if (IS_IOS_7) {
    
                titleImageView.frame = CGRectMake(0, 15, 265, 43);
            }
            
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.layer.borderWidth = 0; //Steve changed for testing
            titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 10;
            titleImageView.layer.masksToBounds = YES;
            
            [fullscrollview addSubview:titleImageView];
            newVal = 60; //Steve added
            contSize = contSize + 60;
            
           // NSLog(@"Title HW After ContentSize = %d", contSize);
            
        }
        
        //Steve added
        //Adds a line separator for the title
        if (IS_IOS_7) {
            
            UIView  *titleSeparator = [[UIView alloc] initWithFrame:CGRectMake(5, newVal + 10, 310, 1)];
            titleSeparator.backgroundColor = [UIColor lightGrayColor];
            [fullscrollview addSubview:titleSeparator];
        }
        else{//iOS 6
            
            UIView  *titleSeparator = [[UIView alloc] initWithFrame:CGRectMake(17, newVal + 10, 270, 1)];
            titleSeparator.backgroundColor = [UIColor lightGrayColor];
            [fullscrollview addSubview:titleSeparator];
        }

        
        newVal = newVal + 10;
        contSize = contSize + 10; //Steve added
        
    }
    
    //  NSLog(@"********** newVal after Title = %d", newVal);
    
    
    /*For project name*/
    if([tempString length]>0)
    {
        //      NSLog(@"newVal at Project beginning = %d", newVal);
        
        ///////////// Steve added //////////////////////
        
        //Calculate the expected size based on the font and linebreak mode of your label
        int labelWidthSize = 270;
        CGSize maximumLabelSize = CGSizeMake(labelWidthSize,9999);
        CGSize expectedSize = [tempString sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:maximumLabelSize lineBreakMode:tempString.length]; 
        //  NSLog(@"expectedLabelSize  Project = %@", NSStringFromCGSize(expectedSize));
        
        ///////////// Steve added End //////////////////////
        
        
        
        ret7 = [UIButton buttonWithType:UIButtonTypeCustom];
        ret7.frame = CGRectMake(15,newVal + 10,labelWidthSize, expectedSize.height);
        ret7.userInteractionEnabled =NO;
        
        ret7.layer.borderWidth = 0.0; //Steve added for testing
        ret7.layer.borderColor = [[UIColor blueColor]CGColor];
        
        
        // [fullscrollview addSubview:ret0];
        //  NotesDataObject *notedataobj = [FindTitleArray objectAtIndex:0];
        //[ret2 addTarget:self action:@selector(flipAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString*tem=[NSString stringWithFormat:@"Project: %@",tempString];
        
   
        TTTAttributedLabel *label = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(7, 0, labelWidthSize, expectedSize.height)];
        
        if (IS_IOS_7) {
            
            
            label.frame = CGRectMake(-10, 0, labelWidthSize, expectedSize.height);
        }
        
        label.font = [UIFont systemFontOfSize:18];//Steve commented                                              // 
        // label.font = [UIFont fontWithName:@"Noteworthy-Bold" size:18.0];  //Steve changed
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.textAlignment = NSTextAlignmentLeft;
        label.numberOfLines = 8;
        label.adjustsFontSizeToFitWidth = YES;
        label.layer.borderWidth = 0; //Steve changed for testing
        label.layer.borderColor = [[UIColor colorWithRed:1.0 green:0.5 blue:0.5 alpha:0.9] CGColor];  
        
        //   NSLog(@"Project label frame = %@", NSStringFromCGRect(label.frame));
        
        /////////////// Steve  added //////////////////////////
        label.text = tem;
        
        [fullscrollview addSubview:ret7];
        
        [ret7 addSubview:label];
        
        
        /////////////// Steve  added End //////////////////////////
    
        newVal=newVal+expectedSize.height + 20;
        contSize = contSize + expectedSize.height + 20; 
        
        //Steve added
        //Adds a line separator for the Project label
        UIView  *titleSeparator = [[UIView alloc] initWithFrame:CGRectMake(17, newVal, 270, 1)];
        titleSeparator.backgroundColor = [UIColor lightGrayColor];
        [fullscrollview addSubview:titleSeparator];
        
        if (IS_IOS_7) {
            titleSeparator.frame = CGRectMake(5, newVal, 310, 1);
        }
        
        newVal = newVal + 10;
        contSize = contSize + 10;
        

    }
    
    /* Text Array */
    if([FindTextValueArray count]>0)
    {
        for (int i=0; i<[FindTextValueArray count]; i++) {
            
            //Steve moved to here
            NSString *cellValue =[[FindTextValueArray objectAtIndex:i]textinfo];
            
            //NSLog(@"textinfo = %@", [[FindTextValueArray objectAtIndex:i]textinfo]);
            
            
            ///////////// Steve added //////////////////////
            
            //Calculate the expected size based on the font and linebreak mode of your label
            int labelWidthSize = 270;
            CGSize maximumLabelSize = CGSizeMake(labelWidthSize,9999);
            
            //Steve -  **** important *** make sure size is one larger than your displaying, otherwise textview sometimes cuts off end.
            CGSize expectedSize = [cellValue sizeWithFont:[UIFont fontWithName:@"Noteworthy-Bold" size:19.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
            
            
            //NSLog(@"************************************************");
            //NSLog(@"textinfo = %@", [[FindTextValueArray objectAtIndex:i]textinfo]);
            //NSLog(@"maximumLabelSize = %@", NSStringFromCGSize(expectedSize));
            
            
            ///////////// Steve added End //////////////////////
            
            
            ret5 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret5.tag = 102;
            
            
            //Steve changed was: ret5.frame = CGRectMake(15,newVal, 290,150);
            ret5.frame = CGRectMake(15, newVal, labelWidthSize, expectedSize.height +10);
            [fullscrollview addSubview:ret5];
            
            
            UITextView *detailTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, labelWidthSize, expectedSize.height + 10)];
            
            if (IS_IOS_7) { //Steve - fixes frame for iOS 7
                
                detailTextView.frame = CGRectMake(-10, 0, labelWidthSize, expectedSize.height + 20);
            
            }
            
            detailTextView.layer.borderWidth = 0;
            detailTextView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            detailTextView.layer.cornerRadius = 10;
            detailTextView.backgroundColor = [UIColor clearColor];
            detailTextView.font = [UIFont fontWithName:@"Noteworthy-Bold" size:18.0];
            detailTextView.textAlignment = NSTextAlignmentLeft;
            detailTextView.text=cellValue;
            detailTextView.editable=NO;
            detailTextView.scrollEnabled = NO;
            detailTextView.dataDetectorTypes = UIDataDetectorTypeAll; //Steve - detects phone, web, etc links
            detailTextView.backgroundColor = [UIColor clearColor];
            [ret5 addSubview:detailTextView];
            
            
            newVal=newVal + expectedSize.height + 10; //steve changed was: newVal=newVal+165;
            contSize = contSize + expectedSize.height + 10; //Steve added
            
            
        }	
        
    }
    
    
    /* Handwriting Array */
    if([FindHwrtValueArray count]>0)
    {
        for (int i=0;i<[FindHwrtValueArray count];i++){ 
            
            ret4 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret4.userInteractionEnabled = NO;
            ret4.tag = 102;
            ret4.frame = CGRectMake(42,newVal, 220,140);  // Steve CGRectMake(42,newVal, 220,150)
            
           [fullscrollview addSubview:ret4];
            
            UIImage *titleImage = [[UIImage alloc ] initWithData:
                                   [[FindHwrtValueArray objectAtIndex:i]hwrtinfo]];
        
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 220, 129)];// Steve CGRectMake(0, 10, 220, 140)
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.layer.borderWidth = 1; //Steve changed for testing
            titleImageView.layer.borderColor =  [UIColor lightGrayColor].CGColor;                // [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 2; //Was 10
            titleImageView.clipsToBounds = YES;
            
            
            //Steve added to improve performance of Shadow affact.  
            UIBezierPath *path = [UIBezierPath bezierPathWithRect:titleImageView.bounds];
            titleImageView.layer.shadowPath = [path CGPath];
            titleImageView.layer.shouldRasterize = YES;  //Renders the layer as a bitmap
            
            //Steve added shadow
            titleImageView.layer.shadowColor = [UIColor blackColor].CGColor;
            titleImageView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            titleImageView.layer.shadowRadius = 2.0;
            titleImageView.layer.shadowOpacity = 0.5;
            titleImageView.layer.masksToBounds = NO;
  
            [ret4 addSubview:titleImageView];
            
            newVal=newVal+150;  //Steve changed was:  newVal=newVal+165;
            
            contSize = contSize + 150; //Steve added
            
           //NSLog(@"Content Size After HW = %d", contSize);
        }
    }
    
    
    /* For image Array */
    if([FindImgValueArray count]>0)
    {
        for (int i=0;i<[FindImgValueArray count];i++){
            
            UIImage *titleImage = [[UIImage alloc ] initWithData:
                                   [[FindImgValueArray objectAtIndex:i]imginfo]];
            //   NSLog(@"%@",NSStringFromCGSize(titleImage.size));
            
            titleImage = [titleImage imageByScalingProportionallyToSize:CGSizeMake(250,250)];  //Steve CGSizeMake(250,250)
            //     NSLog(@"%@",NSStringFromCGSize(titleImage.size));
            
            
            ret1 = [UIButton buttonWithType:UIButtonTypeCustom];
            //Steve changed ret1.frame = CGRectMake((260-titleImage.size.width)/2+10,newVal/*+(160*i)*/,titleImage.size.width+30,titleImage.size.height+30);
            ret1.frame = CGRectMake((260-titleImage.size.width)/2 +10,newVal,titleImage.size.width+30,titleImage.size.height+10);
            //ret1.layer.borderWidth =0;
            //ret1.userInteractionEnabled =NO;
            [fullscrollview addSubview:ret1];
            

            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, titleImage.size.width,titleImage.size.height)];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.layer.borderWidth = 1; //Steve changed for testing
            titleImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;      //Steve  [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 0.0;  //10
            //titleImageView.layer.masksToBounds = YES;
            [titleImageView addTarget:self action:@selector(showFullImage:) forControlEvents:UIControlEventTouchUpInside];
            [titleImageView setTag:i];
            
            //Steve added to improve performance of Shadow affact.  
            UIBezierPath *path = [UIBezierPath bezierPathWithRect:titleImageView.bounds];
            titleImageView.layer.shadowPath = [path CGPath];
            titleImageView.layer.shouldRasterize = YES;  //Renders the layer as a bitmap
            
            //Steve added shadow
            titleImageView.layer.shadowColor = [UIColor blackColor].CGColor;
            titleImageView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            titleImageView.layer.shadowRadius = 2.0;
            titleImageView.layer.shadowOpacity = 0.5;
            titleImageView.layer.masksToBounds = NO;
            
            [ret1 addSubview:titleImageView];
            
            //Steve added - forward button on image
            UIImage *forwardButtonImage = [UIImage imageNamed:@"forwardicon.png"];
            UIButton *forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(titleImage.size.width - 46,titleImage.size.height - 44,36,34)];
            [forwardButton setBackgroundImage:forwardButtonImage forState:UIControlStateNormal];
            //[forwardButton addTarget:self action:@selector(showFullImage:) forControlEvents:UIControlEventTouchUpInside];
            [forwardButton setUserInteractionEnabled:NO];
            
            [titleImageView addSubview:forwardButton];
            

            newVal=newVal+(ret1.frame.size.height+15);
            contSize = contSize+(ret1.frame.size.height + 15); //Steve contSize = contSize+(ret1.frame.size.height-180)
        }
    }
    
    /* for Sketch Array */
    if([FindSktValueArray count]>0)
    {
        
        for (int i=0;i<[FindSktValueArray count];i++){
            ret3 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret3.userInteractionEnabled =NO;
            ret3.tag = 102;
            //ret3.frame = CGRectMake(15,newVal, 260,150); //Steve changed CGRectMake(15,newVal, 260,150) //Steve commented
            
            [fullscrollview addSubview:ret3];
            
            
            UIImage *titleImage = [[UIImage alloc ] initWithData:
                                   [[FindSktValueArray objectAtIndex:i]sktinfo]];
            
            
            
            //      NSLog(@"%@",NSStringFromCGSize(titleImage.size));
            
            titleImage = [titleImage imageByScalingProportionallyToSize:CGSizeMake(250,250)];
            //     NSLog(@"%@",NSStringFromCGSize(titleImage.size));
            
            
            
            //STeve changed was: ret3.frame = CGRectMake((260-titleImage.size.width)/2,newVal/*+(160*i)*/,titleImage.size.width+30,titleImage.size.height+30);
            ret3.frame = CGRectMake((260-titleImage.size.width)/2 +10,newVal,titleImage.size.width+30,titleImage.size.height+10);
            
            
            // UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, 260, 120)];
            
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, titleImage.size.width,titleImage.size.height)];
            
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.backgroundColor = [UIColor clearColor];
            titleImageView.layer.borderWidth = 1; //Steve added border for Sketch
            titleImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
            //titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 0.0; 
         
            
            //Steve added to improve performance of Shadow affact.  
            UIBezierPath *path = [UIBezierPath bezierPathWithRect:titleImageView.bounds];
            titleImageView.layer.shadowPath = [path CGPath];
            titleImageView.layer.shouldRasterize = YES;  //Renders the layer as a bitmap
            
            //Steve added shadow
            titleImageView.layer.shadowColor = [UIColor blackColor].CGColor;
            titleImageView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            titleImageView.layer.shadowRadius = 2.0;
            titleImageView.layer.shadowOpacity = 0.5;
            titleImageView.layer.masksToBounds = NO;
            
            [ret3 addSubview:titleImageView];
            
            newVal=newVal+(ret3.frame.size.height+ 5); //Steve changed was: newVal=newVal+(ret3.frame.size.height+15);
            contSize = contSize+(ret3.frame.size.height + 5); //Steve contSize = contSize+(ret3.frame.size.height-180); 
            
        }
        
    }
    
    
    //Steve Commented out
    /*
    //for Map Array 
    if([FindNoteValueArray count]>0)
    {
        
        for (int i=0;i<[FindNoteValueArray count];i++){
            
            ret2 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret2.userInteractionEnabled=NO;
            ret2.frame = CGRectMake(15,newVal, 260,150);
            //[ret2 addTarget:self action:@selector(flipAction:) forControlEvents:UIControlEventTouchUpInside];
            [fullscrollview addSubview:ret2];
            
            
            UIImage *titleImage = [[UIImage alloc ] initWithData:
                                   [[FindNoteValueArray objectAtIndex:i]mapinfo]];
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, 260, 120)];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.layer.borderWidth = 0;
            titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 10;
            titleImageView.layer.masksToBounds = YES;
            [ret2 addSubview:titleImageView];
            //[titleImage release]; //Steve commented for ARC
            //[titleImageView release]; //Steve commented for ARC
            newVal=newVal+165;
        }			
        
    }
    
*/
    
    //Steve Commented out
    /*
     
    //for Web Array 
    if([FindWebValueArray count]>0)
    {
        
        
        
        for(int i =0; i < [FindWebValueArray count]; i++){		
            ret6 = [UIButton buttonWithType:UIButtonTypeCustom];
            ret6.userInteractionEnabled = NO;
            ret6.frame = CGRectMake(15,newVal, 260,150);
            //[ret2 addTarget:self action:@selector(flipAction:) forControlEvents:UIControlEventTouchUpInside];
            [fullscrollview addSubview:ret6];
            
            
            UIImage *titleImage = [[UIImage alloc ] initWithData:
                                   [[FindWebValueArray objectAtIndex:0]webImg]];
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, 260, 120)];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            titleImageView.layer.borderWidth = 0;
            titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
            titleImageView.layer.cornerRadius = 10;
            titleImageView.layer.masksToBounds = YES;
            [ret6 addSubview:titleImageView];
            //[titleImage release];  //Steve commented for ARC
            //[titleImageView release];  //Steve commented for ARC
            newVal=newVal+165;
        }	
        
    }
    */
    
    //Steve added to give the scroll some bounce room if contSize is less than  the view (about 436).
    //NSLog(@"contsize = %d", contSize);
    if (contSize < 444) 
    {
        contSize = 444;
        fullscrollview.showsHorizontalScrollIndicator = NO;
    }
    
    //NSLog(@"contsize = %d", contSize);
    
    fullscrollview.contentSize =CGSizeMake(290,contSize);
    fullscrollview.bounces = YES; //Steve

    //NSLog(@"fullscrollview = %@", fullscrollview.description);
    
    [fullInfoView addSubview:fullscrollview];
    
}



-(NSString *)getFolderName
{
    if(notesDataObject.belongsToProjectID)
    {
        myArray=[[NSMutableArray alloc]init ];
        y=[self fetch_level_one:notesDataObject.belongsToProjectID];
        yy=Pname;
        [myArray addObject:yy];
        if((y)&&(y!=0)) 
        {
            x= [self fetch_level_one:y];
            xx=Pname;
            [myArray addObject:xx];
            if((x)&&(x!=0)) 
            { 
                u=[self fetch_level_one:x];
                uu=Pname;
                [myArray addObject:uu];
                if((u)&&(u!=0)) 
                { 
                    v= [self fetch_level_one:u];
                    vv=Pname;
                    [myArray addObject:vv];
                }
            }
        }
    }
    
    
    NSString *finalText=[[NSString alloc]init];   
    for(int k=[myArray count] - 1;k>=0; k--)
    {
        finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
    }
    
    if ( [finalText length] > 0)
        finalText = [finalText substringToIndex:[finalText length] - 1];
    
    
    //   NSLog(@"Final String %@", finalText);
    
    
    
    [myArray removeAllObjects];
    
    return  finalText;
    
    
    
}


-(NSInteger)fetch_level_one:(NSInteger)projid
{
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    if (selected_stmt == nil && appDelegate.database)
    {
        Sql = [[NSString alloc]initWithFormat:@"SELECT  belongstoId,projectName from ProjectMaster where projectID =%d",projid];
        //    NSLog(@"%@",Sql);
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW) 
            {
                retVal= sqlite3_column_int(selected_stmt, 0);
			    Pname= [NSString stringWithUTF8String:(char *) sqlite3_column_text(selected_stmt,1)];
                //      NSLog(@"%d   =  %@",retVal,Pname);
                
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    return retVal;
}



-(id)initWithDataObj:(NotesDataObject *)notesDataObjLocal {
	if (self = [super initWithNibName:@"NotesFullViewCantroller" bundle:[NSBundle mainBundle]]){
		//notesDataObject = [notesDataObjLocal retain]; //Steve commented for ARC
        notesDataObject = notesDataObjLocal;
	}
	return self;
}


-(void)SelectTitleNotes:(NSInteger)NodID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT NoteID,NotesTitleType,NoteTitle,NotesTitleBinary,BelongstoProjectID FROM NotesMaster WHERE NoteID= %d",NodID];
		
        //	NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[FindTitleArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				NotesDataObject *notedataobj = [[NotesDataObject alloc]init];
				
				[notedataobj setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [notesDataObject setNotesID:sqlite3_column_int(selectNotes_Stmt, 0)];
				[notedataobj setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[notedataobj setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 3);
				
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:blobLength];
				
				[notedataobj setNotesTitleBinary:binaryData];
                [notesDataObject setBelongsToProjectID:sqlite3_column_int(selectNotes_Stmt, 4)];
				
				
				[FindTitleArray addObject:notedataobj];
                //	[notedataobj release];
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		//[Sql release]; //Steve commented for ARC
	}
	[notesDataObject setNotesTitleArray:FindTitleArray];
}




#pragma mark show full image
-(void)showFullImage:(id)sender
{
    
    ImgDataObject *tempObj =[FindImgValueArray objectAtIndex:[sender tag]];
    
    
    //   NSLog(@"image id is %d",tempObj.imgid);
    
    UIImage *titleImage = [[UIImage alloc ] initWithData:
                           [[FindImgValueArray objectAtIndex:[sender tag]]imginfo]];
    
    
    
    NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[searchPath objectAtIndex:0]  stringByAppendingString:[NSString stringWithFormat:@"/%@",@"selectedImage.jpeg"]];
    NSData *imageData = UIImageJPEGRepresentation(titleImage,0);
    
    [imageData writeToFile:path atomically:YES];
    
    
    localImages = [[NSArray alloc] initWithObjects:@"selectedImage.jpeg", nil]; 
    
    localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    localGallery.imgId = tempObj.imgid;
    
    
    [self.navigationController pushViewController:localGallery animated:YES];
    
}



/* Fetch Sketch Info */

-(void)SelectSktNotes:(NSInteger)NodID
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT SktID ,SktInfo FROM NotesSktInfo WHERE NotesID = %d",NodID];
		
        //	NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[FindSktValueArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				SktDataObject *sktpdataobj = [[SktDataObject alloc]init];
				
				[sktpdataobj setSktid: sqlite3_column_int(selectNotes_Stmt, 0)];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 1);
				
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 1) length:blobLength];
				
				[sktpdataobj setSktinfo:binaryData];
				
				[FindSktValueArray addObject:sktpdataobj];
				//[sktpdataobj release]; //Steve commented for ARC
				//[binaryData release]; //Steve commented for ARC
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		//[Sql release];  //Steve commented for ARC
	}
	
	[notesDataObject setNotessktchimageArrsy:FindSktValueArray];
	
}


/* end Skt Method */

/* Image Method */

-(void)SelectImgNotes:(NSInteger)NodID
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT ImgID ,ImgInfo FROM NotesImgInfo WHERE NotesID = %d",NodID];
		
        //	NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[FindImgValueArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				ImgDataObject *imgdataobj=[[ImgDataObject alloc]init];
				
				[imgdataobj setImgid: sqlite3_column_int(selectNotes_Stmt, 0)];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 1);
				
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 1) length:blobLength];
				
				[imgdataobj setImginfo:binaryData];
				
				[FindImgValueArray addObject:imgdataobj];
				//[imgdataobj release]; //Steve commented for ARC
				//[binaryData release]; //Steve commented for ARC
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		//[Sql release]; //Steve commented for ARC
        [notesDataObject setNotesPicDataArray:FindImgValueArray];
	}	
}




/*  End */




/* Find Text Data */


-(void)SelectTextNotes:(NSInteger)NodID
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT TextID ,TextInfo FROM NotesTextInfo WHERE NotesID = %d",NodID];
		
        //	NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[FindTextValueArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				TextDataObject *textdataobj = [[TextDataObject alloc]init];
				
				[textdataobj setTextid: sqlite3_column_int(selectNotes_Stmt, 0)];
				
				[textdataobj setTextinfo:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				
                
				[FindTextValueArray addObject:textdataobj];
				//[textdataobj release]; //Steve commented for ARC
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		//[Sql release]; //Steve commented for ARC
		
		[notesDataObject setNotesTextArray:FindTextValueArray];
	}
	
	
}
/* End */
//----------------------------------------------

/*  Find Handwriting Image*/

-(void)SelectHwrtNotes:(NSInteger)NodID;
{
	
    
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    sqlite3_stmt *selectNotes_Stmt = nil; 
    
    if(selectNotes_Stmt == nil && appDelegate.database) 
    {	
        
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT HwrtID ,HwertInfo FROM NotesHwrtInfo WHERE NotesID = %d",NodID];
        
        //  NSLog(@" query %@",Sql);
        if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
        {
            NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
        else
        {
            [FindHwrtValueArray removeAllObjects];
            while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
            {
                HwrtDataObject *hwrtdataobj = [[HwrtDataObject alloc]init];
                
                [hwrtdataobj setHwrtid: sqlite3_column_int(selectNotes_Stmt, 0)];
                
                NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 1);
                
                NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 1) length:blobLength];
                
                [hwrtdataobj setHwrtinfo:binaryData];  //STeve ??
                
                [FindHwrtValueArray addObject:hwrtdataobj];
                
                //[hwrtdataobj release]; //Steve commented for ARC
                
            }							        
            sqlite3_finalize(selectNotes_Stmt);		
            selectNotes_Stmt = nil;        
        }
        //[Sql release];  //Steve commented for ARC
        [notesDataObject setNotesHwrtDataArray:FindHwrtValueArray];
    }
    
    
}

/* End */

//Steve 
- (IBAction)exportButton_Clicked:(id)sender {
   // NSLog(@"exportButton_Clicked");
    
    /*
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
 
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    */
     
    UIActionSheet *ActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil
                                                otherButtonTitles:@"Email as PDF", @"Email as JPEG", @"Save to Photo Album", nil];
    
    [ActionSheet setTag:Action_Sheet_Tag];
    [ActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [ActionSheet showInView:self.view];
}


-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    //Send email as PDF
    if(buttonIndex == 0)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            
            if (IS_IOS_7) {
                
                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
                
            }
            else{
                UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
                [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
                
                [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            }
            
            
            
            MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
            [mailCntrlr setMailComposeDelegate:self];
            [mailCntrlr setSubject:@"InFocus Note Attached"];
            NSString *body = @"See attached Note sent by InFocus Pro";
            
            //Steve - Infocus Pro Link
            NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
            NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
            body = [body stringByAppendingString:bodyURL];
            
            [mailCntrlr setMessageBody:body isHTML:YES];
            
            
            UIScrollView *savedScrollView = [[UIScrollView alloc]init];
            savedScrollView = fullscrollview;
            
            //Converts the Note to a PDF
            NSMutableData *pdfData = [NSMutableData data];
            fullscrollview.frame = CGRectMake(0, 0, fullscrollview.contentSize.width, fullscrollview.contentSize.height);
            
            //points pdf converter to mutable data object
            UIGraphicsBeginPDFContextToData(pdfData, fullscrollview.frame, nil);
            
            UIGraphicsBeginPDFPage();
            {
                [fullscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
            }
            UIGraphicsEndPDFContext();
            
            //brings fullscrollview to normal size again
            fullscrollview.frame = CGRectMake(30, 0, fullscrollview.contentSize.width, fullscrollview.contentSize.height);
            
            
            //[mailCntrlr addAttachmentData:pdfData mimeType:@"image/pdf" fileName:@"InFocus Note.pdf"];
            [mailCntrlr addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"InFocus Note.pdf"];
            [self presentViewController:mailCntrlr animated:YES completion:nil];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }
        
    }

    
    
    //Send email as JPEG
    if(buttonIndex == 1)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            
            if (IS_IOS_7) {

                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
                
            }
            else{
                UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
                [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
                
                [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            }
            
            
            
            MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
            [mailCntrlr setMailComposeDelegate:self];
            [mailCntrlr setSubject:@"InFocus Note Attached"];
            NSString *body = @"See attached Note sent by InFocus Pro";
            
            //Steve - Infocus Pro Link
            NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
            NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
            body = [body stringByAppendingString:bodyURL];
            
            [mailCntrlr setMessageBody:body isHTML:YES];
            
            //Converts the Note to an image
            UIImage* image = nil;
            UIGraphicsBeginImageContext(fullscrollview.contentSize);
            {
                CGPoint savedContentOffset = fullscrollview.contentOffset;
                CGRect savedFrame = fullscrollview.frame;
                
                fullscrollview.contentOffset = CGPointZero;
                fullscrollview.frame = CGRectMake(0, 0, fullscrollview.contentSize.width, fullscrollview.contentSize.height);
                
                [fullscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
                image = UIGraphicsGetImageFromCurrentImageContext();
                
                fullscrollview.contentOffset = savedContentOffset;
                fullscrollview.frame = savedFrame;
            }
            UIGraphicsEndImageContext();
            
            NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
            [mailCntrlr addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@"InFocus Note"];
            
            [self presentViewController:mailCntrlr animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        }

    }
    
    //Save to Photo Album
    if(buttonIndex == 2)
    {
        UIImage* image = nil;
        
        UIGraphicsBeginImageContext(fullscrollview.contentSize);
        {
            CGPoint savedContentOffset = fullscrollview.contentOffset;
            CGRect savedFrame = fullscrollview.frame;
            
            fullscrollview.contentOffset = CGPointZero;
            fullscrollview.frame = CGRectMake(0, 0, fullscrollview.contentSize.width, fullscrollview.contentSize.height);
            
            [fullscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
            image = UIGraphicsGetImageFromCurrentImageContext();
            
            fullscrollview.contentOffset = savedContentOffset;
            fullscrollview.frame = savedFrame;
        }
        UIGraphicsEndImageContext();
        
        if (image != nil)
        {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save To Photo Album" message:@"Your Note has been saved to your Photo Album" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    
}

#pragma mark -
#pragma mark MailComposer  Delegates
#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)BackButton_Clicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Edit Notes

-(void)editNotes:(id)sender
{
	AddNewNotesViewCantroller *addnotes=[[AddNewNotesViewCantroller alloc] initWithDataObj:notesDataObject];
	addnotes.Flag=1;
	addnotes.GetNoteID=fetchNotesObject;
    addnotes.projectComponentsArray =  [NSArray arrayWithObject:[NSString stringWithFormat:@"%d",[[notesDataObject.notesTitleArray objectAtIndex:0] belongsToProjectID]]];
    //	addnotes.notsDataObj = notesDataObject;
	[self.navigationController pushViewController:addnotes animated:YES];
	//[addnotes release]; //Steve commented for ARC
}


-(void)showFullPicture:(id)sender
{
    //  NSLog(@"%d",[sender tag]);	
	
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindImgValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release]; //Steve commented for ARC
}


-(void)showFullSketch:(id)sender
{
	//NSLog(@"%d",[sender tag]);	
	
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindSktValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release]; //Steve commented for ARC
}

-(void)showFullMAP:(id)sender
{
    
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindNoteValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release]; //Steve commented for ARC
}


-(void)showFullHWR:(id)sender
{
	
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindHwrtValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release];  //Steve commented for ARC
}

-(void)showFullTXT:(id)sender
{
	
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindTextValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release];  //Steve commented for ARC
}

-(void)showFullWEB:(id)sender
{
	
	NotesDetailViewCantroller *noteDetailViewCantroller11 = [[NotesDetailViewCantroller alloc] initWithNibName:@"NotesDetailViewCantroller" bundle:nil];
	noteDetailViewCantroller11.GETValue = [sender tag];
	noteDetailViewCantroller11.IDValueArray = FindWebValueArray;
	[self.navigationController pushViewController:noteDetailViewCantroller11 animated:YES];
	//[noteDetailViewCantroller11 release];  //Steve commented for ARC
	
}


/*
 
 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 
 -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 130;	
 }
 
 -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
 {
 return 1;
 }
 
 // Customize the number of rows in the table view.
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
 NSInteger myRow=0;
 NSLog(@" Start %d",myRow);
 
 if([FindNoteValueArray count]>0)
 {
 myRow=myRow+1;
 NSLog(@" First %d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 if([FindSktValueArray count]>0)
 {
 myRow=myRow+1;
 NSLog(@" Second %d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 if([FindImgValueArray  count]>0)
 {
 myRow=myRow+1;
 NSLog(@" Thired %d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 if([FindWebValueArray  count]>0)
 {
 myRow=myRow+1;
 NSLog(@"Foruth  %d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 if([FindTextValueArray count]>0)
 {
 myRow=myRow+1;
 NSLog(@" FifTh %d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 if([FindHwrtValueArray count]>0)
 {
 myRow=myRow+1;
 NSLog(@" Sixth%d",myRow);
 valueFor[myRow] = 1;
 }else {
 valueFor[myRow] = 0;
 }
 
 NSLog(@"Final ==%d",myRow);
 return myRow;
 }
 
 
 
 
 // Customize the appearance of table view cells.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 static NSString *CellIdentifier = @"Cell";
 NSLog(@"ttt===%d",CellIdentifier);
 NSLog(@"kkk%d",k);
 
 UITableViewCell *cell =nil;
 
 if (cell == nil) 
 {
 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
 }
 
 for (UIView *subView in [cell.contentView subviews])
 {
 [subView removeFromSuperview];
 }
 
 
 if([FindImgValueArray count] > 0 && valueFor[indexPath.row] ==)
 {
 NSLog(@" I M on %d",indexPath.row);
 
 UIImage *titleImage = [[UIImage alloc ] initWithData:
 [[FindImgValueArray objectAtIndex:0]imginfo]];
 UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
 titleImageView.layer.borderWidth = 0;
 titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 titleImageView.layer.cornerRadius = 10;
 titleImageView.layer.masksToBounds = YES;
 [cell.contentView addSubview:titleImageView];
 [titleImage release];
 [titleImageView release];
 
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageViewd.tag =111;
 [titleImageViewd addTarget:self action:@selector(showFullPicture:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release];
 }
 
 if([FindSktValueArray count]>0)
 {
 NSLog(@" I M on %d",indexPath.row);
 
 UIImage *titleImage = [[UIImage alloc ] initWithData:
 [[FindSktValueArray objectAtIndex:0]sktinfo]];
 UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
 
 titleImageView.layer.borderWidth = 0;
 titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 titleImageView.layer.cornerRadius = 10;
 titleImageView.layer.masksToBounds = YES;
 [cell.contentView addSubview:titleImageView];
 [titleImage release];
 [titleImageView release];
 
 
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageViewd.tag =222;
 [titleImageViewd addTarget:self action:@selector(showFullSketch:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release];
 }
 
 
 if([FindNoteValueArray count]>0)
 {
 NSLog(@" I M on %d",indexPath.row);
 
 UIImage *titleImage = [[UIImage alloc ] initWithData:
 [[FindNoteValueArray objectAtIndex:0] mapinfo]];
 UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
 titleImageView.layer.borderWidth = 0;
 titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 titleImageView.layer.cornerRadius = 10;
 titleImageView.layer.masksToBounds = YES;
 
 [cell.contentView addSubview:titleImageView];
 [titleImage release];
 [titleImageView release];
 
 //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
 
 
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageViewd.tag =333;
 [titleImageViewd addTarget:self action:@selector(showFullMAP:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release];		
 }
 
 if([FindHwrtValueArray count]>0)
 {
 UIImage *titleImage = [[UIImage alloc ] initWithData:
 [[FindHwrtValueArray objectAtIndex:0]hwrtinfo]];
 UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
 
 titleImageView.layer.borderWidth = 0;
 titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 titleImageView.layer.cornerRadius = 10;
 titleImageView.layer.masksToBounds = YES;
 [cell.contentView addSubview:titleImageView];
 [titleImage release];
 [titleImageView release];
 
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageView.tag =444;
 [titleImageView addTarget:self action:@selector(showFullHWR:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release];
 }
 
 
 {
 /*else if((indexPath.row ==4))
 {
 NSLog(@" I M on %d",indexPath.row);
 
 UITextView *detailLabel = [[UITextView alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 detailLabel.tag = 20;
 [cell.contentView addSubview:detailLabel];
 detailLabel.layer.borderWidth = 0;
 detailLabel.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 detailLabel.layer.cornerRadius = 10;
 detailLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
 NSString *cellValue =[[FindTextValueArray objectAtIndex:0]textinfo];
 detailLabel.text=cellValue;
 detailLabel.editable=NO;
 [cell.contentView addSubview:detailLabel];
 [detailLabel release];
 
 
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageViewd.tag =555;
 [titleImageViewd addTarget:self action:@selector(showFullTXT:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release];
 }  
 else if((indexPath.row == 5)) 
 
 {
 NSLog(@" I M on %d",indexPath.row);
 
 
 UIImage *titleImage = [[UIImage alloc ] initWithData:
 [[FindWebValueArray objectAtIndex:0]webImg]];
 UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(20, 13, 240, 103)];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
 [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
 titleImageView.layer.borderWidth = 0;
 titleImageView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
 titleImageView.layer.cornerRadius = 10;
 titleImageView.layer.masksToBounds = YES;
 
 [cell.contentView addSubview:titleImageView];
 [titleImage release];
 [titleImageView release];
 UIImage *titleImaged =
 [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
 UIButton *titleImageViewd = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 50.0,35.0, 23.0)];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateNormal];
 [titleImageViewd setBackgroundImage:titleImaged forState:UIControlStateHighlighted];
 titleImageViewd.layer.masksToBounds = YES;
 [titleImageViewd.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
 titleImageViewd.tag =666;
 [titleImageViewd addTarget:self action:@selector(showFullWEB:) forControlEvents:UIControlEventTouchUpInside];
 [cell.contentView addSubview:titleImageViewd];
 [titleImageViewd release]; 
 
 
 
 }
 */

//}

//k++;
//return cell;

//}



//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//*/

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */
- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];    
    // Release any cached data, images, etc. that aren't in use.
}

#pragma mark - FGalleryViewControllerDelegate Methods


- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
	return [localImages count];
}


- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
    return FGalleryPhotoSourceTypeLocal;
}


- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
    
   	return @"";
}


- (NSString*)photoGallery:(FGalleryViewController*)gallery filePathForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return [localImages objectAtIndex:index];
}


-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [editButton removeFromSuperview];
        [backButton removeFromSuperview];
        [_exportButton removeFromSuperview];
        [_editButtoniOS7 removeFromSuperview];
        [_exportButton_iOS6 removeFromSuperview];
        
    }
    
}


- (void)viewDidUnload
{
    navBar = nil;
    _exportButton = nil;
    backButton = nil;
    editButton = nil;
	[super viewDidUnload];
}



@end













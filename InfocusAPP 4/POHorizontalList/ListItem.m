//
//  ListItem.m
//  POHorizontalList
//
//  Created by Polat Olu on 15/02/2013.
//  Copyright (c) 2013 Polat Olu. All rights reserved.
//

#import "ListItem.h"

@implementation ListItem

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image text:(NSString *)imageTitle
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setUserInteractionEnabled:YES];
        
        self.imageTitle = imageTitle;
        self.image = image;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];

        CALayer *imageViewLayer = [imageView layer];
        [imageViewLayer setMasksToBounds:YES];
        [imageViewLayer setCornerRadius:8.0];
        [imageViewLayer setBorderColor:[UIColor darkGrayColor].CGColor];
        [imageViewLayer setBorderWidth:1.0];
        
        //Steve - changes color of Text Note to White
        if ([self.imageTitle isEqualToString:@"Text"]) {
            
            imageViewLayer.backgroundColor = [UIColor whiteColor].CGColor;
        }
        else if ([self.imageTitle isEqualToString:@"Picture"]){
            
            imageViewLayer.backgroundColor = [UIColor clearColor].CGColor;
        }
        
        //Steve remove Title of image
        /*
        UILabel *title = [[UILabel alloc] init];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont boldSystemFontOfSize:12.0]];
        [title setOpaque: NO];
        [title setText:imageTitle];
         */
        
        imageRect = CGRectMake(0.0, 0.0, 72.0, 72.0);
        textRect = CGRectMake(0.0, imageRect.origin.y + imageRect.size.height + 10.0, 80.0, 20.0);
        
        //[title setFrame:textRect];
        [imageView setFrame:imageRect];
        
       // [self addSubview:title];
        [self addSubview:imageView];
        
        //NSLog(@"image = %@", image);
        
        if (IS_IOS_7) {
            
            self.backgroundColor = [UIColor whiteColor];
        }
        else{ //iOS6
            
            self.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];;
        }
        
    }
    
    return self;
}

@end

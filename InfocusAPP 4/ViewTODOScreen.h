//
//  ViewTODOScreen.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 7/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TODODataObject.h"

//#import "AdWhirlDelegateProtocol.h"
//#import "AdWhirlView.h"


@interface ViewTODOScreen : UIViewController <MFMailComposeViewControllerDelegate>{
	IBOutlet UIView *containerView;
	IBOutlet UITableView *toDoDetailTableView;
	TODODataObject *toDoDataObj; 
	
	NSInteger numberOfRows;
    IBOutlet UINavigationBar *navBar;
    //AdWhirlView *adView;
    
    //Alok added for fullproject name
    NSMutableArray *myArray;
      NSInteger retVal;
    
    NSInteger y;
    NSInteger x;
    NSInteger u;
    NSInteger v;
    
    NSString *yy;
    NSString *xx;
    NSString *uu;
    NSString *vv;
    NSString *Pname;
    
   
    UIButton *starButton;
    IBOutlet UIButton *shareButton;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *EditButton;
    
}
//@property(nonatomic,retain)AdWhirlView *adView;
@property(nonatomic, strong) TODODataObject *toDoDataObj;  

-(id)initWithToDoObject:(TODODataObject *) appObj;
-(IBAction)backButton_Clicked:(id) sender;
-(IBAction)editButton_Clicked:(id) sender;
-(void)	starred_buttonClicked:(id)sender;
-(NSInteger)fetch_level_one:(NSInteger)projid;
- (IBAction)shareButton_Clicked:(UIButton *)sender; //Steve

@end

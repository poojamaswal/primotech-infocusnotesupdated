//
//  QuickViewSubfolder.m
//  Organizer
//
//  Created by Naresh Chauhan on 10/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "QuickViewSubfolder.h"
#import "MyTreeViewCell.h"
#import "MyTreeNode.h"
#import "OrganizerAppDelegate.h"
#import "AddProjectDataObject.h"
#import "AddProjectViewCantroller.h"


@implementation QuickViewSubfolder
@synthesize getRootValue;
@synthesize LevelOneArray;
@synthesize LevelOneNameArray;
@synthesize LevelTwoArray;
@synthesize LevelTwoNameArray;
@synthesize LevelThreeArray;
@synthesize LevelThreeNameArray;
@synthesize LevelFourArray,getoptn;
@synthesize LevelFourNameArray,Sql;
@synthesize getBvalue,getLvalue,Lenght,Lenght2,Lenght3,getnewName;
@synthesize LevelOneBArray,LevelTwoBArray,LevelThreeBArray,LevelFourBArray,getPIDvalue;
@synthesize LevelOneisTempArray,LevelTwoisTempArray,LevelThreeisTempArray,LevelFourisTempArray;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	LevelOneArray = [[NSMutableArray alloc]init];
	LevelOneNameArray = [[NSMutableArray alloc]init];
	LevelTwoArray = [[NSMutableArray alloc]init];
	LevelTwoNameArray = [[NSMutableArray alloc]init];
	LevelThreeArray = [[NSMutableArray alloc]init];
	LevelThreeNameArray = [[NSMutableArray alloc]init];
	LevelFourArray = [[NSMutableArray alloc]init];
	LevelFourNameArray = [[NSMutableArray alloc]init];
	LevelOneBArray = [[NSMutableArray alloc]init];
	LevelTwoBArray = [[NSMutableArray alloc]init];
	LevelThreeBArray = [[NSMutableArray alloc]init];
	LevelFourBArray = [[NSMutableArray alloc]init];
	
	LevelOneisTempArray = [[NSMutableArray alloc]init];
	LevelTwoisTempArray = [[NSMutableArray alloc]init];
	LevelThreeisTempArray = [[NSMutableArray alloc]init];
	LevelFourisTempArray = [[NSMutableArray alloc]init];

	NSLog(@"ROOT %d",getRootValue);
	//NSLog(@"BELONGS %d",getBvalue);
	NSLog(@"LEVEL %d",getLvalue);
	NSLog(@"PIDDDDD %d",getPIDvalue);
	NSLog(@"%@",getnewName);
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];	
	
	[LevelOneArray removeAllObjects];
	[LevelOneNameArray removeAllObjects];
	[LevelTwoArray removeAllObjects];
	[LevelTwoNameArray removeAllObjects];
	[LevelThreeArray removeAllObjects];
	[LevelThreeNameArray removeAllObjects];
	[LevelFourArray removeAllObjects];
	[LevelFourNameArray removeAllObjects];
	[LevelOneBArray removeAllObjects];
	[LevelTwoBArray removeAllObjects];
	[LevelThreeBArray removeAllObjects];
	[LevelFourBArray removeAllObjects];
	[LevelOneisTempArray removeAllObjects];
	[LevelTwoisTempArray removeAllObjects];
	[LevelThreeisTempArray removeAllObjects];
	[LevelFourisTempArray removeAllObjects];
	
	
	[self FindRecoredOne];
	
	NSLog(@"Level One %@",LevelOneArray);
	NSLog(@"Level One %@",LevelOneNameArray);
	NSLog(@"Level One %@",LevelOneBArray);
	
	treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
	node1 = [[MyTreeNode alloc] initWithValue:[LevelOneNameArray objectAtIndex:0]];
	node1.ID=1;
	node1.LevelVal=0;
	[treeNode addChild:node1];
	
	[self FindRecoredTwo:[[LevelOneArray objectAtIndex:0]intValue]];
	NSLog(@" Level Two %@",LevelTwoArray);
	NSLog(@" Level Two %@",LevelTwoBArray);
	NSLog(@" Level Two %@",LevelTwoNameArray);
	NSLog(@" Level Two %@",LevelTwoisTempArray);
	
	for(int m=0;m<[LevelTwoArray count];m++)
	{
		
	node2 = [[MyTreeNode alloc] initWithValue:[LevelTwoNameArray objectAtIndex:m]];
	node2.ID = 2;
	node2.LevelVal=1;
	node2.isTemp=[[LevelTwoisTempArray objectAtIndex:m]intValue];
	[node1 addChild:node2];
		
	[self FindRecoredThree:[[LevelTwoArray objectAtIndex:m]intValue]];
	NSLog(@"Level Three%@",LevelThreeArray);
	NSLog(@"Level Three%@",LevelThreeBArray);
	NSLog(@"Level Three%@",LevelThreeNameArray);

	for(int k=0;k<[LevelThreeArray count];k++)
	{
				
		node3 = [[MyTreeNode alloc] initWithValue:[LevelThreeNameArray objectAtIndex:k]];
		node3.ID=3;
		node3.isTemp=[[LevelThreeisTempArray objectAtIndex:k]intValue];
		[node2 addChild:node3];	
			
	[self FindRecoredFour:[[LevelThreeArray objectAtIndex:k]intValue]];
		NSLog(@" Level Four%@",LevelFourArray);
		NSLog(@" Level Four%@",LevelFourBArray);
		NSLog(@" Level Four%@",LevelFourNameArray);
			
		for(int l=0;l<[LevelFourArray count];l++)
		{		
			node4 = [[MyTreeNode alloc] initWithValue:[LevelFourNameArray objectAtIndex:l]];
			node4.ID=4;
			node3.isTemp=[[LevelFourisTempArray objectAtIndex:l]intValue];
			[node3 addChild:node4];		
		}
			
	
	
	}	
}	
NSLog(@" OUT Level Three%@",LevelThreeArray);
} 
		
-(IBAction)Save_Now:(id)sender
{
	
	[self updateNow];	
	
	
[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES]; 

	
	
	
//if(getoptn)
//{
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setBelongValue:getPIDvalue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setLevelValue:getLvalue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setGetfId:getRootValue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setLEVELOPT:YES];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setGetFName:getnewName];
//	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:3] animated:YES]; 
//}
//else
//{
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setBelongValue:getPIDvalue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setLevelValue:getLvalue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setGetfId:getRootValue];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setLEVELOPT:YES];
//	[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -3)]setGetFName:getnewName];
//
//	[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES]; 
//		
//}
}

-(void)updateNow
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		sql = [[NSString alloc]initWithFormat:@"Update projectMaster set isTemp=%d where isTemp=%d",0,1];
		NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	if(SQLITE_DONE != sqlite3_step(insertProjectData))
	{
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	} 
	else 
	{
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	}	
}

-(void)FindRecoredOne
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID,projectName,level,belongstoID,isTemp from ProjectMaster where projectId=%d AND level =%d",getRootValue,0];
		NSLog(@" query %@",Sql);
			
			//SELECT * FROM table_name WHERE columnname LIKE value%
			if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
			{
				NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
			}
			else
			{
				
				[LevelOneArray removeAllObjects];
				[LevelOneNameArray removeAllObjects];
				[LevelOneBArray removeAllObjects];
				[LevelOneisTempArray removeAllObjects];
				
				while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
				{
					AddProjectDataObject  *projectdataobj = [[AddProjectDataObject alloc]init];
					[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
					[LevelOneArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
					[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
					[LevelOneNameArray addObject:projectdataobj.projectname];
					[projectdataobj setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
					[LevelOneBArray addObject:[NSNumber numberWithInt:projectdataobj.belongstoid]];
					[projectdataobj  setTempValue:sqlite3_column_int(selectNotes_Stmt, 4)];
					[LevelOneisTempArray addObject:[NSNumber numberWithInt:projectdataobj.tempValue]];
				}							        
				sqlite3_finalize(selectNotes_Stmt);		
				selectNotes_Stmt = nil;        
			}
			
		}
}
	
	



-(void)FindRecoredTwo:(NSInteger)Rval;
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,isTemp from ProjectMaster where belongstoID=%d AND level =%d",Rval,1];
		NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[LevelTwoArray  removeAllObjects];
			[LevelTwoNameArray removeAllObjects];
			[LevelTwoBArray removeAllObjects];
			[LevelTwoisTempArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjone = [[AddProjectDataObject alloc]init];
				[projectdataobjone setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[LevelTwoArray addObject:[NSNumber numberWithInt:projectdataobjone.projectid]];
				
				[projectdataobjone setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[LevelTwoNameArray addObject:projectdataobjone.projectname];
				[projectdataobjone setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[LevelTwoBArray addObject:[NSNumber numberWithInt:projectdataobjone.belongstoid]];
				[projectdataobjone setTempValue:sqlite3_column_int(selectNotes_Stmt, 4)];
				[LevelTwoisTempArray addObject:[NSNumber numberWithInt:projectdataobjone.tempValue]];
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
	
}

-(void)FindRecoredThree:(NSInteger)Rval
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID,isTemp from ProjectMaster where belongstoID=%d AND level =%d",Rval,2];
		NSLog(@" =========query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[LevelThreeArray  removeAllObjects];
			[LevelThreeNameArray  removeAllObjects];
			[LevelThreeBArray  removeAllObjects];
			[LevelThreeisTempArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjtwo = [[AddProjectDataObject alloc]init];
				[projectdataobjtwo setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[LevelThreeArray addObject:[NSNumber numberWithInt:projectdataobjtwo.projectid]];
				
				[projectdataobjtwo setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[LevelThreeNameArray addObject:projectdataobjtwo.projectname];
				[projectdataobjtwo setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[LevelThreeBArray addObject:[NSNumber numberWithInt:projectdataobjtwo.belongstoid]];
				
				[projectdataobjtwo setTempValue:sqlite3_column_int(selectNotes_Stmt,4)];
				[LevelThreeisTempArray addObject:[NSNumber numberWithInt:projectdataobjtwo.tempValue]];
	
				
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
	
}


-(void)FindRecoredFour:(NSInteger)Rval
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,belongstoID from ProjectMaster where belongstoID=%d AND level =%d",Rval,3];
		NSLog(@" =========query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[LevelFourArray  removeAllObjects];
			[LevelFourNameArray  removeAllObjects];
			[LevelFourBArray  removeAllObjects];
			[LevelFourisTempArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjthree = [[AddProjectDataObject alloc]init];
				[projectdataobjthree setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[LevelFourArray addObject:[NSNumber numberWithInt:projectdataobjthree.projectid]];
				
				[projectdataobjthree setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[LevelFourNameArray addObject:projectdataobjthree.projectname];
				[projectdataobjthree setBelongstoid:sqlite3_column_int(selectNotes_Stmt, 3)];
				[LevelFourBArray addObject:[NSNumber numberWithInt:projectdataobjthree.belongstoid]];
				[projectdataobjthree setTempValue:sqlite3_column_int(selectNotes_Stmt, 4)];
				[LevelFourisTempArray addObject:[NSNumber numberWithInt:projectdataobjthree.tempValue]];
				
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(IBAction)Cancel_btn:(id)sender
{
	
	[self deleteFirst];
	
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)deleteFirst
{
	
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteproject= nil;
	
	if(deleteproject == nil) 
	{
		sql = [[NSString alloc]initWithFormat:@"delete from ProjectMaster where isTemp = %d ",1];
		
		NSLog(@"%@",sql);
		
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	
	if (SQLITE_DONE != sqlite3_step(deleteproject))
    {
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteproject);
		
	} 
    else
    {
		sqlite3_finalize(deleteproject);
	}
}



#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [treeNode descendantCount];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
	
   	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];  
	

	//cell.textLabel.text=node.value;
	
	if(node.ID==1)
	{
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,9,320,30)];
		myLabel.text = node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		if(node.colorOPT==YES)
		{
			myLabel.textColor = [UIColor redColor];
		}
		else {
			myLabel.textColor = [UIColor blackColor];
			
		}
		[cell.contentView addSubview:myLabel];	
	}
	
	if(node.ID==2)
	{
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(70,7,320,30)];
		myLabel.text = node.value;
		if(node.isTemp==1)
		{
			myLabel.textColor = [UIColor redColor];
		}
		else {
			myLabel.textColor = [UIColor blackColor];
			
		}


		myLabel.backgroundColor = [UIColor clearColor];
		
		[cell.contentView addSubview:myLabel];	
	}
	
	if(node.ID==3)
	{
		
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,7,320,30)];
		myLabel.text = node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		if(node.isTemp==1)
		{
			myLabel.textColor = [UIColor redColor];
		}
		else {
			myLabel.textColor = [UIColor blackColor];
			
		}
		[cell.contentView addSubview:myLabel];	
	}
	
	if(node.ID==4)
	{
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(130,7,320,30)];
		myLabel.text = node.value;
		myLabel.backgroundColor = [UIColor clearColor];
		if(node.isTemp==1)
		{
			myLabel.textColor = [UIColor redColor];
		}
		else {
			myLabel.textColor = [UIColor blackColor];
			
		}
		[cell.contentView addSubview:myLabel];	
	}
	

	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
	
}

#pragma mark -
#pragma mark Table view delegate


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

//
//  SettingsViewController.h
//  Organizer
//
//  Created by alok gupta on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpViewController.h"


//@interface SettingsViewController : UIViewController<MFMailComposeViewControllerDelegate, UITextFieldDelegate>
@interface SettingsViewController : UITableViewController <MFMailComposeViewControllerDelegate, UITextFieldDelegate>
{
    IBOutlet UINavigationBar *navBar;
    IBOutlet UILabel *lbl_export;
    IBOutlet UIButton *btn_export;
    IBOutlet UIImageView *img_export;
    BOOL IsAlert;
    IBOutlet UIButton *btn_backup;
    IBOutlet UILabel *lbl_backup;
    NSString * ItuneURL;
    NSString * AppsTarget;
    IBOutlet UIImageView *img_export_line;
    
    //Steve added
    IBOutlet UIButton *VTTinfoButton;
    IBOutlet UIView *infoButtonView; 
    IBOutlet UIButton *okButtonVTTAuth; 
    IBOutlet UIButton *cancelButtonVTTAuth; 
    IBOutlet UITextField *userNameVTTAuthTextField;
    IBOutlet UITextField *passwordVTTAuthTextField;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UILabel *passwordLabel;
    IBOutlet UILabel *youAreAuthLabel;
    IBOutlet UILabel *notAvailableOnFREELabel;
    
    //Steve added
    IBOutlet UIButton *helpButton;
    IBOutlet UIButton *faqButton;
    IBOutlet UIButton *reccomendButton;
    IBOutlet UIButton *giftButton;
    IBOutlet UIButton *rateButton;
    IBOutlet UIButton *supportEmailButton;
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    //Steve added
    UITapGestureRecognizer *tap;
}

@property(nonatomic, strong) IBOutlet UITextField *userNameVTTAuthTextField; //Steve
@property(nonatomic, strong) IBOutlet UITextField *passwordVTTAuthTextField; //Steve


-(IBAction)BackButtonClicked:(id)sender;
-(IBAction)HelpButtonClicked:(id)sender;
-(IBAction)RecommendButtonClicked:(id)sender;
-(IBAction)RateButtonClicked:(id)sender;
-(IBAction)GiftButtonClicked:(id)sender;
-(IBAction)ExportDataToProVersion:(id)sender;
-(IBAction)CreateBackup:(id)sender;
- (IBAction)VTTinfoButton_Clicked:(id)sender;//STeve
- (IBAction)okBuutonVTTAuth_Clicked:(id)sender; //Steve
- (IBAction)cancelButtonVTTAuth_Clicked:(id)sender; //Steve


- (IBAction)faqButton_Clicked:(id)sender;
- (IBAction)supportEmailButton_Clicked:(id)sender;
-(void)Exportdata;
- (BOOL)textFieldShouldReturn:(UITextField *)textField; //Steve

@end

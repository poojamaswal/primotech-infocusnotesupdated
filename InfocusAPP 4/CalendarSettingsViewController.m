//
//  CalendarTableViewController.m
//  Organizer
//
//  Created by Steven Abrams on 1/31/13.
//
//

#import "CalendarSettingsViewController.h"

@interface CalendarSettingsViewController ()

- (IBAction)listViewSwitch_ValueChanged:(UISwitch *)sender;
- (IBAction)dayViewSwitch_ValueChanged:(UISwitch *)sender;
- (IBAction)weekViewSwitch_ValueChanged:(UISwitch *)sender;
- (IBAction)monthViewSwitch_ValueChanged:(UISwitch *)sender;


@end

@implementation CalendarSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


    if (IS_IOS_7) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.view.backgroundColor = [UIColor whiteColor];
        
        //pooja-iPad
       if(IS_IPAD)
       {
           [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
           self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
           self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
           self.navigationController.navigationBar.translucent = YES;

           listViewSwitch.frame  = CGRectMake(656, 8, 49, 31);
           dayViewSwitch.frame   = CGRectMake(656, 8, 49, 31);
           weekViewSwitch.frame  = CGRectMake(656, 8, 49, 31);
           monthViewSwitch.frame = CGRectMake(656, 8, 49, 31);
       }
        else
        {
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            listViewSwitch.frame = CGRectMake(251, 8, 51, 31);
            dayViewSwitch.frame = CGRectMake(251, 8, 51, 31);
            weekViewSwitch.frame = CGRectMake(251, 8, 51, 31);
            monthViewSwitch.frame = CGRectMake(251, 8, 51, 31);
        }
        
          self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        //
        
        
        listViewSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        dayViewSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        weekViewSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        monthViewSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0];
        
    }

    
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults integerForKey:@"DefaultCalendar"] == LIST_BUTTON_TAG){
        [listViewSwitch setOn:YES];
        [dayViewSwitch setOn:NO];
        [weekViewSwitch setOn:NO];
        [monthViewSwitch setOn:NO];
    }
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == DAY_BUTTON_TAG){
        [dayViewSwitch setOn:YES];
        [listViewSwitch setOn:NO];
        [weekViewSwitch setOn:NO];
        [monthViewSwitch setOn:NO];
    }
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == WEEK_BUTTON_TAG){
        [weekViewSwitch setOn:YES];
        [listViewSwitch setOn:NO];
        [dayViewSwitch setOn:NO];
        [monthViewSwitch setOn:NO];
    }
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == MONTH_BUTTON_TAG){
        [monthViewSwitch setOn:YES];
        [listViewSwitch setOn:NO];
        [dayViewSwitch setOn:NO];
        [weekViewSwitch setOn:NO];
    }
  
    
}

- (IBAction)listViewSwitch_ValueChanged:(UISwitch *)sender {
    
     NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setInteger:LIST_BUTTON_TAG forKey:@"DefaultCalendar"];
    [sharedDefaults synchronize];
  
    [listViewSwitch setOn:YES animated:YES];
    [dayViewSwitch setOn:NO animated:YES];
    [weekViewSwitch setOn:NO animated:YES];
    [monthViewSwitch setOn:NO animated:YES];
    
}

- (IBAction)dayViewSwitch_ValueChanged:(UISwitch *)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setInteger:DAY_BUTTON_TAG forKey:@"DefaultCalendar"];
    [sharedDefaults synchronize];
    
    [dayViewSwitch setOn:YES animated:YES];
    [listViewSwitch setOn:NO animated:YES];
    [weekViewSwitch setOn:NO animated:YES];
    [monthViewSwitch setOn:NO animated:YES];
    
}

- (IBAction)weekViewSwitch_ValueChanged:(UISwitch *)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setInteger:WEEK_BUTTON_TAG forKey:@"DefaultCalendar"];
    [sharedDefaults synchronize];
    
    [weekViewSwitch setOn:YES animated:YES];
    [listViewSwitch setOn:NO animated:YES];
    [dayViewSwitch setOn:NO animated:YES];
    [monthViewSwitch setOn:NO animated:YES];
    
}

- (IBAction)monthViewSwitch_ValueChanged:(UISwitch *)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setInteger:MONTH_BUTTON_TAG forKey:@"DefaultCalendar"];
    [sharedDefaults synchronize];
    
    [monthViewSwitch setOn:YES animated:YES];
    [listViewSwitch setOn:NO animated:YES];
    [dayViewSwitch setOn:NO animated:YES];
    [weekViewSwitch setOn:NO animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    listViewSwitch = nil;
    dayViewSwitch = nil;
    weekViewSwitch = nil;
    monthViewSwitch = nil;
    [super viewDidUnload];
}



@end

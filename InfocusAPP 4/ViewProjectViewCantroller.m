//
//  ViewProjectViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
// 8285303764       dla455_10 kalpna sherma 
//


#define EDit_BTN 9000
#define SCROLL_VIEW_TAG				61111
#define Progress_BTN_TAG			71111
#define BADGE_VIEW_TAG				81111
#define TABLE_ROW_HEIGHT_IPHONE		55
#define TABLE_ROW_HEIGHT_IPAD		70

#import "ViewProjectViewCantroller.h"
#import "MyTreeViewCell.h"
#import "OrganizerAppDelegate.h"
#import "AddProjectDataObject.h"
#import "ToDoParentController.h"
#import "AddProjectViewCantroller.h"
#import "AddProjectViewCantroller.h"
#import "ToDoViewController.h"
#import "TODODataObject.h"
#import "CustomBadge.h"
#import "EditProjectViewCantroller.h"
#import "AddNewNotesViewCantroller.h"
#import "NotesFullViewCantroller.h"
//#import "AdWhirlView.h"


@interface ViewProjectViewCantroller()

@property (strong, nonatomic) IBOutlet UIButton *editButtoniOS7;


@end


@implementation ViewProjectViewCantroller
@synthesize PIDval,getLevelRootArray,pidleveloneArray,pidleveltwoArray,pidlevelthreeArray;
@synthesize getONE;
@synthesize pidleveloneNameArray,getLevelRootNameArray,pidleveltwoNameArray,pidlevelthreeNameArray;
@synthesize getLevelRootLevelArray,pidleveloneLevelArray,pidleveltwoLevelArray,pidlevelthreeLevelArray,getTODOIDeArray,getTODOTitleArray;
@synthesize getTODOTitleOneArray,getTODOIDOneArray;
@synthesize getTODOTwoArray,getTODOTwoNameArray;
@synthesize getTODOThreeArray;
@synthesize getTODOThreeNameArray;
@synthesize getLevelRootTextTypeArray;
@synthesize getLevelRootBinaryArray;
@synthesize TODOTitletypeOneArray;
@synthesize TODOtitleBinaryOneArray;
@synthesize TODOTitletypeTwoArray;
@synthesize TODOtitleBinaryTwoArray;
@synthesize TODOTitletypeThreeArray;
@synthesize TODOtitleBinaryThreeArray;
@synthesize getLevelRootimageArray;
@synthesize pidleveloneImgArray;
@synthesize pidleveltwoImgArray;
@synthesize pidlevelthreeImgArray;
@synthesize toDODataObject;
@synthesize toDODataObjectOne;
@synthesize toDODataObjectTwo;
@synthesize toDODataObjectThree,editOPT;
@synthesize getLevelRootBinarLargeyArray;
@synthesize TODOtitleBinaryLargeOneArray;
@synthesize TODOtitleBinaryLargeTwoArray;
@synthesize TODOtitleBinaryLargeThreeArray,isEditNow,delegate,optnView;
@synthesize priorityRootArray,priorityOneArray,priorityTwoArray,priorityThreeArray,isEdit;
@synthesize pidlevelonePriorityArray,pidleveltwoPriorityArray,pidlevelthreePriorityArray;
@synthesize getLevelRootObjectTypeArray,pidleveloneObjectTypeArray,pidleveltwoObjectTypeArray,pidlevelthreeObjectTypeArray;
@synthesize getLevelRootPriortyArray;

@synthesize pidleveloneInclusiveArray, pidleveltwoInclusiveArray, pidlevelthreeInclusiveArray; // Anil
@synthesize pidRootInclusiveArray;
//@synthesize adView;
@synthesize mytable;

-(void)viewDidLoad
{
   
    
    if(IS_LITE_VERSION)
    {
        [super viewDidLoad];
        [mytable setFrame:CGRectMake(0,41,320, 370)];
    }
    
    [super viewDidLoad];
    
        _dictData = [[NSMutableDictionary alloc]init];
     NSLog(@"viewProjectViewController");
    
    mytable.delegate=self;
    mytable.dataSource=self;
    
    
	titleImage =[UIImage imageNamed:@"DetailsDisclouserBtn.png"];
	getLevelRootArray=[[NSMutableArray alloc]init];
	getLevelRootNameArray=[[NSMutableArray alloc]init];
	getLevelRootTextTypeArray=[[NSMutableArray alloc]init];
	getLevelRootBinaryArray=[[NSMutableArray alloc]init];
	pidleveloneArray = [[NSMutableArray alloc]init];
	pidleveloneNameArray=[[NSMutableArray alloc]init];
	pidleveltwoArray=[[NSMutableArray alloc]init];
	pidleveltwoNameArray=[[NSMutableArray alloc]init];
    pidlevelthreeNameArray=[[NSMutableArray alloc]init];
	pidlevelthreeArray=[[NSMutableArray alloc]init];
	getLevelRootLevelArray=[[NSMutableArray alloc]init];
	pidleveloneLevelArray=[[NSMutableArray alloc]init];
	pidleveltwoLevelArray=[[NSMutableArray alloc]init];
	pidlevelthreeLevelArray=[[NSMutableArray alloc]init];
	getTODOIDeArray=[[NSMutableArray alloc]init];
	getTODOTitleArray=[[NSMutableArray alloc]init];
	getTODOTitleOneArray=[[NSMutableArray alloc]init];
	getTODOIDOneArray=[[NSMutableArray alloc]init];
	getTODOTwoArray=[[NSMutableArray alloc]init];
	getTODOTwoNameArray=[[NSMutableArray alloc]init];
	getTODOThreeArray=[[NSMutableArray alloc]init];
	getTODOThreeNameArray=[[NSMutableArray alloc]init];
	TODOTitletypeOneArray=[[NSMutableArray alloc]init];
	TODOtitleBinaryOneArray=[[NSMutableArray alloc]init];
	TODOTitletypeTwoArray=[[NSMutableArray alloc]init];
	TODOtitleBinaryTwoArray=[[NSMutableArray alloc]init];
	TODOTitletypeThreeArray=[[NSMutableArray alloc]init];
	TODOtitleBinaryThreeArray=[[NSMutableArray alloc]init];
    getLevelRootimageArray=[[NSMutableArray alloc]init];
    pidleveloneImgArray=[[NSMutableArray alloc]init];
    pidleveltwoImgArray=[[NSMutableArray alloc]init];
    pidlevelthreeImgArray=[[NSMutableArray alloc]init];
    getLevelRootBinarLargeyArray =[[NSMutableArray alloc]init];
    TODOtitleBinaryLargeOneArray =[[NSMutableArray alloc]init];
    TODOtitleBinaryLargeTwoArray =[[NSMutableArray alloc]init];
    TODOtitleBinaryLargeThreeArray =[[NSMutableArray alloc]init];
	getONE=[[NSMutableArray alloc]init];
	priorityRootArray =[[NSMutableArray alloc]init];
	priorityOneArray =[[NSMutableArray alloc]init];
	priorityTwoArray =[[NSMutableArray alloc]init];
	priorityThreeArray =[[NSMutableArray alloc]init];
	progresRootArray =[[NSMutableArray alloc]init];
	progresOneArray=[[NSMutableArray alloc]init];
	progresTwoArray=[[NSMutableArray alloc]init];
	progresThreetArray=[[NSMutableArray alloc]init];
    
    
    getLevelRootPriortyArray = [[NSMutableArray alloc]init];
	
    pidlevelonePriorityArray = [[NSMutableArray alloc]init];
    
    pidleveltwoPriorityArray = [[NSMutableArray alloc]init];
    
    pidlevelthreePriorityArray = [[NSMutableArray alloc]init];
    
	
    getLevelRootObjectTypeArray = [[NSMutableArray alloc]init];
    pidleveloneObjectTypeArray = [[NSMutableArray alloc]init];
    pidleveltwoObjectTypeArray = [[NSMutableArray alloc]init];
    pidlevelthreeObjectTypeArray = [[NSMutableArray alloc]init];
    
    
    pidleveloneInclusiveArray =[[NSMutableArray alloc]init];
    pidleveltwoInclusiveArray =[[NSMutableArray alloc]init];
    pidlevelthreeInclusiveArray =[[NSMutableArray alloc]init];
    pidRootInclusiveArray = [[NSMutableArray alloc]init];
    
 	
}	


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}


-(void)viewWillAppear:(BOOL)animated
{
   // NSLog(@"viewWillAppear");
    
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    
    UIBarButtonItem *doneButton_iOS7;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        projectViewLabel.hidden = YES;
        
        self.navigationController.navigationBarHidden = NO;
   
        if (IS_IOS_7) {
            
            
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            self.view.backgroundColor = [UIColor blackColor];
            mytable.backgroundColor = [UIColor whiteColor];
            mytable.separatorColor = [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
            
#pragma mark -- IPad Primotech
            
            if(IS_IPAD)
            {
                self.title = @"Project View";
                
                
                UIButton * btnEdit;
                btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
                btnEdit.frame = CGRectMake(0, 0, 40, 29);
                [btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
                btnEdit.showsTouchWhenHighlighted = YES;
                [btnEdit addTarget:self action:@selector(Edit_Project:) forControlEvents:UIControlEventTouchUpInside];
                // Perform Action for edit button
                UIBarButtonItem * edit = [[UIBarButtonItem alloc]initWithCustomView:btnEdit];
                
               
                
                UIButton * btnDone;
                btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
                btnDone.frame = CGRectMake(0, 0, 50, 29);
                [btnDone setTitle:@"Done" forState:UIControlStateNormal];
                btnDone.showsTouchWhenHighlighted = YES;
                [btnDone addTarget:self action:@selector(Done_Project:) forControlEvents:UIControlEventTouchUpInside];
                // perform Done Button Action also
                //UIBarButtonItem * done = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
                
                self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:edit,nil];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
                
            }
            else
            {
                
            self.title = @"Project View";
        
            
            //Edit Button made in NIB. Note - floating on page, not on view in NIB
            _editButtoniOS7.frame = CGRectMake(218, 8, 51, 29); //CGRectMake(207, 7, 51, 29)
            _editButtoniOS7.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:19];
            [self.navigationController.navigationBar addSubview:_editButtoniOS7];
            
            
            //done Button
            doneButton.hidden = YES;
            doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(Done_Project:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
                if (IS_IPHONE_5) {
                    mytable.frame = CGRectMake(0, -64, 320, 480 + 88 + 64);
                    self.view.frame = CGRectMake(0, 0, 320, 480 + 88);
                    self.view.bounds = CGRectMake(0, 0, 320, 568);
                }
                else{
                    mytable.frame = CGRectMake(0, -64, 320, 480 + 64);
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    self.view.bounds = CGRectMake(0, 0, 320, 480);
                }
                
                
                UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
                [mytable setContentInset:inset];
            
            }
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245)
            { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor: [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
                _editButtoniOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
              if(IS_IPAD)
              {
              
              }
                else
                {
                    
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                
                _editButtoniOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                
            }
            
        }
        else{//iOS 6
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            [mytable setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
            mytable.separatorColor = [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f];  //Steve added

            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            //self.title = @"Project View";
            
            //Needed to add a small label on Navigation
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 5, 100, 30)];
            titleLabel.text = @"Project View";
            
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.numberOfLines = 1;
            //titleLabel.minimumFontSize = 8.;
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.font=[UIFont boldSystemFontOfSize:15.0];
            projectViewLabel = titleLabel;
            [self.navigationController.navigationBar addSubview:projectViewLabel];
            
            UIImage *imglOne=[UIImage imageNamed:@"Edit.png"];
            UIButton *EditBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [EditBTN setBackgroundImage:imglOne forState:UIControlStateNormal];
            [EditBTN addTarget:self action:@selector(Edit_Project:) forControlEvents:UIControlEventTouchUpInside];
            EditBTN.frame = CGRectMake(207, 7, 51, 29.0);  //Steve  CGRectMake(207, 5, 53, 32)
            editButton = EditBTN;
            [self.navigationController.navigationBar addSubview:editButton];
            
            UIImage *imglTwo=[UIImage imageNamed:@"Done.png"];
            UIButton *DoneBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [DoneBTN setBackgroundImage:imglTwo forState:UIControlStateNormal];
            [DoneBTN addTarget:self action:@selector(Done_Project:) forControlEvents:UIControlEventTouchUpInside];
            DoneBTN.frame = CGRectMake(264.0, 7.00, 51, 29.0);  //Steve  CGRectMake(265.0,5.00,54,32.0)
            doneButton = DoneBTN;
            [self.navigationController.navigationBar addSubview:doneButton];
            
            
             if (IS_IPHONE_5) {
                mytable.frame = CGRectMake(0, 44 - 44, 320, 416 + 88);
                self.view.frame = CGRectMake(0, 0, 320, 460 + 88);
            }
            else{
                mytable.frame = CGRectMake(0, 44 - 44, 320, 416);
                self.view.frame = CGRectMake(0, 0, 320, 460);
            }

        }
        
        
    }
    else{ //Old Navigation
        
        self.navigationController.navigationBarHidden = YES;
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        UIImage *imglOne=[UIImage imageNamed:@"Edit.png"];
        UIButton *EditBTN = [UIButton buttonWithType:UIButtonTypeCustom];
        [EditBTN setBackgroundImage:imglOne forState:UIControlStateNormal];
        [EditBTN addTarget:self action:@selector(Edit_Project:) forControlEvents:UIControlEventTouchUpInside];
        EditBTN.frame = CGRectMake(207,7.00,51,29.0);  //Steve  CGRectMake(211.0,5.00,54,32.0)
        [self.view addSubview:EditBTN];
        
        UIImage *imglTwo=[UIImage imageNamed:@"Done.png"];
        UIButton *DoneBTN = [UIButton buttonWithType:UIButtonTypeCustom];
        [DoneBTN setBackgroundImage:imglTwo forState:UIControlStateNormal];
        [DoneBTN addTarget:self action:@selector(Done_Project:) forControlEvents:UIControlEventTouchUpInside];
        DoneBTN.frame = CGRectMake(264.0,7.00,51.0,29.0);  //Steve  CGRectMake(265.0,5.00,54,32.0)
        [self.view addSubview:DoneBTN];
        
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        [mytable setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
        mytable.separatorColor = [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f];  //Steve added

        
    }
    
    // ***************** Steve End ***************************
    
        
    if ([sharedDefaults boolForKey:@"FirstLaunch_ProjectView"])
    {
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
        [helpView addGestureRecognizer:tapHelpView];
        
        UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        
        swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpView addGestureRecognizer:swipeUpHelpView];
        [helpView addGestureRecognizer:swipeLeftHelpView];
        [helpView addGestureRecognizer:swipeRightHelpView];

        helpView.hidden = NO;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            self.navigationController.navigationBarHidden = NO;
        }
        
        /*
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 1.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
         */
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_ProjectView"];  //Steve add back - commented for Testing
        [sharedDefaults synchronize];
        

    }
    
	[self FindRecored];
    
	if([getLevelRootArray count]>0)
	{
		treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
		node1 = [[MyTreeNode alloc] initWithValue:[getLevelRootNameArray objectAtIndex:0]];
		node1.ID=PIDval;
		node1.prntID=PIDval;
		node1.LevelVal=0;
		node1.isBTN=NO;
		node1.Fimg  =[getLevelRootimageArray objectAtIndex:0];
        node1.projectPriorty= [getLevelRootPriortyArray objectAtIndex:0];
        node1.inclusive = [[pidRootInclusiveArray objectAtIndex:0] intValue]; // Anil
		[treeNode addChild:node1];
        
        
		[self FindTodo];
        [self FindNote];
        
		if([getTODOIDeArray count]>0)
		{
			for(int ti=0;ti<[getTODOIDeArray count];ti++)
			{
				nodeT1=[[MyTreeNode alloc] initWithValue:[getTODOTitleArray objectAtIndex:ti]];
				nodeT1.TODO =0;
				nodeT1.isBTN=YES;
				nodeT1.TODOid=[[getTODOIDeArray objectAtIndex:ti]intValue];
				nodeT1.titleType=[getLevelRootTextTypeArray objectAtIndex:ti];
				nodeT1.titlebinary=[getLevelRootBinaryArray objectAtIndex:ti];
				nodeT1.PriorityTODO=[priorityRootArray objectAtIndex:ti];
				nodeT1.PROGRES=[[progresRootArray objectAtIndex:ti]intValue];
                
                if([[getLevelRootObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                {
                    nodeT1.objectType = NoteVal;
                }
                else
                {
                    nodeT1.objectType = ToDo;
                    
                }
                
				[node1 addChild:nodeT1];
			}
			
		}
 		[self getLevelOne:[[getLevelRootArray objectAtIndex:0]intValue]];
        
        
        //Steve -  fixes bug when a To-do or Note is deleted in another module, the Collapse "+" was still showing even when there were no folders & to-do's under it
        if (pidleveloneArray.count == 0 && getLevelRootObjectTypeArray.count == 0) {
            
            node1.inclusive = YES; //changes to it's Expanded "-"
            [self updateInclusiveValue:node1.inclusive :node1.ID]; //Method stores inclusive value
        }
        
 		for(int m=0;m<[pidleveloneArray count];m++)
		{
			
			node2 = [[MyTreeNode alloc] initWithValue:[pidleveloneNameArray objectAtIndex:m]];
			node2.ID = [[pidleveloneArray objectAtIndex:m]intValue];
			node2.prntID=PIDval;
			node2.LevelVal=1;
			node2.isBTN=NO;
			node2.Fimg=[pidleveloneImgArray objectAtIndex:m];
            node2.projectPriorty= [pidlevelonePriorityArray objectAtIndex:m];
            node2.inclusive = [[pidleveloneInclusiveArray objectAtIndex:m] intValue]; // Anil
            
			[node1 addChild:node2];
			
			[self getLevelOneTODO:[[pidleveloneArray objectAtIndex:m]intValue]];
            [self getLevelOneNote:[[pidleveloneArray objectAtIndex:m]intValue]];
            
			if([getTODOIDOneArray count]>0)
			{
				for(int ti=0;ti<[getTODOIDOneArray count];ti++)
				{
					nodeT2=[[MyTreeNode alloc] initWithValue:[getTODOTitleOneArray objectAtIndex:ti]];
					nodeT2.TODO =1;
					nodeT2.isBTN=YES;
				    nodeT2.TODOid=[[getTODOIDOneArray objectAtIndex:ti]intValue];
					nodeT2.titleType=[TODOTitletypeOneArray objectAtIndex:ti];
					nodeT2.titlebinary=[TODOtitleBinaryOneArray objectAtIndex:ti];
					nodeT2.PROGRES=[[progresOneArray objectAtIndex:ti]intValue];
					nodeT2.PriorityTODO=[priorityOneArray objectAtIndex:ti];
					
                    
                    if([[pidleveloneObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                    {
                        nodeT2.objectType = NoteVal;
                    }
                    else
                    {
                        nodeT2.objectType = ToDo;
                        
                    }
                    
                    
                    [node2 addChild:nodeT2];
					
				}
			}
			
			[self getLevelTwo: [[pidleveloneArray objectAtIndex:m]intValue]];
 
            //Steve -  fixes bug when a To-do or Note is deleted in another module, the Collapse "+" was still showing even when there were no folders & to-do's under it
            if (pidleveltwoArray.count == 0 && pidleveloneObjectTypeArray.count == 0) {
                node2.inclusive = YES;
                [self updateInclusiveValue:node2.inclusive :node2.ID]; //Method stores inclusive value
            }
			
            
			
			for(int j=0;j<[pidleveltwoArray count];j++)
			{ 
				node3 = [[MyTreeNode alloc] initWithValue:[pidleveltwoNameArray objectAtIndex:j]];
				node3.LevelVal=2;
				node3.isBTN=NO;
				node3.prntID=PIDval;
				node3.ID=[[pidleveltwoArray objectAtIndex:j]intValue];
				node3.Fimg=[pidleveltwoImgArray objectAtIndex:j];
                node3.projectPriorty= [pidleveltwoPriorityArray objectAtIndex:j];
                node3.inclusive = [[pidleveltwoInclusiveArray objectAtIndex:j] intValue]; // Anil
                
				[node2 addChild:node3];
				
				
				[self getLevelTwoTODO:[[pidleveltwoArray objectAtIndex:j]intValue]];
                [self getLevelTwoNote:[[pidleveltwoArray objectAtIndex:j]intValue]];
                
                
				//NSLog(@"TOTAL Value For  TODO level two %d",[pidleveltwoArray count]);
				
				if([getTODOTwoArray count]>0)
				{
					for(int ti=0;ti<[getTODOTwoArray count];ti++)
					{
						nodeT3=[[MyTreeNode alloc] initWithValue:[getTODOTwoNameArray objectAtIndex:ti]];
						nodeT3.TODO =2;
						nodeT3.isBTN=YES;
						nodeT3.TODOid=[[getTODOTwoArray objectAtIndex:ti]intValue];
						nodeT3.titleType=[TODOTitletypeTwoArray objectAtIndex:ti];
						nodeT3.titlebinary=[TODOtitleBinaryTwoArray objectAtIndex:ti];
						nodeT3.PROGRES=[[progresTwoArray objectAtIndex:ti]intValue];
						nodeT3.PriorityTODO=[priorityTwoArray objectAtIndex:ti];
						
                        if([[pidleveltwoObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                        {
                            nodeT3.objectType = NoteVal;
                        }
                        else
                        {
                            nodeT3.objectType = ToDo;
                            
                        }

                         [node3 addChild:nodeT3];
						
					}
					
				}
				
 				[self getLevelThree:[[pidleveltwoArray objectAtIndex:j]intValue]];
                
    
                //Steve -  fixes bug when a To-do or Note is deleted in another module, the Collapse "+" was still showing even when there were no folders & to-do's under it
                if (pidlevelthreeArray.count == 0 && pidleveltwoObjectTypeArray.count == 0) {
                    node3.inclusive = YES;
                    [self updateInclusiveValue:node3.inclusive :node3.ID]; //Method stores inclusive value
                }
  				
				for(int l=0;l<[pidlevelthreeArray count];l++)
				{		
					node4 = [[MyTreeNode alloc] initWithValue:[pidlevelthreeNameArray objectAtIndex:l]];
					node4.LevelVal=3;
					node4.isBTN=NO;
					node4.prntID=PIDval;
					node4.ID=[[pidlevelthreeArray objectAtIndex:l]intValue];
					node4.Fimg=[pidlevelthreeImgArray objectAtIndex:l];
                    node4.projectPriorty= [pidlevelthreePriorityArray objectAtIndex:l];
                    node4.inclusive = [[pidlevelthreeInclusiveArray objectAtIndex:l] intValue]; // Anil
                    
					[node3 addChild:node4];
                
					[self getLevelThreeTODO:[[pidlevelthreeArray objectAtIndex:l]intValue]];
                    [self getLevelThreeNote:[[pidlevelthreeArray objectAtIndex:l]intValue]];
                    
					if([getTODOThreeArray count]>0)
					{
						//NSLog(@"TOTAL Value For  TODO level four %d",[getTODOThreeArray count]);
						for(int ti=0;ti<[getTODOThreeArray count];ti++)
						{
							nodeT4=[[MyTreeNode alloc] initWithValue:[getTODOThreeNameArray objectAtIndex:ti]];
							nodeT4.TODO =3;
							nodeT4.isBTN=YES;
							nodeT4.TODOid=[[getTODOThreeArray objectAtIndex:ti]intValue];
							nodeT4.titleType=[TODOTitletypeThreeArray objectAtIndex:ti];
							nodeT4.titlebinary=[TODOtitleBinaryThreeArray objectAtIndex:ti];
							nodeT4.PROGRES=[[progresThreetArray objectAtIndex:ti]intValue];
							nodeT4.PriorityTODO=[priorityThreeArray objectAtIndex:ti];
							if([[pidlevelthreeObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                            {
                                nodeT4.objectType = NoteVal;
                            }
                            else
                            {
                                nodeT4.objectType = ToDo;
                                
                            }
                                [node4 addChild:nodeT4];
						}
					}
					
                    //Steve -  fixes bug when a To-do or Note is deleted in another module, the Collapse "+" was still showing even when there were no to-do's under it.
                    //Can't have folders on level 4
                    if (pidlevelthreeObjectTypeArray.count == 0) {
                        node4.inclusive = YES;
                        [self updateInclusiveValue:node4.inclusive :node4.ID]; //Method stores inclusive value
                    }
                    
				}
                
            }			
            
        }
		[mytable reloadData];
	}
	else 
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	[mytable reloadData];
}	



#pragma mark -
#pragma mark Get Data From Table

-(void)FindRecored
{
    //NSLog(@"FindRecored");
	
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
        
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,folderImage,projectPriority, Inclusive from ProjectMaster where projectId=%d AND level =%d ORDER BY Case when (projectPriority ISNULL or projectPriority Like '') Then 1000 Else case when SubStr(projectPriority, 1, 1) = 'A' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) else (case when SubStr(projectPriority,1,1) = 'B' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 10 else CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 20 end) end End",PIDval,0];
        //NSLog(@" query %@",Sql);
        if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
            [getLevelRootArray removeAllObjects];
            [getLevelRootNameArray removeAllObjects];
            [getLevelRootLevelArray removeAllObjects];
            [getLevelRootimageArray removeAllObjects];
            [getLevelRootPriortyArray removeAllObjects];
            [pidRootInclusiveArray removeAllObjects]; // Anil
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				AddProjectDataObject  *projectdataobj = [[AddProjectDataObject alloc]init];
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[getLevelRootArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[getLevelRootNameArray addObject:projectdataobj.projectname];
				
				[projectdataobj setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
				[getLevelRootLevelArray addObject:[NSNumber numberWithInt:projectdataobj.level]];
				
				[projectdataobj setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
				[getLevelRootimageArray addObject:projectdataobj.projectIMGname];
                [projectdataobj setProjectpriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[getLevelRootPriortyArray addObject:projectdataobj.projectpriority];
                
                [pidRootInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]]; // Anil
            
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		
	}
}

-(void)FindNote
{
     //NSLog(@"FindNote");
    
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		//NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary ,b.ImgID,b.ImgInfo ,c.HwrtID,c.HwertInfo FROM NotesMaster  left outer join (Select * From NotesImgInfo Group by NotesID)  b  on NotesMaster.NoteID=b.NotesID  left outer join (Select * From NotesHwrtInfo Group by NotesID) c on  NotesMaster.NoteID = c.NotesID;"];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary FROM NotesMaster where BelongstoProjectId =%d",PIDval];
        
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [getTODOIDeArray addObject:[NSNumber numberWithInt:notesDataObject.notesID]];
                [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [getLevelRootTextTypeArray addObject:notesDataObject.notesTitleType];
                [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                [getTODOTitleArray addObject:notesDataObject.notesTitle];
                NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                [notesDataObject setNotesTitleBinary:binaryDataL];
                [getLevelRootBinaryArray addObject:notesDataObject.notesTitleBinary];
                [progresRootArray addObject:[NSNumber numberWithInt:0]];
                [priorityRootArray addObject:@"  "];
                [getLevelRootObjectTypeArray addObject:@"note"];
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
        
	}
    
}

-(void)FindTodo
{
	//NSLog(@"FindTodo");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *selectToDO_Stmt = nil; 
	
	if(selectToDO_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql =[[NSString alloc] initWithFormat:@"SELECT TODOID,title,TitleTextType,TitleBinary,TitleBinaryLarge,Priority,Progress FROM TODOTable where BelongstoProjectId =%d ORDER BY Progress=100,  Case when (Priority ISNULL or Priority Like '') Then 1000 Else case when SubStr(Priority, 1, 1) = 'A' then CAST( SubStr(Priority, 2, length(Priority)) as integer) else (case when SubStr(Priority,1,1) = 'B' then CAST( SubStr(Priority, 2, length(Priority)) as integer) + 10 else CAST( SubStr(Priority, 2, length(Priority)) as integer) + 20 end) end End",PIDval];
		
        //NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectToDO_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[getTODOIDeArray removeAllObjects];
			[getTODOTitleArray removeAllObjects];
			[getLevelRootTextTypeArray removeAllObjects];
			[getLevelRootBinaryArray removeAllObjects];
            [getLevelRootBinarLargeyArray removeAllObjects];
			[priorityRootArray removeAllObjects];
			[progresRootArray removeAllObjects];
            [getLevelRootObjectTypeArray removeAllObjects];
			
			while(sqlite3_step(selectToDO_Stmt) == SQLITE_ROW) 
			{
				TODODataObject  *todoprojectdataobj = [[TODODataObject alloc]init];
				
				[todoprojectdataobj setTodoID:sqlite3_column_int(selectToDO_Stmt, 0)];
				
				[getTODOIDeArray addObject:[NSNumber numberWithInt:todoprojectdataobj.todoID]];
				
				[todoprojectdataobj setTodoTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 1)]];
				[getTODOTitleArray addObject:todoprojectdataobj.todoTitle];
				
				[todoprojectdataobj setTodoTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 2)]];
				[getLevelRootTextTypeArray addObject:todoprojectdataobj.todoTitleType];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectToDO_Stmt, 3);
				NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 3) length:blobLength];
				[todoprojectdataobj setTodoTitleBinary:binaryData];
				[getLevelRootBinaryArray addObject:todoprojectdataobj.todoTitleBinary];
                
				NSUInteger blobLength1 = sqlite3_column_bytes(selectToDO_Stmt, 4);
				NSData *binaryData1 = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 4) length:blobLength1];
				[todoprojectdataobj setTodoTitleBinaryLarge:binaryData1];
				[getLevelRootBinarLargeyArray addObject:todoprojectdataobj.todoTitleBinaryLarge];
				
				[todoprojectdataobj setPriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 5)]];
				[priorityRootArray addObject:todoprojectdataobj.priority];
				[todoprojectdataobj setToDoProgress:sqlite3_column_int(selectToDO_Stmt, 6)];
				[progresRootArray addObject:[NSNumber numberWithInt:todoprojectdataobj.toDoProgress]];
                [getLevelRootObjectTypeArray addObject:@"todo"];
                
			}							        
			sqlite3_finalize(selectToDO_Stmt);		
			selectToDO_Stmt = nil;        
		}
		
	}
	
}

-(void)getLevelOne:(NSInteger)lONE
{
	//NSLog(@"getLevelOne");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,folderImage,projectPriority, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY Case when (projectPriority ISNULL or projectPriority Like '') Then 1000 Else case when SubStr(projectPriority, 1, 1) = 'A' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) else (case when SubStr(projectPriority,1,1) = 'B' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 10 else CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 20 end) end End ",lONE,1];                         
        
        
        
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[pidleveloneArray  removeAllObjects];
			[pidleveloneNameArray removeAllObjects];
            [pidleveloneImgArray removeAllObjects];
            [pidlevelonePriorityArray removeAllObjects];
            [pidleveloneInclusiveArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjone = [[AddProjectDataObject alloc]init];
				[projectdataobjone setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidleveloneArray addObject:[NSNumber numberWithInt:projectdataobjone.projectid]];
				
				[projectdataobjone setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidleveloneNameArray addObject:projectdataobjone.projectname];
				
				[projectdataobjone setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
				[pidleveloneLevelArray addObject:[NSNumber numberWithInt:projectdataobjone.level]];
                
                [projectdataobjone setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
                [pidleveloneImgArray addObject:projectdataobjone.projectIMGname];
                
                [projectdataobjone setProjectpriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[pidlevelonePriorityArray addObject:projectdataobjone.projectpriority];
				
				[pidleveloneInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]];
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
	
	
}


-(void)getLevelOneTODO:(NSInteger)pidVAL
{
	//NSLog(@"getLevelOneTODO");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *selectToDO_Stmt = nil; 
	
	if(selectToDO_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT TODOID,title,TitleTextType,TitleBinary,TitleBinaryLarge,Priority,Progress FROM TODOTable where BelongstoProjectId =%d ORDER BY Progress=100,  Case when (Priority ISNULL or Priority Like '') Then 1000 Else case when SubStr(Priority, 1, 1) = 'A' then CAST( SubStr(Priority, 2, length(Priority)) as integer) else (case when SubStr(Priority,1,1) = 'B' then CAST( SubStr(Priority, 2, length(Priority)) as integer) + 10 else CAST( SubStr(Priority, 2, length(Priority)) as integer) + 20 end) end End",pidVAL];
		//fNSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectToDO_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[getTODOTitleOneArray removeAllObjects];
			[getTODOIDOneArray removeAllObjects];
			[TODOTitletypeOneArray removeAllObjects];
			[TODOtitleBinaryOneArray removeAllObjects];
            [TODOtitleBinaryLargeOneArray removeAllObjects];
			[progresOneArray removeAllObjects];
			[priorityOneArray removeAllObjects];
            [pidleveloneObjectTypeArray removeAllObjects];
            
			while(sqlite3_step(selectToDO_Stmt) == SQLITE_ROW) 
			{
				TODODataObject  *todoprojectdataobjone = [[TODODataObject alloc]init];
				
				[todoprojectdataobjone setTodoID:sqlite3_column_int(selectToDO_Stmt, 0)];
				
				[getTODOIDOneArray addObject:[NSNumber numberWithInt:todoprojectdataobjone.todoID]];
				
                
				[todoprojectdataobjone setTodoTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 1)]];
                
				[getTODOTitleOneArray addObject:todoprojectdataobjone.todoTitle];
				
				
				[todoprojectdataobjone setTodoTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 2)]];
				[TODOTitletypeOneArray addObject:todoprojectdataobjone.todoTitleType];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectToDO_Stmt, 3);
				NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 3) length:blobLength];
				[todoprojectdataobjone setTodoTitleBinary:binaryData];
				[TODOtitleBinaryOneArray addObject:todoprojectdataobjone.todoTitleBinary];
                
                NSUInteger blobLength1 = sqlite3_column_bytes(selectToDO_Stmt, 4);
				NSData *binaryData1 = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 4) length:blobLength1];
				[todoprojectdataobjone setTodoTitleBinaryLarge:binaryData1];
				[TODOtitleBinaryLargeOneArray addObject:todoprojectdataobjone.todoTitleBinaryLarge];
                [todoprojectdataobjone setToDoProgress:sqlite3_column_int(selectToDO_Stmt, 6)];
				[progresOneArray addObject:[NSNumber numberWithInt:todoprojectdataobjone.toDoProgress]];
				
				[todoprojectdataobjone setPriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt,5)]];
                
				[priorityOneArray addObject:todoprojectdataobjone.priority];
                [pidleveloneObjectTypeArray addObject:@"todo"];
				
			}							        
			sqlite3_finalize(selectToDO_Stmt);		
			selectToDO_Stmt = nil;        
		}
		
	}
}


-(void)getLevelOneNote:(NSInteger)pidVAL
{
    //NSLog(@"getLevelOneNote");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		//NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary ,b.ImgID,b.ImgInfo ,c.HwrtID,c.HwertInfo FROM NotesMaster  left outer join (Select * From NotesImgInfo Group by NotesID)  b  on NotesMaster.NoteID=b.NotesID  left outer join (Select * From NotesHwrtInfo Group by NotesID) c on  NotesMaster.NoteID = c.NotesID;"];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary FROM NotesMaster where BelongstoProjectId =%d",pidVAL];
        
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [getTODOIDOneArray addObject:[NSNumber numberWithInt:notesDataObject.notesID]];
                [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [TODOTitletypeOneArray addObject:notesDataObject.notesTitleType];
                [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                [getTODOTitleOneArray addObject:notesDataObject.notesTitle];
                NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                [notesDataObject setNotesTitleBinary:binaryDataL];
                [TODOtitleBinaryOneArray addObject:notesDataObject.notesTitleBinary];
                [progresOneArray addObject:[NSNumber numberWithInt:0]];
                [priorityOneArray addObject:@"  "];
                [pidleveloneObjectTypeArray addObject:@"note"];
                
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
        
	}
}


-(void)getLevelTwo:(NSInteger)lTWO
{
    //NSLog(@"getLevelTwo");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,folderImage,projectPriority, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY Case when (projectPriority ISNULL or projectPriority Like '') Then 1000 Else case when SubStr(projectPriority, 1, 1) = 'A' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) else (case when SubStr(projectPriority,1,1) = 'B' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 10 else CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 20 end) end End ",lTWO,2];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			[pidleveltwoArray  removeAllObjects];
			[pidleveltwoNameArray removeAllObjects];
            [pidleveltwoImgArray removeAllObjects];
            [pidleveltwoPriorityArray removeAllObjects];
            [pidleveltwoInclusiveArray removeAllObjects];
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjtwo = [[AddProjectDataObject alloc]init];
				[projectdataobjtwo setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidleveltwoArray addObject:[NSNumber numberWithInt:projectdataobjtwo.projectid]];
				
				[projectdataobjtwo setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidleveltwoNameArray addObject:projectdataobjtwo.projectname];
				
				[projectdataobjtwo setLevel:sqlite3_column_int(selectNotes_Stmt, 2)];
				[pidleveltwoLevelArray addObject:[NSNumber numberWithInt:projectdataobjtwo.level]];
                
                [projectdataobjtwo setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
                [pidleveltwoImgArray addObject:projectdataobjtwo.projectIMGname];
				
				[projectdataobjtwo setProjectpriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[pidleveltwoPriorityArray addObject:projectdataobjtwo.projectpriority];
				
                [pidleveltwoInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]];
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
    }
}


-(void)getLevelTwoTODO:(NSInteger)pidVal
{
    //NSLog(@"getLevelTwoTODO");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *selectToDO_Stmt = nil; 
	
	if(selectToDO_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT TODOID,title,TitleTextType,TitleBinary,TitleBinaryLarge,Priority,Progress FROM TODOTable where BelongstoProjectId =%d ORDER BY Progress=100,  Case when (Priority ISNULL or Priority Like '') Then 1000 Else case when SubStr(Priority, 1, 1) = 'A' then CAST( SubStr(Priority, 2, length(Priority)) as integer) else (case when SubStr(Priority,1,1) = 'B' then CAST( SubStr(Priority, 2, length(Priority)) as integer) + 10 else CAST( SubStr(Priority, 2, length(Priority)) as integer) + 20 end) end End",pidVal];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectToDO_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[getTODOTwoArray removeAllObjects];
			[getTODOTwoNameArray removeAllObjects];
			[TODOTitletypeTwoArray removeAllObjects];
			[TODOtitleBinaryTwoArray removeAllObjects];
            [TODOtitleBinaryLargeTwoArray removeAllObjects];
			[progresTwoArray removeAllObjects];
			[priorityTwoArray removeAllObjects];
            [pidleveltwoObjectTypeArray removeAllObjects];
			while(sqlite3_step(selectToDO_Stmt) == SQLITE_ROW) 
			{
				TODODataObject  *todoprojectdataobjtwo = [[TODODataObject alloc]init];
				
				[todoprojectdataobjtwo setTodoID:sqlite3_column_int(selectToDO_Stmt, 0)];
				[getTODOTwoArray addObject:[NSNumber numberWithInt:todoprojectdataobjtwo.todoID]];
				[todoprojectdataobjtwo setTodoTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 1)]];
				[getTODOTwoNameArray addObject:todoprojectdataobjtwo.todoTitle];
				[todoprojectdataobjtwo setTodoTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 2)]];
				[TODOTitletypeTwoArray addObject:todoprojectdataobjtwo.todoTitleType];
				NSUInteger blobLength = sqlite3_column_bytes(selectToDO_Stmt, 3);
				NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 3) length:blobLength];
				[todoprojectdataobjtwo setTodoTitleBinary:binaryData];
				[TODOtitleBinaryTwoArray addObject:todoprojectdataobjtwo.todoTitleBinary];
                NSUInteger blobLength1 = sqlite3_column_bytes(selectToDO_Stmt, 4);
				NSData *binaryData1 = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 4) length:blobLength1];
				[todoprojectdataobjtwo setTodoTitleBinaryLarge:binaryData1];
				[TODOtitleBinaryLargeTwoArray addObject:todoprojectdataobjtwo.todoTitleBinaryLarge];
				[todoprojectdataobjtwo setToDoProgress:sqlite3_column_int(selectToDO_Stmt, 6)];
				[progresTwoArray addObject:[NSNumber numberWithInt:todoprojectdataobjtwo.toDoProgress]];
				[todoprojectdataobjtwo setPriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt,5)]];
				[priorityTwoArray addObject:todoprojectdataobjtwo.priority];
                [pidleveltwoObjectTypeArray addObject:@"todo"];
				
			}							        
			sqlite3_finalize(selectToDO_Stmt);		
			selectToDO_Stmt = nil;        
		}
		
	}
	
	
}

-(void)getLevelTwoNote:(NSInteger)pidVAL
{
     //NSLog(@"getLevelTwoNote");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		//NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary ,b.ImgID,b.ImgInfo ,c.HwrtID,c.HwertInfo FROM NotesMaster  left outer join (Select * From NotesImgInfo Group by NotesID)  b  on NotesMaster.NoteID=b.NotesID  left outer join (Select * From NotesHwrtInfo Group by NotesID) c on  NotesMaster.NoteID = c.NotesID;"];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary FROM NotesMaster where BelongstoProjectId =%d",pidVAL];
        
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [getTODOTwoArray addObject:[NSNumber numberWithInt:notesDataObject.notesID]];
                [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [TODOTitletypeTwoArray addObject:notesDataObject.notesTitleType];
                [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                [getTODOTwoNameArray addObject:notesDataObject.notesTitle];
                NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                [notesDataObject setNotesTitleBinary:binaryDataL];
                [TODOtitleBinaryTwoArray addObject:notesDataObject.notesTitleBinary];
                [progresTwoArray addObject:[NSNumber numberWithInt:0]];
                [priorityTwoArray addObject:@"  "];
                [pidleveltwoObjectTypeArray addObject:@"note"];
                
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
        
	}
}





-(void)getLevelThree:(NSInteger)lThree
{
     //NSLog(@"getLevelThree");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,level,folderImage,projectPriority, Inclusive from ProjectMaster where belongstoID=%d AND level =%d ORDER BY Case when (projectPriority ISNULL or projectPriority Like '') Then 1000 Else case when SubStr(projectPriority, 1, 1) = 'A' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) else (case when SubStr(projectPriority,1,1) = 'B' then CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 10 else CAST( SubStr(projectPriority, 2, length(projectPriority)) as integer) + 20 end) end End",lThree,3];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
			
		{
			
			[pidlevelthreeArray  removeAllObjects];
			[pidlevelthreeNameArray removeAllObjects];
			[pidlevelthreeImgArray removeAllObjects];
            [pidlevelthreePriorityArray removeAllObjects];
            [pidlevelthreeInclusiveArray removeAllObjects];
            
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
		        AddProjectDataObject  *projectdataobjthree = [[AddProjectDataObject alloc]init];
				[projectdataobjthree setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[pidlevelthreeArray addObject:[NSNumber numberWithInt:projectdataobjthree.projectid]];
				
				
				[projectdataobjthree setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[pidlevelthreeNameArray addObject:projectdataobjthree.projectname];
                //NSLog(@" projectdataobjthree.projectname = %@",projectdataobjthree.projectname);
				
				[projectdataobjthree setProjectid:sqlite3_column_int(selectNotes_Stmt, 2)];
				[pidlevelthreeLevelArray addObject:[NSNumber numberWithInt:projectdataobjthree.level]];
				
                [projectdataobjthree setProjectIMGname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
                [pidlevelthreeImgArray addObject:projectdataobjthree.projectIMGname];
                
                [projectdataobjthree setProjectpriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[pidlevelthreePriorityArray addObject:projectdataobjthree.projectpriority];
                
                //Steve commented - causes level 3 subfolders to crash by assigning it an Int.  Aman's addition replaces this
                //pidlevelthreeNameArray is assigned a value above
                //[pidlevelthreeNameArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]];
                
                [pidlevelthreeInclusiveArray addObject:[NSNumber numberWithInt:sqlite3_column_int(selectNotes_Stmt,5)]];// Aman's Added
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
}

-(void)getLevelThreeTODO:(NSInteger)pidVal
{
    //NSLog(@"getLevelThreeTODO");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *selectToDO_Stmt = nil; 
	
	if(selectToDO_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT TODOID,title,TitleTextType,TitleBinary,TitleBinaryLarge,Priority,Progress FROM TODOTable where BelongstoProjectId =%d ORDER BY Progress=100,  Case when (Priority ISNULL or Priority Like '') Then 1000 Else case when SubStr(Priority, 1, 1) = 'A' then CAST( SubStr(Priority, 2, length(Priority)) as integer) else (case when SubStr(Priority,1,1) = 'B' then CAST( SubStr(Priority, 2, length(Priority)) as integer) + 10 else CAST( SubStr(Priority, 2, length(Priority)) as integer) + 20 end) end End ",pidVal];
		//NSLog(@" query %@",Sql);
		
		//SELECT * FROM table_name WHERE columnname LIKE value%
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectToDO_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[getTODOThreeArray removeAllObjects];
			[getTODOThreeNameArray removeAllObjects];
			[TODOTitletypeThreeArray removeAllObjects];
			[TODOtitleBinaryThreeArray removeAllObjects];
            [TODOtitleBinaryLargeThreeArray removeAllObjects];
			[progresThreetArray removeAllObjects];
			[priorityThreeArray removeAllObjects];
            [pidlevelthreeObjectTypeArray removeAllObjects];
            
			while(sqlite3_step(selectToDO_Stmt) == SQLITE_ROW) 
			{
				TODODataObject  *todoprojectdataobjthree = [[TODODataObject alloc]init];
				
				[todoprojectdataobjthree setTodoID:sqlite3_column_int(selectToDO_Stmt, 0)];
				
				[getTODOThreeArray addObject:[NSNumber numberWithInt:todoprojectdataobjthree.todoID]];
				
				[todoprojectdataobjthree setTodoTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 1)]];
				[getTODOThreeNameArray addObject:todoprojectdataobjthree.todoTitle];
				
				
				[todoprojectdataobjthree setTodoTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt, 2)]];
				[TODOTitletypeThreeArray addObject:todoprojectdataobjthree.todoTitleType];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectToDO_Stmt, 3);
				NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 3) length:blobLength];
				[todoprojectdataobjthree setTodoTitleBinary:binaryData];
				[TODOtitleBinaryThreeArray addObject:todoprojectdataobjthree.todoTitleBinary];
                
                NSUInteger blobLength1 = sqlite3_column_bytes(selectToDO_Stmt, 4);
				NSData *binaryData1 = [NSData dataWithBytes:sqlite3_column_blob(selectToDO_Stmt, 4) length:blobLength1];
				[todoprojectdataobjthree setTodoTitleBinaryLarge:binaryData1];
				[TODOtitleBinaryLargeThreeArray addObject:todoprojectdataobjthree.todoTitleBinaryLarge];
                
				[todoprojectdataobjthree setToDoProgress:sqlite3_column_int(selectToDO_Stmt, 6)];
				[progresThreetArray addObject:[NSNumber numberWithInt:todoprojectdataobjthree.toDoProgress]];
				
				[todoprojectdataobjthree setPriority:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectToDO_Stmt,5)]];
				[priorityThreeArray addObject:todoprojectdataobjthree.priority];
				[pidlevelthreeObjectTypeArray addObject:@"todo"];
			}							        
			sqlite3_finalize(selectToDO_Stmt);		
			selectToDO_Stmt = nil;        
		}
		
	}
}


-(void)getLevelThreeNote:(NSInteger)pidVAL
{
   // NSLog(@"getLevelThreeNote");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		//NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary ,b.ImgID,b.ImgInfo ,c.HwrtID,c.HwertInfo FROM NotesMaster  left outer join (Select * From NotesImgInfo Group by NotesID)  b  on NotesMaster.NoteID=b.NotesID  left outer join (Select * From NotesHwrtInfo Group by NotesID) c on  NotesMaster.NoteID = c.NotesID;"];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary FROM NotesMaster where BelongstoProjectId =%d",pidVAL];
        
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [getTODOThreeArray addObject:[NSNumber numberWithInt:notesDataObject.notesID]];
                [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [TODOTitletypeThreeArray addObject:notesDataObject.notesTitleType];
                [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                [getTODOThreeNameArray addObject:notesDataObject.notesTitle];
                NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                [notesDataObject setNotesTitleBinary:binaryDataL];
                [TODOtitleBinaryThreeArray addObject:notesDataObject.notesTitleBinary];
                [progresThreetArray addObject:[NSNumber numberWithInt:0]];
                [priorityThreeArray addObject:@"  "];
                [pidlevelthreeObjectTypeArray addObject:@"note"];
                
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
        
	}
}


-(IBAction)addSub_folder:(id)sender
{
	//NSLog(@"%d",[pidleveltwoArray count]);
	AddProjectViewCantroller *addprojectview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantroller" bundle:[NSBundle mainBundle]];
    addprojectview.getfId =PIDval;
	[self.navigationController pushViewController:addprojectview animated:YES];
	
}

- (IBAction)helpView_Tapped:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    CATransition  *transition = [CATransition animation ];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.7;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}

- (void)updateToDoProgress:(NSInteger)TODOID ProgresVAL:(NSInteger)ProgresVAL;
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
        NSString *sql = [[NSString alloc]initWithFormat:@"Update ToDoTable set Progress=%d  where TODOID=%d ",ProgresVAL,TODOID];
		//NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	if(SQLITE_DONE != sqlite3_step(insertProjectData))
	{
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	} 
	else 
	{
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
        [self refreshTableView];
        
		//[self getValuesFromDatabase];
	}	
}
-(void)refreshTableView
{
    //NSLog(@"refreshTableView");
    
    [self FindRecored];
	if([getLevelRootArray count]>0)
	{
		treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
		node1 = [[MyTreeNode alloc] initWithValue:[getLevelRootNameArray objectAtIndex:0]];
		node1.ID=PIDval;
		node1.prntID=PIDval;
		node1.LevelVal=0;
		node1.isBTN=NO;
		node1.Fimg  =[getLevelRootimageArray objectAtIndex:0];
        node1.projectPriorty= [getLevelRootPriortyArray objectAtIndex:0];
        node1.inclusive = [[pidRootInclusiveArray objectAtIndex:0] intValue]; // Anil
		[treeNode addChild:node1];
		[self FindTodo];
        [self FindNote];
		if([getTODOIDeArray count]>0)
		{
			for(int ti=0;ti<[getTODOIDeArray count];ti++)
			{
				nodeT1=[[MyTreeNode alloc] initWithValue:[getTODOTitleArray objectAtIndex:ti]];
				nodeT1.TODO =0;
				nodeT1.isBTN=YES;
				nodeT1.TODOid=[[getTODOIDeArray objectAtIndex:ti]intValue];
				nodeT1.titleType=[getLevelRootTextTypeArray objectAtIndex:ti];
				nodeT1.titlebinary=[getLevelRootBinaryArray objectAtIndex:ti];
				nodeT1.PriorityTODO=[priorityRootArray objectAtIndex:ti];
				nodeT1.PROGRES=[[progresRootArray objectAtIndex:ti]intValue];
                if([[getLevelRootObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                {
                    nodeT1.objectType = NoteVal;
                }
                else
                {
                    nodeT1.objectType = ToDo;
                    
                }
				[node1 addChild:nodeT1];
			}
			
		}
		[self getLevelOne:[[getLevelRootArray objectAtIndex:0]intValue]];
		for(int m=0;m<[pidleveloneArray count];m++)
		{
			
			node2 = [[MyTreeNode alloc] initWithValue:[pidleveloneNameArray objectAtIndex:m]];
			node2.ID = [[pidleveloneArray objectAtIndex:m]intValue];
			node2.prntID=PIDval;
			node2.LevelVal=1;
			node2.isBTN=NO;
			node2.Fimg=[pidleveloneImgArray objectAtIndex:m];
            node2.projectPriorty= [pidlevelonePriorityArray objectAtIndex:m];
            node2.inclusive = [[pidleveloneInclusiveArray objectAtIndex:m] intValue]; // Anil
            
			[node1 addChild:node2];
			
			[self getLevelOneTODO:[[pidleveloneArray objectAtIndex:m]intValue]];
            [self getLevelOneNote:[[pidleveloneArray objectAtIndex:m]intValue]];
            
			if([getTODOIDOneArray count]>0)
			{
				for(int ti=0;ti<[getTODOIDOneArray count];ti++)
				{
					nodeT2=[[MyTreeNode alloc] initWithValue:[getTODOTitleOneArray objectAtIndex:ti]];
					nodeT2.TODO =1;
					nodeT2.isBTN=YES;
				    nodeT2.TODOid=[[getTODOIDOneArray objectAtIndex:ti]intValue];
					nodeT2.titleType=[TODOTitletypeOneArray objectAtIndex:ti];
					nodeT2.titlebinary=[TODOtitleBinaryOneArray objectAtIndex:ti];
					nodeT2.PROGRES=[[progresOneArray objectAtIndex:ti]intValue];
					nodeT2.PriorityTODO=[priorityOneArray objectAtIndex:ti];
                    if([[pidleveloneObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                    {
                        nodeT2.objectType = NoteVal;
                    }
                    else
                    {
                        nodeT2.objectType = ToDo;
                        
                    }
					[node2 addChild:nodeT2];
					
				}
			}
			
			[self getLevelTwo: [[pidleveloneArray objectAtIndex:m]intValue]];
            
			
			for(int j=0;j<[pidleveltwoArray count];j++)
			{ 
				node3 = [[MyTreeNode alloc] initWithValue:[pidleveltwoNameArray objectAtIndex:j]];
				node3.LevelVal=2;
				node3.isBTN=NO;
				node3.prntID=PIDval;
				node3.ID=[[pidleveltwoArray objectAtIndex:j]intValue];
				node3.Fimg=[pidleveltwoImgArray objectAtIndex:j];
                node3.projectPriorty= [pidleveltwoPriorityArray objectAtIndex:j];
                node3.inclusive = [[pidleveltwoInclusiveArray objectAtIndex:j] intValue]; // Anil
                
				[node2 addChild:node3];
				
				
				[self getLevelTwoTODO:[[pidleveltwoArray objectAtIndex:j]intValue]];
                [self getLevelTwoNote:[[pidleveltwoArray objectAtIndex:j]intValue]];
                
				//NSLog(@"TOTAL Value For  TODO level two %d",[pidleveltwoArray count]);
				
				if([getTODOTwoArray count]>0)
				{
					for(int ti=0;ti<[getTODOTwoArray count];ti++)
					{
						nodeT3=[[MyTreeNode alloc] initWithValue:[getTODOTwoNameArray objectAtIndex:ti]];
						nodeT3.TODO =2;
						nodeT3.isBTN=YES;
						nodeT3.TODOid=[[getTODOTwoArray objectAtIndex:ti]intValue];
						nodeT3.titleType=[TODOTitletypeTwoArray objectAtIndex:ti];
						nodeT3.titlebinary=[TODOtitleBinaryTwoArray objectAtIndex:ti];
						nodeT3.PROGRES=[[progresTwoArray objectAtIndex:ti]intValue];
						nodeT3.PriorityTODO=[priorityTwoArray objectAtIndex:ti];
                        if([[pidleveltwoObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                        {
                            nodeT3.objectType = NoteVal;
                        }
                        else
                        {
                            nodeT3.objectType = ToDo;
                            
                        }
                        
						[node3 addChild:nodeT3];
						
					}
					
				}
				
				
				[self getLevelThree:[[pidleveltwoArray objectAtIndex:j]intValue]];
				
				//NSLog(@"%d",[pidlevelthreeArray count]);
				for(int l=0;l<[pidlevelthreeArray count];l++)
				{		
					node4 = [[MyTreeNode alloc] initWithValue:[pidlevelthreeNameArray objectAtIndex:l]];
					node4.LevelVal=3;
					node4.isBTN=NO;
					node4.prntID=PIDval;
					node4.ID=[[pidlevelthreeArray objectAtIndex:l]intValue];
					node4.Fimg=[pidlevelthreeImgArray objectAtIndex:l];
                    node4.projectPriorty= [pidlevelthreePriorityArray objectAtIndex:l];
                    node4.inclusive = [[pidlevelthreeInclusiveArray objectAtIndex:l] intValue]; // Anil
                    
					[node3 addChild:node4];	
					[self getLevelThreeTODO:[[pidlevelthreeArray objectAtIndex:l]intValue]];
                    [self getLevelThreeNote:[[pidlevelthreeArray objectAtIndex:l]intValue]];
                    
					if([getTODOThreeArray count]>0)
					{
						//NSLog(@"TOTAL Value For  TODO level four %d",[getTODOThreeArray count]);
						for(int ti=0;ti<[getTODOThreeArray count];ti++)
						{
							nodeT4=[[MyTreeNode alloc] initWithValue:[getTODOThreeNameArray objectAtIndex:ti]];
							nodeT4.TODO =3;
							nodeT4.isBTN=YES;
							nodeT4.TODOid=[[getTODOThreeArray objectAtIndex:ti]intValue];
							nodeT4.titleType=[TODOTitletypeThreeArray objectAtIndex:ti];
							nodeT4.titlebinary=[TODOtitleBinaryThreeArray objectAtIndex:ti];
							nodeT4.PROGRES=[[progresThreetArray objectAtIndex:ti]intValue];
							nodeT4.PriorityTODO=[priorityThreeArray objectAtIndex:ti];
                            if([[pidlevelthreeObjectTypeArray objectAtIndex:ti] isEqualToString:@"note"])
                            {
                                nodeT4.objectType = NoteVal;
                            }
                            else
                            {
                                nodeT4.objectType = ToDo;
                                
                            }
                            
                            
							[node4 addChild:nodeT4];
						}
					}
					
				}
                
            }			
            
            
        }
		[mytable reloadData];
	}
	else 
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	[mytable reloadData];
}

// Done By naresh Still working 


#pragma mark Progress button clicked

-(void)progressBtn_Clicked:(id)sender
{
    NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
    NSArray *components = [getSTR componentsSeparatedByString:@"~"];
    //	
    int  valueId    =  [[components objectAtIndex:0]intValue];
    int progresVal =  [[components objectAtIndex:1]intValue];
    //	
    //NSLog(@"%d %d",valueId,progresVal);
    //	
    //	
    if(progresVal==100)
    {
        [self updateToDoProgress:valueId ProgresVAL:0];
        
        [mytable reloadData];
        //        
    }
    else 
    {
        [self updateToDoProgress:valueId ProgresVAL:100];
        [mytable reloadData];
    }
}


#pragma mark -
#pragma mark Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	float height;
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	if(node.isBTN==NO)
	{
		height=80.40;
	}
	else {
		height=40.40;
	}
	return height;
}


// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [treeNode descendantCount];
    // return [treeNode.children count];
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//NSLog(@"cellForRowAtIndexPath");
    
    static NSString *CellIdentifier = @"Cell";
	
   	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
    
    
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier  level:[node levelDepth] - 1  expanded:node.inclusive];
    
    
    [[cell arrowImage]  addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
    [[cell arrowImage] setTag:indexPath.row + 1];
    
    //sets up to do & note on cell
	if(node.isBTN==YES)
	{
        //NSLog(@"node.isBTN ==YES");
        
		UIImage *imgl4=[UIImage imageNamed:@"DetailsDisclouserBtn.png"];
		dbtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
		[dbtn3 setImage:imgl4 forState:UIControlStateNormal];
        if (IS_IPAD)
        {
          dbtn3.frame = CGRectMake(380.0,7.0,25.0,25.0);
        }else
		dbtn3.frame = CGRectMake(290.0,7.0,25.0,25.0);
		[dbtn3 setTitle:node.value forState:UIControlStateNormal];
		dbtn3.tag = node.TODOid;
        
        if(node.objectType == ToDo)
            [dbtn3 addTarget:self action:@selector(gotoToday:) forControlEvents:UIControlEventTouchUpInside];
        else
            [dbtn3 addTarget:self action:@selector(gotoNoteInfo:) forControlEvents:UIControlEventTouchUpInside];
		
        //dbtn3.tag = indexPath.row;
        [cell.contentView addSubview:dbtn3];
	}
	
    
    if(node.LevelVal==0 || node.TODO==0 )
    {
        UIImage *imgl1=[UIImage imageNamed:node.Fimg];
        buttonLevelone = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonLevelone setBackgroundImage:imgl1 forState:UIControlStateNormal];
        if(node.isBTN==NO)
        {
            [buttonLevelone addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
            buttonLevelone.tag=indexPath.row + 1;
            
        }
        else
        {
            buttonLevelone.tag=node.TODOid;
            if(node.objectType == ToDo)
                [buttonLevelone addTarget:self action:@selector(gotoToday:) forControlEvents:UIControlEventTouchUpInside];
            else
                [buttonLevelone addTarget:self action:@selector(gotoNoteInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        if (IS_IPAD)
        {
            buttonLevelone.frame = CGRectMake(26.0,7.0,380.0,40.0);
        }
        else
		buttonLevelone.frame = CGRectMake(26.0,7.0,288.0,40.0);
        
        if(node.isBTN==YES)
        {
			
            if(node.PROGRES==100)
            {
                
                cell.arrowImage.hidden=YES;
                proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
                proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                [proIndBorder setTag:9909];
                [proIndBorder setFrame:CGRectMake(5,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
                [cell.contentView addSubview:proIndBorder];
                proIndBorder.alpha=0.50;
                
                ///Aman's added
                UIButton *invisibleIndBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn setTag:indexPath.row];
                [invisibleIndBtn addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                [invisibleIndBtn addSubview:viewmB];
                [cell.contentView addSubview:invisibleIndBtn];
                
            }
            else 
            {
                
                cell.arrowImage.hidden=YES;
                
                if(node.objectType == ToDo)
                {
                    
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBG" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                    cell.arrowImage.hidden=YES;
                    
                }
                else if (node.objectType == NoteVal)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smallnotes" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                    cell.arrowImage.hidden=YES;
                    
                }
                
                pbutton=[UIButton buttonWithType:UIButtonTypeCustom];
                pbutton.backgroundColor=[UIColor redColor];
                [cell.contentView addSubview:pbutton];
                [proIndBorder setFrame:CGRectMake(5,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
                
                float buttonHeight = proIndBorderImage.size.height - 4;
                int percentComplete = node.PROGRES;
                float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
                if(percentComplete != 0)
                    
                    [pbutton setFrame:CGRectMake(4, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght-1, proIndBorderImage.size.width - 4, finalHieght+1)];
                else
                    [pbutton setFrame:CGRectMake(4, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, proIndBorderImage.size.width - 4, finalHieght)];
                
                UIButton *invisibleIndBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn setTag:indexPath.row];
                [invisibleIndBtn addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                
                
                [invisibleIndBtn addSubview:viewmB];
                if(node.objectType == ToDo)
                {
                    [cell.contentView addSubview:invisibleIndBtn];
                }
                
                
            }
			
			
        }
        if([node.titleType isEqualToString:@"H"])
        {
            //Add Handwriting
            cell.arrowImage.hidden=YES;
            UIImage *textImage = [[UIImage alloc] initWithData:node.titlebinary]; 
            UIImageView *starImgView = [[UIImageView alloc] initWithFrame:
                                        CGRectMake(40,1,210,20)]; 
            starImgView.image=textImage ;
            if(node.PROGRES==100)
            { starImgView.alpha=0.50;}
            [buttonLevelone addSubview:starImgView];
            
            
            //Add Priority Label to Handwriting
            myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)]; //Steve CGRectMake(2,1,40,20)];
            myLabelOne.text=node.PriorityTODO;
            myLabelOne.backgroundColor = [UIColor clearColor];
            if(node.PROGRES==100)
            {myLabelOne.textColor = [UIColor grayColor];}
            else {myLabelOne.textColor = [UIColor blackColor];}
            myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
            [buttonLevelone addSubview:myLabelOne];
            
			
        }
        else
        {
            if(node.isBTN==NO)
            {
                UILabel *myLabel;
                
                if([node.projectPriorty isEqualToString:@"None"])
                {
                    myLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,10,buttonLevelone.frame.size.width-40,30)];//(4,7,buttonLevelone.frame.size.width-40,30)
                    myLabel.text =node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                   // myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
                else {
                    UILabel *myLabell = [[UILabel alloc] initWithFrame:CGRectMake(4,10,buttonLevelone.frame.size.width-40,30)];//(4,7,buttonLevelone.frame.size.width-40,30)
                    myLabell.text = node.projectPriorty;
                    myLabell.backgroundColor = [UIColor clearColor];
                    myLabell.textColor = [UIColor whiteColor];
                    myLabell.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabell setFont:[UIFont fontWithName:@"Helvetica" size:15]];
                    [buttonLevelone addSubview:myLabell];
                    
                    if([node.projectPriorty length]>=3)
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,7,buttonLevelone.frame.size.width-40,30)];
                    }
                    else 
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(28,10,buttonLevelone.frame.size.width-40,30)];//(28,7,buttonLevelone.frame.size.width-40,30)
                    }
                    
                    myLabel.text = node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                    myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
				[buttonLevelone addSubview:myLabel];
                
                
                
                [self getBadgeCounts:node.ID];
                
                /////////////////// Calculating total badge counts //////
                int totalObjects;
                
                totalObjects = [self getBadgeCountsForNotes:node.ID];
                
                totalObjects = totalObjects + badgeCount[0];
                
                badgeCount[0] = totalObjects; 
                /////////////////////////////////////////////////////////////
                
                CustomBadge *badgeViewAll1  = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d", badgeCount[0]]];
                
                if(badgeCount[0]<10)
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelone.frame.size.width -28,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                else
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelone.frame.size.width -35,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                [buttonLevelone addSubview:badgeViewAll1]; 
                
                
            }
            else
            {
                cell.arrowImage.hidden=YES;
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,1,210,20)];
                myLabel.text =node.value;
                myLabel.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabel.textColor = [UIColor grayColor];}
                else {myLabel.textColor = [UIColor blackColor];}
                
                
                myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLevelone addSubview:myLabel];
                
                
                myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)]; //Steve CGRectMake(5,1,40,20)]
                myLabelOne.text=node.PriorityTODO;
                myLabelOne.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabelOne.textColor = [UIColor grayColor];}
                else {myLabelOne.textColor = [UIColor blackColor];}
                myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLevelone addSubview:myLabelOne];
                
                
            }
        }
        
        [cell.contentView addSubview:buttonLevelone];
    }
	
//************ END if(node.LevelVal==0 || node.TODO==0 )
    
    
    if(node.LevelVal==1 || node.TODO==1)
    {
        
        [buttonLevelone removeFromSuperview];
        UIImage *imgl2=[UIImage imageNamed:node.Fimg];
        buttonLeveltwo = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonLeveltwo setBackgroundImage:imgl2 forState:UIControlStateNormal];
        if (IS_IPAD)
        {
              buttonLeveltwo.frame = CGRectMake(54.0,7.0,340.0, 40.0);
        }
        else
        buttonLeveltwo.frame = CGRectMake(54.0,7.0,260.0, 40.0);
        if(node.isBTN==NO)
        {
            [buttonLeveltwo addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
            buttonLeveltwo.tag=indexPath.row + 1;
        }
        else
        {
            buttonLeveltwo.tag=node.TODOid;
            
            if(node.objectType == ToDo)
                [buttonLeveltwo addTarget:self action:@selector(gotoToday:) forControlEvents:UIControlEventTouchUpInside];
            else
                [buttonLeveltwo addTarget:self action:@selector(gotoNoteInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
		
		
        if(node.isBTN==YES)
        {
            
            
            if(node.PROGRES==100)
            {
                [proIndBorder removeFromSuperview];
                [myLabelOne removeFromSuperview];
                cell.arrowImage.hidden=YES;
                proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
                proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                [proIndBorder setFrame:CGRectMake(32,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
                [proIndBorder setTag:9909];
                proIndBorder.alpha=0.50;
                [cell.contentView addSubview:proIndBorder];
                
                ///Aman's added
                UIButton *invisibleIndBtn1 = [[UIButton alloc] initWithFrame:CGRectMake(34, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn1 setTag:indexPath.row];
                [invisibleIndBtn1 addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                [invisibleIndBtn1 addSubview:viewmB];
                [cell.contentView addSubview:invisibleIndBtn1];
            }
            else 
            {
                [proIndBorder removeFromSuperview];
                [pbutton removeFromSuperview];
                [myLabelOne removeFromSuperview];
                cell.arrowImage.hidden=YES;
                
                if(node.objectType == ToDo)
                {
                    
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBG" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                    
                }
                else if (node.objectType == NoteVal)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smallnotes" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                }
                
                cell.arrowImage.hidden=YES;
                
                
                pbutton=[UIButton buttonWithType:UIButtonTypeCustom];
                pbutton.backgroundColor=[UIColor redColor];
                [cell.contentView addSubview:pbutton];
				
                [proIndBorder setFrame:CGRectMake(32,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
				
                float buttonHeight = proIndBorderImage.size.height - 4;
                int percentComplete = node.PROGRES;
                float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
                [pbutton setFrame:CGRectMake(34, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, proIndBorderImage.size.width - 4, finalHieght)];
                
                UIButton *invisibleIndBtn1 = [[UIButton alloc] initWithFrame:CGRectMake(34, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn1 setTag:indexPath.row];
                [invisibleIndBtn1 addTarget:self
                                     action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                
                [invisibleIndBtn1 addSubview:viewmB];
                if(node.objectType == ToDo)
                {
                    [cell.contentView addSubview:invisibleIndBtn1];
                }
				
            }
			
        }
		
        if([node.titleType isEqualToString:@"H"])
        {
			
            UIImage *textImage = [[UIImage alloc] initWithData:node.titlebinary]; 			
            UIImageView *starImgView = [[UIImageView alloc] initWithFrame:
										CGRectMake(40,1,187,20)];
            starImgView.image=textImage ;
            if(node.PROGRES==100)
            { starImgView.alpha=0.50;}
            [buttonLeveltwo addSubview:starImgView];
            //[starImgView release];
            
            myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)];//Steve CGRectMake(2,1,40,20)]
            myLabelOne.text=node.PriorityTODO;
            myLabelOne.backgroundColor = [UIColor clearColor];
            if(node.PROGRES==100)
            {myLabelOne.textColor = [UIColor grayColor];}
            else {myLabelOne.textColor = [UIColor blackColor];}
            myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
            [buttonLeveltwo addSubview:myLabelOne];
        }
        else
        {
            if(node.isBTN==NO)
            {
                
                UILabel *myLabel;
                if([node.projectPriorty isEqualToString:@"None"])
                {
                    myLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,10,buttonLeveltwo.frame.size.width-40,30)];//(6,8,buttonLeveltwo.frame.size.width-40,30)
                    myLabel.text = node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                    myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
                else 
                {
                    UILabel *myLabel2;
                    //myLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(6,8,buttonLeveltwo.frame.size.width-40,30)];
                    myLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(4,10,buttonLeveltwo.frame.size.width-40,30)];//(6,8,buttonLeveltwo.frame.size.width-40,30)
                    myLabel2.text = node.projectPriorty;
                    myLabel2.backgroundColor = [UIColor clearColor];
                    myLabel2.textColor = [UIColor whiteColor];
                    myLabel2.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel2 setFont:[UIFont fontWithName:@"Helvetica" size:15]];
                    [buttonLeveltwo addSubview:myLabel2];
                    
                    if([node.projectPriorty length]>=3)
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,8,buttonLeveltwo.frame.size.width-40,30)]; 
                    }
                    else {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(28,10,buttonLeveltwo.frame.size.width-40,30)];//(30,8,buttonLeveltwo.frame.size.width-40,30)
                    }
                    myLabel.text = node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                    myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
                
                [buttonLeveltwo addSubview:myLabel];
                
                [self getBadgeCounts:node.ID];
                
                /////////////////// Calculating total badge counts //////
                int totalObjects;
                
                totalObjects = [self getBadgeCountsForNotes:node.ID];
                
                totalObjects = totalObjects + badgeCount[0];
                
                badgeCount[0] = totalObjects; 
                /////////////////////////////////////////////////////////////
                
                
                CustomBadge *badgeViewAll1  = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d", badgeCount[0]]];
                if(badgeCount[0]<10)
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLeveltwo.frame.size.width -28,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                else
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLeveltwo.frame.size.width -35,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                [buttonLeveltwo addSubview:badgeViewAll1]; 
            }
            else
            {
                cell.arrowImage.hidden=YES;
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,1,187,20)];
                myLabel.text = node.value;
                myLabel.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabel.textColor = [UIColor grayColor];}
                else {myLabel.textColor = [UIColor blackColor];}
                myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLeveltwo addSubview:myLabel];
                
                myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)];//Steve CGRectMake(2,1,40,20)]
                myLabelOne.text=node.PriorityTODO;
                myLabelOne.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabelOne.textColor = [UIColor grayColor];}
                else {myLabelOne.textColor = [UIColor blackColor];}
                myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLeveltwo addSubview:myLabelOne];
            }
        }
        [cell.contentView addSubview:buttonLeveltwo];
    }
	
//************ END if(node.LevelVal==1 || node.TODO==1 )
    
    if(node.LevelVal==2 || node.TODO==2)
    {
        
        [buttonLevelone removeFromSuperview];
        UIImage *imgl3=[UIImage imageNamed:node.Fimg];
        buttonLevelthree = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonLevelthree setBackgroundImage:imgl3 forState:UIControlStateNormal];
        if (IS_IPAD)
        {
            buttonLevelthree.frame = CGRectMake(82.0,7.0,300.0, 40.0);
        }else
        buttonLevelthree.frame = CGRectMake(82.0,7.0,232.0, 40.0);
        if(node.isBTN==NO)
        {
            [buttonLevelthree addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
            buttonLevelthree.tag=indexPath.row + 1;
            
        }
        else
        {
            buttonLevelthree.tag=node.TODOid;
            
            if(node.objectType == ToDo)
                [buttonLevelthree addTarget:self action:@selector(gotoToday:) forControlEvents:UIControlEventTouchUpInside];
            else
                [buttonLevelthree addTarget:self action:@selector(gotoNoteInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        //[buttonLevelthree setTitle:node.value forState:UIControlStateNormal];
		
		
        if(node.isBTN==YES)
        {
            if(node.PROGRES==100)
            {
                [proIndBorder removeFromSuperview];
                proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
                proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                [proIndBorder setTag:9909];
                [proIndBorder setFrame:CGRectMake(60,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
                [cell.contentView addSubview:proIndBorder];
                proIndBorder.alpha=0.50;
                ///Aman's added
                UIButton *invisibleIndBtn2 = [[UIButton alloc] initWithFrame:CGRectMake(60, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn2 setTag:indexPath.row];
                
                [invisibleIndBtn2 addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                [invisibleIndBtn2 addSubview:viewmB];
                
                [cell.contentView addSubview:invisibleIndBtn2];
            }
            else 
            {
                
                [proIndBorder removeFromSuperview];
                [pbutton removeFromSuperview];
                
                if(node.objectType == ToDo)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBG" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                }
                else if (node.objectType == NoteVal)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smallnotes" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                    
                }
                cell.arrowImage.hidden=YES;
				
                
                pbutton=[UIButton buttonWithType:UIButtonTypeCustom];
                pbutton.backgroundColor=[UIColor redColor];
                [cell.contentView addSubview:pbutton];
				
                [proIndBorder setFrame:CGRectMake(60,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
				
                float buttonHeight = proIndBorderImage.size.height - 4;
                int percentComplete = node.PROGRES;
                float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
                [pbutton setFrame:CGRectMake(62, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, proIndBorderImage.size.width - 4, finalHieght)];
                
                UIButton *invisibleIndBtn2 = [[UIButton alloc] initWithFrame:CGRectMake(60, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn2 setTag:indexPath.row];
                
                [invisibleIndBtn2 addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                
                [invisibleIndBtn2 addSubview:viewmB];
                if(node.objectType == ToDo)
                    
                {   
                    [cell.contentView addSubview:invisibleIndBtn2];
                }
				
            }
            
        }
        
        if([node.titleType isEqualToString:@"H"])
        {
			
            UIImage *textImage = [[UIImage alloc] initWithData:node.titlebinary]; 
			
            UIImageView *starImgView = [[UIImageView alloc] initWithFrame:
										CGRectMake(38,1,150,20)]; 
            starImgView.image=textImage ;
            if(node.PROGRES==100)
            { starImgView.alpha=0.50;}
            [buttonLevelthree addSubview:starImgView];
            
            
            
            myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)];//Steve CGRectMake(2,1,40,20)]
            myLabelOne.text=node.PriorityTODO;
            myLabelOne.backgroundColor = [UIColor clearColor];
            if(node.PROGRES==100)
            {myLabelOne.textColor = [UIColor grayColor];}
            else {myLabelOne.textColor = [UIColor blackColor];}	myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
            [buttonLevelthree addSubview:myLabelOne];
        }
        else
        {
            
            if(node.isBTN==NO)
            {
                
                UILabel *myLabel;
                if([node.projectPriorty isEqualToString:@"None"])
                {
                    
                    myLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,10,buttonLevelthree.frame.size.width-40,30)];//(6,8,buttonLevelthree.frame.size.width-40,30)
                    myLabel.text = node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                    //myLabel.font=[UIFont boldSystemFontOfSize:15];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
                else 
                {
                    
                    UILabel *myLabel3;
                    //myLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(6,8,buttonLevelthree.frame.size.width-40,30)];
                    myLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(4,10,buttonLevelthree.frame.size.width-40,30)];//(6,8,buttonLevelthree.frame.size.width-40,30)
                    myLabel3.text = node.projectPriorty;
                    myLabel3.backgroundColor = [UIColor clearColor];
                    myLabel3.textColor = [UIColor whiteColor];
                    myLabel3.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel3 setFont:[UIFont fontWithName:@"Helvetica" size:15]];
                    [buttonLevelthree addSubview:myLabel3];
                    
                    if([node.projectPriorty length]>=3)
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,8,buttonLevelthree.frame.size.width-40,30)]; 
                    }
                    else {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(28,10,buttonLevelthree.frame.size.width-40,30)];//(30,8,buttonLevelthree.frame.size.width-40,30)
                    }
                    
                    myLabel.text = node.value;
                    myLabel.backgroundColor = [UIColor clearColor];
                    myLabel.textColor = [UIColor whiteColor];
                    //myLabel.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                    
                }
                [buttonLevelthree addSubview:myLabel];
                
                [self getBadgeCounts:node.ID];
                
                /////////////////// Calculating total badge counts //////
                int totalObjects;
                
                totalObjects = [self getBadgeCountsForNotes:node.ID];
                
                totalObjects = totalObjects + badgeCount[0];
                
                badgeCount[0] = totalObjects; 
                /////////////////////////////////////////////////////////////
                
                
                CustomBadge *badgeViewAll1  = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d", badgeCount[0]]];
                if(badgeCount[0]<10)
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelthree.frame.size.width -28,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                else
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelthree.frame.size.width -35,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                [buttonLevelthree addSubview:badgeViewAll1]; 
            }
            else
            {
				
                cell.arrowImage.hidden=YES;
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(38,1,150,20)];
                myLabel.text = node.value;
                myLabel.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabel.textColor = [UIColor grayColor];}
                else {myLabel.textColor = [UIColor blackColor];}
                myLabel.font=[UIFont boldSystemFontOfSize:15];
                [buttonLevelthree addSubview:myLabel];
                
                myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)]; //Steve CGRectMake(2,1,40,20)]
                myLabelOne.text=node.PriorityTODO;
                myLabelOne.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabelOne.textColor = [UIColor grayColor];}
                else {myLabelOne.textColor = [UIColor blackColor];}	myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLevelthree addSubview:myLabelOne];
            }
        }
        [cell.contentView addSubview:buttonLevelthree];
		
    }
	
//************ END if(node.LevelVal==2 || node.TODO==2 )
    
    if(node.LevelVal==3 || node.TODO==3)
    {
		[buttonLevelone removeFromSuperview];
		UIImage *imgl4=[UIImage imageNamed:node.Fimg];
		buttonLevelfour = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelfour setBackgroundImage:imgl4 forState:UIControlStateNormal];
        if (IS_IPAD)
        {
            buttonLevelfour.frame = CGRectMake(110.0,7.0,260.0, 40.0);
        }
        else
		buttonLevelfour.frame = CGRectMake(110.0,7.0,204.0, 40.0);
        if(node.isBTN==NO)
        {
            [buttonLevelfour addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
            buttonLevelfour.tag=indexPath.row + 1;
            
        }
        else
        {
            buttonLevelfour.tag=node.TODOid;
            
            if(node.objectType == ToDo)
                [buttonLevelfour addTarget:self action:@selector(gotoToday:) forControlEvents:UIControlEventTouchUpInside];
            else
                [buttonLevelfour addTarget:self action:@selector(gotoNoteInfo:) forControlEvents:UIControlEventTouchUpInside];
            
        }
		// [buttonLevelfour setTitle:node.value forState:UIControlStateNormal];
		
		
        if(node.isBTN==YES)
        {
            if(node.PROGRES==100)
            {
                [proIndBorder removeFromSuperview];
                cell.arrowImage.hidden=YES;
                proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBGTicked" ofType:@"png"]];
                proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                [proIndBorder setTag:9909];
                [proIndBorder setFrame:CGRectMake(88,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
                [cell.contentView addSubview:proIndBorder];
                proIndBorder.alpha=0.50;
                ///Aman's added
                UIButton *invisibleIndBtn3 = [[UIButton alloc] initWithFrame:CGRectMake(88, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn3 setTag:indexPath.row];
                
                [invisibleIndBtn3 addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                [invisibleIndBtn3 addSubview:viewmB];
                [cell.contentView addSubview:invisibleIndBtn3];
            }
            else 
            {
                
                [proIndBorder removeFromSuperview];	
                [pbutton removeFromSuperview];	
                cell.arrowImage.hidden=YES; 
                
                
                if(node.objectType == ToDo)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"completionBG" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                }
                else if(node.objectType == NoteVal)
                {
                    proIndBorderImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smallnotes" ofType:@"png"]];
                    proIndBorder =  [[UIImageView alloc] initWithImage:proIndBorderImage];
                    [proIndBorder setTag:9909];
                    [cell.contentView addSubview:proIndBorder];
                }
                cell.arrowImage.hidden=YES;
				
                //UILabel *myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(proIndBorderImage.size.width+1,1,buttonLevelone.frame.size.width-66,20)];
                //myLabelOne.text=node.PriorityTODO;
                //myLabelOne.backgroundColor = [UIColor clearColor];
                //myLabelOne.textColor = [UIColor blackColor];
                //[buttonLevelfour addSubview:myLabelOne];
				
                pbutton=[UIButton buttonWithType:UIButtonTypeCustom];
                pbutton.backgroundColor=[UIColor redColor];
                [cell.contentView addSubview:pbutton];
				
                [proIndBorder setFrame:CGRectMake(88,10, proIndBorderImage.size.width, proIndBorderImage.size.height)];
				
                float buttonHeight = proIndBorderImage.size.height - 4;
                int percentComplete = node.PROGRES;
                float finalHieght =   (percentComplete * buttonHeight) / 100.0 ;
                [pbutton setFrame:CGRectMake(90, proIndBorder.frame.origin.y +  proIndBorder.frame.size.height - 2 - finalHieght, proIndBorderImage.size.width - 4, finalHieght)];
                
                UIButton *invisibleIndBtn3 = [[UIButton alloc] initWithFrame:CGRectMake(88, proIndBorder.frame.origin.y, 30, 30)];
                [invisibleIndBtn3 setTag:indexPath.row];
                
                [invisibleIndBtn3 addTarget:self action:@selector(progressBtn_Clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UILabel *viewmB=[[UILabel alloc]init];
                viewmB.text =[NSString stringWithFormat:@"%d~%d",node.TODOid,node.PROGRES];//Aman's Changed
                viewmB.tag =666;
                [invisibleIndBtn3 addSubview:viewmB];
                if(node.objectType == ToDo)
                {
                    [cell.contentView addSubview:invisibleIndBtn3];
                }
				
            }
			
        }
		
		
        if([node.titleType isEqualToString:@"H"])
        {
			
            UIImage *textImage = [[UIImage alloc] initWithData:node.titlebinary]; 
            UIImageView *starImgView = [[UIImageView alloc] initWithFrame:
										CGRectMake(40,1,120,20)]; 
            starImgView.image=textImage ;
            if(node.PROGRES==100)
            { starImgView.alpha=0.50;}
            [buttonLevelfour addSubview:starImgView];
            //[starImgView release];
            
            
            myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)];//Steve CGRectMake(2,1,40,20)]
            myLabelOne.text=node.PriorityTODO;
            myLabelOne.backgroundColor = [UIColor clearColor];
            if(node.PROGRES==100)
            {myLabelOne.textColor = [UIColor grayColor];}
            else {myLabelOne.textColor = [UIColor blackColor];}
            myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
            [buttonLevelfour addSubview:myLabelOne];
        }
        else
        {
            if(node.isBTN==NO)
            {
                UILabel *myLabel  ;         
                
                if([node.projectPriorty isEqualToString:@"None"])
                {
                    myLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,10,buttonLevelfour.frame.size.width-40,30)]; //(8,8,buttonLevelfour.frame.size.width-40,30)
                    
                }
                else 
                {
                    UILabel *myLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(4,10,buttonLevelfour.frame.size.width-40,30)];//(4,7,buttonLevelfour.frame.size.width-40,30)
                    myLabel4.text = node.projectPriorty;
                    myLabel4.backgroundColor = [UIColor clearColor];
                    myLabel4.textColor = [UIColor whiteColor];
                    myLabel4.font=[UIFont boldSystemFontOfSize:15.0];
                    [myLabel4 setFont:[UIFont fontWithName:@"Helvetica" size:15]];
                    [buttonLevelfour addSubview:myLabel4];
                    if([node.projectPriorty length]>=3)
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,7,buttonLevelfour.frame.size.width-40,30)];
                    }
                    else 
                    {
                        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(28,10,buttonLevelfour.frame.size.width-40,30)];//(28,7,buttonLevelfour.frame.size.width-40,30)
                    }
                    
                }
                
                
                myLabel.text = node.value;
                myLabel.backgroundColor = [UIColor clearColor];
                myLabel.textColor = [UIColor whiteColor];
                myLabel.font=[UIFont boldSystemFontOfSize:15];
                [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
                [buttonLevelfour addSubview:myLabel];
                
                
                [self getBadgeCounts:node.ID];
                /////////////////// Calculating total badge counts //////
                int totalObjects;
                
                totalObjects = [self getBadgeCountsForNotes:node.ID];
                
                totalObjects = totalObjects + badgeCount[0];
                
                badgeCount[0] = totalObjects; 
                /////////////////////////////////////////////////////////////
                
				
                CustomBadge *badgeViewAll1  = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d", badgeCount[0]]];
                if(badgeCount[0]<10)
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelfour.frame.size.width -28,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                else
                {
                    [badgeViewAll1 setFrame:CGRectMake(buttonLevelfour.frame.size.width -35,15, badgeViewAll1.frame.size.width, badgeViewAll1.frame.size.height)];
                }
                [buttonLevelfour addSubview:badgeViewAll1];  
            }
            else
            {
                cell.arrowImage.hidden=YES;
                UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,1,120,20)];
                myLabel.text = node.value;
                myLabel.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabel.textColor = [UIColor grayColor];}
                else {myLabel.textColor = [UIColor blackColor];}
                myLabel.font=[UIFont boldSystemFontOfSize:15];
                [buttonLevelfour addSubview:myLabel];
                
                myLabelOne = [[UILabel alloc] initWithFrame:CGRectMake(5,1,40,20)];//Steve CGRectMake(2,1,40,20)]
                myLabelOne.text=node.PriorityTODO;
                myLabelOne.backgroundColor = [UIColor clearColor];
                if(node.PROGRES==100)
                {myLabelOne.textColor = [UIColor grayColor];}
                else {myLabelOne.textColor = [UIColor blackColor];}
                myLabelOne.font=[UIFont boldSystemFontOfSize:15.0];
                [buttonLevelfour addSubview:myLabelOne];
				
				
            }
        }
		
		
        [cell.contentView addSubview:buttonLevelfour];
		
        if(isEdit==YES && node.isBTN==NO)
        {
            UIImage *imgl4=[UIImage imageNamed:@"Edit.png"];
            UIButton *dbtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [dbtn setBackgroundImage:imgl4 forState:UIControlStateNormal];
            dbtn.frame = CGRectMake(150.0,12.50,40.0, 25.0);
            dbtn.tag=node.ID;
            [dbtn addTarget:self action:@selector(Edit_Project:) forControlEvents:UIControlEventTouchUpInside];
            [buttonLevelfour addSubview:dbtn];
        }
    }
    
//************ END if(node.LevelVal==3 || node.TODO==3 )
    
	
	//---------------------------------------
    
    //sets up folder button and "Plus" (Add) buttons on cell
    if(node.isBTN==NO)
    {
        // NSLog(@"node.isBTN ==NO");
		
        //Adds folder "+" button only if node.LevelVal < 3
        if(node.LevelVal<3)
        {
            UIImage *nImg=[UIImage imageNamed:@"Plus.png"];
            UIButton *dbtnNew = [UIButton buttonWithType:UIButtonTypeCustom];
            [dbtnNew setImage:nImg forState:UIControlStateNormal];
            dbtnNew.frame = CGRectMake(100.0,50.0,20.0, 20.0);
            [dbtnNew addTarget:self action:@selector(gotoFolder:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:dbtnNew];
			
            UILabel *viewmB=[[UILabel alloc]init];
            viewmB.text =[NSString stringWithFormat:@"%d~%d",node.LevelVal,node.ID];
            viewmB.tag =666;
            [dbtnNew addSubview:viewmB];
            //[viewmB release];
			
			
            UIButton *dmBTN=[UIButton buttonWithType:UIButtonTypeCustom];	
            dmBTN.frame = CGRectMake(120.0,50.0,55.0, 24.0);
            [dmBTN addTarget:self action:@selector(gotoFolder:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:dmBTN];
			
            UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0,0.0,55.0, 24.0)];
            myLabel.text = @"Add Folder";
            myLabel.backgroundColor = [UIColor clearColor];
            myLabel.textColor = [UIColor blackColor];
            myLabel.font=[UIFont boldSystemFontOfSize:10];
            [dmBTN addSubview:myLabel];
			
            UILabel *viewmB1=[[UILabel alloc]init];
            viewmB1.text =[NSString stringWithFormat:@"%d~%d",node.LevelVal,node.ID];
            viewmB1.tag =666;
            [dmBTN addSubview:viewmB1];	
			
        }
        
         ////////////////////////////////  Add to do Button //////////////////////////////
		UIImage *nImg2=[UIImage imageNamed:@"Plus.png"];
		UIButton *dbtnNew2 = [UIButton buttonWithType:UIButtonTypeCustom];
		[dbtnNew2 setBackgroundImage:nImg2 forState:UIControlStateNormal];
		dbtnNew2.frame = CGRectMake(175.0,50.0,20.0, 20.0);
		[dbtnNew2 addTarget:self action:@selector(gotoTodo:) forControlEvents:UIControlEventTouchUpInside];
		UILabel *viewmB2=[[UILabel alloc]init];
		viewmB2.text =[NSString stringWithFormat:@"%d~%@~%d",node.ID,node.value,node.prntID];
		viewmB2.tag =777;
		[dbtnNew2 addSubview:viewmB2];
		[cell.contentView addSubview:dbtnNew2];
		
		UIButton *todobtn=[UIButton buttonWithType:UIButtonTypeCustom];
		todobtn.frame = CGRectMake(195.0,50.0,55.0, 24.0);
		[todobtn addTarget:self action:@selector(gotoTodo:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:todobtn];
		
		UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(00.0,00.0,55.0, 24.0)];
		myLabel.text = @"Add To Do";
		myLabel.backgroundColor = [UIColor clearColor];
		myLabel.textColor = [UIColor blackColor];
		myLabel.font=[UIFont boldSystemFontOfSize:10];
		
		UILabel *viewmB23=[[UILabel alloc]init];
		viewmB23.text =[NSString stringWithFormat:@"%d~%@~%d",node.ID,node.value,node.prntID];
		viewmB23.tag =777;
		[dbtnNew2 addSubview:viewmB2];
		[todobtn addSubview:viewmB23];
        
		[todobtn addSubview:myLabel];
        
       //NSLog(@" node.ID = %d", node.ID);
        //NSLog(@" node.value = %@", node.value);
        //NSLog(@" node.prntID = %d", node.prntID);
        
        
        
        ////////////////////////////////  Add Note Button //////////////////////////////
        UIImage *nImg3=[UIImage imageNamed:@"Plus.png"];
		UIButton *dbtnNew3 = [UIButton buttonWithType:UIButtonTypeCustom];
		[dbtnNew3 setBackgroundImage:nImg3 forState:UIControlStateNormal];
		dbtnNew3.frame = CGRectMake(245.0,50.0,20.0, 20.0);
		[dbtnNew3 addTarget:self action:@selector(goToAddNotes:) forControlEvents:UIControlEventTouchUpInside];
		UILabel *viewmB3=[[UILabel alloc]init];
		viewmB3.text =[NSString stringWithFormat:@"%d~%@~%d",node.ID,node.value,node.prntID];
		viewmB3.tag =888;
		[dbtnNew3 addSubview:viewmB3];
		[cell.contentView addSubview:dbtnNew3];
		
		UIButton *notebtn=[UIButton buttonWithType:UIButtonTypeCustom];
		notebtn.frame = CGRectMake(265.0,50.0,55.0, 24.0);
		[notebtn addTarget:self action:@selector(goToAddNotes:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:notebtn];
		
		UILabel *myLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(00.0,00.0,55.0, 24.0)];
		myLabel2.text = @"Add Note";
		myLabel2.backgroundColor = [UIColor clearColor];
		myLabel2.textColor = [UIColor blackColor];
		myLabel2.font=[UIFont boldSystemFontOfSize:10];
		
		UILabel *viewmB24=[[UILabel alloc]init];
		viewmB24.text =[NSString stringWithFormat:@"%d~%@~%d",node.ID,node.value,node.prntID];
		viewmB24.tag =888;
		[dbtnNew3 addSubview:viewmB24];
		[notebtn addSubview:viewmB24];
        
		[notebtn addSubview:myLabel2];
        
        
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (IS_IOS_7) {
        cell.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    
    return cell;
	
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    

    
    //	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
    //	if (!node.hasChildren) return;
    //
    //	node.inclusive = !node.inclusive;
    //	[treeNode flattenElementsWithCacheRefresh:YES];
    //	[tableView reloadData];
}


-(void)PlusNow:(id)sender
{
    //NSLog(@"PlusNow");
    
 	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]];
    //NSLog(@"Todo id %d",node.TODOid);
    
    if (!node.hasChildren) return;
    
    node.inclusive = !node.inclusive;
	[treeNode flattenElementsWithCacheRefresh:YES];
    [mytable reloadData];
    [self updateInclusiveValue:node.inclusive :node.ID];
    
}

-(void)updateInclusiveValue:(BOOL)_inclusive:(int)_treeID{
     //NSLog(@"updateInclusiveValue");
    
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(appDelegate.database) 
	{
        NSString *sql = [[NSString alloc]initWithFormat:@"Update ProjectMaster set Inclusive=%d  where projectID=%d ",_inclusive,_treeID];
		//NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	if(SQLITE_DONE != sqlite3_step(insertProjectData))
	{
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	} 
	else 
	{
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
        
	}	
    
}


#pragma mark Calculate Badge counts



//-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId 
-(void)getBadgeCounts:(NSInteger)value {
	
	badgeCount[0] = 0;
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
		NSString  *Sql = [[NSString alloc] initWithFormat:@"SELECT COUNT(TODOID)  from ToDoTable WHERE BelongstoProjectID=%d and progress != 100",value];
		
		//NSLog(@"%@",Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
				badgeCount[0] = sqlite3_column_int(selectBadgeStmt, 0);
				
				//NSLog(@"starImage %d", badgeCount[0]);
			}
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
	}
	selectBadgeStmt = nil;
	
	return;
}

-(int)getBadgeCountsForNotes:(NSInteger)value {
	
    int notesBadgeCount;
    
    notesBadgeCount = 0;
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectBadgeStmt = nil;
	
	
	if(selectBadgeStmt == nil && appDelegate.database) {
		
		
		NSString  *Sql = [[NSString alloc] initWithFormat:@"SELECT COUNT(NoteID)  from NotesMaster WHERE BelongstoProjectID=%d",value];
		
		//NSLog(@"%@",Sql);
		
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectBadgeStmt, NULL) == SQLITE_OK) {
			if (sqlite3_step(selectBadgeStmt) == SQLITE_ROW) {
				notesBadgeCount = sqlite3_column_int(selectBadgeStmt, 0);
			}
			sqlite3_finalize(selectBadgeStmt);		
			selectBadgeStmt = nil;
		}
	}
	selectBadgeStmt = nil;
	
	return notesBadgeCount;
}

#pragma mark Add New Note

-(void)goToAddNotes:(id)sender
{
    NSLog(@"tag is = %ld",(long)[sender tag]);
    [_dictData setValue:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] forKey:KeyNotesData];
    [self newNotesOpen:sender];
    
    /*
    //Alok Added Get Count Number of Notes's
    NSString *selectStat=@"select count(NoteID) from NotesMaster";
    
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];   
    if(CountRecords >= MAX_LIMIT_NOTE && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Notes Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d notes.",MAX_LIMIT_NOTE] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgade", nil];
        [alert show];
        return;
    }
    
    NSString *getSTR = [(UILabel *)[sender viewWithTag:888] text];
    
	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
	//NSLog(@"=============%@",components);
    
    NSString *viewId;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        viewId = @"AddNewNotesViewController_iPad";
    }else{
        viewId = @"AddNewNotesViewController2";
    }
    
    AddNewNotesViewCantroller *_addNewNotesViewController  = [[AddNewNotesViewCantroller alloc] initWithNibName:viewId bundle:nil];
    _addNewNotesViewController.projectComponentsArray = components;
    _addNewNotesViewController.Flag = NO;
    _addNewNotesViewController.isComingFromProjectModule = YES;
    _addNewNotesViewController.prntVAL = [[components objectAtIndex:0] intValue];
    [self.navigationController pushViewController:_addNewNotesViewController animated:YES];
    _addNewNotesViewController = nil;
     
     */
    
  }

-(void)newNotesOpen:(id)sender
{
    
    NSString *getSTR = [(UILabel *)[sender viewWithTag:888] text];
    
    NSArray *components = [getSTR componentsSeparatedByString:@"~"];
 
    
    NSLog(@"%d",[[components objectAtIndex:0] intValue]);
    
     ACEViewController *aACEViewController ;
    
    if(IS_IPAD)
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"ACEViewController" bundle:nil];
    else
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"AVEViewController-iPhn" bundle:nil];
    
     
    aACEViewController.isComingFromProjectModule=YES;
    aACEViewController.isFetchDataOnly=NO;
    aACEViewController.prntVAL=[[components objectAtIndex:0] intValue];
    
    
    NSInteger noteCount;;
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"noteCount"])
    {
        noteCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"noteCount"] integerValue]+1;
        
    }else
    {
        noteCount=1;
    }
    
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInteger:noteCount] forKey:@"noteCount"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [aACEViewController newRecordOrExistingRecord:YES withTotalCount:noteCount];

     UINavigationController* nav=[[UINavigationController alloc]initWithRootViewController:aACEViewController];
    
    if (IS_IPAD)
    {
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        
    }else
    {
        [self.navigationController pushViewController:aACEViewController animated:YES];
    }

 
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1)
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];  
}

-(void)gotoTodo:(id)sender
{
    //NSLog(@"gotoTodo");
    
    //Alok Added Get Count Number of todo's 
    NSString *selectStat=@"select count(TODOID) from ToDoTable";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    
    if(CountRecords >= MAX_LIMIT_TODO && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Task Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d todos.",MAX_LIMIT_TODO] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
        return;
    }
    
    //Steve changed
        
    NSString *getSTR = [(UILabel *)[sender viewWithTag:777] text];
    NSArray *components = [getSTR componentsSeparatedByString:@"~"];
    
    NSString *projectName = [components objectAtIndex:1];
    int      *projectTag = [[components objectAtIndex:0] intValue];
    BOOL     isAdd = YES;
    
    
    ToDoViewController *todoViewController = [[ToDoViewController alloc]initWithProjectName:projectName projectTagInt:projectTag isAdd:isAdd];
    [self.navigationController pushViewController:todoViewController animated:YES];
    
    todoViewController.prntVAL= [[components objectAtIndex:2]intValue];
    [todoViewController.projectNameLabel setText:[components objectAtIndex:1]];
    [todoViewController.projectNameLabel setTag:[[components objectAtIndex:0]intValue]];
    todoViewController.isAdd=YES;
    
     //NSLog(@"=============%@",components);
     //NSLog(@"projectName =  %@", projectName);
     //NSString *getSTR = [(UILabel *)[sender viewWithTag:777] text];
     //NSArray *components = [getSTR componentsSeparatedByString:@"~"];
     //NSLog(@"=============%@",components);
     //NSLog(@"todocantroller.projectNameLabel =  %@", todocantroller.projectNameLabel);
    
}

-(void)gotoFolder:(id)sender
{
    //NSLog(@"gotoFolder");
    
    //Steve modified to limit Lite version adding folders & subfolders 
    NSString *selectStat=@"select count(projectID) from ProjectMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];
    
    
    if(CountRecords >= MAX_LIMIT_PROJECT && IS_LITE_VERSION) //Steve added IS_LITE_VERSION
    {
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Project Folder Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d project folders and subfolders.",MAX_LIMIT_PROJECT] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgrade", nil];
        [alert show];
    }
    else{
        
        if (IS_IOS_7) {
            
            NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
            NSArray *components = [getSTR componentsSeparatedByString:@"~"];
            // NSLog(@"=============%@",components);
            
            AddProjectViewCantroller *Prjctview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantrollerIPad" bundle:[NSBundle mainBundle]];
            Prjctview.optionVal =YES;
            Prjctview.belongValue=[[components objectAtIndex:1]intValue];
            Prjctview.levelValue=[[components objectAtIndex:0]intValue]+1;
            [self.navigationController pushViewController:Prjctview animated:YES];
        }
        else{ //iOS 6
            
            NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
            NSArray *components = [getSTR componentsSeparatedByString:@"~"];
            // NSLog(@"=============%@",components);
            
            AddProjectViewCantroller *Prjctview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantroller" bundle:[NSBundle mainBundle]];
            Prjctview.optionVal =YES;
            Prjctview.belongValue=[[components objectAtIndex:1]intValue];
            Prjctview.levelValue=[[components objectAtIndex:0]intValue]+1;
            [self.navigationController pushViewController:Prjctview animated:YES];
        }
        
    }
	
}

#pragma mark -
#pragma mark Table view delegate

-(IBAction)cancel_click:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark ViewTODOScreen
-(void)gotoToday:(id)sender
{
   // NSLog(@"gotoToday");
    
	//NSLog(@"%d",[sender tag]);
    
	[self FindAllToDo:[sender tag]];

	viewToDOScreen = [[ViewTODOScreen alloc] initWithToDoObject:toDODataObject];
    [self.navigationController pushViewController:viewToDOScreen animated:YES]; //Steve added    
    
}

#pragma mark Go to Notes detail page
-(void)gotoNoteInfo:(UIButton*)sender
{
    
    
    NSString  * tittleIS=[self SelectTitleNotes:[sender tag]];
    
    NSLog(@"%@",tittleIS);
    
    
    //NSLog(@"gotoNoteInfo");
    [_dictData setValue:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny] forKey:KeyNotesData];
    
    
    
    [mytable reloadData];
    ACEViewController *aACEViewController ;
    
    if(IS_IPAD)
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"ACEViewController" bundle:nil];
    else
        aACEViewController = [[ACEViewController alloc]initWithNibName:@"AVEViewController-iPhn" bundle:nil];
    
    
    aACEViewController.strValueSelectedFromDatabase=tittleIS;
    
    //    [aACEViewController setRowSelected:[[_dictData valueForKey:KeyNotesData] objectAtIndex:indexVal] atSelectedIndex:indexVal withTotalCount: ((NSArray*)[_dictData valueForKey:KeyNotesData]).count ];
    
    [aACEViewController newRecordOrExistingRecord:NO withTotalCount:((NSArray*)[_dictData valueForKey:KeyNotesData]).count];
    aACEViewController.isFetchDataOnly=YES;
    aACEViewController.isComingFromProjectModule=YES;
    UINavigationController* nav=[[UINavigationController alloc]initWithRootViewController:aACEViewController];
    
    if (IS_IPAD)
    {
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        
    }else
    {
        [self.navigationController pushViewController:aACEViewController animated:YES];
    }
    
    
    //    NSString *viewId;
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    //
    //        viewId = @"NotesFullViewCantroller_IPad";
    //    }else{
    //        viewId = @"NotesFullViewCantroller";
    //    }
    //
    //    NotesFullViewCantroller *_notesFullViewController = [[NotesFullViewCantroller alloc] initWithNibName:viewId bundle:nil];
    //
    //    //   _notesFullViewController.fetchNotesObject = [self getNotesObject:[sender tag]];
    //    _notesFullViewController.fetchNotesObject = [sender tag];
    //    [self.navigationController pushViewController:_notesFullViewController animated:YES];
    
}
#pragma mark -
#pragma mark FindAllToDo DataObject

-(void)FindAllToDo:(NSInteger)todoVal
{
    //NSLog(@"FindAllToDo");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *selectAllTODO = nil;
	NSString *Sql = @"";
	
	NSString *selectStr = @"Select a.TODOID, a.Title, a.TitleTextType, a.TitleBinary, a.BelongstoCalenderID, a.BelongstoProjectID, a.DateStarted, a.DueDate, a.DateCreated,  a.DateModified, a.Progress, IFNULL(a.Priority, ''), a.IsStarred, a.ManuallySortTodoID, IFNULL(b.ProjectName, 'ZZZ'), IFNULL(c.CalendarName, ''),a.TitleBinaryLarge,a.Alert,a.AlertSecond,a.isAlertOn,a.PerntID From ToDoTable a Left Outer Join ProjectMaster b on a.BelongstoProjectID = b.ProjectID  Left Outer Join CalendarTable c on a.BelongstoCalenderID = c.CalendarID";
	
	NSString *prioritySortStr = @"Case when (a.Priority ISNULL or a.Priority Like '') Then 1000 Else case when SubStr(a.Priority, 1, 1) = 'A' then CAST( SubStr(a.Priority, 2, length(a.Priority)) as integer) else (case when SubStr(a.Priority,1,1) = 'B' then CAST( SubStr(a.Priority, 2, length(a.Priority)) as integer) + 10 else CAST( SubStr(a.Priority, 2, length(a.Priority)) as integer) + 20 end) end End";
	
	NSString *whereClauseStr=[NSString stringWithFormat:@"Where TODOID=%d",todoVal];
	
	
	if(selectAllTODO == nil && appDelegate.database) {
		
		
        Sql = [NSString stringWithFormat:@"%@ %@ Order By a.DateCreated, %@ Asc",  selectStr, whereClauseStr, prioritySortStr];
		
		
		
        //NSLog(@"THe Final Query ::: %@", Sql);
		
        if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAllTODO, NULL) == SQLITE_OK) {
			
            while (sqlite3_step(selectAllTODO) == SQLITE_ROW) {
				
                NSUInteger blobLength = sqlite3_column_bytes(selectAllTODO, 3);
                NSData *binaryData = [NSData dataWithBytes:sqlite3_column_blob(selectAllTODO, 3) length:blobLength];
				
                toDODataObject = [[TODODataObject alloc] init];
				
                [toDODataObject setTodoID:sqlite3_column_int(selectAllTODO, 0)];
                [toDODataObject setTodoTitle:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 1) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 1)]];
                [toDODataObject setTodoTitleType:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 2)]];
                [toDODataObject setTodoTitleBinary:binaryData];
                [toDODataObject setCalID:sqlite3_column_int(selectAllTODO, 4)];
                [toDODataObject setProjID:sqlite3_column_int(selectAllTODO, 5)];
				
                [toDODataObject setStartDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 6)]];
                [toDODataObject setDueDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 7)]];
                [toDODataObject setCreateDateTime:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 8)]];
				
                [toDODataObject setModifyDateTime:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 9) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 9)]];
				
                [toDODataObject setToDoProgress:sqlite3_column_int(selectAllTODO, 10)];
				
                [toDODataObject setPriority:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 11) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 11)]];
				
                [toDODataObject setIsStarred:sqlite3_column_int(selectAllTODO, 12)];
                [toDODataObject setManuallySortedTodoID:sqlite3_column_int(selectAllTODO, 13)];
				
                [toDODataObject setProjectName:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 14) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 14)]];
                [toDODataObject setCalendarName:[NSString stringWithUTF8String: (char *)sqlite3_column_text(selectAllTODO, 15) == NULL ? "" :(char *)sqlite3_column_text(selectAllTODO, 15)]];
				
                NSUInteger blobLengthLarge = sqlite3_column_bytes(selectAllTODO, 16);
                NSData *binaryDataLarge = [NSData dataWithBytes:sqlite3_column_blob(selectAllTODO, 16) length:blobLengthLarge];
                [toDODataObject setAlert:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 17)]]; // Anil's Addition
                [toDODataObject setAlertSecond:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllTODO, 18)]]; // Anil's Addition
                
                [toDODataObject setIsAlertOn:sqlite3_column_int(selectAllTODO, 19)];// Anil's Addition 
                [toDODataObject setProjPrntID:sqlite3_column_int(selectAllTODO, 20)];//Alok's Added
                
                [toDODataObject setTodoTitleBinaryLarge:binaryDataLarge];
				
				
            }
			sqlite3_finalize(selectAllTODO);		
			selectAllTODO = nil;
		}
	}
	selectAllTODO = nil;
	return;
	
}


-(IBAction)Done_Project:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Edit Project

-(IBAction)Edit_Project:(id)sender
{
    EditProjectViewCantroller * editproject;

	if(IS_IPAD)
    {
        editproject = [[EditProjectViewCantroller alloc] initWithNibName:@"EditProjectViewCantrollerIPad" bundle:[NSBundle mainBundle]];
    }else
    {
        editproject = [[EditProjectViewCantroller alloc] initWithNibName:@"EditProjectViewCantroller" bundle:[NSBundle mainBundle]];
        
	
    }
    editproject.EPIDval=PIDval;
	editproject.optnViewTWO=optnView;
	[self.navigationController pushViewController:editproject animated:YES];
	
	
}
-(NSString *)SelectTitleNotes:(NSInteger)NodID
{
    OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *selectNotes_Stmt = nil;
    
    if(selectNotes_Stmt == nil && appDelegate.database)
    {
        
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT NoteID,NotesTitleType,NoteTitle,NotesTitleBinary,BelongstoProjectID FROM NotesMaster WHERE NoteID= %d",NodID];
        
        //	NSLog(@" query %@",Sql);
        if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK)
        {
            NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
        else
        {
            
            while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW)
            {
                NotesDataObject *notedataobj = [[NotesDataObject alloc]init];
                
                [notedataobj setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [notesDataObject setNotesID:sqlite3_column_int(selectNotes_Stmt, 0)];
                [notedataobj setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [notedataobj setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                
                NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                
                NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:blobLength];
                
                [notedataobj setNotesTitleBinary:binaryData];
                [notesDataObject setBelongsToProjectID:sqlite3_column_int(selectNotes_Stmt, 4)];
                
                
                
                noteTittleStr=notedataobj.notesTitle;
                
                //[FindTitleArray addObject:notedataobj.notesTitle];
                
                // [FindTitleArray addObject:notedataobj];
                //	[notedataobj release];
                
            }
            sqlite3_finalize(selectNotes_Stmt);
            selectNotes_Stmt = nil;
        }
        //[Sql release]; //Steve commented for ARC
    }
    // [notesDataObject setNotesTitleArray:FindTitleArray];
    
    
    
    
    
    return noteTittleStr;
}

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [doneButton removeFromSuperview];
        [editButton removeFromSuperview];
        [projectViewLabel removeFromSuperview];
        [_editButtoniOS7 removeFromSuperview];
    }
    
}



- (void)viewDidUnload 
{
    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    swipeDownHelpView = nil;
    backButton = nil;
    projectViewLabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

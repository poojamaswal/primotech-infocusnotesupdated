//
//  AddProjectViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectDataObject.h"
#import "MyTreeNode.h"

@protocol AddProjectViewCantrollerDelegate
-(void)setPriorityNew:(NSString*)priority1 priority2:(NSString*)priority2;
@end


@interface AddProjectViewCantroller : UIViewController <UIPickerViewDelegate ,UIPickerViewDataSource>
{
	
	
	IBOutlet UITableView *showTable;
	IBOutlet UITextField *addproject;
	IBOutlet UILabel *addprojectpriority;
	IBOutlet UILabel *addprojectsubfolder;
	IBOutlet UILabel *quickviewlabel;
	IBOutlet UILabel *levelTxT;
	IBOutlet UILabel *subTxT;
	UIButton *savebtn;
	IBOutlet UIButton *pButton;
	IBOutlet UIButton *subbutton;
	IBOutlet UIButton *colorbutton;
	IBOutlet UIButton *subfolderBTN;
	
	AddProjectDataObject *projectdataObj;
	NSMutableArray *arrayNo;
	NSMutableArray *arrayCR;
	UIPickerView *myPickerView;
	UIButton *pcView;
	
	NSInteger FindProjectID;
	BOOL Editoptn;
	BOOL LEVELOPT;
	NSString *sql;
	UIImage *saveIMG;
	NSInteger belongValue;
	NSInteger levelValue;
	NSString *folderName;
	int myOptionFind;
	NSInteger Fval;

	NSString *Sql;
	BOOL optionVal;
	NSInteger val;
	NSMutableArray *getBIDArray;
	NSInteger getfId;
	//NSString *newName;
	NSInteger IDval;
	NSInteger LEVELval;
	NSInteger checkLEVEL;
	NSInteger findIDVal;
	
	NSInteger checkChild;
	NSInteger newViewValue;
	UIPanGestureRecognizer *pan;
	id __unsafe_unretained delegate;
	
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    UIButton          *saveButton;
    UILabel           *projectAddLabel;
    UIButton          *deleteButton;
	
    
}
@property(unsafe_unretained)id   delegate;
//@property(nonatomic,retain)NSString *newName;
@property(nonatomic,assign)NSInteger getfId;
@property(nonatomic,assign)NSInteger newViewValue;
@property(nonatomic,strong)NSMutableArray *getBIDArray;
@property(nonatomic)BOOL optionVal;
@property(assign)int myOptionFind;
@property(assign)BOOL LEVELOPT;
@property(nonatomic,assign)NSInteger belongValue;
@property(nonatomic,assign)NSInteger levelValue;
@property(nonatomic,strong)NSString *sql;
@property(nonatomic,strong)NSMutableArray *arrayNo;
@property(nonatomic,strong)NSMutableArray *arrayCR;
@property(nonatomic,assign)NSInteger FindProjectID;
@property(assign)BOOL Editoptn;

@property(nonatomic,strong)AddProjectDataObject *projectdataObj;
@property(nonatomic,strong)UIView *addView;
@property(nonatomic,strong)UITextField *addproject;
@property(nonatomic,strong)UILabel *addprojectpriority;
@property(nonatomic,strong)UILabel *addprojectsubfolder;

-(IBAction)homeButton_Clicked:(id)sender;
-(IBAction)rgb_click:(id)sender;
-(IBAction)click_Subfolder:(id)sender;
-(IBAction)priority_click:(id)sender;
-(IBAction)save_click:(id)sender;
//-(void)alertDone;
-(void)getPId_data:(NSInteger)Val;
//-(void)alerDelete ;
-(IBAction)delete_click:(id)sender;
-(void)delete_Project:(NSInteger)fval;
-(void)dum:(NSMutableArray *)faa;
//-(BOOL)delete_Project_fromtodo;
-(void)todo_Find:(NSInteger)bValue;
-(void)update_todo:(NSInteger)CValue;
//-(IBAction)clickBTN;
-(void)getSubfolder:(NSInteger)fID;
//-(void)UpdateNew:(NSInteger)NID;
//-(void)updateFinal:(NSInteger)updateVal updatevalID:(NSInteger)upID;
-(void)checkIT:(NSInteger)chkVAL;
-(void)updateLevelOne:(NSInteger)BID;
-(void)checkCHLD:(NSInteger)CHKD;
-(IBAction)can_click:(id)sender;
-(void)getMainProjectImage:(NSInteger)Val; // Added by anil
-(void)delete_notes:(NSInteger)CValue; // Added by anil

@end

//
//  ProjectPriorityViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 12/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectViewCantroller.h"
#define kFillingComponent  0
#define kBreadComponent  1


@interface ProjectPriorityViewCantroller : UIViewController<AddProjectViewCantrollerDelegate> 

{
	IBOutlet UIPickerView *doublePicker;
	NSArray *fillingTypes;
	NSArray *breadTypes;
	id __unsafe_unretained delegate;
	NSString *first;
	NSString *second;
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    
}
@property(unsafe_unretained)id delegate;

@property (nonatomic,strong)  UIPickerView *doublePicker;
@property (nonatomic,strong)  NSArray *fillingTypes;
@property (nonatomic,strong)  NSArray *breadTypes;
@property (strong, nonatomic) IBOutlet UILabel *priorityLabel; //Steve
@property (strong, nonatomic) IBOutlet UISwitch *prioritySwitch; //Steve
@property (strong, nonatomic) IBOutlet UILabel *priorityFixedLabel;//Steve



-(IBAction)buttonPressed;
-(IBAction)Clicl_Cancel:(id)sender;
-(IBAction)Click_Save:(id)sender;
- (IBAction)prioritySwitch_Changed:(id)sender; //Steve

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil setNewPriority:(NSString*) valueone;

@end
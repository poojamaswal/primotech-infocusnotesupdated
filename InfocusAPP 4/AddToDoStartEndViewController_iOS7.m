//
//  AddToDoAlertViewController_iOS7.m
//  Organizer
//
//  Created by Steven Abrams on 10/13/13.
//
//

#import "AddToDoStartEndViewController_iOS7.h"

@interface AddToDoStartEndViewController_iOS7 (){
    
    BOOL isShowingStartDate;
    BOOL isShowingEndDate;
}

@property (strong, nonatomic) IBOutlet UIView *nonBlurredBackgroundView;
@property (strong, nonatomic) IBOutlet UIImageView *nonBlurredBackgroundImageView;
@property (strong, nonatomic) IBOutlet UIDatePicker *alertDatePicker;
@property (strong, nonatomic) IBOutlet UITableView *alertTableView;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UIView *startDueDateView_Main;
@property (strong, nonatomic) IBOutlet UIView *startDueDateView;

@property (strong, nonatomic) UILabel *startDateLabel;
@property (strong, nonatomic) UILabel *endDateLabel;
@property (strong, nonatomic) UISwitch *alertOnOffSwitch;


-(void) tapBlurredView; //when blurred view is tapped
-(void) alertDatePickerValueChanged;
-(void) doneButtonClicked;
-(void) alertSwitchValueChanged;

@end

@implementation AddToDoStartEndViewController_iOS7

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}
*/

-(id)initWithStartDateString:(NSString*) startDateStr endDateStr:(NSString*) endDateStr {
    
    self = [super initWithNibName:@"AddToDoStartEndViewController_iOS7" bundle:[NSBundle mainBundle]];
    if (self) {
		
        _startDateLabel = [[UILabel alloc]init];
        _endDateLabel = [[UILabel alloc] init];
        
        
        if (startDateStr == nil) {
            _startDateLabel.text = @"None";
        }
        else{
            _startDateLabel.text = startDateStr;
        }
        
        
        if (endDateStr == nil) {
            _endDateLabel.text = @"None";
        }
        else{
            _endDateLabel.text = endDateStr;
        }
        
        
    }
    
    //NSLog(@"strtDateString  = %@", strtDateString);
    //NSLog(@"endDateString  = %@", endDateString);
    //NSLog(@"todoVal  = %d", self.todoVal);
    
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    self.navigationController.navigationBarHidden = YES;
    
    _alertOnOffSwitch = [[UISwitch alloc]init];
    [_alertOnOffSwitch setOn:YES animated:NO];
    isShowingStartDate = YES;
    isShowingEndDate = NO;
    
     _alertDatePicker = [[UIDatePicker alloc]init];
    _alertDatePicker.timeZone = [NSTimeZone localTimeZone];
    _alertDatePicker.locale   = [NSLocale currentLocale];
    _alertDatePicker.calendar = [NSCalendar currentCalendar];
    _alertDatePicker.datePickerMode = UIDatePickerModeDate;
    

    
    //********* setup Start & Due Date **********************
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    if ([_startDateLabel.text isEqualToString:@"None"]) {

        NSDate *todayDate = [NSDate date];
        
        _startDateLabel.text = [dateFormatter stringFromDate:todayDate];
        _alertDatePicker.date = todayDate;
        
    }
    else{
        
        NSDate *tempStartDate = [[NSDate alloc]init];
        
        tempStartDate = [dateFormatter dateFromString:_startDateLabel.text];
        _alertDatePicker.date = tempStartDate;
    }
    
    if ([_endDateLabel.text isEqualToString:@"None"]) {
        
        _endDateLabel.text = _startDateLabel.text;
    }
    
    
    
    CGRect  frameSize = [[UIScreen mainScreen] bounds];
    
    //********* Background Image of To Do **********************
    _nonBlurredBackgroundView = [[UIView alloc] initWithFrame:frameSize];
    [self.view addSubview:_nonBlurredBackgroundView];
    
    //ImageView
    UIImage *tempNonBlurredImage = self.nonBlurredImage; //derived image from previous screen shot of To Do
    _nonBlurredBackgroundImageView = [[UIImageView alloc] initWithImage:tempNonBlurredImage];
    
    [_nonBlurredBackgroundView addSubview:_nonBlurredBackgroundImageView];

    
    //********* Blurred Background Image of To Do **********************
    _blurredBackgroundView = [[UIView alloc] initWithFrame:frameSize];
    [self.view addSubview:_blurredBackgroundView];
    
    //Blurred ImageView
    UIImage *tempBlurredImage = self.blurredImage; //derived image from previous screen shot of To Do
    _blurredBackgroundImageView = [[UIImageView alloc] initWithImage:tempBlurredImage];
    
    [_blurredBackgroundView addSubview:_blurredBackgroundImageView];
    
    /*
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBlurredView)];
    _tapGesture.numberOfTapsRequired = 1;
    _tapGesture.numberOfTouchesRequired = 1.0;
    [_blurredBackgroundView addGestureRecognizer:_tapGesture];
    */

    //********* StartDueDateView **********************
    //Adds view on top of blurred image, but uses self.view so tap gesture won't respond
    _startDueDateView = [[UIView alloc]initWithFrame:CGRectMake(28, 75, 265, 350)];
    _startDueDateView.backgroundColor = [UIColor blackColor];
    _startDueDateView.layer.cornerRadius = 5;
    _startDueDateView.layer.shadowColor = [UIColor blackColor].CGColor;
    _startDueDateView.layer.shadowOpacity = 0.5;
    _startDueDateView.layer.shadowRadius = 5.0;
    _startDueDateView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    [self.view addSubview:_startDueDateView];
    
    
    //********* Save Button **********************
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [doneButton addTarget:self action:@selector(doneButtonClicked) forControlEvents:UIControlEventTouchDown];
    [doneButton setTitle:@"Save" forState:UIControlStateNormal];
    doneButton.frame = CGRectMake(235, 80, 51, 29);
    doneButton.backgroundColor = [UIColor clearColor];
    doneButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
    [self.view addSubview:doneButton];
    
    
    //********* Cancel Button **********************
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(tapBlurredView) forControlEvents:UIControlEventTouchDown];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(35, 80, 51, 29);
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
    [self.view addSubview:cancelButton];
    
    
     //********* Alert Tableview **********************
    _alertTableView = [[UITableView alloc]initWithFrame:CGRectMake(28 + 2, 120, 244, 138)];
    _alertTableView.delegate = self;
    _alertTableView.dataSource = self;
    [self.view addSubview:_alertTableView];
    
    //********* Alert Picker BackGround View - used to round edges **********************
    UIView *whiteBackGroundForDatePicker = [[UIView alloc]initWithFrame:CGRectMake(48, 255, 224, 148)];
    whiteBackGroundForDatePicker.backgroundColor = [UIColor whiteColor];
    whiteBackGroundForDatePicker.layer.cornerRadius = 5;
    
    [self.view addSubview:whiteBackGroundForDatePicker];
    
    
    //********* Alert Picker **********************
    _alertDatePicker.frame = CGRectMake(28, 248, 264, 148);
    _alertDatePicker.transform = CGAffineTransformMake(0.85, 0, 0, 0.85, 0, 0); //Change DatePicker size
    _alertDatePicker.backgroundColor = [UIColor clearColor];
    [_alertDatePicker addTarget:self
                      action:@selector(alertDatePickerValueChanged)
                      forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:_alertDatePicker];
    
    
    //************** Set up for Animation  **********************
    _startDueDateView.alpha = 0.0;
    _alertDatePicker.alpha = 0.0;
    _alertTableView.alpha = 0.0;
    _blurredBackgroundView.alpha = 0.0;
    _blurredBackgroundImageView.alpha = 0.0;
    doneButton.alpha = 0.0;
    cancelButton.alpha = 0.0;
    
    
    //************** Fade In Animation  **********************
    [UIView beginAnimations:@"fade in" context:nil];
    
    [UIView setAnimationDuration:0.7];
    
    _startDueDateView.alpha = 0.5;
    _alertDatePicker.alpha = 1.0;
    _alertTableView.alpha = 1.0;
    _blurredBackgroundView.alpha = 1.0;
    _blurredBackgroundImageView.alpha = 1.0;
    doneButton.alpha = 1.0;
    cancelButton.alpha = 1.0;
    
    [UIView commitAnimations];
    
    
    
}


-(void) tapBlurredView{
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void) doneButtonClicked{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    NSDate *tempStartDate = [dateFormatter dateFromString:_startDateLabel.text];
    NSDate *tempDueDate = [dateFormatter dateFromString:_endDateLabel.text];
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSString *tempStartString = [dateFormatter stringFromDate:tempStartDate];
    NSString *tempDueString = [dateFormatter stringFromDate:tempDueDate];
    
    NSComparisonResult result = [tempStartDate compare:tempDueDate];
    
    if (result == NSOrderedDescending) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"The Due date can not be a date before Start date. Please enter a date equal to or greater than Start date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    //Steve - It is nil when changed to @"None"  so TempStartString needs to be changed.
    if (tempStartString == nil || tempDueString == nil) {
        
        tempStartString = @"None";
        tempDueString = @"None";
    }
    
 
    if([self.delegate respondsToSelector:@selector(setStartDate:todoID:)]){
        
        [self.delegate setStartDate:tempStartString todoID:self.todoVal];
    }
    
    if ([self.delegate respondsToSelector:@selector(setEndDate:todoID:)]) {
        
        [self.delegate setEndDate:tempDueString todoID:self.todoVal];
    }
    
    
    
    
    [self.navigationController popViewControllerAnimated:NO];
    
}


-(void) alertDatePickerValueChanged{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];

    
    if (isShowingStartDate) {
        
        NSDate *changedDate = _alertDatePicker.date;
        _startDateLabel.text = [dateFormatter stringFromDate:changedDate];
        _endDateLabel.text = [dateFormatter stringFromDate:changedDate];
        [_alertTableView reloadData];
        
    }
    else if (isShowingEndDate){
        
        NSDate *changedDate = _alertDatePicker.date;
        _endDateLabel.text = [dateFormatter stringFromDate:changedDate];
        [_alertTableView reloadData];
    }
    
}

-(void) alertSwitchValueChanged{
    
    if (_alertOnOffSwitch.isOn) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        
        
        NSDate *todayDate = [NSDate date];
        
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        
        _startDateLabel.text = [dateFormatter stringFromDate:todayDate];
        _endDateLabel.text = _startDateLabel.text;
        _alertDatePicker.date = todayDate;
        
        [_alertTableView reloadData];
        
    }
    else{
        
        _startDateLabel.text = @"None";
        _endDateLabel.text = @"None";
        
        _startDateLabel.textColor = [UIColor whiteColor];
        _endDateLabel.textColor = [UIColor whiteColor];

    }
    
}


#pragma mark -
#pragma mark TableView Methods
#pragma mark -


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

	return 40;
	
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    
    tableView.backgroundColor = [UIColor clearColor];
    tableView.opaque = NO;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
		
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    
    int y = 10;
    int x = 15;
    
    if (indexPath.row == 0) { //Start Date
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        UILabel *startDateFixed = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 85, 25)];
        startDateFixed.textAlignment = NSTextAlignmentLeft;
        startDateFixed.font = [UIFont fontWithName:@"Halvetica-Bold" size:15.0];
        startDateFixed.textColor = [UIColor whiteColor];
        startDateFixed.backgroundColor = [UIColor clearColor];
        startDateFixed.text = @"Start Date";
        [cell.contentView addSubview:startDateFixed];
        
        
        _startDateLabel.frame = CGRectMake(120, y, 120, 25);
        _startDateLabel.textAlignment = NSTextAlignmentRight;
        _startDateLabel.font = [UIFont fontWithName:@"Halvetica" size:15.0];
        _startDateLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:_startDateLabel];
        
        if (isShowingStartDate) {
            
             _startDateLabel.textColor = [UIColor yellowColor];
        }
        else{
            
             _startDateLabel.textColor = [UIColor whiteColor];
        }
        
        
    }
    if (indexPath.row == 1) { //Due Date
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        UILabel *endDateFixed = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 85, 25)];
        endDateFixed.textAlignment = NSTextAlignmentLeft;
        endDateFixed.font = [UIFont fontWithName:@"Halvetica-Bold" size:15.0];
        endDateFixed.textColor = [UIColor whiteColor];
        endDateFixed.backgroundColor = [UIColor clearColor];
        endDateFixed.text = @"Due Date";
        [cell.contentView addSubview:endDateFixed];
        
        
        _endDateLabel.frame = CGRectMake(120, y, 120, 25);
        _endDateLabel.textAlignment = NSTextAlignmentRight;
        _endDateLabel.font = [UIFont fontWithName:@"Halvetica" size:15.0];
        _endDateLabel.textColor = [UIColor whiteColor];
        _endDateLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:_endDateLabel];
        
        
        if (isShowingEndDate) {
            
            _endDateLabel.textColor = [UIColor yellowColor];
        }
        else{
            
            _endDateLabel.textColor = [UIColor whiteColor];
        }


    }
    if (indexPath.row == 2) { //ON/OFF
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        UILabel *onOffFixed = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 120, 25)];
        onOffFixed.textAlignment = NSTextAlignmentLeft;
        onOffFixed.font = [UIFont fontWithName:@"Halvetica-Bold" size:15.0];
        onOffFixed.textColor = [UIColor whiteColor];
        onOffFixed.backgroundColor = [UIColor clearColor];
        onOffFixed.text = @"Turn ON/OFF";
        [cell.contentView addSubview:onOffFixed];
        
        //Switch
        //Init in viewDidLoad
        _alertOnOffSwitch.frame = CGRectMake(190, 4, 51, 29);
        _alertOnOffSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        [_alertOnOffSwitch addTarget:self action:@selector(alertSwitchValueChanged) forControlEvents:UIControlEventValueChanged];
        
        [cell.contentView addSubview:_alertOnOffSwitch];
 
    }
	
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    if (indexPath.row == 0) { //Start Date
        
        isShowingStartDate = YES;
        isShowingEndDate = NO;
        
        NSDate *tempStartDate = [[NSDate alloc]init];
        tempStartDate = [dateFormatter dateFromString:_startDateLabel.text];
        _alertDatePicker.date = tempStartDate;
    
        //_startDateLabel.text = [dateFormatter stringFromDate:_alertDatePicker.date];
        
        [tableView reloadData];
        
    }
    if (indexPath.row == 1) { //End Date
        
        isShowingStartDate = NO;
        isShowingEndDate = YES;
        
        NSDate *tempEndDate = [[NSDate alloc]init];
        tempEndDate = [dateFormatter dateFromString:_endDateLabel.text];
        _alertDatePicker.date = tempEndDate;
        
       // _endDateLabel.text = [dateFormatter stringFromDate:_alertDatePicker.date];
        
        [tableView reloadData];
    }
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

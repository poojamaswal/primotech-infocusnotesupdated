//
//  singleViewDataObject.h
//  Organizer
//
//  Created by Naresh Chauhan on 8/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface singleViewDataObject : NSObject {
	
	NSInteger	notesID;
	NSString	*notesTitle; 
	NSString	*notesTitleType; 
	NSData		*notesTitleBinary; 
	NSData		*notesTitleBinaryLarge;
	

}

@property (nonatomic, assign) NSInteger		notesID;
@property (nonatomic, retain) NSString		*notesTitle;
@property (nonatomic, retain) NSString		*notesTitleType; 
@property (nonatomic, retain) NSData		*notesTitleBinary; 
@property (nonatomic, retain) NSData		*notesTitleBinaryLarge;
@end

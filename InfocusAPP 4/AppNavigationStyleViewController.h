//
//  AppNavigationStyleViewController.h
//  Organizer
//
//  Created by Steven Abrams on 3/4/13.
//
//

#import <UIKit/UIKit.h>
#import "OrganizerAppDelegate.h"

@interface AppNavigationStyleViewController : UITableViewController{
    
    IBOutlet UISwitch   *newStyle;
    IBOutlet UISwitch   *oldStyle;
    
    BOOL                *isNewStyleSelected;
    BOOL                *isOldStyleSelected;
    
    OrganizerAppDelegate *appDelegate;
    
    IBOutlet UIView       *exitAppView;
}

@end

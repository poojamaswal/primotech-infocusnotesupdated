//
//  ToDoViewController.m
//  Organizer
//
//  Created by Tarun on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ToDoViewController.h"
#import "AddEditStartDateEndDateVC.h"
#import "ProjectListViewController.h"
#import "GetAllDataObjectsClass.h"
#import "ViewTODOScreen.h"
#import "ProgressPickerView.h"
#import "PriorityPickerView.h"
#import "AddToDoAlertViewController.h"
#import "UIImage+ImageEffects.h"//Steve

@interface ToDoViewController(){
    
    BOOL    isStartDateShowing;
    BOOL    isEndDateShowing;
    BOOL    isAlert_1Showing;
    BOOL    isAlert_2Showing;
    BOOL    isProgressShowing;
    BOOL    isPriorityShowing;
    
}
@property (nonatomic, strong) UIImage *screenShotImage; //Steve - screenshot of AppointmentViewController

@property (strong, nonatomic) NSDate            *startDate_Date; //Steve
@property (strong, nonatomic) NSDate            *endDate_Date; //Steve

@property (strong, nonatomic) UIDatePicker      *startDatePicker; //Steve
@property (strong, nonatomic) UIDatePicker      *endDatePicker; //Steve
@property (strong, nonatomic) UISwitch          *startEndDateSwitch; //Steve

@property (strong, nonatomic) UIDatePicker      *alert_1_Picker; //Steve
@property (strong, nonatomic) UIDatePicker      *alert_2_Picker; //Steve
@property (strong, nonatomic) UISwitch          *alertSwitchOnOff; //Steve


@property (strong, nonatomic) NSDate            *alert_1_Date; //Steve - Handle International Dates
@property (strong, nonatomic) NSDate            *alert_2_Date; //Steve - Handle International Dates

@property (strong, nonatomic) NSMutableArray    *progressPickerArray;//Steve - % Complete
@property (strong, nonatomic) UIPickerView      *progressPicker; //Steve

@property (strong, nonatomic) NSArray    *priorityPickerColumn_1Array;//Steve
@property (strong, nonatomic) NSArray    *priorityPickerColumn_2Array;//Steve
@property (strong, nonatomic) UIPickerView      *priorityPicker; //Steve
@property (strong, nonatomic) UISwitch          *prioritySwitch; //Steve


-(void)setLabelValues;
-(void)moveScreenDown;
-(void) startPickerChanged; //Steve
-(void) endPickerChanged; //Steve
-(void) startEndSwitchValueChanged; //Steve
-(void) alert_1_PickerValueChanged; //Steve
-(void) alert_2_PickerValueChanged; //Steve
-(void) alertSwitchOnOffValueChange;//Steve
-(void) prioritySwitchValueChange;//Steve
-(void) animateAlertSectionToTop; //Steve

@end



@implementation ToDoViewController
@synthesize lbl_PercentageSymbol;
@synthesize img_CompletedArrow;
@synthesize img_PriorityArrow;
@synthesize lbl_Completed;
@synthesize lbl_Priority;
@synthesize lbl_Starred;
@synthesize btn_Alert;
@synthesize img_SecondAlertArrow;
@synthesize lbl_SecondAlert;
@synthesize btn_CustomView;
@synthesize btn_SecondAlert;
@synthesize btn_Completed;
@synthesize btn_Priority;
@synthesize btn_Starred;
@synthesize isAdd, isStarred, aToDoObj;
@synthesize todoTitle, todoTitleType;
@synthesize todoTitleBinary, todoTitleBinaryLarge;
@synthesize delegate, projectID, titleTextField,projectNameLabel;
@synthesize myVAL,myVAL1,fullString,prntVAL;

#pragma mark -
#pragma mark ViewController LifeCycle
#pragma mark -

-(id)initWithToDoDataObject:(TODODataObject *) todoObj {
    
#pragma mark -- IPad Primotech
    if(IS_IPAD)
    {
        if (self = [super initWithNibName:@"ToDoViewControllerIPad" bundle:[NSBundle mainBundle]]) {
            
            projectNameLabel = [[UILabel alloc] init];
            
            isAdd = NO;
            self.aToDoObj = todoObj;
        }
    }
    else
    {
        if (self = [super initWithNibName:@"ToDoViewController2" bundle:[NSBundle mainBundle]]) {
            
            projectNameLabel = [[UILabel alloc] init];
            
            isAdd = NO;
            self.aToDoObj = todoObj;
        }
    }
    
    //NSLog(@" self.aToDoObj.todoTitleType) = %@", self.aToDoObj.todoTitleType);
    //NSLog(@" self.aToDoObj.todoTitle = %@", self.aToDoObj.todoTitle);
    // NSLog(@" self.aToDoObj.todoTitle = %@", self.aToDoObj.startDateTime);
    
    return self;
}


//Steve - to bring in Project name when coming from Projects Module
-(id)initWithProjectName:(NSString *) projectName projectTagInt:(int) projectTag isAdd:(BOOL) isAddTodo{
    
#pragma mark -- IPad Primotech
    if(IS_IPAD)
    {
        if (self = [super initWithNibName:@"ToDoViewControllerIPad2" bundle:[NSBundle mainBundle]]) {
            
            isAdd = isAddTodo;
            
            projectNameLabel = [[UILabel alloc]init];
            projectNameLabel.text = projectName;
            projectNameLabel.tag = projectTag;
        }
    }
    else
    {
        
        if (self = [super initWithNibName:@"ToDoViewController2" bundle:[NSBundle mainBundle]]) {
            
            isAdd = isAddTodo;
            
            projectNameLabel = [[UILabel alloc]init];
            projectNameLabel.text = projectName;
            projectNameLabel.tag = projectTag;
            
            
        }
    }
    
    //NSLog(@"projectNameLabel.text  = %@", projectNameLabel.text );
    //NSLog(@"projectNameLabel.tag  = %d", projectNameLabel.tag );
    
    
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        projectNameLabel = [[UILabel alloc] init];
        isAdd = YES;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    //[NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]]; //Steve
    
    //Initialize objects
    titleTextField = [[UITextField alloc] init];
    inputSelecterButton = [[UIButton alloc] init];
    titleTextField.delegate = self;
    
    startsLabel = [[UILabel alloc]init];
    endsLabel = [[UILabel alloc]init];
    
    projectFixedLabel = [[UILabel alloc]init];
    
    startFixedLabel = [[UILabel alloc] init];
    dueDateFixedLabel = [[UILabel alloc] init];
    lblStartDate = [[UILabel alloc] init];
    lblEndDate = [[UILabel alloc] init];
    
    alertFixedLabel = [[UILabel alloc] init];
    lbl_SecondAlert = [[UILabel alloc] init];
    alertsLabel = [[UILabel alloc] init];
    secondAlertsLabel = [[UILabel alloc] init];
    
    lbl_Completed = [[UILabel alloc] init];
    lbl_PercentageSymbol = [[UILabel alloc] init];
    progressLabel = [[UILabel alloc] init];
    
    lbl_Priority = [[UILabel alloc] init];
    priorityLabel = [[UILabel alloc] init];
    
    lbl_Starred = [[UILabel alloc] init];
    starredImgView = [[UIImageView alloc]init];
    
    
    deleteButton = [[UIButton alloc]init];
    
    
    self.startDate_Date = [[NSDate alloc]init];
    self.endDate_Date = [[NSDate alloc]init];
    
    self.startDatePicker = [[UIDatePicker alloc]init];
    self.endDatePicker = [[UIDatePicker alloc]init];
    self.startDatePicker.hidden = YES;
    self.endDatePicker.hidden = YES;
    self.startEndDateSwitch = [[UISwitch alloc]init];
    
    self.alert_1_Picker = [[UIDatePicker alloc]init];
    self.alert_2_Picker = [[UIDatePicker alloc]init];
    self.alert_1_Picker.hidden = YES;
    self.alert_2_Picker.hidden = YES;
    self.alertSwitchOnOff = [[UISwitch alloc]init];
    
    
    self.alert_1_Date = [[NSDate alloc]init];
    self.alert_2_Date = [[NSDate alloc]init];
    
    self.progressPickerArray = [[NSMutableArray alloc]init];
    self.progressPicker = [[UIPickerView alloc]init];
    self.progressPicker.hidden = YES;
    
    self.priorityPicker = [[UIPickerView alloc]init];
    self.priorityPicker.hidden = YES;
    self.prioritySwitch = [[UISwitch alloc]init];
    
    self.progressPicker.delegate = self;
    self.progressPicker.dataSource = self;
    
    self.priorityPicker.delegate = self;
    self.priorityPicker.dataSource = self;
    
    
    
    //setup values for Progress Picker
    for (int i = 0; i <= 100; i = i+5)
    {
        NSString *progressString = [NSString stringWithFormat:@"%d", i];
        [self.progressPickerArray addObject:progressString];
    }
    
    
    //For Priority Picker
    self.priorityPickerColumn_1Array = [[NSArray alloc] initWithObjects:@"None",@"A",@"B",@"C",nil];
    self.priorityPickerColumn_2Array = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    
    
    
    if(isAdd)
    {
        updateButton.hidden = YES;
        deleteButton.hidden = YES;
        saveButton.hidden	= NO;
    }
    else
    {
        updateButton.hidden = NO;
        deleteButton.hidden = NO;
        saveButton.hidden	= YES;
    }
    
    
    
    if (aToDoObj && isAdd == NO)
    {
        if([aToDoObj.todoTitleType isEqualToString:@"H"] && aToDoObj.todoTitleBinary!= nil)
        {
            [titleTextField setPlaceholder: @""];
            UIImage *tempTitleImage = [[UIImage alloc] initWithData:aToDoObj.todoTitleBinary];
            [titleTextField setBackground:tempTitleImage];
            if ([objHand.lineCount.text intValue] > 1)
            {
                UILabel *dotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleImgView.frame.size.width, titleImgView.frame.size.height)];
                dotLabel.text = @"...";
                [dotLabel setTextColor:[UIColor blackColor]];
                //	[titleTextField addSubview:dotLabel];
            }
            
            self.todoTitleBinary = aToDoObj.todoTitleBinary;
            self.todoTitleBinaryLarge = aToDoObj.todoTitleBinaryLarge;
            self.todoTitleType = aToDoObj.todoTitleType;
            
            self.todoTitle = aToDoObj.todoTitle;
            
            [titleTextField setText:@""];
            [titleTextField resignFirstResponder];
        }
        else if ([aToDoObj.todoTitleType isEqualToString:@"T"] || [aToDoObj.todoTitleType isEqualToString:@"V"])
        {
            if (aToDoObj.todoTitle && (![aToDoObj.todoTitle isEqualToString:@""]))
            {
                [titleTextField setText:aToDoObj.todoTitle];
                [titleTextField setTextColor:[UIColor blueColor]];
                [titleTextField resignFirstResponder];
                titleTextField.layer.borderWidth = 0.0;
            }
            else
            {
                [titleTextField setPlaceholder: @"Title"];
                todoTitleType = @"T";
                todoTitleBinary = nil;
            }
            
        }
        
        
        //Steve - Segue doesn't initiate the first 3 methods
        if (IS_IPAD) {
            projectNameLabel = [[UILabel alloc] init];
        }
        
        
        if([aToDoObj projID] == 0)
        {
            projectNameLabel.text  = @"None";
            projectNameLabel.tag  = 0;
        }
        else
        {
            //Naresh's Commented
            //projectNameLabel.text = ([aToDoObj projectName] == nil || [[aToDoObj projectName] isEqualToString:@""]) ? @"Project Name" :  [aToDoObj fullprojPath];
            projectNameLabel.text = ([aToDoObj projectName] == nil || [[aToDoObj projectName] isEqualToString:@""]) ? @"Project Name" :  [aToDoObj fullprojPath];
            projectNameLabel.tag  = [aToDoObj projID];
        }
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        ///////// *********** Steve - Start & Due Date ************* ////////////////
        
        if ( [[aToDoObj startDateTime] isEqual:@"None"] || [[aToDoObj dueDateTime] isEqual:@"None"]) {
            
            if ( [[aToDoObj startDateTime] isEqual:@"None"] )
                [startsLabel setText:lblStartDate.text];
            
            if ( [[aToDoObj dueDateTime] isEqual:@"None"] )
                [endsLabel setText:lblEndDate.text];
            
            self.startEndDateSwitch.on = NO;
            
        }
        else{ //Start & Due is Not "None"
            
            self.startEndDateSwitch.on = YES;
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            //Steve
            self.startDate_Date = [dateFormatter dateFromString:[aToDoObj startDateTime]];
            self.endDate_Date = [dateFormatter dateFromString:[aToDoObj dueDateTime]];
            
            //NSLog(@"self.alert_1_Date  %@", [self.alert_1_Date descriptionWithLocale:@"GMT"]);
            //NSLog(@"self.alert_2_Date  %@", [self.alert_2_Date descriptionWithLocale:@"GMT"]);
            
            
            //Steve set picker
            self.startDatePicker.date = self.startDate_Date;
            self.endDatePicker.date = self.endDate_Date;
            
            NSDate *strtDate =[dateFormatter dateFromString:[aToDoObj startDateTime]];
            NSDate *endDate=[dateFormatter dateFromString:[aToDoObj dueDateTime]];
            
            //Steve - changed to be International compatible
            //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            [dateFormatter setLocale:[NSLocale currentLocale]];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            
            
            NSString* strStrtDate = [dateFormatter stringFromDate:strtDate];
            NSString* strEndDate = [dateFormatter stringFromDate:endDate];
            
            startsLabel.text = strStrtDate;
            endsLabel.text = strEndDate;
            
            //Steve updated substringToIndex:11 instead of 12
            lblStartDate.text = [strStrtDate substringToIndex:11];
            lblEndDate.text = [strEndDate substringToIndex:11];
        }
        
        
        
        //NSLog(@"lblStartDate.text = %@", lblStartDate.text);
        //NSLog(@"lblEndDate.text = %@", lblEndDate.text);
        
        //NSLog(@"startsLabel.text = %@", startsLabel.text);
        //NSLog(@"endsLabel.text) = %@", endsLabel.text);
        
        
        
        
        if (aToDoObj.isStarred == YES) {
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred.png"]];
        }else {
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred-a.png"]];
        }
        
        
        if (![aToDoObj.priority isEqualToString:@""] && aToDoObj.priority) {
            priorityChar = [aToDoObj.priority substringToIndex:1];
            priorityNum = [aToDoObj.priority substringFromIndex:1];
            priorityLabel.text = aToDoObj.priority;
            
            /////////// Steve - sets the Priority Picker ///////////////
            NSInteger selectedRow = [priorityNum integerValue] - 1; //finds location of row of second component
            
            if ([priorityChar isEqualToString:@"None"]) {
                
                self.prioritySwitch.on = NO;
                [self.priorityPicker selectRow:0 inComponent:0 animated:NO];
                [self.priorityPicker selectRow:0 inComponent:1 animated:NO];
            }
            else if ([priorityChar isEqualToString:@"A"]) {
                
                self.prioritySwitch.on = YES;
                [self.priorityPicker selectRow:1 inComponent:0 animated:NO];
                [self.priorityPicker selectRow:selectedRow inComponent:1 animated:NO];
            }
            else if ([priorityChar isEqualToString:@"B"]){
                
                self.prioritySwitch.on = YES;
                [self.priorityPicker selectRow:2 inComponent:0 animated:NO];
                [self.priorityPicker selectRow:selectedRow inComponent:1 animated:NO];
            }
            else if ([priorityChar isEqualToString:@"C"]){
                
                self.prioritySwitch.on = YES;
                [self.priorityPicker selectRow:3 inComponent:0 animated:NO];
                [self.priorityPicker selectRow:selectedRow inComponent:1 animated:NO];
            }
            
        }else {
            priorityChar = @"A";
            priorityNum = @"1";
            priorityLabel.text = @"None";
        }
        
        
        if (aToDoObj.toDoProgress != 0 && aToDoObj.toDoProgress) {
            progressLabel.text = [NSString stringWithFormat:@"%d",aToDoObj.toDoProgress];
            intProgress = aToDoObj.toDoProgress;
            
            //Steve - Set the Progress Picker - finds row by dividing by 5.  Progress is in increments of 5
            NSInteger row = intProgress/5;
            [self.progressPicker selectRow:row inComponent:0 animated:NO];
            
        }else {
            progressLabel.text = @"";
        }
        
        
        if (aToDoObj.isStarred) {
            isStarred = YES;
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred.png"]];
        }else {
            isStarred = NO;
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred-a.png"]];
        }
        
        
        
        //////////// set Alert Picker Values & Alert switch /////////////////////
        
        
        ///////////////////// ************** Alert 1 check ************************ //////////////////////////////////////
        //Steve - doesn't set picker if alert_1_Date is nil. Tries to fix it here.  Old programmers entered it in SQL as USA date format
        //From now on it will be saved in correct format to prevent this problem in future with international formats
        
        //Alert 1
        if ([aToDoObj.alert isEqualToString:@"None"]) {
            
            self.alertSwitchOnOff.on = NO;
        }
        else{
            
            self.alertSwitchOnOff.on = YES;
            
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            self.alert_1_Date = [dateFormatter dateFromString:[aToDoObj alert]];
            
            
            //NSLog(@" aToDoObj.alert = %@", aToDoObj.alert);
            //NSLog(@" self.alert_1_Date = %@", [self.alert_1_Date descriptionWithLocale:@"GMT"]);
            
            
            if (self.alert_1_Date == nil)
            {
                
                //Steve - converts it if US Format. Note "," after yyyy,
                [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
                self.alert_1_Date =[dateFormatter dateFromString:[aToDoObj alert]];
                
                //NSLog(@" self.alert_1_Date = %@", self.alert_1_Date);
                
                
                if (self.alert_1_Date != nil){
                    
                    self.alert_1_Picker.date = self.alert_1_Date;
                    
                    [dateFormatter setLocale:[NSLocale currentLocale]];
                    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                    
                    alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date];
                    
                    
                }
                else { //Another attempt at fixing it
                    
                    //Steve - converts it if US Format. Note NO "," after yyyy
                    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
                    self.alert_1_Date =[dateFormatter dateFromString:[aToDoObj alert]];
                    
                    
                    //NSLog(@" self.alert_1_Date = %@", self.alert_1_Date);
                    
                    
                    if (self.alert_1_Date != nil){
                        
                        self.alert_1_Picker.date = self.alert_1_Date;
                        
                        [dateFormatter setLocale:[NSLocale currentLocale]];
                        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                        
                        alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date];
                    }
                    else { //Not Fixed so change to StartDate_Date
                        
                        self.alert_1_Picker.date = self.startDate_Date;
                        self.alert_1_Date = self.startDate_Date;
                        
                        [dateFormatter setLocale:[NSLocale currentLocale]];
                        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                        
                        alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date];
                    }
                    
                    
                }
                
            }
            else{ //alert_1_Date is valid
                
                self.alert_1_Picker.date = self.alert_1_Date;
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                
                alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date];
                alertString = [dateFormatter stringFromDate:self.alert_1_Date];
            }
            
            ///////////////////// ******************* End ******************* //////////////////////////////////////
            
            
        }
        
        
        ///////////////////// ************** Alert 2 check ************************ //////////////////////////////////////
        //Steve - doesn't set picker if alert_1_Date is nil. Tries to fix it here.  Old programmers entered it in SQL as USA date format
        //From now on it will be saved in correct format to prevent this problem in future with international formats
        
        
        if ([aToDoObj.alertSecond isEqualToString:@"None"]) {
            
            self.alertSwitchOnOff.on = NO;
        }
        else{
            
            self.alertSwitchOnOff.on = YES;
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            self.alert_2_Date = [dateFormatter dateFromString:[aToDoObj alertSecond]];
            
            
            //NSLog(@" aToDoObj.alert = %@", aToDoObj.alert);
            //NSLog(@" self.alert_1_Date = %@", [self.alert_1_Date descriptionWithLocale:@"GMT"]);
            
            
            if (self.alert_2_Date == nil){
                
                //Steve - converts it if US Format. Note "," after yyyy,
                [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
                self.alert_2_Date =[dateFormatter dateFromString:[aToDoObj alertSecond]];
                
                
                //NSLog(@" self.alert_2_Date = %@", self.alert_2_Date);
                
                
                if (self.alert_2_Date != nil){
                    
                    self.alert_2_Picker.date = self.alert_2_Date;
                    
                    [dateFormatter setLocale:[NSLocale currentLocale]];
                    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                    
                    secondAlertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date];
                    
                }
                else{ //Another attempt at fixing it
                    
                    
                    //Steve - converts it if US Format. Note NO "," after yyyy
                    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
                    self.alert_2_Date =[dateFormatter dateFromString:[aToDoObj alertSecond]];
                    
                    //NSLog(@" self.alert_2_Date = %@", self.alert_2_Date);
                    
                    
                    if (self.alert_2_Date != nil){
                        
                        self.alert_2_Picker.date = self.alert_2_Date;
                        
                        [dateFormatter setLocale:[NSLocale currentLocale]];
                        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                        
                        secondAlertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date];
                        
                    }
                    else { //Not Fixed so change to StartDate_Date
                        
                        self.alert_2_Picker.date = self.startDate_Date;
                        self.alert_2_Date = self.startDate_Date;
                        
                        [dateFormatter setLocale:[NSLocale currentLocale]];
                        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                        
                        secondAlertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date];
                        
                    }
                    
                    
                }
                
            }
            else{ //alert_2_Date is valid
                
                self.alert_2_Picker.date = self.alert_2_Date;
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                
                secondAlertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date];
                
            }
            
        }
        
        ///////////////////// ******************* End ******************* //////////////////////////////////////
        
        
    }
    else // isAdd == YES;
    {
        priorityChar = @"A";
        priorityNum = @"1";
        priorityLabel.text = @"None";
        intProgress = 0;
        progressLabel.text = @"";
        
        
        
        //Shows Priority Picker None
        [self.priorityPicker selectRow:0 inComponent:0 animated:YES];
        
        //Steve added
        [startsLabel setText:lblStartDate.text];
        [endsLabel setText:lblEndDate.text];
        
        
        //Steve moved here.  Don't want to show when edting a to do
        if (IS_IPAD)
        {
            UIBarButtonItem * cancelBarButton=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelBarButtonPressed)];
            cancelBarButton.tintColor=[UIColor whiteColor];
            self.navigationItem.leftBarButtonItem=cancelBarButton;
        }
        
    }
    
    

    
    
}

#pragma mark Cancel Bar Button Method
-(void)cancelBarButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ViewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(!appdelegate.IsVTTDevice || !appdelegate.isVTTDeviceAuthorized)  //Steve changed was: if(!appdelegate.IsVTTDevice || IS_LITE_VERSION)
        [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon_pen.png"] forState:UIControlStateNormal];
    
    //UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    //[navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    bgView.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    titleTextField.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            
            //self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
            self.view.backgroundColor = [UIColor whiteColor];
            self.todoTableView.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            }
            else{ //Dark Theme
                
                if(IS_IPAD)
                {
                    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
                    
                    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                    self.navigationController.navigationBar.translucent = YES;
                    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                    self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    
                }
                else //iPhone
                {
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                    self.navigationController.navigationBar.translucent = YES;
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                }
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBarHidden = NO;
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
            
            
            
            
            if (isAdd) {
                
                self.title =@"Add To Do";
                [self.navigationController.navigationBar addSubview:saveButton];
                
                
                //done Button
                UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                    initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                    target:self
                                                    action:@selector(save_buttonClicked:)];
                doneButton_iOS7.tintColor = [UIColor whiteColor];
                self.navigationItem.rightBarButtonItem = doneButton_iOS7;
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    doneButton_iOS7.tintColor = [UIColor whiteColor];
                }
                else{ //Dark Theme
                    
                    doneButton_iOS7.tintColor = [UIColor whiteColor];
                }
                
                
            }
            else{ //Edit
                
                if ( IS_IPAD) {
                    backButton.hidden = YES;
                }
                else{ //iPhone
                    
                }
                
                self.title =@"Edit To Do";
                [self.navigationController.navigationBar addSubview:updateButton];
                
                
                //done Button
                UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                    initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                    target:self
                                                    action:@selector(update_buttonClicked:)];
                
                self.navigationItem.rightBarButtonItem = doneButton_iOS7;
                
                
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                else{ //Dark Theme
                    
                    doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                doneButton_iOS7.tintColor = [UIColor whiteColor];
            }
            
            
            
        }
        else{ //iOS 6
            
            navBar.hidden = YES;
            backButton.hidden = YES;
            
            self.navigationController.navigationBarHidden = NO;
            
            deleteButton.frame = CGRectMake(39, 418 - 44, 237, 41);
            
            
            if (isAdd) {
                self.title =@"Add To Do";
                [self.navigationController.navigationBar addSubview:saveButton];
            }
            else{
                self.title =@"Edit To Do";
                [self.navigationController.navigationBar addSubview:updateButton];
            }
            
            
            //Back Button
            UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
            UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButtonNewNav addTarget:self action:@selector(backButon_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
            
            [[self navigationItem] setLeftBarButtonItem:barButton];
            
            
            //Done Button
            if (isAdd) {  //Add
                UIImage *saveButtonImage = [UIImage imageNamed:@"Done.png"];
                saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
                [saveButton setBackgroundImage:saveButtonImage forState:UIControlStateNormal];
                [saveButton addTarget:self action:@selector(save_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
                
                [[self navigationItem] setRightBarButtonItem:barButton];
            }
            else{   //Update
                UIImage *updateButtonImage = [UIImage imageNamed:@"Done.png"];
                updateButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
                [updateButton setBackgroundImage:updateButtonImage forState:UIControlStateNormal];
                [updateButton addTarget:self action:@selector(update_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:updateButton];
                
                [[self navigationItem] setRightBarButtonItem:barButton];
            }
            
        }
    }
    else{
        
        self.navigationController.navigationBarHidden = YES;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    
    
    /*
     NSLog(@"***************************************************");
     NSLog(@"UIScreen.mainScreen.bounds  = %@", NSStringFromCGRect(UIScreen.mainScreen.bounds));
     NSLog(@"UIScreen.mainScreen.applicationFrame  = %@", NSStringFromCGRect(UIScreen.mainScreen.applicationFrame));
     NSLog(@"self.view.frame = %@",NSStringFromCGRect(self.view.frame));
     NSLog(@"  self.navigationController.view.bounds = %@",NSStringFromCGRect(self.navigationController.view.bounds));
     NSLog(@"  self.navigationController.view.frame = %@",NSStringFromCGRect(self.navigationController.view.frame));
     NSLog(@"bgView.frame  = %@", NSStringFromCGRect(bgView.frame));
     NSLog(@"bgView.bounds  = %@", NSStringFromCGRect(bgView.bounds));
     NSLog(@"navBar.frame  = %@", NSStringFromCGRect(navBar.frame));
     NSLog(@"***************************************************");
     */
    
    
    
    [self setLabelValues];
    
    [self.view bringSubviewToFront:titleTextField];
    
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    
    //Steve
    if (IS_IPAD && self.isHandwritingShowing) {
        
        self.todoTableView.userInteractionEnabled = NO;
        //HandWritingViewController
        HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController_iPad"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
        handWritingViewController.handwritingForiPadDelegate = self;
        
        NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
        [stringList setValue:@"Calender" forKey:@"Viewcontroller"];
        [stringList synchronize];
        
        [self addChildViewController:handWritingViewController];
        [self.view addSubview:handWritingViewController.view];
        [self.view bringSubviewToFront:handWritingViewController.view];
        handWritingViewController.view.hidden = YES;
        
        //animates the handWritingViewController.view
        [self performSelector:@selector(fadeInHandwritingView:) withObject:handWritingViewController.view afterDelay:0.0];
        
        
        //Steve - adds the blurr image of the screehshot
        int blurredViewWidth = [[UIScreen mainScreen] bounds].size.width;
        int blurredViewHeight = [[UIScreen mainScreen] bounds].size.height - 320;
        
        UIImage *image = [self captureScreenShotAndblurImage];
        self.blurredImageView = [[UIImageView alloc]initWithImage:image];
        self.blurredImageView.frame = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight); //Change frame size so it doesn't cover HandwritingView
        [self.view addSubview:self.blurredImageView];
        self.blurredImageView.hidden = YES;
        
        //Animates self.blurredImageView
        [self performSelector:@selector(fadeInBlurredImage) withObject:nil afterDelay:0.0];
        
        //UIImageView *testImageView = self.blurredImageView;
    }
}



#pragma mark -
#pragma mark Picker Values Changed
#pragma mark -


-(void) startPickerChanged{ //startDate Picker changed value
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    //[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *newStartDate_Date = [[NSDate alloc]init];
    newStartDate_Date = self.startDatePicker.date;
    startsLabel.text = [formatter stringFromDate:newStartDate_Date];
    endsLabel.text = startsLabel.text;
    
    lblStartDate.text = startsLabel.text;
    lblEndDate.text = startsLabel.text;
    
    self.startDate_Date = newStartDate_Date;
    self.endDate_Date = newStartDate_Date;
    
    [self.todoTableView reloadData];
    
}

-(void) endPickerChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *newEndDate_Date = [[NSDate alloc]init];
    newEndDate_Date = self.endDatePicker.date;
    
    NSDate *newStartDate_Date = [[NSDate alloc]init];
    newStartDate_Date = self.startDatePicker.date;
    
    self.startDate_Date = newStartDate_Date;
    self.endDate_Date = newEndDate_Date;
    
    
    NSComparisonResult result = [newStartDate_Date compare:newEndDate_Date];
    
    if (result == NSOrderedDescending) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Due date can't be a date before Start date. Please enter a date greater than starting date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [titleTextField resignFirstResponder];
        
    }
    else{
        
        endsLabel.text = [formatter stringFromDate:newEndDate_Date];
        lblEndDate.text = endsLabel.text;
        
        [self.todoTableView reloadData];
    }
    
    
}


//Alert 1 Picker Value Changed
-(void) alert_1_PickerValueChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    
    self.alert_1_Date = self.alert_1_Picker.date;
    
    alertsLabel.text = [formatter stringFromDate:self.alert_1_Date];
    
    
    [self.todoTableView reloadData];
    
}


//Alert 2 Picker Value Changed
-(void) alert_2_PickerValueChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    
    self.alert_2_Date = self.alert_2_Picker.date;
    
    secondAlertsLabel.text = [formatter stringFromDate:self.alert_2_Date];
    
    
    [self.todoTableView reloadData];
    
}

-(void) alertSwitchOnOffValueChange{
    
    [titleTextField resignFirstResponder];
    
    
    if (self.alertSwitchOnOff.on) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        alertsLabel.text = [formatter stringFromDate:self.alert_1_Date];
        
        
        isAlert_1Showing = YES;
        isAlert_2Showing = NO;
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
        NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
        NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
        [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
        //Scrolls this section & row to the top for easier viewing
        NSInteger row = 0;
        NSInteger section = 3;
        NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
        [self.todoTableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
        
        
    }
    else{
        
        alertsLabel.text = @"None";
        secondAlertsLabel.text = @"None";
        
        
        if (isAlert_1Showing || isAlert_2Showing) {
            
            isAlert_1Showing = NO;
            isAlert_2Showing = NO;
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
            [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
        }
        else{
            
            isAlert_1Showing = NO;
            isAlert_2Showing = NO;
        }
        
    }
    
    
}


-(void) startEndSwitchValueChanged{
    
    [titleTextField resignFirstResponder];
    
    
    if (self.startEndDateSwitch.on) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        
        startsLabel.text = [formatter stringFromDate:self.startDate_Date];
        endsLabel.text = [formatter stringFromDate:self.endDate_Date];
        
        lblStartDate.text =  startsLabel.text;
        lblEndDate.text = endsLabel.text;
        
        
        isStartDateShowing = YES;
        isEndDateShowing = NO;
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
        NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:2];
        NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:2];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
        [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
        //Scrolls this section & row to the top for easier viewing
        NSInteger row = 0;
        NSInteger section = 2;
        NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
        [self.todoTableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
        
        
    }
    else{
        
        
        lblStartDate.text = @"None";
        lblEndDate.text = @"None";
        
        startsLabel.text = @"None";
        endsLabel.text = @"None";
        
        isStartDateShowing = NO;
        isEndDateShowing = NO;
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
        NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:2];
        NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:2];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
        [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
        //[self.todoTableView reloadData];
    }
    
    
}

-(void) prioritySwitchValueChange{
    
    
    if (self.prioritySwitch.isOn) {
        
        //shows Priority: A 1
        [self.priorityPicker selectRow:1 inComponent:0 animated:YES];
        [self.priorityPicker selectRow:0 inComponent:1 animated:YES];
        
        NSString *column_1String = [self.priorityPickerColumn_1Array objectAtIndex:1];
        NSString *column_2String = [self.priorityPickerColumn_2Array objectAtIndex:0];
        
        priorityLabel.text = [column_1String stringByAppendingString:column_2String];
        
        
        isPriorityShowing = YES;
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:5];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:5];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
        //Scrolls this section & row to the top for easier viewing
        NSInteger row = 0;
        NSInteger section = 5;
        NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
        [self.todoTableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
        
    }
    else{
        
        //Shows Priority: None
        [self.priorityPicker selectRow:0 inComponent:0 animated:YES];
        [self.priorityPicker selectRow:0 inComponent:1 animated:YES];
        
        priorityLabel.text = @"None";
        
        
        isPriorityShowing = NO;
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:5];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:5];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    
}


#pragma mark
#pragma mark pickerView
#pragma mark


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    NSInteger numberOfComponents;
    
    if (pickerView == self.progressPicker) {
        numberOfComponents = 2;
    }
    
    if (pickerView == self.priorityPicker) {
        numberOfComponents = 2;
    }
    
    return numberOfComponents;
}



- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    
    NSInteger numberOfRows;
    
    
    //Progress
    if (pickerView == self.progressPicker) {
        
        if(component==0)
        {
            numberOfRows = [self.progressPickerArray count];
        }
        else
        {
            numberOfRows = 1;
        }
        
    }
    
    
    //Priority
    if (pickerView == self.priorityPicker) {
        
        if (component == 0) {
            
            numberOfRows = [self.priorityPickerColumn_1Array count];
        }
        else{
            
            numberOfRows = [self.priorityPickerColumn_2Array count];
        }
    }
    
    
    return numberOfRows;
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    
    NSString *titleForRowString;
    
    if (pickerView == self.progressPicker) {
        
        if(component==0)
        {
            titleForRowString = [self.progressPickerArray objectAtIndex:row];
            
        }
        else
        {
            NSString *str1 = @"%";
            titleForRowString = str1;
        }
    }
    
    
    if (pickerView == self.priorityPicker) {
        
        if (component == 0) {
            
            titleForRowString = [self.priorityPickerColumn_1Array objectAtIndex:row];
        }
        else{
            
            titleForRowString = [self.priorityPickerColumn_2Array objectAtIndex:row];
        }
        
        
        
    }
    
    return titleForRowString;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    //Progress Picker
    if (pickerView == self.progressPicker) {
        
        progressLabel.text = [self.progressPickerArray objectAtIndex:row];
        
        
        if ([progressLabel.text isEqualToString:@"100"])
        {
            //NSLog(@"HERE");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"To-do's that are 100% complete will be moved to the \"Done\" Tab"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
    }
    
    
    //Priority Picker
    if (pickerView == self.priorityPicker) {
        
        
        NSInteger column_1Picker = [self.priorityPicker selectedRowInComponent:0];
        NSInteger column_2Picker = [self.priorityPicker selectedRowInComponent:1];
        
        NSString *column1  = [self.priorityPickerColumn_1Array objectAtIndex:column_1Picker];
        NSString *column2  = [self.priorityPickerColumn_2Array objectAtIndex:column_2Picker];
        
        
        priorityLabel.text = [column1 stringByAppendingString:column2];
        
        
        //Checks for "None"
        if ([priorityLabel.text length] > 2)
        {
            
            //remove the first 4 characters to avoid things like "None1", should be "None"
            NSMutableString *tempString = [NSMutableString stringWithString: priorityLabel.text];
            NSString *priorityLabel_Remove4Char = [tempString substringWithRange: NSMakeRange (0, 4)];
            
            if ([priorityLabel_Remove4Char isEqualToString:@"None"])
            {
                //if 1st 4 char are "None", then should label is "None". We don't want the priority number
                priorityLabel.text = priorityLabel_Remove4Char;
            }
        }
        
        
        if ([priorityLabel.text isEqualToString: @"None"]) {
            [self.prioritySwitch setOn:NO animated:YES];
        }
        else{
            [self.prioritySwitch setOn:YES animated:YES];
        }
        
    }
    
    
    //NSLog(@"row = %d ", row);
    //NSLog(@"component = %d ", component);
    //NSLog(@"getString = %@ ", getString);
    
}


#pragma mark text field delegagte

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == titleTextField)
    {
        
        self.todoTitleType = @"T";
        
        
        [titleTextField setBackground:nil];
        
        return YES;
    }
    
    return NO;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [titleTextField resignFirstResponder];
    
    
}




-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [titleTextField resignFirstResponder];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [backButton removeFromSuperview];
        
        if (isAdd)
            [saveButton removeFromSuperview];
        else
            [updateButton removeFromSuperview];
        
        backButton.hidden = YES;
        saveButton.hidden = YES;
        updateButton.hidden = YES;
    }
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([todoTitleType isEqualToString:@"H"]) {
        [textField resignFirstResponder];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)setStartDateLabel:(NSString*)aStartDate
{
    startsLabel = [[UILabel alloc] init];
    startsLabel.text = aStartDate;
    
    
    if (![aStartDate isEqual:@"None"]) {
        NSString *temp= [aStartDate substringToIndex:12];
        lblStartDate.text = temp;
    }
    else
        lblStartDate.text = aStartDate;
    
    
}

-(void)setEndDateLabel:(NSString*)aEndDate
{
    endsLabel = [[UILabel alloc]init];
    endsLabel.text = aEndDate;
    
    if (![endsLabel.text isEqual:@"None"]) {
        NSString *temp= [aEndDate substringToIndex:12];
        lblEndDate.text = temp;
    }
    else
        lblEndDate.text = aEndDate;
    
    
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -


-(void)setLabelValues
{
    if ([todoTitleType isEqualToString: @"T"] || [todoTitleType isEqualToString: @"V"]) {
        if (todoTitle && (![todoTitle isEqualToString:@""])) {
            [titleTextField setBackground:nil];
            [titleTextField setText:todoTitle];
            [titleTextField setTextColor:[UIColor blueColor]];
            titleTextField.layer.borderWidth = 0.0;
        }
        //[titleTextField becomeFirstResponder];
    }else if ([todoTitleType isEqualToString: @"H"] && todoTitleBinary) {
        [titleTextField setPlaceholder: @""];
        UIImage *tempTitleImage = [[UIImage alloc] initWithData:todoTitleBinary];
        [titleTextField setBackground:tempTitleImage];
        [titleTextField setText:@""];
        [titleTextField resignFirstResponder];
        [titleTextField setUserInteractionEnabled:YES];
    }else if (([todoTitleType isEqualToString: @""] || todoTitleType == nil)){
        [titleTextField setPlaceholder: @"Title"];
        todoTitleType = @"T";
        todoTitleBinary = nil;
    }
}


-(void)setAlertString:(NSString *) alert {
    alertsLabel.text =  alert;
    [alertsLabel setTextColor:[UIColor grayColor]];
}

-(void)setSecondAlertString:(NSString *)  alert
{
    secondAlertsLabel.text =  alert;
    [secondAlertsLabel setTextColor:[UIColor grayColor]];
}

-(void)setIsAlertOn:(BOOL)_isAlertOn
{
    isAlertOn = _isAlertOn;
}

-(void) setAlertDate:(NSDate *) alertDate{ //Steve
    
    self.alert_1_Date = alertDate;
}

-(void) setAlertSecondDate:(NSDate *) alertSecondDate{ //Steve
    
    self.alert_2_Date = alertSecondDate;
}


#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -

-(IBAction)backButon_Clicked:(id)sender
{
    if(IS_IPAD)
    {
        self.navigationController.navigationBarHidden = YES;
    }else
    {
        self.navigationController.navigationBarHidden = NO;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) inputTypeSelectorButton_Clicked:(id)sender
{
    [titleTextField resignFirstResponder];
    
    if (inputTypeSelectorViewController) {
        inputTypeSelectorViewController = nil;
    }
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appdelegate.IsVTTDevice && appdelegate.isVTTDeviceAuthorized){
        inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 97, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(270, 85) andParentVC:self fromMainScreen:NO];
        
        [inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:inputTypeSelectorViewController.view];
        [self.view bringSubviewToFront:inputTypeSelectorViewController.view];
    }
    else
    {
        if(IS_IPAD)
        {

            [self handwritingView];
        }
        else
        {
            HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
            
            [self.navigationController pushViewController:handWritingViewController animated:NO];
        }
    }
}


- (IBAction)alerts_buttonClicked:(id)sender {
    
    
    //NSLog(@"startsLabel.text = %@", startsLabel.text);
    
    NSString *alertDateString;
    
    //Steve added - need now that start date can be "None", must set a date for Today to send to AlertViewController
    if ([startsLabel.text isEqualToString:@"None"] || startsLabel.text == nil) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        NSDate *todayDate = [NSDate date];
        alertDateString = [dateFormatter stringFromDate:todayDate];
        
    }
    else{
        alertDateString = startsLabel.text;
    }
    
    
    
    AddToDoAlertViewController *addToDoAlertViewController = [[AddToDoAlertViewController alloc] initWithAlertDate:self.alert_1_Date alertSecondDate:self.alert_2_Date]; //Steve change to date
    [addToDoAlertViewController setAlertString:alertsLabel.text];
    [addToDoAlertViewController setAlertStringSecond:secondAlertsLabel.text];
    
    [addToDoAlertViewController setAlertDelegate:self];
    [self.navigationController pushViewController:addToDoAlertViewController animated:YES];
}



- (IBAction)alertsecond_buttonClicked:(id)sender {
    
    
    AddToDoAlertViewController *addToDoAlertViewController = [[AddToDoAlertViewController alloc] initWithAlertDate:self.alert_1_Date alertSecondDate:self.alert_2_Date]; //Steve change to date
    
    
    if(![alertsLabel.text isEqualToString:@"None"])
    {
        [addToDoAlertViewController setAlertString:alertsLabel.text];
        
    }
    else
    {
        [addToDoAlertViewController setAlertString:startsLabel.text];
        
    }
    [addToDoAlertViewController setAlertStringSecond:secondAlertsLabel.text];
    
    
    
    [addToDoAlertViewController setAlertStringSecond:secondAlertsLabel.text];
    [addToDoAlertViewController setDelegate:self];
    [self.navigationController pushViewController:addToDoAlertViewController animated:YES];
}


//Steve (Protocol)
-(void) setAlertString_1:(NSString*) alertString_1 setAlertString_2:(NSString *) alertString_2 isAlertOn:(BOOL) isAlertOnOrOff{
    
    alertsLabel.text = alertString_1;
    secondAlertsLabel.text = alertString_2;
    isAlertOn = isAlertOnOrOff;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [titleTextField resignFirstResponder];
}


-(IBAction)startsEnds_buttonClicked:(id)sender
{
    //NSLog(@"startsLabel.text =  %@", startsLabel.text);
    //NSLog(@"lblStartDate.text =  %@", lblStartDate.text);
    
    
    AddEditStartDateEndDateVC *addEditStartDateEndDateVC= [[AddEditStartDateEndDateVC alloc] initWithStartDateString:startsLabel.text == nil ? @"" : startsLabel.text endDateStr:endsLabel.text == nil ? @"" : endsLabel.text isAllDay:NO];
    
    
    [addEditStartDateEndDateVC setDelegate:self];
    [addEditStartDateEndDateVC setCallerVC:@"ToDoViewController"];
    [self.navigationController pushViewController:addEditStartDateEndDateVC animated:YES];
}

-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId prentID:(NSInteger)prentID
{
    projectNameLabel.text = projName;
    projectNameLabel.tag = projId;
    prntVAL=prentID;
}

-(IBAction)project_buttonClicked:(id)sender{
    
    [titleTextField resignFirstResponder];
    ProjectListViewController *projectListViewController = [[ProjectListViewController alloc] initWithNibName:@"ProjectListViewController" bundle:[NSBundle mainBundle] withProjectID:projectNameLabel.tag];
    [projectListViewController setDelegate:self];
    [self.navigationController pushViewController:projectListViewController animated:YES];
}

-(IBAction)priority_buttonClicked:(id)sender
{
    PriorityPickerView *selectviewP = [[PriorityPickerView alloc]initWithNibName:@"PriorityPickerView" bundle:[NSBundle mainBundle]setNewPriority:[NSString stringWithFormat:@"%@", priorityLabel.text]];
    [self.navigationController pushViewController:selectviewP animated:YES];
    [selectviewP setDelegate:self];
}

-(IBAction)	progress_buttonClicked:(id)sender
{
    ProgressPickerView *selectview = [[ProgressPickerView alloc] initWithNibName:@"ProgressPickerView" bundle:[NSBundle mainBundle]withProgressVAL:[NSString stringWithFormat:@"%@", progressLabel.text]];
    [self.navigationController pushViewController:selectview animated:YES];
    [selectview setDelegate:self];
}

-(void)setProgressVAL:(NSString*)progress
{
    
    progressLabel.text=progress;
}


-(void)setPriorityNew:(NSString*)priority1 priority2:(NSString*)priority2;
{
    
    priorityLabel.text=[priority2 stringByAppendingString:priority1];
    
}


-(IBAction)	starred_buttonClicked:(id)sender
{
    if (isStarred==YES)
    {
        isStarred = NO;
        [starredImgView setImage:[UIImage imageNamed:@"IconStarred-a.png"]];
    }
    else
    {
        isStarred = YES;
        [starredImgView setImage:[UIImage imageNamed:@"IconStarred.png"]];
    }
}


-(IBAction) save_buttonClicked:(id)sender {
    
    if ([todoTitleType isEqualToString:@"H"]) {
        if (todoTitleBinary == nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }else {
        if (titleTextField.text == nil || [titleTextField.text isEqualToString:@""] || [titleTextField.text isEqualToString:@"Title"] ||([[titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]<=0)) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        if (todoTitleType == nil || [todoTitleType isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    
    
    //Steve - checks to see if startsLabel and end are alloc yet
    if (startsLabel.text == nil) {
        startsLabel = [[UILabel alloc]init];
        startsLabel.text = lblStartDate.text;
    }
    
    
    if (endsLabel.text == nil) {
        endsLabel = [[UILabel alloc]init];
        endsLabel.text = lblEndDate.text;
    }
    
    
    
    
    TODODataObject *todoObj = [[TODODataObject alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Steve changed so can add "None" as an option
    if ( [startsLabel.text isEqual:@"None"] || [endsLabel.text isEqual:@"None"] ) {
        
        if ([startsLabel.text isEqual:@"None"]) {
            [todoObj setStartDateTime:startsLabel.text];
        }
        
        if ( [endsLabel.text isEqual:@"None"] ) {
            [todoObj setDueDateTime:endsLabel.text];
        }
        
        
        
        NSLog(@"todoObj.startDateTime =  %@", todoObj.startDateTime);
        
        
    }
    
    else{
        
        
        
        if (IS_IOS_7) {
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            //Steve changed
            NSString* strStrtDate = [dateFormatter stringFromDate:self.startDate_Date];
            NSString* strEndDate = [dateFormatter stringFromDate:self.endDate_Date];
            
            [todoObj setStartDateTime:strStrtDate];
            [todoObj setDueDateTime:strEndDate];
        }
        else{ //Steve - OS 6
            
            [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
            //NSLog(@"***startsLabel.text==>%@",startsLabel.text);
            //NSLog(@"***endsLabel.text==>%@",endsLabel.text);
            
            
            NSDate *startDate = [dateFormatter dateFromString:startsLabel.text];
            NSDate *endDate = [dateFormatter dateFromString:endsLabel.text];
            
            
            //Steve changed
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            NSString* strStrtDate = [dateFormatter stringFromDate:startDate];
            NSString* strEndDate = [dateFormatter stringFromDate:endDate];
            
            
            if (strStrtDate == nil) { //Converts date another way with comma after year
                
                [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
                
                //NSLog(@"***startsLabel.text==>%@",startsLabel.text);
                // NSLog(@"***endsLabel.text==>%@",endsLabel.text);
                
                
                startDate = [dateFormatter dateFromString:startsLabel.text];
                endDate = [dateFormatter dateFromString:endsLabel.text];
                
                
                //Steve changed
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                strStrtDate = [dateFormatter stringFromDate:startDate];
                strEndDate = [dateFormatter stringFromDate:endDate];
            }
            
            
            //NSLog(@"***strStrtDate==>%@",strStrtDate);
            //NSLog(@"***strEndDate==>%@",strEndDate);
            
            [todoObj setStartDateTime:strStrtDate];
            [todoObj setDueDateTime:strEndDate];
            
        }
        
        
        
    }
    
    
    //NSDate *createDate = [NSDate date];
    //	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    //
    //    NSLog(@"%@",createDate);
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDate *createDate  = destinationDate;
    
    //NSLog(@"createDate = %@",createDate);
    
    
    [todoObj setCreateDateTime:[dateFormatter stringFromDate:createDate]];
    
    //NSLog(@"todoObj.createDateTime = %@",todoObj.createDateTime);
    
    [todoObj setProjPrntID:prntVAL];
    [todoObj setProjID:projectNameLabel.tag];
    [todoObj setToDoProgress:[progressLabel.text intValue]];
    [todoObj setPriority:[priorityLabel.text isEqualToString:@"None"] ? @"": priorityLabel.text];
    [todoObj setIsStarred:isStarred];
    
    if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil) {
        [todoObj setTodoTitle:@""];
        [todoObj setTodoTitleType:todoTitleType];
        [todoObj setTodoTitleBinary:todoTitleBinary];
        [todoObj setTodoTitleBinaryLarge:todoTitleBinaryLarge];
    } else {
        [todoObj setTodoTitle:[titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [todoObj setTodoTitleType:todoTitleType];
        NSString *dataStr = @"";
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        [todoObj setTodoTitleBinary:data];
        [todoObj setTodoTitleBinaryLarge:data];
    }
    
    
    NSDateFormatter *newdateFormatter = [[NSDateFormatter alloc] init];
    
    NSDate *ModDate  = [NSDate date];
    [newdateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    [todoObj setModifyDateTime:[newdateFormatter stringFromDate:ModDate]];
    
    
    
    // Anil's Addition
    // Logic to add Alert, second Alert and IsAlerton
    
    if (![alertsLabel.text isEqualToString:@"None"])
    {
        //[todoObj setAlert:alertsLabel.text];
        
        NSString *alertString = [newdateFormatter stringFromDate:self.alert_1_Date];
        [todoObj setAlert:alertString];
    }
    else
    {
        [todoObj setAlert:@"None"];
        
    }
    
    if (![secondAlertsLabel.text isEqualToString:@"None"])
    {
        //[todoObj setAlertSecond:secondAlertsLabel.text];
        
        NSString *alertString = [newdateFormatter stringFromDate:self.alert_2_Date];
        [todoObj setAlertSecond:alertString];
    }
    else
    {
        [todoObj setAlertSecond:@"None"];
        
    }
    
    [todoObj setIsAlertOn:isAlertOn];
    
    
    
    //NSLog(@"alertsLabel.text =  %@", alertsLabel.text);
    
    
    
    //////////////////////////////////////////////////////////
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    BOOL isInserted = [AllInsertDataMethods insertToDoDataValues:todoObj];
    
    if (isInserted == YES) {
        
        GetappID =  sqlite3_last_insert_rowid(appDelegate.database);
        
        
        notification = [[UILocalNotification alloc] init];
        
        ////////////////////  Setting first Alert //////////////////////
        
        if (![alertsLabel.text isEqualToString:@"None"])
            
        {
            notification.timeZone		= [NSTimeZone defaultTimeZone];
            
            if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil)
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [todoObj todoTitle];
                
            }
            
            
            //notification.alertBody		= [todoObj todoTitle];
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            notification.fireDate = self.alert_1_Date;
            
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", GetappID] forKey:@"toDoID"];
            //NSLog(@"UserDict->%@",userDict);
            notification.userInfo = userDict;
            //NSLog(@"ALERT 1->%@->%@ - %@",[todoObj alert],notification.alertBody,notification.fireDate);
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            //////////////////////////////////////////////////////////////
            
        }
        
        if (![secondAlertsLabel.text isEqualToString:@"None"])
            
        {
            notification.timeZone		= [NSTimeZone defaultTimeZone];
            
            if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil)
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [todoObj todoTitle];
                
            }
            
            
            // notification.alertBody		= [todoObj todoTitle];
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            notification.fireDate = self.alert_2_Date;
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", GetappID] forKey:@"toDoID"];
            //NSLog(@"%@",userDict);
            notification.userInfo = userDict;
            //NSLog(@"ALERT 2->%@ - %@",notification.alertBody,notification.fireDate);
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            //////////////////////////////////////////////////////////////
            
        }
        
        
        if (IS_IPAD)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }else{
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
        
        
        
    }
}


-(IBAction) update_buttonClicked:(id)sender {
    
    
    if ([todoTitleType isEqualToString:@"H"]) {
        if (todoTitleBinary == nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }else {
        if (titleTextField.text == nil || [titleTextField.text isEqualToString:@""] || [titleTextField.text isEqualToString:@"Title"] ||([[titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]<=0)) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        if (todoTitleType == nil || [todoTitleType isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    if (([startsLabel.text isEqualToString:@""] || [startsLabel.text isEqualToString:@"Starts"]) || ([endsLabel.text isEqualToString:@""] || [endsLabel.text isEqualToString:@"Ends"]))
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill start/end date to save a To-Do." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    //Steve changed to handle "None"
    TODODataObject *todoObj = [[TODODataObject alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //NSLog(@"startsLabel.text = %@",startsLabel.text);
    //NSLog(@"endsLabel.text = %@", endsLabel.text);
    
    
    
    if ( [startsLabel.text isEqual:@"None"] || [endsLabel.text isEqual:@"None"] || startsLabel.text == nil || endsLabel.text == nil) {
        
        if ([startsLabel.text isEqual:@"None"]) {
            [todoObj setStartDateTime:startsLabel.text];
        }
        else if (startsLabel.text == nil){
            [todoObj setStartDateTime:@"None"];
        }
        
        
        
        if ( [endsLabel.text isEqual:@"None"] ) {
            [todoObj setDueDateTime:endsLabel.text];
        }
        else if (endsLabel.text == nil){
            [todoObj setDueDateTime:@"None"];
        }
        
        
    }
    else{
        
        
        if (IS_IOS_7) {
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            //Steve changed
            NSString* strStrtDate = [dateFormatter stringFromDate:self.startDate_Date];
            NSString* strEndDate = [dateFormatter stringFromDate:self.endDate_Date];
            
            [todoObj setStartDateTime:strStrtDate];
            [todoObj setDueDateTime:strEndDate];
        }
        else{ //Steve - OS 6
            
            [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
            //NSLog(@"***startsLabel.text==>%@",startsLabel.text);
            //NSLog(@"***endsLabel.text==>%@",endsLabel.text);
            
            
            NSDate *startDate = [dateFormatter dateFromString:startsLabel.text];
            NSDate *endDate = [dateFormatter dateFromString:endsLabel.text];
            
            
            //Steve changed
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            NSString* strStrtDate = [dateFormatter stringFromDate:startDate];
            NSString* strEndDate = [dateFormatter stringFromDate:endDate];
            
            
            if (strStrtDate == nil) { //Converts date another way with comma after year
                
                [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
                
                //NSLog(@"***startsLabel.text==>%@",startsLabel.text);
                //NSLog(@"***endsLabel.text==>%@",endsLabel.text);
                
                
                startDate = [dateFormatter dateFromString:startsLabel.text];
                endDate = [dateFormatter dateFromString:endsLabel.text];
                
                
                //Steve changed
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                strStrtDate = [dateFormatter stringFromDate:startDate];
                strEndDate = [dateFormatter stringFromDate:endDate];
            }
            
            
            //******** Finish here with Europe Date format.
            //******** TEST iOS 6 device
            
            
            // NSLog(@"***strStrtDate==>%@",strStrtDate);
            //NSLog(@"***strEndDate==>%@",strEndDate);
            
            [todoObj setStartDateTime:strStrtDate];
            [todoObj setDueDateTime:strEndDate];
            
        }
        
        /*
         [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
         
         NSString* strStrtDate = [dateFormatter stringFromDate:self.startDate_Date];
         NSString* strEndDate = [dateFormatter stringFromDate:self.endDate_Date];
         
         //NSLog(@"strStrtDate = %@",strStrtDate);
         //NSLog(@"strEndDate = %@", strEndDate);
         
         [todoObj setStartDateTime:strStrtDate];
         [todoObj setDueDateTime:strEndDate];
         */
    }
    
    
    // NSLog(@"todoObj.startDateTime = %@", todoObj.startDateTime);
    //NSLog(@"todoObj.dueDateTime = %@", todoObj.dueDateTime);
    
    
    //NSDate *modDate = [NSDate date];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    
    [todoObj setCreateDateTime:aToDoObj.createDateTime];
    //	[todoObj setModifyDateTime:[dateFormatter stringFromDate:modDate]];
    [todoObj setModifyDateTime:aToDoObj.modifyDateTime];
    
    
    [todoObj setProjID:projectNameLabel.tag];
    [todoObj setToDoProgress:[progressLabel.text intValue]];
    [todoObj setPriority:[priorityLabel.text isEqualToString:@"None"] ? @"": priorityLabel.text];
    [todoObj setIsStarred:isStarred];
    
    if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil)
    {
        [todoObj setTodoTitle: @""];
        [todoObj setTodoTitleType:todoTitleType];
        [todoObj setTodoTitleBinary:todoTitleBinary];
        [todoObj setTodoTitleBinaryLarge:todoTitleBinaryLarge];
    } else {
        [todoObj setTodoTitle: [titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        [todoObj setTodoTitleType:todoTitleType];
        NSString *dataStr = @"";
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        [todoObj setTodoTitleBinary:data];
        [todoObj setTodoTitleBinaryLarge:data];
    }
    
    [todoObj setTodoID:aToDoObj.todoID];
    
    // Anil's Addition
    // Logic to add Alert, second Alert and IsAlerton
    
    if (![alertsLabel.text isEqualToString:@"None"])
    {
        //[todoObj setAlert:alertsLabel.text];
        
        NSString *alertString = [dateFormatter stringFromDate:self.alert_1_Date];
        [todoObj setAlert:alertString];
        
    }
    else
    {
        [todoObj setAlert:@"None"];
    }
    
    if (![secondAlertsLabel.text isEqualToString:@"None"])
    {
        //[todoObj setAlertSecond:secondAlertsLabel.text];
        
        NSString *alertString = [dateFormatter stringFromDate:self.alert_2_Date];
        [todoObj setAlertSecond:alertString];
    }
    else
    {
        [todoObj setAlertSecond:@"None"];
        
    }
    
    
    [todoObj setIsAlertOn:isAlertOn];
    
    
    
    //////////////////////////////////////////////////////////
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *notificationArray = [app scheduledLocalNotifications];
    //NSLog(@"notificationArray->%@",notificationArray);
    
    UILocalNotification *row = nil;
    
    for (row in notificationArray) {
        
        NSDictionary *userInfo = row.userInfo;
        NSString *identifier = [userInfo valueForKey:@"toDoID"];
        
        if([identifier intValue]==[todoObj todoID]) {
            //NSLog(@"Found a match!");
            [app cancelLocalNotification:row];
            
        }
    }
    
    
    
    if(!todoObj.projPrntID)[todoObj setProjPrntID:prntVAL];
    //NSLog(@"ProjPrntID 2->%d",todoObj.projPrntID);
    
    //OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    BOOL isInserted = [AllInsertDataMethods updateToDoDataValues:todoObj];
    
    if (isInserted == YES) {
        
        notification = [[UILocalNotification alloc] init];
        
        ////////////////////  Setting first Alert //////////////////////
        
        if (![alertsLabel.text isEqualToString:@"None"])
            
        {
            //NSLog(@"AlertLabel%@",alertsLabel.text);
            notification.timeZone		= [NSTimeZone defaultTimeZone];
            
            if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil)
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [todoObj todoTitle];
                
            }
            
            
            //  notification.alertBody		= [todoObj todoTitle];
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            
            /*
             [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
             NSDate *alertDate =[dateFormatter dateFromString:[todoObj alert]];
             [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
             NSString* strAlert =@"";
             strAlert = [dateFormatter stringFromDate:alertDate];
             //NSLog(@"strAlert%@",strAlert);
             if ([dateFormatter dateFromString:strAlert])
             {
             
             notification.fireDate = [dateFormatter dateFromString:strAlert];
             
             }
             */
            
            notification.fireDate = self.alert_1_Date;
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", [todoObj todoID]] forKey:@"toDoID"];
            //NSLog(@"%@",userDict);
            notification.userInfo = userDict;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            //////////////////////////////////////////////////////////////
            
        }
        
        if (![secondAlertsLabel.text isEqualToString:@"None"])
            
        {
            
            
            notification = nil;
            
            
            notification = [[UILocalNotification alloc] init];
            
            notification.timeZone		= [NSTimeZone defaultTimeZone];
            
            if([todoTitleType isEqualToString:@"H"] && todoTitleBinary != nil)
            {
                notification.alertBody		= @"Handwritten Task";
                
            }
            else
            {
                notification.alertBody		= [todoObj todoTitle];
                
            }
            
            // notification.alertBody		= [todoObj todoTitle];
            notification.alertAction	= @"ToDo";
            notification.soundName		= UILocalNotificationDefaultSoundName;
            notification.applicationIconBadgeNumber++;
            notification.repeatInterval = 0;
            
            
            /*
             [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
             NSDate *alertDate =[dateFormatter dateFromString:[todoObj alertSecond]];
             
             [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
             NSString* strAlert =@"";
             
             strAlert = [dateFormatter stringFromDate:alertDate];
             if ([dateFormatter dateFromString:strAlert])
             {
             
             notification.fireDate = [dateFormatter dateFromString:strAlert];
             
             }
             */
            
            notification.fireDate = self.alert_2_Date;
            
            
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",[todoObj todoID]] forKey:@"toDoID"];
            //NSLog(@"%@",userDict);
            notification.userInfo = userDict;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            //////////////////////////////////////////////////////////////
            
        }
        
        
        
        
        
        
        if (isAdd) {
            
            
            [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
            
            [self.navigationController popViewControllerAnimated:YES];
        }else { //Edit
            
            if (IS_IPAD) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
            else{ //iPhone
                 [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
            }
           
        }
        
        
    }
    
    
    //[[UIApplication sharedApplication] cancelLocalNotification:notification];
    
}

-(IBAction) delete_buttonClicked:(id)sender {
    //NSLog(@"%s",__FUNCTION__);
    if ([AllInsertDataMethods deleteToDoWithID:aToDoObj.todoID]) {
        
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *notificationArray = [app scheduledLocalNotifications];
        
        UILocalNotification *row = nil;
        for (row in notificationArray) {
            NSDictionary *userInfo = row.userInfo;
            NSString *identifier = [userInfo valueForKey:@"toDoID"];
            if([identifier intValue]==aToDoObj.todoID) {
                //NSLog(@"Found a match!");
                [app cancelLocalNotification:row];
            }
        }
        
        if (IS_IPAD) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        else{ //iPhone
            [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
        }
        
        
        
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error in deletion." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark -
#pragma mark TableView Methods
#pragma mark -

// Customize the number of sections in the table view.

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 15; //15
    return 7.0; //5
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 7.0; //5
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections;
    
    if (isAdd)
        sections = 7;
    else
        sections = 8;
    
    
    return sections;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IOS_7) {
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        
        
    }
    else{ //iOS 6
        
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
    //Steve - removes the borders & cell background from the deleteButton cell
    if([cell.reuseIdentifier isEqualToString:@"deleteButtonCell"]){
        cell.backgroundView = nil;
        
    }
    
    
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger rows;
    
    if (section == 0) //Title
        rows = 1;
    else if (section == 1) //Projects
        rows = 1;
    else if (section == 2){ //Start/End
        
        if (IS_IOS_7)
            rows = 5;
        else
            rows = 1;
    }
    
    else if (section == 3) { //Alerts
        
        if (IS_IOS_7)
            rows = 5;
        else
            rows = 2;
    }
    else if (section == 4) { //% Complete
        
        if (IS_IOS_7)
            rows = 2;
        else
            rows = 1;
    }
    else if (section == 5){ //Priority
        
        if (IS_IOS_7)
            rows = 3;
        else
            rows = 1;
    }
    else if ( section == 6) //Starred
        rows = 1;
    else if (section == 7){ //Delete
        
        if (isAdd)
            rows = 0;
        else
            rows = 1;
        
    }
    
    
    
    return rows;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int height = 40;
    int heightOfPicker = 216; //Picker Height
    
    int row = indexPath.row;
    int section = indexPath.section;
    
    if (section == 2 && row == 0) { //iOS 6 --> Start/End Label
        
        if (IS_IOS_7)
            return height;
        else
            return 55; //larger for Start/End combo
    }
    else if (section == 2 && row == 2){ //iOS 7 --> Start Picker
        
        if (isStartDateShowing)
            return heightOfPicker;
        else
            return 0;
    }
    else if (section == 2 && row == 4){ //iOS 7 --> End Picker
        
        if (isEndDateShowing)
            return heightOfPicker;
        else
            return 0;
    }
    else if (section == 3 && row == 2){ //iOS 7 --> Alert 1 Picker
        
        if IS_IOS_7{
            
            if (isAlert_1Showing)
                return heightOfPicker;
            else
                return 0;
        }
        else{ //iOS 6
            
            return height;
        }
        
    }
    else if (section == 3 && row == 4){ //iOS 7 --> Alert 2 Picker
        
        if (isAlert_2Showing)
            return heightOfPicker;
        else
            return 0;
    }
    else if (section == 4 && row == 1){ //iOS 7 --> Progress Picker
        
        if (isProgressShowing)
            return heightOfPicker;
        else
            return 0;
    }
    else if (section == 5 && row == 2){ //iOS 7 --> Priority Picker
        
        if (isPriorityShowing)
            return heightOfPicker;
        else
            return 0;
    }
    else{
        return 40;
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier;
    int leftFrom=250;
    int switchLeftFrom=18;
    if (indexPath.section == 7) {
        CellIdentifier = @"deleteButtonCell";
    }
    else{
        CellIdentifier = @"Cell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    for (UIView  *subViews in [cell.contentView subviews]) {
        [subViews removeFromSuperview];
    }
    
    
    int y = 7; //y of each standard label
    
    
    if (indexPath.section == 0 && indexPath.row == 0) { //Title
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        titleTextField.frame = CGRectMake(10, y, 240, 24);
        [cell.contentView addSubview:titleTextField];
        
        
        [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.contentView addSubview:inputSelecterButton];
        
        
        if(IS_IPAD)
        {
            inputSelecterButton.frame = CGRectMake(self.view.frame.size.width-50, 5, 30, 30);
            titleTextField.frame = CGRectMake(10, y, 688, 24); //Steve
        }
        else
        {
            if (IS_IOS_7) {
                
                inputSelecterButton.frame = CGRectMake(280, 5, 30, 30);
            }
            else{
                
                inputSelecterButton.frame = CGRectMake(260, 5, 30, 30);
            }
        }
        
    }
    else if (indexPath.section == 1 && indexPath.row == 0){ //Projects Section
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        projectFixedLabel.frame = CGRectMake(10, y, 110, 24);
        projectFixedLabel.text = @"Project";
        [projectFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        projectFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:projectFixedLabel];
        
        if ([projectNameLabel.text isEqualToString:@""] || projectNameLabel.text == nil) {
            projectNameLabel.text = @"None";
        }
        
        //pooja-iPad
        if(IS_IPAD)
            projectNameLabel.frame = CGRectMake(self.view.frame.size.width-leftFrom, y, 200, 24);
        else
            projectNameLabel.frame = CGRectMake(125, y, 142, 24);
        
        
        projectNameLabel.backgroundColor = [UIColor clearColor];
        projectNameLabel.textAlignment = NSTextAlignmentRight;
        projectNameLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:projectNameLabel];
        
        
        //        if (IS_IOS_7) {
        //            //pooja-iPad
        //            if(IS_IPAD)
        //                projectNameLabel.frame = CGRectMake(250, y, 142, 24);
        //            else
        //                projectNameLabel.frame = CGRectMake(135, y, 152, 24);
        //        }
        //        else{ //iOS 6
        //
        //            projectNameLabel.frame = CGRectMake(125, y, 142, 24);
        //        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0){//iOS 7 --> On/Off , iOS 6 --> Start & Due
        
        if (IS_IOS_7) { //iOS 7 --> On/Off
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            UILabel *onOffLabel = [[UILabel alloc]init];
            onOffLabel.frame = CGRectMake(10, y, 180, 24);
            onOffLabel.text = @"Start & Due On/Off";
            [onOffLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            onOffLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:onOffLabel];
            
            //pooja-iPad
            if(IS_IPAD)
                self.startEndDateSwitch.frame = CGRectMake(self.view.frame.size.width - self.startEndDateSwitch.frame.size.width - switchLeftFrom, 3, 51, 29);
            else
                self.startEndDateSwitch.frame = CGRectMake(250, 3, 51, 29);
            
            
            self.startEndDateSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            [self.startEndDateSwitch addTarget:self
                                        action:@selector(startEndSwitchValueChanged)
                              forControlEvents:UIControlEventValueChanged];
            
            [cell.contentView addSubview:self.startEndDateSwitch];
            
        }
        else{ //iOS 6 --> Start & Due
            
            int y_Top = 2; // Start Label y (top label)
            int y_Bottom = 23; //Due Label y (Bottom Label)
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            startFixedLabel.frame = CGRectMake(10, y_Top, 110, 24);
            startFixedLabel.text = @"Start";
            [startFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            startFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:startFixedLabel];
            
            dueDateFixedLabel.frame = CGRectMake(10, y_Bottom, 110, 24);
            dueDateFixedLabel.text = @"Due";
            [dueDateFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            dueDateFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:dueDateFixedLabel];
            
            
            if ([lblStartDate.text isEqualToString:@""] || lblStartDate.text == nil) {
                lblStartDate.text = @"None";
            }
            //pooja-iPad
            if(IS_IPAD)
                lblStartDate.frame = CGRectMake(self.view.frame.size.width-leftFrom, y_Top, 142, 24);
            else
                lblStartDate.frame = CGRectMake(125, y_Top, 142, 24);
            lblStartDate.backgroundColor = [UIColor clearColor];
            lblStartDate.textAlignment = NSTextAlignmentRight;
            lblStartDate.textColor = [UIColor grayColor];
            [cell.contentView addSubview:lblStartDate];
            
            if ([lblEndDate.text isEqualToString:@""] || lblEndDate.text == nil) {
                lblEndDate.text = @"None";
            }
            //pooja-iPad
            if(IS_IPAD)
                lblEndDate.frame = CGRectMake(self.view.frame.size.width-leftFrom, y_Top, 142, 24);
            else
                lblEndDate.frame = CGRectMake(125, y_Bottom, 142, 24);
            lblEndDate.backgroundColor = [UIColor clearColor];
            lblEndDate.textAlignment = NSTextAlignmentRight;
            lblEndDate.textColor = [UIColor grayColor];
            [cell.contentView addSubview:lblEndDate];
            
        }
    }
    else if (indexPath.section == 2 && indexPath.row == 1){ //iOS 7 --> Start Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        startFixedLabel.frame = CGRectMake(10, y, 110, 24);
        startFixedLabel.text = @"Start";
        [startFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        startFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:startFixedLabel];
        
        
        if ([lblStartDate.text isEqualToString:@""] || lblStartDate.text == nil) {
            lblStartDate.text = @"None";
        }
        
        //pooja-iPad
        if(IS_IPAD)
            lblStartDate.frame = CGRectMake(self.view.frame.size.width - 200 - switchLeftFrom, y, 200, 24);
        else
            lblStartDate.frame = CGRectMake(103, y, 200, 24);
        
        lblStartDate.backgroundColor = [UIColor clearColor];
        lblStartDate.textAlignment = NSTextAlignmentRight;
        lblStartDate.textColor = [UIColor blackColor];
        lblStartDate.adjustsFontSizeToFitWidth=YES;
        [cell.contentView addSubview:lblStartDate];
        
        if (isStartDateShowing) {
            
            lblStartDate.textColor = [UIColor redColor];
        }
        else{
            
            lblStartDate.textColor = [UIColor blackColor];
        }
        
        
        
    }
    else if (indexPath.section == 2 && indexPath.row == 2){ //iOS 7 --> Start Picker
        
        if (isStartDateShowing)
            self.startDatePicker.hidden = NO;
        else
            self.startDatePicker.hidden = YES;
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self.startDatePicker addTarget:self
                                 action:@selector(startPickerChanged)
                       forControlEvents:UIControlEventValueChanged];
        
        
        self.startDatePicker.timeZone = [NSTimeZone localTimeZone];
        self.startDatePicker.locale   = [NSLocale currentLocale];
        self.startDatePicker.calendar = [NSCalendar currentCalendar];
        self.startDatePicker.datePickerMode = UIDatePickerModeDate;
        self.startDatePicker.minuteInterval = 5;
        
        [cell.contentView addSubview:self.startDatePicker];
        
        //Steve - centers picker
        if (IS_IPAD) {
            self.startDatePicker.frame = CGRectMake(self.view.frame.size.width/2 - self.startDatePicker.frame.size.width/2, 0, self.startDatePicker.frame.size.width, self.startDatePicker.frame.size.height);
        }
        
        
    }
    else if (indexPath.section == 2 && indexPath.row == 3){ //iOS 7 --> End Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        dueDateFixedLabel.frame = CGRectMake(10, y, 110, 24);
        dueDateFixedLabel.text = @"Due";
        [dueDateFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        dueDateFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:dueDateFixedLabel];
        
        
        if ([lblEndDate.text isEqualToString:@""] || lblEndDate.text == nil) {
            lblEndDate.text = @"None";
        }
        
        //pooja-iPad
        if(IS_IPAD)
            lblEndDate.frame = CGRectMake(self.view.frame.size.width-200-switchLeftFrom, y, 200, 24);
        else
            lblEndDate.frame = CGRectMake(103, y, 200, 24);
        lblEndDate.backgroundColor = [UIColor clearColor];
        lblEndDate.textAlignment = NSTextAlignmentRight;
        lblEndDate.adjustsFontSizeToFitWidth=YES;
        [cell.contentView addSubview:lblEndDate];
        
        if (isEndDateShowing) {
            
            lblEndDate.textColor = [UIColor redColor];
        }
        else{
            
            lblEndDate.textColor = [UIColor blackColor];
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 4){ //iOS 7 --> End Picker
        
        
        if (isEndDateShowing)
            self.endDatePicker.hidden = NO;
        else
            self.endDatePicker.hidden = YES;
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        
        [self.endDatePicker addTarget:self
                               action:@selector(endPickerChanged)
                     forControlEvents:UIControlEventValueChanged];
        
        //self.endDatePicker.date = self.endDate_Date; //keeps international Date compatible
        self.endDatePicker.timeZone = [NSTimeZone localTimeZone];
        self.endDatePicker.locale   = [NSLocale currentLocale];
        self.endDatePicker.calendar = [NSCalendar currentCalendar];
        self.endDatePicker.datePickerMode = UIDatePickerModeDate;
        self.endDatePicker.minuteInterval = 5;
        
        [cell.contentView addSubview:self.endDatePicker];
        
        //Steve
        if (IS_IPAD) {
            self.endDatePicker.frame = CGRectMake(self.view.frame.size.width/2 - self.endDatePicker.frame.size.width/2, 0, self.endDatePicker.frame.size.width, self.endDatePicker.frame.size.height);
        }
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //iOS 7 --> Alert ON/Off, iOS 6 --> Alert 1
        
        if (IS_IOS_7) {
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            UILabel *onOffLabel = [[UILabel alloc]init];
            onOffLabel.frame = CGRectMake(10, y, 150, 24);
            onOffLabel.text = @"Alerts On/Off";
            [onOffLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            onOffLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:onOffLabel];
            
            if(IS_IPAD)
                self.alertSwitchOnOff.frame = CGRectMake(self.view.frame.size.width - self.alertSwitchOnOff.frame.size.width - switchLeftFrom, 3, 51, 29);
            else
                self.alertSwitchOnOff.frame = CGRectMake(250, 3, 51, 29);
            
            
            self.alertSwitchOnOff.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            [self.alertSwitchOnOff addTarget:self
                                      action:@selector(alertSwitchOnOffValueChange)
                            forControlEvents:UIControlEventValueChanged];
            
            [cell.contentView addSubview:self.alertSwitchOnOff];
            
            
        }
        else{ // iOS 6 --> Alert 1
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            alertFixedLabel.frame = CGRectMake(10, y, 80, 24);
            alertFixedLabel.text = @"Alert 1";
            [alertFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            alertFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:alertFixedLabel];
            
            if ([alertsLabel.text isEqualToString:@""] || alertsLabel.text == nil) {
                alertsLabel.text = @"None";
            }
            //pooja-iPad
            if(IS_IPAD)
                alertsLabel.frame = CGRectMake(self.view.frame.size.width-leftFrom, y, 192, 24);
            else
                alertsLabel.frame = CGRectMake(75, y, 192, 24);//CGRectMake(95, y, 172, 24);
            alertsLabel.numberOfLines = 1;
            //alertsLabel.minimumFontSize = 14;
            alertsLabel.numberOfLines = 14;
            alertsLabel.adjustsFontSizeToFitWidth = YES;
            alertsLabel.backgroundColor = [UIColor clearColor];
            alertsLabel.textAlignment = NSTextAlignmentRight;
            alertsLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:alertsLabel];
        }
        
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ //iOS 7 --> Alert 1 Label, iOS 6 --> Alert 2
        
        
        if (IS_IOS_7) { //iOS 7 --> Alert 1 Label
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            alertFixedLabel.frame = CGRectMake(10, y, 110, 24);
            alertFixedLabel.text = @"Alert 1";
            [alertFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            alertFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:alertFixedLabel];
            
            
            if ([alertsLabel.text isEqualToString:@""] || alertsLabel.text == nil) {
                alertsLabel.text = @"None";
            }
            
            //pooja-iPad
            if(IS_IPAD)
                alertsLabel.frame = CGRectMake(self.view.frame.size.width - 200 - switchLeftFrom , y, 200, 24);
            else
                alertsLabel.frame = CGRectMake(103, y, 200, 24);
            
            
            alertsLabel.backgroundColor = [UIColor clearColor];
            alertsLabel.textAlignment = NSTextAlignmentRight;
            alertsLabel.textColor = [UIColor blackColor];
            [cell.contentView addSubview:alertsLabel];
            
            if (isAlert_1Showing) {
                
                alertsLabel.textColor = [UIColor redColor];
            }
            else{
                
                alertsLabel.textColor = [UIColor blackColor];
            }
            
        }
        else{//iOS 6 --> Alert 2
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            lbl_SecondAlert.frame = CGRectMake(10, y, 80, 24); //CGRectMake(10, y, 110, 24)
            lbl_SecondAlert.text = @"Alert 2";
            [lbl_SecondAlert setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            lbl_SecondAlert.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:lbl_SecondAlert];
            
            if ([secondAlertsLabel.text isEqualToString:@""] || secondAlertsLabel.text == nil) {
                secondAlertsLabel.text = @"None";
            }
            //pooja-iPad
            if(IS_IPAD)
                secondAlertsLabel.frame = CGRectMake(self.view.frame.size.width-leftFrom, y, 192, 24);
            else
                secondAlertsLabel.frame = CGRectMake(75, y, 192, 24); //CGRectMake(95, y, 172, 24);
            secondAlertsLabel.numberOfLines = 1;
            //secondAlertsLabel.minimumFontSize = 14;
            secondAlertsLabel.numberOfLines = 14;
            secondAlertsLabel.adjustsFontSizeToFitWidth = YES;
            secondAlertsLabel.backgroundColor = [UIColor clearColor];
            secondAlertsLabel.textAlignment = NSTextAlignmentRight;
            secondAlertsLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:secondAlertsLabel];
            
        }
    }
    else if (indexPath.section == 3 && indexPath.row == 2){ //iOS 7 --> Alert 1 Picker
        
        if (isAlert_1Showing)
            self.alert_1_Picker.hidden = NO;
        else
            self.alert_1_Picker.hidden = YES;
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self.alert_1_Picker addTarget:self
                                action:@selector(alert_1_PickerValueChanged)
                      forControlEvents:UIControlEventValueChanged];
        
        
        self.alert_1_Picker.timeZone = [NSTimeZone localTimeZone];
        self.alert_1_Picker.locale   = [NSLocale currentLocale];
        self.alert_1_Picker.calendar = [NSCalendar currentCalendar];
        self.alert_1_Picker.datePickerMode = UIDatePickerModeDateAndTime;
        self.alert_1_Picker.minuteInterval = 5;
        //self.alert_1_Picker.backgroundColor =[UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.5];
        
        [cell.contentView addSubview:self.alert_1_Picker];
        
        //Steve
        if (IS_IPAD) {
            self.alert_1_Picker.frame = CGRectMake(self.view.frame.size.width/2 - self.alert_1_Picker.frame.size.width/2, 0, self.alert_1_Picker.frame.size.width, self.alert_1_Picker.frame.size.height);
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 3){ //iOS 7 --> Alert 2 Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        lbl_SecondAlert.frame = CGRectMake(10, y, 110, 24);
        lbl_SecondAlert.text = @"Alert 2";
        [lbl_SecondAlert setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        lbl_SecondAlert.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:lbl_SecondAlert];
        
        
        if ([secondAlertsLabel.text isEqualToString:@""] || secondAlertsLabel.text == nil) {
            secondAlertsLabel.text = @"None";
        }
        
        //pooja-iPad
        if(IS_IPAD)
            secondAlertsLabel.frame = CGRectMake(self.view.frame.size.width- 200 - switchLeftFrom, y, 200, 24);
        else
            secondAlertsLabel.frame = CGRectMake(103, y, 200, 24);
        
        
        secondAlertsLabel.backgroundColor = [UIColor clearColor];
        secondAlertsLabel.textAlignment = NSTextAlignmentRight;
        secondAlertsLabel.textColor = [UIColor blackColor];
        [cell.contentView addSubview:secondAlertsLabel];
        
        if (isAlert_2Showing) {
            
            secondAlertsLabel.textColor = [UIColor redColor];
        }
        else{
            
            secondAlertsLabel.textColor = [UIColor blackColor];
        }
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 4){ //iOS 7 --> Alert 2 Picker
        
        if (isAlert_2Showing)
            self.alert_2_Picker.hidden = NO;
        else
            self.alert_2_Picker.hidden = YES;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self.alert_2_Picker addTarget:self
                                action:@selector(alert_2_PickerValueChanged)
                      forControlEvents:UIControlEventValueChanged];
        
        
        self.alert_2_Picker.timeZone = [NSTimeZone localTimeZone];
        self.alert_2_Picker.locale   = [NSLocale currentLocale];
        self.alert_2_Picker.calendar = [NSCalendar currentCalendar];
        self.alert_2_Picker.datePickerMode = UIDatePickerModeDateAndTime;
        self.alert_2_Picker.minuteInterval = 5;
        //self.alert_2_Picker.backgroundColor =[UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.5];
        
        [cell.contentView addSubview:self.alert_2_Picker];
        
        if (IS_IPAD) {
            self.alert_2_Picker.frame = CGRectMake(self.view.frame.size.width/2 - self.alert_2_Picker.frame.size.width/2, 0, self.alert_2_Picker.frame.size.width, self.alert_2_Picker.frame.size.height);
        }
        
    }
    
    else if (indexPath.section == 4 && indexPath.row == 0){ // iOS 7 --> % Complete Label, iOS 6 --> % Completed
        
        if (IS_IOS_7) { // iOS 7 --> % Complete Label
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            
            lbl_Completed.frame = CGRectMake(10, y, 110, 24);
            lbl_Completed.text = @"% Completed";
            [lbl_Completed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            lbl_Completed.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:lbl_Completed];
            
            if ([progressLabel.text isEqualToString:@""] || progressLabel.text == nil) {
                progressLabel.text = @"0";
            }
            
            //progress label with value
            //pooja-iPad
            if(IS_IPAD)
                progressLabel.frame = CGRectMake(self.view.frame.size.width-185-switchLeftFrom-15, y, 185, 24);
            else
                progressLabel.frame = CGRectMake(103, y, 185, 24);
            
            
            progressLabel.backgroundColor = [UIColor clearColor];
            progressLabel.textAlignment = NSTextAlignmentRight;
            progressLabel.adjustsFontSizeToFitWidth=YES;
            [cell.contentView addSubview:progressLabel];
            
            
            if ([lbl_PercentageSymbol.text isEqualToString:@""] || lbl_PercentageSymbol.text == nil) {
                lbl_PercentageSymbol.text = @"%";
            }
            
            //% symbol fixed label
            lbl_PercentageSymbol.frame = CGRectMake(progressLabel.frame.origin.x + progressLabel.frame.size.width, y, 17, 24);
            lbl_PercentageSymbol.backgroundColor = [UIColor clearColor];
            lbl_PercentageSymbol.textAlignment = NSTextAlignmentLeft;
            [cell.contentView addSubview:lbl_PercentageSymbol];
            
            
            if (isProgressShowing) {
                
                progressLabel.textColor = [UIColor redColor];
                lbl_PercentageSymbol.textColor = [UIColor redColor];
            }
            else{
                
                progressLabel.textColor = [UIColor grayColor];
                lbl_PercentageSymbol.textColor = [UIColor grayColor];
            }
            
        }
        else{ //iOS 6 --> % Completed
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            
            lbl_Completed.frame = CGRectMake(10, y, 110, 24);
            lbl_Completed.text = @"% Completed";
            [lbl_Completed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            lbl_Completed.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:lbl_Completed];
            
            if ([progressLabel.text isEqualToString:@""] || progressLabel.text == nil) {
                progressLabel.text = @"0";
            }
            
            //progress label with value
            //pooja-iPad
            if(IS_IPAD)
                progressLabel.frame = CGRectMake(self.view.frame.size.width-leftFrom, y, 152, 24);
            else
                progressLabel.frame = CGRectMake(100, y, 152, 24);
            progressLabel.backgroundColor = [UIColor clearColor];
            progressLabel.textAlignment = NSTextAlignmentRight;
            progressLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:progressLabel];
            
            
            if ([lbl_PercentageSymbol.text isEqualToString:@""] || lbl_PercentageSymbol.text == nil) {
                lbl_PercentageSymbol.text = @"%";
            }
            
            //% symbol fixed label
            lbl_PercentageSymbol.frame = CGRectMake(progressLabel.frame.origin.x + progressLabel.frame.size.width, y, 17, 24);
            lbl_PercentageSymbol.backgroundColor = [UIColor clearColor];
            lbl_PercentageSymbol.textAlignment = NSTextAlignmentLeft;
            lbl_PercentageSymbol.textColor = [UIColor grayColor];
            [cell.contentView addSubview:lbl_PercentageSymbol];
        }
        
    }
    if (indexPath.section == 4 && indexPath.row == 1) {  // iOS 7 --> % Complete Picker
        
        if (isProgressShowing)
            self.progressPicker.hidden = NO;
        else
            self.progressPicker.hidden = YES;
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        self.progressPicker.frame = CGRectMake(0, 0, 320, 216);
        //self.progressPicker.backgroundColor =[UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.5];
        
        [cell.contentView addSubview:self.progressPicker];
        
        //Steve
        if (IS_IPAD) {
            self.progressPicker.frame = CGRectMake(self.view.frame.size.width/2-self.progressPicker.frame.size.width/2, 0, self.progressPicker.frame .size.width, self.progressPicker.frame.size.height);
        }
        
        
    }
    if (indexPath.section == 5 && indexPath.row == 0) { // iOS 7 --> Priority ON/OFF, iOS 6 --> Priority
        
        
        if (IS_IOS_7) { // iOS 7 --> Priority ON/OFF
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            UILabel *onOffLabel = [[UILabel alloc]init];
            onOffLabel.frame = CGRectMake(10, y, 150, 24);
            onOffLabel.text = @"Priority On/Off";
            [onOffLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            onOffLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:onOffLabel];
            
            //pooja-iPad
            if(IS_IPAD)
                self.prioritySwitch.frame = CGRectMake(self.view.frame.size.width-51-switchLeftFrom, 3, 51, 29);
            else
                self.prioritySwitch.frame = CGRectMake(250, 3, 51, 29);
            
            
            [self.prioritySwitch addTarget:self
                                    action:@selector(prioritySwitchValueChange)
                          forControlEvents:UIControlEventValueChanged];
            self.prioritySwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            
            [cell.contentView addSubview:self.prioritySwitch];
            
        }
        else{ //iOS 6 --> Priority
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            lbl_Priority.frame = CGRectMake(10, y, 100, 24);
            lbl_Priority.text = @"Priority";
            [lbl_Priority setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            lbl_Priority.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:lbl_Priority];
            
            if ([priorityLabel.text isEqualToString:@""] || priorityLabel.text == nil) {
                priorityLabel.text = @"None";
            }
            //pooja-iPad
            if(IS_IPAD)
                priorityLabel.frame = CGRectMake(self.view.frame.size.width-leftFrom, y, 152, 24);
            else
                priorityLabel.frame = CGRectMake(115, y, 152, 24);
            priorityLabel.backgroundColor = [UIColor clearColor];
            priorityLabel.textAlignment = NSTextAlignmentRight;
            priorityLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:priorityLabel];
        }
        
    }
    if (indexPath.section == 5 && indexPath.row == 1) { //iOS 7 --> Priority Label
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        lbl_Priority.frame = CGRectMake(10, y, 110, 24);
        lbl_Priority.text = @"Priority";
        [lbl_Priority setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        lbl_Priority.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:lbl_Priority];
        
        
        if ([priorityLabel.text isEqualToString:@""] || priorityLabel.text == nil) {
            priorityLabel.text = @"None";
        }
        
        //priorityLabel.frame = CGRectMake(135, y, 152, 24);
        //pooja-iPad
        if(IS_IPAD)
            priorityLabel.frame = CGRectMake(self.view.frame.size.width-200-switchLeftFrom, y, 200, 24);
        else
            priorityLabel.frame = CGRectMake(103, y, 200, 24);
        
        
        priorityLabel.backgroundColor = [UIColor clearColor];
        priorityLabel.textAlignment = NSTextAlignmentRight;
        priorityLabel.textColor = [UIColor blackColor];
        [cell.contentView addSubview:priorityLabel];
        
        
        if (isPriorityShowing) {
            
            priorityLabel.textColor = [UIColor redColor];
        }
        else{
            
            priorityLabel.textColor = [UIColor blackColor];
        }
        
        
        
    }
    if (indexPath.section == 5 && indexPath.row == 2) {  //iOS 7 --> Priority Picker
        
        if (isPriorityShowing)
            self.priorityPicker.hidden = NO;
        else
            self.priorityPicker.hidden = YES;
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        self.priorityPicker.frame = CGRectMake(((self.view.frame.size.width-320)/2), 0, 320, 216);
        //self.priorityPicker.backgroundColor =[UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.5];
        //self.priorityPicker.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:0.50];
        
        [cell.contentView addSubview:self.priorityPicker];
        
        //Steve
        if (IS_IPAD) {
            self.priorityPicker.frame = CGRectMake(self.view.frame.size.width/2-self.priorityPicker.frame.size.width/2, 0, self.priorityPicker.frame.size.width, self.priorityPicker.frame.size.height);
        }
        
        
    }
    if (indexPath.section == 6 && indexPath.row == 0) { //Starred
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        
        lbl_Starred.frame = CGRectMake(10, y, 100, 24);
        lbl_Starred.text = @"Starred";
        [lbl_Starred setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        lbl_Starred.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:lbl_Starred];
        
        
        if (isStarred==YES){
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred.png"]];
        }
        else{
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred-a.png"]];
        }
        
        if (IS_IOS_7) {
            if(IS_IPAD)
                starredImgView.frame = CGRectMake(self.view.frame.size.width-21-switchLeftFrom, 10, 21, 19);
            else
                starredImgView.frame = CGRectMake(280, 10, 21, 19);
        }
        else{
            
            starredImgView.frame = CGRectMake(270, 10, 21, 19);
        }
        starredImgView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:starredImgView];
    }
    if (indexPath.section == 7 && indexPath.row == 0) { //Delete Button
        
        if (IS_IOS_7) {
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            int cellWidth = cell.frame.size.width;
            int cellHeight = cell.frame.size.height;
            int buttonWidth = 150;
            int buttonHeight = 35;
            
            UIButton *deleteButtonCustom = [UIButton buttonWithType:UIButtonTypeSystem];
            
            [deleteButtonCustom setTitle:@"Delete" forState:UIControlStateNormal];
            deleteButtonCustom.tintColor = [UIColor redColor];
            deleteButtonCustom.titleLabel.textAlignment = NSTextAlignmentCenter;
            deleteButtonCustom.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18]; //HelveticaNeue-Bold
            deleteButtonCustom.backgroundColor = [UIColor clearColor];
            
            //Steve
            if (IS_IPAD)
            {
                deleteButtonCustom.frame =CGRectMake(self.view.frame.size.width/2 - buttonWidth/2, cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
            }else
            {
                deleteButtonCustom.frame =CGRectMake(cellWidth/2  , cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
            }
            
            
            [deleteButtonCustom addTarget:self
                                   action:@selector(delete_buttonClicked:)
                         forControlEvents:UIControlEventTouchDown];
            
            [cell.contentView addSubview:deleteButtonCustom];
            
        }
        else{
            
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            
            UIImage *deleteButtonImage = [UIImage imageNamed:@"DeleteBtn.png"];
            
            int cellWidth = cell.frame.size.width - 20; //approximate size of cell width
            int deleteButtonWidth = deleteButtonImage.size.width;
            
            deleteButton.frame = CGRectMake(cellWidth/2 - deleteButtonWidth/2, 2, deleteButtonWidth, 41);
            [deleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(delete_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:deleteButton];
            
        }
        
        
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [titleTextField resignFirstResponder];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    
    if (indexPath.section == 1 && indexPath.row == 0) { //Projects
        ProjectListViewController *projectListViewController;
        if(IS_IPAD)
        {
            projectListViewController = [[ProjectListViewController alloc] initWithNibName:@"ProjectListViewControllerIPad" bundle:[NSBundle mainBundle] withProjectID:projectNameLabel.tag];
        }
        else
        {
            projectListViewController = [[ProjectListViewController alloc] initWithNibName:@"ProjectListViewController" bundle:[NSBundle mainBundle] withProjectID:projectNameLabel.tag];
        }
        [projectListViewController setDelegate:self];
        [self.navigationController pushViewController:projectListViewController animated:YES];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0 && !IS_IOS_7){ //iOS 6 --> Start/End
        
        /*
         NSLog(@"startsLabel.text =  %@", startsLabel.text);
         NSLog(@"endsLabel.text =  %@", endsLabel.text);
         
         NSLog(@"lblStartDate.text =  %@", lblStartDate.text);
         NSLog(@"endsLabel.text =  %@", endsLabel.text);
         */
        
        AddEditStartDateEndDateVC *addEditStartDateEndDateVC= [[AddEditStartDateEndDateVC alloc] initWithStartDateString:startsLabel.text == nil ? @"" : startsLabel.text endDateStr:endsLabel.text == nil ? @"" : endsLabel.text isAllDay:NO];
        
        
        [addEditStartDateEndDateVC setDelegate:self];
        [addEditStartDateEndDateVC setCallerVC:@"ToDoViewController"];
        
        [self.navigationController pushViewController:addEditStartDateEndDateVC animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 1 && IS_IOS_7){ //iOS 7 --> Start Picker Open/close
        
        if (isStartDateShowing) { //Closes cell with picker
            
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            isStartDateShowing = NO;
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:2];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects: indexPath1, indexPath2, indexPath3, indexPath4, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        }
        else{           //Opens cell with picker
            
            isStartDateShowing = YES;
            self.startEndDateSwitch.on = YES;
            
            //Steve - for iPad
            self.startDatePicker.hidden = NO;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            //closes all other pickers
            isEndDateShowing = NO;
            isAlert_1Showing = NO;
            isAlert_2Showing = NO;
            isProgressShowing = NO;
            isPriorityShowing = NO;
            
            
            self.startDatePicker.date = self.startDate_Date;
            
            [dateFormatter setLocale:[NSLocale currentLocale]];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            
            startsLabel.text = [dateFormatter stringFromDate:self.startDate_Date];
            lblStartDate.text = startsLabel.text;
            
            self.endDatePicker.date = self.endDate_Date;
            endsLabel.text = [dateFormatter stringFromDate:self.endDate_Date];
            lblEndDate.text = endsLabel.text;
            
            //NSLog(@"lblStartDate.text =  %@", lblStartDate.text);
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:3 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            //[tableView reloadData];
            
            if (!IS_IPHONE_5) { //Not iPhone 5
                
                //Scrolls this section & row to the top for easier viewing
                NSInteger row = 0;
                NSInteger section = indexPath.section;
                NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
                [tableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
                
            }
            
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 3 && IS_IOS_7){ //iOS 7 --> End Picker Open/Close
        
        if (isEndDateShowing) { //Closes cell with picker
            
            isEndDateShowing = NO;
            
            //Steve for iPad
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:2];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        }
        else{           //Opens cell with pricker
            
            isEndDateShowing = YES;
            self.startEndDateSwitch.on = YES;
            
            //Steve for iPad
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = NO;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            //closes all other pickers
            isStartDateShowing = NO;
            isAlert_1Showing = NO;
            isAlert_2Showing = NO;
            isProgressShowing = NO;
            isPriorityShowing = NO;
            
            
            self.startDatePicker.date = self.startDate_Date;
            
            [dateFormatter setLocale:[NSLocale currentLocale]];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            
            startsLabel.text = [dateFormatter stringFromDate:self.startDate_Date];
            lblStartDate.text = startsLabel.text;
            
            self.endDatePicker.date = self.endDate_Date;
            endsLabel.text = [dateFormatter stringFromDate:self.endDate_Date];
            lblEndDate.text = endsLabel.text;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:3 inSection:2];
            NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:4 inSection:2];
            
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4, indexPath5, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            // [tableView reloadData];
            
            if (!IS_IPHONE_5) { //Not iPhone 5
                
                //Scrolls this section & row to the top for easier viewing
                NSInteger row = 0;
                NSInteger section = indexPath.section;
                NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
                [tableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
            }
            
            
        }
        
    }
    
    else if (indexPath.section == 3 && indexPath.row == 0){ //iOS 7 --> Alert On/Off, iOS 6 --> Alert 1
        
        if (!IS_IOS_7) { //Not iOS 7
            
            [self alerts_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ //iOS 7 --> Alert 1 Label Picker Open/Close, iOS 6 --> Alert 2
        
        if (IS_IOS_7) { //iOS 7 --> Alert 1 Label Picker Open/Close
            
            if (isAlert_1Showing) { //Closes cell with picker
                
                isAlert_1Showing = NO;
                
                //Steve for iPad
                self.startDatePicker.hidden = YES;
                self.endDatePicker.hidden = YES;
                self.alert_1_Picker.hidden = YES;
                self.alert_2_Picker.hidden = YES;
                self.progressPicker.hidden = YES;
                self.priorityPicker.hidden = YES;
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
                NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
                NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            }
            else{           //Opens cell with pricker
                
                isAlert_1Showing = YES;
                self.alertSwitchOnOff.on = YES;
                
                //Steve for iPad
                self.startDatePicker.hidden = YES;
                self.endDatePicker.hidden = YES;
                self.alert_1_Picker.hidden = NO;
                self.progressPicker.hidden = YES;
                self.priorityPicker.hidden = YES;
                self.alert_2_Picker.hidden = YES;
                
                //closes all other pickers
                isAlert_2Showing = NO;
                isStartDateShowing = NO;
                isEndDateShowing = NO;
                isProgressShowing = NO;
                isPriorityShowing = NO;
                
                
                if (self.alert_1_Date != nil)
                    self.alert_1_Picker.date = self.alert_1_Date;
                
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                
                alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date];
                alertString = alertsLabel.text;
                
                
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath0 = [NSIndexPath indexPathForRow:0 inSection:3];
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
                NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
                NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath0, indexPath1, indexPath2, indexPath3, indexPath4, nil];
                [self.todoTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                
                //Moves Section to top
                [self performSelector:@selector(animateAlertSectionToTop) withObject:nil afterDelay:0.25];
                
            }
            
        }
        else{ //iOS 6
            
            [self alerts_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 3){ //iOS 7 --> Alert 2 Label,  Picker Open/Close
        
        
        if (isAlert_2Showing) { //Closes cell with picker
            
            isAlert_2Showing = NO;
            
            //Steve for iPad
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        }
        else{           //Opens cell with pricker
            
            isAlert_2Showing = YES;
            self.alertSwitchOnOff.on = YES;
            
            //Steve for iPad
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = NO;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            //closes all other pickers
            isAlert_1Showing = NO;
            isStartDateShowing = NO;
            isEndDateShowing = NO;
            isProgressShowing = NO;
            isPriorityShowing = NO;
            
            
            self.alert_2_Picker.date = self.alert_2_Date;
            
            [dateFormatter setLocale:[NSLocale currentLocale]];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            
            secondAlertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date];
            //secondAlertsLabel = secondAlertsLabel.text;
            
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:3];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:3];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:3];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:3];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2,indexPath3, indexPath4, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            
            //moves section to top
            [self performSelector:@selector(animateAlertSectionToTop) withObject:nil afterDelay:0.25];
            
        }
        
        
    }
    else if (indexPath.section == 4 && indexPath.row == 0){ //% Complete (Progress)
        
        if (IS_IOS_7) {
            
            if (isProgressShowing) { //Closes cell with picker
                
                isProgressShowing = NO;
                
                
                //Steve for iPad
                self.startDatePicker.hidden = YES;
                self.endDatePicker.hidden = YES;
                self.alert_1_Picker.hidden = YES;
                self.alert_2_Picker.hidden = YES;
                self.progressPicker.hidden = YES;
                self.priorityPicker.hidden = YES;
                
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:4];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:1 inSection:4];
                
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
            }
            else{           //Opens cell with picker
                
                isProgressShowing = YES;
                
                self.startDatePicker.hidden = YES;
                self.endDatePicker.hidden = YES;
                self.alert_1_Picker.hidden = YES;
                self.alert_2_Picker.hidden = YES;
                self.progressPicker.hidden = NO;
                self.priorityPicker.hidden = YES;
                
                //closes all other pickers
                isAlert_1Showing = NO;
                isAlert_2Showing = NO;
                isStartDateShowing = NO;
                isEndDateShowing = NO;
                isPriorityShowing = NO;
                
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:4];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:1 inSection:4];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                
                //[tableView reloadData];
                
                
                //Scrolls this section & row to the top for easier viewing
                NSInteger row = 0;
                NSInteger section = indexPath.section;
                NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
                [tableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
                
            }
            
        }
        else{ //iOS 6
            
            ProgressPickerView *selectview = [[ProgressPickerView alloc] initWithNibName:@"ProgressPickerView" bundle:[NSBundle mainBundle]withProgressVAL:[NSString stringWithFormat:@"%@", progressLabel.text]];
            [self.navigationController pushViewController:selectview animated:YES];
            [selectview setDelegate:self];
        }
        
    }
    else if (indexPath.section == 5 && indexPath.row == 0){ //iOS 6 --> Priority
        
        if (!IS_IOS_7) {
            
            PriorityPickerView *selectviewP = [[PriorityPickerView alloc]initWithNibName:@"PriorityPickerView" bundle:[NSBundle mainBundle]setNewPriority:[NSString stringWithFormat:@"%@", priorityLabel.text]];
            [self.navigationController pushViewController:selectviewP animated:YES];
            [selectviewP setDelegate:self];
        }
        
    }
    if (indexPath.section == 5 && indexPath.row == 1){ //iOS 7 --> Priority Picker
        
        if (isPriorityShowing) { //Closes cell with picker
            
            isPriorityShowing = NO;
            
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = YES;
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:5];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:5];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
        }
        else{           //Opens cell with pricker
            
            isPriorityShowing = YES;
            
            self.startDatePicker.hidden = YES;
            self.endDatePicker.hidden = YES;
            self.alert_1_Picker.hidden = YES;
            self.alert_2_Picker.hidden = YES;
            self.progressPicker.hidden = YES;
            self.priorityPicker.hidden = NO;
            
            //closes all other pickers
            isAlert_1Showing = NO;
            isAlert_2Showing = NO;
            isStartDateShowing = NO;
            isEndDateShowing = NO;
            isProgressShowing = NO;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:5];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:5];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            //[tableView reloadData];
            
            
            //Scrolls this section & row to the top for easier viewing
            NSInteger row = 0;
            NSInteger section = indexPath.section;
            NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
            [tableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
            
        }
        
    }
    else if (indexPath.section == 6 && indexPath.row == 0){ //Starred
        
        if (isStarred==YES)
        {
            isStarred = NO;
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred-a.png"]];
        }
        else
        {
            isStarred = YES;
            [starredImgView setImage:[UIImage imageNamed:@"IconStarred.png"]];
        }
        
    }
    else if (indexPath.section == 7 && indexPath.row == 0) { //Delete Button
        
        [self delete_buttonClicked:self];
    }
    
}

-(void) animateAlertSectionToTop{
    
    //Scrolls this section & row to the top for easier viewing
    NSInteger row = 0;
    NSInteger section = 3;
    NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
    [self.todoTableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
    
}

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        
        [titleTextField resignFirstResponder];
        ProjectListViewController *projectListViewController = [[ProjectListViewController alloc] initWithNibName:@"ProjectListViewController" bundle:[NSBundle mainBundle] withProjectID:projectNameLabel.tag];
        [projectListViewController setDelegate:self];
        [self.navigationController pushViewController:projectListViewController animated:YES];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0){
        
        AddEditStartDateEndDateVC *addEditStartDateEndDateVC= [[AddEditStartDateEndDateVC alloc] initWithStartDateString:startsLabel.text == nil ? @"" : startsLabel.text endDateStr:endsLabel.text == nil ? @"" : endsLabel.text isAllDay:NO];
        
        
        [addEditStartDateEndDateVC setDelegate:self];
        [addEditStartDateEndDateVC setCallerVC:@"ToDoViewController"];
        [self.navigationController pushViewController:addEditStartDateEndDateVC animated:YES];
        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){
        [self alerts_buttonClicked:self];
    }
    else if (indexPath.section == 3 && indexPath.row == 1){
        [self alertsecond_buttonClicked:self];
    }
    else if (indexPath.section == 4 && indexPath.row == 0){
        
        ProgressPickerView *selectview = [[ProgressPickerView alloc] initWithNibName:@"ProgressPickerView" bundle:[NSBundle mainBundle]withProgressVAL:[NSString stringWithFormat:@"%@", progressLabel.text]];
        [self.navigationController pushViewController:selectview animated:YES];
        [selectview setDelegate:self];
    }
    else if (indexPath.section == 5 && indexPath.row == 0){
        
        PriorityPickerView *selectviewP = [[PriorityPickerView alloc]initWithNibName:@"PriorityPickerView" bundle:[NSBundle mainBundle]setNewPriority:[NSString stringWithFormat:@"%@", priorityLabel.text]];
        [self.navigationController pushViewController:selectviewP animated:YES];
        [selectviewP setDelegate:self];
    }
    
    
}


#pragma mark -
#pragma mark Memory Management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    lblStartDate = nil;
    lblEndDate = nil;
    alertsLabel = nil;
    secondAlertsLabel = nil;
    [self setBtn_CustomView:nil];
    [self setLbl_SecondAlert:nil];
    [self setImg_SecondAlertArrow:nil];
    [self setBtn_Alert:nil];
    [self setBtn_SecondAlert:nil];
    [self setBtn_Completed:nil];
    [self setBtn_Priority:nil];
    [self setBtn_Starred:nil];
    [self setLbl_Completed:nil];
    [self setLbl_Priority:nil];
    [self setLbl_Starred:nil];
    [self setLbl_PercentageSymbol:nil];
    [self setImg_CompletedArrow:nil];
    [self setImg_PriorityArrow:nil];
    //lbl_Line = nil; //Steve commented
    navBar = nil;
    box_Line = nil;
    backButton = nil;
    titleButton = nil;
    projectButton = nil;
    projectFixedLabel = nil;
    projectDisclosureImageView = nil;
    startEndButton = nil;
    startFixedLabel = nil;
    dueDateFixedLabel = nil;
    startEndDisclosureImageVw = nil;
    alertFixedLabel = nil;
    alertDisclosureImageVw = nil;
    self.todoTableView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)handwritingView
{
    if (IS_IPAD ) {
        
        //HandWritingViewController
        HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController_iPad"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
        handWritingViewController.handwritingForiPadDelegate=self;
        
        NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
        [stringList setValue:@"TODO" forKey:@"Viewcontroller"];
        [stringList synchronize];
        
        [self addChildViewController:handWritingViewController];
        [self.view addSubview:handWritingViewController.view];
        [self.view bringSubviewToFront:handWritingViewController.view];
        handWritingViewController.view.hidden = YES;
        
        //animates the handWritingViewController.view
        [self performSelector:@selector(fadeInHandwritingView:) withObject:handWritingViewController.view afterDelay:0.0];
        
        
        //Steve - adds the blurr image of the screehshot
        int blurredViewWidth = [[UIScreen mainScreen] bounds].size.width;
        int blurredViewHeight = [[UIScreen mainScreen] bounds].size.height - 320;
        
        UIImage *image = [self captureScreenShotAndblurImage];
        self.blurredImageView = [[UIImageView alloc]initWithImage:image];
        self.blurredImageView.frame = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight); //Change frame size so it doesn't cover HandwritingView
        [self.view addSubview:self.blurredImageView];
        self.blurredImageView.hidden = YES;
        
        //Animates self.blurredImageView
        [self performSelector:@selector(fadeInBlurredImage) withObject:nil afterDelay:0.0];
        
        //UIImageView *testImageView = self.blurredImageView;
    }
    
}


-(UIImage*) captureScreenShotAndblurImage{
    
    
    // Create a graphics context with the target size
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    
    UIGraphicsBeginImageContext(imageSize);
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Iterate over every window from back to front
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context
            CGContextSaveGState(context);
            // Center the context around the window's anchor point
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            // Apply the window's transform about the anchor point
            CGContextConcatCTM(context, [window transform]);
            // Offset by the portion of the bounds left of and above the anchor point
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
            
            // Render the layer hierarchy to the current context
            //[[window layer] renderInContext:context];
            
            // iOS 7 New API
            [window drawViewHierarchyInRect:[[UIScreen mainScreen] bounds] afterScreenUpdates:NO];
            
            // Restore the context
            CGContextRestoreGState(context);
        }
    }
    
    
    // Get the snapshot
    _screenShotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Now apply the blur effect using Apple's UIImageEffect category
    UIImage *blurredScreenshotImage = [_screenShotImage applyDarkEffectWithLessBlur];//applyLightEffect  applyDarkEffectWithLessBlur
    
    // Or apply any other effects available in "UIImage+ImageEffects.h"
    // UIImage *blurredScreenshotImage = [screenShotImage applyDarkEffect];
    //UIImage *blurredScreenshotImage = [screenShotImage applyExtraLightEffect];
    
    UIGraphicsEndImageContext();
    
    
    return blurredScreenshotImage;
    
}

- (void) fadeInHandwritingView:(UIView*) handwritingView{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.50;
    [handwritingView.layer addAnimation:transition forKey:nil];
    handwritingView.hidden = NO;
    
}

- (void) fadeInBlurredImage{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.blurredImageView.layer addAnimation:transition forKey:nil];
    self.blurredImageView.hidden = NO;
    
}

-(void) hideBluredThings{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.blurredImageView.layer addAnimation:transition forKey:nil];
    self.blurredImageView.hidden = YES;
    
    self.todoTableView.userInteractionEnabled = YES;
    
    [self viewWillAppear:YES];
}

@end

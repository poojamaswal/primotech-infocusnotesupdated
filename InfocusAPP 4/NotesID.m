//
//  NotesID.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "NotesID.h"
#import "DrawingData.h"
#import "ImageData.h"
#import "DatabaseManager.h"
#import "TextData.h"
#import "StickyData.h"
#import "DrawingDefaults.h"



@implementation NotesID

// Insert code here to add functionality to your managed object subclass
// Insert code here to add functionality to your managed object subclass
-(void)saveDrawingInfo:(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name
{
    
    
    DrawingData *aDrawingData = [DrawingData addDrawingData:dict withContext:context];
    aDrawingData.notesID = (id) self;
    
    //    self.drawingData = aDrawingData;
    //    [self addDrawingDataObject:aDrawingData];
    [context save:nil];
}

- (void)saveImageInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name
{
    ImageData *aImageData = [ImageData addImageData:dict withContext:context];
    aImageData.notesID = (id)self;
    
    [context save:nil];
    //    PersonalInfo *aProfessionalInfo = [PersonalInfo addPersonalInfo:dict withContext:context];
    //    aProfessionalInfo.userID = self;
    //    [self addProfessionInfoObject:aProfessionalInfo];
    //    [context save:nil];
}

- (void)saveTextInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name
{
    TextData *aTextData = [TextData addTextData:dict withContext:context];
    aTextData.notesID = (id)self;
    //    self.textData = aTextData;
    [context save:nil];
}
- (void)saveStickyInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name
{
    StickyData *aStickyData = [StickyData addStickyData:dict withContext:context];
    aStickyData.notesID = (id)self;
    //    self.stickyData = aStickyData;
    [context save:nil];
}
+ (NotesID*) createPage:(NSManagedObjectContext*)context withName:(NSString*)pageID withProjectID:(NSInteger)project_ID
{
    NSEntityDescription *desc = [NSEntityDescription entityForName:@"NotesID"
                                            inManagedObjectContext:context];
    NotesID *nm = [[NotesID alloc] initWithEntity:desc
                   insertIntoManagedObjectContext:nil];
    nm.pageID = [NSString stringWithString:pageID];
    nm.subNotesCount = [NSNumber numberWithInt:[DrawingDefaults sharedObject].intNotesSubNotesCount];
    
    
    nm.projectID=[NSNumber numberWithInteger:project_ID];
    
    
    [context insertObject:nm];
    
    return nm;
}

+ (BOOL)userAlReadyExist:(NSString*)name
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    BOOL exists = NO;
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        if ([nm.pageID isEqualToString:name])
        {
            exists = YES;
            break;
        }
    }
    
    return exists;
}

+ (NotesID*) changeValue:(NSString*)changeValue from:(NSString*)fromvalue
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:@"NotesID"];
    NSError *error = nil;
    NSArray *array = [[DatabaseManager sharedManager].managedObjectContext executeFetchRequest:fetch error:&error];
    
    for (int i = 0; i< [array count]; i++)
    {
        NotesID *nm = [array objectAtIndex:i];
        if ([nm.pageID isEqualToString:fromvalue])
        {
            nm.subNotesCount = [NSNumber numberWithInt:[DrawingDefaults sharedObject].intNotesSubNotesCount];
            nm.pageID = changeValue;
            return nm;
        }
    }
    
    return nil;
}
@end

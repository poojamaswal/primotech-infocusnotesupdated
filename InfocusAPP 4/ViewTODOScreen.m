//
//  ViewTODOScreen.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 7/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewTODOScreen.h"
#import "ToDoViewController.h"
//#import "AdWhirlView.h"
#import "GetAllDataObjectsClass.h"


@interface ViewTODOScreen()


@property(nonatomic, strong) NSString   *alertString;//Steve
@property(nonatomic, strong) NSString   *alertSecondString;//Steve

@end


@implementation ViewTODOScreen
@synthesize toDoDataObj;
//@synthesize adView;

-(id)initWithToDoObject:(TODODataObject *) appObj
{
    if (IS_IPAD)
    {
    self=[super initWithNibName:@"ViewToDoScreenIPad" bundle:[NSBundle mainBundle]];
        self.toDoDataObj = appObj;
    }
    else if (self = [super initWithNibName:@"ViewTODOScreen" bundle:[NSBundle mainBundle]])
    {
		self.toDoDataObj = appObj;
        //NSLog(@"---->%@",appObj.fullprojPath);


       if(appObj.projID && !appObj.fullprojPath)
        {
            myArray=[[NSMutableArray alloc]init ];
            y=[self fetch_level_one:appObj.projID];
            yy=Pname;
            [myArray addObject:yy];
            if((y)&&(y!=0)) 
            {
                x= [self fetch_level_one:y];
                xx=Pname;
                [myArray addObject:xx];
                if((x)&&(x!=0)) 
                { 
                    u=[self fetch_level_one:x];
                    uu=Pname;
                    [myArray addObject:uu];
                    if((u)&&(u!=0)) 
                    { 
                        v= [self fetch_level_one:u];
                        vv=Pname;
                        [myArray addObject:vv];
                    }
                }
            }
        
        // Append All folder in A String By Alok 
        NSString *finalText=[[NSString alloc]init];   
        for(int k=[myArray count] - 1;k>=0; k--)
        {
            finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
        }
        
        if ( [finalText length] > 0)
            finalText = [finalText substringToIndex:[finalText length] - 1];
        
        [self.toDoDataObj setFullprojPath:finalText];
                 [myArray removeAllObjects];
     
        }
        
          }
       
    //NSLog(@"toDoDataObj.projPrntID-->%d",self.toDoDataObj.projPrntID);
    //NSLog(@"toDoDataObj.startDateTime-->%@",self.toDoDataObj.startDateTime);

    
	return self;
}
#pragma -
#pragma  Fetch Level Name
#pragma -


-(NSInteger)fetch_level_one:(NSInteger)projid
{
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    if (selected_stmt == nil && appDelegate.database)
    {
        NSString *  Sql = [[NSString alloc]initWithFormat:@"SELECT  belongstoId,projectName from ProjectMaster where projectID =%d",projid];
        // NSLog(@"%@",Sql);
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW) 
            {
                retVal= sqlite3_column_int(selected_stmt, 0);
			    Pname= [NSString stringWithUTF8String:(char *) sqlite3_column_text(selected_stmt,1)];
                // NSLog(@"%d   =  %@",retVal,Pname);
                           
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    return retVal;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Steve - Convert Alert string to proper format
    
    //NSLog(@"self.toDoDataObj.alert-->%@",self.toDoDataObj.alert);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    if (![self.toDoDataObj.alert isEqualToString:@"None"]) {
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        NSDate *alertDate = [dateFormatter dateFromString: self.toDoDataObj.alert];
        
        //NSLog(@" alertDate = %@", alertDate);
        
        
        if (alertDate == nil)
        {
            
            //Steve - converts it if US Format. Note "," after yyyy,
            [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
            alertDate =[dateFormatter dateFromString:self.toDoDataObj.alert];
            
            //NSLog(@" alertDate = %@", alertDate);
            
            
            if (alertDate != nil){
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                
                _alertString = [dateFormatter stringFromDate:alertDate];
                
               // NSLog(@" _alertString = %@", _alertString);
                
            }
            else { //Another attempt at fixing it
                
                //Steve - converts it if US Format. Note NO "," after yyyy
                [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
                alertDate =[dateFormatter dateFromString:[self.toDoDataObj alert]];
                
                
                //NSLog(@" alertDate = %@", alertDate);
                
                
                if (alertDate != nil){
                    
                    [dateFormatter setLocale:[NSLocale currentLocale]];
                    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                    
                   _alertString = [dateFormatter stringFromDate:alertDate];
                    
                      //NSLog(@" _alertString = %@", _alertString);
                }
                
            }
            
        }
        else{ //alert_1_Date is valid
            
            [dateFormatter setLocale:[NSLocale currentLocale]];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            
            _alertString = [dateFormatter stringFromDate:alertDate];

              //NSLog(@" _alertString = %@", _alertString);
            
        }

    }
    
    
    
    
    
}


-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
    

    navBar.hidden = YES;
    backButton.hidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.title =@"Details";
    self.view.autoresizingMask = NO;
    self.view.autoresizesSubviews = NO;
    
    
    
    if (IS_IOS_7) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        
        self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone //
        
        self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        toDoDetailTableView.backgroundColor = [UIColor whiteColor];
        
        //Edit Button
        EditButton.hidden = YES;
        
        UIBarButtonItem *editButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButton_Clicked:)];
        self.navigationItem.rightBarButtonItem = editButton_iOS7;
        
        
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        
        ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
        
        if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            
            editButton_iOS7.tintColor = [UIColor whiteColor];
            
        }
        else{ //Dark Theme
            
            
            //Steve
            if(IS_IPAD)
            {
                
                self.edgesForExtendedLayout = UIRectEdgeNone;
                
                
                // Update  (Pooja Changes)
                self.navigationController.navigationBarHidden=NO;
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
                
                navBar.hidden=YES;
                
                
                //self.cancelButton_iPad.tintColor=[UIColor whiteColor];
                self.navigationItem.title=@"Event Details";
                
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
                
                editButton_iOS7.tintColor = [UIColor whiteColor];
                
            }
            else{ //iPhone
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                editButton_iOS7.tintColor = [UIColor whiteColor];
            }
            
        }
        
        ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
        if (IS_IPAD)
        {
            shareButton.frame = CGRectMake(0, self.view.frame.size.height - 60, 768, 50);
            [self.view addSubview:shareButton];
        }
        else //iPhone
        {
            
            UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
            [toDoDetailTableView setContentInset:inset];
            
            if (IS_IPHONE_5)
            {
                
                shareButton.frame = CGRectMake(124, 380 + 88 + 64, 73, 31);
                toDoDetailTableView.frame = CGRectMake(0, 0, 320, 435 + 88);
            }
            else {
                
                shareButton.frame = CGRectMake(124, 380 + 30, 73, 31 + 64);
                toDoDetailTableView.frame = CGRectMake(0, 0, 320, 435);
                
                //NSLog(@"herererererer");
            }
            
        }

        [self.navigationController.navigationBar addSubview:EditButton];
        
        
    }
    else{ //iOS 6
        
        
        
        //NSLog(@"i m here with ios 6");
        
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        //self.view.backgroundColor = [UIColor purpleColor];
        //toDoDetailTableView.backgroundColor = [UIColor purpleColor];
        
        if (IS_IPHONE_5) {
            
            shareButton.frame = CGRectMake(124, 417 + 88 - 44, 73, 31);
            toDoDetailTableView.frame = CGRectMake(0, 10, 320, 493 - 44);
        }
        else {
            
            shareButton.frame = CGRectMake(124, 417 - 44, 73, 31);
            toDoDetailTableView.frame = CGRectMake(0, 10, 320, 405 - 44);
        }
        
        
        [self.navigationController.navigationBar addSubview:EditButton];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
    }
    

    
    //NSLog(@"share = %@",NSStringFromCGRect(shareButton.frame));
    //NSLog(@"tablview = %@",NSStringFromCGRect(toDoDetailTableView.frame));
    
    //NSLog(@"self.view.frame = %@",NSStringFromCGRect(self.view.frame));
    //NSLog(@"self.view.bounds = %@",NSStringFromCGRect(self.view.bounds));

    

	[toDoDetailTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    
    //STeve - TEST
    //toDoDetailTableView.backgroundColor = [UIColor yellowColor];
    //self.view.backgroundColor = [UIColor redColor];
}


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}



#pragma mark -
#pragma mark Button Clicked
#pragma mark -

-(IBAction)backButton_Clicked:(id) sender {
    
    //Steve
    if (IS_IPAD) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else{ //iPhone
        [self.navigationController popViewControllerAnimated:YES];
    }
	
}


-(IBAction)editButton_Clicked:(id) sender {
    
    //Steve
    if (IS_IPAD) {
        
        [self performSegueWithIdentifier: @"ToDoViewController" sender: self];
    }
    else{ //iPhone
        
        ToDoViewController *toDoViewController = [[ToDoViewController alloc] initWithToDoDataObject:self.toDoDataObj];
        toDoViewController.prntVAL = self.toDoDataObj.projPrntID;
        [self.navigationController pushViewController:toDoViewController animated:YES];
        toDoViewController.isAdd = NO;
    }

}


//Steve - iPad Storyboard
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"ToDoViewController"])
    {
        ToDoViewController *toDoViewController = (ToDoViewController*)segue.destinationViewController;
        toDoViewController.aToDoObj = self.toDoDataObj;
        toDoViewController.prntVAL = self.toDoDataObj.projPrntID;
        toDoViewController.isAdd = NO;
        toDoViewController.title = @"Edit To Do";
    }
    
}


-(void)	starred_buttonClicked:(id)sender
{
	if (toDoDataObj.isStarred==YES) 
	{
		toDoDataObj.isStarred = NO;
  if([AllInsertDataMethods updateToDoStarred:toDoDataObj])
        [starButton setImage:[UIImage imageNamed:@"IconStarred-a.png"] forState:UIControlStateNormal];
        
	}
	else 
	{
		toDoDataObj.isStarred = YES;
     if([AllInsertDataMethods updateToDoStarred:toDoDataObj])
        [starButton setImage:[UIImage imageNamed:@"IconStarred.png"] forState:UIControlStateNormal];
	}
}
#pragma mark -
#pragma mark Tabel View DataSource & Delegate Methods
#pragma mark -


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IOS_7) {
        
        cell.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        tableView.backgroundColor = [UIColor whiteColor];
    }
    else{
        
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f] ] ;
        [cell setBackgroundColor:[UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f] ] ;
        tableView.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    CGFloat height;
    
    if (IS_IOS_7) {
        
        height = 1;
    }
    else{
        
        height = 1;
    }
    
    return height;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%s",__FUNCTION__);

	int rows = 6;
	
	if (toDoDataObj.projID == 0) {
		rows -= 1;
	}
	if (toDoDataObj.priority == nil || [toDoDataObj.priority isEqualToString:@""] ) {
		rows -= 1;
	}
	if (toDoDataObj.toDoProgress == 0 ) {
		rows -= 1;
	}
    if (toDoDataObj.alert == nil || [toDoDataObj.alert isEqualToString:@"None"]) {
		rows -= 1;
	}
	numberOfRows = rows;
    //NSLog(@"Number of rows = %d",numberOfRows);
    
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"%s",__FUNCTION__);
    
    NSInteger section = 0;
	if ([indexPath row] < 2 || numberOfRows == 6) {
		section = [indexPath row];
	}
    
    else {
		if (numberOfRows == 3) {
			if (toDoDataObj.projID != 0) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0)  {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;
                
                
            }
		}else if (numberOfRows == 4) {
			if (toDoDataObj.projID != 0 ) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0)  {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]  && indexPath.row == numberOfRows-1) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;
                
                
            }
		}
        
        else if (numberOfRows == 5 ) {
			if (toDoDataObj.projID != 0  && indexPath.row ==2 ) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0)  {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]  && indexPath.row == numberOfRows-2) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;

            
            }
		}
	}
    switch (section) {
		case 0: {
			if ([indexPath row] == 0) {
                if ([toDoDataObj.todoTitleType isEqualToString:@"H"]) {
                    return 150; 
                }
                else {
                    //NSLog(@"%@",toDoDataObj.todoTitle);
                    CGSize strSize = [toDoDataObj.todoTitle sizeWithFont:[UIFont boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
                    return strSize.height + 30;
                }
            }
		}
			break;
		case 1: {
			 
				return 70;
			
		}
			break;
            
		case 2: {
			if (toDoDataObj.projID != 0)
            {
                return 70;
            }
            else
            {
                return 0; 
            } 
		}
			break;
		case 3: {
            if ((toDoDataObj.toDoProgress != 0) || ![toDoDataObj.priority isEqualToString:@"0 % Completed"]) 
            {
                return 70;
            }
            else
            {
                return 0; 
            }

                        
		}
			break;
		case 4: {
            	
            return 70;
         if (toDoDataObj.priority != nil || [toDoDataObj.priority isEqualToString:@"None"])
         {
             return 70;
            
         
        }
            else
            {
            
                return 0;
            }
        }
			break;
		case 5: {
            
            

			if (![toDoDataObj.alert isEqualToString:@"None"])
            {
                return 70;
            }
            else
            {
                return 70; 
            }

		}
			break;
		default:
			return 70; 
			break;
	}

    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	
    //Steve - fixes bug in iOS 7 where cell content don't show
    /*
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	}
     */
    
	NSInteger section = 0;
    
    
    //NSLog(@"indexPath.row = %d", indexPath.row);
    
    
	
	if ([indexPath row] <2 || numberOfRows == 6) {
		section = [indexPath row];
	}else {
		if (numberOfRows == 3) {
			if (toDoDataObj.projID != 0) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0) {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;
                
                
            }
		}else if (numberOfRows == 4) {
			if (toDoDataObj.projID != 0 && indexPath.row == 2) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0 && indexPath.row != 3) {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]  && indexPath.row == ([tableView numberOfRowsInSection:0] -1)) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;
                
                
            }
		}
        
        else if (numberOfRows == 5) {
			if (toDoDataObj.projID != 0 && indexPath.row == 2) {
				section = 2;
			}else if (toDoDataObj.toDoProgress != 0 && indexPath.row ==3) {
				section = 3;
			}else if (toDoDataObj.priority != nil && ![toDoDataObj.priority isEqualToString:@""]  &&indexPath.row==4) {
				section = 4;
			}
            else if (![toDoDataObj.alert isEqualToString:@""]||![toDoDataObj.alert isEqualToString:@"None"])
            {
                section = 5;
                
                
            }
		}
	}
	
	switch (section) {
		case 0: {
			//			IconStarred
            UIImage *staredImage;
            
			if (toDoDataObj.isStarred) 
			{
				staredImage  = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"IconStarred" ofType:@"png"]];
			} else 
			{
				staredImage  = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"IconStarred-a" ofType:@"png"]];
			}
			
			starButton = [[UIButton alloc] init]; 
			[starButton setImage:staredImage forState:UIControlStateNormal];
            [starButton addTarget:self action:@selector(starred_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
			[cell.contentView addSubview:starButton];
			
			if ([toDoDataObj.todoTitleType isEqualToString:@"H"]) 
			{
                
				UIImage *titleImage = [[UIImage alloc ] initWithData:toDoDataObj.todoTitleBinaryLarge];
				UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, 290 - staredImage.size.width - 5, 130)];
				[titleImageView setImage:titleImage forState:UIControlStateNormal];
				[titleImageView setImage:titleImage forState:UIControlStateHighlighted];
				[cell.contentView addSubview:titleImageView];
				
                if(IS_IPAD)
                    [starButton setFrame:CGRectMake(748 - staredImage.size.width - 5, 150/2 - staredImage.size.height/2, staredImage.size.width, staredImage.size.height)];
                else
                    [starButton setFrame:CGRectMake(300 - staredImage.size.width - 5, 150/2 - staredImage.size.height/2, staredImage.size.width, staredImage.size.height)];
			}
			else 
			{
                
				CGSize strSize = [toDoDataObj.todoTitle sizeWithFont:[UIFont boldSystemFontOfSize:16] 
												   constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
				
				UILabel *titleLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 15, 290 - staredImage.size.width - 5, strSize.height + 5)];
				[titleLabel setText:toDoDataObj.todoTitle];
				[titleLabel setFont:[UIFont systemFontOfSize:16]];
                titleLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
				[titleLabel setTextAlignment:NSTextAlignmentLeft];
				[titleLabel setBackgroundColor:[UIColor clearColor]];
				[titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
				[titleLabel setNumberOfLines:0];
				[cell.contentView addSubview:titleLabel];
                
                if(IS_IPAD)
                    [starButton setFrame:CGRectMake(748 - staredImage.size.width - 5, (strSize.height + 30) / 2 - staredImage.size.height/2, staredImage.size.width, staredImage.size.height)];
                else
                    [starButton setFrame:CGRectMake(300 - staredImage.size.width - 5, (strSize.height + 30) / 2 - staredImage.size.height/2, staredImage.size.width, staredImage.size.height)];
			}
		}
			break;
		case 1: {
			
			UILabel *strtDateEndDateCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 3, 290, 18)];
			[strtDateEndDateCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
			[strtDateEndDateCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[strtDateEndDateCaptionLabel setText:@"Start Date & End Date"];
			[cell.contentView  addSubview:strtDateEndDateCaptionLabel];
			[strtDateEndDateCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[strtDateEndDateCaptionLabel  setTextColor:[UIColor blackColor]];
			[cell.contentView  addSubview:strtDateEndDateCaptionLabel];
			
			UILabel *strtDateLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, strtDateEndDateCaptionLabel.frame.origin.y + strtDateEndDateCaptionLabel.frame.size.height + 3, 290, 18)];
			[strtDateLabel setFont:[UIFont systemFontOfSize:16]];
			[strtDateLabel setTextAlignment:NSTextAlignmentLeft];
			[strtDateLabel  setBackgroundColor:[UIColor clearColor]];
			//[strtDateLabel  setTextColor:[UIColor blueColor]];
            strtDateLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:strtDateLabel];
			
			
			UILabel *endDateLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, strtDateLabel.frame.origin.y + strtDateLabel.frame.size.height +3, 290, 18)];
			[endDateLabel setFont:[UIFont systemFontOfSize:16]];
			[endDateLabel setTextAlignment:NSTextAlignmentLeft];
			[endDateLabel  setBackgroundColor:[UIColor clearColor]];
			//[endDateLabel  setTextColor:[UIColor blueColor]];
            endDateLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:endDateLabel];
			
			
			NSDateFormatter *formate = [[NSDateFormatter alloc] init];
			[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            //Steve - finds "None" stard/End dates
            if ( [toDoDataObj.startDateTime isEqual:@"None"] || [toDoDataObj.dueDateTime isEqual:@"None"] ) {
                [strtDateLabel setText:[toDoDataObj startDateTime]];
                [endDateLabel setText:[toDoDataObj dueDateTime]];
            }
            else
            {
                NSDate *strtDate = [formate dateFromString:[toDoDataObj startDateTime]];
                NSDate *endDate = [formate dateFromString:[toDoDataObj dueDateTime]];
                
                [formate setDateFormat:@"yyyy-MM-dd"];
                
                if ([[formate dateFromString:[[strtDate description] substringToIndex:10]] compare:  [formate dateFromString:[[endDate description] substringToIndex:10]]] == NSOrderedSame) {
                    
                    [formate setDateStyle:NSDateFormatterFullStyle];
                    [strtDateLabel setText:[formate stringFromDate:strtDate]];
                    [endDateLabel setText:[formate stringFromDate:endDate]];
                    
                }else {
                    [formate setDateStyle:NSDateFormatterFullStyle];
                    [strtDateLabel setText:[formate stringFromDate:strtDate]];
                    [endDateLabel setText:[formate stringFromDate:endDate]];
                    
                }

            }
			
		
						
		}	
			break;			
		case 2:{
            if (!([toDoDataObj.fullprojPath isEqualToString:@""]||[toDoDataObj.fullprojPath isEqualToString:@"None"]))
            {
			UILabel *projectCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
			[projectCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
			[projectCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[projectCaptionLabel setText:@"Project"];
			[cell.contentView  addSubview:projectCaptionLabel];
			[projectCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[projectCaptionLabel  setTextColor:[UIColor blackColor]];
			[cell.contentView  addSubview:projectCaptionLabel];
			
			UILabel *projectLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, projectCaptionLabel.frame.origin.y + projectCaptionLabel.frame.size.height + 3, 290, 25)];
			[projectLabel setFont:[UIFont systemFontOfSize:16]];
			[projectLabel setTextAlignment:NSTextAlignmentLeft];
                [projectLabel setText:toDoDataObj.fullprojPath];
			[projectLabel setBackgroundColor:[UIColor clearColor]];
			//[projectLabel setTextColor:[UIColor blueColor]];
            projectLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:projectLabel];
			}
		}
			break;
		case 3: {
			UILabel *progressCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
			[progressCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
			[progressCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[progressCaptionLabel setText:@"Progress"];
			[cell.contentView  addSubview:progressCaptionLabel];
			[progressCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[progressCaptionLabel  setTextColor:[UIColor blackColor]];
			[cell.contentView  addSubview:progressCaptionLabel];
			
			UILabel *progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, progressCaptionLabel.frame.origin.y + progressCaptionLabel.frame.size.height + 3, 290, 25)];
			[progressLabel setFont:[UIFont systemFontOfSize:16]];
			[progressLabel setTextAlignment:NSTextAlignmentLeft];
			[progressLabel setText:[NSString stringWithFormat:@"%d %% Completed", toDoDataObj.toDoProgress]];
			[progressLabel setBackgroundColor:[UIColor clearColor]];
			//[progressLabel setTextColor:[UIColor blueColor]];
            progressLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:progressLabel];
			
		}
			break;
		case 4: {
            
			UILabel *priorityCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
			[priorityCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
			[priorityCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[priorityCaptionLabel setText:@"Priority"];
			[cell.contentView  addSubview:priorityCaptionLabel];
			[priorityCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[priorityCaptionLabel  setTextColor:[UIColor blackColor]];
			[cell.contentView  addSubview:priorityCaptionLabel];
			
			UILabel *priorityLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, priorityCaptionLabel.frame.origin.y + priorityCaptionLabel.frame.size.height + 3, 290, 25)];
			[priorityLabel setFont:[UIFont systemFontOfSize:16]];
			[priorityLabel setTextAlignment:NSTextAlignmentLeft];
			[priorityLabel setText:toDoDataObj.priority];
			[priorityLabel setBackgroundColor:[UIColor clearColor]];
			//[priorityLabel setTextColor:[UIColor blueColor]];
            priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:priorityLabel];
			
		}	
			break;
            
        case 5: {
            
            UILabel *alertLabel;
            if (![toDoDataObj.alert isEqualToString:@"None"])
            {
                UILabel *alertCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 4, 290,18)];
                [alertCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
                [alertCaptionLabel setTextAlignment:NSTextAlignmentLeft];
                [alertCaptionLabel setText:@"Alert"];
                [cell.contentView  addSubview:alertCaptionLabel];
                [alertCaptionLabel setBackgroundColor:[UIColor clearColor]];
                [alertCaptionLabel  setTextColor:[UIColor blackColor]];
                
                
                //Steve - convert format of Alert string
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                NSDate *alertDate = [dateFormatter dateFromString:toDoDataObj.alert];
                
                
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                NSString *alertString = [dateFormatter stringFromDate:alertDate];
                
                
                alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertCaptionLabel.frame.origin.y + alertCaptionLabel.frame.size.height+2, 290, 18)];
                [alertLabel setFont:[UIFont systemFontOfSize:16]];
                [alertLabel setTextAlignment:NSTextAlignmentLeft];
                //[alertLabel setText:toDoDataObj.alert];
                [alertLabel setText:alertString];
                [alertLabel setBackgroundColor:[UIColor clearColor]];
                //[alertLabel setTextColor:[UIColor blueColor]];
                alertLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
                
                [cell.contentView  addSubview:alertLabel];
            }
        
            ///Anil's added///////
            if (![toDoDataObj.alertSecond isEqualToString:@"None"]){
                
                //Steve - convert format of Alert string
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                NSDate *alertDate = [dateFormatter dateFromString:toDoDataObj.alertSecond];
                
                [dateFormatter setLocale:[NSLocale currentLocale]];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                NSString *alertString = [dateFormatter stringFromDate:alertDate];
                
                
                UILabel *alertSecondLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertLabel.frame.origin.y + alertLabel.frame.size.height, 290, 25)];
                [alertSecondLabel setFont:[UIFont systemFontOfSize:16]];
                [alertSecondLabel setTextAlignment:NSTextAlignmentLeft];
                //[alertSecondLabel setText:toDoDataObj.alertSecond];
                [alertSecondLabel setText:alertString]; //Steve
                [alertSecondLabel setBackgroundColor:[UIColor clearColor]];
                //[alertSecondLabel setTextColor:[UIColor blueColor]];
                alertSecondLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
                [cell.contentView  addSubview:alertSecondLabel];
                ///Anil's added///////
                
			}
            
            
		}
			break;
   
            
		default:
			break;
	}
    
    /*
    if (IS_IOS_7) {
        
        cell.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else{
        
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
     */
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark Share
#pragma mark -

//Steve added
-(IBAction)shareButton_Clicked:(id) sender {
    
    
    
    if ([MFMailComposeViewController canSendMail])
	{
        
        if (IS_IOS_7) {


            UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
            UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            
        }
        else{
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
        
        
        NSString *dateString = @"";
        
        MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
        [mailCntrlr setMailComposeDelegate:self];
        
        NSDateFormatter *formate = [[NSDateFormatter alloc] init];
        [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSDate *strtDate = [formate dateFromString:[toDoDataObj startDateTime]];
        NSDate *endDate = [formate dateFromString:[toDoDataObj dueDateTime]];
        
        [formate setDateFormat:@"yyyy-MM-dd"];
        [formate setDateStyle:NSDateFormatterFullStyle];
        
        NSString *startDate_String = [formate stringFromDate:strtDate];
        NSString *endDate_String = [formate stringFromDate:endDate];
        
        
        if (startDate_String == nil) {
            startDate_String = @"None";
        }
        
        if (endDate_String == nil) {
            endDate_String = @"None";
        }
        
        //NSLog(@" startDate_String = %@", startDate_String);
        //NSLog(@" endDate_String = %@", endDate_String);

        
        
        
        if (toDoDataObj.projPrntID != 0) {
            dateString = [dateString stringByAppendingFormat:@"<p> <b>Project:</b> <BR> %@ </p>", toDoDataObj.projectName];
        }
        
        
        
        dateString = [dateString stringByAppendingFormat:@"<p> <b>Start Date:</b> <BR> %@</p>  <p> <b>Due Date:</b> <BR> %@ </p>",
                     startDate_String, endDate_String];
        
        

    	if ( !(toDoDataObj.priority == nil) && ![toDoDataObj.priority isEqualToString:@""] ) {
            dateString = [dateString stringByAppendingFormat:@"<p> <b>Priority:</b> <BR> %@ </p>", toDoDataObj.priority];
        }
        
        if ( !(toDoDataObj.toDoProgress == 0)) {
            NSString *todoProgressString = [NSString stringWithFormat:@"%d %% Completed", toDoDataObj.toDoProgress];
            dateString = [dateString stringByAppendingFormat:@"<p> <b>Progress:</b> <BR> %@ </p>", todoProgressString];
        }
        
        
        
        NSString *body;
        
        
        if ([[toDoDataObj todoTitleType] isEqualToString:@"H"]  && toDoDataObj.todoTitleBinaryLarge != nil) {
            [mailCntrlr setSubject:@"Shared Event"];
            [mailCntrlr addAttachmentData:[toDoDataObj todoTitleBinaryLarge] mimeType:@"image/jpeg" fileName:@"Shared Event"];
            body = dateString;
        }else {
            [mailCntrlr setSubject:[toDoDataObj todoTitle]];
            body = @"<b>Task:</b><BR>";
            body = [body stringByAppendingString:[toDoDataObj todoTitle]];
            body = [body stringByAppendingFormat:@"%@", dateString];
        }
        
        
        //Steve - Infocus Pro Link
        NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
        body = [body stringByAppendingString:bodyURL];
        
        
        [mailCntrlr setMessageBody:body isHTML:YES];
        [self presentViewController:mailCntrlr animated:YES completion:nil];
        
    }
    else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark -
#pragma mark Memory management
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [backButton removeFromSuperview];
        [EditButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    shareButton = nil;
    backButton = nil;
    EditButton = nil;
    [super viewDidUnload];
}




@end

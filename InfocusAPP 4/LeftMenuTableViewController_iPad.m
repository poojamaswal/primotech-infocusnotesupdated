//
//  LeftMenuTableViewController_iPad.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/9/15.
//
//

#import "LeftMenuTableViewController_iPad.h"
#import "ToDoParentController.h"

@interface LeftMenuTableViewController_iPad ()

@property (nonatomic, assign) int selectedButton;

@end



@implementation LeftMenuTableViewController_iPad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor=[UIColor whiteColor];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    //self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor]; 
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:18.0/255.0 blue:43.0/255.0 alpha:1.0f]; //Steve
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    
    self.selectedButton = 0;
    [self sentDataWithSelectedIndex:0];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Views"; //Steve
}

//Steve
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *sectionLabel = [[UILabel alloc] init];
    sectionLabel.frame = CGRectMake(2, 2, 320, 15);
    sectionLabel.textColor = [UIColor whiteColor];
    sectionLabel.font = [UIFont boldSystemFontOfSize:14]; //boldSystemFontOfSize
    sectionLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    //Header View Background color
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:56.0/255.0 blue:140.0/255.0 alpha:1.0f];
    [headerView addSubview:sectionLabel];
    
    return headerView;
}

//Steve
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 21;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 10;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tableView.backgroundColor = [UIColor whiteColor];
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //Steve - Clears all subviews so line separator doesn't overlap each time tableview is called
    for (UIView  *subViews in [cell.contentView subviews])
    {
        [subViews removeFromSuperview];
    }
    
    /*
     SelectedButtonIndex:
     1 = Today
     2 = Projects
     3 = Starred
     4 = Overdue
     5 = Priority
     6 = Due Date
     7 = Progress
     8 = Done
     9 = Created
     100 = All
     */
    
    
     NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    int selectedButtonIndex = [sharedDefaults integerForKey:@"DefaultSelectedButtonIndex"];
    
    if (selectedButtonIndex == 0) {
        selectedButtonIndex = 1;
    }
    
    
//****Customize Cell Background Color
    //UIColor *backgroundColorCell = [UIColor colorWithRed:153.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:0.35f];
    UIColor *backgroundColorCell = [UIColor colorWithRed:15.0f/255.0f green:83.0f/255.0f blue:191.0f/255.0f alpha:0.5f];
    UIColor *backgroundColorNormalCell = [UIColor whiteColor];
    
    tableView.backgroundColor = [UIColor whiteColor];
    
    
    //Steve - cusomt separator color
    UIView *separatorView = [[UIView alloc]init];
    separatorView.backgroundColor =  [UIColor colorWithRed:126.0f/255.0f green:172.0f/255.0f blue:255.0f/255.0f alpha:0.25f];
    separatorView.frame = CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1);
    [cell.contentView addSubview:separatorView];
    
    
    
    if (indexPath.row == 0 && selectedButtonIndex != 1) {
        
        cell.textLabel.text = @"Today";
        cell.backgroundColor = backgroundColorNormalCell;
        
    }
    else if (indexPath.row == 0 && selectedButtonIndex == 1) {
        
        cell.textLabel.text = @"Today";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 1 && selectedButtonIndex != 2) {
        
        cell.textLabel.text = @"Projects";
        cell.backgroundColor = backgroundColorNormalCell;
       
    }
    else if (indexPath.row == 1 && selectedButtonIndex == 2) {
        
        cell.textLabel.text = @"Projects";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 2 && selectedButtonIndex != 3) {
        
        cell.textLabel.text = @"Starred";
        cell.backgroundColor = backgroundColorNormalCell;
   
    }
    else if (indexPath.row == 2 && selectedButtonIndex == 3) {
        
        cell.textLabel.text = @"Starred";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
       [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 3 && selectedButtonIndex != 4) {
        
        cell.textLabel.text = @"Over Due";
        cell.backgroundColor = backgroundColorNormalCell;
        
    }
    else if (indexPath.row == 3 && selectedButtonIndex == 4) {
        
        cell.textLabel.text = @"Over Due";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
       [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 4 && selectedButtonIndex != 5) {
        
        cell.textLabel.text = @"Priority";
        cell.backgroundColor = backgroundColorNormalCell;
        
    }
    else if (indexPath.row == 4 && selectedButtonIndex == 5) {
        
        cell.textLabel.text = @"Priority";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 5 && selectedButtonIndex != 6) {
        
        cell.textLabel.text = @"Due Date";
        cell.backgroundColor = backgroundColorNormalCell;
        
    }
    else if (indexPath.row == 5 && selectedButtonIndex == 6) {
        
        cell.textLabel.text = @"Due Date";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 6 && selectedButtonIndex != 7) {
        
        cell.textLabel.text = @"Progress";
        cell.backgroundColor = backgroundColorNormalCell;
       
    }
    else if (indexPath.row == 6 && selectedButtonIndex == 7) {
        
        cell.textLabel.text = @"Progress";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 7 && selectedButtonIndex != 8) {
        
        cell.textLabel.text = @"Done";
        cell.backgroundColor = backgroundColorNormalCell;
       
    }
    else if (indexPath.row == 7 && selectedButtonIndex == 8) {
        
        cell.textLabel.text = @"Done";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 8 && selectedButtonIndex != 9) {
        
        cell.textLabel.text = @"Created";
        cell.backgroundColor = backgroundColorNormalCell;
        
    }
    else if (indexPath.row == 8 && selectedButtonIndex == 9) {
        
        cell.textLabel.text = @"Created";
        selectedButton = selectedButtonIndex;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }
    else if (indexPath.row == 9 && selectedButtonIndex != 100) {
        
        cell.textLabel.text = @"All";
        cell.backgroundColor = backgroundColorNormalCell;
       
    }
    else if (indexPath.row == 9 && selectedButtonIndex == 100) {
        
        cell.textLabel.text = @"All";
        selectedButton = 100;
        cell.backgroundColor = backgroundColorCell;
        
        [self sentDataWithSelectedIndex:selectedButton];
        
    }

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     SelectedButtonIndex:
     1 = Today
     2 = Projects
     3 = Starred
     4 = Overdue
     5 = Priority
     6 = Due Date
     7 = Progress
     8 = Done
     9 = Created
     100 = All
     */
    
    if (indexPath.row == 9){
        selectedButton = 100;
    }
    else{
        selectedButton = indexPath.row + 1;
    }
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setInteger:selectedButton forKey:@"DefaultSelectedButtonIndex"];
    //NSLog(@"%d",selectedButton);
    
    [self sentDataWithSelectedIndex:selectedButton];
    
    [tableView reloadData];
    
}


-(void)sentDataWithSelectedIndex:(int)selectedindex

{
    
    OrganizerAppDelegate *app_Delegate=(OrganizerAppDelegate*)[[UIApplication sharedApplication]delegate];
    UISplitViewController *splitVC=[[UISplitViewController alloc]init];
    splitVC=app_Delegate.splitVCToDo;
    
    ToDoParentController *toDoParentView=[[ToDoParentController alloc]initWithNibName:@"ToDoParentControllerIPad" bundle:[NSBundle mainBundle]];
     UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:toDoParentView];
    
    toDoParentView.selectedButtonIndex = selectedButton;
    //NSLog(@"toDoParentView.selectedButtonIndex = %d",toDoParentView.selectedButtonIndex);
    
    app_Delegate.splitVCToDo.viewControllers = @[[app_Delegate.splitVCToDo.viewControllers objectAtIndex:0],nav];
    app_Delegate.splitVCToDo.delegate = [app_Delegate.splitVCToDo.viewControllers objectAtIndex:0];
    app_Delegate.splitVCToDo.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    
    
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  StickyData.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NotesID;

NS_ASSUME_NONNULL_BEGIN

@interface StickyData : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+(StickyData*)addStickyData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context;


@end

NS_ASSUME_NONNULL_END

#import "StickyData+CoreDataProperties.h"

//
//  StickyData+CoreDataProperties.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StickyData.h"

NS_ASSUME_NONNULL_BEGIN

@interface StickyData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *stickyCoordinates;
@property (nullable, nonatomic, retain) NSString *stickyText;
@property (nullable, nonatomic, retain) NSSet<NotesID *> *notesID;
@property (nullable, nonatomic, retain) NSDate *savedDate;
@property (nullable, nonatomic, retain) NSString *stickyID;


@end

@interface StickyData (CoreDataGeneratedAccessors)

- (void)addNotesIDObject:(NotesID *)value;
- (void)removeNotesIDObject:(NotesID *)value;
- (void)addNotesID:(NSSet<NotesID *> *)values;
- (void)removeNotesID:(NSSet<NotesID *> *)values;

@end

NS_ASSUME_NONNULL_END

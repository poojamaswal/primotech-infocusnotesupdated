//
//  StickyData.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "StickyData.h"
#import "NotesID.h"
#import "DrawingDefaults.h"
#import "DatabaseManager.h"
#import "DrawingData.h"


@implementation StickyData

// Insert code here to add functionality to your managed object subclass
+(StickyData*)addStickyData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context
{
    NSEntityDescription *desc = [NSEntityDescription entityForName:@"StickyData"
                                            inManagedObjectContext:context];
    
    StickyData *aStickyData = [[StickyData alloc] initWithEntity:desc insertIntoManagedObjectContext:nil];
    
    if([dictData valueForKey:KeyData])
        aStickyData.stickyText = [dictData valueForKey:KeyData];
    
    if([dictData valueForKey:KeyFrames])
        aStickyData.stickyCoordinates = [dictData valueForKey:KeyFrames];
    
    
    
    
    aStickyData.stickyID = [NSString stringWithFormat:@"%@",[dictData valueForKey:KeyID]];
    
    aStickyData.savedDate = [DrawingDefaults sharedObject].dateStored;
    
    NSLog(@"%@",aStickyData.savedDate);
    
    [context insertObject:aStickyData];
    
    return aStickyData;//risk
}
@end

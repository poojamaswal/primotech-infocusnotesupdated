//
//  StickyData+CoreDataProperties.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StickyData+CoreDataProperties.h"

@implementation StickyData (CoreDataProperties)

@dynamic stickyCoordinates;
@dynamic stickyText;
@dynamic notesID;
@dynamic savedDate;
@dynamic stickyID;


@end

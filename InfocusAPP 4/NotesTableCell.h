//
//  NotesTableCell.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesTableCell : UITableViewCell


-(void)setTextHeader :(NSString*)text_;
@end

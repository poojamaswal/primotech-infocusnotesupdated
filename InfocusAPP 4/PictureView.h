//
//  PictureView.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 1/21/15.
//
//

#import <UIKit/UIKit.h>

@interface PictureView : UIView{
    
    CGPoint touchStart;
    BOOL isResizing;
}

@property (nonatomic, retain) UIView *contentView;
//@property (nonatomic, retain) UIImageView *contentImageView;


@end

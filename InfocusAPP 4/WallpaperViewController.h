//
//  WallpaperViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/22/13.
//
//

#import <UIKit/UIKit.h>

@interface WallpaperViewController : UIViewController<UIScrollViewDelegate>

@end

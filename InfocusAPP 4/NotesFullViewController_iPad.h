//
//  NotesFullViewController_iPad.h
//  Organizer
//
//  Created by Steven Abrams on 11/29/14.
//
//

#import <UIKit/UIKit.h>
#import "PenColorViewController.h"
#import "DrawView_iPad.h"
#import "RichTextEditor.h"


@interface NotesFullViewController_iPad : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate, PenNoteColorPickerDelegate, RichTextEditorDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (strong, nonatomic) IBOutlet DrawView_iPad *drawView;
@property (strong, nonatomic) IBOutlet RichTextEditor *drawTextView; //Steve TEST

//Set stroke width, color and alhpa. Methods are called on PenColorViewController as a protocol (PenNoteColorPickerDelegate)
-(void)SetLineWidth:(float) lineWidth;  //Stroke width
-(void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue; //Stroke Color
-(void)setAlphaColor:(CGFloat) alphaColorSelection; //Stroke Alpha

-(void)photoAlbum;
-(void)takePicture;

@end



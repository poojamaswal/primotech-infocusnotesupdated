

#import "RichTextEditorColorPickerViewController.h"

@implementation RichTextEditorColorPickerViewController

{
    NSMutableArray *colorArray;
}
#pragma mark - VoewController Methods -

//pooja
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    colorArray=[[NSMutableArray alloc]initWithObjects:[UIColor blackColor],[UIColor redColor],[UIColor greenColor],[UIColor lightGrayColor],[UIColor blueColor],[UIColor cyanColor],[UIColor grayColor],[UIColor darkGrayColor] ,[UIColor brownColor],[UIColor yellowColor ],[UIColor magentaColor],[UIColor orangeColor],[UIColor purpleColor],[UIColor whiteColor],[UIColor colorWithRed:0.8157 green:0.7294 blue:0.4863 alpha:1.0],[UIColor colorWithRed:0.2000 green:0.2235 blue:0.2510 alpha:1.0],[UIColor colorWithRed:0.0000 green:0.0000 blue:1.0000 alpha:0.2],[UIColor colorWithRed:0.7725 green:0.4902 blue:0.5569 alpha:1.0],[UIColor colorWithRed:0.1765 green:0.3569 blue:0.0000 alpha:1.0],[UIColor colorWithRed:0.8824 green:0.7529 blue:0.3686 alpha:1.0],nil];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //	UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 60, 30)];
    //	[btnClose addTarget:self action:@selector(closeSelected:) forControlEvents:UIControlEventTouchUpInside];
    //	[btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //	[btnClose setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //	[btnClose setTitle:@"Close" forState:UIControlStateNormal];
    //	[self.view addSubview:btnClose];
    //
    //	UIButton *btnDone = [[UIButton alloc] initWithFrame:CGRectMake(65, 5, 60, 30)];
    //	[btnDone addTarget:self action:@selector(doneSelected:) forControlEvents:UIControlEventTouchUpInside];
    //	[btnDone.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //	[btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //	[btnDone setTitle:@"Done" forState:UIControlStateNormal];
    //	[self.view addSubview:btnDone];
    
    self.selectedColorView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35 - 5, 5, 35, 30)];
    self.selectedColorView.backgroundColor = [UIColor blackColor];
    self.selectedColorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.selectedColorView.layer.borderWidth = 1;
    self.selectedColorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    //[self.view addSubview:self.selectedColorView];
    
    [self colorButtons];
    
    
    //	self.colorsImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"colors.jpg"]];;
    //	self.colorsImageView.frame = CGRectMake(2, 40, self.view.frame.size.width-4, self.view.frame.size.height - 40 - 2);
    //	self.colorsImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //	self.colorsImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //	self.colorsImageView.layer.borderWidth = 1;
    //	[self.view addSubview:self.colorsImageView];
    
    if ([self.dataSource richTextEditorColorPickerViewControllerShouldDisplayToolbar])
    {
        CGFloat reservedSizeForStatusBar = (
                                            UIDevice.currentDevice.systemVersion.floatValue >= 7.0
                                            && !(   UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad
                                                 && self.modalPresentationStyle==UIModalPresentationFormSheet
                                                 )
                                            ) ? 20.:0.; //Add the size of the status bar for iOS 7, not on iPad presenting modal sheet
        
        CGFloat toolbarHeight = 44 +reservedSizeForStatusBar;
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, toolbarHeight)];
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:toolbar];
        
        UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                           target:nil
                                                                                           action:nil];
        
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                  target:self
                                                                                  action:@selector(doneSelected:)];
        
        UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                   target:self
                                                                                   action:@selector(closeSelected:)];
        
        UIBarButtonItem *selectedColorItem = [[UIBarButtonItem alloc] initWithCustomView:self.selectedColorView];
        
        [toolbar setItems:@[closeItem, flexibleSpaceItem, selectedColorItem, doneItem]];
        [self.view addSubview:toolbar];
        
        self.colorsImageView.frame = CGRectMake(2, toolbarHeight+2, self.view.frame.size.width-4, self.view.frame.size.height - (toolbarHeight+4));
    }
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
    
    self.preferredContentSize = CGSizeMake(300, 240);
#else
    
    self.contentSizeForViewInPopover = CGSizeMake(300, 240);
#endif
    
}



-(void)colorButtons
{
    
    int x=10;
    int y=20;
    
    int count = 0;
    
    for (int i=0; i<colorArray.count; i++)
    {
        
        UIButton * btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(x, y, 40, 40);
        btn.backgroundColor=[colorArray objectAtIndex:i];
        btn.layer.cornerRadius=5.0;
        btn.layer.borderWidth=1.0;
        btn.layer.borderColor=[UIColor lightGrayColor].CGColor;
        btn.tag=i;
        [btn addTarget:self action:@selector(colorBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        count++;
        x+=btn.frame.size.width+20;
        if (count==5)
        {
            y+=btn.frame.size.height+10;
            x=10;
            count=0;
        }
        
        
    }
    
    
}


-(void)colorBtnPressed:(UIButton *)sender
{
    
    self.selectedColorView.backgroundColor = sender.backgroundColor;
    
    [self doneSelected:sender.backgroundColor];
    
}


#pragma mark - Private Methods -

- (void)populateColorsForPoint:(CGPoint)point
{
	self.selectedColorView.backgroundColor = [self.colorsImageView colorOfPoint:point];
}

#pragma mark - IBActions -

- (IBAction)doneSelected:(id)sender
{
	[self.delegate richTextEditorColorPickerViewControllerDidSelectColor:self.selectedColorView.backgroundColor withAction:self.action];
}

- (IBAction)closeSelected:(id)sender
{
	[self.delegate richTextEditorColorPickerViewControllerDidSelectClose];
}

#pragma mark - Touch Detection -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint locationPoint = [[touches anyObject] locationInView:self.colorsImageView];
	[self populateColorsForPoint:locationPoint];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint locationPoint = [[touches anyObject] locationInView:self.colorsImageView];
	[self populateColorsForPoint:locationPoint];
}

@end

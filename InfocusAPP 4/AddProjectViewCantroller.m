//
//  AddProjectViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.

#define EDit_BTN 9000
#import "AddProjectViewCantroller.h"
#import "RGBColorViewCantroller.h"
#import "AddProjectDataObject.h"
#import "AllInsertDataMethods.h"
#import "OrganizerAppDelegate.h"
//#import "AddNewSubfolderViewCantroller.h"
#import "SelectFolderOptionViewCantroller.h"
#import "MyTreeViewCell.h"
#import "SelectProjectColorViewCantroller.h"
#import "ProjectPriorityViewCantroller.h"
#import "ViewProjectViewCantroller.h" //Steve


@interface AddProjectViewCantroller(){
    
        BOOL    isPriorityPickerShowing; //Steve
}

@property(nonatomic, strong) UISwitch *priorityOnOffSwitch; //Steve
@property(nonatomic, strong) UIPickerView *priorityPicker; //Steve
@property (strong, nonatomic) IBOutlet UITableView *myTable_iOS7;

-(void) prioritySwitchValueChange;//Steve

@end

@implementation AddProjectViewCantroller
@synthesize addView,addproject,addprojectpriority,addprojectsubfolder,sql;
@synthesize belongValue,levelValue,LEVELOPT,myOptionFind,getfId,newViewValue;
@synthesize projectdataObj,arrayNo,arrayCR,FindProjectID,Editoptn,optionVal,getBIDArray,delegate;
@synthesize priorityOnOffSwitch, priorityPicker, myTable_iOS7; //Steve

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
    projectdataObj = [[AddProjectDataObject alloc] init];
    
    //Steve - initialize objects for iOS 7 since they are not "hookep up" on the NIB (AddProjectViewCantroller_iOS7.xib)
    if (IS_IOS_7) {
        
        addproject = [[UITextField alloc]init];
        addprojectpriority = [[UILabel alloc]init];
        addprojectsubfolder = [[UILabel alloc]init];
        levelTxT = [[UILabel alloc]init];
    }


	subfolderBTN.alpha=0;
	levelTxT.alpha=0;
	addprojectsubfolder.alpha=0;
	subTxT.alpha=0;
	getBIDArray=[[NSMutableArray alloc]init];
    
	pButton=[UIButton buttonWithType:UIButtonTypeCustom];
    pButton.frame =CGRectMake(8,8, 15,15);
	[pButton.layer setBorderWidth:.24];
	[pButton.layer setCornerRadius:7.50];
	[pButton.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];

	pButton.layer.masksToBounds = YES;
	[colorbutton addSubview:pButton];

    
    arrayCR =[[NSMutableArray alloc]init];
	
    [arrayCR addObject:@"None"];
	[arrayCR addObject:@"A"];
	[arrayCR addObject:@"B"];
	[arrayCR addObject:@"C"];
    
	
    arrayNo = [[NSMutableArray alloc]init];
	
	[arrayNo addObject:@"1"];
	[arrayNo addObject:@"2"];
	[arrayNo addObject:@"3"];
	[arrayNo addObject:@"4"];
	[arrayNo addObject:@"5"];
	[arrayNo addObject:@"6"];
	[arrayNo addObject:@"7"];
	[arrayNo addObject:@"8"];
	[arrayNo addObject:@"9"];
	//[arrayNo addObject:@"10"];
	
    
    priorityOnOffSwitch = [[UISwitch alloc]init]; //Steve
    priorityPicker = [[UIPickerView alloc]init]; //Steve
    
    priorityPicker.delegate = self;
    priorityPicker.dataSource = self;
    
    priorityPicker.hidden = YES; //Steve - Hides initial picker
    
    
	if(Editoptn)
	{
		Fval=FindProjectID-EDit_BTN;
		[self getPId_data:Fval];
		subfolderBTN.alpha=1;
		levelTxT.alpha=1;
		addprojectsubfolder.alpha=1;
		subTxT.alpha=1;
	}
    else{
        addprojectpriority.text = @"None";
    }
	
}

-(void)viewWillAppear:(BOOL)animated{
	
    [super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = NO;
        backButton.hidden = NO;
        self.navigationController.navigationBarHidden = NO;
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            self.view.backgroundColor = [UIColor whiteColor];
            isPriorityPickerShowing = NO;
            
    
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
               
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                myTable_iOS7.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:1.0];
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                                [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            
            
            if (IS_IPHONE_5) {
                
              //  myTable_iOS7.frame = CGRectMake(0, -20, 320, 568);
            }
            else{
                
               // myTable_iOS7.frame = CGRectMake(0, -20, 320, 480);
            }
            
            showTable.backgroundColor = [UIColor blueColor];
            
            
        }
        else{ //iOS 6
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            [showTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        }
        
        
        
    }
    else{ //Old Navigation
        
        
        self.navigationController.navigationBarHidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];

        [showTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

    }
    
    
    // ***************** Steve End ***************************
    
  
	
	pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
	[self.view addGestureRecognizer:pan];
	

    
if(Editoptn)
{
    
     if ([sharedDefaults boolForKey:@"NavigationNew"]){
         
         if (IS_IOS_7) {
             
             self.title = @"Edit Folder";
         }
         else{//iOS 6
             
             UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 5, 200, 30)];
             titleLabel.text = @"Edit Folder";
             
             titleLabel.backgroundColor = [UIColor clearColor];
             titleLabel.textColor = [UIColor whiteColor];
             titleLabel.numberOfLines = 1;
             titleLabel.minimumScaleFactor = 8.;
             [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
             titleLabel.adjustsFontSizeToFitWidth = YES;
             projectAddLabel = titleLabel;
             [self.navigationController.navigationBar addSubview:projectAddLabel];
             
             
             saveIMG=[UIImage imageNamed:@"UpdateSmalBtn.png"];
             
             savebtn=[[UIButton alloc] initWithFrame:CGRectMake(264,7,51,29)];
             [savebtn setBackgroundImage:saveIMG forState:UIControlStateNormal];
             [savebtn setBackgroundImage:saveIMG forState:UIControlStateHighlighted];
             [savebtn addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
             saveButton = savebtn;
             
             [self.navigationController.navigationBar addSubview:saveButton];
         }
         
         
     }
     else{ //Old Navigation
         
         UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
         myLabel.text=@"Edit Folder";
         myLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
         myLabel.backgroundColor = [UIColor clearColor];
         myLabel.textColor = [UIColor whiteColor];
         [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
         [self.view addSubview:myLabel];
         
         
         saveIMG=[UIImage imageNamed:@"UpdateSmalBtn.png"];
         
         savebtn=[[UIButton alloc] initWithFrame:CGRectMake(264,7,51,29)];
         [savebtn setBackgroundImage:saveIMG forState:UIControlStateNormal];
         [savebtn setBackgroundImage:saveIMG forState:UIControlStateHighlighted];
         [savebtn addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
         saveButton = savebtn;
         
         [self.view addSubview:saveButton];
     }
    
	
	

    if (IS_IOS_7) {
        
        UIBarButtonItem *updateButton = [[UIBarButtonItem alloc] initWithTitle:@"Update"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(save_click:)];
        
         updateButton.tintColor = [UIColor whiteColor];
        [self.navigationItem setRightBarButtonItem:updateButton];
    }
    else{ //iOS 6

        
        UIImage *delImg =[UIImage imageNamed:@"DeleteBtn.png"];
        UIButton *delbtn=[[UIButton alloc] initWithFrame:CGRectMake(41.50,400,237,54)];
        [delbtn setBackgroundImage:delImg forState:UIControlStateNormal];
        [delbtn setBackgroundImage:delImg forState:UIControlStateHighlighted];
        [delbtn addTarget:self action:@selector(delete_click:) forControlEvents:UIControlEventTouchUpInside];
        deleteButton = delbtn;
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]) {
            if (IS_IPHONE_5)
                deleteButton.frame = CGRectMake(41.50, 400, 237, 54);
            else
                deleteButton.frame = CGRectMake(41.50, 400 - 88, 237, 54);
        }
        
        [self.view addSubview:deleteButton];
    }
    
    
    
    CGRect myImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f);
    UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
    [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
    [pButton addSubview:myImage];
	   
    
    
	if(LEVELOPT)
	{
		levelTxT.text=[NSString stringWithFormat:@"%d",levelValue];
		[self getSubfolder:belongValue];
		addprojectsubfolder.text=projectdataObj.projectSubname;	
	}
	else 
	{
        
		levelTxT.text=[NSString stringWithFormat:@"%d",projectdataObj.level];
		[self getSubfolder:projectdataObj.belongstoid];
		addprojectsubfolder.text=projectdataObj.projectSubname;	
		
        
		CGRect myImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f);
		UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
        
		if(projectdataObj.projectIMGname==nil)
		{
			
			[projectdataObj setProjectIMGname:@"FolderColor1.png"];
			[myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
		}
		else
		{
			[myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
		}
        
		[pButton addSubview:myImage];	
	}

}

else // NOT Editoptn
{
    
    
    
    if(projectdataObj.projectIMGname == nil)
        
        [self getMainProjectImage:belongValue];
    
    
         if ([sharedDefaults boolForKey:@"NavigationNew"]){
             
             
             if (IS_IOS_7) {
                 
                 if(IS_IPAD)
                 self.title = @"";
                 else
                self.title = @"New Folder";
                 
                 UIBarButtonItem *saveButton_iOS7 = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                                     style:UIBarButtonItemStylePlain
                                                                                    target:self
                                                                                    action:@selector(save_click:)];
                 
                 saveButton_iOS7.tintColor = [UIColor whiteColor];
                 [self.navigationItem setRightBarButtonItem:saveButton_iOS7];
                 
                 
                 CGRect myImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f); //Takes the middle portion of the color folder
                 UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
                 
                 if(projectdataObj.projectIMGname==nil)
                 {
                     
                     [projectdataObj setProjectIMGname:@"FolderColor1.png"];
                     [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
                 }
                 else
                 {
                     [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
                 }
                 [pButton addSubview:myImage];
                 
                 
             }
             else{ //iOS 6
                 
                 UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 5, 200, 30)];
                 titleLabel.text = @"New Folder";
                 
                 titleLabel.backgroundColor = [UIColor clearColor];
                 titleLabel.textColor = [UIColor whiteColor];
                 titleLabel.numberOfLines = 1;
                 //titleLabel.minimumFontSize = 8.;
                 titleLabel.minimumScaleFactor = 8;
                 [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
                 titleLabel.adjustsFontSizeToFitWidth = YES;
                 projectAddLabel = titleLabel;
                 [self.navigationController.navigationBar addSubview:projectAddLabel];

                 
                 saveIMG=[UIImage imageNamed:@"SaveSmallBtn.png"];
                 
                 
                 CGRect myImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f); //Takes the middle portion of the color folder
                 UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
                 
                 if(projectdataObj.projectIMGname==nil)
                 {
                     
                     [projectdataObj setProjectIMGname:@"FolderColor1.png"];
                     [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
                 }
                 else
                 {
                     [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
                 }
                 [pButton addSubview:myImage];		

                 
                 [savebtn removeFromSuperview];                   //262, 7, 51,29
                 savebtn=[[UIButton alloc] initWithFrame:CGRectMake(264,7,51,29)];  //Steve CGRectMake(255,6,58,32)
                 [savebtn setBackgroundImage:saveIMG forState:UIControlStateNormal];
                 [savebtn setBackgroundImage:saveIMG forState:UIControlStateHighlighted];
                 [savebtn addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
                 saveButton = savebtn;
                 
                 [self.navigationController.navigationBar addSubview:saveButton];
             }


         }
         else{ //Old Navigation
             
             UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(83,5,190,30)];
             //myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
             myLabel.text=@"New Folder";
             myLabel.textColor= [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0];
             myLabel.backgroundColor = [UIColor clearColor];
             myLabel.textColor = [UIColor whiteColor];
             [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
             [self.view addSubview:myLabel];
             
             
             saveIMG=[UIImage imageNamed:@"SaveSmallBtn.png"];
             CGRect myImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f); //Takes the middle portion of the color folder
             UIImageView *myImage = [[UIImageView alloc] initWithFrame:myImageRect];
             if(projectdataObj.projectIMGname==nil)
             {
                 
                 [projectdataObj setProjectIMGname:@"FolderColor1.png"];
                 [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
             }
             else
             {
                 [myImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
             }
             [pButton addSubview:myImage];		

             
             [savebtn removeFromSuperview];                   //262, 7, 51,29
             savebtn=[[UIButton alloc] initWithFrame:CGRectMake(264,7,51,29)];  //Steve CGRectMake(255,6,58,32)
             [savebtn setBackgroundImage:saveIMG forState:UIControlStateNormal];
             [savebtn setBackgroundImage:saveIMG forState:UIControlStateHighlighted];
             [savebtn addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
             saveButton = savebtn;

            [self.view addSubview:saveButton];
         }
    
			
				
	}

    
    addproject.placeholder = @"Project Name";
    
    if (IS_IOS_7) {
        
        if ([addprojectpriority.text isEqualToString:@"None"]) {
            
            priorityOnOffSwitch.on = NO;
        }
        else{
            
            priorityOnOffSwitch.on = YES;
        }
        
        [myTable_iOS7 reloadData];
        
    }
    
    
}


#pragma mark -
#pragma mark Table view - iOS 7 Only
#pragma mark -


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [addproject resignFirstResponder]; //removes keyboard when scrolling starts

}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 15; //15
    return 7.0; //5
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 7.0; //5
}


-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections;
    
    if (Editoptn)
        sections = 5;
    else
        sections = 3;
    
    
    return sections;
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (IS_IOS_7) {
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        
        
    }
    else{ //iOS 6
        
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
    /*
    //Steve - removes the borders & cell background from the deleteButton cell
    if([cell.reuseIdentifier isEqualToString:@"deleteButtonCell"]){
       // cell.backgroundView = nil;
        
    }
    
    if ([cell.reuseIdentifier isEqualToString:@"subFolderCell"]) {
       // cell.backgroundView = nil;
    }
     */
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    CGFloat height;
    
    if (indexPath.section == 2 && indexPath.row == 2 && isPriorityPickerShowing) {
        
        height = 216;
    }
    else if (indexPath.section == 2 && indexPath.row == 2 && !isPriorityPickerShowing) {
        height = 0;
    }
    else{
        
        height = 40;
    }

    
    return height;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    int numberOfRows;
    
    if (section == 2) {
        
        numberOfRows = 3;
    }
    else{
        
        numberOfRows = 1;
    }
    
    
    return numberOfRows;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier;
    
    /*
    if (indexPath.section == 4) {
        CellIdentifier = @"deleteButtonCell";
    }
    else if (indexPath.section == 3){
        CellIdentifier = @"subFolderCell";
    }
    else{
        CellIdentifier = @"Cell";
    }
     */
    
    CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
	
	for (UIView  *subViews in [cell.contentView subviews]) {
		[subViews removeFromSuperview];
	}
    
    
    int y = 7;
    
    
    if (indexPath.section == 0) { //Project Title
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        addproject.backgroundColor = [UIColor clearColor];
        
        [cell.contentView addSubview:addproject];
        
        if (IS_IPAD) {
            addproject.frame = CGRectMake(10, y, 750, 24);
        }
        else{ //iPhone
            addproject.frame = CGRectMake(10, y, 240, 24);
        }
        
    }
    else if (indexPath.section == 1) { //Project Color
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

        //Round Color Image
        ////obtains a portion of the color from the folder & adds it to a button shaped like a small circle
        CGRect myColorImageRect = CGRectMake(0.0f, -17.0f, 70.0f, 60.0f); //Takes this portion of image
        UIImageView *myColorImage = [[UIImageView alloc] initWithFrame:myColorImageRect];
        [myColorImage setImage:[UIImage imageNamed:projectdataObj.projectIMGname]];
        pButton.frame = CGRectMake(3, 40/2 - 15/2 - 1, 15, 15);
        [pButton addSubview:myColorImage]; //pButton declared with layers in viewDidLoad
        [cell.contentView addSubview:pButton];

        
        UILabel *projectColorLabelFixed = [[UILabel alloc]init];
        projectColorLabelFixed.frame = CGRectMake(pButton.frame.origin.x + pButton.frame.size.width + 7, y, 150, 24);
        projectColorLabelFixed.text = @"Project Color";
        [projectColorLabelFixed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        projectColorLabelFixed.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:projectColorLabelFixed];

    }
    else if (indexPath.section == 2 && indexPath.row == 0) { //Project Priority ON/OFF
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        UILabel *priorityOnOffLabelFixed = [[UILabel alloc]init];
        priorityOnOffLabelFixed.frame = CGRectMake(10, y, 150, 24);
        priorityOnOffLabelFixed.text = @"Priority On/Off";
        [priorityOnOffLabelFixed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        priorityOnOffLabelFixed.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:priorityOnOffLabelFixed];
        

        [priorityOnOffSwitch addTarget:self action:@selector(prioritySwitchValueChange) forControlEvents:UIControlEventTouchUpInside];
         priorityOnOffSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        
        [cell.contentView addSubview:priorityOnOffSwitch];
        
        if (IS_IPAD) {
            priorityOnOffSwitch.frame = CGRectMake(703, 5, 51, 31);
        }else{ //iPhone
            priorityOnOffSwitch.frame = CGRectMake(255, 5, 51, 31);
        }
        
    }

    else if (indexPath.section == 2 && indexPath.row == 1) { //Project Priority Label
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        
        UILabel *priorityLabelFixed = [[UILabel alloc]init];
        priorityLabelFixed.frame = CGRectMake(5, y, 150, 24);
        priorityLabelFixed.text = @"Project Priority";
        [priorityLabelFixed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        priorityLabelFixed.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:priorityLabelFixed];
        
        
        //UILabel *priorityLabelFixed = [[UILabel alloc]init];
        addprojectpriority.frame = CGRectMake(155, y, 150, 24);
        addprojectpriority.backgroundColor = [UIColor clearColor];
        addprojectpriority.numberOfLines = 1.0;
        addprojectpriority.textAlignment = NSTextAlignmentRight;
        [addprojectpriority setFont:[UIFont fontWithName:@"Helvetica" size:17]];
        
        if (isPriorityPickerShowing) {
            
            addprojectpriority.textColor = [UIColor redColor];
        }
        else{
            
            addprojectpriority.textColor = [UIColor lightGrayColor];
        }
        
        [cell.contentView addSubview:addprojectpriority];
        
        
        //Steve
        if (IS_IPAD) {
            addprojectpriority.frame = CGRectMake(155 + 448, y, 150, 24);
        }
        else{ //iPhone
            addprojectpriority.frame = CGRectMake(155, y, 150, 24);
        }

    }
    else if (indexPath.section == 2 && indexPath.row == 2) { //Project Priority Picker
        
        [cell.contentView addSubview:priorityPicker];
        
        //Steve
        if (IS_IPAD) {
            priorityPicker.frame = CGRectMake(self.view.frame.size.width/2 - priorityPicker.frame.size.width/2, 0, priorityPicker.frame.size.width, priorityPicker.frame.size.height);
        }

       
    }
    else if (indexPath.section == 3) { //Project SubFolder
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        
        UILabel *prioritySubFolderLabelFixed = [[UILabel alloc]init];
        prioritySubFolderLabelFixed.frame = CGRectMake(10, y, 150, 24);
        prioritySubFolderLabelFixed.text = @"Subfolder";
        [prioritySubFolderLabelFixed setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        prioritySubFolderLabelFixed.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:prioritySubFolderLabelFixed];
        
        
    }
    if (indexPath.section == 4) { //Delete
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        int cellWidth = cell.frame.size.width;
        int cellHeight = cell.frame.size.height;
        int buttonWidth = 150;
        int buttonHeight = 35;
        
        UIButton *deleteButtonCustom = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [deleteButtonCustom setTitle:@"Delete" forState:UIControlStateNormal];
        deleteButtonCustom.tintColor = [UIColor redColor];
        deleteButtonCustom.titleLabel.textAlignment = NSTextAlignmentCenter;
        deleteButtonCustom.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18]; //HelveticaNeue-Bold
        deleteButtonCustom.backgroundColor = [UIColor clearColor];
        deleteButtonCustom.frame =
        CGRectMake(cellWidth/2 - buttonWidth/2, cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
        
        [deleteButtonCustom addTarget:self
                               action:@selector(delete_click:)
                     forControlEvents:UIControlEventTouchDown];
        
        [cell.contentView addSubview:deleteButtonCustom];

    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

    if (indexPath.section == 1 && indexPath.row == 0) {
        
        [addproject resignFirstResponder];
        
        [self rgb_click:self];
    }
    
    else if (indexPath.section == 2 && indexPath.row == 1){
        
        [addproject resignFirstResponder];
        
        if (isPriorityPickerShowing) {
            
            priorityPicker.hidden = YES;
            isPriorityPickerShowing = NO;
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];

        }
        else{
            
            priorityPicker.hidden = NO;
            isPriorityPickerShowing = YES;
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    else if (indexPath.section == 3){
        
        [addproject resignFirstResponder];
        [self click_Subfolder:self];
        
    }
    else if(indexPath.section == 4){ //Delete
        
        [self delete_click:self];
        
    }

    
}


#pragma mark -
#pragma mark  Switch changes - iOS 7 Only
#pragma mark -

-(void) prioritySwitchValueChange{
    
    [addproject resignFirstResponder]; //releases keyboard
	
    if (priorityOnOffSwitch.isOn) {
        
        priorityPicker.hidden = NO;
        
        //shows Priority: A 1
        [priorityPicker selectRow:1 inComponent:0 animated:YES];
        [priorityPicker selectRow:0 inComponent:1 animated:YES];
        
        NSString *column_1String = [arrayCR objectAtIndex:1];
        NSString *column_2String = [arrayNo objectAtIndex:0];
        
        addprojectpriority.text = [column_1String stringByAppendingString:column_2String];
        isPriorityPickerShowing = YES;
        
        
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
        
        // Add them in an index path array
        NSArray *indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [myTable_iOS7 reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
        //Scrolls this section & row to the top for easier viewing
        /*
        NSInteger row = 0;
        NSInteger section = 2;
        NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
        [showTable scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
         */
        
        
    }
    else{
        
        priorityPicker.hidden = YES;
        
        //Shows Priority: None
        [priorityPicker selectRow:0 inComponent:0 animated:YES];
        [priorityPicker selectRow:0 inComponent:1 animated:YES];
        
        addprojectpriority.text = @"None";
        isPriorityPickerShowing = NO;
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
        
        // Add them in an index path array
        NSArray *indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [myTable_iOS7 reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    
}


#pragma mark
#pragma mark pickerView
#pragma mark


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
	return 2;
}



- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    
    NSInteger numberOfRows;
    
    
    if (component == 0) {
        
        numberOfRows = [arrayCR count];
    }
    else{
        
        numberOfRows = [arrayNo count];
    }


	
    return numberOfRows;
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    
    NSString *titleForRowString;
    
    
    if (component == 0) {
        
        titleForRowString = [arrayCR objectAtIndex:row];
    }
    else{
        
        titleForRowString = [arrayNo objectAtIndex:row];
    }
    

	return titleForRowString;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    

    NSInteger column_1Picker = [priorityPicker selectedRowInComponent:0];
    NSInteger column_2Picker = [priorityPicker selectedRowInComponent:1];
    
    NSString *column1  = [arrayCR objectAtIndex:column_1Picker];
    NSString *column2  = [arrayNo objectAtIndex:column_2Picker];
    
    
    addprojectpriority.text = [column1 stringByAppendingString:column2];
    
    
    //Checks for "None"
    if ([addprojectpriority.text length] > 2)
    {
        
        //remove the first 4 characters to avoid things like "None1", should be "None"
        NSMutableString *tempString = [NSMutableString stringWithString: addprojectpriority.text];
        NSString *priorityLabel_Remove4Char = [tempString substringWithRange: NSMakeRange (0, 4)];
        
        if ([priorityLabel_Remove4Char isEqualToString:@"None"])
        {
            //if 1st 4 char are "None", then should label is "None". We don't want the priority number
            addprojectpriority.text = priorityLabel_Remove4Char;
        }
    }
    
    
    if ([addprojectpriority.text isEqualToString: @"None"]) {
        [priorityOnOffSwitch setOn:NO animated:YES];
    }
    else{
        [priorityOnOffSwitch setOn:YES animated:YES];
    }
    
    

    
    

    
    
    //NSLog(@"row = %d ", row);
    //NSLog(@"component = %d ", component);
    //NSLog(@"getString = %@ ", getString);
    
}





- (void)panRecognized:(UIPanGestureRecognizer *)gestureRecognizer {
    switch ([gestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
		{
			[addproject resignFirstResponder];
			[addprojectpriority resignFirstResponder];
		}
            break;
        case UIGestureRecognizerStateEnded:
		{
			[addproject resignFirstResponder];
			[addprojectpriority resignFirstResponder];
		}
            break;
			
        default:
            break;
    }
	
    // handle panning
}


-(void)setPriorityNew:(NSString*)priority1 priority2:(NSString*)priority2;
{
	
	addprojectpriority.text=[priority2 stringByAppendingString:priority1];
	
}


-(void)getSubfolder:(NSInteger)fID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		Sql = [[NSString alloc]initWithFormat:@"SELECT projectID,projectName from ProjectMaster where projectID=%d",fID];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				
				[projectdataObj setSubfolderID:sqlite3_column_int(selectNotes_Stmt, 0)];
				[projectdataObj setProjectSubname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
			}
			
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
		
}


-(void)getMainProjectImage:(NSInteger)Val
{			
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
        
		Sql = [[NSString alloc]initWithFormat:@"SELECT folderImage from ProjectMaster where projectID=%d",Val];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				
                [projectdataObj setProjectIMGname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 0)]];
                
			}
			
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
}



-(void)getPId_data:(NSInteger)Val;
{			
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	

		Sql = [[NSString alloc]initWithFormat:@"SELECT projectID, projectName,projectPriority,level,belongstoID,folderImage from ProjectMaster where projectID=%d",Val];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				[projectdataObj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[projectdataObj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				addproject.text = projectdataObj.projectname;
				[projectdataObj setProjectpriority: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
				addprojectpriority.text =projectdataObj.projectpriority;
				[projectdataObj setLevel:sqlite3_column_int(selectNotes_Stmt, 3)];
				[projectdataObj setBelongstoid:sqlite3_column_int(selectNotes_Stmt,4)];
                [projectdataObj setProjectIMGname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 5)]];
                
			}
			
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
	[addproject resignFirstResponder];
	[addprojectpriority resignFirstResponder];
}



- (BOOL)textFieldShouldReturn:(UITextField *)theTextField 
{
	[addproject resignFirstResponder];
	
	[addprojectpriority resignFirstResponder];
	[addprojectsubfolder resignFirstResponder];	
	return YES;
}

-(IBAction)rgb_click:(id)sender
{
	[addproject resignFirstResponder];
    SelectProjectColorViewCantroller *selectview;
    
    if(IS_IPAD)
    {
        selectview = [[SelectProjectColorViewCantroller alloc] initWithNibName:@"SelectProjectColorViewCantrollerIPad" bundle:[NSBundle mainBundle]];
    }
    else
    {
        selectview = [[SelectProjectColorViewCantroller alloc] initWithNibName:@"SelectProjectColorViewCantroller" bundle:[NSBundle mainBundle]];
    }
	selectview.PdataObj = projectdataObj;
	[self.navigationController pushViewController:selectview animated:YES];

}

-(IBAction)click_Subfolder:(id)sender
{
	
	[self checkCHLD:Fval];
	
	if(projectdataObj.level==0)
	{
		UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:@"Moving Project Folders"
							  message:@"You can't move this folder.  You can only move folders with no Subfolders below it. Move the lowest subfolder below it, then move this subfolder."
							  delegate:nil
							  cancelButtonTitle:@"OK"
							  otherButtonTitles:nil];
		[alert show];
		return;
		
	}
	
	else if(checkChild==0)
	{
	SelectFolderOptionViewCantroller *selectview = [[SelectFolderOptionViewCantroller alloc] initWithNibName:@"SelectFolderOptionViewCantroller" bundle:[NSBundle mainBundle]];
	selectview.getRootVal=getfId;
	selectview.getfval=Fval;
	selectview.lastProjectID=projectdataObj.belongstoid;
	selectview.getViewValue=newViewValue;
	[self.navigationController pushViewController:selectview animated:YES];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:@"Moving Project Folders"
							  message:@"You can't move this folder.  You can only move folders with no Subfolders below it. Move the lowest subfolder below it, then move this subfolder."
							  delegate:nil
							  cancelButtonTitle:@"OK"
							  otherButtonTitles:nil];
		[alert show];
		return;
		
	}


	
}


-(void)checkCHLD:(NSInteger)CHKD
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		Sql = [[NSString alloc]initWithFormat:@"SELECT projectID from ProjectMaster where belongstoID=%d",CHKD];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				checkChild=sqlite3_column_int(selectNotes_Stmt, 0);
			}
			
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
}


-(IBAction)done_click:(id)sender
{
   NSInteger fRow =[myPickerView selectedRowInComponent:0];
   NSInteger sRow = [myPickerView selectedRowInComponent:1];
   NSString *bread = [arrayCR objectAtIndex:fRow];
   NSString *filling = [arrayNo objectAtIndex:sRow];
   NSString *fullText=[bread stringByAppendingString:filling];
   addprojectpriority.text=fullText;
   [myPickerView removeFromSuperview];
   [pcView removeFromSuperview];
}

-(IBAction)can_click:(id)sender
{
	[myPickerView removeFromSuperview];
	[pcView removeFromSuperview];
}


-(IBAction)priority_click:(id)sender
{
	ProjectPriorityViewCantroller *selectviewP = [[ProjectPriorityViewCantroller alloc]initWithNibName:@"ProjectPriorityViewCantroller" bundle:[NSBundle mainBundle]setNewPriority:[NSString stringWithFormat:@"%@", addprojectpriority.text]];
	[self.navigationController pushViewController:selectviewP animated:YES];
	[selectviewP setDelegate:self];
	
	
//[addproject resignFirstResponder];
//	myPickerView = [[UIPickerView alloc] init];
//	myPickerView.frame = CGRectMake(0,240,320,240);
//	myPickerView.delegate = self;
//	myPickerView.showsSelectionIndicator = YES;
//	
//	
//	[self.view addSubview:myPickerView];
//	
//	UIImage *imgv=[UIImage imageNamed:@"Srtip.png"];
//	
//	pcView=[UIButton buttonWithType:UIButtonTypeCustom];
//	pcView.frame=CGRectMake(0,208,320,40);
//	[pcView setBackgroundImage:imgv forState:UIControlStateNormal];
//	[self.view addSubview:pcView];
//	
//	UIImage *picIMG=[UIImage imageNamed:@"Done.png"];
//	UIButton *picbtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [picbtn setBackgroundImage:picIMG forState:UIControlStateNormal];
//	[picbtn addTarget:self 
//			   action:@selector(done_click:)forControlEvents:UIControlEventTouchDown];
//	picbtn.frame=CGRectMake(5,6,50,30);
//	[pcView  addSubview:picbtn];
//	[picIMG release];		
	

	
	//UIImage *picIMGC=[UIImage imageNamed:@"CancelSmallBtn.png"];
//	UIButton *picbtnC = [UIButton buttonWithType:UIButtonTypeCustom];
//    [picbtnC setBackgroundImage:picIMGC forState:UIControlStateNormal];
//	[picbtnC addTarget:self 
//			   action:@selector(can_click:)forControlEvents:UIControlEventTouchDown];
//	picbtnC.frame=CGRectMake(265,6,50,30);
//	[pcView  addSubview:picbtnC];
//	[picIMGC release];			
}


-(void)delete_Project:(NSInteger)fval
{

	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteproject= nil;
	if(deleteproject ==nil)
	{   sql = [[NSString alloc]initWithFormat:@"select projectID from ProjectMaster where belongstoId = %d ",fval];
		
				
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
		{
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		{
			
			while(sqlite3_step(deleteproject) == SQLITE_ROW) 
			{
				NSInteger Bval=sqlite3_column_int(deleteproject, 0);
				NSLog(@"%d",Bval);
				[getBIDArray addObject:[NSString stringWithFormat:@"%d",Bval]];
				[self delete_Project:Bval];
			}
			
			sqlite3_finalize(deleteproject);		
			deleteproject = nil;        
		}
	}
}

-(void)dum:(NSMutableArray *)faa;
{
	
	[faa addObject:[NSString stringWithFormat:@"%d",Fval]];
	
	NSLog(@"%@",faa);
	
	
	for(int mm=0;mm<[faa count];mm++)
	{
		[self update_todo:[[faa objectAtIndex:mm]intValue]];
        [self delete_notes:[[faa objectAtIndex:mm]intValue]];
		[self todo_Find:[[faa objectAtIndex:mm]intValue]];
	}
	
	
}
-(void)todo_Find:(NSInteger)bValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteproject= nil;
	
	if(deleteproject == nil) 
	{
		sql = [[NSString alloc]initWithFormat:@"delete from ProjectMaster where projectID = %d",bValue];
		
		NSLog(@"%@",sql);
		
		if(sqlite3_prepare_v2(appDelegate.database,[sql UTF8String ], -1, &deleteproject, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	
	
	if (SQLITE_DONE != sqlite3_step(deleteproject)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteproject);
		
	} else {
		sqlite3_finalize(deleteproject);
		
	}
	
}


-(void)delete_notes:(NSInteger)CValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		//sql = [[NSString alloc]initWithFormat:@"Update TODOTable set BelongstoProjectID=%d where BelongstoProjectID=%d",0,CValue];
        
        sql = [[NSString alloc]initWithFormat:@"delete from NotesMaster where BelongstoProjectID=%d",CValue];
        
		NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
    if(SQLITE_DONE != sqlite3_step(insertProjectData))
    {
        NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    } 
    else 
    {
        sqlite3_finalize(insertProjectData);
        insertProjectData =nil;
        
    }	
}



-(void)update_todo:(NSInteger)CValue
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		//sql = [[NSString alloc]initWithFormat:@"Update TODOTable set BelongstoProjectID=%d where BelongstoProjectID=%d",0,CValue];
        
        sql = [[NSString alloc]initWithFormat:@"delete from TODOTable where BelongstoProjectID=%d",CValue];
        
		NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
		if(SQLITE_DONE != sqlite3_step(insertProjectData))
		{
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertProjectData);
			insertProjectData =nil;
			
		} 
		else 
		{
			sqlite3_finalize(insertProjectData);
			insertProjectData =nil;
			
		}	
}

-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	// the user clicked one of the OK/Cancel buttons
	
if(actionSheet.tag==10)
{
    
    NSLog(@"levelTxT =  %@", levelTxT);

    NSInteger levelOfFolder = levelTxT.text.integerValue; //Level of Folder.  0 is Main Project Folder
    
    
		if (buttonIndex == 0)
		{
			[self delete_Project:Fval];
			[self dum:getBIDArray];
			UIAlertView *alertNow = [[UIAlertView alloc]
								  initWithTitle:@"Project"
								  message:@"Project Deleted Successfully"
								  delegate:nil
								  cancelButtonTitle:@"OK"
								  otherButtonTitles:nil];
			[alertNow show];
            
            if (levelOfFolder == 0) {
                
                [self.navigationController popToRootViewControllerAnimated:YES]; //pop to rootViewcontroller
            }
            else{
                
                UIViewController* requireController = [[[self navigationController] viewControllers] objectAtIndex:1]; //Pops to ViewProjectViewcontroleer
                [self.navigationController popToViewController:requireController animated:YES];
                
            }

		}
		else
		{
			return;
		}
		
		
}
	else 
	{
		if (buttonIndex == 0)
		{
			[self.navigationController popViewControllerAnimated:YES];
		}
		else
		{
			
		}
		
	}	
}

-(IBAction)save_click:(id)sender
{
	
    NSLog(@" addproject.text = %@", addproject.text);
    
if((addproject.text == nil)||([[addproject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]<=0))
	{
		UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:@"Project Name"
							  message:@"You must have a project name"
							  delegate:nil
							  cancelButtonTitle:@"OK"
							  otherButtonTitles:nil];
		[alert show];
		return;
	}	
	else 
	{
		OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
		sqlite3_stmt *insertProjectData=nil;
		
		
		NSDate *today1 = [NSDate date];
		NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
		[dateFormat setDateFormat:@"dd/MM/yyyy"];
		
		NSString *dateString11 = [dateFormat stringFromDate:today1];
		
		NSLog(@"date: %@", dateString11);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm:ss a"];
        NSDate *_date = [NSDate date];
        NSString *modifydateStr = [dateFormatter stringFromDate:_date];
		
        
		 if([addprojectsubfolder.text isEqualToString:@"None"])
		{
			UIAlertView *alert = [[UIAlertView alloc]
								  initWithTitle:@"Project Folder or SubFolder"
								  message:@"Must Be a Project Folder/SubFolder:"
								  delegate:nil
								  cancelButtonTitle:@"OK"
								  otherButtonTitles:nil];
			[alert show];
			return;
		}
		
        
		if(insertProjectData == nil && appDelegate.database) 
		{
			
			if(Editoptn)
			{
				
				sql=[[NSString alloc]initWithFormat:@"UPDATE ProjectMaster set projectName = ?, projectPriority=?,modifyDate=?,belongstoID=?,level=?,folderImage=? where projectId=%d",Fval];
				
			}
			else
			{
				sql =[[NSString alloc]initWithFormat:@"insert into ProjectMaster(projectName,projectPriority,creationDate,belongstoID,level,folderImage,modifyDate)Values(?,?,?,?,?,?,?)"];
			}
			
			if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
		}
		

		//NSLog(@"%@",notesDataObj.notesPicBinaryimg);
		sqlite3_bind_text(insertProjectData,1,[[addproject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] UTF8String], -1, SQLITE_TRANSIENT);
		
		if([addprojectpriority.text isEqualToString:@"None"])
		{
			sqlite3_bind_text(insertProjectData,2,[[NSString stringWithFormat:@"%@", @"None"] UTF8String], -1, SQLITE_TRANSIENT);
		}
		else 
		{
		sqlite3_bind_text(insertProjectData,2,[addprojectpriority.text UTF8String], -1, SQLITE_TRANSIENT);
		}
		
		sqlite3_bind_text(insertProjectData,3,[dateString11 UTF8String], -1, SQLITE_TRANSIENT);
		
		if(Editoptn)
		{
			if(LEVELOPT)
			{
				sqlite3_bind_int(insertProjectData,4,belongValue);
				sqlite3_bind_int(insertProjectData,5,levelValue);
			}
			else 
			{
				sqlite3_bind_int(insertProjectData,4,projectdataObj.belongstoid);
				sqlite3_bind_int(insertProjectData,5,projectdataObj.level);
			}
			sqlite3_bind_text(insertProjectData,6,[projectdataObj.projectIMGname UTF8String], -1, SQLITE_TRANSIENT);
		}
		else 
		{
        sqlite3_bind_int(insertProjectData,4,belongValue);
        sqlite3_bind_int(insertProjectData,5,levelValue);
		sqlite3_bind_text(insertProjectData,6,[projectdataObj.projectIMGname UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertProjectData,7,[modifydateStr UTF8String], -1, SQLITE_TRANSIENT);    
       
		}
	
		//sqlite3_bind_text(insertProjectData,8,[addprojectcolorsubfolder.text UTF8String], -1, SQLITE_TRANSIENT);
		 if(SQLITE_DONE != sqlite3_step(insertProjectData))
		 {
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
			sqlite3_finalize(insertProjectData);
			insertProjectData =nil;
		 } 
		 else 
		 {
			sqlite3_finalize(insertProjectData);
			insertProjectData =nil;
			 
			//if(projectdataObj.level==1)
//			{
//				[self checkIT:Fval];
//				if(findIDVal==0)
//				{
//				[self updateLevelOne:Fval];	
//					
//				}
//				
//			}
			//[self.navigationController popViewControllerAnimated:YES];
             
             
             
             if(Editoptn)
                 
             {
                 int count = [self.navigationController.viewControllers count];
                 [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
             }
             else 
             {
                 [self.navigationController popViewControllerAnimated:YES];
             }
             
             
		 }	
	}	
}

-(void)updateLevelOne:(NSInteger)BID
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	sqlite3_stmt *insertProjectData=nil;
	
	if(insertProjectData == nil && appDelegate.database) 
	{
		sql = [[NSString alloc]initWithFormat:@"Update projectMaster set level=%d,belongstoID=%d where belongstoID=%d and level=%d",1,projectdataObj.belongstoid,BID,2];
		NSLog(@"%@",sql);
		if(sqlite3_prepare_v2(appDelegate.database, [sql UTF8String],-1, &insertProjectData, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	if(SQLITE_DONE != sqlite3_step(insertProjectData))
	{
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	} 
	else 
	{
		sqlite3_finalize(insertProjectData);
		insertProjectData =nil;
		
	}	
}

-(void)checkIT:(NSInteger)chkVAL
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		Sql = [[NSString alloc]initWithFormat:@"SELECT level from ProjectMaster where projectID=%d AND level=%d",chkVAL,0];
		//NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				findIDVal=sqlite3_column_int(selectNotes_Stmt, 0);
			}
			
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}	
}


-(IBAction)delete_click:(id)sender
{
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Project" message:@"Are you sure you want to Delete this Project Folder?  If you delete it, all the associated Todo's and Notes will be deleted as well.  If there is a Todo or Note you want to save, move it to another folder before Deleting."  delegate:self cancelButtonTitle:@"YES" otherButtonTitles: @"NO", nil];
	alert.tag=10;
    [alert show];
}


-(IBAction)homeButton_Clicked:(id)sender 
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [saveButton removeFromSuperview];
        [backButton removeFromSuperview];
        [projectAddLabel removeFromSuperview];

    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    backButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end

    //
//  RGBColorViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RGBColorViewCantroller.h"
#import "AddProjectDataObject.h"
#import "AddProjectViewCantroller.h"

@implementation RGBColorViewCantroller
@synthesize colorPanelView,PdataObj, CdataObj;
@synthesize	redSlider, greenSlider, blueSlider, aView, delegate;


- (void)loadView 
{
	colorPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 320)];
	[colorPanelView setBackgroundColor:[UIColor lightGrayColor]];
	[colorPanelView setAlpha:1];
	[colorPanelView setTag:1029];
	[self setView:colorPanelView];
	
	UIImage *saveImg=[UIImage imageNamed:@"SaveBtn.png"];
	
	UIButton *colorSaveButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[colorSaveButton setFrame:CGRectMake(110, 350, 100, 30)]; //was (110, 350, 100, 30)
	[colorPanelView addSubview:colorSaveButton];
	[colorSaveButton setImage:saveImg forState:UIControlStateNormal];
	[colorSaveButton addTarget:self action:@selector(SaveValue:) forControlEvents:UIControlEventTouchUpInside];
	int x = 15, y = 15, rowCount = 1;
	
	
//	NSArray *imgNameArray = [[NSArray alloc] initWithObjects: @"dark yellow.png", @"blue.png", @"green.png",
//							 @"red.png", @"voilet.png", @"blue red.png",@"Grey.png", @"White.png", 
//							 @"yellow.png", @"light blue.png", @"light green.png", @"Pink.png", 
//							 @"light blue1.png", @"light red.png", @"light grey.png", nil];
	
	NSArray *imgNameArray = [[NSArray alloc] initWithObjects:@"black1.png", @"charcoal1.png", @"darkblue1.png", @"blue1.png",
							 @"purple1.png", @"pink1.png", @"darkgreen1.png", @"green1.png", @"olive1.png", @"red1.png",
							 @"darkred1.png", @"teal1.png", @"blueviolet1.png", @"saddlebrown1.png", @"indigo1.png",
							 nil];
	
	for(int i = 0; i < [imgNameArray count]; i++) 
	{
		NSLog(@" value %d  , %d",x, i);
		UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setFrame:CGRectMake(x, y, 50, 50)];
		[btn setBackgroundImage:[UIImage imageNamed:[imgNameArray objectAtIndex:i]] forState:UIControlStateNormal];
		
		[btn setTag:i];
		[btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
		[colorPanelView addSubview:btn];  
		
		x = x + btn.frame.size.width + 10;
		
		if (x > 265)
		{
			x = 15; 
			y = btn.frame.origin.y + btn.frame.size.height + 15;
			rowCount++;
		}
	} 
	
    aView = [[UIView alloc] initWithFrame:CGRectMake(100, y + 30, 80, 80)]; //was y + 50
	[aView setBackgroundColor:[UIColor blackColor]];
	
	redSlider = [[UISlider alloc] initWithFrame:CGRectMake(15, y + 30, 150, 23)]; //was y + 50
	[redSlider setMaximumValue:1.0]; 
	[redSlider setMinimumValue:0.0];
	[redSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorPanelView addSubview:redSlider];
	
	greenSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 55, 150, 23)]; //was y + 75
	[greenSlider setMaximumValue:1.0]; 
	[greenSlider setMinimumValue:0.0];
	[greenSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorPanelView addSubview:greenSlider];
	
	blueSlider=[[UISlider alloc] initWithFrame:CGRectMake(15, y + 80, 150, 23)]; //was y + 100
	[blueSlider setMaximumValue:1.0]; 
	[blueSlider setMinimumValue:0.0];
	[blueSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
 
	[colorPanelView addSubview:blueSlider];
	[colorPanelView addSubview:aView];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.3];
	[colorPanelView setAlpha:1.0];
	[UIView commitAnimations];
	
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        self.title =@"Color";
        
        /*
        //Back Button
        UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
        UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
        [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [backButtonNewNav addTarget:self action:@selector(backButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
        
        barButton.tintColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
         
        [[self navigationItem] setLeftBarButtonItem:barButton];
         */
        
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

    }    
    
    // ***************** Steve End ***************************

}

-(void)SaveValue:(id)sender
{
	[[self delegate]setNewRGBColor:redSlider.value greenColor:greenSlider.value blueColor:blueSlider.value];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)btnClicked:(id)sender
{
	UIImage *image = [sender backgroundImageForState:UIControlStateNormal];
	[self getRGBAsFromImage:image atX:10 andY:10];
}


- (UIColor *)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy 
{
	// First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,bitsPerComponent, bytesPerRow, colorSpace,kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
	
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
	
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
	byteIndex += 4;
	
	[drawImage setBrushColorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0];
	
	[redSlider setValue:(rawData[byteIndex] * 1.0) / 255.0 animated:YES];
	[greenSlider setValue:(rawData[byteIndex + 1] * 1.0) / 255.0 animated:YES];
	[blueSlider setValue:(rawData[byteIndex + 2] * 1.0) / 255.0 animated:YES];
	
	UIColor *acolor = [UIColor colorWithRed:(rawData[byteIndex] * 1.0) / 255.0 green:(rawData[byteIndex + 1] * 1.0) / 255.0 blue:(rawData[byteIndex + 2] * 1.0) / 255.0 alpha:1];
	
	[aView setBackgroundColor:acolor];
    
	free(rawData);
    return acolor;
}


-(void)valueChanged:(id)sender{
	
	UIColor *previousColor = [UIColor colorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value alpha:1.0];
	[drawImage setBrushColorWithRed:redSlider.value green:greenSlider.value blue:blueSlider.value];
	[aView setBackgroundColor: previousColor];
}

-(void)viewDidLoad 
{
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}



@end




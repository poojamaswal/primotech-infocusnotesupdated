//
//  ImgDataObject.h
//  Organizer
//
//  Created by Naresh Chouhan on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ImgDataObject : NSObject 
{
    NSInteger imgid;
	NSData *imginfo;
	
	
}
@property(readwrite) NSInteger imgid;
@property(nonatomic,retain) NSData *imginfo;
@end

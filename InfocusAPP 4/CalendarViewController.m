//
//  CalendarViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 10/28/13.
//
//

#import "CalendarViewController.h"
#import "CalendarDataObject.h"
#import "AddAndEditCalendarViewController.h"

#define kCheckMarkTag	 500
#define kAddCalendarTag	 600

@interface CalendarViewController ()


@property (strong, nonatomic) IBOutlet UITableView          *calendarTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem      *doneButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem      *editButton;
@property (strong, nonatomic) EKEventStore                  *eventStore;
@property (strong, nonatomic) OrganizerAppDelegate          *appDelegate;
@property (strong, nonatomic) NSMutableArray                *sourceArray;
@property (strong, nonatomic) NSMutableArray                *calendarArray;
@property (strong, nonatomic) CalendarDataObject            *calendarObject;
@property (strong, nonatomic) NSMutableDictionary           *calendarDictionary;
@property (assign, nonatomic) BOOL                          *isEditing;
@property (strong, nonatomic) UIButton                      *checkMarkButton;
@property (assign, nonatomic) NSInteger                     checkMarkTag;
@property (strong, nonatomic) NSMutableArray                *calendarIdentifierArray;
@property (strong, nonatomic) NSIndexPath                   *indexPathForDataObject;
@property (assign, nonatomic) BOOL                          isCalendarsExtractedFromEventStore;



- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender;
- (IBAction)editButton_Clicked:(UIBarButtonItem *)sender;
- (void)extractCalendarsOutOfEventStore;
-(void)addCalendarsThatDoNotShowToUserDefault:(NSIndexPath*)indexPath;
-(void)removeCalendarsThatDoNotShowToUserDefault:(NSIndexPath*)indexPath;



@end



@implementation CalendarViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
    self.calendarIdentifierArray = [[NSMutableArray alloc]init];
    
    
    self.appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.eventStore = _appDelegate.eventStore;
    
    if (self.eventStore == nil) {
        self.eventStore = [[EKEventStore alloc]init];
    }
    
    [self extractCalendarsOutOfEventStore]; //extracts Calendars
    _isCalendarsExtractedFromEventStore = YES;
     
    
}


- (void)extractCalendarsOutOfEventStore{
    
    self.sourceArray = [[NSMutableArray alloc]init];
    self.calendarArray = [[NSMutableArray alloc]init];
    
    //if (self.calendarDictionary) {
    //    [self.calendarDictionary removeAllObjects];
   // }
    self.calendarDictionary = [[NSMutableDictionary alloc]init];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Make an array of UserDefaults for Calendars that don't show
    NSArray *calendarsThatDontShowArray = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] copy]; //make  copy
 
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// finds Calendars //////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    for (EKSource *source in self.eventStore.sources) {
        
        [self.calendarArray removeAllObjects];
        
        /*
        NSLog(@" *********************************************");
        NSLog(@" source  Title = %@", source.title);
        NSLog(@" source  = %@", source);
        NSLog(@" *********************************************");
        */
        
        
        
        NSSet *calendars = [source calendarsForEntityType:EKEntityTypeEvent];
        
        
        for (EKCalendar *calendar in calendars) {
            
            /*
            NSLog(@" *********************************************");
            NSLog(@" Calendar source Title = %@", calendar.source.title);
            NSLog(@" Calendar Title = %@", calendar.title);
            NSLog(@" Calendar Type = %d", calendar.type);
            NSLog(@" Calendar CGColor = %@", calendar.CGColor);
            //NSLog(@" Calendar allowsContentModifications = %d", calendar.allowsContentModifications);
            //NSLog(@" Calendar calendarIdentifier = %@", calendar.calendarIdentifier);
            //NSLog(@" Calendar = %@", [calendar description]);
            //NSLog(@" source = %@", [source description]);
            */
            
            self.calendarObject = [[CalendarDataObject alloc]init];
    
            [self.calendarObject setCalendarIdentifire:calendar.calendarIdentifier];
            [self.calendarObject setCalendarName:calendar.title];
            [self.calendarObject setCalendarSource:source.title];
            
            const CGFloat *component = CGColorGetComponents(calendar.CGColor);
            [self.calendarObject setCalendarColorRed:component[0]];
            [self.calendarObject setCalendarColorGreen:component[1]];
            [self.calendarObject setCalendarColorBlue:component[2]];
            
            
            if(calendar.immutable)
                [self.calendarObject setIsCalendarImmutable:YES];
            else
                [self.calendarObject setIsCalendarImmutable:NO];
            
            
            if(calendar.allowsContentModifications)
                [self.calendarObject setIsAllowsContentModifications:YES];
            else
                [self.calendarObject setIsAllowsContentModifications:NO];
            
            
            
            [self.calendarObject setIsCalendarShowing:YES]; //assumes it's showing, but will change in For loop below if not
            
            
            for (NSString *calendarIdentifier in calendarsThatDontShowArray) { //iterate thru copy of array of UserDefaults CalendarsDoNotShow
            
                if ([self.calendarObject.calendarIdentifire isEqualToString:calendarIdentifier]){ //looks for matches
                    
                    [self.calendarObject setIsCalendarShowing:NO]; //used to Uncheck Calendar in CellForRow
                    break;
                }
                
            }
            
            
            [self.calendarArray addObject: self.calendarObject]; //builds an array of Calendar objects
            
            
        }
        
        if([calendars count] > 0)
            [self.calendarDictionary setObject:[NSArray arrayWithArray:self.calendarArray] forKey:source.title];
        
    }
    

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////// Checks Calendars againts UserDefaults to make sure they all exist ////////////////////////
    ////////////// Calendar could be deleted in another app, therefore need to make a new Calendar Default list///
     /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    BOOL isCalendarFound;
    NSMutableArray *calendarsInDefaultThatMatchArray = [[NSMutableArray alloc]init];

    
    for (NSString *calendarIdentifier in calendarsThatDontShowArray) { //iterates thru copy of default calendar Array

        
        
        for (NSString *key in self.calendarDictionary) { //checks each key in Dictionsary
            

            NSArray *calendarArray = [self.calendarDictionary valueForKey:key]; //gets the Array stored in Dictionary for each source Calendar
            
            for (int x = 0; x < calendarArray.count; x++) {
                
                self.calendarObject = [calendarArray objectAtIndex:x]; //gets the object values of each Calendar
                
                if ([calendarIdentifier isEqualToString:self.calendarObject.calendarIdentifire]) {
                    
                    isCalendarFound = YES;
                    break;
                }
                else{

                    isCalendarFound = NO;
                }
                

            }
    
            if (isCalendarFound){
                [calendarsInDefaultThatMatchArray addObject:calendarIdentifier];
                break;
            }
    
        }
        
        
    }
    

    
    ////////// Update user Defaults Calendars that are Not Shown ///////////////
    [sharedDefaults setObject:calendarsInDefaultThatMatchArray forKey:@"CalendarsDoNotShow"]; //set defaults with new array
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    if (!_isCalendarsExtractedFromEventStore) { //makse sure viewDidLoad method gets called
        
        [self performSelector:@selector(delayCalendarExtraction) withObject:self afterDelay:0.10]; //Need delay to give time for EventStore to update first
    }
    
    if (IS_IOS_7) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        //pooja-iPad
        if(IS_IPAD)
        {
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
            
            _doneButton.tintColor = [UIColor whiteColor];
            _editButton.tintColor = [UIColor whiteColor];
        }
        else
        {
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0f green:66.0/255.0f blue:66.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
            
            _doneButton.tintColor = [UIColor whiteColor];
            _editButton.tintColor = [UIColor whiteColor];
        }
        
    }
    else{ //iOS 6
        
        UINavigationBar *navBarNew = [[self navigationController]navigationBar];
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    }
    
    
}

-(void) delayCalendarExtraction{
    
    [self extractCalendarsOutOfEventStore];
    [self.calendarTableView reloadData];
}


#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.calendarDictionary.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    int numberOfRows;
    //NSArray *keysArray = [self.calendarDictionary allKeys];
    
    NSArray *calendarKeyArray = [self.calendarDictionary allKeys]; //Gets the array of keys
    NSString *key = [calendarKeyArray objectAtIndex:section]; //Gets the key string for each Section (Calendar Source)
    
    NSArray *calendarArray = [self.calendarDictionary valueForKey:key]; //gets the Array stored in Dictionary for each source Calendar
    self.calendarObject = [calendarArray objectAtIndex:0]; //gets the object values of each Calendar for first row.
    
    
    //If isEditing, then must add one row to each sectin that allowsContentModification so user can add Calendars
    if (self.isEditing) {
        
        if (self.calendarObject.isAllowsContentModifications && !self.calendarObject.isCalendarImmutable) {
            
            numberOfRows = calendarArray.count + 1; //adds a row to section
        }
        else{
            
            numberOfRows = calendarArray.count;
        }

    }
    else{ //NOT isEditing
        
        numberOfRows = calendarArray.count;
    }
    

    
    return numberOfRows;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *keys = [self.calendarDictionary allKeys];
    return [keys objectAtIndex:section];
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //Section bacground color
    UIView *sectionView;
    
    //pooja-iPad
    if(IS_IPAD)
        sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 44)];
    else
        sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    sectionView.backgroundColor = [UIColor colorWithRed:235.0/255.0f green:235.0/255.0f blue:235.0/255.0f alpha:1.0f];
    
    //Section Label
    UILabel *titleLabel;
    
    //pooja-iPad
    if(IS_IPAD)
    {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 768, 25)];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    }
    else
    {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 320, 25)];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    }
    NSArray *arrayOfKeys = [self.calendarDictionary allKeys]; //puts keys into an array
    titleLabel.text = [arrayOfKeys objectAtIndex:section]; //finds the title for the section
    
    titleLabel.backgroundColor = [UIColor clearColor];
    [sectionView addSubview:titleLabel];
    
    return sectionView;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    for (UIView  *subViews in [cell.contentView subviews]) {
        [subViews removeFromSuperview];
    }
    
    
    
    //sets up Calendar Color Circle
    UIButton *btnCalColorIndicator = [[UIButton alloc] init];
    
    //sets up Title of Calendar
    UILabel *aTitleLabel = [[UILabel alloc] init];
    
    //pooja-iPad
    if(IS_IPAD)
    {
         btnCalColorIndicator.frame = CGRectMake(50, 22-10, 20, 20);
        aTitleLabel.frame=CGRectMake(btnCalColorIndicator.frame.origin.x + btnCalColorIndicator.frame.size.width + 15, 22-15, 720, GENERAL_LABEL_HEIGHT);
    }
    else
    {
        btnCalColorIndicator.frame = CGRectMake(50, 22-10, 20, 20);
        aTitleLabel.frame=CGRectMake(btnCalColorIndicator.frame.origin.x + btnCalColorIndicator.frame.size.width + 15, 22-15, 290, GENERAL_LABEL_HEIGHT);
    }
    
    
    // if isEditing, then it finds out if calendarObject "allowsContentModificaiton" and if it does, adds an "add Calendar" row
    if (self.isEditing) {
        
        
        //Set up new frames
        btnCalColorIndicator.frame = CGRectMake(10, 22-10, 20, 20);
        
        //pooja-iPad
        if(IS_IPAD)
        {
            aTitleLabel.frame=CGRectMake(btnCalColorIndicator.frame.origin.x + btnCalColorIndicator.frame.size.width + 15, 22-15, 720, GENERAL_LABEL_HEIGHT);
            [aTitleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size: 18.0]];
        }
        else
        {
            aTitleLabel.frame = CGRectMake(btnCalColorIndicator.frame.origin.x + btnCalColorIndicator.frame.size.width + 15, 22-15, 290, GENERAL_LABEL_HEIGHT);
            [aTitleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size: 15.0]];
        }
        
        NSArray *calendarKeyArray = [self.calendarDictionary allKeys]; //Gets the array of keys
        NSString *key = [calendarKeyArray objectAtIndex:indexPath.section]; //Gets the key string for each Section (Calendar Source)
        NSArray *calendarArray = [self.calendarDictionary valueForKey:key]; //gets the Array stored in Dictionary for each source Calendar
        self.calendarObject = [calendarArray objectAtIndex:0]; //gets calendarObject of first Calendar (row == 0)
        
        //Tempoarily look at index.row ==0
        //self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:0];
        
        
        
        if (self.calendarObject.isAllowsContentModifications && !self.calendarObject.isCalendarImmutable) {

            
            if (indexPath.row < calendarArray.count ) { //can see Object thats in the array.  Extra row is "Add Calendar" row
                
                self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
                
                
                cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle =UITableViewCellSelectionStyleGray;
                
                //Add Calendar Color Circle
                [btnCalColorIndicator setUserInteractionEnabled:NO];
                [btnCalColorIndicator setTag:40];
                [btnCalColorIndicator.layer setCornerRadius:10.0];
                btnCalColorIndicator.layer.masksToBounds = YES;
                btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
                btnCalColorIndicator.layer.borderWidth = 1.5;
                [btnCalColorIndicator.layer setOpaque:YES];
                [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:self.calendarObject.calendarColorRed green:self.calendarObject.calendarColorGreen blue:self.calendarObject.calendarColorBlue alpha:0.85]];
                
                [cell.contentView addSubview:btnCalColorIndicator];
                
                
                //Add Title of Calendar
                [aTitleLabel setBackgroundColor:[UIColor clearColor]];
                [aTitleLabel setTextColor:[UIColor blackColor]];
                [aTitleLabel setText:self.calendarObject.calendarName];
                [cell.contentView addSubview:aTitleLabel];
                
            }
            else{ //Extra row "Add Calendar"
                
                
                cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle =UITableViewCellSelectionStyleGray;
                
                
                //Add Title of Calendar
                [aTitleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size: 15.0]];
                [aTitleLabel setBackgroundColor:[UIColor clearColor]];
                [aTitleLabel setTextColor:[UIColor blackColor]];
                [aTitleLabel setText:@"Add Calendar"];
                [cell.contentView addSubview:aTitleLabel];
                

            }
            
            
        }
        else{ //self.calendarObject.isAllowsContentModifications = NO && !self.calendarObject.isCalendarImmutable
            
            
            self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            
            
            //Add Calendar Color Circle
            [btnCalColorIndicator setUserInteractionEnabled:NO];
            [btnCalColorIndicator setTag:40];
            [btnCalColorIndicator.layer setCornerRadius:10.0];
            btnCalColorIndicator.layer.masksToBounds = YES;
            btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
            btnCalColorIndicator.layer.borderWidth = 1.5;
            [btnCalColorIndicator.layer setOpaque:YES];
            [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:self.calendarObject.calendarColorRed green:self.calendarObject.calendarColorGreen blue:self.calendarObject.calendarColorBlue alpha:0.85]];
            
            [cell.contentView addSubview:btnCalColorIndicator];
            
            
            //Add Title of Calendar
            [aTitleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size: 15.0]];
            [aTitleLabel setBackgroundColor:[UIColor clearColor]];
            [aTitleLabel setTextColor:[UIColor blackColor]];
            [aTitleLabel setTextColor:[UIColor lightGrayColor]]; //dim color
            [aTitleLabel setText:self.calendarObject.calendarName];
            [cell.contentView addSubview:aTitleLabel];
            
            
        }
        
    }
    else{ //self.isEditing = NO;
        
        self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Make unique Tag for checkmark by using combining Section & Row and convert to intValue
        NSInteger tagCheckMark = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row].intValue;
        tagCheckMark = tagCheckMark + kCheckMarkTag;
        
        
        //Checkmark
        UIImage *checkMarkImage =[[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]];
        self.checkMarkButton = [[UIButton alloc] init];
        if(IS_IPAD)
            [self.checkMarkButton setFrame: CGRectMake(13, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
            else
        [self.checkMarkButton setFrame: CGRectMake(3, 44/2 - checkMarkImage.size.height/2-2 + 3, checkMarkImage.size.width * 0.75, checkMarkImage.size.height * 0.75)];
        [self.checkMarkButton setTag:tagCheckMark];
        [self.checkMarkButton setImage:checkMarkImage forState:UIControlStateNormal];
        
        [cell.contentView addSubview:self.checkMarkButton];
        
        
        //if Not isCalendarShowing, then needs to hide checkmark.
        if (!self.calendarObject.isCalendarShowing ) {
            
            self.checkMarkButton.hidden = YES;
        }
        
        //Add Calendar Color Circle
        [btnCalColorIndicator setUserInteractionEnabled:NO];
        [btnCalColorIndicator setTag:40];
        [btnCalColorIndicator.layer setCornerRadius:10.0];
        btnCalColorIndicator.layer.masksToBounds = YES;
        btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f  blue:220.0/255.0f  alpha:0.65].CGColor;
        btnCalColorIndicator.layer.borderWidth = 1.5;
        [btnCalColorIndicator.layer setOpaque:YES];
        [btnCalColorIndicator setBackgroundColor:[UIColor colorWithRed:self.calendarObject.calendarColorRed green:self.calendarObject.calendarColorGreen blue:self.calendarObject.calendarColorBlue alpha:0.85]];
        
        [cell.contentView addSubview:btnCalColorIndicator];
        
        
        //Add Title of Calendar
        [aTitleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size: 15.0]];
        [aTitleLabel setBackgroundColor:[UIColor clearColor]];
        [aTitleLabel setTextColor:[UIColor blackColor]];
        [aTitleLabel setText:self.calendarObject.calendarName];
        [cell.contentView addSubview:aTitleLabel];
        
        cell.selectionStyle =UITableViewCellSelectionStyleNone;

    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (self.isEditing) {
        
        //Looks at first row to see if "isAllowsContentModification", etc
        self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:0];
        
        if (!self.calendarObject.isCalendarImmutable && self.calendarObject.isAllowsContentModifications) {
            
            self.indexPathForDataObject = indexPath;
            [self performSegueWithIdentifier: @"AddAndEditCalendarViewController" sender: self];
        }

        
    }
    else{ 
        
        //Make unique Tag for checkmark by using combining Section & Row and convert to intValue
        self.checkMarkTag = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row].intValue;
        self.checkMarkTag = self.checkMarkTag + kCheckMarkTag;
        
        
        if ([self.view viewWithTag:self.checkMarkTag].hidden) {
            
            [[self.view viewWithTag:self.checkMarkTag] setHidden:NO];
            [self removeCalendarsThatDoNotShowToUserDefault:indexPath]; //method to remove calendar from UserDefault array
            
        }
        else{
            
            [[self.view viewWithTag:self.checkMarkTag] setHidden:YES];
            [self addCalendarsThatDoNotShowToUserDefault:indexPath];//method to add calendar to UserDefault array
        }

        
    }
    

    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    _isCalendarsExtractedFromEventStore = NO; //sets this so events will be extracted when "poped" back
    
    if([segue.identifier isEqualToString:@"AddAndEditCalendarViewController"])
    {
    
        NSInteger section = self.indexPathForDataObject.section;
        NSInteger row = self.indexPathForDataObject.row;
        NSInteger ObjectCount = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:section]] count];

        
        AddAndEditCalendarViewController *addAndEditCalendarViewController = segue.destinationViewController;
        
        
        if (self.isEditing && row < ObjectCount) //Edit Existing Calendar
        {
            self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:section]] objectAtIndex:row];
            addAndEditCalendarViewController.isEditing = YES;
            addAndEditCalendarViewController.calendarObject = self.calendarObject;
        }
        else //Add Calendar
        {
            //takes calendarObject of one row less than this one, so can pass along Calendar Source information
            self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:section]] objectAtIndex:row - 1];
            
            addAndEditCalendarViewController.isEditing = NO;
            addAndEditCalendarViewController.calendarObject = self.calendarObject;
        }

        
    }
    
}


-(void)addCalendarsThatDoNotShowToUserDefault:(NSIndexPath*)indexPath{
    
    self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Add new calendar Identifier to NSUserDefaults
    NSMutableArray *mutableArrayCopy = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] mutableCopy]; //make mutable copy
    
    [mutableArrayCopy addObject:self.calendarObject.calendarIdentifire]; //Add Object
    [sharedDefaults setObject:mutableArrayCopy forKey:@"CalendarsDoNotShow"]; //set defaults with new array
    
    [self extractCalendarsOutOfEventStore]; //sets all the correct dataObjects for tableview
    
}


-(void)removeCalendarsThatDoNotShowToUserDefault:(NSIndexPath*)indexPath{
    
    
    self.calendarObject = [[self.calendarDictionary valueForKey:[[self.calendarDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Add new calendar Identifier to NSUserDefaults
    NSMutableArray *mutableArrayCopy = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] mutableCopy]; //make mutable copy
    NSMutableArray *discardedItems = [NSMutableArray array]; //make an array of objects to be removed
    
    
    ////////// remove calendarIdentifier items from array //////////////////
    for (NSString *calIdentifier in mutableArrayCopy){
        
        if ([calIdentifier isEqualToString:self.calendarObject.calendarIdentifire]) {
            
            [discardedItems addObject:calIdentifier]; //array of deleted items
        }
    }
    
    [mutableArrayCopy removeObjectsInArray:discardedItems]; //removes all discarded items
    [sharedDefaults setObject:mutableArrayCopy forKey:@"CalendarsDoNotShow"]; //set defaults with modified array
    
    [self extractCalendarsOutOfEventStore]; //sets all the correct dataObjects for tableview


}


- (IBAction)doneButton_Clicked:(UIBarButtonItem *)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)editButton_Clicked:(UIBarButtonItem *)sender {
    
    
    if ([_editButton.title isEqualToString:@"Edit"]) {
        
         _editButton.title = @"Done";
        self.navigationItem.rightBarButtonItem = nil;
        self.isEditing = YES;
        
        [self.tableView reloadData];
        
        //Animates the tableview
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]    withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
    }
    else if ([_editButton.title isEqualToString:@"Done"]) {
        
        _editButton.title = @"Edit";
        self.navigationItem.rightBarButtonItem = _doneButton;
        self.isEditing = NO;
        
        [self.tableView reloadData];
        
        //Animates the tableview
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]    withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







@end









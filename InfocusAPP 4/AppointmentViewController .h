//
//  AppointmentViewController .h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetAllDataObjectsClass.h"
#import "AllInsertDataMethods.h"
#import "AppointmentsDataObject.h"
#import "AddEditNotesViewController.h"
#import "OrganizerAppDelegate.h"
#import "LocationDataObject.h"
#import "InputTypeSelectorViewController.h"
#import <EventKit/EventKit.h> //Steve
#import <EventKitUI/EventKitUI.h> //Steve
#import "HandWritingViewController.h" //Steve


@class OrganizerAppDelegate;
@class AddEditTitleViewController;


@protocol AppointmentViewControllerDelegate
-(void)setNote:(NSString*)anote;
-(void)setTitle:(NSString*)atitle;
-(void)setStartDateLabel:(NSString*)aStartDate;
-(void)setEndDateLabel:(NSString*)aEndDate;
-(void)setIsAllDay:(BOOL)isallDay;
-(void)setStartDateInternational:(NSDate*)startDate_DateFormat; //Steve
-(void)setEndDateInternational:(NSDate*)endDate_DateFormat; //Steve
-(void)setLocObject:(LocationDataObject*)locObj selectedval:(NSString*)val;
-(void)setRepeatString:(NSString *) repeat;
-(void)setAlertString:(NSString *)  alert;
-(void)setSecondAlertString:(NSString *)  alert;
-(void)setCalendarName:(NSString *) calName calID:(NSInteger)calId;
//Alok Added for set calendar obj
-(void)setCalendarData:(CalendarDataObject *) calObj;
@end



@interface AppointmentViewController : UIViewController <UITextFieldDelegate,UIActionSheetDelegate, UIPickerViewDelegate, UIPickerViewDataSource, HandWritingDelegate>{
	
    UIButton                *titleLocButton;
    UIImageView             *titleImgView;
    UIButton                *saveButton;
    UIButton                *updateButton;
    UIButton                *deleteButton;
    
    UIButton                *alertButton;
    UIButton                *notesButton;
    UIButton                *calendarButton;
    UIButton                *secondalertButton;
    UIButton                *notes_discButton;
    UIButton                *calendar_discButton;
    UIButton                *secondalert_discButton;
    UILabel                 *notes_lbl;
    UILabel                 *calendar_lbl;
    UILabel                 *secondalert_lbl;
    UILabel                 *secondalertsLabel;
    UIButton                *inputSelecterButton;
    
    __unsafe_unretained IBOutlet UIButton *btn_CustomView;
    
    //******* 
	
    UILabel                 *titleLabel;
    UILabel                 *locationLabel;
	///
    UITextField             *titleTxtFld;
    UITextField             *locTxtFld;
	InputTypeSelectorViewController	*inputTypeSelectorViewController;
	NSString				*locationtext;
    NSString				*locationID;
    NSString                *caleIdentifire;
	id						__unsafe_unretained delegate;
	//
    UILabel                 *startsLabel;
    UILabel                 *endsLabel;
    UILabel                 *repeatLabel;
    UILabel                 *alertsLabel;
    UILabel                 *notesLabel;
    UILabel                 *calendarLabel;
	
    __unsafe_unretained IBOutlet UILabel *alert_lbl;
    LocationDataObject		*aLocObj;
    AppointmentsDataObject	*aAppObj;
	BOOL                    isAdd;
	BOOL                    isAllDay;
	
    NSString				*appointmentTitle;
	NSString				*appointmentType;
	NSData					*appointmentBinary;
	NSData					*appointmentBinaryLarge;
	
	NSString *currentDate;
	OrganizerAppDelegate *appDelegate;
	UIPanGestureRecognizer *pan;
    
    NSInteger GetappID;
    UILocalNotification *notification ;
    IBOutlet UINavigationBar *navBar;
    
    EKEventStore        *eventStore; //Steve added
    EKEvent             *event;  //Steve added
    CalendarDataObject *defaultcal;//Alok Added
    NSString            *relation_Field1; //Steve - stores evetnIdentifier
    
    //Steve
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *titleLocationButton;
    IBOutlet UILabel *titleLineDividerLabel;
    
    IBOutlet UIButton *startButton;
    IBOutlet UILabel *startLabel;
    IBOutlet UILabel *endLabel;
    IBOutlet UIButton *startDisclosureButton;
    
    IBOutlet UIButton *repeatButton;
    IBOutlet UIButton *repeatDisclosureButton;
    IBOutlet UILabel *repeatFixedLabel;
    
    IBOutlet UILabel *alert_1_FixedLabel;
    IBOutlet UIButton *alert1DisclosureButton;
    
    BOOL                isShowStartDatePicker; //Steve - show start date picker
    BOOL                isShowEndDatePicker; //Steve - show end date picker
    
    BOOL                isShowAlert_1_Picker; //Steve - show Alert 1  picker
    BOOL                isShowAlert_2_Picker; //Steve - show Alert 2  picker

    __weak IBOutlet UIBarButtonItem *cancelButton_Ipad;
    
}
//
@property(nonatomic,assign)  NSInteger GetappID;
@property (nonatomic, strong)  UITextField		*locTxtFld;
@property (unsafe_unretained) id delegate;
@property(nonatomic, strong) UITextField			*titleTxtFld;
@property(nonatomic, strong) NSString				*locationtext;
@property(nonatomic, strong) NSString				*locationID;
//
@property (nonatomic,strong) IBOutlet UILabel		*notesLabel;
@property (nonatomic,strong) LocationDataObject		*aLocObj;
@property (nonatomic,strong) AppointmentsDataObject *aAppObj;
@property (assign) BOOL isAdd;
@property (nonatomic,assign) BOOL isAllDay;
@property (nonatomic, strong) NSString		*appointmentTitle;
@property (nonatomic, strong) NSString		*appointmentType;
@property (nonatomic, strong) NSData		*appointmentBinary;
@property (nonatomic, strong) NSData		*appointmentBinaryLarge;

@property (nonatomic, strong) EKEventStore      *eventStore; //Steve added
@property (nonatomic, strong) EKEvent           *event;     //Steve added
@property (nonatomic, strong) NSString          *relation_Field1; //Steve - stores evetnIdentifier

@property (nonatomic, strong) UIDatePicker      *startDatePicker; //Steve - StartDate picker
@property (nonatomic, strong) UIDatePicker      *endDatePicker; //Steve - StartDate picker

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@property (strong, nonatomic) NSMutableArray    *alertPickerArray;//Steve

@property (strong, nonatomic) UIDatePicker      *alert_1_Picker; //Steve
@property (strong, nonatomic) UIPickerView      *alertCustomPicker_1; //Steve

@property (strong, nonatomic) UIDatePicker      *alert_2_Picker; //Steve
@property (strong, nonatomic) UIPickerView      *alertCustomPicker_2; //Steve

@property (strong, nonatomic) UISegmentedControl *Alert_1segmentedControl; //Steve
@property (strong, nonatomic) UISegmentedControl *Alert_2segmentedControl; //Steve

@property (strong, nonatomic) NSDate   *startDateInternationalCompatible; //Steve - Start Date - handle international dates
@property (strong, nonatomic) NSDate    *endDateInternationalCompatible; //Steve - End Date - handle international dates

@property (strong, nonatomic) NSDate    *alert_1_Date; //Steve - Handle International Dates
@property (strong, nonatomic) NSDate    *alert_2_Date; //Steve - Handle International Dates

@property (strong, nonatomic) UISwitch  *allDaySwitch;//Steve
@property (strong, nonatomic) UIImageView *blurredImageView; //Steve - Blurred Image of screenshot of AppointmentViewController

@property (assign, nonatomic) BOOL isHandwritingShowing; //Steve


-(void) showHandwritingForiPad; //Steve - used to show HW for iPad by calling ViewWillAppear
-(IBAction) backClicked:(id)sender;
//-(IBAction) titleLocatio_buttonClicked:(id)sender;
-(IBAction) startsEnds_buttonClicked:(id)sender;
-(IBAction) repeat_buttonClicked:(id)sender;
-(IBAction) alerts_buttonClicked:(id)sender;
-(IBAction) alertsecond_buttonClicked:(id)sender;
-(IBAction) notes_buttonClicked:(id)sender;
-(IBAction) calendar_buttonClicked:(id)sender;
-(IBAction) save_buttonClicked:(id)sender;
-(IBAction) update_buttonClicked:(id)sender;
-(IBAction) delete_buttonClicked:(id)sender;
-(IBAction) detDiscBtn_Clicked:(UIButton *)sender;

-(void) startPickerChanged; //Steve
-(void) endPickerChanged; //Steve

-(void) alert_1_PickerValueChanged; //Steve
-(void) alert_2_PickerValueChanged; //Steve

-(void) alert_1SegmentedControlValueChanged; //Steve
-(void) alert_2SegmentedControlValueChanged; //Steve

-(void) allDaySwitchChangedValue; //Steve


-(id)initWithDataObj: (AppointmentsDataObject *) appData;

-(IBAction)inputTypeSelectorButton_Clicked:(id)sender;
//-(IBAction)selectLocationButton_Clicked:(id)sender;

- (IBAction)textFieldDidEndEditing:(UITextField *)textField;
- (IBAction)textFieldDidBeginEditing:(UITextField *)textField;

-(BOOL) setAlertForSpecificWeekDay:(int)_weekDay;

-(UIColor*) getCalNameData:(NSInteger)CalID ;//alok added

@end

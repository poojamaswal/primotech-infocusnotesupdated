//
//  PasswordProtectAppViewController.m
//  Organizer
//
//  Created by Steven Abrams on 7/4/13.
//
//

#import "PasswordProtectAppViewController.h"
#import "SettingsTableViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface PasswordProtectAppViewController (){
    
    UIBarButtonItem *back;
    
}

-(void) SavePassword; //saves password
-(void) checkPassword; //checks to see if password is good
-(void) eraseOldPassword; //erases old password so new one can be saved
-(void) addCancelButtonToNavigation;//sets up cancel button
-(void) cancelButton_Clicked;
- (void)authenticateTouchID; //Used Touch ID to log on

@end

@implementation PasswordProtectAppViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentViewController{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.parentVC = parentViewController;
        
        NSLog(@"self.parentVC =  %@", self.parentVC);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
   // self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
    //self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    //Hide Touch ID label & Switch until know if device & iOS support it
    _touchIDLabel.hidden = YES;
    _touchIDSwitch.hidden = YES;
    _touchIDTextView.hidden = YES;
    
    
    //*************** Would you like to password protect this app? ******************************
     /* primo
    //Custom "No" Button
    [self.passwordNOButton setTitle:@"No" forState:UIControlStateNormal];
    [self.passwordNOButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.passwordNOButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.passwordNOButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    self.passwordNOButton.layer.cornerRadius = 10.0;
    self.passwordNOButton.layer.borderWidth = 1.0;
    self.passwordNOButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    //self.laterRateButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.passwordNOButton.backgroundColor = [UIColor darkGrayColor];
    
    //Custom "Yes" Button
    [self.passwordYESButton setTitle:@"Yes" forState:UIControlStateNormal];
    [self.passwordYESButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.passwordYESButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.passwordYESButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    self.passwordYESButton.layer.cornerRadius = 10.0;
    self.passwordYESButton.layer.borderWidth = 1.0;
    self.passwordYESButton.layer.borderColor = [[UIColor grayColor]CGColor];
    self.passwordYESButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
     */
    //*************************** End ***********************************************************
    
    
    //*************** Would you like CHANGE the password? ******************************
   
    //Custom "No" Button
 
    
    [self.passwordChangeNOButton setTitle:@"No" forState:UIControlStateNormal];
    [self.passwordChangeNOButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.passwordChangeNOButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.passwordChangeNOButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    self.passwordChangeNOButton.layer.cornerRadius = 10.0;
    self.passwordChangeNOButton.layer.borderWidth = 1.0;
    self.passwordChangeNOButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    //self.laterRateButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.passwordChangeNOButton.backgroundColor = [UIColor darkGrayColor];
    
    //Custom "Yes" Button
    [self.passwordChangeYESButton setTitle:@"Yes" forState:UIControlStateNormal];
    [self.passwordChangeYESButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.passwordChangeYESButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.passwordChangeYESButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    self.passwordChangeYESButton.layer.cornerRadius = 10.0;
    self.passwordChangeYESButton.layer.borderWidth = 1.0;
    self.passwordChangeYESButton.layer.borderColor = [[UIColor grayColor]CGColor];
    self.passwordChangeYESButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
   
    //*************************** End ************************************************************

    
    //Custom "Save" button
    /* Primo
    [self.saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.saveButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    self.saveButton.layer.cornerRadius = 10.0;
    self.saveButton.layer.borderWidth = 1.0;
    self.saveButton.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.saveButton.backgroundColor = [UIColor darkGrayColor];
*/
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL isTouchIDAvail = [self isTouchIDAvailable]; //Checks if device supports TouchID
    BOOL isTouchIDON = [sharedDefaults boolForKey:@"TouchIDSwitchOn"];//Set TouchID switch
    
    if (isTouchIDON) {
        _touchIDSwitch.on = YES;
    }
    else{
        _touchIDSwitch.on = NO;
    }
    
    
    //Checks to see if view came from SettingTableViewController
    if([self.parentVC isKindOfClass:[SettingsTableViewController class]] ) {
        

        self.navigationController.navigationBarHidden = NO;
        
        [self addCancelButtonToNavigation]; //Adds a cancel Button
        
        
        //************** Already Password Protected *******************
        //************** Change Password View opens first, then Password text fields
        if ([sharedDefaults boolForKey:@"PasswordLockApp"]){  //Password Protected
            
            self.title = @"Change Password";
            
            self.passwordONOFFSwitch.hidden = NO;
            self.passwordONOFFLabel.hidden = NO;
            self.titleLabel.hidden = YES;
            
            [self.passwordONOFFSwitch setOn:YES];
            
            [self.view addSubview:self.passwordChangeView];
            
           	pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
            [self.view addGestureRecognizer:pan];
            
            if (isTouchIDAvail) {
                
                _touchIDLabel.hidden = NO;
                _touchIDSwitch.hidden = NO;
                _touchIDTextView.hidden = NO;
            }
            
        }
        //****************** No Password ******************
        //********** Password view with text fields open ************
        else{
            
            self.title = @"Add Password";
            
            self.passwordTextField.hidden = YES; //password not yet set so hide this field
            self.titleLabel.hidden = YES;
           
            self.passwordONOFFSwitch.hidden = YES;
            self.passwordONOFFLabel.hidden = YES;
            
            
            if (isTouchIDAvail) {
                
                _touchIDLabel.hidden = NO;
                _touchIDSwitch.hidden = NO;
                _touchIDTextView.hidden = NO;
                
                //Moves TouchID up
                _touchIDLabel.frame = CGRectMake(30, 217, 145, 21);
                _touchIDSwitch.frame = CGRectMake(228, 212, 51, 31);
                _touchIDTextView.frame = CGRectMake(30, 290 - 40, 252, 45);
            }
        }
        
    }
    //*********** First Time App Opens **********************************
    else if ( [sharedDefaults boolForKey:@"PasswordSetUp"] && ![sharedDefaults boolForKey:@"PasswordLockApp"] ){
        
        self.navigationController.navigationBarHidden = NO;
        self.navigationItem.title=@"Password Protect";
        self.navigationItem.hidesBackButton = YES;
        
        self.passwordTextField.hidden = YES;
        self.passwordONOFFLabel.hidden = YES;
        self.passwordONOFFSwitch.hidden = YES;
        
        self.passwordTextField_1.hidden = NO;
        self.passwordLabel.hidden = NO;
        self.passwordTextField_2.hidden = NO;
        self.reenterpasswordLabel_1.hidden = NO;
        self.reenterpasswordLabel_2.hidden = NO;
        
        
        //Primo
        //self.passwordProtectSetupView.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        
        
        //check device for touchID
        BOOL isTouchIDAvail = [self isTouchIDAvailable];
        
        if (IS_IOS_8 && isTouchIDAvail) {
            
            _touchIDLabel.hidden = NO;
            _touchIDSwitch.hidden = NO;
            _touchIDTextView.hidden = NO;
            
            //Adjust TouchID frame up
            _touchIDLabel.frame = CGRectMake(30, 217, 145, 21);
            _touchIDSwitch.frame = CGRectMake(228, 212, 51, 31);
            _touchIDTextView.frame = CGRectMake(30, 290 - 40, 252, 45);
        }
        else{
            
            _touchIDLabel.hidden = YES;
            _touchIDSwitch.hidden = YES;
        }
        
        
        [self.view addSubview:self.passwordProtectSetupView];
    }
    //***************** On Startup, Password Protected **************************
    else if ([sharedDefaults boolForKey:@"PasswordLockApp"]){

        self.title = @"LOGIN";
        self.navigationController.navigationBarHidden = NO;
        self.navigationItem.hidesBackButton = YES;
        
        //Not needed since password already stored
        [self.saveButton setTitle:@"Login" forState:UIControlStateNormal];
        
        self.passwordTextField.hidden = NO;
       // self.passwordTextField.frame = CGRectMake(20, self.saveButton.frame.origin.y + 20, 280, 30);
        
        self.passwordTextField_1.hidden = YES;
        self.passwordLabel.hidden = YES;
        self.passwordTextField_2.hidden = YES;
        self.reenterpasswordLabel_1.hidden = YES;
        self.reenterpasswordLabel_2.hidden = YES;
        self.titleLabel.hidden = YES;
        
        self.passwordONOFFLabel.hidden = YES;
        self.passwordONOFFSwitch.hidden = YES;
    
        
         //check device for touchID
        if (IS_IOS_8 && isTouchIDAvail && isTouchIDON) { //Authenticate Touch ID
            
            _touchIDLabel.hidden = NO;
            _touchIDSwitch.hidden = NO;
            _touchIDTextView.hidden = NO;
            
            //Adjust TouchID frame up
            _touchIDLabel.frame = CGRectMake(30, 217, 145, 21);
            _touchIDSwitch.frame = CGRectMake(228, 212, 51, 31);
            _touchIDTextView.frame = CGRectMake(30, 290 - 40, 252, 45);
            
            [self authenticateTouchID];  //Authenticate TouchID
        }
        else if (IS_IOS_8 && isTouchIDAvail && !isTouchIDON){ //TouchID Switch is off. "isTouchIDON" is declared before If statements
            
            _touchIDLabel.hidden = NO;
            _touchIDSwitch.hidden = NO;
            _touchIDTextView.hidden = NO;
            
            //Adjust TouchID frame up
            _touchIDLabel.frame = CGRectMake(30, 217, 145, 21);
            _touchIDSwitch.frame = CGRectMake(228, 212, 51, 31);
            _touchIDTextView.frame = CGRectMake(30, 290 - 40, 252, 45);
            
        }
        else{  //TouchID not Available on device or not using iOS8
            
            _touchIDLabel.hidden = YES;
            _touchIDSwitch.hidden = YES;
            _touchIDTextView.hidden = YES;
        }
    
    }
    
    
   
    
   // NSLog(@" self.saveButton.frame = %@", NSStringFromCGRect(self.saveButton.frame));
    
}

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//    
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    //self.view.backgroundColor = [UIColor whiteColor];
//    if(IS_IPAD)
//         self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    else
//        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
//    //self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
//    //self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
//    
//   
//    
//}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
    
    //self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
}
//Sees if TouchID can be used on device
- (BOOL) isTouchIDAvailable {
    
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        
        NSLog(@"%@", [authError localizedDescription]);
        return NO;
    }
    return YES;
}


- (void)authenticateTouchID{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:YES forKey:@"TouchIDConfirmed"];
    
    // Touch ID must be called with a high priority queue, otherwise it might fail.
    // Also, a dispatch_after is required, otherwise we might receive "Pending UI mechanism already set."
    dispatch_queue_t highPriorityQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    //Must delay TouchID, otherwise if user has TouchID pressed when app starts, app will freeze whole device.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.75 * NSEC_PER_SEC), highPriorityQueue, ^{
        LAContext *context = [[LAContext alloc] init];
        NSError *error = nil;
        
        // Check if device supports TouchID
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            
            // TouchID supported, show it to user
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:@"Unlock InFocus Pro"
                              reply:^(BOOL success, NSError *error) {
                                  
                                  if (success) {
                                      // This action has to be on main thread and must be synchronous
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          NSLog(@"success");
                                          [sharedDefaults setBool:YES forKey:@"PasswordAccepted"]; //user TouchID Accepted
                                          [self.navigationController popViewControllerAnimated:NO];
                                      });
                                  }
                                  else {
                                      
                                      switch (error.code)
                                      {
                                          case LAErrorAuthenticationFailed:
                                          {
                                              NSLog(@"Authentication Failed");
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Failed"
                                                                                                  message:@"Please log on with your password"
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"Ok"
                                                                                        otherButtonTitles:nil];
                                                  [alert show];
                                              });
                                              
                                          }
                                              break;
                                              
                                          case LAErrorUserCancel:
                                              NSLog(@"User pressed Cancel button");
                                              break;
                                              
                                          case LAErrorUserFallback:
                                          {
                                              NSLog(@"User pressed \"Enter Password\"");
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                   NSLog(@"Enter Password");
                                              });
                                          }
                                              break;
                                              
                                          default:
                                          {
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  NSLog(@"error = %@", error.debugDescription);
                                                  
                                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Touch ID is not configured"
                                                                                                  message:@"Please log on with your password"
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"Ok"
                                                                                        otherButtonTitles:nil];
                                                  
                                                  [alert show];
                                                 
                                              });

                                          }
                                              
                                              break;
                                      }
                                      
                                      
                                      
                                  }
                              }];
        }
        else //No TouchID
        {
            _touchIDLabel.hidden = YES;
            _touchIDSwitch.hidden = YES;
        }
    });
    
    
}


-(void) addCancelButtonToNavigation{
    
    
    if (!IS_IOS_7) {
        
        //Cancel Button
        UIImage *backButtonImage = [UIImage imageNamed:@"CancelSmallBtn.png"];
        UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
        [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [backButtonNewNav addTarget:self action:@selector(cancelButton_Clicked) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
        
        [[self navigationItem] setLeftBarButtonItem:barButton];
        
    }
    
}



-(void) cancelButton_Clicked{
    
     [self.navigationController popViewControllerAnimated:YES];
}


//Releases keyboard so user can see other buttons
- (void)panRecognized:(UIPanGestureRecognizer *)gestureRecognizer {
    switch ([gestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
		{
			[self.passwordTextField_1 resignFirstResponder];
            [self.passwordTextField_2 resignFirstResponder];
		}
            break;
        case UIGestureRecognizerStateEnded:
		{
            [self.passwordTextField_1 resignFirstResponder];
            [self.passwordTextField_2 resignFirstResponder];
		}
            break;
            
        default:
            break;
    }
	
    
}


- (IBAction)passwordNO_Clicked:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"PasswordLockApp"]; //user does Not want password protection
    [sharedDefaults setBool:NO forKey:@"PasswordSetUp"]; //user viewed password setup view
    [sharedDefaults setBool:YES forKey:@"PasswordAccepted"]; //user viewed password setup view
    
 
    NSString *passwordBlank = @""; 
    [sharedDefaults setObject:passwordBlank forKey:@"Pass"]; //changes password to ""
    
    //NSLog(@" objectForKey Pass =  %@", [sharedDefaults objectForKey:@"Pass"]);
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)passwordYES_Clicked:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    [sharedDefaults setBool:YES forKey:@"PasswordSetUp"]; //user wants password protection

    [self.passwordProtectSetupView removeFromSuperview];
    
    
    back = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonAction:)];
    back.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc] initWithObjects:back,nil];
    
    
}

-(void)backBarButtonAction:(id)sender
{
    back.enabled = NO;
    back.title = @" ";
   // [self.view removeFromSuperview];
    [self.view addSubview:self.passwordProtectSetupView];
}


- (IBAction)passwordChangeNO_Clicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)passwordChangeYES_Clicked:(id)sender {
    
    isChangePassword = YES;
    
    self.passwordTextField.hidden = YES;
    
    self.passwordTextField_1.hidden = NO;
    self.passwordLabel.hidden = NO;
    self.passwordTextField_2.hidden = NO;
    self.reenterpasswordLabel_1.hidden = NO;
    self.reenterpasswordLabel_2.hidden = NO;

    
    [self.passwordChangeView removeFromSuperview];
    
}

- (IBAction)passwordSaveButtonClicked:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([sharedDefaults boolForKey:@"PasswordLockApp"]) { //if user selected to password protect app
        
        if (isChangePassword) {
            
            if (![self.passwordTextField_1.text isEqualToString:self.passwordTextField_2.text]) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Does Not Match"
                                                                message:@"The password you entered does not match.  Please re-type your password."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else if ([self.passwordTextField_1.text isEqualToString:@""]){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error"
                                                                message:@"The password you entered can not be blank."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else{
                [self SavePassword];
                isChangePassword = NO;
            }
            

        }
        
        
        
        else{
            [self checkPassword];
        }
    }
    else if (![self.passwordTextField_1.text isEqualToString:self.passwordTextField_2.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Does Not Match"
                                                        message:@"The password you entered does not match.  Please re-type your password."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([self.passwordTextField_1.text isEqualToString:@""]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error"
                                                        message:@"The password you entered can not be blank."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        [self SavePassword];
    }
    
}

//Password Switeched off
- (IBAction)passwordONOFF_Clicked:(id)sender {
    
    if ( ![self.passwordONOFFSwitch isOn]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Turn Off Password"
                                                        message:@"Are you sure you want to turn off password protection?"
                                                       delegate:self
                                              cancelButtonTitle:@"NO"
                                              otherButtonTitles:@"YES",nil];
        [alert show];
    }
    

    
}

- (IBAction)touchIDSwithChanged:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
     if([self.parentVC isKindOfClass:[SettingsTableViewController class]] )
     {
         if (_touchIDSwitch.on) {
             [sharedDefaults setBool:YES forKey:@"TouchIDSwitchOn"];
         }
         else{
             [sharedDefaults setBool:NO forKey:@"TouchIDSwitchOn"];
         }
         
     }
    else
    {
        if (_touchIDSwitch.on) {
            [sharedDefaults setBool:YES forKey:@"TouchIDSwitchOn"];
            [self authenticateTouchID]; //can do TouchID here since user already has a password
        }
        else{
            [sharedDefaults setBool:NO forKey:@"TouchIDSwitchOn"];
        }
    }
    
    

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex==0){
        [self.passwordONOFFSwitch setOn:YES];
    }
    else if (buttonIndex == 1){
        [self eraseOldPassword];
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        [sharedDefaults setBool:NO forKey:@"PasswordLockApp"]; //user does Not want password protection
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}


//if user selected to password protect app
-(void) checkPassword{
    
    
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
   
    NSString *password = [sharedDefaults objectForKey:@"Pass"];
    
    
    //NSLog(@"Pass = %@", [sharedDefaults objectForKey:@"Pass"]);
    
    
    if ([self.passwordTextField.text isEqualToString:password]) {
        
        [sharedDefaults setBool:YES forKey:@"PasswordAccepted"]; //user entered correct password
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error"
                                                        message:@"The password you entered is not correct.  Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
    
}

//Saves the password
-(void) SavePassword{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //If user wants to change password, must erase old one
    if (isChangePassword) {
        [self eraseOldPassword];
    }
    
    NSString *newPassword = self.passwordTextField_1.text;
    [sharedDefaults setObject:newPassword forKey:@"Pass"]; //new password
    
    [sharedDefaults setBool:YES forKey:@"PasswordAccepted"]; //user entered correct password
    [sharedDefaults setBool:YES forKey:@"PasswordLockApp"]; //user does want password protection
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    //NSLog(@"Pass =  %@", [sharedDefaults objectForKey:@"Pass"]);
    
   }

-(void) eraseOldPassword{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];

    NSString *newPassword = @"";
    [sharedDefaults setObject:newPassword forKey:@"Pass"]; //new password is blank
    
    NSLog(@"Pass = %@", [sharedDefaults objectForKey:@"Pass"]);


}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    self.navigationController.navigationBarHidden = NO;
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setPasswordNOButton:nil];
    [self setPasswordYESButton:nil];
    [self setPasswordProtectSetupView:nil];
    [self setPasswordTextField_1:nil];
    [self setPasswordTextField_2:nil];
    [self setReenterpasswordLabel_2:nil];
    [self setReenterpasswordLabel_1:nil];
    [self setSaveButton:nil];
    [self setPasswordTextField:nil];
    [self setPasswordTextField:nil];
    [self setPasswordLabel:nil];
    [self setPasswordChangeNOButton:nil];
    [self setPasswordChangeYESButton:nil];
    [self setPasswordChangeView:nil];
    [self setPasswordONOFFLabel:nil];
    [self setPasswordONOFFSwitch:nil];
    [self setTitleLabel:nil];
    [super viewDidUnload];
}
@end

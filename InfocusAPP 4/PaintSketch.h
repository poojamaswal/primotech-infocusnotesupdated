//
//  PaintSketch.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

//CONSTANTS:
// (Steve)
#define kBrushOpacity	 (1.0 / 1.0)
#define kBrushPixelStep	 0.10
#define kBrushScale	 3.5
#define kLuminosity	 1.0
#define kSaturation	 8.0



//#define kBrushOpacity		(1.0 / 1.0)
//#define kBrushPixelStep		1
//#define kBrushScale			3
//#define kLuminosity			0.75
//#define kSaturation			1.0

//CLASS INTERFACES:

@interface PaintSketch : UIView{
@private
	// The pixel dimensions of the backbuffer
	GLint backingWidth;
	GLint backingHeight;
	
	EAGLContext *context;
	
	// OpenGL names for the renderbuffer and framebuffers used to render to this view
	GLuint viewRenderbuffer, viewFramebuffer;
	
	// OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist)
	GLuint depthRenderbuffer;
	
	GLuint	brushTexture;
	CGPoint	location;
	CGPoint	previousLocation;
	Boolean	firstTouch;
	Boolean needsErase;	
    
    BOOL isTouchAgain;
    CGFloat verticleDrowLimit;
    CGFloat horizontalDrawLimit;
    BOOL mouseSwiped;
}

@property(nonatomic, readwrite) CGPoint location;
@property(nonatomic, readwrite) CGPoint previousLocation;

- (void)erase;
- (UIImage *)getImage;
- (void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

@end


#import <UIKit/UIKit.h>
#import "ACEDrawingView.h"

@interface PanView : UIView

@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, strong) UIImage *displayImage;
@property (nonatomic, strong) IBOutlet ACEDrawingView *drawingView;

@end

//
//  TextData.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/12/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NotesID;

NS_ASSUME_NONNULL_BEGIN

@interface TextData : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+(TextData*)addTextData:(NSDictionary*)dictData withContext:(NSManagedObjectContext*)context;


@end

NS_ASSUME_NONNULL_END

#import "TextData+CoreDataProperties.h"

//
//  TakePictureTableViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 1/19/15.
//
//

#import <UIKit/UIKit.h>

@protocol PhotoAlbumSelectedDelagate <NSObject>

-(void)photoAlbum;
-(void)takePicture;

@end

@interface TakePictureTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate>

@property(nonatomic, assign) id<UINavigationControllerDelegate> delegate;
@property(nonatomic, weak) id<PhotoAlbumSelectedDelagate> photoAlbumDelegate;

@end

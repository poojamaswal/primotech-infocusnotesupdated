//
//  SearchBarViewController.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SearchBarViewControllerDelegate 
-(void)setEKeventDict:(NSMutableDictionary *) eventsDic;
@end


@interface SearchBarViewController : UIViewController <UISearchBarDelegate> {
    
	UIView              *searchbarContainerView;
	UISearchBar         *searchAppBar;
	CGRect              contentFrame;
	NSString            *arrowDirection;
	CGPoint             arrowStartPoint;
    
	
	id                  parentVC;
	NSMutableArray      *currentArrayCopy;
    //AK
    id					__unsafe_unretained delegate;
    EKEventStore         *eventStore;
    NSMutableDictionary  *sections;
    NSMutableArray       *sortedDays;
    
    float               keyboardYPositionBeginVARIABLE; //Steve
    float               keyboardYPositionEndVARIABLE;  //Steve
    UISearchBar         *searchAppBarVARIABLE; //Steve
    
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withContentFrame:(CGRect) frame andArrowDirection:(NSString *) arrowDirectionLocal andArrowStartPoint:(CGPoint) arrowStartPointLocal withParentVC:(id)parentVCLocal;
- (void) animateTextFieldUp: (UISearchBar*) searchBar up: (BOOL) up;////Naresh/
- (void) animateTextFieldDown: (UISearchBar*) searchBar up: (BOOL) down;////Naresh/

@property(nonatomic, strong) UIView *searchbarContainerView;
@property(nonatomic, strong) UISearchBar *searchAppBar; 
@property(nonatomic, assign) CGRect contentFrame; 
@property(nonatomic, strong) NSString *arrowDirection;
@property(nonatomic, assign) CGPoint arrowStartPoint;
@property (nonatomic,strong) id parentVC;
//AK
@property (unsafe_unretained) id delegate;
@property(nonatomic, strong) NSMutableArray   *events;
@property(nonatomic, strong) NSMutableDictionary *sections;

@end



#import "ToolSelectionObject.h"

@implementation ToolSelectionObject

__strong static ToolSelectionObject *toolSelection;

+ (ToolSelectionObject*)sharedObject {
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        toolSelection = [[self alloc] init];
    });
    return toolSelection;
}

- (void)insertSelectedShape:(ShapeType)shapeType {
    if ([self.delegate respondsToSelector:@selector(drawShapeOfType:)]) {
        [self.delegate drawShapeOfType:shapeType];
    }
}

- (void)insertSelectedImage:(UIImage*)image {
    if ([self.delegate respondsToSelector:@selector(drawImageViewWithImage:withFrame:)]) {
        [self.delegate drawImageViewWithImage:image withFrame:CGRectMake(0, 0, 300, 300) ];
    }
}

- (void)insertSelectedSticky {
    if ([self.delegate respondsToSelector:@selector(drawStickyWithText:andFrame:)]) {
        [self.delegate drawStickyWithText:@"" andFrame:CGRectMake(0, 0, 300, 300)];
    }
}

- (void)openCameraRoll {
    if ([self.delegate respondsToSelector:@selector(openCameraRoll)]) {
        [self.delegate openCameraRoll];
    }
}

@end

//
//  RGBColorViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintingView.h"
#import "AddProjectDataObject.h"
#import "AddCalenderDataObject.h"
#import "CalenderAddButtonController.h"

@interface RGBColorViewCantroller : UIViewController <CalenderAddButtonControllerDelegate>
{
	id __unsafe_unretained delegate;
	IBOutlet PaintingView	*drawImage;
	UIView      *colorPanelView;
	UISlider	*redSlider;
	UISlider	*greenSlider;
	UISlider	*blueSlider;
	UIView *aView;
	AddProjectDataObject *PdataObj;
	AddCalenderDataObject *CdataObj;
	
}
@property (unsafe_unretained) id delegate;
@property(nonatomic,strong)AddProjectDataObject *PdataObj;
@property (nonatomic, strong) AddCalenderDataObject *CdataObj;
@property(nonatomic,strong)UIView  *colorPanelView;
@property(nonatomic,strong)UISlider	*redSlider;
@property(nonatomic,strong)UISlider *greenSlider;
@property(nonatomic,strong)UISlider	*blueSlider;
@property(nonatomic,strong)UIView *aView;

-(void)valueChanged:(id)sender;
//-(void)colorChangeSaved: (id) sender;
-(void)btnClicked:(id)sender;
-(void)SaveValue:(id)sender;
- (UIColor *)getRGBAsFromImage: (UIImage*)image atX: (int)xx andY: (int)yy;
@end

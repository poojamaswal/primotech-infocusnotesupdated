//
//  SplitViewController.h
//  Organizer
//
//  Created by lakshmi on 15/07/15.
//
//

#import <UIKit/UIKit.h>

@interface SplitViewController : UIViewController
{
    UINavigationController *leftController;
    UINavigationController *rightController;
    
    
}

@property (nonatomic, retain) UINavigationController *leftController;
@property (nonatomic, retain) UINavigationController *rightController;

- (SplitViewController*) initwithLeftVC:(UIViewController*)leftvc rightVC:(UIViewController*)rightvc;

@end

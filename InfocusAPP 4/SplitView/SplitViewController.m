//
//  SplitViewController.m
//  Organizer
//
//  Created by lakshmi on 15/07/15.
//
//

#import "SplitViewController.h"

static int MV_WIDTH  =320.0;	// Main view width
static int SV_GAP    =   2.0;	// Gap size
static int SB_HEIGHT =  0.0;	// Status bar height

@interface SplitViewController ()

@end

@implementation SplitViewController
@synthesize leftController,rightController;

- (SplitViewController*) initwithLeftVC:(UIViewController*)leftvc rightVC:(UIViewController*)rightvc
{
    //	if(self=[super init])
    if(self==[super init])
    {
        UINavigationController *lnc=[[UINavigationController alloc] initWithRootViewController:leftvc];
        
        
        
        [lnc.navigationBar setBackgroundImage:[UIImage imageNamed:@"left_topbar.png"] forBarMetrics:UIBarMetricsDefault];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectZero];
        label1.backgroundColor = [UIColor clearColor];
        label1.font = [UIFont boldSystemFontOfSize:20.0];
        label1.textAlignment = NSTextAlignmentCenter;
        // ^-Use UITextAlignmentCenter for older SDKs.
        label1.textColor = [UIColor whiteColor]; // change this color
        lnc.navigationItem.title=label1.text;
        label1.text = NSLocalizedString(@"Wallet", @"");
        [label1 sizeToFit];
        
        
        [[UINavigationBar appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], UITextAttributeTextColor,
          
          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
          [UIFont boldSystemFontOfSize:19], UITextAttributeFont,
          nil]];
        lnc.navigationBarHidden=NO;
        self.leftController=lnc;
        
        
        
        
        UINavigationController *rnc=[[UINavigationController alloc] initWithRootViewController:rightvc];
        
        
        
        
        [rnc.navigationBar setBackgroundImage:[UIImage imageNamed:@"master_topbar.png"] forBarMetrics:UIBarMetricsDefault];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use UITextAlignmentCenter for older SDKs.
        label.textColor = [UIColor whiteColor]; // change this color
        rnc.navigationItem.title=label.text;
        label.text = NSLocalizedString(@"Wallet", @"");
        [label sizeToFit];
        
        
        [[UINavigationBar appearance] setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor whiteColor], UITextAttributeTextColor,
          
          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
          [UIFont boldSystemFontOfSize:19], UITextAttributeFont,
          nil]];
        rnc.navigationBarHidden=NO;
        self.rightController=rnc;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.autoresizesSubviews=YES;
    self.view.backgroundColor=[UIColor blackColor];
    
    [self.leftController viewDidLoad];
    [self.view addSubview:self.leftController.view];
    
    [self.rightController viewDidLoad];
    [self.view addSubview:self.rightController.view];
}

//---------------------------------------------------------------------------------------- viewDidAppear
- (void)viewDidAppear:(BOOL)animated
{
    self.leftController.view.frame=CGRectMake(0,SB_HEIGHT,MV_WIDTH,self.view.frame.size.height-SB_HEIGHT);
    [self.leftController viewDidAppear:animated];
    [self.view addSubview:self.leftController.view];
    
    self.rightController.view.frame=CGRectMake(MV_WIDTH+SV_GAP,SB_HEIGHT,self.view.frame.size.width-MV_WIDTH-SV_GAP,self.view.frame.size.height-SB_HEIGHT);
    
    [self.rightController viewDidAppear:animated];
    [self.view addSubview:self.rightController.view];
}

//---------------------------------------------------------------------------------------- viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[self layoutViews:UIInterfaceOrientationPortrait initialVerticalOffset:SB_HEIGHT];
    
    [self.leftController viewWillAppear:animated];
    [self.rightController viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

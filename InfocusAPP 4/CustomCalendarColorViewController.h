//
//  CustomCalendarColorViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/7/13.
//
//

#import <UIKit/UIKit.h>
#import "CalendarDataObject.h"


@protocol CustomCalendarColorDelegate <NSObject>

-(void) updateCalendarObject:(CalendarDataObject*)calObject;

@end



@interface CustomCalendarColorViewController : UIViewController

@property (weak, nonatomic) id<CustomCalendarColorDelegate>     delegate;
@property (strong,nonatomic) CalendarDataObject                 *calendarObject;
@property (assign, nonatomic) BOOL                              isCustomColor;


@end

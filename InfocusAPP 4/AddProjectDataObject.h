//
//  AddProjectDataObject.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AddProjectDataObject : NSObject 
{

	NSInteger   projectid;
	NSInteger   belongstoid;
	NSString    *projectSubname;
	NSString    *projectname;
    NSString    *projectIMGname;
	NSString    *projectpriority;
	NSInteger   level;
	NSString	*creationDate;
	NSString	*modifyDate;
	NSInteger   tempValue;
	
	
	NSMutableArray *Pnamearray;
	NSMutableArray *PcororRarray;
	NSMutableArray *PcororGarray;
	NSMutableArray *PcororBarray;
	NSMutableArray *Pidarray;
	
	NSInteger subfolderID;
	
}
@property(nonatomic,assign)NSInteger subfolderID;
@property(nonatomic,strong) NSString    *projectIMGname;
@property(nonatomic,strong)NSMutableArray *Pnamearray;
@property(nonatomic,strong)NSMutableArray *Pidarray;
@property(nonatomic,strong)NSString    *projectSubname;
@property(nonatomic,assign)NSInteger projectid;
@property(nonatomic,assign)NSInteger belongstoid;
@property(nonatomic,strong)NSString    *projectpriority;
@property(nonatomic,assign)NSInteger level;
@property(nonatomic,strong)NSString *projectname;

@property(nonatomic,strong)NSString	*creationDate;
@property(nonatomic,strong)NSString	*modifyDate;
@property(nonatomic,assign)NSInteger tempValue;

@end

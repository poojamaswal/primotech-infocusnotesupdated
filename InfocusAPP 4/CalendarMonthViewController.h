//
//  CalendarMonthViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKCalendarMonthView.h"
#import "OrganizerAppDelegate.h" //Steve added


@protocol ToolBarToFrontDelegate <NSObject>

//Steve - works with protocal on Calendar Month View
-(void)SetToolBarToFrontPosition:(BOOL) isToolBarFront;

@end



@interface CalendarMonthViewController : UIViewController <TKCalendarMonthViewDelegate, TKCalendarMonthViewDataSource, UITableViewDelegate, UITableViewDataSource,UIActionSheetDelegate>
{
	TKCalendarMonthView *calendarTKCalendarMonthView;
	NSMutableArray		*monthObjArray;
	NSMutableArray		*objPerDay;
	NSMutableArray		*arrayPerDay;
	UITableView			*dayDetailTableView;
	NSMutableArray      *tmArray;
    NSMutableArray      *arrayDateSelected;
    EKEventStore        *eventStore;
    UIView              *loadingView;
    dispatch_queue_t    fetchingEvents_queue;
    OrganizerAppDelegate *appDelegate; //Steve added
    BOOL                isTodayButtonClicked;//Steve added
    BOOL                isEventBeingDeleted;//Steve added
    
    NSMutableArray      *arrayPerDayForMarks; //Steve added - stores where to put marks
   

}

@property (nonatomic, strong) TKCalendarMonthView   *calendarTKCalendarMonthView;
@property (nonatomic, strong) UITableView           *dayDetailTableView;
@property(nonatomic, strong) NSMutableString        *strTextToSpeak;//Aman's Added
@property(nonatomic, strong) NSDate                 *strtDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *endDate ;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrM_finalEvents;//Aman's Added
@property(nonatomic, strong) NSMutableString        *str_todaysDate;
@property(nonatomic, strong) NSMutableString        *currentCalanderDate ;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrayDateSelected; //Steve added
@property(nonatomic, strong) NSMutableArray         *events; //Steve added
@property(nonatomic, weak) id<ToolBarToFrontDelegate> toolBarDelegate;


- (void) getAppointmentsArrayWithStrtDate:(NSString *) startDate endDate:(NSString *) endDate;
- (void)toggleCalendar;
- (void)gotoToday;
- (void) reloadTableView;
- (NSDate *)dateAfterDays:(NSInteger) days inDate:(NSDate *) fromDate;
-(void)eventToSpeak;//Aman's Added
-(int)getNumberOfRows:(int)section;
-(NSMutableString *)finalString;
-(NSInteger)numberofAppointments;
-(BOOL)hasConnectivity;
-(NSString *)callToSpeaktext:(NSString *)callingText;//Aman's Added
- (void) btnAccessory: (UIControl *) button withEvent: (UIEvent *) event;
- (void) refreshViews;
-(void) getAppointmentsForCellDate:(NSDate *) selectedDate; //Steve added
//-(void)removeView; //Steve added here so TKCalendarMonthView could access this method
-(void)getAppointmentMethod; //Steve moved here so CalendarparentController can access it
@end

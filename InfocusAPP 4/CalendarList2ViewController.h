//
//  MasterViewController.h
//  AppointmentList
//
//  Created by Ole Begemann on 01.12.11.
//  Copyright (c) 2011 Ole Begemann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentsDataObject.h"
#import "AppointmentViewController .h"
#import "OrganizerAppDelegate.h"

@class OrganizerAppDelegate;

@interface CalendarList2ViewController : UITableViewController{
    
    
    OrganizerAppDelegate    *appDelegate;
    NSInteger               headerIndex;
    EKEventStore            *eventStore;
    IBOutlet UITableView    *tblAppinmentList;
    NSMutableDictionary     *sections;
    NSMutableArray          *sortedDays;
    NSMutableArray          *appointmentsArray;
    BOOL                    internet_YES_NO;
    NSMutableDictionary     *HWImagesDic;
    
    id                      __unsafe_unretained delegate;
    int                     counter;
   
}
//Alok Added
 @property (unsafe_unretained) id delegate;
@property(nonatomic, strong) UITableView      *tblAppinmentList;
@property(nonatomic, strong) NSMutableString  *strTextToSpeak;
@property(nonatomic, strong) NSMutableArray   *appointmentsArray;
@property(nonatomic, strong) NSMutableDictionary *sections;
@property (strong, nonatomic) NSMutableArray *sortedDays;

@property(nonatomic, strong) NSDate           *currentVisibleDate; //Steve added
@property(nonatomic, strong) NSMutableArray   *events; //Steve added


-(void)gotoToday;
-(void)eventToSpeak;//Alok's Added
-(BOOL)hasConnectivity;//Alok's Added
-(void) refreshViewsAfterDelay; //Steve


@end

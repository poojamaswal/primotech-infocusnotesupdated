//
//  LevelTwoViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LevelTwoViewCantroller.h"
#import "AddProjectViewCantroller.h"
#import "OrganizerAppDelegate.h"


@implementation LevelTwoViewCantroller

@synthesize getID,getBelongID,getName;

@synthesize pidArray;
@synthesize pnameArray;
@synthesize priorityArray;
@synthesize pcolorRArray;
@synthesize PcolorGArray;
@synthesize PcolorBArray;
@synthesize PleveArray;
@synthesize PbelongtoArray;
@synthesize levleValue,belongsTo,editop;


-(void)fradio_click:(id)sender
{
	NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
	NSLog(@"%@",components);
	
	[myImage removeFromSuperview];
	if([sender tag]==211)
	{
		myImageRect = CGRectMake(230.0f, 4.0f, 25.0f, 25.0f);
	}
	else 
	{
		myImageRect = CGRectMake(182.0f, 4.0f, 25.0f, 25.0f);
	}
	
	myImage = [[UIImageView alloc] initWithFrame:myImageRect];
	[myImage setImage:[UIImage imageNamed:@"TickArrow.png"]];
	
	
	
	if ([sender isSelected])
	{
		[sender addSubview:myImage];
		[sender setSelected:NO];
	}	
	else
	{
		if([sender tag]==211)
		{
			levleValue=2;
			belongsTo=getID;
		}
		else 
		{
			levleValue =[[components objectAtIndex:1]intValue]+1;
			belongsTo  =[[components objectAtIndex:0]intValue];
		}
		[sender setSelected:YES];
		[sender addSubview:myImage];
		
	}
	
}


-(void)chekBTN_click:(id)sender
{
	NSString *getSTR = [(UILabel *)[sender viewWithTag:666] text];
	NSArray *components1 = [getSTR componentsSeparatedByString:@"~"];
	NSLog(@"%@",components1);
	
	if ([sender isSelected])
	{
		[sender setSelected:NO];
	}	
	else
	{
	    levleValue =[[components1 objectAtIndex:1]intValue]+1;
		belongsTo  =[[components1 objectAtIndex:0]intValue];
		[sender setSelected:YES];
		
	}
	
}

-(void)next_btnClick:(id)sender
{
   	
	//NSString *getSTR = [(UILabel *)[sender viewWithTag:111] text];
//	NSArray *components = [getSTR componentsSeparatedByString:@"~"];
//	
//	
//	NSLog(@"%@",components);
//	
//	LevelTwoViewCantroller *leveltwo= [[LevelTwoViewCantroller alloc] initWithNibName:@"LeveloneViewCantroller" bundle:[NSBundle mainBundle]];
//	leveltwo.getID = belongsTo;
//	leveltwo.getName =[components objectAtIndex:1];
//	[self.navigationController pushViewController:leveltwo animated:YES];
//	[leveltwo release];
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	NSLog(@"getID%d",getID);
	NSLog(@"getName%@",getName);
		
	
	pidArray       =  [[NSMutableArray alloc]init];
	pnameArray     =  [[NSMutableArray alloc]init];
	priorityArray  =  [[NSMutableArray alloc]init];
	pcolorRArray   =  [[NSMutableArray alloc]init];
	PcolorGArray   =  [[NSMutableArray alloc]init];
	PcolorBArray   =  [[NSMutableArray alloc]init];
	PleveArray     =  [[NSMutableArray alloc]init];
	PbelongtoArray =  [[NSMutableArray alloc]init];
	
	
	UIImage *bar=[ UIImage imageNamed:@"Bar1.png"];
	myButton = [UIButton buttonWithType:UIButtonTypeCustom];
	myButton.frame = CGRectMake(30,70,262,33);
	[myButton setAdjustsImageWhenHighlighted:NO];
	[myButton setImage:bar forState:UIControlStateNormal];
	[myButton setImage:bar forState:UIControlStateSelected];
	
	UILabel* titleLabel = [[[UILabel alloc] 
							initWithFrame:CGRectMake(5,0,200,30)]
						   autorelease];
	titleLabel.text = getName;
	titleLabel.font = [UIFont fontWithName:@"Halvetica Bold" size: 15.0];
	titleLabel.textColor = [UIColor blackColor];
	titleLabel.backgroundColor = [UIColor clearColor];
	titleLabel.textAlignment = UITextAlignmentLeft;
	[myButton addSubview:titleLabel];
	[myButton setTag:211];
	[myButton addTarget:self action:@selector(fradio_click:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:myButton];
	
	[self getLevelData:getID];
	
	if([pidArray count]>0)
	{
		
		int j=120;
		for(int i=1 ,k=0;i<=[pidArray count]+[pidArray count];k<=[pidArray count],i=i+2,k++)
		{
			myButton = [UIButton buttonWithType:UIButtonTypeCustom];
			myButton.frame = CGRectMake(58,j,235,32);
			[myButton setAdjustsImageWhenHighlighted:NO];
			[myButton setAdjustsImageWhenHighlighted:NO];
			[myButton setImage:bar forState:UIControlStateNormal];
			[myButton setImage:bar forState:UIControlStateSelected];
			
			//[myButton setImage:fradio forState:UIControlStateNormal];
			UILabel* titleLabel = [[[UILabel alloc] 
									initWithFrame:CGRectMake(5,0,100,30)]
								   autorelease];
			titleLabel.text = [pnameArray objectAtIndex:k];
			titleLabel.font = [UIFont fontWithName:@"Halvetica Bold" size: 15.0];
			titleLabel.textColor = [UIColor blackColor];
			titleLabel.backgroundColor = [UIColor clearColor];
			titleLabel.textAlignment = UITextAlignmentLeft;
			[myButton addTarget:self action:@selector(fradio_click:) forControlEvents:UIControlEventTouchUpInside];
			
			UILabel *viewmB=[[UILabel alloc]init];
			viewmB.text =[NSString stringWithFormat:@"%d~%d",[[pidArray objectAtIndex:k]intValue],[[PleveArray objectAtIndex:k]intValue]];
			viewmB.tag =666;
			[myButton addSubview:viewmB];
			
			[myButton addSubview:titleLabel];
			[myButton setTag:i+1];
			[self.view addSubview:myButton];
			colorBTN=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
			colorBTN.frame = CGRectMake(33,j+6,20,20);
			[colorBTN contentHorizontalAlignment];
			colorBTN.backgroundColor = [UIColor colorWithRed:[[pcolorRArray objectAtIndex:k] floatValue] green:[[PcolorGArray objectAtIndex:k] floatValue] blue:[[PcolorBArray objectAtIndex:k]floatValue] alpha:1];
			[colorBTN.layer setBorderWidth:.24];
			[colorBTN.layer setCornerRadius:10.0];
			[colorBTN.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			colorBTN.layer.masksToBounds = YES;
			[self.view addSubview:colorBTN];	
			
			
			j=j+40;
			
		}
	}
}



-(void)getLevelData:(NSInteger)idVal
{
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT projectID, projectName,projectPriority,projectColorR,projectColorG,projectColorB,belongstoID,level from ProjectMaster where level = %d AND  belongstoID =%d",2,getID];
		
		NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				projectdataobj = [[AddProjectDataObject alloc]init];
				
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				
				[pidArray addObject:[NSNumber numberWithInt:projectdataobj.projectid]];
				
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				
				[pnameArray addObject:projectdataobj.projectname];
				
				[projectdataobj setProjectpriority: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
				
				[priorityArray addObject:projectdataobj.projectpriority];
				
				[projectdataobj setProjectcolorR:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
				[pcolorRArray addObject:projectdataobj.projectcolorR];
				
				[projectdataobj setProjectcolorG:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 4)]];
				[PcolorGArray addObject:projectdataobj.projectcolorG];
				
				[projectdataobj setProjectcolorB:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 5)]];
				[PcolorBArray addObject:projectdataobj.projectcolorB];
				
				[projectdataobj setBelongstoid:sqlite3_column_int(selectNotes_Stmt,6)];
                [PbelongtoArray addObject:[NSNumber numberWithInt:projectdataobj.level]]; 
				
				[projectdataobj setLevel:sqlite3_column_int(selectNotes_Stmt,7)];
                [PleveArray addObject:[NSNumber numberWithInt:projectdataobj.level]]; 
				[projectdataobj release];
				
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		[Sql release];
	}	
	
	
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(IBAction)cancel_btn:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)save_btn:(id)sender
{
	
	NSLog(@" Level => %d",levleValue);
	NSLog(@" Belong => %d",belongsTo);
	
	
	
	
	
	if(editop==1)
	{
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setBelongValue:belongsTo];
		
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLevelValue:levleValue];
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLEVELOPT:YES];
		
		
		[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:3] animated:YES];
	}
	else if(editop==2)
		
	{
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setBelongValue:belongsTo];
		
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLevelValue:levleValue];
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLEVELOPT:YES];		
		
		[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:4] animated:YES]; 
		
		
	}
	
	
	else {
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setBelongValue:belongsTo];
		
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLevelValue:levleValue];
		
		[(AddProjectViewCantroller*)[[self.navigationController viewControllers] objectAtIndex:([[self.navigationController viewControllers] count] -4)]setLEVELOPT:YES];
		
		[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
	}
}


- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc 
{
    [super dealloc];
	[pidArray release];
	[pnameArray release];
	[priorityArray release];
	[pcolorRArray release];
	[PcolorGArray release];
	[PcolorBArray release];
	[PleveArray release];
	[PbelongtoArray release];
}


@end

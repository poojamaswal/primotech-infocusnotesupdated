//
//  INFPopoverController.h
//  InfocusNotesApp
//
//  Created by jaswinder blagun on 29/07/2015.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CustomBarButton.h"

@interface INFPopoverController : NSObject

//@property (nonatomic, strong) CustomBarButton *presentingBarButton;
@property (nonatomic, strong) UIPopoverController *popoverController;

+ (INFPopoverController*)sharedObject;
+ (UIPopoverController*)currentPopoverController;

@end
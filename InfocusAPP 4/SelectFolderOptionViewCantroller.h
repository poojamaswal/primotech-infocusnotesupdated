//
//  SelectFolderOptionViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//R1012114103


#import <UIKit/UIKit.h>
#import "MyTreeNode.h"
@class AddProjectViewCantroller;

@interface SelectFolderOptionViewCantroller : UIViewController
{
	IBOutlet UITableView *foldetTable;
   // IBOutlet UIButton *newFolderbtn;
	IBOutlet UIButton *selctBTN;
	MyTreeNode *treeNode;	
	MyTreeNode *node1;
	MyTreeNode *node2;
	MyTreeNode *node3;
	MyTreeNode *node4;
	
	
	
	NSMutableArray *SgetLevelRootArray;
	NSMutableArray *SgetLevelRootNameArray;
	NSMutableArray *SgetLevelRootLevelArray;
	NSMutableArray *SgetLevelRootBelongsToArray;
	
	NSMutableArray *SpidleveloneArray;
	NSMutableArray *SpidleveloneNameArray;
	NSMutableArray *SpidleveloneLevelArray;
	NSMutableArray *SpidleveloneBelongsToArray;
	
	NSMutableArray *SpidleveltwoArray;
	NSMutableArray *SpidleveltwoNameArray;
	NSMutableArray *SpidleveltwoLevelArray;
	NSMutableArray *SpidleveltwoBelongsTolArray;
	
	NSMutableArray *SpidlevelthreeArray;
	
	NSMutableArray *SpidlevelthreeLevelArray;
	NSMutableArray *SpidlevelthreeBelongsToArray;


	
	NSMutableArray *SpidlevelthreeNameArray;
	UIButton *titleImageViewEdit;
	
	CGRect myImageRect;
	UIImageView *backImageView;
	
	NSInteger sentLvalue;
	NSInteger sentBvalue;
	NSString *fname;
	NSInteger senPID;
	
	//IBOutlet UIButton *newFolder;
	
	UIButton *buttonLevelone;
	UIButton  *buttonLeveltwo;
	UIButton  *buttonLevelthree;
	UIButton  *buttonLevelfour;
    
    UIButton *HidebuttonLevelone;
	UIButton  *HidebuttonLeveltwo;
	UIButton  *HidebuttonLevelthree;
	UIButton  *HidebuttonLevelfour;
	
	
	NSMutableArray *SgetRootTODOArray;
	
	NSInteger sentRootValue;
	NSString *Sql;
	BOOL SaveIT;
	BOOL optn;
	
	//NSString *newName;
	NSString *newpriorty;
	NSString *sql;
	
	NSInteger getRootVal;
	
	NSMutableArray *SgetLevelRootimageArray; 
	NSMutableArray *SgetLevelOneimageArray; 
	NSMutableArray *SgetLevelTwoimageArray; 
	NSMutableArray *SgetLevelThreetimageArray; 
	NSMutableArray *fixArray;
	
	UIImage *selectimgSel;
	UIButton *selectbtn;
    UIImageView* blockView;
	UIImageView* blockView2;
	NSInteger getfval;
	
	int f4;
	int f3;
	int f2;
	int f1;
	UIImageView				*tickImageView;
	NSInteger				lastProjectID;
	NSInteger getViewValue;

    IBOutlet UINavigationBar *navBar;

}
@property(nonatomic,assign) NSInteger lastProjectID;
@property(nonatomic,assign)NSInteger getViewValue;
@property(nonatomic,assign)NSInteger getfval;
@property(nonatomic,strong)NSMutableArray *fixArray;
@property(nonatomic,strong)NSMutableArray *SgetLevelRootimageArray;
@property(nonatomic,strong)NSMutableArray *SgetLevelOneimageArray; 
@property(nonatomic,strong)NSMutableArray *SgetLevelTwoimageArray; 
@property(nonatomic,strong)NSMutableArray *SgetLevelThreetimageArray; 
@property(nonatomic,assign)NSInteger getRootVal;
@property(assign) BOOL SaveIT;
@property(nonatomic,strong)NSString *sql;

@property(nonatomic,strong)NSString *newpriorty;

//@property(nonatomic,retain)NSString *newName;
@property(assign)BOOL optn;
@property(nonatomic,strong)NSString *fname;
@property(nonatomic,assign)NSInteger senPID;
@property(nonatomic,strong)NSString *Sql;
@property(nonatomic,assign)NSInteger sentRootValue;
@property(nonatomic,strong)NSMutableArray *SgetRootTODOArray;
@property(nonatomic,strong)UIButton *buttonLevelone;;
//@property(nonatomic,retain)IBOutlet UIButton *newFolder;
@property(nonatomic)NSInteger sentLvalue;
@property(nonatomic)NSInteger sentBvalue;
@property(nonatomic,strong)NSMutableArray *SgetLevelRootBelongsToArray;
@property(nonatomic,strong)NSMutableArray *SpidleveloneBelongsToArray;
@property(nonatomic,strong)NSMutableArray *SpidleveltwoBelongsTolArray;
@property(nonatomic,strong)NSMutableArray *SpidlevelthreeBelongsToArray;

@property(nonatomic,strong)NSMutableArray *firstArray;
@property(nonatomic,strong)NSMutableArray *SpidlevelthreeArray;
@property(nonatomic,strong)NSMutableArray *SpidlevelthreeNameArray;
@property(nonatomic,strong)NSMutableArray *SpidlevelthreeLevelArray;

@property(nonatomic,strong)NSMutableArray *SgetLevelRootLevelArray;
@property(nonatomic,strong)NSMutableArray *SpidleveloneLevelArray;
@property(nonatomic,strong)NSMutableArray *SpidleveltwoLevelArray;
@property(nonatomic,strong)NSMutableArray *SgetLevelRootNameArray;
@property(nonatomic,strong)NSMutableArray *SgetLevelRootArray;
@property(nonatomic,strong)NSMutableArray *SpidleveloneNameArray;
@property(nonatomic,strong)NSMutableArray *SpidleveltwoNameArray;

@property(nonatomic,strong)NSMutableArray *SpidleveltwoArray;
@property(nonatomic,strong)NSMutableArray *SpidleveloneArray;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;//Steve

-(IBAction)Cancel_btn:(id)sender;
-(void)selectionMethod:(id)sender;
-(void)FindRecored;
-(void)getLevelOne:(NSInteger)lONE;
-(void)getLevelTwo:(NSInteger)lTWO;
-(void)getLevelThree:(NSInteger)lThree;
-(void)PlusNow:(id)sender;
-(IBAction)SaveValue:(id)sender;
-(void)selectNowEnd:(id)sender;
-(IBAction)newFolderAdd:(id)sender;
-(void)SaveDummy;

@end

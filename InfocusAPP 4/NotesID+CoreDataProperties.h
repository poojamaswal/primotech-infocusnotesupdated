//
//  NotesID+CoreDataProperties.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotesID.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotesID (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *pageID;
@property (nullable, nonatomic, retain) NSString *pageName;
@property (nullable, nonatomic, retain) NSNumber *pageNumber,*subNotesCount;

@property (nullable, nonatomic, retain) NSSet<DrawingData*> *drawingData;
@property (nullable, nonatomic, retain) NSSet<ImageData *> *imageData;
@property (nullable, nonatomic, retain) NSSet<TextData*> *textData;
@property (nullable, nonatomic, retain) NSSet<StickyData*> *stickyData;

@property (nonatomic) NSNumber * projectID;

@end

@interface NotesID (CoreDataGeneratedAccessors)

- (void)addDrawingDataObject:(DrawingData *)value;
- (void)removeDrawingDataObject:(DrawingData *)value;
- (void)addDrawingData:(NSSet<DrawingData *> *)values;
- (void)removeDrawingData:(NSSet<DrawingData *> *)values;

- (void)addImageDataObject:(ImageData *)value;
- (void)removeImageDataObject:(ImageData *)value;
- (void)addImageData:(NSSet<ImageData *> *)values;
- (void)removeImageData:(NSSet<ImageData *> *)values;

- (void)addTextDataObject:(TextData *)value;
- (void)removeTextDataObject:(TextData *)value;
- (void)addTextData:(NSSet<TextData *> *)values;
- (void)removeTextData:(NSSet<TextData *> *)values;

- (void)addStickyDataObject:(StickyData *)value;
- (void)removeStickyDataObject:(StickyData *)value;
- (void)addStickyData:(NSSet<StickyData *> *)values;
- (void)removeStickyData:(NSSet<StickyData *> *)values;

@end

NS_ASSUME_NONNULL_END

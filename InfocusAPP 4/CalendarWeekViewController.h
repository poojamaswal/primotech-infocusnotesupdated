//
//  CalendarWeekViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrganizerAppDelegate.h"
#import "CustomeScrollView.h"
#import "UIImage+RoundedCorner.h"
#import "NSDate+TKCategory.h"




@interface CalendarWeekViewController : UIViewController<UIGestureRecognizerDelegate, UIScrollViewDelegate> {
	IBOutlet CustomeScrollView	*weekTimeScrollView;
	IBOutlet UIImageView        *weekTimeImageView;
	IBOutlet UIImageView        *allDayDiviserImageView;
	IBOutlet UIButton           *allWeekTimeImageView;
	IBOutlet UILabel            *weekNumberLabel;
	
	IBOutlet UILabel *sunDayLabel;
	IBOutlet UILabel *monDayLabel;
	IBOutlet UILabel *tueDayLabel;
	IBOutlet UILabel *wedDayLabel;
	IBOutlet UILabel *thuDayLabel;
	IBOutlet UILabel *friDayLabel;
	IBOutlet UILabel *satDayLabel;
	
	NSMutableArray *weekAppDataObjArray;
	NSMutableArray *allDayWeekObjArray;
	
	
	OrganizerAppDelegate  *appDelegate;
	NSDateFormatter *formater;
	
	NSDate *currentStartDateofWeek;
	NSDate *currentEndDateofWeek;
	
	CGPoint currentClickPoint;
	UIButton *lastSelectedEvent;
	
	NSMutableArray *sundaysObjArray;
	NSMutableArray *mondaysObjArray;
	NSMutableArray *tuedaysObjArray;
	NSMutableArray *weddaysObjArray;
	NSMutableArray *thudaysObjArray;
	NSMutableArray *fridaysObjArray;
	NSMutableArray *satdaysObjArray;
    
	NSMutableArray *sundaysObjArrayAllDay;
	NSMutableArray *mondaysObjArrayAllDay;
	NSMutableArray *tuedaysObjArrayAllDay;
	NSMutableArray *weddaysObjArrayAllDay;
	NSMutableArray *thudaysObjArrayAllDay;
	NSMutableArray *fridaysObjArrayAllDay;
	NSMutableArray *satdaysObjArrayAllDay;
	
	NSInteger		loopStarter;
	NSInteger		loopStarterAllDay;
    
	UIImageView	    *currentTimeImageView;
	
	UILabel			*allDayLabel;
	
	CGFloat			allDayViewHeight;
    
    //Steve added to recognize taps
    UITapGestureRecognizer      *tap;
    
    //Steve added: recognize swipes
    UISwipeGestureRecognizer    *swipeRight;
    UISwipeGestureRecognizer    *swipeLeft;
    NSMutableArray              *arrM_finalEvents;
    NSString                    *currentWeekNumber;
    EKEventStore                *eventStore;
    
    BOOL                        isRefresh;//Steve
}

@property(nonatomic, strong) UIButton				*lastSelectedEvent;
@property(nonatomic, strong) NSMutableArray			*weekAppDataObjArray;
@property(nonatomic, strong) NSMutableArray			*allDayWeekObjArray;
@property(nonatomic, strong) IBOutlet UIImageView	*currentTimeImageView;
@property(nonatomic, strong) NSMutableString        *strTextToSpeak;//Aman's Added
@property(nonatomic, strong) NSDate                 *strtDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *endDate ;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrM_finalEvents;//Aman's Added
@property(nonatomic, strong) NSMutableString        *str_todaysDate;
@property(nonatomic, strong) NSString               *str_Week;

-(IBAction)nextButton_Clicked:(UIButton *) sender;
-(IBAction)previousButton_Clicked:(UIButton *) sender;
-(void)getAppointmentsFromDate:(NSString *) fromDate toDate:(NSString *) toDate andDayNumber:(NSInteger) dayNumber isAllDay:(BOOL) isallDay;
-(void)gotoToday;
-(void)showData:(NSArray *) withDataObj andColumnNo:(NSInteger) columnNmbr forDateStr:(NSString* ) forDateStr;
-(void)showDataAllDay:(NSArray *) withDataObj andColumnNo:(NSInteger) columnNmbr forDateStr:(NSString* ) forDateStr;
-(void)hidePopups;
-(IBAction) allDayButton_Clicked:(UIButton* )sender;

-(void)eventToSpeak;//Aman's Added
-(NSMutableString *)finalString;
-(NSInteger)numberofAppointments;
-(BOOL)hasConnectivity;
-(NSString *)callToSpeaktext:(NSString *)callingText;//Aman's Added
- (void) refreshViews;
-(NSString *)EKAlarmToStr:(EKAlarm*)ekAlarm;//ALok gupta added
@end

//
//  ProjectMainViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectDataObject.h"

@class AddProjectDataObject;
#import "ProjectSelectionDelegate.h"

@interface ProjectMainViewCantroller : UIViewController <MFMailComposeViewControllerDelegate>
{
	IBOutlet UIScrollView *scroll;	
	NSMutableArray *pidArray;
	NSMutableArray *pnameArray;
	NSMutableArray *priorityArray;
    NSMutableArray *PimageArray;
	IBOutlet UIView *srchView;
	IBOutlet UITextField *srchtxt;
	IBOutlet UIButton *srchbtn;
	int conSize;
	AddProjectDataObject *projectdataobj ;
	NSString *queryVal;
	BOOL Qval;
	NSString *Sql;
	int cnt;
	int yVal;
	int	badgeCount[2];
	NSArray *listofIMG;
	
	NSMutableArray *collect1;
	NSMutableArray *collect2;
	NSMutableArray *collect3;
	
	NSInteger collectTODO1;
	NSInteger collectTODO2;
	NSInteger collectTODO3;
	NSInteger totalTODO;
	NSInteger collectTODO4;
    
    ////////Searching Variables//////
    BOOL searching;
    NSMutableArray *arrayM_SearchContents;
    NSMutableArray *arrayM_OriginalData;
    IBOutlet UINavigationBar *navBar;

    IBOutlet UIToolbar *bottomBar;
    IBOutlet UIView *helpView;  //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;  //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView; //Steve
    
	//AdWhirlView *adView;
    
    //Steve added
    IBOutlet UIButton *homeBtn;
    IBOutlet UIButton *noteBtn;
    IBOutlet UIButton *todoBtn;
    IBOutlet UIButton *listBtn;
    IBOutlet UIButton *calendarButton;
    
    
    IBOutlet UIView *upgradeView; //Steve
    IBOutlet UIScrollView *upgradeScrollView; //Steve
    IBOutlet UINavigationBar *navBarUpgradeView; //Steve
    
    float           keyboardYPositionBeginVARIABLE; //Steve
    float           keyboardYPositionEndVARIABLE; //Steve
    
    IBOutlet UIButton *addButton; //Steve
    BOOL                isMenuClosed; //Steve
    
    
}
//@property(nonatomic,retain)AdWhirlView *adView;
@property(nonatomic,strong)NSMutableArray *collect1;
@property(nonatomic,strong)NSMutableArray *collect2;
@property(nonatomic,strong)NSMutableArray *collect3;
@property(nonatomic,strong)NSArray *listofIMG;
@property(nonatomic,strong)NSMutableArray *PimageArray;
@property(nonatomic,strong)NSString *queryVal;
@property(assign)BOOL Qval;
@property(nonatomic,strong)AddProjectDataObject *projectdataobj ;
@property(nonatomic,strong)NSMutableArray *priorityArray;
@property(assign)int conSize;
@property(nonatomic,strong)NSMutableArray *pidArray;
@property(nonatomic,strong)NSMutableArray *pnameArray;
@property (strong, nonatomic) IBOutlet UISearchBar *search_Bar;
@property (strong, nonatomic) IBOutlet UIButton *noteButtonClicked;
- (IBAction)calenderButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *calendarBtnClicked;

-(IBAction)addProject_Clicked:(id)sender;
-(void)getProject;
-(IBAction)home_click:(id)sender;
-(IBAction)option_click:(id)sender;
-(IBAction)search_Click:(id)sender;
-(IBAction)get_serchData:(id)sender;
-(IBAction)Pview_click:(id)sender;
-(IBAction)mailBarButton_Clicked:(id)sender;
-(IBAction)add_todo:(id)sender;
-(IBAction)add_calender:(id)sender;
-(void)getBadgeCounts:(NSInteger)value;
-(IBAction)edit_option:(id)sender;
- (IBAction)listBtnClicked:(id)sender;

-(NSMutableArray*)findBage:(NSInteger)valueB; 
-(NSInteger)findTODOTotal:(NSInteger)valueTODO;


-(void)reloadScrollView;///Naresh/Aman
////////Searching Methods//////

- (void) animateTextFieldUp: (UISearchBar*) searchBar up: (BOOL) up;////Naresh
- (void) animateTextFieldDown: (UISearchBar*) searchBar up: (BOOL) down;////Naresh
-(int)getBadgeCountsForNotes:(NSInteger)value;
- (IBAction)noteBtnClicked:(id)sender;
- (IBAction)helpView_Tapped:(id)sender;  //Steve

//Steve added
-(IBAction)settingBarButton_Clicked:(id)sender;
- (IBAction)infoBarButton_Clicked:(id)sender;
- (IBAction)cancelUpgradeButton_Clicked:(id)sender;
- (IBAction)upgradeButton_Clicked:(id)sender;
    

@property (nonatomic, assign) id<ProjectSelectionDelegate> delegate;

@end

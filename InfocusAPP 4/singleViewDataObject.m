//
//  singleViewDataObject.m
//  Organizer
//
//  Created by Naresh Chauhan on 8/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "singleViewDataObject.h"


@implementation singleViewDataObject
@synthesize notesID;
@synthesize	notesTitle; 
@synthesize	notesTitleType; 
@synthesize	notesTitleBinary; 
@synthesize	notesTitleBinaryLarge;


-(void)dealloc {
	[super dealloc];
	
	[notesTitle release];  
	[notesTitleType release]; 
	[notesTitleBinary release]; 
	
	if (notesTitleBinaryLarge)
	{
		[notesTitleBinaryLarge release];
	}
	

}



@end

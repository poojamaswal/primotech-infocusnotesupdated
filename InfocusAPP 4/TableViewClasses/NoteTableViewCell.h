//
//  NoteTableViewCell.h
//  InfocusNotesApp
//
//  Created by jaswinder blagun on 18/09/2015.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACEDrawingView.h"
#import "RichTextEditor.h"

@protocol NoteTableViewCellDelegate <NSObject>

- (void)updatePanViewWithImage:(UIImage*)image;

@end

@interface NoteTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet ACEDrawingView *drawingView;
@property (nonatomic, weak) IBOutlet UIView *savedDrawingView;
@property (nonatomic, weak) IBOutlet UIView *panDrawingView;
@property (nonatomic, weak) IBOutlet RichTextEditor *richTextEditor;

@property (nonatomic, strong) CAShapeLayer *selectionShapeLayer;
//@property (nonatomic, assign) NotesPage *currentPage;
@property (nonatomic, assign) CGRect currentFrame;
@property (nonatomic, assign) CGFloat defaultWidthOfPanDrawing;
@property (nonatomic, assign) CGFloat defaultHeightOfPanDrawing;
@property (nonatomic, strong) NSArray *exclusionPathsArray;

@property (nonatomic, assign) id<NoteTableViewCellDelegate> noteDelegate;

- (void)moveFrameForward;
- (void)moveFrameBackward;

@end

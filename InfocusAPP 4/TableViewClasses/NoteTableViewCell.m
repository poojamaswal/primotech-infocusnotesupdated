//
//  NoteTableViewCell.m
//  InfocusNotesApp
//
//  Created by jaswinder blagun on 18/09/2015.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import "NoteTableViewCell.h"
#import "UIImage+Drawing.h"

@interface NoteTableViewCell () <UITextViewDelegate>

//@property (nonatomic, strong) NotesText *notesText;

@end

@implementation NoteTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self configureView];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self configureView];
    }
    return self;
}

- (void)configureView {
    [self setDefaultValueInRichTextEditor];
    
    //Handles Images when tapped
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [gestureRecognizer setDelegate:self];
    [_richTextEditor addGestureRecognizer:gestureRecognizer];
}

- (void)setDefaultValueInRichTextEditor {
    UIFont *font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    UIColor *textColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f];
    
    NSDictionary *attrs = @{NSForegroundColorAttributeName: textColor,
                            NSFontAttributeName:            font,
                            NSTextEffectAttributeName:      NSTextEffectLetterpressStyle};
    
    NSAttributedString *attrString = [[NSAttributedString alloc]initWithString:@" \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" attributes:attrs];
    _richTextEditor.attributedText = attrString;
}

- (void)handleTapGesture:(UITapGestureRecognizer*)currentGestureRecognizer {
    //Finds cursor position for the location touched
    if (currentGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        self.richTextEditor.editable = YES;
        self.richTextEditor.dataDetectorTypes = UIDataDetectorTypeNone;
        [self.richTextEditor becomeFirstResponder];
        
        //Consider replacing self.view here with whatever view you want the point within
        CGPoint point = [currentGestureRecognizer locationInView:self.contentView];
        UITextPosition *position=[self.richTextEditor closestPositionToPoint:point];
        [self.richTextEditor setSelectedTextRange:[self.richTextEditor textRangeFromPosition:position toPosition:position]];
    }
}

//- (void)setCurrentPage:(NotesPage *)currentPage {
//    _currentPage = currentPage;
//    if (currentPage.hasText.count>0) {
//        for (NotesText *obj in currentPage.hasText) {
//            self.notesText = obj;
//        }
//    }
//    else {
//        self.notesText = [[AppModel sharedObject] createEntityWithName:@"NotesText"];
//        self.notesText.heldByPage = currentPage;
//    }
//    
//    if (self.notesText.textData.bytes>0) {
//        NSAttributedString *attrString = [NSKeyedUnarchiver unarchiveObjectWithData:self.notesText.textData];
//        if (attrString) {
//            _richTextEditor.attributedText = attrString;
//        }
//    }
//    else {
//        [self setDefaultValueInRichTextEditor];
//    }
//}

- (void)setExclusionPathsArray:(NSArray *)exclusionPathsArray {
    _exclusionPathsArray = exclusionPathsArray;
    //self.richTextEditor.textContainer.exclusionPaths = exclusionPathsArray;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)drawRect:(CGRect)rect {
//    UIImage *image = [UIImage imageWithView:self.savedDrawingView fromRect:self.savedDrawingView.frame];
//    image = [UIImage imageWithData:self.currentPage.pageDrawingImageData];
//    
//    CALayer *newLayer = [CALayer layer];
//    newLayer.frame = self.drawingView.bounds;
//    newLayer.contents = (__bridge id)(image.CGImage);
//    [self.drawingView.layer addSublayer:newLayer];
//    //[image drawInRect:self.drawingView.frame];
//}

- (void)setCurrentFrame:(CGRect)currentFrame {
    _currentFrame = currentFrame;
    [self updateSelectionLayerPosition];
}

#pragma mark - Layer Selection methods
- (CAShapeLayer *)selectionShapeLayer {
    if (!_selectionShapeLayer) {
        _selectionShapeLayer = [CAShapeLayer layer];
        _selectionShapeLayer.lineWidth = 2;
        _selectionShapeLayer.strokeColor = [UIColor redColor].CGColor;
        _selectionShapeLayer.fillColor = [UIColor clearColor].CGColor;
        [self.panDrawingView.layer addSublayer:_selectionShapeLayer];
        _selectionShapeLayer.hidden = YES;
    }
    
    return _selectionShapeLayer;
}

- (void)updateSelectionLayerPosition {
    if (CGRectIsNull(self.currentFrame)) {
        self.selectionShapeLayer.hidden = YES;
    }
    else {
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.currentFrame.origin.x-2, self.currentFrame.origin.y-2, self.currentFrame.size.width+4, self.currentFrame.size.height+4)];
        self.selectionShapeLayer.path = bezierPath.CGPath;
        self.selectionShapeLayer.hidden = NO;
        
        //Update Image
        UIImage *image = [UIImage imageWithView:self fromRect:self.currentFrame];
        [self.noteDelegate updatePanViewWithImage:image];
    }
}

- (void)moveFrameForward {
    CGRect newFrame = self.currentFrame;
    newFrame.origin.x = self.currentFrame.origin.x + self.currentFrame.size.width;
    newFrame.size = CGSizeMake(self.defaultWidthOfPanDrawing, self.defaultHeightOfPanDrawing);
    if (newFrame.origin.x >= self.panDrawingView.frame.size.width) {
        newFrame.origin.x = 0;
        newFrame.origin.y = self.currentFrame.origin.y + newFrame.size.height;
        if (newFrame.origin.y >= self.panDrawingView.frame.size.height) {
            newFrame.origin.y = 0;
        }
        else if ((newFrame.origin.y + newFrame.size.height) > self.panDrawingView.frame.size.height) {
            newFrame.origin.y = self.panDrawingView.frame.size.height - newFrame.size.height;
        }
    }
    else if ((newFrame.origin.x + newFrame.size.width) > self.panDrawingView.frame.size.width) {
        newFrame.origin.x = self.panDrawingView.frame.size.width - newFrame.size.width;
    }
    self.currentFrame = newFrame;
}

- (void)moveFrameBackward {
    CGRect newFrame = self.currentFrame;
    newFrame.origin.x = self.currentFrame.origin.x - self.currentFrame.size.width;
    newFrame.size = CGSizeMake(self.defaultWidthOfPanDrawing, self.defaultHeightOfPanDrawing);
    if ((newFrame.origin.x + newFrame.size.width) <= 0) {
        newFrame.origin.x = self.panDrawingView.frame.size.width - newFrame.size.width;
        newFrame.origin.y = self.currentFrame.origin.y - newFrame.size.height;
        if ((newFrame.origin.y + newFrame.size.height) <= 0) {
            newFrame.origin.y = self.panDrawingView.frame.size.height - newFrame.size.height;
        }
        else if (newFrame.origin.y < 0) {
            newFrame.origin.y = 0;
        }
    }
    else if (newFrame.origin.x < 0) {
        newFrame.origin.x = 0;
    }
    self.currentFrame = newFrame;
}

#pragma mark - Touch events
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.view == self.panDrawingView) {
        CGPoint touchPoint = [touch locationInView:self.panDrawingView];
        self.currentFrame = CGRectMake(touchPoint.x, touchPoint.y, self.defaultWidthOfPanDrawing, self.defaultHeightOfPanDrawing);
    }
}

//#pragma mark - TextView Delegates
//- (void)textViewDidEndEditing:(UITextView *)textView {
//    NSData *textData = [NSKeyedArchiver archivedDataWithRootObject:self.richTextEditor.attributedText];
//    if (self.notesText) {
//        self.notesText.textData = textData;
//        [[AppModel sharedObject] saveEntity:self.notesText];
//    }
//}

@end

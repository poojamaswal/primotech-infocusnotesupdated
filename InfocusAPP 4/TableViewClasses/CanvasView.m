//
//  CanvasView.m
//  InfocusNotesApp
//
//  Created by Seema Blagun on 07/09/15.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import "CanvasView.h"
#import "SPUserResizableView.h"
#import "NoteTableViewCell.h"
#import "ToolSelectionObject.h"

#import "UIImage+Drawing.h"
#import "INFPopoverController.h"
#import "MBProgressHUD.h"

@interface CanvasView () <ACEDrawingViewDelegate, SPUserResizableViewDelegate, UITableViewDataSource, UITableViewDelegate, ToolSelectionDelegate, NoteTableViewCellDelegate> {
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
}

@property (nonatomic, strong) NoteTableViewCell *selectedCell;
@property (nonatomic, assign) NSInteger selectedRow;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *panViewButtonBottomConstraint;
@property (nonatomic, weak) IBOutlet UIView *movePanView;
@property (nonatomic, weak) IBOutlet UIButton *movePanViewButton;

- (IBAction)panViewButtonClick:(UIButton*)sender;

@end

@implementation CanvasView

static NSString *cellIdentifierString = @"NoteTableCell";

#pragma mark - UIView events
- (void)viewWillDisappear {
//    if (dataManager.currentNote) {
//        [[AppModel sharedObject] saveEntity:dataManager.currentNote];
//    }
}

#pragma mark - Initial Settings
- (void)configureView {
    self.isScrollingEnabled = NO;
    self.panView.drawingView.delegate = self;
    
    //Register NoteTableCell
    UINib *cellNib = [UINib nibWithNibName:@"NoteTableViewCell" bundle:[NSBundle mainBundle]];
    [self.noteTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifierString];
}

- (void)initialViewSettings {
    self.panView.drawingView.lineWidth = 25;
    self.panView.drawingView.isPanView = YES;
    
    // Tool selection
    [ToolSelectionObject sharedObject].delegate = self;
}

//- (void)loadSavedDrawingForCurrentPage {
//    // Set Current note and its last used settings
//    if (dataManager.currentNote.drawToolType)
//    {
//        self.drawTool = [dataManager.currentNote.drawToolType intValue];
//    }
//    else
//    {
//        self.drawTool = ACEDrawingToolTypePen;
//    }
//    
//    if (dataManager.currentNote.drawToolColorData.bytes>0) {
//        self.lineColor = [NSKeyedUnarchiver unarchiveObjectWithData:dataManager.currentNote.drawToolColorData];
//    }
//    else {
//        self.lineColor = [UIColor blackColor];
//    }
//    
//    if (dataManager.currentNote.drawToolWidth>0) {
//        self.lineWidth = [dataManager.currentNote.drawToolWidth floatValue];
//    }
//    else {
//        self.lineWidth = 10;
//    }
//    
//    if (dataManager.currentNote.drawToolAlpha > 0) {
//        self.lineAlpha = [dataManager.currentNote.drawToolAlpha floatValue];
//    }
//    else {
//        self.lineAlpha = 1;
//    }
//    
//    //Load first page by default
//    [self reloadDataForPage:[[AppModel sharedObject] getNotesPageFromNote:dataManager.currentNote havingPageNumber:1]];
//}

- (void)bringViewToFront:(viewType)viewType {
    if (viewType == DrawingView) {
        [self.selectedCell.contentView sendSubviewToBack:self.selectedCell.richTextEditor];
        [self.selectedCell.richTextEditor resignFirstResponder];
        
        self.selectedCell.richTextEditor.userInteractionEnabled = NO;
        self.selectedCell.richTextEditor.multipleTouchEnabled = NO;
        self.movePanViewButton.hidden = NO;
    }
    else if (viewType == NoteTextView) {
        [self.selectedCell.contentView bringSubviewToFront:self.selectedCell.richTextEditor];
        
        [currentlyEditingView hideEditingHandles];
        [lastEditedView hideEditingHandles];
        
        self.selectedCell.richTextEditor.userInteractionEnabled = YES;
        self.selectedCell.richTextEditor.multipleTouchEnabled = YES;
        self.selectedCell.richTextEditor.editable = YES;
        self.selectedCell.richTextEditor.canCancelContentTouches = YES;
        [self.selectedCell.richTextEditor becomeFirstResponder];
        
        self.movePanViewButton.hidden = YES;
    }
}

//- (void)reloadDataForPage:(NotesPage *)page {
//    if (page) {
//        dataManager.currentNotesPage = page;
//        self.selectedRow = [dataManager.currentNotesPage.pageNumber integerValue]-1;
//        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedRow inSection:0];
//        [self.noteTableView reloadData];
//        [self.noteTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//        
//        //Configure Selected Cell
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
//        hud.mode = MBProgressHUDModeIndeterminate;
//        hud.labelText = @"Loading...";
//        
//        //Update drawing paths and images
//        [self updateDrawingPaths:^{
//            [self.selectedCell setNeedsDisplay];
//            
//            [self updateImages:^{
//                [self updateImageForCurrentPage];
//                [MBProgressHUD hideHUDForView:self
//                                     animated:YES];
//                
//                self.selectedCell.drawingView.lineWidth = self.lineWidth;
//                self.selectedCell.drawingView.lineColor = self.lineColor;
//                self.selectedCell.drawingView.lineAlpha = self.lineAlpha;
//                self.selectedCell.drawingView.drawTool = self.drawTool;
//            }];
//        }];
//    }
//}

- (void)updateDrawingPaths:(void (^)(void))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        completionHandler();
    });
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"recordIndex" ascending:YES]];
//        //Sorting
//        NSArray *sortedArray = [self.selectedCell.currentPage.hasDrawingPaths sortedArrayUsingDescriptors:sortDescriptors];
//        for (DrawingPath *drawingPath in sortedArray) {
//            CGFloat lineWidth = [drawingPath.pathWidth floatValue];
//            CGFloat lineAlpha = [drawingPath.pathAlpha floatValue];
//            UIColor *lineColor = self.backgroundColor;
//            if (drawingPath.pathColorData.bytes>0) {
//                lineColor = [NSKeyedUnarchiver unarchiveObjectWithData:drawingPath.pathColorData];
//            }
//            UIColor *fillColor = [NSKeyedUnarchiver unarchiveObjectWithData:drawingPath.pathFillColorData];
//            UIBezierPath *drawableBezierPath = [NSKeyedUnarchiver unarchiveObjectWithData:drawingPath.pathData];
//            
//            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//            shapeLayer.lineCap = kCALineCapRound;
//            shapeLayer.lineWidth = lineWidth;
//            shapeLayer.strokeColor = [lineColor colorWithAlphaComponent:lineAlpha].CGColor;
//            shapeLayer.fillColor = fillColor.CGColor;
//            shapeLayer.path = drawableBezierPath.CGPath;
//            [self.selectedCell.savedDrawingView.layer addSublayer:shapeLayer];
//            
//            [self.selectedCell.drawingView.exclusionPathsArray addObject:drawableBezierPath];
//        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            completionHandler();
//        });
//    });
}

//- (void)updateImages:(void (^)(void))completionHandler {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"recordIndex" ascending:YES]];
//        //Sorting
//        NSArray *sortedArray = [self.selectedCell.currentPage.hasImages sortedArrayUsingDescriptors:sortDescriptors];
//        for (NotesImage *notesImage in sortedArray)
//        {
//            CGRect frame = CGRectMake([notesImage.xVal floatValue], [notesImage.yVal floatValue], [notesImage.width floatValue], [notesImage.height floatValue]);
//            UIImage *image = [NSKeyedUnarchiver unarchiveObjectWithData:notesImage.imageData];
//            
//            SPUserResizableView *userResizableView = [[SPUserResizableView alloc] initWithFrame:frame];
//            UIImageView *contentView = [[UIImageView alloc] initWithImage:image];
//            userResizableView.contentViewSPU = contentView;
//            userResizableView.delegateSPU = self;
//            [userResizableView showEditingHandles];
//            [self.selectedCell.contentView addSubview:userResizableView];
//            
//            userResizableView.associatedImageObject = notesImage;
//            [userResizableView hideEditingHandles];
//            
//            // Update ExclusionPaths
//            userResizableView.bezierPath = [UIBezierPath bezierPathWithRect:userResizableView.frame];
//            [self.selectedCell.drawingView.exclusionPathsArray addObject:userResizableView.bezierPath];
//        }
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            completionHandler();
//        });
//    });
//}

#pragma mark - Getter-Setter events
- (void)setSelectedCell:(NoteTableViewCell *)selectedCell {
    _selectedCell = selectedCell;
    _selectedCell.defaultWidthOfPanDrawing = self.panView.drawingView.frame.size.width/5;
    _selectedCell.defaultHeightOfPanDrawing = self.panView.drawingView.frame.size.height/5;
}

- (void)setIsScrollingEnabled:(BOOL)isScrollingEnabled {
    _isScrollingEnabled = isScrollingEnabled;
    self.noteTableView.scrollEnabled = _isScrollingEnabled;
}

- (void)setLineWidth:(CGFloat)lineWidth {
    _lineWidth = lineWidth;
    self.selectedCell.drawingView.lineWidth = lineWidth;
    self.panView.drawingView.lineWidth = lineWidth*5;
    
   // dataManager.currentNote.drawToolWidth = [NSNumber numberWithFloat:lineWidth];
}

- (void)setLineColor:(UIColor *)lineColor {
    _lineColor = lineColor;
    self.selectedCell.drawingView.lineColor = lineColor;
    self.panView.drawingView.lineColor = lineColor;
    
   // dataManager.currentNote.drawToolColorData = [NSKeyedArchiver archivedDataWithRootObject:lineColor];
}

- (void)setLineAlpha:(CGFloat)lineAlpha {
    _lineAlpha = lineAlpha;
    self.selectedCell.drawingView.lineAlpha = lineAlpha;
    self.panView.drawingView.lineAlpha = lineAlpha;
    
    //dataManager.currentNote.drawToolAlpha = [NSNumber numberWithFloat:lineAlpha];
}

- (void)setDrawTool:(ACEDrawingToolType)drawTool {
    _drawTool = drawTool;
    self.selectedCell.drawingView.drawTool = drawTool;
    self.panView.drawingView.drawTool = drawTool;
    
    //dataManager.currentNote.drawToolType = [NSNumber numberWithInt:drawTool];
}

#pragma mark - Button click events
- (IBAction)panViewButtonClick:(UIButton*)sender {
    self.panView.backgroundColor = self.backgroundColor;
    self.panView.isOpen = !self.panView.isOpen;
    
    [UIView animateWithDuration:0.5 animations:^{
        if (self.panView.isOpen) {
            self.panView.frame = CGRectMake(0, self.frame.size.height-200, self.frame.size.width, 200);
        }
        else {
            self.panView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 200);
        }
        
        CGRect buttonFrame = sender.frame;
        buttonFrame.origin.y = self.panView.frame.origin.y - buttonFrame.size.height;
        sender.frame = buttonFrame;
    } completion:^(BOOL finished)
    
    {
        
        if (self.panView.isOpen) {
            [self.selectedCell.contentView bringSubviewToFront:self.selectedCell.panDrawingView];
            self.panViewButtonBottomConstraint.constant = 200;
            self.isScrollingEnabled = YES;
            self.selectedCell.currentFrame = CGRectMake(self.selectedCell.contentView.center.x, self.selectedCell.contentView.center.y, self.selectedCell.defaultWidthOfPanDrawing, self.selectedCell.defaultHeightOfPanDrawing);
        }
        else {
            [self.selectedCell.contentView sendSubviewToBack:self.selectedCell.panDrawingView];
            self.panViewButtonBottomConstraint.constant = 0;
            self.isScrollingEnabled = NO;
            self.selectedCell.currentFrame = CGRectNull;
        }
        [self bringSubviewToFront:self.panView];
        [self bringSubviewToFront:sender];
        //[self reloadDataForPage:self.currentPage];
    }];
}

#pragma mark - TableView Delegates & DataSources
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.frame.size.height;
}

- (NoteTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierString];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NoteTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    else {
        [cell.drawingView clear];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.drawingView.delegate = self;
    cell.noteDelegate = self;
    
    [cell.drawingView clear];
    
//    NotesPage *notesPage = [[AppModel sharedObject] getNotesPageFromNote:dataManager.currentNote havingPageNumber:indexPath.row+1];
//    cell.currentPage = notesPage;
    if (indexPath.row == self.selectedRow) {
        self.selectedCell = cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(NoteTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - Page events
//- (void)updateImageForCurrentPage {
//    if (self.selectedCell) {
//        UIImage *image = [UIImage imageWithView:self.selectedCell fromPoint:CGPointZero];
//        
//        if (dataManager.currentNotesPage)
//        {
//            dataManager.currentNotesPage.pageImageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
//            dataManager.currentNotesPage.pageDrawingImageData = [NSData dataWithData:UIImagePNGRepresentation([UIImage imageWithView:self.selectedCell.drawingView fromPoint:CGPointZero])];
//            
//            
//            //********* Save Data Here *************//
//            
//            [[AppModel sharedObject] saveEntity:dataManager.currentNotesPage];
//            
//        }
// 
//        self.selectedCell.exclusionPathsArray = [NSArray arrayWithArray:self.selectedCell.drawingView.exclusionPathsArray];
//        
//        if ([self.delegate respondsToSelector:@selector(updatePageTableCellImageForPage:)]) {
//            [self.delegate updatePageTableCellImageForPage:dataManager.currentNotesPage];
//        }
//    }
//}

#pragma mark - ACEDrawing View Delegate
- (void)drawingView:(ACEDrawingView *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool {
    [self hideEditingHandles];
}

- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool withFinalPath:(UIBezierPath *)finalPath {
    
    self.selectedCell.exclusionPathsArray = [NSArray arrayWithArray:self.selectedCell.drawingView.exclusionPathsArray];
    if (view == self.panView.drawingView) {
        CGAffineTransform scaleTransform = self.panView.drawingView.scaleTransform;
        CGPathRef path = CGPathCreateCopyByTransformingPath(finalPath.CGPath, &scaleTransform);
                
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.lineCap = kCALineCapRound;
        shapeLayer.lineWidth = view.lineWidth/5;
        shapeLayer.strokeColor = [view.lineColor colorWithAlphaComponent:view.lineAlpha].CGColor;
        if (view.drawTool == ACEDrawingToolTypeRectagleFill || view.drawTool == ACEDrawingToolTypeEllipseFill) {
            shapeLayer.fillColor = shapeLayer.strokeColor;
        }
        else {
            shapeLayer.fillColor = [UIColor clearColor].CGColor;
        }
        shapeLayer.path = path;
        [self.selectedCell.drawingView.layer addSublayer:shapeLayer];
    }
    
//    [self updateImageForCurrentPage];
}

#pragma mark - ToolSelection Delegate
- (void)drawShapeOfType:(ShapeType)shapeType {
    switch (shapeType) {
        case RectangleFill:
            self.drawTool = ACEDrawingToolTypeRectagleFill;
            break;
            
        case RectangleStroke:
            self.drawTool = ACEDrawingToolTypeRectagleStroke;
            break;
            
        case EllipseFill:
            self.drawTool = ACEDrawingToolTypeEllipseFill;
            break;
            
        case EllipseStroke:
            self.drawTool = ACEDrawingToolTypeEllipseStroke;
            break;
            
        case Line:
            self.drawTool = ACEDrawingToolTypeLine;
            break;
    }
    
    if ([[INFPopoverController currentPopoverController] isPopoverVisible]) {
        [[INFPopoverController currentPopoverController] dismissPopoverAnimated:YES];
    }
}

- (void)drawImageViewWithImage:(UIImage *)image {
    if ([[INFPopoverController currentPopoverController] isPopoverVisible]) {
        [[INFPopoverController currentPopoverController] dismissPopoverAnimated:YES];
    }
    
    if (!image) {
        return;
    }
    
    self.drawTool = ACEDrawingToolTypeImageSPU;
    
    CGRect frame = CGRectMake(0, 0, 100, 100);
    SPUserResizableView *userResizableView = [[SPUserResizableView alloc] initWithFrame:frame];
    userResizableView.center = self.selectedCell.drawingView.center;
    UIImageView *contentView = [[UIImageView alloc] initWithImage:image];
   // userResizableView.contentViewSPU = contentView;
    userResizableView.delegate = self;
    [userResizableView showEditingHandles];
    [self.selectedCell.contentView addSubview:userResizableView];
    
    currentlyEditingView = userResizableView;
    if (lastEditedView) {
        [self hideEditingHandles];
    }
    
    // Update ExclusionPaths
    userResizableView.bezierPath = [UIBezierPath bezierPathWithRect:userResizableView.frame];
    [self.selectedCell.drawingView.exclusionPathsArray addObject:userResizableView.bezierPath];
    [self drawingView:self.selectedCell.drawingView didEndDrawUsingTool:nil withFinalPath:nil];
    
    //Create CoreData Object
   // NSInteger recordIndex = dataManager.currentNotesPage.hasDrawingPaths.count + dataManager.currentNotesPage.hasImages.count;
    
//    NotesImage *notesImage = [[AppModel sharedObject] createEntityWithName:@"NotesImage"];
//    notesImage.imageData = [NSKeyedArchiver archivedDataWithRootObject:image];
//    notesImage.xVal = [NSNumber numberWithFloat:frame.origin.x];
//    notesImage.yVal = [NSNumber numberWithFloat:frame.origin.y];
//    notesImage.width = [NSNumber numberWithFloat:frame.size.width];
//    notesImage.height = [NSNumber numberWithFloat:frame.size.height];
//    notesImage.heldByPage = dataManager.currentNotesPage;
//    notesImage.recordIndex = [NSNumber numberWithInteger:recordIndex];
//    
//    //[[AppModel sharedObject] saveEntity:notesImage];
//    [dataManager.currentNotesPage addHasImagesObject:notesImage];
//    userResizableView.associatedImageObject = notesImage;
    
    //[self updateImageForCurrentPage];
}

- (void)drawSticky {
    [self hideEditingHandles];
    
    self.drawTool = ACEDrawingToolTypeSticky;
    if ([[INFPopoverController currentPopoverController] isPopoverVisible]) {
        [[INFPopoverController currentPopoverController] dismissPopoverAnimated:YES];
    }
    
    CGRect frame = CGRectMake(0, 0, 200, 200);
    
//    UserMovableStickies *newStickey = [[UserMovableStickies alloc] initWithFrame:frame];
//    newStickey.center = self.selectedCell.drawingView.center;
//    [self.selectedCell.contentView addSubview:newStickey];

    [self.delegate hidePopoverController];
}

//- (void)openCameraRoll {
//    if ([self.delegate respondsToSelector:@selector(hidePopoverController)]) {
//        [self.delegate hidePopoverController];
//    }
//
//    // Check Camera availability
//    if (IS_CAMERA_AVAILABLE) {
//        if ([self.delegate respondsToSelector:@selector(openCameraRoll)]) {
//            [self.delegate openCameraRoll];
//        }
//    }
//    else {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Camera is not available." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
//        [alertView show];
//    }
//    
//}

#pragma mark - SPUResizableView Delegates
- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    if (userResizableView.bezierPath) {
        [self.selectedCell.drawingView.exclusionPathsArray removeObject:userResizableView.bezierPath];
    }
    
    currentlyEditingView = userResizableView;
}

//- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
//    lastEditedView = userResizableView;
//    
//    // Update ExclusionPaths
//    userResizableView.bezierPath = [UIBezierPath bezierPathWithRect:userResizableView.frame];
//    [self.selectedCell.drawingView.exclusionPathsArray addObject:userResizableView.bezierPath];
//    [self drawingView:self.selectedCell.drawingView didEndDrawUsingTool:nil withFinalPath:nil];
//    
//    //Update CoreData Object
//    if (userResizableView.associatedImageObject) {
//        NotesImage *notesImage = userResizableView.associatedImageObject;
//        notesImage.xVal = [NSNumber numberWithFloat:userResizableView.frame.origin.x];
//        notesImage.yVal = [NSNumber numberWithFloat:userResizableView.frame.origin.y];
//        notesImage.width = [NSNumber numberWithFloat:userResizableView.frame.size.width];
//        notesImage.height = [NSNumber numberWithFloat:userResizableView.frame.size.height];
//        
//        [[AppModel sharedObject] saveEntity:notesImage];
//    }
//    
//    [self updateImageForCurrentPage];
//}

- (void)hideEditingHandles {
    if (lastEditedView) {
        [lastEditedView hideEditingHandles];
    }
}

#pragma mark - PanView events
- (IBAction)panViewSwipeRight:(UISwipeGestureRecognizer*)sender {
    [self.selectedCell moveFrameBackward];
}

- (IBAction)panViewSwipeLeft:(UISwipeGestureRecognizer*)sender {
    [self.selectedCell moveFrameForward];
}

- (void)updatePanViewWithImage:(UIImage *)image {
    self.panView.displayImage = image;
    
    CGFloat scaleFactor = 0.2;
    CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, scaleFactor, scaleFactor);
    scaleTransform = CGAffineTransformTranslate(scaleTransform, self.selectedCell.currentFrame.origin.x*5, self.selectedCell.currentFrame.origin.y*5);
    self.panView.drawingView.scaleTransform = scaleTransform;
}

#pragma mark - RichTextEditor
- (RichTextEditorToolbarPresentationStyle)presentarionStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return RichTextEditorToolbarPresentationStyleModal;
}

- (UIModalPresentationStyle)modalPresentationStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return UIModalPresentationFormSheet;
}

- (UIModalTransitionStyle)modalTransitionStyleForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return UIModalTransitionStyleFlipHorizontal;
}


- (RichTextEditorFeature)featuresEnabledForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return RichTextEditorFeatureFontSize | RichTextEditorFeatureFont | RichTextEditorFeatureAll;    
}

@end

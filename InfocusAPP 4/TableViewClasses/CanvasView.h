//
//  CanvasView.h
//  InfocusNotesApp
//
//  Created by Seema Blagun on 07/09/15.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACEDrawingView.h"
#import "PanView.h"

typedef enum {
    DrawingView = 0,
    NoteTextView = 1
} viewType;

@protocol CanvasViewDelegate <NSObject>

//- (void)updatePageTableCellImageForPage:(NotesPage*)page;
- (void)openCameraRoll;
- (void)hidePopoverController;

@end

@interface CanvasView : UIView

@property (nonatomic, weak) IBOutlet PanView *panView;
@property (nonatomic, weak) IBOutlet UITableView *noteTableView;

@property (nonatomic, assign) ACEDrawingToolType drawTool;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat lineAlpha;
@property (nonatomic, assign) BOOL isScrollingEnabled;


@property (nonatomic, assign) id<CanvasViewDelegate> delegate;

- (void)configureView;
- (void)initialViewSettings;
- (void)loadSavedDrawingForCurrentPage;
- (void)bringViewToFront:(viewType)viewType;
- (void)hideEditingHandles;
//- (void)reloadDataForPage:(NotesPage*)page;
- (void)viewWillDisappear;

@end

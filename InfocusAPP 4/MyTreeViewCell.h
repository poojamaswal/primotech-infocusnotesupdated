//
//  MyTreeViewCell.h
//  MyTreeViewPrototype
//
//  Created by Jon Limjap on 4/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MyTreeViewCell : UITableViewCell {
	UILabel *valueLabel;
	//UIImageView
	 UIButton *arrowImage;
	UIButton *myButton;
	
	int level;
	BOOL expanded;
	CGRect myImageRect;
	UIImageView *backImageView;
}
@property(nonatomic,strong)UIButton *myButton;
@property (nonatomic, strong) UILabel *valueLabel;
@property (nonatomic, strong) UIButton *arrowImage;
@property (nonatomic) int level;
@property (nonatomic) BOOL expanded;

- (id)initWithStyle:(UITableViewCellStyle)style 
	reuseIdentifier:(NSString *)reuseIdentifier 
			  level:(NSUInteger)_level 
		   expanded:(BOOL)_expanded; 

@end

//
//  NotesViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.


//Steve added
#define TABLE_ROW_HEIGHT_IPHONE		45


#import "NotesViewCantroller.h"
#import "NotesFullViewCantroller.h"
#import "AddNewNotesViewCantroller.h"
#import "OrganizerAppDelegate.h"
#import "InputTypeSelectorViewController.h"
#import "ImgDataObject.h"
#import "SktDataObject.h" 
#import "NotesPictureViewCantroller.h"
#import "CalendarParentController.h"
#import "OrganizerAppDelegate.h"
#import "inputTypeSelectorViewController.h"
#import "searchBarViewController.h"
#import "ProjectMainViewCantroller.h"
//#import "AdWhirlView.h"
#import "LIstToDoProjectViewController.h"
#import "QuantcastMeasurement.h"
#import "Flurry.h" //Steve
#import "PasswordProtectAppViewController.h"//Steve
#import "AddNewNotesViewCantroller.h" //Steve - iPad

@interface NotesViewCantroller()


@property (nonatomic, strong) UIBarButtonItem *addButton_iOS7; //Steve

-(void)showInputOptPopover:(BOOL)show;
-(void)showSearchOptPopover:(BOOL)show;
//-(void)showOptionPopover:(BOOL)show;
//-(void)getValuesFromDatabase;
//-(void)getBadgeCounts;
//-(void)setBadges;

@end



@implementation NotesViewCantroller
@synthesize notesTableView,searchBar;
@synthesize notesDateObjArray;
@synthesize notesTitleDataArray,optionVal,userDefaults,viewStatus;
@synthesize getImgValueArray,getSktValueArray,SetVAL,notespicview;
@synthesize theCopyListOfItems;
//@synthesize adView;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidLoad 
{
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Notes" withLabels:Nil]; //Steve
    [Flurry logEvent:@"Notes"]; //Steve
    
    self.view.userInteractionEnabled = YES;
    isMenuClosed = YES; // if viewDidLoad method is called, then Menu is closed.  Used to set userEndabled to YES when Reveal button clicked (revealToggleHandle)
    
    freeBtn.hidden = YES;
    settingsBtn.hidden = YES;
    
    
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //Steve - loads password viewController
    if ( ![sharedDefaults boolForKey:@"PasswordAccepted"] && [sharedDefaults boolForKey:@"NavigationNew"]){
        
        PasswordProtectAppViewController *passwordProtectAppViewController=
        [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController" bundle:[NSBundle mainBundle]];
        
        [self.navigationController pushViewController:passwordProtectAppViewController animated:NO];
    }
    
    
	if(IS_LITE_VERSION)
    {

        [super viewDidLoad];
        [notesTableView setFrame:CGRectMake(0,41,320, 370)];
    }
    else if (IS_NOTES_VER){
        freeBtn.hidden = NO;
        settingsBtn.hidden = NO;
        [calendarBtn removeFromSuperview];
        [projectBtn removeFromSuperview];
        [todoBtn removeFromSuperview];
        [listBtn removeFromSuperview];
        //Plus icon tag set on XIB and tag = 1116. Plus icon moved over a little to accomadate more space
        [[self.view viewWithTag:1116] setFrame:CGRectMake(275,3,48,34)];
        
        //Removes Home Button, which is a NavBar item
        NSMutableArray     *items = [navBar.items mutableCopy];
        [items removeObjectAtIndex:0];
        navBar.items = items;
        
        //[[[navBar items]objectAtIndex:0] setTitle:@"InFocus Notes"];
        
        //Adds a title on top of NavBar
         UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
         label.backgroundColor = [UIColor clearColor];
         label.numberOfLines = 1;
         label.font = [UIFont boldSystemFontOfSize: 18.0f];
         label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
         label.textAlignment = NSTextAlignmentCenter;
         label.textColor = [UIColor whiteColor];
         label.text = @"InFocus Notes";
         [self.view addSubview:label];
         

    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    // NSLog(@"%s",__FUNCTION__);
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //NSLog(@"New Navigation");
    
    self.navigationController.navigationBarHidden = NO;
    navBar.hidden = YES;
    homeBtn.hidden = YES;
    todoBtn.hidden = YES;
    listBtn.hidden = YES;
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    searchBar.delegate = self;
    [searchBar setTintColor:[UIColor darkGrayColor]];
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
    notesTableView.backgroundColor = [UIColor whiteColor];
    
    
    
    UIBarButtonItem *revealButtonItem;
    
    //iPhone only
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        self.view.backgroundColor = [UIColor blackColor];
        
        
        revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(revealToggleHandle)];
        
        
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        
        //Add Button
        addBtn.hidden = YES;
        _addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewnotes_Clicked:)];
        self.navigationItem.rightBarButtonItem = _addButton_iOS7;
        
    }
    else{ //iPad
        
        self.view.backgroundColor = [UIColor whiteColor];
    }
    
    
    
    ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
        
        revealButtonItem.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        _addButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
        searchBar.barTintColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:1.0];
        
        
        
    }
    else{ //Dark Theme
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
        revealButtonItem.tintColor = [UIColor whiteColor];
        _addButton_iOS7.tintColor = [UIColor whiteColor];
        searchBar.tintColor = [UIColor lightGrayColor];
        
    }
    
    ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
    
    
    if (IS_IPHONE_5)
        notesTableView.frame = CGRectMake(0, 0, 320, 416 + 88 + 65);
    else
        notesTableView.frame = CGRectMake(0, 0, 320, 416 + 65);
    
    
    UIEdgeInsets insets = UIEdgeInsetsMake(64, 0, 0, 0);
    notesTableView.contentInset = insets;
    
    
    // ***************** Steve End ***************************
    
    self.title = @"Notes";
    
    
    viewStatus = 1;
    
	notesTitleDataArray = [[NSMutableArray alloc]init];

    
    hideSearchBar = NO;
    
    searching = NO;
    letUserSelectRow = YES;
    self.notesTableView.scrollEnabled = YES;
    self.notesTableView.tableHeaderView=searchBar;
    theCopyListOfItems = [[NSMutableArray alloc] init];
    [self addNotes];
    
	[notesTableView reloadData];
}


- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return self;
	
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //NSLog(@"searchBarSearchButtonClicked");
    
    [self.searchBar resignFirstResponder];
    
    self.notesTableView.scrollEnabled = YES;
	letUserSelectRow = YES;
	//searching = NO; 
    
    [self searchTableView];

    
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     //NSLog(@"searchBarCancelButtonClicked ");

    //Steve changed so icons will appear white again
   // [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];


    
    self.searchBar.text = @"";
    
    [self.searchBar resignFirstResponder];
    self.notesTableView.scrollEnabled = YES;
	letUserSelectRow = YES;
	searching = NO;
    self.searchBar.showsCancelButton = NO;
    
    [self.notesTableView reloadData]; //Steve added
    
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
     //NSLog(@"searchBarTextDidBeginEditing ");
    
 
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
    
    searchBar.showsCancelButton = YES;
    
    
    if (isSearchTextBlank) { //Steve added - fixes bug causes crash ("Search"-->X-->click table-->Crash)
        searching = NO;
        letUserSelectRow = NO;
        self.notesTableView.scrollEnabled = NO;
    }
    else{
        searching = YES;
        letUserSelectRow = YES;
        self.notesTableView.scrollEnabled = YES;
    }
    
    //Add the done button.
    
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
     //NSLog(@"searchBar textDidChange");
    
    //Remove all objects first.
    [theCopyListOfItems removeAllObjects];
    
    searchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([searchText length]>0) {
        isSearchTextBlank = NO;
        searching = YES;
        letUserSelectRow = YES;
        self.notesTableView.scrollEnabled = YES;
        [self searchTableView];
    }
    else {
        isSearchTextBlank = YES;
        searching = NO;
        letUserSelectRow = NO;
        self.notesTableView.scrollEnabled = NO;
    }
    
    [self.notesTableView reloadData];
    
    
    if(searching)
    {
        [searchBar becomeFirstResponder];
    }
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
   // NSLog(@"searchBarTextDidEndEditing");
    
     //[self.notesTableView reloadData];
}



//Steve added for Help views
-(void)viewDidAppear:(BOOL)animated{
  
    
}


-(void) revealToggleHandle{
    //NSLog(@"revealToggleHandle");
    
    if (isMenuClosed){
        self.view.userInteractionEnabled = NO;
        isMenuClosed = NO;
    }
    else{
        self.view.userInteractionEnabled = YES;
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
    [revealController revealToggle:self];
}


- (NSIndexPath *)tableView :(UITableView *)theTableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(letUserSelectRow)
        return indexPath;
    else
        return nil;
}


- (void) searchTableView {
	
	NSString *searchText = searchBar.text;
	NSMutableArray *searchArray = [[NSMutableArray alloc] init];
	
    /*
     for (NSDictionary *dictionary in listOfItems)
     {
     NSArray *array = [dictionary objectForKey:@"Countries"];
     [searchArray addObjectsFromArray:array];
     }
     */
	
    for (id item in notesTitleDataArray)
    {
        NotesDataObject *temp = item;
        [searchArray addObject:[temp notesTitle]];
    }
    
    
    
	for (NSString *sTemp in searchArray)
	{
        
        
        
		NSRange titleResultsRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
		
		if (titleResultsRange.length > 0)
        {
			//[copyListOfItems addObject:sTemp];
            for (id item in notesTitleDataArray)
            {
                NotesDataObject *temp1 = item;
                if([[temp1 notesTitle] isEqualToString:sTemp])
                {
                    [theCopyListOfItems addObject:temp1];
                }
            }
        }
	}
	
    //Steve commented out for ARC
	//[searchArray release];
	searchArray = nil;
}
- (IBAction)listBtnClicked:(id)sender {
    LIstToDoProjectViewController *ptr_ListToDoViewController = [[LIstToDoProjectViewController alloc]initWithNibName:@"LIstToDoProjectViewController" bundle:nil];
    [self.navigationController pushViewController:ptr_ListToDoViewController animated:YES];
}

-(IBAction)CalendarClicked:(id)sender
{
	
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[appDelegate.navigationController popViewControllerAnimated:NO];
	
	CalendarParentController *calendarParentController = [[CalendarParentController alloc] initWithNibName:@"CalendarParentController" bundle:[NSBundle mainBundle]];
	[appDelegate.navigationController pushViewController:calendarParentController animated:YES];
    
    //Steve commented out for ARC
	//[calendarParentController release];
	
}





-(IBAction)inputOptionButton_Clicked:(id)sender 
{
	if (inputTypeSelectorViewController) {
		if ([inputTypeSelectorViewController.view alpha] == 0) {
            if(![self.view viewWithTag:IPTYPE_VC_VIEW_TAG]){
                [self.view addSubview:inputTypeSelectorViewController.view];
            }
			[self showInputOptPopover:YES];
		}else {
            [self showInputOptPopover:NO];
		}
	}else {
		[self showInputOptPopover:YES];
	}
	[self showSearchOptPopover:NO];
}

-(void)showInputOptPopover:(BOOL) show {
	if (show) {
		if (inputTypeSelectorViewController) {
			if ([inputTypeSelectorViewController.view alpha] == 0) {
				[inputTypeSelectorViewController.view setAlpha:1];
			}
		}else {
			inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(10, 291, 150, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(50, 402) andParentVC:self fromMainScreen:YES];
			inputTypeSelectorViewController.view.tag = IPTYPE_VC_VIEW_TAG;
			[inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
			[self.view addSubview:inputTypeSelectorViewController.view];
		}
	} else {
		if (inputTypeSelectorViewController) {
			if ([inputTypeSelectorViewController.view alpha] == 1) {
				[inputTypeSelectorViewController.view setAlpha:0];
			}else {
				[inputTypeSelectorViewController.view removeFromSuperview];
                //Steve commented out for ARC
				//[inputTypeSelectorViewController release];
				inputTypeSelectorViewController = nil;
			}
		}	
	}
	[self.view bringSubviewToFront:inputTypeSelectorViewController.view];
}
-(void)showSearchOptPopover:(BOOL) show 
{
    //NSLog(@" showSearchOptPopover");
    
    
	if (show) {
		if (searchBarViewController) {
            
            //Steve commented out for ARC
			//[searchBarViewController release];
			searchBarViewController = nil;
			
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
			
		}else {
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(58, 316, 300, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(102, 396) withParentVC:self];
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
		}
	} else {
		if (searchBarViewController) {
			if ([searchBarViewController.view alpha] == 1) {
				[searchBarViewController.view setAlpha:0];
			}else {
				[searchBarViewController.view removeFromSuperview];
                //Steve commented out for ARC
				//[searchBarViewController release];
				searchBarViewController = nil;
			}
		}	
	}
	[self.view bringSubviewToFront:searchBarViewController.view];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{	
    return 1;
	//return [notesTitleDataArray count];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    
    if (searching)
		return [theCopyListOfItems count];
	else {
        
        return [notesTitleDataArray count];
    }
	//return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(viewStatus == 1)
	{
        return TABLE_ROW_HEIGHT_IPHONE;
	}
	else {
		
        return 140;
    }
}


-(void)showView:(UIButton *)sender
{
	//NSLog(@"%d",[sender tag]);
    
	NotesFullViewCantroller *notesFullViewCantroller = [[NotesFullViewCantroller alloc] initWithDataObj:[notesTitleDataArray objectAtIndex: sender.tag]];
	if(!searching)
        notesFullViewCantroller.fetchNotesObject=[[notesTitleDataArray objectAtIndex:[sender tag]]notesID];
    else
        notesFullViewCantroller.fetchNotesObject=[[theCopyListOfItems objectAtIndex:[sender tag]]notesID];
	
	[[self navigationController] pushViewController:notesFullViewCantroller animated:YES];
			
}


-(void)showPicture:(id)sender
{
	//NSLog(@" value is %d",[sender tag]);
    
	NotesPictureViewCantroller *pictureView =[[NotesPictureViewCantroller alloc]initWithNibName:@"NotesPictureViewCantroller" bundle:nil];
	pictureView.GetnotesID = [sender tag];
	[[self navigationController] pushViewController:pictureView animated:YES];

}


//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // NSLog(@"%f",scrollView.contentOffset.y);
    
    if(scrollView.contentOffset.y < 0) {
        
        searchBar.frame = CGRectMake(0, scrollView.contentOffset.y, searchBar.frame.size.width, searchBar.frame.size.height);
        hideSearchBar = NO;
    }
    else if (scrollView.contentOffset.y >= 45)
    {
        //        [self.notesTableView setContentOffset:CGPointMake(0,44+scrollView.contentOffset.y) animated:NO];
        //      searchBar.frame = CGRectMake(0,44-scrollView.contentOffset.y, searchBar.frame.size.width, searchBar.frame.size.height);
        //        
        //        hideSearchBar = YES;
        
        
    }
    else if(scrollView.contentOffset.y >0 && scrollView.contentOffset.y < 44)
    {
        if(!hideSearchBar)
        {
            searchBar.frame = CGRectMake(0,-scrollView.contentOffset.y, searchBar.frame.size.width, searchBar.frame.size.height);
            hideSearchBar = NO;
        }
        
    }
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(hideSearchBar)
    {
       // NSLog(@"Hide Search Bar");
        //   [self.notesTableView setContentOffset:CGPointMake(0,44) animated:NO];
        
    }
    else
    {
        
    }
    
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	
    if (IS_IOS_7) {
        
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        
           cell.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    }
    
	
    if (searching)
    {
        if([theCopyListOfItems count]>0)
        {
            for (UIView *subView in [cell.contentView subviews])
            {
                [subView removeFromSuperview];
            }
            
            if([[[theCopyListOfItems objectAtIndex:indexPath.row]notesTitleType]isEqualToString:@"H"])
            {
                UIImage *titleImage =
                [[UIImage alloc] initWithData: [[theCopyListOfItems objectAtIndex:indexPath.row]notesTitleBinary]];
                
                //Steve changed
                //Y is TABLE_ROW_HEIGHT_IPHONE/2 - (GRect Height/2)
                //UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(10,2,250,30)];
                UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(10,TABLE_ROW_HEIGHT_IPHONE/2 -15,250,30)];
                
                [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
                [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
                titleImageView.tag=indexPath.row;
                [titleImageView addTarget:self action:@selector(showView:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:titleImageView];
                //Steve commented out for ARC
                //[titleImageView release];
            }
            else {
                //Steve changed
                //Y is TABLE_ROW_HEIGHT_IPHONE/2 - (GRect Height/2)
                // UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,2, 250,25)];
                UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,TABLE_ROW_HEIGHT_IPHONE/2 - 15, 250,30)];
                [detailLabel setBackgroundColor:[UIColor clearColor]];
                detailLabel.tag = 20;
                [cell.contentView addSubview:detailLabel];
                //Steve changed
                // detailLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
                [detailLabel setFont:[UIFont fontWithName:@"Noteworthy-Bold" size: 18.0]];
                NSString *cellValue = [[theCopyListOfItems objectAtIndex:indexPath.row]notesTitle];
                detailLabel.text=cellValue;
                [detailLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth]; //Steve
                [cell.contentView addSubview:detailLabel];
                //Steve commented out for ARC
                //[detailLabel release];
            }
            
            
            
            UIImage *titleImage =
            [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
            
            //Steve changed
            //Y is TABLE_ROW_HEIGHT_IPHONE/2 - (GRect Height/2)
            //UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(290.0, 05.0,23.0, 23.0)];
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(290.0,TABLE_ROW_HEIGHT_IPHONE/2 - 11.9,23.0, 23.0)];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            
            titleImageView.layer.masksToBounds = YES;
            [titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
            titleImageView.tag=indexPath.row;
            [titleImageView addTarget:self action:@selector(showView:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:titleImageView];
            //Steve commented out for ARC
            // [titleImageView release];
            
            
        }
        
    }
	else {
        
        
        if([notesTitleDataArray count]>0)
        {
            for (UIView *subView in [cell.contentView subviews])
            {
                [subView removeFromSuperview];
            }
            
            if([[[notesTitleDataArray objectAtIndex:indexPath.row]notesTitleType]isEqualToString:@"H"])
            {
                UIImage *titleImage =
                [[UIImage alloc] initWithData: [[notesTitleDataArray objectAtIndex:indexPath.row]notesTitleBinary]];
                
                //Steve changed
                //Y is TABLE_ROW_HEIGHT_IPHONE/2 - (GRect Height/2)
                //UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(10,2,250,30)];
                UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(10,TABLE_ROW_HEIGHT_IPHONE/2 -15,250,30)];
                
                [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
                [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
                titleImageView.tag=indexPath.row;
                [titleImageView addTarget:self action:@selector(showView:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:titleImageView];
                //Steve commented out for ARC
                // [titleImageView release];
            }
            else {
                //Steve changed
                //UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,2, 250,25)];
                UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,TABLE_ROW_HEIGHT_IPHONE/2 - 15, 250,30)];
                [detailLabel setBackgroundColor:[UIColor clearColor]];
                detailLabel.tag = 20;
                [cell.contentView addSubview:detailLabel];
                //Steve changed
                //detailLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
                [detailLabel setFont:[UIFont fontWithName:@"Noteworthy-Bold" size: 18.0]];
                NSString *cellValue = [[notesTitleDataArray objectAtIndex:indexPath.row]notesTitle];
                detailLabel.text=cellValue;
                [detailLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth]; //Steve
                [cell.contentView addSubview:detailLabel];
                //Steve commented out for ARC
                // [detailLabel release];
            }
            
            
            
            UIImage *titleImage =
            [UIImage imageNamed:@"DetailsDisclouserBtn.png"];
            
            //Steve changed
            //Y is TABLE_ROW_HEIGHT_IPHONE/2 - (GRect Height/2)
            //UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(290.0, 05.0,23.0, 23.0)];
            UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(290.0, TABLE_ROW_HEIGHT_IPHONE/2 -11.9,23.0, 23.0)];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
            [titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
            
            titleImageView.layer.masksToBounds = YES;
            [titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
            titleImageView.tag=indexPath.row;
            [titleImageView addTarget:self action:@selector(showView:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:titleImageView];
            //Steve commented out for ARC
            //[titleImageView release];
            
            
        }
    }


	//cell.selectionStyle=UITableViewCellSelectionStyleNone;
	
    return cell;
    
}


//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return searchBarPlaceholderView;
//    
//}

//Steve added back.  It was commented out

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{	
    ///////////////////// Steve added ////////////////////////////////
    
   // NSLog(@"%d",indexPath.row);
    NotesFullViewCantroller *notesFullViewCantroller = [[NotesFullViewCantroller alloc] initWithDataObj:[notesTitleDataArray objectAtIndex: indexPath.row]];
    if(!searching)
        notesFullViewCantroller.fetchNotesObject=[[notesTitleDataArray objectAtIndex:indexPath.row]notesID];
    else
        notesFullViewCantroller.fetchNotesObject=[[theCopyListOfItems objectAtIndex:indexPath.row]notesID];
    
    [[self navigationController] pushViewController:notesFullViewCantroller animated:YES];
    
    ///////////////////// Steve added End ////////////////////////////////
    
    //Steve commented out
    /*
     NotesFullViewCantroller *notesFullViewCantroller = [[NotesFullViewCantroller alloc] initWithNibName:@"NotesFullViewCantroller" bundle:nil];
     //NSUInteger section = [indexPath section];
     notesFullViewCantroller.fetchNotesObject=[[notesTitleDataArray objectAtIndex:indexPath.section]notesID];
     
     [[self navigationController] pushViewController:notesFullViewCantroller animated:YES];
     //Steve commented out for ARC
     //[notesFullViewCantroller release];	
     */
    
}


/* Methods Sesction*/

//Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath 
{
	// Return NO if you do not want the specified item to be editable.
	return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        BOOL isDeleted;
        
        isDeleted = [AllInsertDataMethods deleteNoteWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteHandwritingObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteImageObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteTextObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteWebObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteMapObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        //     isDeleted = [AllInsertDataMethods deleteNoteBMObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        isDeleted = [AllInsertDataMethods deleteNoteSketchObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
        
        
        [self addNotes];
        
        [tableView reloadData];
        
    }
    
    
    
}


-(void)addNotes
{
	//notesDateObjArray = [[NSMutableArray alloc]initWithObjects:@"iPhone", @"iPod",@"iPad",nil];		
	[notesTitleDataArray removeAllObjects];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
		//NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary ,b.ImgID,b.ImgInfo ,c.HwrtID,c.HwertInfo FROM NotesMaster  left outer join (Select * From NotesImgInfo Group by NotesID)  b  on NotesMaster.NoteID=b.NotesID  left outer join (Select * From NotesHwrtInfo Group by NotesID) c on  NotesMaster.NoteID = c.NotesID;"];
        NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT  NotesMaster.NoteID ,NotesMaster.NotesTitleType,NotesMaster.NoteTitle,NotesMaster.NotesTitleBinary, NotesMaster.BelongstoProjectID FROM NotesMaster order by NoteID desc;"];
        
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			//[notesTitleDataArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
                /*
                 NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                 [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                 [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                 [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                 NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                 NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                 [notesDataObject setNotesTitleBinary:binaryDataL];
                 
                 [notesDataObject setFImgID:sqlite3_column_int(selectNotes_Stmt, 4)];
                 
                 NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 5);
                 NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 5) length:blobLength];
                 [notesDataObject setFirstImg:binaryData];
                 
                 [notesDataObject setFSktID:sqlite3_column_int(selectNotes_Stmt, 6)];
                 
                 NSUInteger blobLength1 = sqlite3_column_bytes(selectNotes_Stmt, 7);
                 NSData *binaryData1 = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 7) length:blobLength1];
                 [notesDataObject setFirstSkt:binaryData1];
                 
                 
                 [notesTitleDataArray addObject:notesDataObject];
                 */                
                NotesDataObject* notesDataObject = [[NotesDataObject alloc]init];
                [notesDataObject setNotesID: sqlite3_column_int(selectNotes_Stmt, 0)];
                [notesDataObject setNotesTitleType:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
                [notesDataObject setNotesTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
                NSUInteger binaryength = sqlite3_column_bytes(selectNotes_Stmt, 3);
                NSData *binaryDataL = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 3) length:binaryength];
                [notesDataObject setNotesTitleBinary:binaryDataL];
                [notesDataObject setBelongsToProjectID:sqlite3_column_int(selectNotes_Stmt, 4)];
                
                [notesTitleDataArray addObject:notesDataObject];
                
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
        //Steve commented out for ARC
		//[Sql release];
	}
}

-(void)setNext:(id)sender
{
	NotesFullViewCantroller *notesFullViewCantroller = [[NotesFullViewCantroller alloc] initWithNibName:@"NotesFullViewCantroller" bundle:nil];
	//NSUInteger row = [indexPath row];
	//notesFullViewCantroller.fetchNotesObject=[notesDataArray objectAtIndex: [sender tag]];	
	[[self navigationController] pushViewController:notesFullViewCantroller animated:YES];
    //Steve commented out for ARC
	//[notesFullViewCantroller release];	
	
	
}


-(IBAction)homeButton_Clicked:(id)sender 
{
	
	
	if(SetVAL==1)
	{
		HomeViewController *homeview = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
		
		[[self navigationController] pushViewController:homeview animated:YES];
        //Steve commented out for ARC
		//[homeview release];
	}
	else
	{	
		//[self.navigationController popViewControllerAnimated:YES]; // Anil Changed on 26
        [self.navigationController popToRootViewControllerAnimated:YES];
	}
	
}


- (IBAction)notesViewoption_Clicked:(id)sender
{
	/*
	NotesOptionViewCantroller *notesViewOptionCantroller = [[NotesOptionViewCantroller alloc] initWithNibName:@"NotesOptionViewCantroller" bundle:nil];
	[[self navigationController] pushViewController:notesViewOptionCantroller animated:YES];
    //Steve commented out for ARC
	//[notesViewOptionCantroller release];
	*/
	
}

-(IBAction)addNewnotes_Clicked:(id)sender
{
    //Alok Added Get Count Number of Notes's 
    
    NSString *selectStat=@"select count(NoteID) from NotesMaster";
    int CountRecords = [GetAllDataObjectsClass getCountRecords:selectStat];   
    if(CountRecords >= MAX_LIMIT_NOTE && IS_LITE_VERSION) 
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Notes Limit Reached" message:[NSString stringWithFormat:@"The Lite version is limited to %d notes.",MAX_LIMIT_NOTE] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Upgade", nil];
        [alert show];
    }
    else{
        
        AddNewNotesViewCantroller *addNewNotesViewCantroller = [[AddNewNotesViewCantroller alloc] initWithNibName:@"AddNewNotesViewController2" bundle:nil];
        [[self navigationController] pushViewController:addNewNotesViewCantroller animated:YES];

    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1)
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:PRO_VERSION_URL]];
}


-(IBAction)settingBarButton_Clicked:(id)sender{
    //SettingsViewController *settingsViewController = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    //[self.navigationController pushViewController:settingsViewController animated:YES];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SettingsTableViewController" bundle:nil];
    UIViewController *settingsTableViewController = [sb instantiateViewControllerWithIdentifier:@"SettingsTableViewController"];
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    [self presentViewController:settingsTableViewController animated:YES completion:nil];
}


- (IBAction)infoBarButton_Clicked:(id)sender{
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"FREE Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"FREE Button"]; //Steve
    
    //Steve - goes right to App store instead of using sales page below
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];
    
    //**********************************************************************
    //**************** Steve - Removed Sales Page ***************************
    //**********************************************************************
    
    /*
    UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    [navBarUpgradeView setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    if (IS_IPHONE_5) {
        upgradeView.frame = CGRectMake(0, 0, 320, 568);
        upgradeScrollView.frame = CGRectMake(0, 44, 320, 568);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1088)];
    }
    else{
        upgradeView.frame = CGRectMake(0, 0, 320, 480);
        upgradeScrollView.frame = CGRectMake(0, 44, 320, 480);
        [upgradeScrollView setContentSize:CGSizeMake(320, 1010)];
    }
    
    upgradeScrollView.userInteractionEnabled = YES;
    upgradeScrollView.bounces = NO;
    
    upgradeView.hidden = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromBottom];
    [self.view addSubview:upgradeView];
    */
}

- (IBAction)cancelUpgradeButton_Clicked:(id)sender {
    
    upgradeView.hidden = YES;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [upgradeView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
}

- (IBAction)upgradeButton_Clicked:(id)sender {
    
    [[QuantcastMeasurement sharedInstance] logEvent:@"Upgrade Button" withLabels:Nil];//Steve
    [Flurry logEvent:@"Upgrade Button"]; //Steve
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8"]];
    
}

/*end of Method Section*/


-(void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [addBtn removeFromSuperview];
    }
}

- (void)viewDidUnload
{
    navBar = nil;
    homeBtn = nil;
    listBtn = nil;
    calendarBtn = nil;
    todoBtn = nil;
    projectBtn = nil;
    freeBtn = nil;
    settingsBtn = nil;
    upgradeView = nil;
    upgradeScrollView = nil;
    navBarUpgradeView = nil;
    upgradeScrollView = nil;
    addBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


//Steve commented out for ARC
/*
 - (void)dealloc {
 [super dealloc];
 [notesDateObjArray release];
 //		[notesTitleDataArray release];
 }
 */


- (IBAction)projectButtonClicked:(id)sender {ProjectMainViewCantroller *projectbtn=[[ProjectMainViewCantroller alloc]initWithNibName: @"ProjectMainViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:projectbtn animated:YES];
}

- (IBAction)todoButtonClicked:(id)sender {
    ToDoParentController *toDoParentController = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:toDoParentController animated:YES];
}
@end











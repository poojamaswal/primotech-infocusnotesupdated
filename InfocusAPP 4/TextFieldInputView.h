//
//  TextFieldInputView.h
//  CIO-PS
//
//  Created by Gaurav Goyal on 04/06/15.
//  Copyright (c) 2015 CanvasM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TextFieldInputViewDelegate <NSObject>
-(void)inputViewDidEndEditing:(NSString*)text;

@end


@interface TextFieldInputView : UIViewController

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, assign) id<TextFieldInputViewDelegate> delegate;

@end

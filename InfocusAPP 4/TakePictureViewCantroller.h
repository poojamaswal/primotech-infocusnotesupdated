//
//  TakePictureViewCantroller.h
//  Organizer
//
//  Created by Naresh Chouhan on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TakePictureViewCantroller :UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    
	IBOutlet UIImageView *imageView;
	IBOutlet UIButton *takePictureButton;
	IBOutlet UIButton *selectFromCameraRollButton;
	NSData *myImage;
	id delegate;
	
	/*IBOutlet UIButton *button;
     IBOutlet UIImageView *image;
     UIImagePickerController *imgPicker;
     id delegate;
     NSData *Img;
     NSData *ImgLarg;
	 */
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIToolbar *bottomBar;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    
}
//- (IBAction)grabImage;

//Steve commented out
/*
 -(IBAction)homeButton_Clicked:(id)sender ;
 @property (assign) id delegate;
 @property(nonatomic,retain) UIImageView *imageView;
 @property(nonatomic,retain) UIButton *takePictureButton;
 @property(nonatomic,retain) UIButton *selectFromCameraRollButton;
 @property(nonatomic,retain) NSData *myImage;
 */

-(IBAction)homeButton_Clicked:(id)sender ;
@property (strong) id delegate;
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIButton *takePictureButton;
@property(nonatomic,strong) UIButton *selectFromCameraRollButton;
@property(nonatomic,strong) NSData *myImage;

-(IBAction)getCameraPicture:(id)sender;
-(IBAction)selectExitingPicture;
-(IBAction)SaveMyPic;

/*@property (assign) id delegate;
 @property (nonatomic, retain) UIImagePickerController *imgPicker;
 @property (nonatomic, retain) NSData *Img;
 @property(nonatomic,retain)NSData *ImgLarg;*/

@end

//
//  NotesFullViewCantroller.h
//  Organizer
//
//  Created by Naresh Chouhan on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesDataObject.h"
#import "NotesDetailViewCantroller.h"
#import "FGalleryViewController.h"



@interface NotesFullViewCantroller : UIViewController<FGalleryViewControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
{
	IBOutlet UITableView *fullTable;
	NSMutableArray *showValue;
	NSInteger fetchNotesObject;
	NSString *fetchNotesObjectInfo;
	NSMutableArray *FindNoteValueArray;
	NSMutableArray *FindSktValueArray;
	NSMutableArray *FindImgValueArray;
	NSMutableArray *FindWebValueArray;
	NSMutableArray *FindTextValueArray;
	NSMutableArray *FindHwrtValueArray;
	NSMutableArray *FindTitleArray;
	NotesDataObject *notesDataObject;
	int valueFor[6];
	
	IBOutlet UIView *fullInfoView;
	IBOutlet UIScrollView *fullscrollview;
    UIButton *ret0;
    UIButton *ret7;
	UIButton *ret1;
	UIButton *ret2;
	UIButton *ret3;
	UIButton *ret4;
	UIButton *ret5;
	UIButton *ret6;
	int newVal;
	int contSize;
    
    NSArray *localImages;
	FGalleryViewController *localGallery;
    
    UIImage *currentImage;
    
    NSInteger y;
    NSInteger x;
    NSInteger u;
    NSInteger v;
    
    NSString *yy;
    NSString *xx;
    NSString *uu;
    NSString *vv;
    NSString *Pname;
    NSMutableArray *myArray;	
    NSInteger retVal;
    NSString *Sql;
    IBOutlet UINavigationBar *navBar;

    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *editButton;
    
}

@property(assign)int newVal;
@property(nonatomic, strong) NSMutableArray *FindTitleArray;
@property(nonatomic, strong) NSString		*fetchNotesObjectInfo;

@property(unsafe_unretained)int contSize;
@property(nonatomic,strong)UIButton *ret6;
@property(nonatomic,strong)UIButton *ret5;
@property(nonatomic,strong)UIButton *ret4;
@property(nonatomic,strong)UIButton *ret3;
@property(nonatomic,strong)UIButton *ret2;
@property(nonatomic,strong)UIButton *ret1;
@property(nonatomic,strong)UIButton *ret0;
@property(nonatomic,strong)UIButton *ret7;
@property(nonatomic,strong)UIView *fullInfoView;
@property(nonatomic,strong)UIScrollView *fullscrollview;
@property(unsafe_unretained)int k;
@property(nonatomic, strong) NSMutableArray *FindHwrtValueArray;
@property(nonatomic, strong) NSMutableArray *FindTextValueArray;
@property(nonatomic, strong) NSMutableArray *FindWebValueArray;
@property(nonatomic, strong) NSMutableArray *FindImgValueArray;
@property(nonatomic, strong) NSMutableArray *FindSktValueArray;
@property(nonatomic, strong) NSMutableArray *FindNoteValueArray;
@property(nonatomic,strong) UIImage *currentImage;

-(id)initWithDataObj:(NotesDataObject *)notesDataObjLocal;


-(IBAction)BackButton_Clicked:(id)sender;
//-(void)SelectMapNotes:(NSInteger)NodID;
-(void)SelectSktNotes:(NSInteger)NodID;
-(void)SelectImgNotes:(NSInteger)NodID;
//-(void)SelectWebNotes:(NSInteger)NodID;
-(void)SelectTextNotes:(NSInteger)NodID;
-(void)SelectHwrtNotes:(NSInteger)NodID;
-(void)SelectTitleNotes:(NSInteger)NodID;
- (IBAction)exportButton_Clicked:(id)sender; //Steve


//Steve commented for ARC
/*
 @property(readwrite)NSInteger fetchNotesObject;
 @property(nonatomic,retain)IBOutlet UITableView *fullTable;
 @property(nonatomic,retain)NSMutableArray *showValue;
 */

@property(readwrite)NSInteger fetchNotesObject;
@property(nonatomic,strong)IBOutlet UITableView *fullTable;
@property(nonatomic,strong)NSMutableArray *showValue;


-(void)showFullPicture:(id)sender;
-(void)editNotes:(id)sender;
-(NSInteger)fetch_level_one:(NSInteger)projid;
-(NSString *)getFolderName;


@end

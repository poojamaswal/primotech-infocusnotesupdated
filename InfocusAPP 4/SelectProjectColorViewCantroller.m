//
//  SelectProjectColorViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 11/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SelectProjectColorViewCantroller.h"
#import "MyTreeViewCell.h"


@implementation SelectProjectColorViewCantroller
@synthesize PdataObj,pImagename,finalfArray,GValue,delegate, CdataObj;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	listofIMG=[[NSMutableArray alloc]initWithObjects:@"FolderColor1.png",@"FolderColor2.png",@"FolderColor3.png",@"FolderColor4.png",@"FolderColor5.png",@"FolderColor6.png",@"FolderColor7.png",@"FolderColor8.png",@"FolderColor9.png",@"FolderColor10.png",@"FolderColor11.png",@"FolderColor12.png",@"FolderColor13.png",@"FolderColor14.png",@"FolderColor15.png",@"FolderColor16.png",@"FolderColor17.png",@"FolderColor18.png",@"FolderColor19.png",@"FolderColor20.png",nil];
    
	NSLog(@"%d",[listofIMG count]);
	
	treeNode = [[MyTreeNode alloc] initWithValue:@"Root"];
	for(int i=0;i<[listofIMG count];i++)
	{
		//NSLog(@"%d",[[getLevelRootBelongsToArray objectAtIndex:i]intValue]);
		node1 = [[MyTreeNode alloc] initWithValue:[listofIMG objectAtIndex:i]];
		[treeNode addChild:node1];
	}

}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone
            self.view.backgroundColor = [UIColor whiteColor];
            

            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                            [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                
            }

        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            
            //Steve - No need for UIEdgeInsets on iPad
            if (!IS_IPAD) { //iPhone
                if (IS_IPHONE_5){
                    
                    newTable.frame = CGRectMake(0, 0, 320, 416 + 88 + 64);
                    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
                    [newTable setContentInset:inset];
                }
                else{
                    newTable.frame = CGRectMake(0, 0, 320, 416 + 64);
                    UIEdgeInsets inset = UIEdgeInsetsMake(64, 0, 0, 0);
                    [newTable setContentInset:inset];
                }
            }

            

            
            self.title =@"Folder Color";
            
        }
        else{ //iOS 6
            
            if (IS_IPHONE_5)
                newTable.frame = CGRectMake(0, 44 - 44, 320, 416 + 88);
            else
                newTable.frame = CGRectMake(0, 44 - 44, 320, 416);
            
            
            self.title =@"Folder Color";
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        }
        
    
        
    }
    else{ //Old Navigation
        
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

    }
    
    
    // ***************** Steve End ***************************
    

    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    [newTable setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    

	//NSLog(@"%@",PdataObj.projectIMGname);
	//NSLog(@"%@", CdataObj.calenderIMGname);
	
}


-(IBAction)cancel_click:(id)sender
{
  [self.navigationController popViewControllerAnimated:YES]; 
 
}

-(IBAction)save_click:(id)sender
{
	if ([delegate isKindOfClass:[AddProjectViewCantroller class]])
	{
		[PdataObj setProjectIMGname:pImagename];
		NSArray *rArray;//=[[NSArray alloc]init];
		rArray=[finalfArray objectAtIndex:GValue];
		[self.navigationController popViewControllerAnimated:YES];
		return;
	}
	else if ([delegate isKindOfClass:[AddCalenderViewController class]])
	{
		[CdataObj setCalenderIMGname:pImagename];
		NSArray *rArray;//=[[NSArray alloc]init];
		rArray=[finalfArray objectAtIndex:GValue];
		[self.navigationController popViewControllerAnimated:YES];
		return;
	}
}

#pragma mark -
#pragma mark Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 65;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
	return [treeNode descendantCount];

}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];   
	[[cell arrowImage]  addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
	[[cell arrowImage] setTag:indexPath.row + 1];
	
	
		cell.arrowImage.hidden=YES;
		UIImage *imgl1=[UIImage imageNamed:node.value];
		buttonLevelone = [UIButton buttonWithType:UIButtonTypeCustom];
		[buttonLevelone setBackgroundImage:imgl1 forState:UIControlStateNormal];
		buttonLevelone.frame = CGRectMake(25.0,7.0,235.0, 40.0);
		buttonLevelone.tag=indexPath.row;
		[buttonLevelone addTarget:self action:@selector(selectionMethod:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:buttonLevelone];
		
	
	NSLog(@"%@ == %@ ",node.value,PdataObj.projectIMGname);
	
			
	if (PdataObj != 0) {
		if ([node.value isEqualToString:PdataObj.projectIMGname]) 
		{
			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
			[tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
			[cell.contentView addSubview:tickImageView];
		}
	}
	
	if (CdataObj != 0)
	{
		if ([node.value isEqualToString:CdataObj.calenderIMGname])
		{
			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
			[tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
			[cell.contentView addSubview:tickImageView];
		}
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:indexPath.row + 1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];   
	[[cell arrowImage]  addTarget:self action:@selector(PlusNow:) forControlEvents:UIControlEventTouchUpInside];
	[[cell arrowImage] setTag:indexPath.row + 1];
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	
	[cell.contentView addSubview:tickImageView];
	
    [PdataObj setProjectIMGname:node.value];
	[CdataObj setCalenderIMGname:node.value];
	
	[self.navigationController popViewControllerAnimated:YES];
}








-(void)selectionMethod:(id)sender
{
	
	static NSString *CellIdentifier = @"Cell";
	
	MyTreeNode *node = [[treeNode flattenElements] objectAtIndex:[sender tag]+1];
	MyTreeViewCell *cell = [[MyTreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												 reuseIdentifier:CellIdentifier 
														   level:[node levelDepth] - 1 
														expanded:node.inclusive];  
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	
	[cell.contentView addSubview:tickImageView];
	
    [PdataObj setProjectIMGname:node.value];
	[CdataObj setCalenderIMGname:node.value];
	
	[self.navigationController popViewControllerAnimated:YES];
	
	
}

#pragma mark -
#pragma mark Table view delegate

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [backButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload 
{
    navBar = nil;
    backButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end

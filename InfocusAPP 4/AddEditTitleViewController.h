//
//  AddEditTitleViewController.h
//  Organizer
//
//  Created by Tarun on 4/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppointmentsDataObject.h"
#import "LocationDataObject.h"

@interface AddEditTitleViewController : UIViewController <UITextFieldDelegate> {

	IBOutlet UITextField		*titleTxtFld;
	IBOutlet UILabel			*locationLbl;

	NSString					*appointmentTitle;
	NSString					*appointmentType;
	NSData						*appointmentBinary;
	NSData						*appointmentBinaryLarge;

	LocationDataObject			*locationDataObj;
     
}

@property(nonatomic, retain) UILabel				*locationLbl;
@property(nonatomic, retain) UITextField			*titleTxtFld;

@property(nonatomic, retain) NSString				*appointmentTitle;
@property(nonatomic, retain) NSString				*appointmentType;
@property(nonatomic, retain) NSData					*appointmentBinary;
@property(nonatomic, retain) NSData					*appointmentBinaryLarge;

@property(nonatomic, retain) LocationDataObject		*locationDataObj;

-(id)initWithAppType:(NSString *) appType appTitle:(NSString *) appTitle appBinary:(NSData *) appBinary andLocationDataObject:(LocationDataObject *) locDataObject; 

-(IBAction)inputTypeSelectorButton_Clicked:(id)sender;
-(IBAction)selectLocationButton_Clicked:(id)sender;

-(void)saveLocation;

@end

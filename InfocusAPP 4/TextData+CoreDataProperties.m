//
//  TextData+CoreDataProperties.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/12/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TextData+CoreDataProperties.h"

@implementation TextData (CoreDataProperties)

@dynamic textData;
@dynamic notesID;
@dynamic savedDate;

@end

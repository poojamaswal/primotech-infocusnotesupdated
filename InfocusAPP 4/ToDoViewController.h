//
//  ToDoViewController.h
//  Organizer
//
//  Created by Tarun on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TODODataObject.h"
#import "GetAllDataObjectsClass.h"
#import "AllInsertDataMethods.h"
#import "TODODataObject.h"
#import "ProjectsDataObject.h"
#import "InputTypeSelectorViewController.h"
#import "HandWritingViewController.h"
#import "OrganizerAppDelegate.h"
#import "AddToDoAlertViewController.h"



@protocol ToDoViewControllerDelegate
-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId prentID:(NSInteger)prentID; 
-(void)setStartDateLabel:(NSString*)aStartDate;
-(void)setEndDateLabel:(NSString*)aEndDate;
-(void)setProgressVAL:(NSString*)progress;
-(void)setPriorityNew:(NSString*)priority1 priority2:(NSString*)priority2;
-(void)setAlertString:(NSString *)  alert; // Anil's Addition
-(void)setSecondAlertString:(NSString *)  alert;  // Anil's Addition
-(void)setIsAlertOn:(BOOL)_isAlertOn; // Anil's Addition
-(void) setAlertDate:(NSDate *) alertDate; //Steve
-(void) setAlertSecondDate:(NSDate *) alertSecondDate;//Steve

 @end



@interface ToDoViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource,  UITextFieldDelegate, UITextFieldDelegate, AlertDelegate,HandWritingDelegate>
{
	HandWritingViewController   *objHand;
	IBOutlet UIImageView		*titleImgView;
    IBOutlet UITextField        *titleTextField;
    //UITextField                 *titleTextField;
	IBOutlet UIView				*bgView;
    IBOutlet UIImageView                 *starredImgView;
    UIButton                    *saveButton;
    UIButton                    *updateButton;
    IBOutlet UIButton                    *deleteButton;
    UIButton                    *inputSelecterButton;
	
    UILabel                     *startsLabel;
    UILabel                     *endsLabel;
    UILabel                     *projectNameLabel;
    IBOutlet UILabel                     *priorityLabel;
    UILabel                     *progressLabel;
	
	IBOutlet UIPickerView		*apickerView;
	IBOutlet UIToolbar			*toolbar;
    IBOutlet UILabel                     *lblEndDate;
    IBOutlet  UILabel                     *lblStartDate;
	
	BOOL                        isAdd;
	BOOL                        isStarred;
	
	TODODataObject					*aToDoObj;
	InputTypeSelectorViewController	* inputTypeSelectorViewController;
	
	//NSMutableArray			*progressArray;
	NSMutableArray			*priorityArray;
	NSInteger				intProgress;
	
	NSString				*todoTitle;
	NSString				*todoTitleType;
	NSData					*todoTitleBinary;
	NSData					*todoTitleBinaryLarge;
	NSString				*projectID;
    id                       __unsafe_unretained delegate;
	
	NSString                *priorityChar;
	NSString                *priorityNum;
	NSString                *myVAL;
	NSString                *myVAL1;
	NSString                *fullString;
	UIPanGestureRecognizer  *pan;
	NSInteger               prntVAL;
    
   //__unsafe_unretained IBOutlet UILabel *alertsLabel;//Anils Added
    //__unsafe_unretained IBOutlet UILabel *secondAlertsLabel;//Anils Added

   IBOutlet UILabel                 *alertsLabel;//Anils Added
   IBOutlet UILabel                 *secondAlertsLabel;//Anils Added

    
    IBOutlet UIImageView *box_Line; //Steve added
    
   // __unsafe_unretained IBOutlet UILabel *lbl_Line; //Steve commented
    BOOL isAlertOn; // Anil's Added
    UILocalNotification *notification ; // Anil's Addition
    NSInteger GetappID; // Anil's Addition
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *titleButton;
    
    IBOutlet UIButton *projectButton;
    IBOutlet UILabel *projectFixedLabel;
    IBOutlet UIImageView *projectDisclosureImageView;
    
    IBOutlet UIButton *startEndButton;
    IBOutlet UILabel *startFixedLabel;
    IBOutlet UILabel *dueDateFixedLabel;
    IBOutlet UIImageView *startEndDisclosureImageVw;
    
    IBOutlet UILabel *alertFixedLabel;
    IBOutlet UIImageView *alertDisclosureImageVw;
    
    
}
@property (strong, nonatomic) UIImageView *blurredImageView; //Steve - Blurred Image of screenshot of

@property (unsafe_unretained, nonatomic)  UIButton *btn_Alert;//Anils Added
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img_SecondAlertArrow;//Anils Added
@property (strong, nonatomic)  UILabel *lbl_SecondAlert;//Anils Added
@property (unsafe_unretained, nonatomic)  UIButton *btn_CustomView;//Anils Added
@property (strong, nonatomic)  UIButton *btn_SecondAlert;//Anils Added
@property (strong, nonatomic) IBOutlet UIButton *btn_Completed;//Anils Added
@property (strong, nonatomic) IBOutlet UIButton *btn_Priority;//Anils Added
@property (strong, nonatomic) IBOutlet UIButton *btn_Starred;//Anils Added
@property (strong, nonatomic) IBOutlet UILabel *lbl_Completed;//Anils Added
@property (strong, nonatomic) IBOutlet UILabel *lbl_Priority;//Anils Added
@property (strong, nonatomic) IBOutlet UILabel *lbl_Starred;//Anils Added
@property (strong, nonatomic) IBOutlet UILabel *lbl_PercentageSymbol;//Anils Added
@property (strong, nonatomic) IBOutlet UIImageView *img_CompletedArrow;//Anils Added
@property (strong, nonatomic) IBOutlet UIImageView *img_PriorityArrow;//Anils Added
@property(nonatomic, strong)  IBOutlet UITableView *todoTableView; //Steve



@property(nonatomic,assign)NSInteger prntVAL;
@property(nonatomic,strong)NSString *fullString;
@property(nonatomic,strong)NSString *myVAL;
@property(nonatomic,strong)NSString *myVAL1;
@property (nonatomic, strong) UITextField		*titleTextField;
@property (nonatomic, strong) TODODataObject *aToDoObj;
@property (assign) BOOL isAdd;
@property (assign) BOOL isStarred;
@property (nonatomic, strong) NSString *todoTitle;
@property (nonatomic, strong) NSString *todoTitleType;
@property (nonatomic, strong) NSData *todoTitleBinary;
@property (nonatomic, strong) NSData *todoTitleBinaryLarge;
@property (unsafe_unretained) id delegate;
@property (nonatomic, strong) NSString *projectID;
@property (nonatomic, strong) UILabel	*projectNameLabel;

@property (assign, nonatomic) BOOL isHandwritingShowing; //Steve




-(id)initWithToDoDataObject:(TODODataObject *) todoObj;
-(IBAction) doneButton_Clicked:(id)sender;
-(IBAction) backButon_Clicked:(id)sender;
-(IBAction) inputTypeSelectorButton_Clicked:(id)sender;
-(IBAction) startsEnds_buttonClicked:(id)sender;
-(IBAction)	project_buttonClicked:(id)sender;
-(IBAction)	priority_buttonClicked:(id)sender;
-(IBAction)	progress_buttonClicked:(id)sender;
-(IBAction)	starred_buttonClicked:(id)sender;
-(IBAction) save_buttonClicked:(id)sender;
-(IBAction) update_buttonClicked:(id)sender;
-(IBAction) delete_buttonClicked:(id)sender;
-(IBAction) cancelButton_Clicked:(id)sender;
- (IBAction)alerts_buttonClicked:(id)sender;
- (IBAction)alertsecond_buttonClicked:(id)sender;
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event; 
- (IBAction)helpView_Tapped:(id)sender; //Steve
-(id)initWithProjectName:(NSString *) projectName projectTagInt:(int) projectTag isAdd:(BOOL) isAddTodo; //Steve
-(void) setAlertString_1:(NSString*) alertString_1 setAlertString_2:(NSString *) alertString_2 isAlertOn:(BOOL) isAlertOnOrOff;//Steve (Protocol)



@end

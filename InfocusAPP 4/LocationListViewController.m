//
//  LocationListViewController.m
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocationListViewController.h"
#import "LocationViewController.h"
#import "OrganizerAppDelegate.h"

@interface LocationListViewController()
-(void)getAllLocationData;
@end


@implementation LocationListViewController
@synthesize locationArray;
@synthesize delegate;

#pragma mark - View lifecycle

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withLocationID:(NSInteger) locationId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		lastLocationID = locationId;
    }
    return self;
}

- (void)viewDidLoad
{
 	[locationTblVw.layer setCornerRadius:5.0];
	locationTblVw.layer.masksToBounds = YES;
	locationTblVw.layer.borderColor = [UIColor purpleColor].CGColor;
	locationTblVw.layer.borderWidth = 0.5;

	locationArray = [[NSMutableArray alloc] init];
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	[self getAllLocationData];
	UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[editBtn setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
	[editBtn setFrame:CGRectMake(220, 10, 45, 19)];
	[editBtn addTarget:self action:@selector(editCLicked) forControlEvents:UIControlEventTouchUpInside];
	[self.navigationController.navigationBar addSubview:editBtn];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void) getAllLocationData 
{
	[locationArray  removeAllObjects];
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate]; 

	sqlite3_stmt *selectAllLocationData_stmt = nil;
	
	if(selectAllLocationData_stmt == nil && appDelegate.database) {
		const char* Sql = "Select LocationID, LocationPersonName, LocationAddress,LocationCity, LocationState, LocationZIP,IsVerified From LocationTable";
		
		if(sqlite3_prepare_v2(appDelegate.database, Sql, -1, &selectAllLocationData_stmt, NULL) == SQLITE_OK) {
			while (sqlite3_step(selectAllLocationData_stmt) == SQLITE_ROW) {
				LocationDataObject *locationDataObject = [[LocationDataObject alloc] init ];
                [locationDataObject setLocationID:sqlite3_column_int(selectAllLocationData_stmt, 0)];
                
				[locationDataObject setLocationPersonName:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 1)]];
                [locationDataObject setLocationAddress:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 2)]];
                [locationDataObject setLocationCity:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 3)]];
                [locationDataObject setLocationState:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 4)]];
                [locationDataObject setLocationZip:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAllLocationData_stmt, 5)]];
                [locationDataObject setIsVerified:sqlite3_column_int(selectAllLocationData_stmt, 6)];
                
               	[locationArray addObject:locationDataObject];
			}
			sqlite3_finalize(selectAllLocationData_stmt);		
			selectAllLocationData_stmt = nil;
		}
	}
	return ;
}

-(IBAction)backClicked:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)addClicked:(id)sender
{
	LocationViewController *locationListViewController = [[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:locationListViewController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [locationArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LocationDataObject *locationDataObject = [locationArray objectAtIndex:indexPath.row];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
	if (cell == nil){
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"];
	}

	[cell.textLabel setText:[NSString stringWithFormat:@"%@, %@, %@",[locationDataObject locationAddress],[locationDataObject locationCity],[locationDataObject locationState]]];
	
	if (lastLocationID != 0) {
		if ([locationDataObject locationID] == lastLocationID) {
			UIImage *chekMarkImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TickArrow" ofType:@"png"]]; 
			tickImageView = [[UIImageView alloc] initWithImage:chekMarkImage];
			[tickImageView setFrame:CGRectMake(260, 10, chekMarkImage.size.width, chekMarkImage.size.height)];
			[cell.contentView addSubview:tickImageView];
		}
	}
	return cell;
}

//Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
	    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    id from,to;
    
    from = [locationArray objectAtIndex:fromIndexPath.row];
    to = [locationArray objectAtIndex:toIndexPath.row];
    [locationArray replaceObjectAtIndex:fromIndexPath.row withObject:to];
    [locationArray replaceObjectAtIndex:toIndexPath.row withObject:from];
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
    
	if (tickImageView) {
		[tickImageView removeFromSuperview];
	}
	tickImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickArrow.png"]];
	[tickImageView setFrame:CGRectMake(260, 10, 27, 25)];
	
	[Cell.contentView addSubview:tickImageView];
	
	LocationDataObject *locationDataObject = [locationArray objectAtIndex:indexPath.row];
	selectedObject=locationDataObject;
    NSString* aval=[NSString stringWithFormat:@"%@, %@, %@",[locationDataObject locationAddress],[locationDataObject locationCity],[locationDataObject locationState]];
    //[[self delegate] setLocObject:selectedObject selectedval:aval];
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark All Methods
#pragma mark -

-(void)editCLicked{
	if (locationTblVw.editing==YES) {
		[locationTblVw setEditing:NO];
	}
	else {
		[locationTblVw setEditing:YES];
	}
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



@end

//
//  ProgressPickerView.m
//  Organizer
//
//  Created by Naresh Chauhan on 12/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProgressPickerView.h"


@implementation ProgressPickerView

@synthesize mlabel,delegate,getString,intSTR;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withProgressVAL:(NSString*) progress
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
	{
		self.getString = progress;		
    }
    return self;
}
- (void)viewDidLoad
{
	[super viewDidLoad];
	arrayNo = [[NSMutableArray alloc] init];
	arrayNo = [[NSMutableArray alloc] init ];
	for (int i = 0; i <= 100; i = i+5)
	{
		NSString *strFormed = [NSString stringWithFormat:@"%d", i];
		[arrayNo addObject:strFormed];
	}	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        pickerView.frame = CGRectMake(0, 141 - 44, 320, 216);
        mlabel.frame = CGRectMake(71, 103 - 44, 155, 35);
        
        self.title =@"Add Progress";
        
        [self.navigationController.navigationBar addSubview:doneButton];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

        
    }
    else{
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************

    
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    if([getString length]>0)
        [pickerView selectRow:[arrayNo indexOfObject: getString] inComponent:0 animated:YES];
	
	//mlabel.text=[getString stringByAppendingString:@"%"];
}


-(IBAction)Save_click:(id)sender
{
	[self.delegate setProgressVAL:getString]; 
	[self.navigationController popViewControllerAnimated:YES]; 
}


-(IBAction)Cancel_click:(id)sender
{

	//[[self delegate] setProgressVAL:getString]; 
	[self.navigationController popViewControllerAnimated:YES]; 
	
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
	return 2;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //NSLog(@"didSelectRow");
    
	if (getString) {
		getString = nil;
	}
	getString = [arrayNo objectAtIndex:row];
	//mlabel.text = [getString stringByAppendingString:@"%"];
    
    
    //NSLog(@"row = %d ", row);
    //NSLog(@"component = %d ", component);
    //NSLog(@"getString = %@ ", getString);
    
    if ([getString isEqualToString:@"100"])
    {
        NSLog(@"HERE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"To-do's that are 100% complete will be moved to the \"Done\" Tab"
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
    }

    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
	
	if(component==0)
	{
		return [arrayNo count];
	}
	else
	{
		return 1;
	}
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
		
	if(component==0)
	{
		return [arrayNo objectAtIndex:row];
		
	}
	else
	{
		NSString *str1 = @"%";
		return str1;
	}
	
	
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [backButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }
    
}

- (void)viewDidUnload
{
    doneButton = nil;
    backButton = nil;
    navBar = nil;
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}




@end

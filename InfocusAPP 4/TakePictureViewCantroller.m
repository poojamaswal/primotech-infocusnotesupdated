//
//  TakePictureViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TakePictureViewCantroller.h"
#import "AddNewNotesViewCantroller.h"
#import "UIImage+Resize.h"
#import "UIImage+UIImage_Extensions.h"




@implementation TakePictureViewCantroller

@synthesize imageView;
@synthesize takePictureButton;
@synthesize selectFromCameraRollButton;
@synthesize myImage;
@synthesize delegate;

- (void)viewDidLoad {
	[super viewDidLoad];
	
}



-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        if (IS_IOS_7) {
            
            self.view.autoresizesSubviews = NO;
            self.view.autoresizingMask = NO;
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; //added since Apple's Photo Library uses Black
            
            self.edgesForExtendedLayout = UIRectEdgeNone;//UIRectEdgeNone
            //fullInfoView.backgroundColor = [UIColor whiteColor];
            
            //Bottom Bar
            bottomBar.hidden = NO;
            bottomBar.translucent = NO;
            [self.view bringSubviewToFront:bottomBar];
            

            //done Button
            doneButton.hidden = YES;
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(SaveMyPic)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;

            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.view.backgroundColor = [UIColor whiteColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                
                bottomBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:
                                                                                                    [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.view.backgroundColor = [UIColor blackColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                bottomBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                bottomBar.tintColor = [UIColor whiteColor];
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            doneButton_iOS7.tintColor = [UIColor whiteColor];
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
            
            if (IS_IPAD)
            {
                 bottomBar.frame = CGRectMake((self.view.frame.size.width-320)/2, 0, 320, 44);
            }else
            {
                if (IS_IPHONE_5) {
                    
                    bottomBar.frame = CGRectMake(0, 372 + 88, 320, 44);
                }
                else{
                    
                    bottomBar.frame = CGRectMake(0, 372, 320, 44);
                }

            }
            
            
            
            
        }
        else{ //iOS 6
            
            [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            [bottomBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
            
            if (IS_IPHONE_5) {
                
                self.view.frame = CGRectMake(0, 20, 320, 460 + 88);
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 460 + 88);
                bottomBar.frame = CGRectMake(0, 416 + 88, 320, 44);
            }
            else{
                
                self.view.frame = CGRectMake(0, 20, 320, 460);
                self.navigationController.view.frame = CGRectMake(0, 0, 320, 460);
                bottomBar.frame = CGRectMake(0, 416, 320, 44);
            }
        }

        /*
        NSLog(@"self.view.frame =  %@", NSStringFromCGRect(self.view.frame));
        NSLog(@"bottom.frame =  %@", NSStringFromCGRect(bottomBar.frame));
        NSLog(@" self.navigationController.view.frame #1 = %@ ", NSStringFromCGRect(self.navigationController.view.frame));
        NSLog(@" ****UIScreen.mainScreen.applicationFrame #1 = %@ ", NSStringFromCGRect(UIScreen.mainScreen.applicationFrame));
        */
        
        self.title =@"Note Picture";
        
    }
    else{ // Old Navigation
        
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
       // [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]]; //Steve added
        
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

    }
    
    
    // ***************** Steve End ***************************
    

    
}

-(IBAction)getCameraPicture:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable:
		UIImagePickerControllerSourceTypeCamera])
	{
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        
        //Steve commented out
        //picker.allowsEditing = YES;
        
        //picker.sourceType = (sender == takePictureButton) ? UIImagePickerControllerSourceTypeCamera :
        //UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        //[self presentModalViewController: picker animated:YES];
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *Alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your device does not support the camera feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        //Steve commented out
        // [Alert release];
        
    }
}

-(IBAction)selectExitingPicture
{
    
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault]; //Changes to Black so can be seen on Apple's white Nav
    
	if([UIImagePickerController isSourceTypeAvailable:
		UIImagePickerControllerSourceTypePhotoLibrary])
	{
		UIImagePickerController *picker= [[UIImagePickerController alloc]init];
		picker.delegate = self;
		picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        [self presentViewController:picker animated:YES completion:nil];
	}
	
}


//Steve - fixes issue with Taking Pictures crashing after 4 pics taken
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    image = [image imageByScalingProportionallyToSize:CGSizeMake(960,1176)];  //resizes images to save on memory. Steve was: (320,370)
    [imageView setImage:image];
    
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
	//[picker dismissModalViewControllerAnimated:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)homeButton_Clicked:(id)sender 
{
	[self.navigationController popViewControllerAnimated:YES];
}



-(IBAction)SaveMyPic
{
	
    if(imageView.image != nil)
    {
        myImage=UIImageJPEGRepresentation(imageView.image, 0.25) ; //Steve changed (imageView.image, 1.0)
        [[self.delegate tempImgArray] addObject:myImage];	
    }
	[self.navigationController popViewControllerAnimated:YES];
	
	
}

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [doneButton removeFromSuperview];
        [backButton removeFromSuperview];
        
    }
    
}

- (void)viewDidUnload {
    NSLog(@"viewDidUnload");
    
    navBar = nil;
    bottomBar = nil;
    backButton = nil;
    doneButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end





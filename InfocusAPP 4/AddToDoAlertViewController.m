//
//  AddToDoAlertViewController.m
//  Organizer
//
//  Created by Aman Gupta on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddToDoAlertViewController.h"
#import "ToDoViewController.h"


@interface AddToDoAlertViewController()

@property(nonatomic, strong) NSDate *alert_Date;//Steve
@property(nonatomic, strong) NSDate *alertSecond_Date;//Steve

@end



@implementation AddToDoAlertViewController

@synthesize lblAlertDate;
@synthesize lblSecondAlert;
@synthesize switchAlert;
@synthesize switchCalander;
@synthesize datePicker;
@synthesize delegate;
@synthesize alertString; //Anil's Added
@synthesize btn_Alert;
@synthesize btn_SecondAlert;
@synthesize alertStringSecond;


//- (id)initWithAlertString:(NSString *) alert { //Steve commented
//- (id)initWithAlertString:(NSDate *) alert {
- (id)initWithAlertDate:(NSDate *) alert alertSecondDate:(NSDate *) alertSecond{

    
    self = [super initWithNibName:@"AddToDoAlertViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        
		//self.alertString = [alert isEqualToString:@"None"] ? @"Select Alert Type":alert;
        //Anil's added
        
        _alert_Date = alert;
        _alertSecond_Date = alertSecond;
        
        [NSTimeZone resetSystemTimeZone];
        [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    //NSLog(@" self.alertString = %@", self.alertString);
    //NSLog(@" self.alertStringSecond = %@", self.alertStringSecond);
    
    //NSLog(@" _alert_Date = %@", [_alert_Date descriptionWithLocale:@"GMT"]);
    //NSLog(@" _alertSecond_Date) = %@",[ _alertSecond_Date descriptionWithLocale:@"GMT"]);
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    
    [datePicker setDate:_alert_Date animated:YES];
    
    
    if (  switchAlert.on && ( [alertString isEqualToString:@""] || !alertString ||[alertString isEqualToString:@"None"]) ) {
        
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        lblAlertDate.text = [formatter stringFromDate:_alert_Date];
        
	}
    else if ([alertString isEqualToString:@""] || !alertString ||[alertString isEqualToString:@"None"] ){
        
        lblAlertDate.text = @"None";
    }
    else {
        
		lblAlertDate.text = alertString;
	}
    
    
    if ([alertStringSecond isEqualToString:@""] || !alertStringSecond ||[alertStringSecond isEqualToString:@"None"]) {
        
		lblSecondAlert.text = @"None";
        [lblSecondAlert setTextColor:[UIColor grayColor]];
	}
    else {
        
		lblSecondAlert.text = alertStringSecond;
        lblSecondAlert.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
        
	}
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
       // backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeNone;
            self.view.backgroundColor = [UIColor whiteColor];
            //self.view.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0  blue:235.0/255.0  alpha:1.0];
            //datePicker.tintColor = [UIColor lightGrayColor];
            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            
            //done Button
            doneButton.hidden = YES;
            
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(doneClicked:)];
            
            doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
            
            
            switchAlert.frame = CGRectMake(221 + 20, 51 - 44, 79, 27);
            
            
        }
        else{ //ios 6
            
            self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        
            [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            switchAlert.frame = CGRectMake(221, 51 - 44, 79, 27);
        }

        
        alertFixedLabel.frame = CGRectMake(15, 54 - 44, 102, 21);
        btn_Alert.frame = CGRectMake(9, 87 - 44, 301, 49);
        alertButtonFixedLabel.frame = CGRectMake(17, 98 - 44, 48, 26);
        lblAlertDate.frame = CGRectMake(122, 98 - 44, 178, 26);
        btn_SecondAlert.frame = CGRectMake(9, 138 - 44, 301, 49);
        secondAlertButtonFixedlabel.frame = CGRectMake(17, 147 - 44, 130, 26);
        lblSecondAlert.frame = CGRectMake(122, 147 - 44, 178, 26);
        datePicker.frame = CGRectMake(0, 244 - 44, 320, 216);
        
        
        //NSLog(@"shareButton =  %@", NSStringFromCGRect( shareButton.frame));
        
        self.title =@"Alert";
        

 
        
    }
    else{
        
        self.navigationController.navigationBarHidden = YES;
        self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************



}

#pragma mark - View lifecycle

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefualts = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefualts boolForKey:@"NavigationNew"]) {
        [backButton removeFromSuperview];
        [doneButton removeFromSuperview];
    }
}

- (void)viewDidUnload
{
    [self setLblAlertDate:nil];
    [self setLblSecondAlert:nil];
    [self setSwitchAlert:nil];
    [self setSwitchCalander:nil];
    [self setDatePicker:nil];
    [self setBtn_Alert:nil];
    [self setBtn_SecondAlert:nil];
    navBar = nil;
    backButton = nil;
    doneButton = nil;
    alertFixedLabel = nil;
    alertButtonFixedLabel = nil;
    secondAlertButtonFixedlabel = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)alertSwitch:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        
          alertFixedLabel.frame = CGRectMake(15, 54 - 44, 102, 21);
    else
          alertFixedLabel.frame = CGRectMake(15, 54, 102, 21);
    

    if([switchAlert isOn] == YES){
        
        selectedButton =0;
        btn_Alert.enabled = TRUE;
        btn_SecondAlert.enabled = TRUE;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
        
       // NSDate *startDate = [formatter dateFromString:alertString];//Steve commented
        //[datePicker setDate:startDate animated:YES]; //Steve commented
        [datePicker setDate:_alert_Date animated:YES];//Steve
        
        
        if ([alertString isEqualToString:@""] || !alertString ||[alertString isEqualToString:@"None"]) {
            
            lblAlertDate.text = @"None";
        }else {
            
            lblAlertDate.text = alertString;
            //[lblAlertDate setTextColor:[UIColor blueColor]]; //Steve commented
            lblAlertDate.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve

        }

    }    
    else
    {
        selectedButton=2;
        [lblAlertDate setTextColor:[UIColor grayColor]];
        [lblSecondAlert setTextColor:[UIColor grayColor]];
        lblAlertDate.text = @"None";
        lblSecondAlert.text = @"None";
        btn_Alert.enabled = FALSE;
        btn_SecondAlert.enabled = FALSE;
        printf("The switch is off\n");
    }
}
- (IBAction)firstAndSecondAlertClicked:(id)sender {
    //Tag =0 for FirstAlert and 1 for Second Alert
    
    if([sender tag] == 0)
       {
        selectedButton =[sender tag];
       
        datePicker.date = _alert_Date; //Steve
       }
    
    else
    {
        selectedButton =[sender tag];
        
        if ([lblSecondAlert.text isEqualToString:@"None"] ) { //Steve
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            [formatter setLocale:[NSLocale currentLocale]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            lblSecondAlert.text = [formatter stringFromDate:datePicker.date];
            lblSecondAlert.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            alertStringSecond = lblSecondAlert.text;
        }
        else{
            
             datePicker.date = _alertSecond_Date; //Steve
        }
       
    
    }
}

- (IBAction)showCalanderSwitch:(id)sender {
    
}

- (IBAction)dateChanged:(id)sender {
    
    switch (selectedButton) {
        case 0:
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            [formatter setLocale:[NSLocale currentLocale]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            if (alertString) {
                alertString = nil;
            }
            
            _alert_Date = datePicker.date;//Steve
            
            alertString = [formatter stringFromDate:[datePicker date]];
            
            lblAlertDate.text = alertString;
            lblAlertDate.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
        }
            
            break;
        case 1:
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            [formatter setLocale:[NSLocale currentLocale]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            
            if (alertStringSecond) {
                alertStringSecond = nil;
            }
            
            _alertSecond_Date = datePicker.date;
            
            //NSLog(@"_alertSecond_Date = %@", [_alertSecond_Date descriptionWithLocale:@"GMT"]);
            
            alertStringSecond = [formatter stringFromDate:_alertSecond_Date];
            
            lblSecondAlert.text = alertStringSecond;
            lblSecondAlert.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
        }
            break;    
        default:
            break;
    }
    
}



- (IBAction)doneClicked:(id)sender {
    
      if ([self.alertDelegate respondsToSelector:@selector(setAlertString_1:setAlertString_2:isAlertOn:)]) {
        
        if([switchAlert isOn]==YES)
        {
            
            [self.alertDelegate setAlertString_1:lblAlertDate.text
                                setAlertString_2:lblSecondAlert.text
                                       isAlertOn:YES];
        }
        else
        {
            
            [self.alertDelegate setAlertString_1:lblAlertDate.text
                                setAlertString_2:lblSecondAlert.text
                                       isAlertOn:NO];
        }
        
    }


    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)backButton_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}

@end

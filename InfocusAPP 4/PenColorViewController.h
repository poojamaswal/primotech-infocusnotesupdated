//
//  penColorViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 12/24/14.
//
//

#import <UIKit/UIKit.h>
#import "DrawView_iPad.h"


@protocol PenNoteColorPickerDelegate <NSObject>

-(void)SetLineWidth:(float) lineWidth;  //Stroke width
-(void)setBrushColorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue; //Stroke Color
-(void)setAlphaColor:(CGFloat) alphaColorSelection; //Stroke Alpha

@end




@interface PenColorViewController : UIViewController

@property (nonatomic, weak) id<PenNoteColorPickerDelegate> delegate;
@property (nonatomic, assign) BOOL isHighlighter;

@end

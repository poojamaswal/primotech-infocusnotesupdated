//
//  LevelDataObject.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LevelDataObject : NSObject 
{
	
	NSInteger levelID;
	NSInteger totalLavel;
	NSString *levelName;

}
@property(nonatomic,strong)	NSString *levelName;
@property(nonatomic,assign)NSInteger levelID;
@property(nonatomic,assign)NSInteger totalLavel;


@end

//
//  ToolTableViewController.m
//  InfocusNotesApp
//
//  Created by jaswinder blagun on 8/08/2015.
//  Copyright (c) 2015 Primotech Inc. All rights reserved.
//

#import "ToolTableViewController.h"
#import "ToolSelectionObject.h"

@interface ToolTableViewController ()

@end

@implementation ToolTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [[ToolSelectionObject sharedObject] insertSelectedSticky];
    }
}


@end

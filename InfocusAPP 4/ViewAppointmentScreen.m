//  ViewAppointmentScreen.m
//  Organizer
//  Created by Nilesh Jaiswal on 7/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.


#import "ViewAppointmentScreen.h"
#import "GetAllDataObjectsClass.h"
#import "AppointmentViewController .h"

@implementation ViewAppointmentScreen
@synthesize appDataObj;
//locDataObj;

#pragma mark -
#pragma mark ViewController LifeCycle
#pragma mark -

-(id)initWithAppObject:(AppointmentsDataObject *) appObj{ //Used for iPhone only
    
    if (self = [super initWithNibName:@"ViewAppointmentScreen" bundle:[NSBundle mainBundle]])
    {
        self.appDataObj = appObj;
    }
    
    //NSLog(@"calObj.calendarIdentifire =  %@",calObj.calendarIdentifire);
    //NSLog(@"appDataObj.alert =  %@",appDataObj.alert);
    
	return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    //NSLog(@"appDataObj %@",self.appDataObj);
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    
    backButton.hidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    //update Button
    editButton.hidden = YES;
    
  
    UIBarButtonItem *editButton_iOS7;
    
    
    if (!IS_IPAD) { //iPhone
        
        editButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButton_Clicked:)];
        self.navigationItem.rightBarButtonItem = editButton_iOS7;
        self.title =@"Details";
    }

    
    
    ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
        
        
        
        if (IS_IPAD) { //iPhone
            
            
        }
        else{ //iPhone

            editButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
            
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.translucent = NO;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
        }
        
        
        self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        appDetailTableView.backgroundColor = [UIColor whiteColor];
        
        
        
    }
    else{ //Dark Theme
        
        if(IS_IPAD)
        {
            // Update  (Pooja Changes)
            self.navigationController.navigationBarHidden=NO;
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
            
            _navBar_iPad.hidden=YES;
            
            
            self.cancelButton_iPad.tintColor=[UIColor whiteColor];
            self.navigationItem.title=@"Event Details";
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
            
            _editButton_iPad.tintColor = [UIColor whiteColor];
        
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:0.7];
            self.navigationController.navigationBar.translucent = NO;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            editButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        }
        
        
        self.view.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        appDetailTableView.backgroundColor = [UIColor whiteColor];
    }
    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    if(IS_IPAD)
    {
        //Steve commented
        self.view.frame = CGRectMake(0, 0, 768, 1024);
        shareButton.frame = CGRectMake(768/2-250/2, 1024 - 200, 250, 50);
        //appDetailTableView.frame = CGRectMake(0, 0, 768, 1024 - 44);
        self.view.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        if (IS_IPHONE_5) {
            self.view.frame = CGRectMake(0, 0, 320, 568);
            shareButton.frame = CGRectMake(105, 416 + 88 - 44, 111, 37);
            appDetailTableView.frame = CGRectMake(0, 0, 320, 430 + 88);
            
        }
        else {
            self.view.frame = CGRectMake(0, 0, 320, 480);
            shareButton.frame = CGRectMake(105, 416 - 44, 111, 37);
            appDetailTableView.frame = CGRectMake(0,  0, 320, 430);
            
        }
    }
    


    
    [self checkIfCalendarIsModifiable]; //Steve added

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Steve added
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];

    
    //Steve added
    calDataDictionary= [[NSMutableDictionary alloc]init];
    calObj = [[CalendarDataObject alloc]init];
    
    
    OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];

    if([appDataObj.appointmentType isEqualToString:@"H"] && !appDataObj.appointmentBinaryLarge)
    {
        sqlite3_stmt *selectAppointment_Stmt = nil;
        
        if(selectAppointment_Stmt == nil && appDelegate.database) {
            
            //NSLog(@"event.eventIdentifier = %@",event.eventIdentifier);
            
            NSString *Sql = [[NSString alloc] initWithFormat: @"Select a.AppointmentBinaryLarge  From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", appDataObj.relation_Field1];
          
            //NSLog(@"Day query : %@", Sql);
            
            if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
            {
                NSLog(@"SQL FAILED");
                NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
                
            }
            else
            {
                //NSLog(@"SQL OK");
                
                while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
                {
                    
                   NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 0);
                     NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 0) length:blobLength];
                     [appDataObj setAppointmentBinaryLarge:binaryDataLarge];

                    
                }//while end
                
            }//else end
            sqlite3_finalize(selectAppointment_Stmt);
            selectAppointment_Stmt = nil;
            
 
        }
        else
        {
            NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
        
        
    }
    

}

#pragma mark -
#pragma mark MailComposer  Delegates
#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
	//[self dismissModalViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark -
#pragma mark Button Clicked
#pragma mark -

-(IBAction)backButton_Clicked:(id) sender {
    
    if (IS_IPAD) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else{ //iPhone
      	[self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)editButton_Clicked:(id) sender {
   
    //Steve added - checks to see if event can be modified first. If not, an Alert shows
    if (!isCalendarAllowedToBeModified) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Can Not Modify This Event"
                                                          message:@"This Event can not be modified. To modify this event, you must go to the source  where this event was created. "
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }
    
    
    //Steve
    if (IS_IPAD) {

        [self performSegueWithIdentifier: @"AppointmentViewController" sender: self];
    }
    else{ //iPhone
        
        AppointmentViewController* appContrl = [[AppointmentViewController alloc]initWithDataObj:appDataObj];
        [self.navigationController pushViewController:appContrl animated:YES];
        appContrl.isAdd = NO;
    }

}


//Steve - iPad Storyboard
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"AppointmentViewController"])
    {
        AppointmentViewController* appContrl = (AppointmentViewController*)segue.destinationViewController;
        appContrl.aAppObj = appDataObj;
        appContrl.isAdd = NO;
        appContrl.title = @"Edit Event";
    }
    
}


-(IBAction)shareButton_Clicked:(id) sender {
    

    
    if ([MFMailComposeViewController canSendMail]) 
	{
    
        if (IS_IOS_7) {
            
            
            if(IS_IPAD)
            {
                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                UINavigationBar.appearance.barTintColor = [UIColor darkGrayColor];
                //UINavigationBar.appearance.tintColor = [UIColor whiteColor]; //Steve commented.  Does not work. Apple bug?
                [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]]; //Steve. 
                
            }
            else
            {
                UINavigationBar.appearance.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
                UINavigationBar.appearance.tintColor = [UIColor colorWithRed:60.0/255.0 green:175/255.0 blue:255/255.0 alpha:1.0];
            }
        }
        else{
            
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
        }

        
        NSString *dateString;
        
        MFMailComposeViewController *mailCntrlr = [[MFMailComposeViewController alloc] init];
        [mailCntrlr setMailComposeDelegate:self];
        
        NSDateFormatter *formate = [[NSDateFormatter alloc] init];
        [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSDate *strtDate = [formate dateFromString:[appDataObj startDateTime]];
        NSDate *endDate = [formate dateFromString:[appDataObj dueDateTime]];
        
        [formate setDateFormat:@"yyyy-MM-dd"];
        
        if ([[formate dateFromString:[[strtDate description] substringToIndex:10]] compare:  [formate dateFromString:[[endDate description] substringToIndex:10]]] == NSOrderedSame) {
            
            [formate setDateStyle:NSDateFormatterFullStyle];
            dateString = [formate stringFromDate:strtDate];
            
            //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
            [formate setLocale:[NSLocale currentLocale]];//Steve
            [formate setDateStyle:NSDateFormatterNoStyle];//Steve
            [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
            
            dateString = [dateString stringByAppendingFormat:@" %@ to %@", [formate stringFromDate:strtDate], [formate stringFromDate:endDate]];
            
        }else {
            
            NSString *strtDateLocalStr;
            NSString *endDateLocalStr;
            
            [formate setDateStyle:NSDateFormatterFullStyle];
            strtDateLocalStr = [formate stringFromDate:strtDate];
            endDateLocalStr = [@"To " stringByAppendingString:[formate stringFromDate:endDate]];
            
            //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
            [formate setLocale:[NSLocale currentLocale]];//Steve
            [formate setDateStyle:NSDateFormatterNoStyle];//Steve
            [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
            
            dateString = [[strtDateLocalStr stringByAppendingFormat:@" %@", [formate stringFromDate:strtDate]] stringByAppendingFormat:@", %@", [endDateLocalStr stringByAppendingFormat:@" %@", [formate stringFromDate:endDate]]];
        }
        
        NSString *body;
        
        
        if ([[appDataObj appointmentType] isEqualToString:@"H"]  && appDataObj.appointmentBinary != nil) {
            [mailCntrlr setSubject:@"Shared Event"];
            [mailCntrlr addAttachmentData:[appDataObj appointmentBinaryLarge] mimeType:@"image/jpeg" fileName:@"Shared Event"];
            body = dateString;
        }else {
            [mailCntrlr setSubject:[appDataObj appointmentTitle]];
            body = [[appDataObj appointmentTitle] stringByAppendingFormat:@", %@", dateString];
        }
        
        if (appDataObj.shortNotes != Nil) {
            //body = [body stringByAppendingFormat:@"\n\n %@", [appDataObj shortNotes]];
            body = [body stringByAppendingFormat:@"<BR><BR> <b> Calendar Notes:</b> <BR> %@", [appDataObj shortNotes]]; //Steve changed
        }
        
        
        //Steve - Infocus Pro Link
        NSString *ItuneURL = @"http://itunes.apple.com/us/app/infocus-pro-organizer-to-dos/id545904979?ls=1&mt=8";
        NSString *bodyURL = [NSString stringWithFormat:@"<BR><BR><BR><p style=font-size:12px>Get the InFocus Pro App Now!<BR><a href=\"%@\">InFocus Pro - Click Here</a>",ItuneURL];
        body = [body stringByAppendingString:bodyURL];
        
        //<p style="font-size:20px">
        
        NSString *_new = [NSString stringWithFormat:@"<table bgcolor=white><tr><td>%@</td></tr></table>",body];
        
        
        [mailCntrlr setMessageBody:_new isHTML:YES];
        
         [self presentViewController:mailCntrlr animated:YES completion:nil];
        
        
        
    }
    else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Status:" message:@"Your phone is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
	}
}

#pragma mark -
#pragma mark Tabel View DataSource & Delegate Methods
#pragma mark -


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 1;
    return 1.0; //1
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	NSInteger section = 0;
    
	if ([indexPath row] < 3 ||  numberOfRows == 6) { //Steve changed was: numberOfRows == 5
		section = [indexPath row];
	}else {
		if (numberOfRows == 4) {
			if (![appDataObj.alert isEqualToString:@""] && ![appDataObj.alert isEqualToString:@"Never"] && appDataObj.alert != nil) {
				section = 3;
			}else if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"] || appDataObj.repeat == nil || [appDataObj.repeat isEqualToString:@"None"])) {
				section = 4;
			}else if (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""]) {
				section = 5;
			}
		}else if (numberOfRows == 5) {
			if (!([appDataObj.alert isEqualToString:@""] || [appDataObj.alert isEqualToString:@"Never"]) && indexPath.row == 3) {
				section = 3;
			}else if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"]) 
                && (appDataObj.shortNotes == nil || [appDataObj.shortNotes isEqualToString:@""]) ){
                    
               // NSLog(@"NOT REPEAT");
				section = 4;
            }else if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"]) 
                      && !(appDataObj.shortNotes == nil || [appDataObj.shortNotes isEqualToString:@""]) ){
                
               // NSLog(@"NOT REPEAT BUT NOTE");
				section = 5;
			}else if ( !(appDataObj.shortNotes == nil || [appDataObj.shortNotes isEqualToString:@""]) ) { //Steve removed: && indexPath.row == ([tableView numberOfRowsInSection:0] -1)
				section = 5;
                // NSLog(@"NOT NOTE");
			}
		}else if (numberOfRows == 6){
            if (!([appDataObj.alert isEqualToString:@""]&&[appDataObj.alert isEqualToString:@"Never"]) && indexPath.row == 3) {
				section = 3;
			}else if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"])) {
				section = 4;
			}else if (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""] && indexPath.row == ([tableView numberOfRowsInSection:0] -1)) {
				section = 5;
			}
        }
	}
    
    
	switch (section) {
		case 0: {
			if ([appDataObj.appointmentType isEqualToString:@"H"] && appDataObj.appointmentBinary != nil)
			{
				//UIImage *titleImage = [[UIImage alloc] initWithData:appDataObj.appointmentBinaryLarge];
				
//				NSLog(@"linecount %d", [appDataObj.appointmentBinary length] );
//
//				if([appDataObj.appointmentBinary length] ==1)
//				{
//					return 50;
//				}
//				else if ([objHand.lineCount.text intValue] == 2)
//					return 100;
				return 150;
			}
			else {
				CGSize strSize = [appDataObj.appointmentTitle sizeWithFont:[UIFont boldSystemFontOfSize:16] 
														 constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
				return strSize.height + 40;
			}
		}
			break;
		case 1: {
			if (![appDataObj.textLocName isEqualToString:@""]) 
			{
				return 130 + 30; //Steve added 22 for Time Zone
			}
			else 
			{
				return 100 + 18; //Steve added 22 for time zone
			}
		}
			break;
		case 2: {
			return 70; 
		}
			break;
		case 3: {
           
            ///Anil's added///////
            if (!([appDataObj.alert isEqualToString:@""]||[appDataObj.alert isEqualToString:@"Never"]))
            {
                if (!([appDataObj.alertSecond isEqualToString:@""]||[appDataObj.alertSecond isEqualToString:@"Never"]))
                return 100;
                else
                {
                
                    return 70;
                }
            }
            else
            {
			return 70; 
            }
		}
			break;
		case 4: {
             if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"]))
			return 70; 
            else
                return 70;
		}
			break;
		case 5: {
            CGSize strSize;
            if(IS_IPAD)
                strSize = [appDataObj.shortNotes sizeWithFont:[UIFont boldSystemFontOfSize:19] constrainedToSize:CGSizeMake(720, 9999) lineBreakMode:NSLineBreakByWordWrapping];
            else
                strSize = [appDataObj.shortNotes sizeWithFont:[UIFont boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
			return strSize.height + 30 + 35;
            
            //NSLog(@"strSize =  %@", NSStringFromCGSize(strSize));
            
		}
			break;
		default:
			return 70; 
			break;
	}
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	//NSLog(@"numberOfRowsInSection");
    
    int rows = 6; //Anil's Added - 5// Steve changed to 6


    if (([appDataObj.alert isEqualToString:@""] || [appDataObj.alert isEqualToString:@"Never"] || appDataObj.alert == NULL || [appDataObj.alert length]<=1) && ([appDataObj.alertSecond isEqualToString:@""] || [appDataObj.alertSecond isEqualToString:@"Never"] || appDataObj.alertSecond == NULL || [appDataObj.alertSecond length]<=1) ){
        rows -= 1;
        //NSLog(@"NO ALERT");
	}
    
	if (appDataObj.repeat == nil || [appDataObj.repeat isEqualToString:@""] || [appDataObj.repeat isEqualToString:@"None"]) {
		rows -= 1;
        //NSLog(@"NO REPEAT");
	}
	if (appDataObj.shortNotes == nil || [appDataObj.shortNotes isEqualToString:@""] || [appDataObj.shortNotes isEqualToString:@"None"]) {
		rows -= 1;
        //NSLog(@"NO NOTES");
	}
    
	numberOfRows = rows;
    
   // NSLog(@"numberOfRows = %d", numberOfRows);
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
    
	if (cell == nil){
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	
	for (UIView *subView in [cell.contentView subviews]) {
		[subView removeFromSuperview];
	}
	
	NSInteger section = 0;
	
	if ([indexPath row] < 3 || numberOfRows < 4) {
		section = [indexPath row];
	}else {
		if (numberOfRows == 4) {
			if (![appDataObj.alert isEqualToString:@""] && ![appDataObj.alert isEqualToString:@"Never"] && [appDataObj.alert length]>=1) {
				section = 3;
			}else if (appDataObj.repeat != nil && ![appDataObj.repeat isEqualToString:@""] && ![appDataObj.repeat isEqual:@"None"]) {
				section = 4;
			}else if (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""]) {
				section = 5;
			}
		}else if (numberOfRows == 5) {
			if ( (![appDataObj.alert isEqualToString:@""] && ![appDataObj.alert isEqualToString:@"Never"] && appDataObj.alert != nil) && indexPath.row == 3) {
				section = 3;
			}else if (!(appDataObj.repeat == nil || [appDataObj.repeat isEqualToString:@""]) && (appDataObj.shortNotes == nil || [appDataObj.shortNotes isEqualToString:@""]) ) { //Steve [changed was:  && indexPath.row != 4
                section = 4;
            }else if (appDataObj.repeat != nil && ![appDataObj.repeat isEqualToString:@"" ] && (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""]) ) { //Steve
        
                //Steve - added to check if scrolling back up.  With a long Calendar Note, No Alert & Repeat, this fixes a bug for Notes duplicationg (instead of Repeat showing)
                // this is caused by scrolling far down (when Note is long) and scrolling back up calls cellForRowAtIndexPath method again.
                if (previousIndexPathRow > indexPath.row) { 
                    isRepeatFilled = NO;
                }
                if ( ([appDataObj.alert isEqualToString:@""] || [appDataObj.alert isEqualToString:@"Never"] || appDataObj.alert == Nil) && isRepeatFilled == NO ) { 
                    section = 4;
                    isRepeatFilled = YES;
                }
                else
                    section = 5;
               
			}else if (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""] && indexPath.row == ([tableView numberOfRowsInSection:0] -1)) {
				section = 5;
			}
		}else if (numberOfRows == 6){ //Steve added
            if (!([appDataObj.alert isEqualToString:@""]&&[appDataObj.alert isEqualToString:@"Never"]) && indexPath.row == 3) {
				section = 3;
			}else if (appDataObj.repeat != nil && ![appDataObj.repeat isEqualToString:@""] && indexPath.row == 4) { //Steve [changed was: indexPath.row != 4
				section = 4;
			}else if (appDataObj.shortNotes != nil && ![appDataObj.shortNotes isEqualToString:@""] && indexPath.row == ([tableView numberOfRowsInSection:0] -1)) {
				section = 5;
			}
        }
	}
    
    
	switch (section) {
		case 0: {
			if ([appDataObj.appointmentType isEqualToString:@"H"]  && appDataObj.appointmentBinary != nil) {
				UIImage *titleImage = [[UIImage alloc ] initWithData:appDataObj.appointmentBinaryLarge];
                UIButton *titleImageView;
                
                if(IS_IPAD)
                    titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 600, 130)];
                else
                    titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, 290, 130)];
                
				[titleImageView setImage:titleImage forState:UIControlStateNormal];
				[titleImageView setImage:titleImage forState:UIControlStateHighlighted];
				[cell.contentView addSubview:titleImageView];
			}else {
                CGSize strSize;
                if(IS_IPAD)
                    strSize = [appDataObj.appointmentTitle sizeWithFont:[UIFont boldSystemFontOfSize:19]
														 constrainedToSize:CGSizeMake(700, 9999) lineBreakMode:NSLineBreakByWordWrapping];
                else
                    strSize = [appDataObj.appointmentTitle sizeWithFont:[UIFont boldSystemFontOfSize:16]
                                                      constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
                UILabel *titleLabel;
                if(IS_IPAD)
                {
                    titleLabel= [[UILabel alloc ] initWithFrame:CGRectMake(15, 15, 700, strSize.height + 5)];
                    [titleLabel setFont:[UIFont systemFontOfSize:19]];
                }
                else
                {
                    titleLabel= [[UILabel alloc ] initWithFrame:CGRectMake(5, 15, 290, strSize.height + 5)];
                    [titleLabel setFont:[UIFont systemFontOfSize:16]];
                }
				[titleLabel setText:appDataObj.appointmentTitle];
				
                titleLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
				[titleLabel setTextAlignment:NSTextAlignmentLeft];
				[titleLabel setBackgroundColor:[UIColor clearColor]];
				[titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
				[titleLabel setNumberOfLines:0];
				[cell.contentView addSubview:titleLabel];
			}
		}
			break;
		case 1:{
            UILabel *locCaptionLabel;
            if(IS_IPAD)
            {
                locCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
                [locCaptionLabel setFont:[UIFont boldSystemFontOfSize:19]];
            }
            else
            {
                 locCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(15, 7, 700, 25)];
                [locCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
            }
			
			[locCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[locCaptionLabel setText:@"Location & Timings"];
			[cell.contentView  addSubview:locCaptionLabel];
			[locCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[locCaptionLabel  setTextColor:[UIColor blackColor]];
			
            
			UILabel *strtDateLabel = [[UILabel alloc ] init];
            if(IS_IPAD)
                [strtDateLabel setFont:[UIFont systemFontOfSize:19]];
            else
                [strtDateLabel setFont:[UIFont systemFontOfSize:16]];
			[strtDateLabel setTextAlignment:NSTextAlignmentLeft];
			[strtDateLabel setBackgroundColor:[UIColor clearColor]];
			//[strtDateLabel setTextColor:[UIColor blueColor]];
            strtDateLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:strtDateLabel];
			
			if (![appDataObj.textLocName isEqualToString:@""]) {
				
                // Steve commented - testing UIText so I can add map Link
                //Steve DELETE later
                /*
                UILabel *locationLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, locCaptionLabel.frame.origin.y + locCaptionLabel.frame.size.height + 3, 290, 25)];
				[locationLabel setFont:[UIFont systemFontOfSize:16]];
				[locationLabel setTextAlignment:UITextAlignmentLeft];
//				[locationLabel setText:[locDataObj.locationAddress stringByAppendingFormat:@"%@ %@", locDataObj.locationCity, locDataObj.locationState]];
				[locationLabel setText:appDataObj.textLocName];
				[locationLabel  setBackgroundColor:[UIColor yellowColor]];
				[locationLabel  setTextColor:[UIColor grayColor]];
                [locationLabel  setTextColor:[UIColor redColor]];
				[cell.contentView  addSubview:locationLabel];
				[strtDateLabel setFrame:CGRectMake(5, locationLabel.frame.origin.y + locationLabel.frame.size.height +3, 290, 25)];
                */
        

                
                //Steve commented.  Not needed
                //CGSize strSize = [appDataObj.textLocName sizeWithFont:[UIFont systemFontOfSize:16]
                 //                                  constrainedToSize:CGSizeMake(290, 22) lineBreakMode:NSLineBreakByWordWrapping];
                
                // NSLog(@"strSize = %@", NSStringFromCGSize(strSize) );
                
                
                //Steve changed from Label to UITextView so weblinks are detected
                //-3 used since doesn't line up with labels
                UITextView *locationLabel;
                if(IS_IPAD)
                {
                    locationLabel = [[UITextView alloc ] initWithFrame:CGRectMake(-3, locCaptionLabel.frame.origin.y + locCaptionLabel.frame.size.height - 5, 750, 55)];
                    [locationLabel setFont:[UIFont systemFontOfSize:19]];
                }
                    else
                {
                    locationLabel = [[UITextView alloc ] initWithFrame:CGRectMake(-3, locCaptionLabel.frame.origin.y + locCaptionLabel.frame.size.height - 5, 315, 55)];
                    [locationLabel setFont:[UIFont systemFontOfSize:16]]; //16
                }
				
				//[locationLabel setTextAlignment:UITextAlignmentLeft]; //Deprecated
                [locationLabel setContentMode:UIViewContentModeLeft];
                [locationLabel setContentMode:UIViewContentModeTop];
                [locationLabel setClipsToBounds:YES];
                [locationLabel setScrollEnabled:NO];
                //[locationLabel setDelegate:self];
                [locationLabel setBounces:NO];
				[locationLabel setText:appDataObj.textLocName];
				[locationLabel  setBackgroundColor:[UIColor clearColor]]; //Clear
				[locationLabel  setTextColor:[UIColor grayColor]];
                
                locationLabel.editable = NO;
                locationLabel.dataDetectorTypes = UIDataDetectorTypeAddress;
                
				[cell.contentView  addSubview:locationLabel];
                
                //Steve
                if(IS_IPAD)
                    [strtDateLabel setFrame:CGRectMake(5, locationLabel.frame.origin.y + locationLabel.frame.size.height + 5, 750, 22)];
                else
                    
                    [strtDateLabel setFrame:CGRectMake(5, locationLabel.frame.origin.y + locationLabel.frame.size.height + 5, 315, 22)]; //25
                
                //Steve commented
                // Steve - adjust strtDateLabel if only one line of Location
                /*
                if (strSize.height < 40) { //40
                    [strtDateLabel setFrame:CGRectMake(5, locationLabel.frame.origin.y + locationLabel.frame.size.height + 15, 315, 22)]; //25
                }
                else{
                    [strtDateLabel setFrame:CGRectMake(5, locationLabel.frame.origin.y + locationLabel.frame.size.height + 5, 315, 22)]; //25
                }
                */
                
                
				

			}
			else
			{
                if(IS_IPAD)
                    [strtDateLabel setFrame:CGRectMake(5, locCaptionLabel.frame.origin.y + locCaptionLabel.frame.size.height + 5, 750, 22)];
                else
                    [strtDateLabel setFrame:CGRectMake(5, locCaptionLabel.frame.origin.y + locCaptionLabel.frame.size.height + 5, 315, 22)]; //25
			}
			
            UILabel *endDateLabel;
            if(IS_IPAD)
            {
                endDateLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, strtDateLabel.frame.origin.y + strtDateLabel.frame.size.height, 750, 22)];
                [endDateLabel setFont:[UIFont systemFontOfSize:19]];
            }
            else
            {
                endDateLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, strtDateLabel.frame.origin.y + strtDateLabel.frame.size.height, 315, 22)]; //25
                [endDateLabel setFont:[UIFont systemFontOfSize:16]];
            }
            
			[endDateLabel setTextAlignment:NSTextAlignmentLeft];
			[endDateLabel  setBackgroundColor:[UIColor clearColor]];
			//[endDateLabel  setTextColor:[UIColor blueColor]];
            endDateLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:endDateLabel];
			
			NSDateFormatter *formate = [[NSDateFormatter alloc] init];
			[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			
			NSDate *strtDate = [formate dateFromString:[appDataObj startDateTime]];
			NSDate *endDate = [formate dateFromString:[appDataObj dueDateTime]];
			
			[formate setDateFormat:@"yyyy-MM-dd"];
			
            
			if ([[formate dateFromString:[[strtDate description] substringToIndex:10]] compare:  [formate dateFromString:[[endDate description] substringToIndex:10]]] == NSOrderedSame) {
				
				[formate setDateStyle:NSDateFormatterFullStyle];
				[strtDateLabel setText:[formate stringFromDate:strtDate]];
				
                //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
                [formate setLocale:[NSLocale currentLocale]];//Steve
                [formate setDateStyle:NSDateFormatterNoStyle];//Steve
                [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
                
				[endDateLabel setText:[NSString stringWithFormat:@"%@ To %@", [formate stringFromDate:strtDate], [formate stringFromDate:endDate]]];
			}
            else{
				[formate setDateStyle:NSDateFormatterFullStyle];
				[strtDateLabel setText:[formate stringFromDate:strtDate]];
				[endDateLabel setText:[@"To " stringByAppendingString:[formate stringFromDate:endDate]]];
				
                //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
                [formate setLocale:[NSLocale currentLocale]];//Steve
                [formate setDateStyle:NSDateFormatterNoStyle];//Steve
                [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
                
				[strtDateLabel setText:[strtDateLabel.text stringByAppendingFormat:@" %@", [formate stringFromDate:strtDate]]];
				[endDateLabel setText:[endDateLabel.text stringByAppendingFormat:@" %@", [formate stringFromDate:endDate]]];
			}
            
            
            //Steve - TimeZone Label
            UILabel *timeZoneLabelFixed;
            if(IS_IPAD)
            {
                timeZoneLabelFixed = [[UILabel alloc]initWithFrame:CGRectMake(5, endDateLabel.frame.origin.y + 22, 150, 18)];
                [timeZoneLabelFixed setFont:[UIFont systemFontOfSize:16]];
            }
            else
            {
                 timeZoneLabelFixed = [[UILabel alloc]initWithFrame:CGRectMake(5, endDateLabel.frame.origin.y + 22, 120, 18)];
                [timeZoneLabelFixed setFont:[UIFont systemFontOfSize:14]];
            }
            timeZoneLabelFixed.text = @"(Event Time Zone:";
            
			[timeZoneLabelFixed setTextAlignment:NSTextAlignmentLeft];
			[timeZoneLabelFixed  setBackgroundColor:[UIColor clearColor]];
            timeZoneLabelFixed.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            [cell.contentView addSubview:timeZoneLabelFixed];

            //Steve - Time Zone
            NSTimeZone *eventTimeZone = [appDataObj timeZoneName];
            NSString *localAbbreviation;
            
            if (eventTimeZone)
                 localAbbreviation = [eventTimeZone abbreviation];
            
            else
                localAbbreviation = @"Fixed";
            
            
            //Steve - ")" to the string
            localAbbreviation = [localAbbreviation stringByAppendingString:@")"];
            
           
            //Steve - Time Zone Label
            UILabel *timeZoneLabel;
            if(IS_IPAD)
            {
                timeZoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(timeZoneLabelFixed.frame.origin.x + timeZoneLabelFixed.frame.size.width + 2, timeZoneLabelFixed.frame.origin.y, 160, 18)];
                [timeZoneLabel setFont:[UIFont systemFontOfSize:16]];
            }
            else
            {
                timeZoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(timeZoneLabelFixed.frame.origin.x + timeZoneLabelFixed.frame.size.width + 2, timeZoneLabelFixed.frame.origin.y, 80, 18)];
                [timeZoneLabel setFont:[UIFont systemFontOfSize:14]];
            }
            timeZoneLabel.text = localAbbreviation;
            
			[timeZoneLabel setTextAlignment:NSTextAlignmentLeft];
			[timeZoneLabel  setBackgroundColor:[UIColor clearColor]];
            timeZoneLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            [cell.contentView addSubview:timeZoneLabel];
			
			
		}
			break;
		case 2:{
            UILabel *calendarCaptionLabel;
            if(IS_IPAD)
            {
                calendarCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
                [calendarCaptionLabel setFont:[UIFont boldSystemFontOfSize:19]];
            }
            else
            {
                calendarCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 720, 25)];
                [calendarCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
            }
			[calendarCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[calendarCaptionLabel setText:@"Calendar"];
			[cell.contentView  addSubview:calendarCaptionLabel];
			[calendarCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[calendarCaptionLabel  setTextColor:[UIColor blackColor]];
			
            UILabel *calendarLabel;
            if(IS_IPAD)
            {
                calendarLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, calendarCaptionLabel.frame.origin.y + calendarCaptionLabel.frame.size.height + 3, 290, 25)];
                [calendarLabel setFont:[UIFont systemFontOfSize:19]];
            }
            else
            {
                calendarLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, calendarCaptionLabel.frame.origin.y + calendarCaptionLabel.frame.size.height + 3, 720, 25)];
                [calendarLabel setFont:[UIFont systemFontOfSize:16]];
            }
			[calendarLabel setTextAlignment:NSTextAlignmentLeft];
			[calendarLabel setText:[self getCalendarName:appDataObj.calendarName]];
			[calendarLabel setBackgroundColor:[UIColor clearColor]];
			//[calendarLabel setTextColor:[UIColor blueColor]];
            calendarLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:calendarLabel];
			
		}
			break;
		case 3: {
            if (!([appDataObj.alert isEqualToString:@""]||[appDataObj.alert isEqualToString:@"Never"])){
                UILabel *alertCaptionLabel;
                if(IS_IPAD)
                {
                    alertCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 720, 25)];
                    [alertCaptionLabel setFont:[UIFont boldSystemFontOfSize:19]];
                }
                else
                {
                    alertCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
                    [alertCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
                }
			[alertCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[alertCaptionLabel setText:@"Alert"];
			[cell.contentView  addSubview:alertCaptionLabel];
			[alertCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[alertCaptionLabel  setTextColor:[UIColor blackColor]];
			
                UILabel *alertLabel;
                if(IS_IPAD)
                {
                    alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertCaptionLabel.frame.origin.y + alertCaptionLabel.frame.size.height + 3, 720, 25)];
                     [alertLabel setFont:[UIFont systemFontOfSize:19]];
                }
                else
                {
                    alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertCaptionLabel.frame.origin.y + alertCaptionLabel.frame.size.height + 3, 290, 25)];
                    [alertLabel setFont:[UIFont systemFontOfSize:16]];
                }
			[alertLabel setTextAlignment:NSTextAlignmentLeft];
			[alertLabel setText:appDataObj.alert];
			[alertLabel setBackgroundColor:[UIColor clearColor]];
			//[alertLabel setTextColor:[UIColor blueColor]];
            alertLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:alertLabel];
            ///Anil's added///////
            
                UILabel *alertSecondLabel;
                if(IS_IPAD)
                {
                    alertSecondLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertLabel.frame.origin.y + alertLabel.frame.size.height + 3, 720, 25)];
                    [alertSecondLabel setFont:[UIFont systemFontOfSize:19]];
                }
                else{
                     alertSecondLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, alertLabel.frame.origin.y + alertLabel.frame.size.height + 3, 290, 25)];
                    [alertSecondLabel setFont:[UIFont systemFontOfSize:16]];
                }
			[alertSecondLabel setTextAlignment:NSTextAlignmentLeft];
			[alertSecondLabel setText:appDataObj.alertSecond];
			[alertSecondLabel setBackgroundColor:[UIColor clearColor]];
			//[alertSecondLabel setTextColor:[UIColor blueColor]];
            alertSecondLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:alertSecondLabel];
                ///Anil's added///////

			}
		}
			break;
		case 4:{
            if (!([appDataObj.repeat isEqualToString:@""]||[appDataObj.repeat isEqualToString:@"Never"] || [appDataObj.repeat isEqualToString:@"None"])){
                UILabel *repeatCaptionLabel;
                if(IS_IPAD)
                {
                    repeatCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 720, 25)];
                    [repeatCaptionLabel setFont:[UIFont boldSystemFontOfSize:19]];
                }
                else
                {
                    repeatCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
                    [repeatCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
                }
			[repeatCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[repeatCaptionLabel setText:@"Repeat"];
			[cell.contentView  addSubview:repeatCaptionLabel];
			[repeatCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[repeatCaptionLabel  setTextColor:[UIColor blackColor]];
			
                UILabel *repeatLabel;
                if(IS_IPAD)
                {
                    repeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, repeatCaptionLabel.frame.origin.y + repeatCaptionLabel.frame.size.height + 3, 720, 25)];
                     [repeatLabel setFont:[UIFont systemFontOfSize:19]];
                }
                else
                {
                    repeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, repeatCaptionLabel.frame.origin.y + repeatCaptionLabel.frame.size.height + 3, 290, 25)];
                    [repeatLabel setFont:[UIFont systemFontOfSize:16]];
                }
			[repeatLabel setTextAlignment:NSTextAlignmentLeft];
			[repeatLabel setText:appDataObj.repeat];
			[repeatLabel setBackgroundColor:[UIColor clearColor]];
			//[repeatLabel setTextColor:[UIColor blueColor]];
            repeatLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
			[cell.contentView  addSubview:repeatLabel];
			
		}
        }
			break;
		case 5:{
            UILabel *calendarCaptionLabel;
            if(IS_IPAD)
            {
                calendarCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 720, 25)];
                 [calendarCaptionLabel setFont:[UIFont boldSystemFontOfSize:19]];
            }
            else
            {
                calendarCaptionLabel = [[UILabel alloc ] initWithFrame:CGRectMake(5, 7, 290, 25)];
                [calendarCaptionLabel setFont:[UIFont boldSystemFontOfSize:16]];
            }
			[calendarCaptionLabel setTextAlignment:NSTextAlignmentLeft];
			[calendarCaptionLabel setText:@"Notes"];
			[cell.contentView  addSubview:calendarCaptionLabel];
			[calendarCaptionLabel setBackgroundColor:[UIColor clearColor]];
			[calendarCaptionLabel  setTextColor:[UIColor blackColor]];
			
            CGSize strSize;
            UITextView *calendarLabel;
            if(IS_IPAD)
            {
                strSize = [appDataObj.shortNotes sizeWithFont:[UIFont boldSystemFontOfSize:16]
                                            constrainedToSize:CGSizeMake(720, 9999) lineBreakMode:NSLineBreakByWordWrapping];
                
                strSize.height = strSize.height +30;
                
                
                
                calendarLabel = [[UITextView alloc ] initWithFrame:CGRectMake(5, calendarCaptionLabel.frame.origin.y + calendarCaptionLabel.frame.size.height + 3, 720, strSize.height + 5)];
                [calendarLabel setFont:[UIFont systemFontOfSize:19]];
            }
            else
            {
            strSize = [appDataObj.shortNotes sizeWithFont:[UIFont boldSystemFontOfSize:16]
											   constrainedToSize:CGSizeMake(290, 9999) lineBreakMode:NSLineBreakByWordWrapping];
            
            strSize.height = strSize.height +30;
			
            
            
            calendarLabel = [[UITextView alloc ] initWithFrame:CGRectMake(5, calendarCaptionLabel.frame.origin.y + calendarCaptionLabel.frame.size.height + 3, 290, strSize.height + 5)];
            [calendarLabel setFont:[UIFont systemFontOfSize:16]]; //16
            }
            
            //[locationLabel setTextAlignment:UITextAlignmentLeft]; //Deprecated
            [calendarLabel setContentMode:UIViewContentModeLeft];
            [calendarLabel setContentMode:UIViewContentModeTop];
            [calendarLabel setClipsToBounds:YES];
            [calendarLabel setScrollEnabled:NO];
            [calendarLabel setText:appDataObj.shortNotes];
            [calendarLabel setBounces:NO];
            [calendarLabel  setBackgroundColor:[UIColor clearColor]]; //Clear
            calendarLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
            
            calendarLabel.editable = NO;
            calendarLabel.dataDetectorTypes = UIDataDetectorTypeAll; //Steve - allows for linkiable phone number, web & map links
            
            [cell.contentView  addSubview:calendarLabel];
			
		}
			break;
		default:
			break;
	}
    
    previousIndexPathRow = indexPath.row; //keeps track of previous row for next loop
    
    if (IS_IOS_7) {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        [cell setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    }
    
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(NSString*)getCalendarName:(NSString *)calname
{
    NSArray * temp = [calname componentsSeparatedByString:@"~"];
    
    if([temp count]>=2){
        calname =[temp objectAtIndex:0];
        
        NSString *calID = [temp objectAtIndex:1];
        calendarIdentifier = calID;
    }
    return calname;
}

//Steve added - checks to see if Calendar can be modified
-(void) checkIfCalendarIsModifiable{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKCalendar *calendar = [EKCalendar calendarWithEventStore:eventStore];
    EKSource *theSource = nil;
    NSMutableArray *arrCal =[[NSMutableArray alloc]init];
    
    defaultcal = [[CalendarDataObject alloc]init];
    
    NSString *tempCalendarName = [self getCalendarName:[appDataObj calendarName]];
    [defaultcal setCalendarName:tempCalendarName];
    [defaultcal setCalendarIdentifire:calendarIdentifier];
    
 
    for (EKSource *source in eventStore.sources) {
        theSource = source;
        
         //NSLog(@" theSource.title =  %@", theSource.title);
        
        if (theSource && ![theSource.title isEqualToString:@"Other"])
        {
            calendar.source = theSource;
            [arrCal removeAllObjects];
            
            for (EKCalendar *thisCalendar in theSource.calendars)
            {
                
                calObj = [[CalendarDataObject alloc]init];
                [calObj setCalendarIdentifire:thisCalendar.calendarIdentifier];
                [calObj setCalendarName:thisCalendar.title];
                const CGFloat *component = CGColorGetComponents(thisCalendar.CGColor);
                [calObj setCalendarColorRed:component[0]];
                [calObj setCalendarColorGreen:component[1]];
                [calObj setCalendarColorBlue:component[2]];
            
                if(thisCalendar.immutable)
                    [calObj setIsCalendarImmutable:YES];
                else
                    [calObj setIsCalendarImmutable:NO];
                
                if(thisCalendar.allowsContentModifications)
                    [calObj setIsAllowsContentModifications:YES];
                else
                    [calObj setIsAllowsContentModifications:NO];

                //Steve added - finds if event with Calendar is able to be modified
                if ([defaultcal.calendarIdentifire isEqualToString:calObj.calendarIdentifire]) {
                    
                    if (calObj.isAllowsContentModifications) {
                        isCalendarAllowedToBeModified = YES;
                    }
                    else
                       isCalendarAllowedToBeModified = NO;
                }
                
                [arrCal addObject: calObj];
                
            }
            if([arrCal count]>=1)
                [calDataDictionary setObject:[NSArray arrayWithArray:arrCal] forKey:theSource.title];
            
        }
        
    }
    
   // NSLog(@"******isCalendarAllowedToBeModified = %d", isCalendarAllowedToBeModified);
    
    //return calDataDictionary;
}




#pragma mark -
#pragma mark Memory management
#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

//Steve  added
- (void) viewWillDisappear:(BOOL)animated{
    //NSLog(@"viewWillDisappear ViewAppointmentScreen");
    [super viewWillDisappear:YES];
    
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    //self.navigationItem.rightBarButtonItem = nil; //Steve commented
    [editButton removeFromSuperview];
    
}

- (void)viewDidUnload {
    shareButton = nil;
    backButton = nil;
    editButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end

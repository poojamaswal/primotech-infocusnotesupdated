//
//  WallpaperViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/22/13.
//
//

#import "WallpaperViewController.h"

@interface WallpaperViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView1;


- (IBAction)imageViewTapped:(UITapGestureRecognizer *)sender;

@end

@implementation WallpaperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _scrollView.translatesAutoresizingMaskIntoConstraints  = NO;
    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    _scrollView.delegate = self;
    
    
    
    //pooja-iPad
    if(IS_IPAD)
    {
        _scrollView.frame = CGRectMake(0, 0, 768, 1024);
        _scrollView.contentSize = CGSizeMake(768, 1700);
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    else
    {
        if (IS_IPHONE_5) {
            
            _scrollView.frame = CGRectMake(0, 0, 320, 568);
        }
        else{
            
            _scrollView.frame = CGRectMake(0, 0, 320, 480);
        }
        _scrollView.contentSize = CGSizeMake(320, 1067);
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
    }
     
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)imageViewTapped:(UITapGestureRecognizer *)sender {
    //Each UIImageView is Tagged and each one is assinged a tapGesture on the Storyboard
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString *wallpaperName  = [NSString stringWithFormat:@"CalendarImage_%d.jpg", sender.view.tag];
    [sharedDefaults setObject:wallpaperName forKey:@"CalendarImageName"]; //saves the image name to UserDefualts
    
     [self.navigationController popViewControllerAnimated:YES];
    
    //NSLog(@" sender.view.tag = %d", sender.view.tag);
    //NSLog(@" wallpaperName = %@", wallpaperName);
    
}





@end

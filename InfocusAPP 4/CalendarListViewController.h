//
//  CalendarListViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppointmentsDataObject.h"
#import "AppointmentViewController .h"
#import "OrganizerAppDelegate.h"

@class OrganizerAppDelegate;

@interface CalendarListViewController : UIViewController {
	IBOutlet UITableView *tblAppinmentList;
    OrganizerAppDelegate* appDelegate;
    UIImageView *sctnVw;
	BOOL isTodayTapped;
    //	BOOL isTodayPresent;
	NSString *appString1;
	NSMutableArray *appointmentsArray;
	NSInteger headerIndex;
	BOOL isTodayEvent;
	AppointmentsDataObject* appDataObject;
	NSString *dueStr;
	NSString *destStr;
	NSInteger EventValue;
    NSString *titleStr;
}

@property(nonatomic, strong) UITableView *tblAppinmentList;
@property(nonatomic, strong) NSMutableArray *appointmentsArray;
@property(nonatomic, strong) NSMutableString        *strTextToSpeak;//Aman's Added
@property(nonatomic, strong) NSDate                 *strtDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *endDate ;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrM_finalEvents;//Aman's Added
-(void)gotoToday;
-(void)checkEventPresent;
-(void)eventToSpeak;//Aman's Added
-(NSMutableString *)finalString;
-(NSInteger)numberofAppointments;
-(BOOL)hasConnectivity;
- (void) refreshViews;
@end

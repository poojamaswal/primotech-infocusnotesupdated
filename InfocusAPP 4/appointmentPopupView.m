//
//  appointmentPopupView.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "appointmentPopupView.h"
#import "CalendarDailyViewController.h"
#import "CalendarWeekViewController.h"
#import "AppointmentsDataObject.h"

@interface appointmentPopupView()

@property(assign, nonatomic) BOOL isAllDayShwowingOnThisDate; //Steve


-(void)addControls;  
@end


@implementation appointmentPopupView

-(id)initWithAppObject:(AppointmentsDataObject*) appObj buttonRect:(CGRect) buttonRectLocal visibleRect:(CGRect) rect andParentVC:(id) parent withTag:(NSInteger) tagValue isAllDay:(BOOL) isAllDayLocal isAllDayShowing:(BOOL) isAllDayShowingNow
{                                            
	if (self = [super initWithFrame:CGRectMake(0, 0, 100, 100)]) {
		self.backgroundColor = [UIColor clearColor];
		parentVC = parent;
		appaObj = appObj;
		visibleRectSV = rect;
		buttonRect = buttonRectLocal;
		arrowPoint = CGPointMake(buttonRect.size.width + buttonRect.origin.x, buttonRect.origin.y);
		btnTag = tagValue;
		isAllDay = isAllDayLocal;
        _isAllDayShwowingOnThisDate = isAllDayShowingNow;
        
        //NSLog(@"appaObj.appointmentType =  %@", appaObj.appointmentType);
       // NSLog(@"appaObj.appointmentBinary =  %@", appaObj.appointmentBinary);
       // NSLog(@"appaObj.appointmentBinaryLarge =  %@", appaObj.appointmentBinaryLarge);

		[self addControls];	
		[self setUserInteractionEnabled:YES];
		return self;
	}
	return self;
}

//Alok Added For Checking InFocus Handwritten Event
-(BOOL)IsHandwritingEvent:(NSString *) EventName{
    if([EventName length] >= 25)
    {
        if([[EventName substringToIndex:25] isEqualToString:@"InFocus Handwritten Event"])
            return YES;
        else return NO;
        
    }
    return NO;
}

-(void)addControls {
	CGFloat gapBWLabel = 5;
    
    
    ///////////////// Steve - determine if 12 Hour or 24 Hour clock used /////////////////////
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setLocale:[NSLocale currentLocale]];
    [formatter2 setDateStyle:NSDateFormatterNoStyle];
    [formatter2 setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter2 stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter2 AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter2 PMSymbol]];
    
    is24HourClock = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    //NSLog(@"is24HourClock = %@\n",(is24HourClock ? @"YES" : @"NO"));
    
    /////////// Steve end /////////////////////////////////////////////////////////////////
    
	
	UIImage *arrowImage			= [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxArrowDown" ofType:@"png"]];
	UIImage *popupBGImage;
	UIImage *discloserBtnImage	= [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DetailsDisclouserBtn" ofType:@"png"]];
	
	UIImageView *arrowImageView		= [[UIImageView alloc] initWithImage:arrowImage];
	arrowImageView.layer.shadowColor = [[UIColor blackColor] CGColor];
	arrowImageView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
	arrowImageView.layer.shadowOpacity = 0.5f;
	arrowImageView.layer.shadowRadius = 5.0f;
	
	UIButton	*popupButton		= [[UIButton alloc] init];
	UIButton	*detailDiscloserBtn = [[UIButton alloc] init];
    
    //detailDiscloserBtnLargerInvisible is an invisible button with a larger CGRect than the detailDiscloserBtn to make clicking easier
    UIButton *detailDiscloserBtnLargerInvisible =[[UIButton alloc]init]; //Steve added

    
    UILabel		*titleLable		= [[UILabel alloc] initWithFrame:CGRectMake(7, gapBWLabel, 210, 15)];
    [titleLable setBackgroundColor:[UIColor clearColor]];
    
    if ([appaObj.appointmentType isEqualToString:@"T"]) { //Steve - fixes bug with new transparant Handwritten events
        titleLable.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f]; //Steve
        [titleLable setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
        [titleLable setText:appaObj.appointmentTitle];
    }

	
	UILabel		*locationLable	= [[UILabel alloc] initWithFrame:CGRectMake(7, gapBWLabel + titleLable.frame.origin.y +  titleLable.frame.size.height, 210, 15)];
	[locationLable setBackgroundColor:[UIColor clearColor]];
	[locationLable setTextColor:[UIColor blackColor]];
	[locationLable setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
	locationLable.text = appaObj.textLocName;
	
//	if (appaObj.locID) {
//		LocationDataObject *aLocObj = [[GetAllDataObjectsClass getLocationDataObjectForLocationID:appaObj.locID] retain];
//		locationLable.text  = aLocObj == nil ? @"None" :[NSString stringWithFormat:@"%@, %@, %@",[aLocObj locationAddress],[aLocObj locationCity],[aLocObj locationState]];
//		[aLocObj release];
//	}
	
	UILabel		*timeLableFrom = [[UILabel alloc] initWithFrame:CGRectMake(7, gapBWLabel +  locationLable.frame.origin.y + locationLable.frame.size.height, 210, 15)];
	[timeLableFrom setBackgroundColor:[UIColor clearColor]];
	[timeLableFrom setTextColor:[UIColor blackColor]];
	[timeLableFrom setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[timeLableFrom setAdjustsFontSizeToFitWidth:YES];
	//[timeLableFrom setMinimumFontSize:8];
    [timeLableFrom setMinimumScaleFactor:8];

	UILabel		*timeLableTo = [[UILabel alloc] initWithFrame:CGRectMake(7, gapBWLabel +  timeLableFrom.frame.origin.y + timeLableFrom.frame.size.height, 210, 15)];
	[timeLableTo setBackgroundColor:[UIColor clearColor]];
	[timeLableTo setTextColor:[UIColor blackColor]];
	[timeLableTo setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[timeLableTo setAdjustsFontSizeToFitWidth:YES];
	//[timeLableTo setMinimumFontSize:8];
    [timeLableTo setMinimumScaleFactor:8];

	UILabel		*notesLabel	= [[UILabel alloc] initWithFrame:CGRectMake(7, gapBWLabel +  timeLableTo.frame.origin.y + timeLableTo.frame.size.height, 210, 15)];
	[notesLabel setBackgroundColor:[UIColor clearColor]];
	[notesLabel setTextColor:[UIColor blackColor]];
	[notesLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	//[notesLabel setLineBreakMode:UILineBreakModeWordWrap];
    [notesLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[notesLabel setNumberOfLines:0];
	[notesLabel setText:[appaObj shortNotes]];
	
	UIImage		*titleImage	= [[UIImage alloc] initWithData:[appaObj appointmentBinary]];
	UIButton	*titleImageBtn = [[UIButton alloc] initWithFrame:CGRectMake(7, gapBWLabel, 205, 45)];
	[titleImageBtn setBackgroundImage:titleImage forState:UIControlStateNormal];

	NSDateFormatter *formate = [[NSDateFormatter alloc] init];
	[formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDate *strtDate = [formate dateFromString:[appaObj startDateTime]];
	NSDate *endDate = [formate dateFromString:[appaObj dueDateTime]];
    
	[formate setDateFormat:@"yyyy-MM-dd"];
    
    
//******************* Steve TEST *******************
	if ([[formate dateFromString:[[strtDate description] substringToIndex:10]] compare:  [formate dateFromString:[[endDate description] substringToIndex:10]]] == NSOrderedSame) {
        
        if (is24HourClock) { //Steve added is24HourClock
            //[formate setDateFormat:@"EEE, MMM d, yyyy"];//Not international compatible
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterMediumStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterNoStyle]; //Steve
            
            [timeLableFrom setText:[formate stringFromDate:strtDate]];
            //[formate setDateFormat:@"h:mm a"]; //Not international compatible
            [formate setDateStyle:NSDateFormatterNoStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterShortStyle]; //Steve
            
            [timeLableTo setText:[NSString stringWithFormat:@"%@ To %@", [formate stringFromDate:strtDate], [formate stringFromDate:endDate] ] ];
            
           // NSLog(@"timeLableFrom = %@", timeLableFrom.text);
            
        }
        else{
            //[formate setDateFormat:@"EEE, MMM d, yyyy"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterMediumStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterNoStyle]; //Steve
            
            [timeLableFrom setText:[formate stringFromDate:strtDate]];
            
            //[formate setDateFormat:@"h:mm a"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterNoStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterShortStyle]; //Steve
            
            [timeLableTo setText:[NSString stringWithFormat:@"%@ To %@", [[formate stringFromDate:strtDate] stringByReplacingOccurrencesOfString:@":00" withString:@""], [[formate stringFromDate:endDate] stringByReplacingOccurrencesOfString:@":00" withString:@""]]];
            
            //NSLog(@"timeLableFrom = %@", timeLableFrom.text);
        }
        
	}
	else {
        
        if (is24HourClock) { //Steve added is24HourClock
            //[formate setDateFormat:@"EEE, MMM d, yyyy"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterMediumStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterNoStyle]; //Steve
            
            [timeLableFrom setText:[formate stringFromDate:strtDate]];
            
            //[timeLableTo setText:[@"To " stringByAppendingString:[formate stringFromDate:endDate]]]; //Steve commented to remove "To", which is too close to Disclosure button
            
            [timeLableTo setText:[formate stringFromDate:endDate]]; //Steve added/. No "To"
            
            //[formate setDateFormat:@"h:mm a"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterNoStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterShortStyle]; //Steve
            
            [timeLableFrom setText:[timeLableFrom.text stringByAppendingFormat:@" %@", [formate stringFromDate:strtDate] ] ];
            [timeLableTo setText:[timeLableTo.text stringByAppendingFormat:@" %@", [formate stringFromDate:endDate] ] ];
        }
        else{
            
            //[formate setDateFormat:@"EEE, MMM d, yyyy"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterMediumStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterNoStyle]; //Steve
            
            [timeLableFrom setText:[formate stringFromDate:strtDate]];
            
            //[timeLableTo setText:[@"To " stringByAppendingString:[formate stringFromDate:endDate]]]; //Steve commented to remove "To", which is too close to Disclosure button
            
            [timeLableTo setText:[formate stringFromDate:endDate]]; //Steve added/. No "To"
            
            //[formate setDateFormat:@"h:mm a"];
            [formate setLocale:[NSLocale currentLocale]]; //Steve
            [formate setDateStyle:NSDateFormatterNoStyle]; //Steve
            [formate setTimeStyle:NSDateFormatterShortStyle]; //Steve
            
            [timeLableFrom setText:[timeLableFrom.text stringByAppendingFormat:@" %@", [[formate stringFromDate:strtDate] stringByReplacingOccurrencesOfString:@":00" withString:@""]]];
            [timeLableTo setText:[timeLableTo.text stringByAppendingFormat:@" %@", [[formate stringFromDate:endDate] stringByReplacingOccurrencesOfString:@":00" withString:@""]]];
        }
        
        
	}


	CGSize strSize = [appaObj.shortNotes sizeWithFont:[UIFont boldSystemFontOfSize:12] 
									constrainedToSize:CGSizeMake(219, 9999) lineBreakMode:NSLineBreakByWordWrapping];

	if ([[appaObj appointmentType] isEqualToString:@"H"] || [self IsHandwritingEvent:appaObj.appointmentTitle])
	{
		if ([appaObj.textLocName isEqualToString:@""]) 
		{
			[locationLable setFrame:CGRectMake(7, titleImageBtn.frame.origin.y + titleImageBtn.frame.size.height, 210 - discloserBtnImage.size.width - 5, 0)];
            
			if (strSize.height > 45) 
			{
				popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxThreeLine" ofType:@"png"]];
			}
			else {
				popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxTwoLine" ofType:@"png"]];
			}
		}
		else 
		{
			[locationLable setFrame:CGRectMake(7, gapBWLabel + titleImageBtn.frame.origin.y + titleImageBtn.frame.size.height, 210 - discloserBtnImage.size.width - 5, locationLable.frame.size.height)];
			popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxTwoLine" ofType:@"png"]];
		}
	} 
	else 
	{
		if ([appaObj.textLocName isEqualToString:@""])
		{
			[locationLable setFrame:CGRectMake(7, titleLable.frame.origin.y + titleLable.frame.size.height, 210 - discloserBtnImage.size.width - 5, 0)];
			if (strSize.height > 45) 
			{
				popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxTwoLine" ofType:@"png"]];
			}
			else
			{
				popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxOneLine" ofType:@"png"]];
			}
		}
		else
		{
			[locationLable setFrame:CGRectMake(7, gapBWLabel + titleLable.frame.origin.y + titleLable.frame.size.height, 210 - discloserBtnImage.size.width - 5, locationLable.frame.size.height)];
			popupBGImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxTwoLine" ofType:@"png"]];
		}
	}
	
	
	[titleLable setFrame:CGRectMake(7, gapBWLabel, 210, 15)];
	[timeLableFrom setFrame:CGRectMake(7, gapBWLabel + locationLable.frame.origin.y + locationLable.frame.size.height, 210 - discloserBtnImage.size.width - 5, 15)];
	[timeLableTo setFrame:CGRectMake(7, gapBWLabel + timeLableFrom.frame.origin.y + timeLableFrom.frame.size.height, 210 - discloserBtnImage.size.width - 5, 15)];
	
	CGFloat finalHeight = (popupBGImage.size.height - (gapBWLabel + timeLableTo.frame.origin.y + timeLableTo.frame.size.height)) - 2;

	[notesLabel setFrame:CGRectMake(7, gapBWLabel + timeLableTo.frame.origin.y + timeLableTo.frame.size.height - 2, 210, strSize.height>finalHeight?finalHeight:strSize.height)];
	
	[popupButton addSubview: titleLable];
	[popupButton addSubview: locationLable];
	[popupButton addSubview: timeLableFrom];
	[popupButton addSubview: timeLableTo];
//	[popupButton addSubview: notesLabel];
	[popupButton addSubview: titleImageBtn];
	
    
    
    
	CGFloat	initCntrlSpaceY		= 85; 
	CGFloat	bottomCntrlSpaceY	= 11; 
//	CGFloat	initCntrlSpaceX		= 4; 
	CGFloat	popupHeight		= popupBGImage.size.height + arrowImage.size.height;
	CGFloat	popupWidth		= popupBGImage.size.width;
	CGFloat	popupXPos		= buttonRect.origin.x + buttonRect.size.width/2 - popupWidth/2;
    
    
    //Steve
     if (IS_IPAD) {
     popupXPos				= popupXPos <= 0 ? 0 : popupXPos;
     popupXPos				= popupXPos > 758-popupWidth ? 758-popupWidth : popupXPos;
     }
     else{ //iPhone
     popupXPos				= popupXPos <= 0 ? 0 : popupXPos;
     popupXPos				= popupXPos > 310-popupWidth ? 310-popupWidth : popupXPos;
     }
    
    
	CGFloat	popupYPos;
	CGFloat	arrowXPos		= buttonRect.origin.x + buttonRect.size.width/2 - arrowImage.size.width/2 - popupXPos;
	CGFloat	arrowYPos;
    
    


	
	[popupButton setImage:popupBGImage forState:UIControlStateNormal];
	[popupButton setImage:popupBGImage forState:UIControlStateHighlighted];
    
    [[popupButton layer] setMasksToBounds:NO];
	popupButton.layer.shadowColor = [[UIColor blackColor] CGColor];
	popupButton.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
	popupButton.layer.shadowOpacity = 0.5f;
	popupButton.layer.shadowRadius = 5.0f;
	popupButton.layer.zPosition = 1.0f;
	
	[detailDiscloserBtn setImage:discloserBtnImage forState:UIControlStateNormal];
	[detailDiscloserBtn addTarget:parentVC action:@selector(detailButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[detailDiscloserBtn setTag:btnTag];
    
    detailDiscloserBtnLargerInvisible.userInteractionEnabled = YES;
   	[detailDiscloserBtnLargerInvisible setTag:btnTag]; //Steve
    [detailDiscloserBtnLargerInvisible addTarget:parentVC action:@selector(detailButton_Clicked:) forControlEvents:UIControlEventTouchUpInside]; //Steve
    

    
    
    //Steve - added to make If Statement easier to read & debug below
    int scenario_1 = buttonRect.origin.y - visibleRectSV.origin.y + 44;
    int scenario_2 = (visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height);
    
    
	//if ((buttonRect.origin.y - visibleRectSV.origin.y + 44) > popupHeight && !isAllDay) {
    if (scenario_1 > popupHeight && !isAllDay) {
        
        
        //Steve
        if (_isAllDayShwowingOnThisDate) {
            
            if (IS_IPAD) {
                
                popupYPos = (buttonRect.origin.y - visibleRectSV.origin.y) - popupHeight + initCntrlSpaceY + 18;
                [popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
                arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
            }
            else{ //iPhone
                
                popupYPos = (buttonRect.origin.y - visibleRectSV.origin.y) - popupHeight + initCntrlSpaceY;
                [popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
                arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
            }
        }
        else{ //All Day is NOT showing on this date
            
            
            if (IS_IPAD) {
                
                if ([parentVC isKindOfClass:[CalendarWeekViewController class]]) {
                    
                    popupYPos = (buttonRect.origin.y - visibleRectSV.origin.y) - popupHeight + initCntrlSpaceY + 18;
                    [popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
                    arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
                }
                else if ([parentVC isKindOfClass:[CalendarDailyViewController class]]){
                    
                    popupYPos = (buttonRect.origin.y - visibleRectSV.origin.y) - popupHeight + initCntrlSpaceY;
                    [popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
                    arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
                }
            

            }
            else{ //iPhone
                
                popupYPos = (buttonRect.origin.y - visibleRectSV.origin.y) - popupHeight + initCntrlSpaceY;
                [popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
                arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
            }
            

        }
        

        
		//NSLog(@"Above the event");
        //Steve added  +44 otherwise on Long events its too low (below Toolbar)   + 44
	}
    //popus below the event
    //else if ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height) > popupHeight + 44 || isAllDay) {
    else if (scenario_2 > popupHeight + 44 || isAllDay) {
		
		arrowImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PopupBoxArrowUp" ofType:@"png"]];
		[arrowImageView setImage:arrowImage];
        
		
		if (isAllDay) {
            
            //Steve
            if (IS_IPAD) {
                popupYPos =	initCntrlSpaceY + 18;
            }
            else{ //iPhone
                popupYPos =	initCntrlSpaceY;
            }
            
		}
        else { //All Day event NOT clicked
            
            
            if (_isAllDayShwowingOnThisDate) {
                
                if (IS_IPAD) {
                    
                    popupYPos = visibleRectSV.size.height - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height)) + initCntrlSpaceY + 18;
                    
                }
                else{
                    popupYPos = visibleRectSV.size.height - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height)) + initCntrlSpaceY;
                }
            }
            else{ //is NOT showing All Day on this day
                
                if (IS_IPAD) {
                    
                    if ([parentVC isKindOfClass:[CalendarDailyViewController class]]) {
                        popupYPos = visibleRectSV.size.height - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height)) + initCntrlSpaceY - 2;
                        
                    }
                    else{  //CalendarWeekViewController
                        popupYPos = visibleRectSV.size.height - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height)) + initCntrlSpaceY +18 + 2;
                    }
                    

                }
                else{
                    popupYPos = visibleRectSV.size.height - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height)) + initCntrlSpaceY;
                }
            }
            


		}
		
		[popupButton setFrame:CGRectMake(10, arrowImage.size.height, popupBGImage.size.width, popupBGImage.size.height)];
		arrowYPos = 2;
		//NSLog(@"Below the event");
	}
    else {
		//NSLog(@"In the bottom");
		
		popupYPos = visibleRectSV.size.height - popupHeight + initCntrlSpaceY - bottomCntrlSpaceY;
		
		if (buttonRect.origin.y + buttonRect.size.height < visibleRectSV.origin.y + visibleRectSV.size.height) {
			popupYPos = popupYPos - ((visibleRectSV.origin.y + visibleRectSV.size.height) - (buttonRect.origin.y + buttonRect.size.height));
		}
		
		[popupButton setFrame:CGRectMake(10, 0, popupBGImage.size.width, popupBGImage.size.height)];
		arrowYPos = popupButton.frame.origin.y + popupButton.frame.size.height - 2;
	}
    
    
	[detailDiscloserBtn setFrame:CGRectMake(popupButton.frame.size.width - discloserBtnImage.size.width - 10, popupButton.frame.size.height/2 -  discloserBtnImage.size.height/2, discloserBtnImage.size.width, discloserBtnImage.size.height)];
    
    //Steve added an invisible button so the disclosure button is easier to click - Adjust X by -10, Y by -10, Width by +20, Height by +20
    [detailDiscloserBtnLargerInvisible setFrame:CGRectMake(popupButton.frame.size.width - discloserBtnImage.size.width - 10 -10, popupButton.frame.size.height/2 -  discloserBtnImage.size.height/2 - 10, discloserBtnImage.size.width +20, discloserBtnImage.size.height +20)];
    
    // **************** Steve - Adjustment to self.frame to fix bug  & allow easier to click Discl Button *******************************
    // self.frame was not underneath the popupButton & therefore the right side of the Discl & DisclLarge Button could not be clicked - Adjusted by 15
	//[self setFrame:CGRectMake(popupXPos, popupYPos, popupWidth, popupHeight)]; //Steve commented
    [self setFrame:CGRectMake(popupXPos , popupYPos, popupWidth + 15, popupHeight + 15)];
    
	[arrowImageView setFrame:CGRectMake(arrowXPos, arrowYPos, arrowImage.size.width-6, arrowImage.size.height)];
    
	[self addSubview:popupButton];
    [popupButton addSubview:detailDiscloserBtnLargerInvisible]; //Steve added
   	[popupButton addSubview:detailDiscloserBtn]; 
	[self addSubview:arrowImageView];
	
}		





@end

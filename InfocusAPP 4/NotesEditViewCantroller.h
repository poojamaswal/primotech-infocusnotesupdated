//
//  NotesEditViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NotesEditViewCantroller : UIViewController 
{

	
	IBOutlet UIImageView *inImage;
	IBOutlet UITextField *titleTXT;
	IBOutlet UIButton *titleBTN;
	
    __unsafe_unretained IBOutlet UIButton *DeleteBtn;
	
}

@property(nonatomic,retain)UIImageView *inImage;
@property(nonatomic,retain)UITextField *titleTXT;
@property(nonatomic,retain)UIButton *titleBTN;
-(IBAction)backBTN_Clicked;
- (IBAction)DeleteButtonClicked:(id)sender;
@end

//
//  ProjectReorderViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#define EDit_BTN 9000
#import "ProjectReorderViewCantroller.h"
#import "AddProjectViewCantroller.h"
#import "OrganizerAppDelegate.h"
#import "AddProjectViewCantroller.h"
#import "ViewProjectViewCantroller.h"

@implementation ProjectReorderViewCantroller


@synthesize TempProjectName;
@synthesize tempProjectPriority;
@synthesize temp,tempprojectdataobj,TempProjectID,myOption;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];	
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
	[self getalldata];
	[recordTable reloadData];
}



-(void)getalldata
{
	if (ProjectArrayEdit) 
	{
		ProjectArrayEdit = nil;
		ProjectArrayEdit = [[NSMutableArray alloc] init];
	}
	else
	{
		ProjectArrayEdit = [[NSMutableArray alloc] init];
	}
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{
	NSString *Sql =[[NSString alloc] initWithFormat:@"select projectID, projectName,projectPriority,folderImage from ProjectMaster where level=%d ORDER BY projectPriority ASC",0];
		
		NSLog(@"%@",Sql);
        
	if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
	{
		NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
	}
	else
	{
	
		while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
		{
				AddProjectDataObject  *projectdataobj = [[AddProjectDataObject alloc]init];
				[projectdataobj setProjectid:sqlite3_column_int(selectNotes_Stmt, 0)];
				[projectdataobj setProjectname: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 1)]];
				[projectdataobj setProjectpriority: [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 2)]];
				[projectdataobj setProjectIMGname:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectNotes_Stmt, 3)]];
				[ProjectArrayEdit addObject:projectdataobj];
//				[projectdataobj release];
		
		}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
	}
}

-(IBAction)cancel_click
{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

-(IBAction)done_btnClick:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)add_btnClick:(id)sender
{
	AddProjectViewCantroller *addprojectview = [[AddProjectViewCantroller alloc] initWithNibName:@"AddProjectViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:addprojectview animated:YES];
	
	
}


-(IBAction)editproject_click:(id)sender
{
    ViewProjectViewCantroller *viewprojectview = [[ViewProjectViewCantroller alloc] initWithNibName:@"ViewProjectViewCantroller" bundle:[NSBundle mainBundle]];
    viewprojectview.PIDval =[sender tag];
    //viewprojectview.isEdit =YES;
    viewprojectview.isEditNow =YES;
	viewprojectview.optnView=2;
    [self.navigationController pushViewController:viewprojectview animated:YES];
}

#pragma mark -
#pragma mark Table view data source


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [ProjectArrayEdit count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	
    UIImage *setBimg=[UIImage imageNamed:[[ProjectArrayEdit objectAtIndex:indexPath.row]projectIMGname]];
	UIButton *btni =[[UIButton alloc] initWithFrame:CGRectMake(8, 5, 240, 45)];
	//btni.backgroundColor = [UIColor colorWithRed:[[[ProjectArrayEdit objectAtIndex:indexPath.row]projectcolorR] floatValue] green:[[[ProjectArrayEdit   objectAtIndex:indexPath.row]projectcolorG] floatValue] blue:[[[ ProjectArrayEdit  objectAtIndex:indexPath.row]projectcolorB] floatValue] alpha:1];
    [btni setBackgroundImage:setBimg forState:UIControlStateNormal];
	//[btni setTitle:[[ProjectArrayEdit    objectAtIndex:indexPath.row]projectname] forState:UIControlStateNormal];
    
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,10,200,30)];
    //myLabel.text = [NSString stringWithFormat:@"%d", node.prjctID];
    myLabel.text=[[ProjectArrayEdit    objectAtIndex:indexPath.row]projectname];
    myLabel.textColor= [UIColor whiteColor];
    myLabel.backgroundColor = [UIColor clearColor];
  
    [btni addSubview:myLabel];
	[cell.contentView addSubview:btni];
	UIImage *EditImage =
	[UIImage imageNamed:@"EditBtn.png"];
	UIButton *EditImageView = [[UIButton alloc] initWithFrame:CGRectMake(250.0,20.0,63.0, 27.0)];
	[EditImageView setBackgroundImage:EditImage forState:UIControlStateNormal];
	[EditImageView setBackgroundImage:EditImage forState:UIControlStateHighlighted];
	
	EditImageView.layer.masksToBounds = YES;
	[EditImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	EditImageView.tag=[[ProjectArrayEdit  objectAtIndex:indexPath.row]projectid ];
	[EditImageView addTarget:self action:@selector(editproject_click:) forControlEvents:UIControlEventTouchUpInside];
	[cell.contentView addSubview:EditImageView];
	
	
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//	 [self.navigationController pushViewController:detailViewController animated:YES];
//	 [detailViewController release];
//	 */
//}


- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end

//
//  DBXUtil.h
//  DBTest
//

//

#import <Foundation/Foundation.h>
#import <DropboxSDK/DropboxSDK.h>

@protocol LodingIndicator <NSObject>
@required
-(void)sendIndicatorStopAnimationNotification:(NSString*)strMessage;
@end

@interface DBXUtil : NSObject <DBSessionDelegate, DBRestClientDelegate> {
    DBRestClient *restClient;
    NSString *remoteDir;
    NSString *destPathToBeUsed;
    NSString *origPathToBeUsed;
    int command;
    int result;
    CGFloat commandProgress;
    DBMetadata *fileslist;
}


@property(nonatomic, assign)id<LodingIndicator> delegate;
@property (nonatomic, readonly) DBRestClient *restClient;
@property (strong, nonatomic) NSString *remoteDir;
@property (strong, nonatomic) NSString *destPathToBeUsed;
@property (strong, nonatomic) NSString *origPathToBeUsed;
@property (nonatomic, readwrite) CGFloat commandProgress;
@property (strong, nonatomic) DBMetadata *fileslist;

- (void)linkDB;
- (void)unlinkDB;
- (void)setDir:(NSString *)dir;
- (void)removePath:(NSString *)path;
- (void)uploadFileTo:(NSString *)toPath from:(NSString *)fromPath;
- (void)downloadFileFrom:(NSString *)fromPath to:(NSString *)toPath;
- (void)listFiles:(NSString *)dir;

@end

enum DropboxOp {
    KDBXNOOP = 0,
    KDBXSETDIR,
    KDBXREMOVE,
    KDBXDOWNLOAD,
    KDBXUPLOAD,
    KDBXLISTFILES
};

enum DropboxRc {
    KDBXRCNOERR = 0,
    KDBXRCERROR,
    KDBXRCINPROGRESS,
    KDBXRCNODIR,
    KDBXRCPARMERR
};
//
//  FAQViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQViewController : UITableViewController{

    IBOutlet UIButton *doneButton;
    IBOutlet UIButton *backButton;
    __weak IBOutlet UITableView *TblViewFAQ;
}


- (IBAction)backButton_Clicked:(id)sender;

@end

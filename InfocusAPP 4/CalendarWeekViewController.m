//  CalendarWeekViewController.m
//  Organizer
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define Week_Event_TAG  99999
#define Week_Allday_Event_TAG  999999

#import "CalendarWeekViewController.h"
#import "AppointmentsDataObject.h"
#import "AppointmentViewController .h"
#import "NSDate-Utilities.h"
#import "appointmentPopupView.h"
#import "ViewAppointmentScreen.h"
#import "CustomEventButton.h"
#import "iSpeechViewControllerTTS.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <math.h>
#import "ToDoViewController.h"



@interface CalendarWeekViewController()

@property (strong, nonatomic) IBOutlet UIImageView *weekImageView; //Steve
@property (strong, nonatomic) IBOutlet UIButton *nextButton; //Steve
@property (strong, nonatomic) IBOutlet UIButton *previousButton; //Steve


-(int) weekDayForDate:(NSString *) date;
-(NSString *) dateByAddingDays:(NSUInteger) days fromDate:(NSString *) date;
-(NSString *) dateBySubtractingDays:(NSInteger) days fromDate:(NSString *) date;
-(void) creatAppointmentSubViews;
-(NSString*) createHeaderString;
-(void) setDayLabels;
-(void) removeOldData;
-(void) addRefreshControles;
-(void) autoScrollToEvent;
-(void) showCurrentTime;
-(void) resizeToInitAllDayView;
-(int) compairDateWithoutTimeDate1:(NSDate *) dateOne date2:(NSDate *) dateTwo;
//Steve - sets delay on refreshViews so in iOS 6 data can load first, otherwise in iOS6 it duplicates the data
-(void) refreshViewsAfterDelay;
-(NSArray*)findsCalendarsToShow;//Steve - finds the correct calendars to show
-(BOOL)isEventForTheCurrentWeek:(NSDate *) eventDate; //Steve - determines if event should be shown on week

@end

@implementation CalendarWeekViewController
@synthesize currentTimeImageView, lastSelectedEvent, weekAppDataObjArray, allDayWeekObjArray;
@synthesize strTextToSpeak,strtDate,endDate,arrM_finalEvents;//Aman's Added
@synthesize str_todaysDate;///Aman's Added
@synthesize str_Week;///Aman's Added
BOOL internet_YES_NO;/// Aman 's Added

#pragma mark -
#pragma mark View Controller Methods
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		weekAppDataObjArray = [[NSMutableArray alloc] init];
		allDayWeekObjArray =  [[NSMutableArray alloc] init];
        self.arrM_finalEvents = [[NSMutableArray alloc] init];
        
        
		sundaysObjArray = [[NSMutableArray alloc] init];;
		mondaysObjArray = [[NSMutableArray alloc] init];;
		tuedaysObjArray = [[NSMutableArray alloc] init];;
		weddaysObjArray = [[NSMutableArray alloc] init];;
		thudaysObjArray = [[NSMutableArray alloc] init];;
		fridaysObjArray = [[NSMutableArray alloc] init];;
		satdaysObjArray = [[NSMutableArray alloc] init];;
		
		sundaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		mondaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		tuedaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		weddaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		thudaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		fridaysObjArrayAllDay = [[NSMutableArray alloc] init];;
		satdaysObjArrayAllDay = [[NSMutableArray alloc] init];;
    }
    
	allDayLabel = [[UILabel alloc] init];
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
      [super viewWillAppear:YES];
   // NSLog(@"viewWillAppear");
    
    //Steve - shuts off auto resize mask, otherwise, Password view will change views
    self.view.autoresizingMask = NO;
    
    ///////////////// Steve - determine if 12 Hour or 24 Hour clock used /////////////////////
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setLocale:[NSLocale currentLocale]];
    [formatter2 setDateStyle:NSDateFormatterNoStyle];
    [formatter2 setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter2 stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter2 AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter2 PMSymbol]];
    
    BOOL is24HourClock = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    //NSLog(@"is24HourClock = %@\n",(is24HourClock ? @"YES" : @"NO"));
    
    /////////// Steve end /////////////////////////////////////////////////////////////////
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if (IS_IOS_7) {
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
    
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                self.view.backgroundColor = [UIColor whiteColor];
                
                _weekImageView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
                weekNumberLabel.textColor = [UIColor blackColor];
                sunDayLabel.textColor = [UIColor blackColor];
                monDayLabel.textColor = [UIColor blackColor];
                tueDayLabel.textColor = [UIColor blackColor];
                wedDayLabel.textColor = [UIColor blackColor];
                thuDayLabel.textColor = [UIColor blackColor];
                friDayLabel.textColor = [UIColor blackColor];
                satDayLabel.textColor = [UIColor blackColor];
            
               if(IS_IPAD)
               {
                   [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateNormal]; //This is actually a button despite the name
                   [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateHighlighted];
                   [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateSelected];
               }
                else
                {
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateNormal]; //This is actually a button despite the name
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateHighlighted];
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateSelected];
                }
                
                
                if (is24HourClock)
                {
                    if(IS_IPAD)
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour_iPad.png"];
                    else
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour.png"];
                }
                else
                {
                    
                    if(IS_IPAD)
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_White2_iPad.png"];
                    else
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_White2.png"];
                }
                
                
                    [_nextButton setImage:[UIImage imageNamed:@"right-arrow_Blue2.png"] forState:UIControlStateNormal];
                    [_previousButton setImage:[UIImage imageNamed:@"left-arrow_Blue.png"] forState:UIControlStateNormal];
                
                
                
                //Covers line on Nav Controller
                UIView *coverNavLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 44, 320, 1)];
                coverNavLineView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
                [self.navigationController.navigationBar addSubview:coverNavLineView];
    
                
            }
            else{ //Dark Theme - images in NIB
                
                if(IS_IPAD)
                {
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateNormal]; //This is actually a button despite the name
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateHighlighted];
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2_iPad.png"] forState:UIControlStateSelected];
                }
                else
                {
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateNormal]; //This is actually a button despite the name
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateHighlighted];
                    [allWeekTimeImageView setImage:[UIImage imageNamed:@"AllDayWeek_White2.png"] forState:UIControlStateSelected];
                }
                
                if (is24HourClock)
                {
                    if(IS_IPAD)
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour_White2_iPad.png"];
                    else
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour_White.png"];
                }
                else
                {
                    if(IS_IPAD)
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_White2_iPad.png"];
                    else
                        weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_White2.png"];
                }
                
                //[self.view setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
                [self.view setBackgroundColor:[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f]];
                weekTimeScrollView.backgroundColor = [UIColor colorWithRed:235.0/255.0f green:235.0/255.0f blue:235.0/255.0f alpha:1.0f];
                weekTimeImageView.backgroundColor = [UIColor colorWithRed:235.0/255.0f green:235.0/255.0f blue:235.0/255.0f alpha:1.0f];
                
                self.view.backgroundColor = [UIColor whiteColor];
                weekTimeScrollView.backgroundColor = [UIColor whiteColor];
                //weekTimeImageView.backgroundColor = [UIColor colorWithRed:235.0/255.0f green:235.0/255.0f blue:235.0/255.0f alpha:1.0f];
            }
 
        }
        else{ //Picture Calendar
            
            self.view.backgroundColor = [UIColor clearColor];
            weekTimeScrollView.backgroundColor = [UIColor clearColor];
            weekTimeImageView.backgroundColor = [UIColor clearColor];
            allDayDiviserImageView.backgroundColor = [UIColor clearColor];
            _weekImageView.backgroundColor = [UIColor clearColor];
    
            
            //Divides All Day with the Day
            allDayDiviserImageView.image = nil;
            allDayDiviserImageView.backgroundColor = [UIColor whiteColor];
             
            if (is24HourClock)
            {
                if(IS_IPAD)
                {
                    weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour_Picture_iPad.png"];
                    //weekTimeImageView.contentMode=UIViewContentModeScaleAspectFit;
                    
                }
                else
                {
                    weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_24Hour_Picture.png"];
                }
            }
            else
            {
                if(IS_IPAD)
                    weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_Picture_iPad.png"];
                else
                    weekTimeImageView.image = [UIImage imageNamed:@"Calender-weeks_Picture.png"];
            }

            if(IS_IPAD)
            {
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture_iPad.png"] forState:  UIControlStateNormal];
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture_iPad.png"] forState:UIControlStateHighlighted];
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture_iPad.png"] forState:UIControlStateSelected];
            }
            else
            {
                //This is actually a button despite the name
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture.png"] forState:  UIControlStateNormal];
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture.png"] forState:UIControlStateHighlighted];
                [allWeekTimeImageView setBackgroundImage:[UIImage imageNamed:@"AllDayWeek__Picture.png"] forState:UIControlStateSelected];
            }
            
        }
    }
    else{ //iOS 6
        
         [self.view setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    }
    
    
    //Steve - moved here from viewDidLoad
    [self addRefreshControles];
    [self showCurrentTime];
}

-(void)setLodedValue {
	[APPINSTANCE setIsLoded:YES] ;
}
-(void)viewDidDisappear:(BOOL)animated{
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EventNotification" object:nil];
}

- (void)viewDidLoad
{
	//NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    [weekTimeScrollView setContentSize:CGSizeMake(320, 1028)]; //Steve - Was 1012
    
   
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    eventStore = appDelegate.eventStore;
    if (appDelegate.eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
  
    //Steve added
    //detects swipes to the right to move to Previous day Calendar
    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRight];
    //makes sure when swiping right, scroll veiw does not interfere with swipe (otherwise may start scrolling up or down)
    [[weekTimeScrollView panGestureRecognizer] requireGestureRecognizerToFail:swipeRight];
    
    //detects swipes to the left to move to Next day Calendar
    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeft];
    //makes sure when swiping left, scroll veiw does not interfere with swipe (otherwise may start scrolling up or down)
    [[weekTimeScrollView panGestureRecognizer] requireGestureRecognizerToFail:swipeLeft];
    
    
	[NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];//Steve
	[self performSelector:@selector(setLodedValue) withObject:nil afterDelay:0.5];
    
	
	[allDayLabel setBackgroundColor:[UIColor clearColor]];
	[allDayLabel setTextColor:[UIColor blackColor]];
	[allDayLabel setFont:[UIFont boldSystemFontOfSize:13]];
	[allDayLabel setText:@"all-day"];
	[allDayLabel setTag:3223];
	[allDayLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[allDayLabel setNumberOfLines:2];
	[allDayLabel setFrame:CGRectMake(15, allWeekTimeImageView.frame.size.height/2 - 40/2, 30, 40)];
	[allWeekTimeImageView insertSubview:allDayLabel atIndex:0];
    
    
    if ( IS_IOS_7 == YES && ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) {
        
        [allDayLabel setTextColor:[UIColor whiteColor]]; //White for Picture Calendar
    }
    UIImage *currentTimeImage;
    
    
    if (IS_IPAD)
    {
         currentTimeImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CurrentTimeSrip_ipad" ofType:@"png"]];
    }else
    {
         currentTimeImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CurrentTimeSrip" ofType:@"png"]];
    }
	
    
    
    
	currentTimeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(27, 0, currentTimeImage.size.width, currentTimeImage.size.height)];
	[currentTimeImageView setImage:currentTimeImage];
	[currentTimeImageView setTag:2177];
	[weekTimeImageView addSubview:currentTimeImageView];
	
	loopStarter = 0;
	loopStarterAllDay = 0;
	
	formater = [[NSDateFormatter alloc] init];
	[formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
	
	//[weekTimeScrollView setContentSize:CGSizeMake(320, 1012)]; //1012
	appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	
    
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];

    //NSLog(@"appDelegate.currentVisibleDate #1 = %@", appDelegate.currentVisibleDate);
    
    if(!appDelegate.currentVisibleDate || [appDelegate.currentVisibleDate isEqualToString:@""]) {
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setLocale:[NSLocale currentLocale]];
        [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        
        NSDateComponents *setTimeToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:destinationDate];
        [setTimeToDayBeginning setHour:0];
        [setTimeToDayBeginning setMinute:0];
        [setTimeToDayBeginning setSecond:0];
        
        NSDate *currentVisibleDate_Date = [calendar dateFromComponents:setTimeToDayBeginning];
        
        //Steve - new currentVisibleDate starting at 12AM (beginning of day)
        [appDelegate setCurrentVisibleDate:[formater stringFromDate:currentVisibleDate_Date]];
        
        //NSLog(@"appDelegate.currentVisibleDate #11 = %@", appDelegate.currentVisibleDate);
        
	}
    
    //NSLog(@"appDelegate.currentVisibleDate #2 = %@", appDelegate.currentVisibleDate);
    
	weekNumberLabel.text = [self createHeaderString];
	
	NSInteger dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
	NSDate *currentVisDate = [formater dateFromString:appDelegate.currentVisibleDate];
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 1)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(7 - dayOfWeek)];
    }
    else{ //Mon

        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 2)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(8 - dayOfWeek)];
    }
	

 
    //Steve - commented.  Moved to viewWillAppear
	//[self addRefreshControles];
	//[self showCurrentTime];
    
    //Steve commented - duplicates events after I added EKEventStoreChangedNotification
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"EventNotification" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViews) name:@"EventNotification" object:nil];
    
    //Steve - notifies if events change
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViews) name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    
    
}


//Steve added - refreshes views in case Calendar was changed on iCal & user leaves app & comes back
- (void) refreshViews{
    //NSLog(@"refreshViews -> Daily");
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(refreshViewsAfterDelay) userInfo:nil repeats:NO];
 
}

//Steve - sets delay on refreshViews so in iOS 6 data can load first, otherwise in iOS6 it duplicates the data
-(void) refreshViewsAfterDelay{
    //NSLog(@"refreshViewsAfterDelay -> Daily");
    
    isRefresh = YES;
    
    [self addRefreshControles];
	[self showCurrentTime];
    
    isRefresh = NO;
}


 

#pragma mark -
#pragma mark Touch Methods
#pragma mark -

//Steve added
//detects swipe to Right, then moves to Previous day Calendar
- (void) handleSwipeRight:(UISwipeGestureRecognizer *) sender
{
    // moves to the Previous Day
    [self previousButton_Clicked:(id) sender];
    
    // core animation to make view appear to be moving to Left
    CATransition* transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.duration = 0.25;
    [weekTimeScrollView.layer addAnimation:transition forKey:@"push-transition"];
    
}

//Steve added
//detects swipe to Left, then moves screen to Next Calendar day
- (void) handleSwipeLeft:(UISwipeGestureRecognizer *) sender
{
    //detects swipes to the left, then moves to Next day
    [self nextButton_Clicked:(id) sender];
    
    // core animation to make view appear to be moving to Right
    CATransition* transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    transition.duration = 0.25;
    [weekTimeScrollView.layer addAnimation:transition forKey:@"push-transition"];
    
}

//Steve added
//Handles Taps on the "dayTimeScrollView" so that a tap on the screen removes the popup
- (void)handleTap:(UITapGestureRecognizer *)sender
{
    [self hidePopups];
    
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	
	if (!lastSelectedEvent || ([weekAppDataObjArray count] == 0 && [allDayWeekObjArray count] == 0)) {
		return;
	}
	[self hidePopups];
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
	[self hidePopups];
    
}


#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -


-(void)resizeToInitAllDayView {
    //NSLog(@"resizeToInitAllDayView");
    
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2];
	
	[allWeekTimeImageView setFrame:CGRectMake(allWeekTimeImageView.frame.origin.x, allWeekTimeImageView.frame.origin.y, allWeekTimeImageView.frame.size.width, allWeekTimeImageView.frame.size.height)]; //was 59 steve changed

	[allDayLabel setFrame:CGRectMake(15, allWeekTimeImageView.frame.size.height/2 - 40/2, 30, 40)];
	
    
    if(IS_IPAD)
    {
        [weekTimeScrollView setFrame:CGRectMake(weekTimeScrollView.frame.origin.x,  105, weekTimeScrollView.frame.size.width, weekTimeScrollView.frame.size.height)];
        [allDayDiviserImageView setFrame:CGRectMake(allDayDiviserImageView.frame.origin.x, 105, allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height)];
    }
    else //iPhone
    {
        [weekTimeScrollView setFrame:CGRectMake(weekTimeScrollView.frame.origin.x,  85, weekTimeScrollView.frame.size.width, weekTimeScrollView.frame.size.height)];
        [allDayDiviserImageView setFrame:CGRectMake(allDayDiviserImageView.frame.origin.x, 82, allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height)];
    }
	
	[UIView commitAnimations];
}

-(IBAction) allDayButton_Clicked:(UIButton* )sender {
    //NSLog(@"allDayButton_Clicked");
    
	[self hidePopups];
    
    //Steve
    if (IS_IPAD) {
        return;
    }
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2];
	
	if (sender.frame.size.height  > 45) {
		[self resizeToInitAllDayView];
		[UIView commitAnimations];
		return;
	}
	if ([allDayWeekObjArray count] <= 2) {
		[UIView commitAnimations];
		return;
	}
	
	//	NSInteger numberOfViews = ([[allWeekTimeImageView subviews] count] - 1);
	
	CGFloat allDayHeight = allDayViewHeight + 2 + 2;
	CGFloat heightDifference = allDayHeight - allWeekTimeImageView.frame.size.height;
	
	if (allDayViewHeight > 0) {
		[allWeekTimeImageView setFrame:CGRectMake(allWeekTimeImageView.frame.origin.x, allWeekTimeImageView.frame.origin.y, allWeekTimeImageView.frame.size.width, allDayHeight)];
	}
    
	
    [allDayLabel setFrame:CGRectMake(15, allWeekTimeImageView.frame.size.height/2 - 40/2, 30, 40)];
    [allDayDiviserImageView setFrame:CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y + heightDifference, allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height)];
    [weekTimeScrollView setFrame:CGRectMake(weekTimeScrollView.frame.origin.x, weekTimeScrollView.frame.origin.y + heightDifference, weekTimeScrollView.frame.size.width, weekTimeScrollView.frame.size.height)];
	
	
	
	[UIView commitAnimations];
}

-(void)detailButton_Clicked:(UIButton *) sender
{
    //NSLog(@"detailButton_Clicked");
	//NSLog(@"Tag Value %d", sender.tag);
	
	if ([[appDelegate.navigationController topViewController] isKindOfClass:[ViewAppointmentScreen class]]) {
		return;
	}
	
	AppointmentsDataObject* appObj;
	if (sender.tag < Week_Allday_Event_TAG) {
		
		//NSLog(@"%@",weekAppDataObjArray);
		
		appObj = [weekAppDataObjArray objectAtIndex:sender.tag - Week_Event_TAG];
	}else {
		appObj = [allDayWeekObjArray objectAtIndex:(sender.tag - Week_Allday_Event_TAG)];
	}
	
	ViewAppointmentScreen *viewAppointmentScreen = [[ViewAppointmentScreen alloc] initWithAppObject:appObj];
    
    
    //Steve - also removed "NavigationNew"
    if (IS_IPAD) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ViewAppointmentStoryboard" bundle:nil];
        UINavigationController *navigationController = [sb instantiateViewControllerWithIdentifier:@"ViewAppointmentStoryboard"];
        ViewAppointmentScreen *viewAppointmentScreen = navigationController.viewControllers[0];
        viewAppointmentScreen.appDataObj = appObj; //Sends data to appDataObj
        
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    else{ //iPhone
        [self.navigationController pushViewController:viewAppointmentScreen animated:YES];
    }
    
}


-(void)eventButton_Clicked:(UIButton *)sender
{
    //NSLog(@"eventButton_Clicked");
    
	//NSLog(@"Tag Value %d", sender.tag);
    
	AppointmentsDataObject *aAppobj;
    
	[self hidePopups];
	
	lastSelectedEvent = sender;
	//NSLog(@"lastSelectedEvent = %@", lastSelectedEvent);
    
    
	if (sender.tag < Week_Allday_Event_TAG) {
       		aAppobj = [weekAppDataObjArray objectAtIndex:(sender.tag - Week_Event_TAG)];
	}else {
		aAppobj = [allDayWeekObjArray objectAtIndex:(sender.tag - Week_Allday_Event_TAG)];
	}
    
    ////////////////////////////////////////////////////////////
    //////////////////// Steve added Finds Handwriting ///////////
    //////////////////////////////////////////////////////////
    
    //[aAppobj.appointmentTitle isEqualToString:@"InFocus Handwritten Event"] ||
    if ( [aAppobj.appointmentType isEqualToString:@"H"] )
    {
        //NSLog(@"Handwritten Event");
        [aAppobj setAppointmentType:@"H"];
        
        sqlite3_stmt *selectAppointment_Stmt = nil;
        
        if(selectAppointment_Stmt == nil && appDelegate.database) {
            
            //NSLog(@"event.eventIdentifier = %@",event.eventIdentifier);
            
            NSString *Sql = [[NSString alloc] initWithFormat: @"Select a.AppointmentID, a.AppointmentBinary, a.AppointmentBinaryLarge  From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", aAppobj.relation_Field1];
           // NSString *Sql = [[NSString alloc] initWithFormat: @"Select a.AppointmentID, a.CalendarID, a.LocationID, a.StartDateTime, a.DueDateTime, a.Repeat, a.Alert, a.ShortNotes, a.AppointmentTitle, a.AppointmentType, a.AppointmentBinary, a.AppointmentBinaryLarge, a.textLocName  From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", aAppobj.relation_Field1];
            
            //event.eventIdentifier
            
            //NSLog(@"Day query : %@", Sql);
            
            if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
            {
                NSLog(@"SQL FAILED");
                NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
                
            }
            else
            {
                //NSLog(@"SQL OK");
                
                while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
                {
                    //NSLog(@"SQL WHILE");
                    [aAppobj setAppointmentID:sqlite3_column_int(selectAppointment_Stmt,0)];
                    NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 1);
                    NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 1) length:blobLength];
                    [aAppobj setAppointmentBinary:binaryData];
                    
                    //Steve - appointmentBinarylarge found on appointmentPopupView class.  Helps dayview save time loading both images 
                    
                    //blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 11);
                   // NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 11) length:blobLength];
                   // [aAppobj setAppointmentBinaryLarge:binaryDataLarge];
                    
                    // NSLog(@"appDataObject.appointmentBinaryLarge : %@", aAppobj.appointmentBinaryLarge);
                    // NSLog(@"appDataObject.appointmentBinaryLarge : %@", aAppobj.appointmentBinary);
                    
                }//while end
                
            }//else end
            sqlite3_finalize(selectAppointment_Stmt);
            selectAppointment_Stmt = nil;
            
            //if no Handwritting in database, then it's on another device
          // [aAppobj setAppointmentTitle:[NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType]];
            //[aAppobj setAppointmentType:@"T"];
            
            // //[aAppobj.appointmentTitle isEqualToString:@"InFocus Handwritten Event"] ||
        }
        else
        {
            NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
        
    } //if End if ([aAppobj.appointmentTitle isEqualToString:@"InFocus Handwritten Event" ])
    else
    {
        [aAppobj setAppointmentType:@"T"];
    }
    
    /////////////////////////////////////////////////////////////////////////
    //////////////////////// SQL Query - Find Handwriting END ///////////////////
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////// Steve End /////////////////////////////////
    
	
	//UIColor *firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.85];
	//UIColor *secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.75];
    
    UIColor *firstColor;
    UIColor *secondColor;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( IS_IOS_7 == YES && ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Picture Calendar
        
        firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:1.0];
        secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.90];
    }
    else{ //Classic Calendar or iOS 6
        
        firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
        secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
    }

	
	[sender setGradientWithHiColor:firstColor lowColor:secondColor forHighlighted:YES];
	
	
	CGRect visibleRect;
	visibleRect.origin = weekTimeScrollView.contentOffset;
	visibleRect.size = weekTimeScrollView.bounds.size;
	CGFloat scale = (CGFloat) 1.0 / weekTimeScrollView.zoomScale;
	
	float theScale = 1.0 / scale;
	visibleRect.origin.x *= theScale;
	visibleRect.origin.y *= theScale;
	visibleRect.size.width *= theScale;
	visibleRect.size.height *= theScale;
	
	appointmentPopupView *appPopup;
	
	if (sender.tag < Week_Allday_Event_TAG) {
        
        // Steve - Moves popup up by 44 if no All Day events show
        if (allDayWeekObjArray.count == 0) { 
            
            CGRect newRect;
            newRect = sender.frame;
            newRect.origin.y = sender.frame.origin.y - 44;
            
            appPopup = [[appointmentPopupView alloc] initWithAppObject:aAppobj buttonRect:newRect visibleRect:visibleRect andParentVC:self withTag:sender.tag isAllDay:NO isAllDayShowing:NO];
        }
		else{
            
            appPopup = [[appointmentPopupView alloc] initWithAppObject:aAppobj buttonRect:sender.frame visibleRect:visibleRect andParentVC:self withTag:sender.tag isAllDay:NO isAllDayShowing:YES];
        }
		
	}else {
		appPopup = [[appointmentPopupView alloc] initWithAppObject:aAppobj buttonRect:sender.frame visibleRect:visibleRect andParentVC:self withTag:sender.tag isAllDay:YES isAllDayShowing:YES];
	}
	
	[appPopup setTag:9891];
	[appPopup setUserInteractionEnabled:YES];
	[self.view addSubview:appPopup];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[appPopup setAlpha:1];
	[UIView commitAnimations];
    
    //Steve added
    //adds a UITapGestureRecognizer to the "dayTimeScrollView" so that the popups can be removed if screen(scrollView) is tapped
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [weekTimeScrollView addGestureRecognizer:tap];
    
    
}

-(void)removeView:(UIView *) sender {
    //NSLog(@"removeView");
	UIView *popupView;
	if (lastSelectedEvent.tag < Week_Allday_Event_TAG) {
		popupView =  [self.view viewWithTag:9891];
	}else {
		popupView =  [self.view viewWithTag:9891];
	}
	[popupView removeFromSuperview];
}

-(IBAction) nextButton_Clicked:(UIButton *) sender {
	[self hidePopups];
     
    
	[formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
	NSInteger dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
	
	appDelegate.currentVisibleDate = @"";
	appDelegate.currentVisibleDate = [formater stringFromDate: [tempDate dateByAddingDays:(8 - dayOfWeek)]];
	
	NSDate *currentVisDate = [formater dateFromString:appDelegate.currentVisibleDate];
	weekNumberLabel.text = [self createHeaderString];
	
	dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 1)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(7 - dayOfWeek)];
    }
    else{ //Mon
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 2)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(8 - dayOfWeek)];
    }
    

	
	[self addRefreshControles];
	
	//	[self getAppointmentsFromDate:[formater stringFromDate:currentStartDateofWeek] toDate:[formater stringFromDate:currentEndDateofWeek]];

}

-(IBAction) previousButton_Clicked:(UIButton *) sender {
	[self hidePopups];
	
	[formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
    
    
	NSInteger dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
	appDelegate.currentVisibleDate = @"";
	
	appDelegate.currentVisibleDate = [formater stringFromDate: [tempDate dateBySubtractingDays:(7 + dayOfWeek-1)]];
	NSDate *currentVisDate = [formater dateFromString:appDelegate.currentVisibleDate];
	
	weekNumberLabel.text = [self createHeaderString];
	
	dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 1)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(7 - dayOfWeek)];
    }
    else{ //Mon
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 2)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(8 - dayOfWeek)];
    }
	

	
	[self addRefreshControles];
	
	//	[self getAppointmentsFromDate:[formater stringFromDate:currentStartDateofWeek] toDate:[formater stringFromDate:currentEndDateofWeek]];
    
}


-(void)autoScrollToEvent {
    //NSLog(@"autoScrollToEvent");
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
	CGFloat startY;
	CGFloat	sagmentGapY	= 40.25;
	CGFloat initialGapY = 11;
	CGFloat minutesSpace = 0;
	
	if ([weekAppDataObjArray count] > 0) {
		int i = 0;
		AppointmentsDataObject *aAppobj = [weekAppDataObjArray objectAtIndex:i];
		
		//NSLog(@"Time : %@", [[[aAppobj dueDateTime]  substringFromIndex:11] substringToIndex:5]);
		
		while ([[[[aAppobj dueDateTime]  substringFromIndex:11] substringToIndex:5] isEqualToString:@"00:00"]) {
			i++;
			if ([weekAppDataObjArray count] >= i) {
				break;
			}
			aAppobj = [weekAppDataObjArray objectAtIndex:i];
		}
		
		[formater setDateFormat:@"yyyy-MM-dd"];
		if (![[formater dateFromString:[[aAppobj startDateTime] substringToIndex:10]] compare:[formater dateFromString:[appDelegate.currentVisibleDate substringToIndex:10]]] == NSOrderedSame) {
			
			minutesSpace = ([[[[aAppobj startDateTime] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
			startY = (([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue]) * sagmentGapY) + initialGapY + minutesSpace + ([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue] -1);
			
		}else {
			minutesSpace = ([[[[aAppobj startDateTime] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
			startY = (([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue]) * sagmentGapY) + initialGapY + minutesSpace + ([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue] -1);
		}
	}
	else
	{
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		minutesSpace = ([[[[formater stringFromDate:destinationDate] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
		startY = (([[[formater stringFromDate:destinationDate] substringWithRange:NSMakeRange(11, 12)] floatValue]) * sagmentGapY) + initialGapY + minutesSpace + ([[[formater stringFromDate:destinationDate] substringWithRange:NSMakeRange(11, 12)] floatValue] -1);
        //		//NSLog(@"DateTime %@ %@", [[formater stringFromDate:destinationDate] substringWithRange:NSMakeRange(11, 12)], destinationDate);
	}
    //Steve added
    //controls the animation for the scroll
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationDelay:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    //Steve changed was: animated:YES
    //This scrolls the dayTimeScrollView
    [weekTimeScrollView scrollRectToVisible:CGRectMake(weekTimeScrollView.frame.origin.x, startY - 25, weekTimeScrollView.frame.size.width, weekTimeScrollView.frame.size.height)  animated:NO];
    [UIView commitAnimations];
}


#pragma mark -
#pragma mark Date Manipulation Methods
#pragma mark -

- (void) setDayLabels
{
    //NSLog(@"setDayLabels");
    
    /*
    sunDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    monDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    tueDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    wedDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    thuDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    friDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    satDayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
     */
    
    
	NSDate *tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
	NSInteger dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
    
    
    //Steve - determines what day week starts. i.e. Europe is Mon & USA is Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        NSDate *firstDateOfWeek = [tempDate dateBySubtractingDays:(dayOfWeek - 1)]; //Sun
        sunDayLabel.text = [NSString stringWithFormat:@"Sun %d", [firstDateOfWeek day] ] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        monDayLabel.text = [NSString stringWithFormat:@"Mon %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        tueDayLabel.text = [NSString stringWithFormat:@"Tue %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        wedDayLabel.text = [NSString stringWithFormat:@"Wed %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        thuDayLabel.text = [NSString stringWithFormat:@"Thu %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        friDayLabel.text = [NSString stringWithFormat:@"Fri %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        satDayLabel.text = [NSString stringWithFormat:@"Sat %d", [firstDateOfWeek day]] ;
    }
    else{ //Mon
        

        NSDate *firstDateOfWeek = [tempDate dateBySubtractingDays:(dayOfWeek - 2)]; //Mon
        sunDayLabel.text = [NSString stringWithFormat:@"Mon %d", [firstDateOfWeek day] ] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        monDayLabel.text = [NSString stringWithFormat:@"Tue %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        tueDayLabel.text = [NSString stringWithFormat:@"Wed %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        wedDayLabel.text = [NSString stringWithFormat:@"Thu %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        thuDayLabel.text = [NSString stringWithFormat:@"Fri %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        friDayLabel.text = [NSString stringWithFormat:@"Sat %d", [firstDateOfWeek day]] ;
        
        firstDateOfWeek = [firstDateOfWeek  dateByAddingDays:1];
        satDayLabel.text = [NSString stringWithFormat:@"Sun %d", [firstDateOfWeek day]] ;
    }
    

}

- (NSString *)createHeaderString {
    //NSLog(@"createHeaderString");
    
    NSString *str = @"";
    NSDate *tempDate;
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
        
    }
    else{ //Mon
        
        tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
        tempDate = [tempDate dateByAddingDays:1];
    }
    
    
    
    str = [str stringByAppendingFormat:@"Week %d, ",[tempDate week]];
    
    [formater setDateFormat: @"MMMM"];
    str = [str stringByAppendingFormat:@"%@",[formater stringFromDate:tempDate]];
    
    [formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
    
    str = [str stringByAppendingFormat:@" %d", [[appDelegate.currentVisibleDate substringWithRange:NSMakeRange(0, 4)] intValue]];
    NSArray *arr_temp = [str componentsSeparatedByString:@" "];
    currentWeekNumber = [arr_temp objectAtIndex:1];


    
	[self setDayLabels];
	
	return str;
}

- (int)weekDayForDate:(NSString *) date {
   // NSLog(@"weekDayForDate");
    
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:[formater dateFromString:date]];
	int weekday = [comps weekday];
	return weekday;
}

- (int)weekFromDate:(NSString *) date {
   // NSLog(@"weekFromDate");
    
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:[formater dateFromString:date]];
	int week = [comps week];
	return week;
}


-(int)compairDateWithoutTimeDate1:(NSDate *) dateOne date2:(NSDate *) dateTwo {
     //NSLog(@"compairDateWithoutTimeDate1");
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
	
	NSDateComponents *date1Components = [calendar components:comps
													fromDate: dateOne];
	NSDateComponents *date2Components = [calendar components:comps
													fromDate: dateTwo];
	
	dateOne = [calendar dateFromComponents:date1Components];
	dateTwo = [calendar dateFromComponents:date2Components];
	
	NSComparisonResult result = [dateOne compare:dateTwo];
    
	if (result == NSOrderedAscending) {
		return 1; // Date One is smaller then date two
	} else if (result == NSOrderedDescending) {
		return 3; // Date One is bigger then date two
	}  else {
		return 2; // Date One is equal date two
	}
}



- (NSString *)dateByAddingDays:(NSUInteger)days fromDate:(NSString *) date  {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.day = days;
	NSString *dateStr = [formater stringFromDate:[[NSCalendar currentCalendar] dateByAddingComponents:c toDate:[formater dateFromString:date] options:0]];
	
	return dateStr;
}

- (NSString *) dateBySubtractingDays:(NSInteger) days fromDate:(NSString *) date {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.day = - (days - 1);
	
	NSString *dateStr = [formater stringFromDate:[[NSCalendar currentCalendar] dateByAddingComponents:c toDate:[formater dateFromString:date] options:0]];
	
	return dateStr;
}

#pragma mark -
#pragma mark getData Methods
#pragma mark -

-(void)addRefreshControles {
    //NSLog(@"addRefreshControles");
    
    NSDate *startDateofWeek = currentStartDateofWeek;
    NSDate *endDateofWeek = currentEndDateofWeek;
    
    
    //NSLog(@"startDateofWeek = %@", startDateofWeek);
    //NSLog(@"startDateofWeek = %@", [startDateofWeek descriptionWithLocale:@"GMT"]);
    //NSLog(@"endDateofWeek = %@", endDateofWeek);
    //NSLog(@"endDateofWeek = %@", [endDateofWeek descriptionWithLocale:@"GMT"]);
    
    NSDate  *sundayDateForRecords;
    NSDate  *mondayDateForRecords;
    NSDate  *tuesdayDateForRecords;
    NSDate  *wednesdayDateForRecords;
    NSDate  *thursdayDateForRecords;
    NSDate  *fridayDateForRecords;
    NSDate  *saturdayDateForRecords;
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    if (startOfWeek == 1) { //Sun
        
        sundayDateForRecords = currentStartDateofWeek;
        mondayDateForRecords = [currentStartDateofWeek dateByAddingDays:1];
        tuesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:2];
        wednesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:3];
        thursdayDateForRecords = [currentStartDateofWeek dateByAddingDays:4];
        fridayDateForRecords = [currentStartDateofWeek dateByAddingDays:5];
        saturdayDateForRecords = [currentStartDateofWeek dateByAddingDays:6];

    }
    else{ //Mon
        
        mondayDateForRecords = currentStartDateofWeek;
        tuesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:1];
        wednesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:2];
        thursdayDateForRecords = [currentStartDateofWeek dateByAddingDays:3];
        fridayDateForRecords = [currentStartDateofWeek dateByAddingDays:4];
        saturdayDateForRecords = [currentStartDateofWeek dateByAddingDays:5];
        sundayDateForRecords = [currentStartDateofWeek dateByAddingDays:6];
    }
    

    
	[self removeOldData];
    
    ////////////////// Steve added //////////////////////////
    
    [formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
    [self getAppointmentsFromDate:[formater stringFromDate:startDateofWeek] toDate:[formater stringFromDate:endDateofWeek] andDayNumber:1 isAllDay:NO];
    
    
    if ([sundaysObjArray count] > 0)
    {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:sundaysObjArray andColumnNo:0 forDateStr:[sundayDateForRecords description]];
            [self.arrM_finalEvents addObject:sundaysObjArray];
        }
        else{ //Mon
            
            [self showData:sundaysObjArray andColumnNo:6 forDateStr:[sundayDateForRecords description]];
            [self.arrM_finalEvents addObject:sundaysObjArray];
        }

        
    }
    
    
    if ([mondaysObjArray count] > 0) {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:mondaysObjArray andColumnNo:1 forDateStr:[mondayDateForRecords description]];
            [self.arrM_finalEvents addObject:mondaysObjArray];
        }
        else{ //Mon
            
            [self showData:mondaysObjArray andColumnNo:0 forDateStr:[mondayDateForRecords description]];
            [self.arrM_finalEvents addObject:mondaysObjArray];
        }
        
    }
    
    
    if ([tuedaysObjArray count] > 0) {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:tuedaysObjArray andColumnNo:2 forDateStr:[tuesdayDateForRecords description]];
            [self.arrM_finalEvents addObject:tuedaysObjArray];
        }
        else{ //Mon
            
            [self showData:tuedaysObjArray andColumnNo:1 forDateStr:[tuesdayDateForRecords description]];
            [self.arrM_finalEvents addObject:tuedaysObjArray];
        }
        
    }
    
    
    if ([weddaysObjArray count] > 0) {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:weddaysObjArray andColumnNo:3 forDateStr:[wednesdayDateForRecords description]];
            [self.arrM_finalEvents addObject:weddaysObjArray];
        }
        else{ //Mon
            
            [self showData:weddaysObjArray andColumnNo:2 forDateStr:[wednesdayDateForRecords description]];
            [self.arrM_finalEvents addObject:weddaysObjArray];
        }
        
    }
    
    
    if ([thudaysObjArray count] > 0) {
       // NSLog(@"Arr->%@",[thudaysObjArray description]);
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:thudaysObjArray andColumnNo:4 forDateStr:[thursdayDateForRecords description]];
            [self.arrM_finalEvents addObject:thudaysObjArray];
        }
        else{ //Mon
            
            [self showData:thudaysObjArray andColumnNo:3 forDateStr:[thursdayDateForRecords description]];
            [self.arrM_finalEvents addObject:thudaysObjArray];
        }
    }
    
    
    if ([fridaysObjArray count] > 0) {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:fridaysObjArray andColumnNo:5 forDateStr:[fridayDateForRecords description]];
            [self.arrM_finalEvents addObject:fridaysObjArray];
        }
        else{ //Mon
            
            [self showData:fridaysObjArray andColumnNo:4 forDateStr:[fridayDateForRecords description]];
            [self.arrM_finalEvents addObject:fridaysObjArray];
        }
    }
    
    
    if ([satdaysObjArray count] > 0) {
        
        if (startOfWeek == 1) { //Sun
            
            [self showData:satdaysObjArray andColumnNo:6 forDateStr:[saturdayDateForRecords description]];
            [self.arrM_finalEvents addObject:satdaysObjArray];
        }
        else{ //Mon
            
            [self showData:satdaysObjArray andColumnNo:5 forDateStr:[saturdayDateForRecords description]];
            [self.arrM_finalEvents addObject:satdaysObjArray];
        }
        
        [formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
        
    }
    
    ////////////////// Steve end //////////////////////////
    
	
	[self showDataAllDay:allDayWeekObjArray andColumnNo:1 forDateStr:[appDelegate currentVisibleDate]];
    
    if (!isRefresh) 
        [self autoScrollToEvent];
    
    
    // ************************************************************************
    // ************ steve  added - Hides All Day if there are no Events ********
    // ************************************************************************

    if(IS_IPAD)
    {
        if (allDayWeekObjArray.count == 0){ //No AllDay Events
                                                    
            weekTimeScrollView.frame = CGRectMake(0, 105 - 43, 768, 820 + 43); //Moves up 44 & 44 longer
        }
        else{
            weekTimeScrollView.frame = CGRectMake(0, 105, 768, 820);//919
           
        }
        
        weekTimeImageView.frame = CGRectMake(0, 0, 768, 1024);
       
    }
    else //iPhone
    {
        
        if (IS_IPHONE_5){
            if (allDayWeekObjArray.count == 0){ //No AllDay Events
                
                weekTimeScrollView.frame = CGRectMake(0, 85 - 44, 320, 348 + 36 + 44); //Moves up 44 & 44 longer
            }
            else{
                weekTimeScrollView.frame = CGRectMake(0, 85, 320, 348 + 36);
            }
        }
        else{
            if (allDayWeekObjArray.count == 0){
                weekTimeScrollView.frame = CGRectMake(0, 85 - 44, 320, 260 + 36 + 44); //Moves up 44 & 44 longer
            }
            else{
                weekTimeScrollView.frame = CGRectMake(0, 85, 320, 260 + 36);
            }
        }
        
        weekTimeImageView.frame = CGRectMake(0, 0, 320, 1024);
    }
    
    
    //Steve added - ensures weekTimeImageView doesn't auto adjust.  Without this XCode adjust the scroll image based on the new scroll size
    //We need a constant scroll image to insure Events are on proper time on image
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];

    if (allDayWeekObjArray.count == 0) {
        
        allWeekTimeImageView.hidden = YES;
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
            
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                allDayDiviserImageView.hidden = YES;
                
                CGRect dividerFrame = CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y,
                                                 allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height);
                
                dividerFrame.size.height = 1.5; //Thinner
                dividerFrame.origin.y = 41; //moved divider up
                allDayDiviserImageView.frame = dividerFrame;
                allDayDiviserImageView.backgroundColor = [UIColor blackColor];
                [self.view bringSubviewToFront:allDayDiviserImageView];
            }
            else{ //Dark Theme
                
                 allDayDiviserImageView.hidden = YES;
            }
        }
        else{ //Photo Calendar
            
            allDayDiviserImageView.hidden = NO;
            
            
            if (IS_IPAD) {
                
                CGRect dividerFrame = CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y,
                                                 allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height);
                
                dividerFrame.size.height = 1.5; //Thinner
                dividerFrame.origin.y = 63; //moved divider up
                allDayDiviserImageView.frame = dividerFrame;
                
                NSLog(@"dividerframe -- %@",NSStringFromCGRect(dividerFrame));
            }
            else{ //iPhone
                
                CGRect dividerFrame = CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y,
                                                 allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height);
                
                dividerFrame.size.height = 1.5; //Thinner
                dividerFrame.origin.y = 41; //moved divider up
                allDayDiviserImageView.frame = dividerFrame;
                
                NSLog(@"dividerframe -- %@",NSStringFromCGRect(dividerFrame));
            }
            

        }
        
    }
    else{ //Has allDay events
        
        allWeekTimeImageView.hidden = NO;
        allDayDiviserImageView.hidden = NO;
        
        
        if (![sharedDefaults boolForKey:@"CalendarTypeClassic"]) { //Photo Calendar
            
            //Steve
            if (IS_IPAD) {
                
                CGRect dividerFrame = CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y,
                                                 allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height);
                
                dividerFrame.size.height = 1.5; //Thinner
                //dividerFrame.origin.y = 105;
                allDayDiviserImageView.frame = dividerFrame;
            }
            else{ //iPhone
                
                CGRect dividerFrame = CGRectMake(allDayDiviserImageView.frame.origin.x, allDayDiviserImageView.frame.origin.y,
                                                 allDayDiviserImageView.frame.size.width, allDayDiviserImageView.frame.size.height);
                
                dividerFrame.size.height = 1.5; //Thinner
                dividerFrame.origin.y = 82;
                allDayDiviserImageView.frame = dividerFrame;
            }
            
        }
        else if ([sharedDefaults boolForKey:@"CalendarTypeClassic"] && [sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245){
            
            if (IS_IOS_7) {
                
                //Add black line above text "All Day"
                UIView *blackLineView = [[UIView alloc]init];
                blackLineView.frame = CGRectMake(0, 0, 45, 1);
                blackLineView.backgroundColor = [UIColor lightGrayColor];
                [allWeekTimeImageView addSubview:blackLineView];

            }

        }

        
    }
    
    // ************ Steve End ******************************************
    

}
//Alok Added For Checking InFocus Handwritten Event
-(BOOL)IsHandwritingEvent:(NSString *) EventName{
    if([EventName length] >= 25)
    {
        if([[EventName substringToIndex:25] isEqualToString:@"InFocus Handwritten Event"])
            return YES;
        else return NO;
        
    }
    return NO;
}

//Alok Added For GetFrequency
-(NSString *)getfrequency:(int) freq:(int) interval{
    if(freq == 0)
        return @"Every Day";
    else if(freq == 1 && interval == 2)
        return @"Every 2 Week";
    else if(freq == 1)
        return @"Every Week";
    else if(freq == 2)
        return @"Every Month";
    else if(freq == 3)
        return @"Every Year";
    else return @"Never";
}

-(void)getAppointmentsFromDate:(NSString *) fromDate toDate:(NSString *) toDate andDayNumber:(NSInteger) dayNumber isAllDay:(BOOL) isallDay
{
   //NSLog(@"getAppointmentsFromDate");
    
    //NSLog(@"*****************************************");
    //NSLog(@"dayNumber ---------> %d", dayNumber);
    //NSLog(@"currentStartDateofWeek = %@", currentStartDateofWeek);
    //NSLog(@"fromDate = %@", fromDate);
    //NSLog(@"toDate = %@", toDate);
    
    
	//NSLog(@"Staer Date %@ End Date %@",  fromDate, toDate);
    
    ////////////////////////////////////////////////////////
    ////////////////////// Steve added //////////////////////
    /////////// / Fetching Event from Event Store (iCal) ////
    ////////////////////////////////////////////////////////
    
    //changes to systemTimeZone to show events from iCal.
    //Events from iCal are on systemTimeZone
    //Steve ********** Week view must stay on GMT time, otherwise, times shifts as you go to Day & Month view********
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (weekAppDataObjArray) {
		[weekAppDataObjArray removeAllObjects];
	}else {
		weekAppDataObjArray = [[NSMutableArray alloc] init];
	}
	
	if (allDayWeekObjArray) {
		[allDayWeekObjArray removeAllObjects];
	}else {
		allDayWeekObjArray = [[NSMutableArray alloc] init];
	}
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	//NSDate *selectedDate = [formatter dateFromString:appDelegate.currentVisibleDate]; //Steve commented
    NSDate *selectedDateStart = [formatter dateFromString:fromDate]; //Start date
    NSDate *selectedDateEnd = [formatter dateFromString:toDate];  //end Date
    
    //REMOVE TIME FROM DATE'S (todayDate & dueDate) -  Date-->String-->Date
    NSDateFormatter *dateFormatter99 = [[NSDateFormatter alloc] init];
    [dateFormatter99 setDateFormat:@"yyyy MM dd"]; //Remove the time part in Today and DueDate
    NSString *selectedDateStringSart = [dateFormatter99 stringFromDate:selectedDateStart];
    NSDate *selectedDateFinalStart = [dateFormatter99 dateFromString:selectedDateStringSart]; //Start date
    
    //REMOVE TIME FROM DATE (TODATE)
    [dateFormatter99 setDateFormat:@"yyyy MM dd"]; //Remove the time part in Today and DueDate
    NSString *selectedDateStringEnd = [dateFormatter99 stringFromDate:selectedDateEnd];
    NSDate *selectedDateFinalEnd = [dateFormatter99 dateFromString:selectedDateStringEnd]; //end date
    
    //startDate & endDate are set on Local time, but then adjusted for GMT timezone
    //NSDate *startDate11 = selectedDateFinalStart; //Steve commented
    //NSDate *startDate11 = [selectedDateFinalStart dateBySubtractingMinutes:1]; //Steve added - needed becuase of bug in Apple predicate search???
    //NSDate *endDate11 = [selectedDateFinalEnd dateByAddingTimeInterval:(60*60*24) - 1];  //calculates 1 second before end of day
    
    
    //Steve changed again.  Apple bug?  Floating times don't show unless you expand search for a few days.  The search results are
    //weeded out later for the current day.
    NSDate *startDate11 = [selectedDateFinalStart dateByAddingTimeInterval: -2*(60*60*24)]; //starts search 2 days before selected date
    NSDate *endDate11 = [selectedDateFinalEnd dateByAddingTimeInterval: 3*(60*60*24)]; //ends Search 2 days ahead of selected date
    
    //NSLog(@"*****************************************");
    //NSLog(@"startDate11 = %@", startDate11);
    //NSLog(@"startDate11 = %@", [startDate11 descriptionWithLocale:@"GMT"]);
    //NSLog(@"endDate11 = %@", endDate11);
    //NSLog(@"endDate11 = %@", [endDate11 descriptionWithLocale:@"GMT"]);
    

    
    //Steve Finds the Calendars to Show
    NSArray *calendarsToShowArray = [self findsCalendarsToShow]; //method brings back the calendars to show
    
    if (calendarsToShowArray.count == 0) { //exits if no calendars in array
        return;
    }
    
    NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:startDate11 endDate:endDate11 calendars:calendarsToShowArray];
    NSArray *events = [eventStore eventsMatchingPredicate:predicate];
    
    
   // NSLog(@"#1 events.count = %d",events.count);
    
    //Steve - changes back to GMT timeZone
    //Must change back otherwise events won't show properly
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    
    for(EKEvent *event in events)
    {
        
        //Steve - determines if event should be shown on week
       // BOOL isShowEvent =[self isEventForTheCurrentWeek:event.startDate];
        
        
       // if (isShowEvent) {
        //}
        
        AppointmentsDataObject* appDataObject = [[AppointmentsDataObject alloc] init];
        
        NSDate *eventStartDate = event.startDate;
        
        //Convert event.StartDate to Local Timezone
        NSDate *sourceDate = eventStartDate;
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        NSDate *eventStartDate_local = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
        
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        NSString *startDateString = [formater stringFromDate:eventStartDate_local];
        
        
        NSDate *eventEndDate = event.endDate;
        
        //Convert event.EndDate to Local Timezone
        NSDate *sourceDate2 = eventEndDate;
        NSTimeZone *sourceTimeZone2 = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone *destinationTimeZone2 = [NSTimeZone systemTimeZone];
        NSInteger sourceGMTOffset2 = [sourceTimeZone2 secondsFromGMTForDate:sourceDate2];
        NSInteger destinationGMTOffset2 = [destinationTimeZone2 secondsFromGMTForDate:sourceDate2];
        NSTimeInterval interval2 = destinationGMTOffset2 - sourceGMTOffset2;
        NSDate *eventEndDate_local = [[NSDate alloc] initWithTimeInterval:interval2 sinceDate:sourceDate2];
        
        NSString *endDateString = [formater stringFromDate:eventEndDate_local];
        
        [appDataObject setStartDateTime:startDateString];
        [appDataObject setDueDateTime:endDateString];
        [appDataObject setTimeZoneName:event.timeZone];//Steve
        [appDataObject setShortNotes:event.notes];
        //[appDataObject setRepeat:event.recurrenceRules];
        
        if([event.recurrenceRules count]>0){
            EKRecurrenceRule *rule = [event.recurrenceRules objectAtIndex:0];
            [appDataObject setRepeat:[self getfrequency:rule.frequency :rule.interval]];
        }else [appDataObject setRepeat:@"None"];
        
        //[appDataObject setAlert:event.alarms];
        
        if([event.alarms count]==1)
            [appDataObject setAlert:[self EKAlarmToStr:[event.alarms objectAtIndex:0]]];
        if([event.alarms count]==2){
            [appDataObject setAlert:[self EKAlarmToStr:[event.alarms objectAtIndex:0]]];
            [appDataObject setAlertSecond:[self EKAlarmToStr:[event.alarms objectAtIndex:1]]];
        }
        
        
        NSString * calName =[NSString stringWithFormat:@"%@~%@",event.calendar.title,event.calendar.calendarIdentifier];
        [appDataObject setCalendarName:calName];
        [appDataObject setCalendarColor:[UIColor colorWithCGColor:event.calendar.CGColor]];
        [appDataObject setCalID:[GetAllDataObjectsClass getCalendarId:event.calendar]];
        [appDataObject setTextLocName:event.location];
        [appDataObject setIsAllDay:event.isAllDay];
        [appDataObject setRelation_Field1:event.eventIdentifier];
        [appDataObject setAppointmentTitle:event.title];
        
        
        if ([self IsHandwritingEvent:appDataObject.appointmentTitle])
        {
            //NSLog(@"Handwritten Event");
            [appDataObject setAppointmentType:@"H"];
        }
        
        //*** Note - Handwritten Events Images get called when Event Button is clicked.  eventButton_Clicked
        
        
        /////////////////////////////////////////////////////////////////
        ///////////////// Builds the arrays to display /////////////////////
        /////////////////////////////////////////////////////////////////
        
        
        NSDateFormatter *dateFormatter88 = [[NSDateFormatter alloc]init];
        [dateFormatter88 setDateFormat:@"yyyy MM dd"]; //Remove the time part in Today and DueDate
        
        NSString *startDateofWeek_String = [dateFormatter88 stringFromDate:eventStartDate_local]; //date "eventStartDate_local" taken from above. event.startdate in local time
        NSDate *currentDateOfSearch = [dateFormatter88 dateFromString:startDateofWeek_String]; // //gets search date on event store as a date
        
        //currentStartDateofWeek
        
        NSDate  *sundayDateForRecords;
        NSDate  *mondayDateForRecords;
        NSDate  *tuesdayDateForRecords;
        NSDate  *wednesdayDateForRecords;
        NSDate  *thursdayDateForRecords;
        NSDate  *fridayDateForRecords;
        NSDate  *saturdayDateForRecords;
        
        
        //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
        NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
        
        if (startOfWeek == 1) { //Sun
            
            sundayDateForRecords = currentStartDateofWeek;
            mondayDateForRecords = [currentStartDateofWeek dateByAddingDays:1];
            tuesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:2];
            wednesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:3];
            thursdayDateForRecords = [currentStartDateofWeek dateByAddingDays:4];
            fridayDateForRecords = [currentStartDateofWeek dateByAddingDays:5];
            saturdayDateForRecords = [currentStartDateofWeek dateByAddingDays:6];
            
        }
        else{ //Mon
            
            mondayDateForRecords = currentStartDateofWeek;
            tuesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:1];
            wednesdayDateForRecords = [currentStartDateofWeek dateByAddingDays:2];
            thursdayDateForRecords = [currentStartDateofWeek dateByAddingDays:3];
            fridayDateForRecords = [currentStartDateofWeek dateByAddingDays:4];
            saturdayDateForRecords = [currentStartDateofWeek dateByAddingDays:5];
            sundayDateForRecords = [currentStartDateofWeek dateByAddingDays:6];

        }
        
        
        //NSLog(@"*****************************************");
        //NSLog(@"event.title = %@", event.title);
        //NSLog(@"event.startDate = %@", event.startDate);
        //NSLog(@"event.startDate = %@", [event.startDate descriptionWithLocale:@"GMT"]);
        //NSLog(@"event.startDate = %@", event.endDate);
        
        
        
        //Steve changed was code above that is commented
        // adds events to "all-day" if event is more than one day
        //Steve - TimeIntervalSinceDate show difference in seconds.  Converted to minutes then see if greater than 23 hours & 59 minutes(1,439 minutes)
        if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]])/60 >= 1439 ) || [appDataObject isAllDay]) {
            
            
            //Steve
            BOOL isEventStartDateInProperRange = [self isEventForTheCurrentWeek:event.startDate];
            BOOL isEventEndDateInProperRange = [self isEventForTheCurrentWeek:event.endDate];
            
            
            //Steve - makes sure the start & end date are in current week. The predicate search is bigger than the actual week
            if (isEventStartDateInProperRange || isEventEndDateInProperRange) {
                
                
                [allDayWeekObjArray addObject:appDataObject];
               // NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
               // NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
            }

        }
        //if (!appDataObject.isAllDay) { //Steve was this, change to "else".  All Day should not show on week day
        //Steve - TimeIntervalSinceDate show difference in seconds.  Converted to minutes then see if greater than 23 hours & 59 minutes(1,439 minutes)
        else{ // Steve change to "else".  All Day should not show on week day.
            if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]]) /60 >= 1439) || [appDataObject isAllDay]) {
            }else {
                [weekAppDataObjArray addObject:appDataObject];
                //NSLog(@"*****************************************");
                //NSLog(@"weekAppDataObjArray = %d", weekAppDataObjArray.count);
            }
            
            
            // -(int)compairDateWithoutTimeDate1:(NSDate *) dateOne date2:(NSDate *) dateTwo
            int intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:sundayDateForRecords] / 60.0)/60;
            int intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:sundayDateForRecords] / 60.0)/60;
            
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:sundayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:sundayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                //NSLog(@"*****************************************");
                //NSLog(@"Sunday");
                //if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) ||[appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [sundaysObjArrayAllDay addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }else {
                    [sundaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:mondayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:mondayDateForRecords] / 60.0)/60;
            
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:mondayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:mondayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                // NSLog(@"*****************************************");
                //NSLog(@"Monday");
                // if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) || [appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [mondaysObjArrayAllDay addObject:appDataObject];
                }else {
                    [mondaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:tuesdayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:tuesdayDateForRecords] / 60.0)/60;
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:tuesdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:tuesdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                //NSLog(@"*****************************************");
                //NSLog(@"Tuesday");
                //  if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) || [appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [tuedaysObjArrayAllDay addObject:appDataObject];
                }else {
                    [tuedaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:wednesdayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:wednesdayDateForRecords] / 60.0)/60;
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:wednesdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:wednesdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                //NSLog(@"*****************************************");
                // NSLog(@"Wednesday");
                // if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) || [appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [weddaysObjArrayAllDay addObject:appDataObject];
                }else {
                    [weddaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:thursdayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:thursdayDateForRecords] / 60.0)/60;
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:thursdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:thursdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                //NSLog(@"*****************************************");
                //NSLog(@"Thursday");
                // if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) || [appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [thudaysObjArrayAllDay addObject:appDataObject];
                }else {
                    [thudaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:fridayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:fridayDateForRecords] / 60.0)/60;
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:fridayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:fridayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {//Steve
                //NSLog(@"*****************************************");
                //NSLog(@"Friday");
                //if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) ||
                if([appDataObject isAllDay]) {
                    [fridaysObjArrayAllDay addObject:appDataObject];
                }else {
                    [fridaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
            
            intervalStart = ([[formater dateFromString:[appDataObject startDateTime]] timeIntervalSinceDate:saturdayDateForRecords] / 60.0)/60;
            intervalEnd = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:saturdayDateForRecords] / 60.0)/60;
            
            if ([self compairDateWithoutTimeDate1:currentDateOfSearch date2:saturdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //if ([self compairDateWithoutTimeDate1:currentStartDateofWeek date2:saturdayDateForRecords] == 2 || (intervalStart <0 && intervalEnd > 0)) {
                //NSLog(@"*****************************************");
                //NSLog(@"Saturday");
                // if((([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0)/60 >= 24) || [appDataObject isAllDay]) {
                if([appDataObject isAllDay]) {
                    [satdaysObjArrayAllDay addObject:appDataObject];
                }
                else
                {
                    [satdaysObjArray addObject:appDataObject];
                    //NSLog(@"appDataObject.appointmentTitle = %@", appDataObject.appointmentTitle);
                    //NSLog(@"appDataObject.startDateTime = %@", appDataObject.startDateTime);
                }
            }
            
        }
        
        
    } // for end
    
    
}


-(BOOL)isEventForTheCurrentWeek:(NSDate *) eventDate{
// Gets Dates without Time Component so they can be compared and determine if Event Date should be shown
    
    //Get start & End Date
    NSDate *startDate_Date = currentStartDateofWeek;
    NSDate *endDate_Date = currentEndDateofWeek;
    
    
    //NSLog(@"appDelegate.currentVisibleDate = %@", appDelegate.currentVisibleDate);
    //NSLog(@"currentVisibleDate_DateFormat = %@", currentVisibleDate_DateFormat);
    //NSLog(@"currentVisibleDate_DateFormat = %@", [currentVisibleDate_DateFormat descriptionWithLocale:@"GMT"]);
    
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[NSLocale currentLocale]];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    //[calendar setTimeZone:[NSTimeZone systemTimeZone]];
    
    //Current Event Date
    NSDateComponents *setEventDateToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:eventDate];
    
    [setEventDateToDayBeginning setHour:0];
    [setEventDateToDayBeginning setMinute:0];
    [setEventDateToDayBeginning setSecond:0];
    
    NSDate *eventDateResetTimeToBeginning = [calendar dateFromComponents:setEventDateToDayBeginning];
    
    //Start Date
    NSDateComponents *setStartDateToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:startDate_Date];
    [setStartDateToDayBeginning setHour:0];
    [setStartDateToDayBeginning setMinute:0];
    [setStartDateToDayBeginning setSecond:0];
    
    NSDate *startDateResetTimeToBeginning = [calendar dateFromComponents:setStartDateToDayBeginning];
    
    //End Date
    NSDateComponents *setEndDateToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:endDate_Date];
    [setEndDateToDayBeginning setHour:0];
    [setEndDateToDayBeginning setMinute:0];
    [setEndDateToDayBeginning setSecond:0];
    
    NSDate *endDateResetTimeToBeginning = [calendar dateFromComponents:setEndDateToDayBeginning];
    
    BOOL isDateInRange;
    
    //NSLog(@"selectedDateResetTimeToBeginning = %@", selectedDateResetTimeToBeginning);
    // NSLog(@"startDateResetTimeToBeginning = %@", startDateResetTimeToBeginning);
    // NSLog(@"endDateResetTimeToBeginning = %@", endDateResetTimeToBeginning);
    
    // Steve - Make sure Event is for correct week.  In the predicate put 2 days into the previous day & 3 days ahead since there is an Apple bug(?).
    //Steve - if SelectedDate == Start or End Date or ( EventDate >= StrtDate & SelectedDate <= EndDate ) Then Show Event
    if ([eventDateResetTimeToBeginning compare:endDateResetTimeToBeginning] == NSOrderedSame ||
        [eventDateResetTimeToBeginning compare:startDateResetTimeToBeginning] == NSOrderedSame   ||
        ( [eventDateResetTimeToBeginning compare:startDateResetTimeToBeginning] == NSOrderedDescending
         && [eventDateResetTimeToBeginning compare:endDateResetTimeToBeginning] == NSOrderedAscending)   )
    {
        
        isDateInRange = YES;
        
    }
    else{
        isDateInRange = NO;
    }
    
    
    return isDateInRange;
    
}

-(NSArray*)findsCalendarsToShow{
    
    NSMutableArray *calendarArray = [[NSMutableArray alloc]init];
    //self.calendarDictionary = [[NSMutableDictionary alloc]init];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Make an array of UserDefaults for Calendars that don't show
    NSArray *calendarsThatDontShowArray = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] copy]; //make  copy
    NSMutableArray *discardedItems = [NSMutableArray array]; //make an array of objects to be removed
    
    
    for (EKSource *source in eventStore.sources) {
        
        //[self.calendarArray removeAllObjects];
        
        /*
         NSLog(@" *********************************************");
         NSLog(@" source  = %@", source.title);
         NSLog(@" calendarsThatDontShowArray  = %@", calendarsThatDontShowArray);
         NSLog(@" *********************************************");
         */
        
        
        
        NSSet *calendars = [source calendarsForEntityType:EKEntityTypeEvent];
        
        
        for (EKCalendar *calendar in calendars) {
            
            /*
             NSLog(@" *********************************************");
             NSLog(@" Calendar source Title = %@", calendar.source.title);
             NSLog(@" Calendar Title = %@", calendar.title);
             NSLog(@" Calendar Type = %d", calendar.type);
             //NSLog(@" Calendar CGColor = %@", calendar.CGColor);
             //NSLog(@" Calendar allowsContentModifications = %d", calendar.allowsContentModifications);
             NSLog(@" Calendar calendarIdentifier = %@", calendar.calendarIdentifier);
             */
            
            
            
            if (calendarsThatDontShowArray.count == 0) { //if nothing in UserDefaults not to show, then show all
                
                [calendarArray addObject: calendar]; //builds Calendar Array
            }
            
            
            CalendarDataObject *calendarObject = [[CalendarDataObject alloc]init];
            
            [calendarObject setCalendarIdentifire:calendar.calendarIdentifier];
            
            for (NSString *calendarIdentifier in calendarsThatDontShowArray) { //iterate thru copy of array of UserDefaults CalendarsDoNotShow
                
                if ([calendarObject.calendarIdentifire isEqualToString:calendarIdentifier]){ //looks for items that Dont match
                    
                    [discardedItems addObject: calendar]; //builds array of items to be removed from Calendar Array
                    break;
                    
                }
                else{
                    
                    [calendarArray addObject: calendar]; //builds Calendar Array
                }
                
            }
            
        }
        
        
    }
    
    //NSLog(@" calendarArray 1 = %@", calendarArray);
    
    [calendarArray removeObjectsInArray:discardedItems]; //removes all discarded items
    
    // NSLog(@" discardedItems = %@", discardedItems);
    //NSLog(@" calendarArray after discarded items removed = %@", calendarArray);
    //NSLog(@" *********************************************");
    
    
    return calendarArray;
}


//ALok Gupta Added for convert Alarm to string
-(NSString *)EKAlarmToStr:(EKAlarm*)ekAlarm{
    NSTimeInterval interval = [ekAlarm relativeOffset];
    interval = abs(interval);
    if (interval>0)
    {  NSString *strTime = @"";
        if (interval == D_MINUTE * 5)
        {
            strTime = @"5 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 10)
        {
            strTime = @"10 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 15)
        {
            strTime = @"15 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 30)
        {
            strTime = @"30 Minutes Before";
            return strTime;
        }
        else if (interval == D_HOUR)
        {
            strTime = @"1 Hour Before";
            return strTime;
        }
        else if (interval == D_HOUR * 2)
        {
            strTime = @"2 Hour Before";
            return strTime;
        }
        else if (interval == D_DAY)
        {
            strTime = @"1 Day Before";
            return strTime;
        }
        else if (interval == D_DAY * 2)
        {
            strTime = @"2 Day Before";
            return strTime;
        }
    }
    else
    {
        NSDate *alertdate = [ekAlarm absoluteDate];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"]; //Steve comented - not international compatible
        [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];//Steve
        
        //Steve - EKAlarm is in GMT time zone. This displays converts it to Local time zone so it displays correct
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        NSString *alertDate =[dateFormatter stringFromDate:alertdate];
        return alertDate;
    }
    
    return @"None";
}


-(void) removeOldData {
   // NSLog(@"removeOldData");
    
	[self resizeToInitAllDayView];
	
	if (weekAppDataObjArray) {
		weekAppDataObjArray = [[NSMutableArray alloc] init];
	}
	if (allDayWeekObjArray) {
		allDayWeekObjArray = [[NSMutableArray alloc] init];
	}
	if (sundaysObjArray) {
		sundaysObjArray = [[NSMutableArray alloc] init];
	}
	if (mondaysObjArray) {
		mondaysObjArray = [[NSMutableArray alloc] init];
	}
	if (tuedaysObjArray) {
		tuedaysObjArray = [[NSMutableArray alloc] init];
	}
	if (weddaysObjArray) {
		weddaysObjArray = [[NSMutableArray alloc] init];
	}
	if (thudaysObjArray) {
		thudaysObjArray = [[NSMutableArray alloc] init];
	}
	if (fridaysObjArray) {
		fridaysObjArray = [[NSMutableArray alloc] init];
	}
	if (satdaysObjArray) {
		satdaysObjArray = [[NSMutableArray alloc] init];
	}
	
	if (sundaysObjArrayAllDay) {
		sundaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (mondaysObjArrayAllDay) {
		mondaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (tuedaysObjArrayAllDay) {
		tuedaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (weddaysObjArrayAllDay) {
		weddaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (thudaysObjArrayAllDay) {
		thudaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (fridaysObjArrayAllDay) {
		fridaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	if (satdaysObjArrayAllDay) {
		satdaysObjArrayAllDay = [[NSMutableArray alloc] init];
	}
	[self.arrM_finalEvents removeAllObjects];
	for (UIView *temp in [weekTimeImageView subviews]) {
		if (temp.tag != 2177) {
			[temp removeFromSuperview];
		}
	}
	
	for (UIView *temp in [allWeekTimeImageView subviews]) {
		if (temp.tag != 3223) {
			if (![temp isKindOfClass:[UIImageView class]]) {
				[temp removeFromSuperview];
			}
		}
	}
	loopStarter = 0;
	loopStarterAllDay = 0;
    
}

-(void)creatAppointmentSubViews
{
	//NSLog(@"creatAppointmentSubViews");
    
    int x = 0; //Steve test loop
    
	CGFloat		generalBtnWidth = 37.0;
	CGFloat		initialGapX		= 44.4;
	CGFloat		sagmentGapX		= 38.17;
	CGFloat		initialGapY		= 100;
	CGFloat		sagmentGapY		= 40.65;
	CGFloat		totalViewHeight	= initialGapY + (sagmentGapY * 25);
    //	CGFloat		totalViewWidth	= initialGapX + (sagmentGapX * 7);
	
	NSInteger	dayOfWeek		= 0;
	NSInteger	preDayOfWeek	= 0;
	
	for (UIView *subView in  [weekTimeImageView subviews])
	{
		[subView removeFromSuperview];
	}
	
	NSMutableArray *viewsOfSameSlot = [[NSMutableArray alloc] init];
	for(int i = 0; i < [weekAppDataObjArray count]; i++)
	{
		AppointmentsDataObject* appDataObject = [weekAppDataObjArray objectAtIndex:i];
		[formater setDateFormat:@"yyyy-MM-dd"];
		
		if ([[formater dateFromString:[[appDataObject startDateTime] substringToIndex:10]] compare:[formater dateFromString:[[currentStartDateofWeek description] substringToIndex:10]]] == NSOrderedAscending)
		{
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		}
		else
		{
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			dayOfWeek = [self weekDayForDate:[appDataObject startDateTime]];
			
			CGFloat timeInterval = ([[formater dateFromString:[appDataObject dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[appDataObject startDateTime]]] / 60.0);
			CGFloat minutesSpace = ([[[[appDataObject startDateTime] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
			CGFloat startY = (([[[appDataObject startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue] + 1.0 )* sagmentGapY) + initialGapY + minutesSpace;
			
			UIButton *appIndicatorButton = [[UIButton alloc] init];
			[appIndicatorButton setTag:i];
			[appIndicatorButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
			[appIndicatorButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            //			[appIndicatorButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
			
			[appIndicatorButton setBackgroundColor:[[appDataObject calendarColor] colorWithAlphaComponent:0.7]];
			
			[appIndicatorButton.layer setCornerRadius:3.0];
			appIndicatorButton.layer.masksToBounds = YES;
			appIndicatorButton.layer.borderColor = [UIColor purpleColor].CGColor;
			appIndicatorButton.layer.borderWidth = 1.5;
			
			if ([[appDataObject appointmentType] isEqualToString:@"H"] && appDataObject.appointmentBinary != nil) {
				
				UIImage *titleImage = [[UIImage alloc] initWithData:[appDataObject appointmentBinary]];
				UIImageView *titleImageView=[[UIImageView alloc] initWithImage:titleImage];
				[titleImageView setFrame:CGRectMake(3, 2, 31, 25)];
				[titleImageView.layer setCornerRadius:3.0];
				titleImageView.layer.masksToBounds = YES;
				titleImageView.layer.borderColor = [UIColor purpleColor].CGColor;
				titleImageView.layer.borderWidth = 0.5;
				
				[appIndicatorButton addSubview:titleImageView];
			}
			else {
				[appIndicatorButton setTitle:[appDataObject appointmentTitle] forState:UIControlStateNormal];
			}
			
			if (preDayOfWeek == dayOfWeek) {
				if ([viewsOfSameSlot count] > 0) {
					if ((startY + timeInterval * (sagmentGapY / 60.0)) > totalViewHeight)  {
						CGFloat heightInStrtDate = totalViewHeight - startY;
						CGFloat remainingHeight = (timeInterval * (sagmentGapY / 60.0)) - heightInStrtDate;
						
						[formater setDateFormat:@"yyyy-MM-dd"];
						int loopCount =  [[formater dateFromString:[[appDataObject dueDateTime] substringToIndex:10]] daysAfterDate:[formater dateFromString:[[appDataObject startDateTime] substringToIndex:10]]] + 1;
						[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
						
						for (int j = 0 ; j < loopCount; j++) {
							if (j == 0) {
                                
                                x=x+1; //Steve test loop
								
								UIButton *button = (UIButton*)[viewsOfSameSlot objectAtIndex:0];
								
								if (lroundf(startY - button.frame.origin.y) < lroundf(sagmentGapY/2.0)) {
									
									CGFloat currentWidth = generalBtnWidth / ([viewsOfSameSlot count] + 1);
									CGFloat currentX = ((UIButton*) [viewsOfSameSlot objectAtIndex:([viewsOfSameSlot count] - 1)]).frame.origin.x + currentWidth;
									
									[appIndicatorButton setFrame:CGRectMake(currentX, startY, currentWidth,  heightInStrtDate)];
									[viewsOfSameSlot addObject:appIndicatorButton];
									
									for (int i = 0; i < [viewsOfSameSlot count]; i++) {
										UIButton *slotButton = [viewsOfSameSlot objectAtIndex:i];
										[slotButton setFrame:CGRectMake([(UIButton*)[viewsOfSameSlot objectAtIndex:0]  frame].origin.x + i * currentWidth, slotButton.frame.origin.y, currentWidth, slotButton.frame.size.height)];
									}
								}
								else
								{
									[viewsOfSameSlot removeAllObjects];
									
									[appIndicatorButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1), startY, generalBtnWidth,            heightInStrtDate)];
									[viewsOfSameSlot addObject:appIndicatorButton];
								}
							}
							else if (j == loopCount-1)
							{
								
								NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject: appIndicatorButton];
								UIButton *tempButton = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
								
								if (remainingHeight > 0) {
									[tempButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1 + j) , initialGapY + sagmentGapY, generalBtnWidth, ((int)remainingHeight%(int)(totalViewHeight - sagmentGapY - initialGapY)))];
									
									//NSLog(@" %d ", ((int)remainingHeight%(int)totalViewHeight));
									
									[tempButton setTag:i];
									[tempButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
									[tempButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
									[tempButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:255.0/255.0 alpha:0.7]];
									[tempButton.layer setCornerRadius:3.0];
									tempButton.layer.masksToBounds = YES;
									tempButton.layer.borderColor = [UIColor purpleColor].CGColor;
									tempButton.layer.borderWidth = 0.5;
									
									[weekTimeImageView addSubview:tempButton];
								}
							}
							else {
								NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject: appIndicatorButton];
								UIButton *tempButton = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
								
								[tempButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1 + j) , initialGapY + sagmentGapY, generalBtnWidth, totalViewHeight - initialGapY - sagmentGapY)];
								
								[tempButton setTag:i];
								[tempButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
								[tempButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
								[tempButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:255.0/255.0 alpha:0.7]];
								[tempButton.layer setCornerRadius:3.0];
								tempButton.layer.masksToBounds = YES;
								tempButton.layer.borderColor = [UIColor purpleColor].CGColor;
								tempButton.layer.borderWidth = 0.5;
								
								[weekTimeImageView addSubview:tempButton];
							}
						}
					}
					else
					{
						UIButton *button = (UIButton*)[viewsOfSameSlot objectAtIndex:0];
						if (lroundf(startY - button.frame.origin.y) < lroundf(sagmentGapY/2.0))
						{
							CGFloat currentWidth = generalBtnWidth / ([viewsOfSameSlot count] + 1);
							CGFloat currentX = ((UIButton*) [viewsOfSameSlot objectAtIndex:([viewsOfSameSlot count] - 1)]).frame.origin.x + currentWidth;
							
							[appIndicatorButton setFrame:CGRectMake(currentX, startY, currentWidth, timeInterval * (sagmentGapY / 60.0))];
							[viewsOfSameSlot addObject:appIndicatorButton];
							
							for (int i = 0; i < [viewsOfSameSlot count]; i++)
							{
								UIButton *slotButton = [viewsOfSameSlot objectAtIndex:i];
								[slotButton setFrame:CGRectMake([(UIButton*)[viewsOfSameSlot objectAtIndex:0]  frame].origin.x + i * currentWidth, slotButton.frame.origin.y, currentWidth, slotButton.frame.size.height)];
							}
						}
						else
						{
							[viewsOfSameSlot removeAllObjects];
							
							[appIndicatorButton setFrame:CGRectMake(initialGapX + sagmentGapX * (dayOfWeek - 1), startY, generalBtnWidth, timeInterval * (sagmentGapY / 60.0))];
							[viewsOfSameSlot addObject:appIndicatorButton];
						}
					}
				}
			}
			else
			{
				[viewsOfSameSlot removeAllObjects];
				if ((startY + timeInterval * (sagmentGapY / 60.0)) > totalViewHeight)
				{
					CGFloat heightInStrtDate = totalViewHeight - startY;
					CGFloat remainingHeight = (timeInterval * (sagmentGapY / 60.0)) - heightInStrtDate;
					
					[formater setDateFormat:@"yyyy-MM-dd"];
					int loopCount =  [[formater dateFromString:[[appDataObject dueDateTime] substringToIndex:10]] daysAfterDate:[formater dateFromString:[[appDataObject startDateTime] substringToIndex:10]]] + 1;
					[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
					
					for (int j = 0 ; j < loopCount; j++)
					{
						if (j == 0)
						{
							[appIndicatorButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1), startY, generalBtnWidth,            heightInStrtDate)];
							[viewsOfSameSlot addObject:appIndicatorButton];
						}
						else if (j == loopCount-1)
						{
							
							NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject: appIndicatorButton];
							UIButton *tempButton = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
							
							if (remainingHeight > 0)
							{
								[tempButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1 + j) , initialGapY + sagmentGapY, generalBtnWidth, ((int)remainingHeight%(int)(totalViewHeight - sagmentGapY - initialGapY)))];
								[tempButton setTag:i];
								[tempButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
								[tempButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
								[tempButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:255.0/255.0 alpha:0.7]];
								[tempButton.layer setCornerRadius:3.0];
								tempButton.layer.masksToBounds = YES;
								tempButton.layer.borderColor = [UIColor purpleColor].CGColor;
								tempButton.layer.borderWidth = 0.5;
								
								[weekTimeImageView addSubview:tempButton];
							}
						}
						else
						{
							NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject: appIndicatorButton];
							UIButton *tempButton = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
							
							[tempButton setFrame:CGRectMake(initialGapX +  sagmentGapX * (dayOfWeek - 1 + j) , initialGapY + sagmentGapY, generalBtnWidth, totalViewHeight - initialGapY - sagmentGapY)];
							
							[tempButton setTag:i];
							[tempButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
							[tempButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
							[tempButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:255.0/255.0 alpha:0.7]];
							[tempButton.layer setCornerRadius:3.0];
							tempButton.layer.masksToBounds = YES;
							tempButton.layer.borderColor = [UIColor purpleColor].CGColor;
							tempButton.layer.borderWidth = 0.5;
							
							[weekTimeImageView addSubview:tempButton];
						}
					}
				}
				else
				{
					[appIndicatorButton setFrame:CGRectMake(initialGapX + sagmentGapX * (dayOfWeek - 1), startY, generalBtnWidth, timeInterval * (sagmentGapY / 60.0))];
					[viewsOfSameSlot addObject:appIndicatorButton];
				}
			}
			
			[appIndicatorButton.titleLabel setTextColor:[UIColor blueColor]];
			preDayOfWeek = dayOfWeek;
			[weekTimeImageView addSubview:appIndicatorButton];
		}
	}
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    //NSLog(@"****************** x = %d", x);
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -


- (void)showCurrentTime
{
    //NSLog(@"showCurrentTime");
    
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
	CGFloat startY;
	CGFloat	sagmentGapY	= 40.25;
	CGFloat initialGapY = 2;
	CGFloat minutesSpace = 0;
	
	[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	minutesSpace = ([[[[formater stringFromDate:destinationDate] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
	
	startY = (([[[formater stringFromDate:destinationDate] substringWithRange:NSMakeRange(11, 12)] floatValue]) * sagmentGapY) + initialGapY + minutesSpace + ([[[formater stringFromDate:destinationDate] substringWithRange:NSMakeRange(11, 12)] floatValue] -1);
	
	[currentTimeImageView setFrame:CGRectMake(currentTimeImageView.frame.origin.x, startY, currentTimeImageView.frame.size.width, currentTimeImageView.frame.size.height)];
}

-(void)hidePopups
{
	if ([self.view viewWithTag:9891])
	{
		if (lastSelectedEvent)
		{
			UIView *popupView;
			AppointmentsDataObject *aAppobj;
			if (lastSelectedEvent.tag < Week_Allday_Event_TAG)
			{
				aAppobj = [weekAppDataObjArray objectAtIndex:(lastSelectedEvent.tag - Week_Event_TAG)];
				popupView =  [self.view viewWithTag:9891];
			}
			else
			{
				aAppobj = [allDayWeekObjArray objectAtIndex:(lastSelectedEvent.tag - Week_Allday_Event_TAG)];
				popupView =  [self.view viewWithTag:9891];
			}
			
			//UIColor *firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
			//UIColor *secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
            
            UIColor *firstColor;
            UIColor *secondColor;
            
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            
            if ( IS_IOS_7 == YES && ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Picture Calendar
                
                firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:1.0];
                secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.90];
            }
            else{ //Classic Calendar or iOS 6
                
                firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
                secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
            }

            
			[lastSelectedEvent setGradientWithHiColor:firstColor lowColor:secondColor forHighlighted:NO];
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationDuration:0.4];
			[popupView setAlpha:0];
			[UIView commitAnimations];
			[self performSelector:@selector(removeView:) withObject:nil afterDelay:0.3];
            
            //Steve added: removes UITapGesture so that Events can be tapped
            [weekTimeScrollView removeGestureRecognizer:tap];
            
		}
	}
}

-(void)gotoToday {
	[self hidePopups];
	
	[formater setDateFormat: @"yyyy-MM-dd HH:mm:ss +0000"];
	[appDelegate setCurrentVisibleDate:[formater stringFromDate:[NSDate date]]];
	
    //	NSDate *tempDate = [formater dateFromString:appDelegate.currentVisibleDate];
	NSInteger dayOfWeek ;//= [self weekDayForDate:appDelegate.currentVisibleDate];
	
	NSDate *currentVisDate = [formater dateFromString:appDelegate.currentVisibleDate];
	
	weekNumberLabel.text = [self createHeaderString];
	
	dayOfWeek = [self weekDayForDate:appDelegate.currentVisibleDate];
	currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 1)];
	currentEndDateofWeek =  [currentVisDate dateByAddingDays:(7 - dayOfWeek)];
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    
    if (startOfWeek == 1) { //Sun
    
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 1)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(7 - dayOfWeek)];
    }
    else{ //Mon
        
        currentStartDateofWeek = [currentVisDate dateBySubtractingDays:(dayOfWeek - 2)];
        currentEndDateofWeek =  [currentVisDate dateByAddingDays:(8 - dayOfWeek)];
    }

    
    
	
	[self addRefreshControles];
}


//Aman's Added
//Aman's Added
-(void)eventToSpeak
{
    NSInteger totalApp_Week= 0;
    AppointmentsDataObject *objAppointment;
    
    internet_YES_NO = [self hasConnectivity];
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit | NSWeekCalendarUnit fromDate:destinationDate];
    NSInteger WeekNumber = [currentWeekNumber intValue];
    
    
    
    if(internet_YES_NO)
    {
        //        if(WeekNumber >=[components2 week])
        [self finalString];
        //        else
        //            return;
        int E_Number = [self numberofAppointments];
        int j =0;
        
        if([self.arrM_finalEvents count]!=0)
        {
            for(int k =0; k<[self.arrM_finalEvents count];k++)
            {
                for(int s =0;s<[[self.arrM_finalEvents objectAtIndex:k]count];s++)
                {
                    objAppointment = [[self.arrM_finalEvents objectAtIndex:k]objectAtIndex:s];
                    
                    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
                    [formate setDateStyle:NSDateFormatterFullStyle];
                    [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                    self.strtDate = [formate dateFromString:objAppointment.startDateTime];
                    self.endDate = [formate dateFromString:objAppointment.dueDateTime];
                    
                    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit|NSWeekCalendarUnit fromDate:self.strtDate];
                    if(WeekNumber==[components2 week])
                    {
                        if([components2 weekday]<=[components weekday])
                        {
                            
                            switch ([components weekday]) {
                                case 1:
                                    self.str_Week = @"Sunday";
                                    break;
                                case 2:
                                    self.str_Week = @"Monday";
                                    
                                    break;
                                case 3:
                                    self.str_Week = @"Tuesday";
                                    break;
                                case 4:
                                    self.str_Week = @"Wednesday";
                                    break;
                                case 5:
                                    self.str_Week = @"Thrusday";
                                    break;
                                case 6:
                                    self.str_Week = @"Friday";
                                    break;
                                case 7:
                                    self.str_Week = @"Saturday";
                                    break;
                                default:
                                    break;
                            }
                            
                            
                            if(E_Number>1)
                            {
                                if(j==0)
                                    self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointments remaining this week.,\n Here is a partial list. \n",E_Number];
                            }
                            else
                            {
                                if(j==0)
                                    
                                    self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointment remaining this week,",E_Number];
                            }
                            self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@,",self.str_Week];
                            
                            if([objAppointment.appointmentType isEqualToString:@"H"])
                                self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",@"Handwritten Appointment"];
                            else
                                self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",objAppointment.appointmentTitle];
                            
                            //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
                            [formate setLocale:[NSLocale currentLocale]];//Steve
                            [formate setDateStyle:NSDateFormatterNoStyle];//Steve
                            [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
                            
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@ To ",[formate stringFromDate:self.strtDate]];
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@.,\n",[formate stringFromDate:self.endDate]];
                            
                            j++;
                            totalApp_Week++;
                            
                        }
                        
                    }
                    
                    else if (WeekNumber>[components2 week])
                        
                    {
                        
                        
                        switch ([components weekday]) {
                            case 1:
                                self.str_Week = @"Sunday";
                                break;
                            case 2:
                                self.str_Week = @"Monday";
                                
                                break;
                            case 3:
                                self.str_Week = @"Tuesday";
                                break;
                            case 4:
                                self.str_Week = @"Wednesday";
                                break;
                            case 5:
                                self.str_Week = @"Thrusday";
                                break;
                            case 6:
                                self.str_Week = @"Friday";
                                break;
                            case 7:
                                self.str_Week = @"Saturday";
                                break;
                            default:
                                break;
                                
                        }
                        [formate setDateFormat:@"MMM, dd,yyyy"];
                        
                        NSDate *dateForRecords =  currentStartDateofWeek;
                        if(E_Number>1)
                        {
                            if(j==0)
                                self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointments for the week starting %@.,\n Here is a partial list. \n",E_Number,[formate stringFromDate:dateForRecords]];
                        }
                        else
                        {
                            if(j==0)
                                
                                self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointment for the week starting %@.,",E_Number,[formate stringFromDate:dateForRecords]];
                        }
                        
                        self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@,",self.str_Week];
                        
                        if([objAppointment.appointmentType isEqualToString:@"H"])
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",@"Handwritten Appointment"];
                        else
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",objAppointment.appointmentTitle];
                        
                        //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
                        [formate setLocale:[NSLocale currentLocale]];//Steve
                        [formate setDateStyle:NSDateFormatterNoStyle];//Steve
                        [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
                        
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@ To ",[formate stringFromDate:self.strtDate]];
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@.,\n",[formate stringFromDate:self.endDate]];
                        
                        j++;
                        totalApp_Week++;
                        
                    }
                    
                    else if (WeekNumber<[components2 week])
                        
                    {
                        
                        
                        switch ([components weekday]) {
                            case 1:
                                self.str_Week = @"Sunday";
                                break;
                            case 2:
                                self.str_Week = @"Monday";
                                
                                break;
                            case 3:
                                self.str_Week = @"Tuesday";
                                break;
                            case 4:
                                self.str_Week = @"Wednesday";
                                break;
                            case 5:
                                self.str_Week = @"Thrusday";
                                break;
                            case 6:
                                self.str_Week = @"Friday";
                                break;
                            case 7:
                                self.str_Week = @"Saturday";
                                break;
                            default:
                                break;
                                
                        }
                        [formate setDateFormat:@"MMMM, dd,yyyy"];
                        
                        NSDate *dateForRecords =  currentStartDateofWeek;
                        
                        if(E_Number>1)
                        {
                            if(j==0)
                                self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointments for the week starting %@.,\n Here is a partial list. \n",E_Number,[formate stringFromDate:dateForRecords]];
                        }
                        else
                        {
                            if(j==0)
                                
                                self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",You have %d appointment for the week starting %@.,",E_Number,[formate stringFromDate:dateForRecords]];
                        }
                        self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@,",self.str_Week];
                        
                        if([objAppointment.appointmentType isEqualToString:@"H"])
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",@"Handwritten Appointment"];
                        else
                            self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@",%@,",objAppointment.appointmentTitle];
                        
                        //[formate setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
                        [formate setLocale:[NSLocale currentLocale]];//Steve
                        [formate setDateStyle:NSDateFormatterNoStyle];//Steve
                        [formate setTimeStyle:NSDateFormatterShortStyle];//Steve
                        
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@ To ",[formate stringFromDate:self.strtDate]];
                        self.strTextToSpeak =(NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@" %@.,\n",[formate stringFromDate:self.endDate]];
                        
                        j++;
                        totalApp_Week++;
                        
                    }
                }
                
            }
            //NSLog(@"%@",self.strTextToSpeak);
            
            [[iSpeechViewControllerTTS sharedApplication]readText:[self callToSpeaktext:self.strTextToSpeak]];
        }
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:@"Text to Voice requires an internet connection. Please connect to the internet to use Text to Voice." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
        
        
        
    }
}
-(NSString *)callToSpeaktext:(NSString *)callingText
{
    
    NSArray *temp_String = [self.strTextToSpeak componentsSeparatedByString:@"\n"];
    
    
    NSMutableString *tempString = [NSMutableString stringWithString:@""];
    NSMutableString *appendingString = [NSMutableString stringWithString:@""];
    int lengthOfAppendString  = 0;
    int tempStringlength = 0;
    NSInteger tag =0;
    for(int i =0;i<[temp_String count];i++)
    {
        appendingString = [temp_String objectAtIndex:i];
        lengthOfAppendString = [[appendingString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]count];
        if((tempStringlength + lengthOfAppendString)<=50)
        {
            tempString = [NSMutableString stringWithString:                                    [tempString stringByAppendingFormat:@"%@,",[temp_String objectAtIndex:i]]];
            tempStringlength = [[tempString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]count];
        }
        else
        {
            tag =1;
            break;
        }
    }
    if(tag ==0)
    {
        tempString = [NSString stringWithString:[tempString stringByReplacingOccurrencesOfString:@" Here is a partial list." withString:@""]];
        
    }
    
    return tempString;
    
}


-(NSInteger)numberofAppointments
{
   // NSLog(@"numberofAppointments");
    
    int E_Number =0;
    AppointmentsDataObject *objAppointment;
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit| NSWeekCalendarUnit fromDate:destinationDate];
    NSInteger WeekNumber = [currentWeekNumber intValue];
    
    if([self.arrM_finalEvents count]!=0)
    {
        for(int k =0; k<[self.arrM_finalEvents count];k++)
        {
            for(int s =0;s<[[self.arrM_finalEvents objectAtIndex:k]count];s++)
            {
                objAppointment = [[self.arrM_finalEvents objectAtIndex:k]objectAtIndex:s];
                NSDateFormatter *formate = [[NSDateFormatter alloc] init];
                [formate setDateStyle:NSDateFormatterFullStyle];
                [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                self.strtDate = [formate dateFromString:objAppointment.startDateTime];
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit | NSWeekCalendarUnit fromDate:self.strtDate];
                if(WeekNumber==[components2 week])
                {
                    if([components2 weekday]<=[components weekday])
                    {
                        ++E_Number;
                    }
                    
                    
                }
                else if(WeekNumber>[components2 week])
                {
                    
                    ++E_Number;
                    
                    
                }
                else if(WeekNumber<[components2 week])
                {
                    
                    ++E_Number;
                    
                    
                }
                
            }
            
        }
        
    }
    
    return E_Number;
}

-(NSMutableString *)finalString
{
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDateFormatter *formate2 = [[NSDateFormatter alloc] init];
    
    [formate2 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    
    [formate2 setDateFormat:@"MMM,dd,yyyy"];
    
    self.str_todaysDate =[NSMutableString stringWithString:[formate2 stringFromDate:destinationDate]] ;
    
    //[formate2 setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
    [formate2 setLocale:[NSLocale currentLocale]];//Steve
    [formate2 setDateStyle:NSDateFormatterNoStyle];//Steve
    [formate2 setTimeStyle:NSDateFormatterShortStyle];//Steve
    
    // NSLog(@"%@",[formate2 stringFromDate:destinationDate]);
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit fromDate:destinationDate];
    NSInteger hour = [components hour];
    
    if(hour >= 0 && hour < 12)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good morning,"];
        //NSLog(@"Good morning,");
    }
    else if(hour >= 12 && hour < 18)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good afternoon,"];
        
        //  NSLog(@"Good afternoon,");
    }
    else if(hour >= 18 && hour < 24 )
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good evening,"];
        
        //  NSLog(@"Good evening,");
    }
    
    
    //    NSString *str_Week;
    //    NSString *str_Present_Past_Future;
    //
    //    if([destinationDate isTODO_Cal_Today:destinationDate])
    //        str_Present_Past_Future= @"Today is";
    //
    //    else if([destinationDate isTODO_Cal_Tomorrow:destinationDate])
    //        str_Present_Past_Future= @"Tomorrow is";
    //
    //    else if([destinationDate isTODO_Cal_Yesterday:destinationDate])
    //        str_Present_Past_Future= @"Yesterday was";
    //
    //    switch ([components weekday]) {
    //        case 1:
    //            str_Week = @"Sunday";
    //
    //            break;
    //        case 2:
    //            str_Week = @"Monday";
    //
    //            break;
    //        case 3:
    //            str_Week = @"Tuesday";
    //            break;
    //        case 4:
    //            str_Week = @"Wednesday";
    //            break;
    //        case 5:
    //            str_Week = @"Thrusday";
    //            break;
    //        case 6:
    //            str_Week = @"Friday";
    //            break;
    //        case 7:
    //            str_Week = @"Saturday";
    //            break;
    //        default:
    //            break;
    //    }
    
    //self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@ %@,",str_Present_Past_Future,str_Week];
    
    // self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@,",self.str_todaysDate];
    
    //NSLog(@"self.strTextToSpeak  %@",self.strTextToSpeak);
    return self.strTextToSpeak;
    
    
}

/*Connectivity testing code pulled from Apple's Reachability Example: http://developer.apple.com/library/ios/#samplecode/Reachability
 */
-(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}



-(void)showData:(NSArray *) withDataObj andColumnNo:(NSInteger) columnNmbr forDateStr:(NSString* ) forDateStr {
    //NSLog(@"************ showData *****************");
    
    NSArray *weekDataObjArrayLocal = [withDataObj copy]; //This is getting each day array (sunday...sat)
    
    //NSLog(@"weekDataObjArrayLocal.count = %d", [weekDataObjArrayLocal count]);
    //NSLog(@"columnNmbr = %d", columnNmbr);
    //NSLog(@"forDateStr = %@", forDateStr);
    //NSLog(@"withDataObj = %@", withDataObj.description);
    
    
	if ([weekDataObjArrayLocal count] <= 0) {
		return;
	}
    
    
	
	CGFloat		initialGapX;            //Starting gap x
	CGFloat		initialGapY;            //Starting gap y
	CGFloat		sagmentGapY;
	CGFloat		generalBtnWidth;        //total Height of Week pic
	CGFloat		generalScreenHeight;
    
    
    //Steve
    if (IS_IPAD) {
        
        generalBtnWidth		 = 101.25;
        initialGapX			 = 49 + (generalBtnWidth * columnNmbr) + (columnNmbr*0.75) + columnNmbr;// (columnNmbr*0.75) + columnNmbr adds gap between days
        initialGapY			 = 11.50;
        sagmentGapY			 = 40.50;
        generalScreenHeight  =  (24 * 50);//41.71
    }
    else{ //iPhone
        
        initialGapX			 = 47 + 38 * columnNmbr + columnNmbr;
        initialGapY			 = 11.50;
        sagmentGapY			 = 40.45;
        generalBtnWidth		 = 38;
        generalScreenHeight  =  (24 * 41.71);//(24 * 41.71)
    }
    
    //NSLog(@" generalScreenHeight =  %f", generalScreenHeight);
	
    
    NSInteger widthDiviser = 1;
    NSInteger widthDiviserPastValue = 1;
	NSMutableArray *nmberbtnAtTime = [[NSMutableArray alloc] init];
	NSMutableArray *nmberbtnAtTime2 = [[NSMutableArray alloc] init];
    int k = 0;
	
	if ([weekDataObjArrayLocal count] == 1) {
		[nmberbtnAtTime addObject:@"1"];
		[nmberbtnAtTime2 addObject:@"1"];
	}
    
    BOOL foundParent;
    
	for (int j = 1; j < [weekDataObjArrayLocal count]; j++) {
        // NSLog(@"for (int j = 1; j < [weekDataObjArrayLocal count]; j++) = %d", weekDataObjArrayLocal.objectAtIndex);
        
		foundParent = NO;
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		
		NSDate *baseStrtDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:j-1] startDateTime]];
		//NSLog(@"\nbaseStrtDate %@", baseStrtDate);
		NSDate *baseEndDate = [formater dateFromString:	[[weekDataObjArrayLocal objectAtIndex:j-1] dueDateTime]];
		//NSLog(@"\nbaseEndDate %@", baseEndDate);
		NSDate *curStartDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:j] startDateTime]];
		//NSLog(@"\ncurStartDate %@\n\n", curStartDate);
		
		if (([baseStrtDate compare:curStartDate] == NSOrderedAscending && [baseEndDate compare:curStartDate] == NSOrderedDescending) || ([baseStrtDate compare:curStartDate] == NSOrderedSame || [baseEndDate compare:curStartDate] == NSOrderedDescending)) {
			widthDiviser++;
			widthDiviserPastValue = widthDiviserPastValue>widthDiviser?widthDiviserPastValue:widthDiviser;
			if (j == ([weekDataObjArrayLocal count] - 1)) {
                [nmberbtnAtTime addObject:[NSString stringWithFormat:@"%d", widthDiviserPastValue]];
				[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
				[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
			}else {
				[nmberbtnAtTime2 addObject:@""];
			}
		}
		else {
			int tempL;
			for(tempL = j-1; tempL >= k; tempL--) {
				NSDate *prevStrtDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:tempL] startDateTime]];
				NSDate *prevEndDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:tempL] dueDateTime]];
				if (([prevStrtDate compare:curStartDate] == NSOrderedAscending && [prevEndDate compare:curStartDate] == NSOrderedDescending) || ([prevStrtDate compare:curStartDate] == NSOrderedSame || [prevEndDate compare:curStartDate] == NSOrderedDescending)) {
					foundParent = YES;
					if (j == ([weekDataObjArrayLocal count]-1)) {
						[nmberbtnAtTime addObject:[NSString stringWithFormat:@"%d", widthDiviserPastValue]];
						[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
						[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
					}else {
						[nmberbtnAtTime2 addObject:@""];
					}
					break;
				}
			}
			if (!foundParent) {
				[nmberbtnAtTime addObject:[NSString stringWithFormat:@"%d", widthDiviserPastValue]];
				[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
				widthDiviserPastValue = 1;
				widthDiviser = 1;
				k = j-1;
				if (j == ([weekDataObjArrayLocal count]-1)) {
					[nmberbtnAtTime addObject:[NSString stringWithFormat:@"%d", widthDiviser]];
					[nmberbtnAtTime2 addObject:[nmberbtnAtTime objectAtIndex:([nmberbtnAtTime count] - 1)]];
				}
			}
		}
	}
    
    
	NSString *tempStr;
	for (int t = ([nmberbtnAtTime2 count] -1); t >= 0 ; t--) {
		if ([[nmberbtnAtTime2 objectAtIndex:t] isEqualToString:@""]) {
			[nmberbtnAtTime2 replaceObjectAtIndex:t withObject:tempStr];
		}else {
			tempStr = [nmberbtnAtTime2 objectAtIndex:t];
		}
	}
	
    int tempVar = 0;
    int p = 0;
    
    
	for (int i = loopStarter; i < [weekDataObjArrayLocal count] + loopStarter; i++) {
        
        //NSLog(@"***************** for Loop **********************)");
        // NSLog(@"loopStarter = %d", loopStarter);
        // NSLog(@"i = %d", i);
        
		CGFloat timeInterval = 0;
		CGFloat minutesSpace = 0;
		CGFloat startY = 0;
		CGFloat startX = initialGapX;
		CGFloat buttonHeight = 0;
		CGFloat currentWidth = 0;
		
		//*************************** Steve Test *******************
		AppointmentsDataObject *aAppobj = [weekDataObjArrayLocal objectAtIndex:p];
        //*************************** Steve Test *******************
        
		
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		NSDate *strtDate11 = [formater dateFromString:[aAppobj startDateTime]];
		NSDate *endDate11 = [formater dateFromString:[aAppobj dueDateTime]];
        
        //[formater setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
        [formater setLocale:[NSLocale currentLocale]];//Steve
        [formater setDateStyle:NSDateFormatterNoStyle];//Steve
        [formater setTimeStyle:NSDateFormatterShortStyle];//Steve
		
		CustomEventButton *appIndicatorButton;
		
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
		timeInterval = ([[formater dateFromString:[aAppobj dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[aAppobj startDateTime]]] / 60.0);
        
		minutesSpace = ([[[[aAppobj startDateTime] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
		buttonHeight = (((timeInterval * (sagmentGapY / 60.0)) + startY) > (initialGapY + sagmentGapY * 24) ?  ((initialGapY + sagmentGapY * 24) - startY): (timeInterval * (sagmentGapY / 60.0))) + ((timeInterval * (sagmentGapY / 60.0)) / sagmentGapY) ;
        
        
		buttonHeight = buttonHeight <= (sagmentGapY * 40)/100 ? (sagmentGapY * 40)/100 : buttonHeight;
		
		if ([nmberbtnAtTime2 count]  == 0) {
			[nmberbtnAtTime2 addObject:@"0"];
		}
		
		currentWidth = generalBtnWidth/[[nmberbtnAtTime2 objectAtIndex:p] intValue];
       
        
		if ([[nmberbtnAtTime2 objectAtIndex:p] intValue] <= 1) {
			startX = initialGapX;
			currentWidth = generalBtnWidth/[[nmberbtnAtTime2 objectAtIndex:p] intValue];
			tempVar = i;
		}
        else {
           
			if (p == 0) {
                tempVar = i;
				startX = initialGapX;
			}else {
                [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
				NSDate *baseStrtDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p-1] startDateTime]];
				NSDate *baseEndDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p-1] dueDateTime]];
				NSDate *curStartDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p] startDateTime]];
                
                //NSLog(@"baseStrtDate = %@", [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p-1] startDateTime]]);
                //NSLog(@"baseEndDate = %@", [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p-1] dueDateTime]]);
				//NSLog(@"curStartDate = %@", [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:p] startDateTime]]);
                
                
				if (([baseStrtDate compare:curStartDate] == NSOrderedAscending && [baseEndDate compare:curStartDate] == NSOrderedDescending) || ([baseStrtDate compare:curStartDate] == NSOrderedSame && [baseEndDate compare:curStartDate] == NSOrderedDescending)){// || ([baseEndDate compare:curStartDate] == NSOrderedSame && [baseStrtDate compare:curStartDate] == NSOrderedAscending)) {
                     int objectindex = [weekAppDataObjArray indexOfObject:aAppobj];
                    UIButton *preBtn = (UIButton *)[weekTimeImageView viewWithTag:Week_Event_TAG + objectindex-1];
                    //alok commented
					//UIButton *preBtn = (UIButton *)[weekTimeImageView viewWithTag:Week_Event_TAG + i-1];
					startX = preBtn.frame.origin.x + preBtn.frame.size.width;
				}
                else
                {
                    
                    // NSLog(@"  ******* Else *****************");
                    //NSLog(@"weekDataObjArrayLocal.count = %d", weekDataObjArrayLocal.count);
                    //NSLog(@" i = %d", i);
                    //NSLog(@" p = %d", p);
                    // NSLog(@" tempVar = %d", tempVar);
					
                    //int tempL; //Steve commented out since Not used now
					foundParent = NO;
                    
                    //**************Steve commented out************** - Calls the wrong index. wrong formula, make the ***app crash***
                    //Not sure of it's purpose
                    /*
                     for(tempL = i-1; tempL >= tempVar; tempL--) {
                     NSLog(@"  crash ***********************************");
                     NSLog(@"weekDataObjArrayLocal.count = %d", weekDataObjArrayLocal.count);
                     NSLog(@"i = %d", i);
                     NSLog(@"tempL = %d", tempL);
                     
                     NSDate *prevStrtDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:tempL] startDateTime]];
                     NSDate *prevEndDate = [formater dateFromString:[[weekDataObjArrayLocal objectAtIndex:tempL] dueDateTime]];
                     if (([prevStrtDate compare:curStartDate] == NSOrderedAscending && [prevEndDate compare:curStartDate] == NSOrderedDescending) || ([prevStrtDate compare:curStartDate] == NSOrderedSame || [prevEndDate compare:curStartDate] == NSOrderedDescending)){
                     UIButton *preBtn = (UIButton *)[weekTimeImageView viewWithTag:Week_Event_TAG + tempL];
                     startX = preBtn.frame.origin.x + preBtn.frame.size.width;
                     foundParent = YES;
                     break;
                     }
                     } //for End
                     */
                    
					if (!foundParent) {
                        startX = initialGapX;
						tempVar = i-1;
					}
				}
			}
		}

        
		if (timeInterval/60 >= 24) {
           
            
            //****************************************
            //************** Steve ********************
            //if more than one day, show on "All Day" instead
            // move to showDataAllDay
            //****************************************
           // NSLog(@"aAppobj.startDateTime = %@", aAppobj.startDateTime);
            //NSLog(@"aAppobj.dueDateTime = %@", aAppobj.dueDateTime);
            //NSLog(@"timeInterval #1 = %f", timeInterval);
            
             /* Steve commented - crashes & should show event on ALL Day
            startY = 2;
			buttonHeight = 36;
			
			[appIndicatorButton setFrame:CGRectMake(initialGapX, startY, generalBtnWidth, buttonHeight)];
			UIColor *firstColor  = [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
			UIColor *secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
			[appIndicatorButton setGradientWithHiColor:firstColor lowColor:secondColor forHighlighted:NO];
			[allWeekTimeImageView addSubview:appIndicatorButton];
            
            NSLog(@"[[allWeekTimeImageView subviews] count] = %d", [[allWeekTimeImageView subviews] count]);
			
			for (int i = 0; i < [[allWeekTimeImageView subviews] count]; i++) {
				CustomEventButton *tempBtn = (CustomEventButton *)[allWeekTimeImageView.subviews objectAtIndex:i];
				if ( i == 0 ) {
					startX = initialGapX;
				}else {
					UIButton *preBtn = (UIButton *)[allWeekTimeImageView.subviews objectAtIndex:i-1];
					startX = preBtn.frame.origin.x + preBtn.frame.size.width;
				}
				
				[tempBtn setFrame:CGRectMake(startX, startY, generalBtnWidth/[[allWeekTimeImageView subviews] count], buttonHeight)];
                */
                
                //******** Crashes here ***********
                //****************************************
                //************** Steve ********************
                //****************************************
                //********* STEVE - BUG HERE WITH EVENT SPANNING 3 DAYS??? ***********
				//[tempBtn setFrames];
                
			//}
		}
		else {
			[formater setDateFormat:@"yyyy-MM-dd"];
            
			if ([[formater dateFromString:[[aAppobj startDateTime] substringToIndex:10]] compare:[formater dateFromString:[forDateStr substringToIndex:10]]] == NSOrderedAscending) {
				[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
				
				timeInterval = ([[formater dateFromString:[aAppobj dueDateTime]] timeIntervalSinceDate:[formater dateFromString:[[forDateStr substringToIndex:10] stringByAppendingString:@" 00:00:00 +0000"]]] / 60.0);
				
				minutesSpace = ([[[[aAppobj startDateTime] substringFromIndex:14] substringToIndex:2] floatValue] * (sagmentGapY / 60.0));
				
				buttonHeight = (((timeInterval * (sagmentGapY / 60.0)) + startY) > (initialGapY + sagmentGapY * 24) ?  ((initialGapY + sagmentGapY * 24) - startY): (timeInterval * (sagmentGapY / 60.0))) + ((timeInterval * (sagmentGapY / 60.0)) / sagmentGapY) ;
				
				buttonHeight = buttonHeight==0?0:(buttonHeight<= (sagmentGapY * 40)/100 ? (sagmentGapY * 40)/100 : buttonHeight);
				startY = initialGapY;
                
			}
			else {
				[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
				
				startY = (([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue]) * sagmentGapY) + initialGapY + minutesSpace + ([[[aAppobj startDateTime] substringWithRange:NSMakeRange(11, 12)] floatValue] -1);
				
				buttonHeight = startY + buttonHeight > generalScreenHeight ?  (buttonHeight - (startY + buttonHeight - generalScreenHeight)): buttonHeight;
				buttonHeight = buttonHeight==0?0:(buttonHeight<= (sagmentGapY * 40)/100 ? (sagmentGapY * 40)/100 : buttonHeight);
			}
			//NSLog(@"Y position %f", startY);
            
            //[formater setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
            [formater setLocale:[NSLocale currentLocale]];//Steve
            [formater setDateStyle:NSDateFormatterNoStyle];//Steve
            [formater setTimeStyle:NSDateFormatterShortStyle];//Steve
			
			appIndicatorButton = [[CustomEventButton alloc] initWithFrame: CGRectMake(startX, startY, currentWidth, buttonHeight) andAppointmentType: aAppobj.appointmentType];
            int objectindex = [weekAppDataObjArray indexOfObject:aAppobj];
           [appIndicatorButton setTag:Week_Event_TAG + objectindex];
            //Alok Commented
			//[appIndicatorButton setTag:Week_Event_TAG + i];
			[appIndicatorButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
			[appIndicatorButton setReversesTitleShadowWhenHighlighted:YES];
			[appIndicatorButton setShowsTouchWhenHighlighted:YES];
			[appIndicatorButton setAdjustsImageWhenHighlighted:YES];
			
			[appIndicatorButton.timeLabel setText:[NSString stringWithFormat:@"%@ - %@", [[formater stringFromDate:strtDate11] stringByReplacingOccurrencesOfString:@":00" withString:@""], [[formater stringFromDate:endDate11] stringByReplacingOccurrencesOfString:@":00" withString:@""]]];
			
            //Steve commented
			//[appIndicatorButton.notesLabel setText:[aAppobj shortNotes]];
            [appIndicatorButton.notesLabel setText:[aAppobj textLocName]];//Steve - show Location instead of Note
			
			if ([aAppobj.appointmentType isEqualToString:@"H"]) {
				UIImage *titleImage = [[UIImage alloc] initWithData:[aAppobj appointmentBinary]];
                [titleImage roundedCornerImage:5 borderSize:1];
				//			[appIndicatorButton.titleImage setImage:titleImage];
				[appIndicatorButton.titleImage setBackgroundImage:titleImage forState:UIControlStateNormal];
			}else {
				[appIndicatorButton.evntTitleLabel setText:[aAppobj appointmentTitle]];
			}
			
			
            		//[appIndicatorButton setFrame:CGRectMake(startX, startY, currentWidth, buttonHeight)];
			[appIndicatorButton setFrames];
            
        
            UIColor *firstColor;
			UIColor *secondColor;
            
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            
            if ( IS_IOS_7 == YES && ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Picture Calendar
                
                firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:1.0];
                secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.90];
            }
            else{ //Classic Calendar or iOS 6
                
                firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
                secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
            }

            
			
			[appIndicatorButton setGradientWithHiColor:firstColor midColor:secondColor lowColor:firstColor forHighlighted:NO];
			[weekTimeImageView addSubview:appIndicatorButton];
		}
		p++;
	}
    
    
	loopStarter = loopStarter + [weekDataObjArrayLocal count];
	
}

-(void)showDataAllDay:(NSArray *) withDataObj andColumnNo:(NSInteger) columnNmbr forDateStr:(NSString *) forDateStr {
   // NSLog(@"showDataAllDay");
    
    
	NSInteger dayDiff   = 0;
    //	CGFloat	sagmentGapY = 0.0;
	
	NSMutableArray *allDayObjArray = [withDataObj mutableCopy];
    
	if ([allDayObjArray count] == 0) {
		return ;
	}
    
	[formater	setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	NSDate		*tempCurrentVisibleDate	= [formater dateFromString:appDelegate.currentVisibleDate];
	NSInteger	dayOfWeek				= [self weekDayForDate:appDelegate.currentVisibleDate];
    
    
    //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
    NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
    
    NSDate		*firstDateOfWeek;
	NSDate		*lastDateOfWeek;
    
    if (startOfWeek == 1) { //Sun
        
        firstDateOfWeek		= [tempCurrentVisibleDate dateBySubtractingDays:(dayOfWeek - 1)];
        lastDateOfWeek		= [tempCurrentVisibleDate dateByAddingDays:(7 - dayOfWeek)];
    }
    else{ //Mon
        
        firstDateOfWeek		= [tempCurrentVisibleDate dateBySubtractingDays:(dayOfWeek - 2)];
        lastDateOfWeek		= [tempCurrentVisibleDate dateByAddingDays:(8 - dayOfWeek)];
    }
    

	
	for(int i = 0; i < [allDayObjArray count]; i++) {
		//BOOL preStart = NO;
		AppointmentsDataObject *aAppobj = [allDayObjArray objectAtIndex:i];
		[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
		
		NSDate *strtDate11 = [formater dateFromString:[aAppobj startDateTime]];
		NSDate *endDate11 = [formater dateFromString:[aAppobj dueDateTime]];
		
		CGFloat	initialGapX = 0;
        
        //******************************************
        //********* Steve Changed *********************
        //******************************************
        //Steve commented - was off by one day on events that spread multiple days.
        //Example 1/1/13 - 1/3/13 showed on 1/1 & 1/2, Not 1/3
        //Need to change how to calculate bandwith
        //1. Take time to beginning of day (12AM) 2. Take Time Diff 3. Add One day
        //We don't care about time interval between days, but the actual days event falls on
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setLocale:[NSLocale currentLocale]];
        [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        
        NSDateComponents *setStartDateToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:strtDate11];
        [setStartDateToDayBeginning setHour:0];
        [setStartDateToDayBeginning setMinute:0];
        [setStartDateToDayBeginning setSecond:0];
        
        NSDate *startDateResetTimeToBeginning = [calendar dateFromComponents:setStartDateToDayBeginning];
        
        
        NSDateComponents *setEndDateToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:endDate11];
        [setEndDateToDayBeginning setHour:0];
        [setEndDateToDayBeginning setMinute:0];
        [setEndDateToDayBeginning setSecond:0];
        
        NSDate *endDateResetTimeToBeginning = [calendar dateFromComponents:setEndDateToDayBeginning];
        
        //NSLog(@"startDateResetTimeToBeginning = %@", startDateResetTimeToBeginning);
        //NSLog(@"endDateResetTimeToBeginning = %@", endDateResetTimeToBeginning);
        
        
        //******************************************
        //********* Steve END *********************
        //******************************************

        
        //Steve - Finds which day the week starts depending on international settings. i.e. Europe starts Mon.  USA Sun
        NSUInteger startOfWeek = [[NSCalendar currentCalendar] firstWeekday];
        
        
        //compairDateWithoutTimeDate1:strtDate11 date2:firstDateOfWeek]
         //return 1; // Date One is smaller then date two
         //return 3; // Date One is bigger then date two
         //return 2; // Date One is equal date two
        
		//if == 1, Date One is smaller than Date Two
		if ([self compairDateWithoutTimeDate1:strtDate11 date2:firstDateOfWeek] == 1) {
            //NSLog(@" ***** #1");
			
			//preStart = YES;
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			
			//			dayDiff =  [firstDateOfWeek differenceInDaysTo:endDate];
			//dayDiff =  [firstDateOfWeek daysBetweenDate:endDateResetTimeToBeginning]; //Steve commented
            dayDiff =  [endDateResetTimeToBeginning timeIntervalSinceDate:firstDateOfWeek]/60/60/24; //Steve added
            
            
            if (startOfWeek == 1) { //Sun
                
                //Steve
                if (IS_IPAD) {
                    initialGapX = 49 + 102 * ([self weekDayForDate:[firstDateOfWeek description]] - 1) + ([self weekDayForDate:[firstDateOfWeek description]] - 1);
                    initialGapX	= initialGapX  + columnNmbr + (columnNmbr*0.75);//Adjust each x position to add gap for lines bewtween days
                }
                else{ //iPhone
                    initialGapX = 47 + 38 * ([self weekDayForDate:[firstDateOfWeek description]] - 1) + ([self weekDayForDate:[firstDateOfWeek description]] - 1);
                }

            }
            else{ //Mon
                
                //Steve
                if (IS_IPAD) {
                    initialGapX = 47 + 102 * ([self weekDayForDate:[firstDateOfWeek description]] - 2) + ([self weekDayForDate:[firstDateOfWeek description]] - 2);
                    initialGapX	= initialGapX  + columnNmbr + (columnNmbr*0.75);//Adjust each x position to add gap for lines bewtween days
                }
                else{ //iPhone
                    initialGapX = 47 + 38 * ([self weekDayForDate:[firstDateOfWeek description]] - 2) + ([self weekDayForDate:[firstDateOfWeek description]] - 2);
                }
                
            }
            
		}
        //if == 1, Date One is smaller than Date Two
		else if ([self compairDateWithoutTimeDate1:lastDateOfWeek date2:endDate11] == 1) {
            //NSLog(@" ***** #2");
			//preStart = NO;
			
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			//			dayDiff =  [strtDate differenceInDaysTo:lastDateOfWeek];
			//dayDiff =  [strtDate11 daysBetweenDate:lastDateOfWeek]; //Steve commented
            dayDiff =  [lastDateOfWeek timeIntervalSinceDate:startDateResetTimeToBeginning]/60/60/24; //Steve added
            
            
            if (startOfWeek == 1) { //Sun
                
                //Steve
                if (IS_IPAD) {
                    initialGapX = 49 + 102 * ([self weekDayForDate:[aAppobj startDateTime]] - 1) + ([self weekDayForDate:[aAppobj startDateTime]] - 1);
                    initialGapX	= initialGapX  + columnNmbr + (columnNmbr*0.75);//Adjust each x position to add gap for lines bewtween days
                }
                else{
                     initialGapX = 47 + 38 * ([self weekDayForDate:[aAppobj startDateTime]] - 1) + ([self weekDayForDate:[aAppobj startDateTime]] - 1);
                }
                
            }
            else{ //Mon
                
                //Steve
                if (IS_IPAD) {
                     initialGapX = 47 + 102 * ([self weekDayForDate:[aAppobj startDateTime]] - 2) + ([self weekDayForDate:[aAppobj startDateTime]] - 2);
                }
                else{ //iPhone
                     initialGapX = 47 + 38 * ([self weekDayForDate:[aAppobj startDateTime]] - 2) + ([self weekDayForDate:[aAppobj startDateTime]] - 2);
                }
               
            }
            
			
		}
		else {
            //NSLog(@" ***** #3");

			//preStart = NO;
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			//dayDiff =  [strtDate differenceInDaysTo:endDate];
            //Steve commented
			//dayDiff =  [strtDate11 daysBetweenDate:endDate11];
            
            //Steve changed
            dayDiff =  [endDateResetTimeToBeginning timeIntervalSinceDate:startDateResetTimeToBeginning]/60/60/24;

            
            if (startOfWeek == 1) { //Sun
                
                //Steve
                if (IS_IPAD) {
                        initialGapX = 47 + 102 * ([self weekDayForDate:[aAppobj startDateTime]] - 1) + ([self weekDayForDate:[aAppobj startDateTime]] - 1);
                        initialGapX	= initialGapX  + columnNmbr + (columnNmbr*0.75);//Adjust each x position to add gap for lines bewtween days

                }
                else{ //iPhone
                        initialGapX = 47 + 38 * ([self weekDayForDate:[aAppobj startDateTime]] - 1) + ([self weekDayForDate:[aAppobj startDateTime]] - 1);
                }

            }
            else{ //Mon
                
                //Steve
                if (IS_IPAD) {
                    initialGapX = 47 + 102 * ([self weekDayForDate:[aAppobj startDateTime]] - 2) + ([self weekDayForDate:[aAppobj startDateTime]] - 2);
                    initialGapX	= initialGapX  + columnNmbr + (columnNmbr*0.75);//Adjust each x position to add gap for lines bewtween days
                }
                else{
                    initialGapX = 47 + 38 * ([self weekDayForDate:[aAppobj startDateTime]] - 2) + ([self weekDayForDate:[aAppobj startDateTime]] - 2);
                }
                
                //NSLog(@"%f",initialGapX);
            }
            
        }
        
        //******************************************
        //********* Steve TEST *********************
        //******************************************

        
        //Steve commented - was off by one day on events that spread multiple days
		//CGFloat generalBtnWidth	= 38 * ((dayDiff + 1) == 0 ? 1 : (dayDiff + 1));
		//generalBtnWidth = (generalBtnWidth > 272.5 ? 272.5 : generalBtnWidth) + dayDiff;
        
        CGFloat generalBtnWidth;
        dayDiff = dayDiff + 1; //Add one to show days covered on Calendar
        
        
        if (dayDiff == 1) {
            
            //Steve
            if (IS_IPAD) {
                generalBtnWidth = 101.25 * dayDiff;
            }
            else{ //iPhone
                generalBtnWidth = 38 * dayDiff;
            }
            
        }
        else{
            
            //Steve
            if (IS_IPAD) {
                 generalBtnWidth = 101.25 * dayDiff; //Steve changed from 38 to 39 to compensate for lines between dates
            }
            else{ //iPhone
                 generalBtnWidth = 39 * dayDiff; //Steve changed from 38 to 39 to compensate for lines between dates
            }
        }
        
  
       // NSLog(@"******* dayDiff = %d", dayDiff);
        //NSLog(@"generalBtnWidth = %f", generalBtnWidth);

		
		CGFloat startX = initialGapX;
		CGFloat startY = 1;
		CGFloat buttonHeight;
        
        //Steve
        if (IS_IPAD) {
            buttonHeight = 42;
        }
        else{ //iPhone
            buttonHeight = 36;
        }
		
        //[formater setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
        [formater setLocale:[NSLocale currentLocale]];//Steve
        [formater setDateStyle:NSDateFormatterNoStyle];//Steve
        [formater setTimeStyle:NSDateFormatterShortStyle];//Steve
		
		CustomEventButton *appIndicatorButton = [[CustomEventButton alloc] initWithFrame: CGRectMake(startX, startY, generalBtnWidth, buttonHeight) andAppointmentType: aAppobj.appointmentType];
		
		[appIndicatorButton setTag:Week_Allday_Event_TAG + i];
		[appIndicatorButton addTarget:self action:@selector(eventButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
		[appIndicatorButton setReversesTitleShadowWhenHighlighted:YES];
		[appIndicatorButton setShowsTouchWhenHighlighted:YES];
		[appIndicatorButton setAdjustsImageWhenHighlighted:YES];
		
        //Steve commented - no need for time since it spans 1 day or more
		//[appIndicatorButton.timeLabel setText:[NSString stringWithFormat:@"%@ - %@", [[formater stringFromDate:strtDate11] stringByReplacingOccurrencesOfString:@":00" withString:@""], [[formater stringFromDate:endDate11] stringByReplacingOccurrencesOfString:@":00" withString:@""]]];
		
        //Steve commented - Not needed on button for Week Cal
		//[appIndicatorButton.notesLabel setText:[aAppobj shortNotes]];
		
		if ([aAppobj.appointmentType isEqualToString:@"H"]) {
            
			UIImage *titleImage = [[UIImage alloc] initWithData:[aAppobj appointmentBinary]];
			[titleImage roundedCornerImage:5 borderSize:1];
			[appIndicatorButton.titleImage setBackgroundImage:titleImage forState:UIControlStateNormal];
		}
        else {
			[appIndicatorButton.evntTitleLabel setText:[aAppobj appointmentTitle]];
		}
        
        NSLog(@"appIndicatorButton.evntTitleLabel = %@", appIndicatorButton.evntTitleLabel);

        
        UIColor *firstColor;
        UIColor *secondColor;
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ( IS_IOS_7 == YES && ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Picture Calendar
            
            firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:1.0];
            secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.90];
        }
        else{ //Classic Calendar or iOS 6
            
            firstColor =  [[aAppobj calendarColor] colorWithAlphaComponent:0.65];
            secondColor = [[aAppobj calendarColor] colorWithAlphaComponent:0.55];
        }
        

        
		[appIndicatorButton setGradientWithHiColor:firstColor lowColor:secondColor forHighlighted:NO];
		
		
		[allWeekTimeImageView addSubview:appIndicatorButton];
		
		[appIndicatorButton setFrames];
		
		if (i == 0) {
			continue;
		} else {
			AppointmentsDataObject *previousAppobj = [allDayObjArray objectAtIndex:i-1];
			[formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
			
			NSDate *startDateOfWeek  = currentStartDateofWeek;
			NSDate *currentStartDate = [formater dateFromString:[aAppobj startDateTime]];
			currentStartDate = [self compairDateWithoutTimeDate1:currentStartDate date2:startDateOfWeek] == 1 ? startDateOfWeek : currentStartDate;
			
			NSDate *currentEndDate = [formater dateFromString:[aAppobj dueDateTime]];
			currentEndDate = [self compairDateWithoutTimeDate1:currentEndDate date2:currentEndDateofWeek] == 3 ? currentEndDateofWeek : currentEndDate;
			
            //			NSDate *currentSelectedDate = [formater dateFromString:appDelegate.currentVisibleDate];
			
			NSDate *preStartDate = [formater dateFromString:[previousAppobj startDateTime]];
			preStartDate = [self compairDateWithoutTimeDate1:preStartDate date2:startDateOfWeek] == 1 ? startDateOfWeek : preStartDate;
			
			NSDate *preEndDate = [formater dateFromString:[previousAppobj dueDateTime]];
			preEndDate = [self compairDateWithoutTimeDate1:preEndDate date2:currentEndDateofWeek] == 3 ? currentEndDateofWeek : preEndDate;
			
			if (([self compairDateWithoutTimeDate1:preStartDate date2:currentStartDate] == 1 || [self compairDateWithoutTimeDate1:preStartDate date2:currentStartDate] == 2)  && ([self compairDateWithoutTimeDate1:preEndDate date2:currentStartDate] == 3 || [self compairDateWithoutTimeDate1:preEndDate date2:currentEndDate] == 2)) {
				
				CustomEventButton *tempBtn = (CustomEventButton *)[allWeekTimeImageView viewWithTag:Week_Allday_Event_TAG + (i-1)];
				if(IS_IPAD)
                    [tempBtn setFrame:CGRectMake(tempBtn.frame.origin.x, tempBtn.frame.origin.y, tempBtn.frame.size.width, buttonHeight/2)];
                else
                    [tempBtn setFrame:CGRectMake(tempBtn.frame.origin.x, tempBtn.frame.origin.y, tempBtn.frame.size.width, buttonHeight/2)];
				
				[appIndicatorButton setFrame:CGRectMake(appIndicatorButton.frame.origin.x,   tempBtn.frame.origin.y + tempBtn.frame.size.height, appIndicatorButton.frame.size.width, buttonHeight/2)];
			}
			
			for(int j = i-1; j >= 0; j--) {
				previousAppobj = [allDayObjArray objectAtIndex:j];
				preStartDate = [formater dateFromString:[previousAppobj startDateTime]];
				preStartDate = [self compairDateWithoutTimeDate1:preStartDate date2:startDateOfWeek] == 1 ? startDateOfWeek : preStartDate;
				
				preEndDate = [formater dateFromString:[previousAppobj dueDateTime]];
				preEndDate = [self compairDateWithoutTimeDate1:preEndDate date2:currentEndDateofWeek] == 3 ? currentEndDateofWeek : preEndDate;
				
				UIButton *tempBtn = (UIButton *)[allWeekTimeImageView viewWithTag:Week_Allday_Event_TAG + j];
				
				if ([self compairDateWithoutTimeDate1:currentStartDate date2:preStartDate] == 2 ||
					([self compairDateWithoutTimeDate1:preStartDate date2:currentStartDate] == 1 &&
					 [self compairDateWithoutTimeDate1:preEndDate date2:currentStartDate] == 3) ||
					[self compairDateWithoutTimeDate1:currentStartDate date2:preEndDate] == 2) {
					
					if ([currentStartDate daysBetweenDate:currentEndDate]  > [preStartDate daysBetweenDate:preEndDate]) {
						NSInteger tempTag;
						
						[appIndicatorButton setFrame:CGRectMake(appIndicatorButton.frame.origin.x, tempBtn.frame.origin.y, appIndicatorButton.frame.size.width, buttonHeight/2)];
						
						tempTag = appIndicatorButton.tag;
                        if(IS_IPAD)
                            [tempBtn setFrame:CGRectMake(tempBtn.frame.origin.x, tempBtn.frame.origin.y + buttonHeight/2, tempBtn.frame.size.width, buttonHeight/2)];
                        else
                            [tempBtn setFrame:CGRectMake(tempBtn.frame.origin.x, tempBtn.frame.origin.y + buttonHeight/2, tempBtn.frame.size.width, buttonHeight/2)];
						appIndicatorButton.tag = tempBtn.tag;
						tempBtn.tag = tempTag;
						[allDayObjArray exchangeObjectAtIndex:j withObjectAtIndex:i];
						//
						//						for(int k = j+1; k <= i; k++) {
						//							UIButton *tempBtn = (UIButton *)[allWeekTimeImageView viewWithTag:Week_Allday_Event_TAG + k];
						//							[tempBtn setFrame:CGRectMake(tempBtn.frame.origin.x, tempBtn.frame.origin.y + buttonHeight/2, tempBtn.frame.size.width, buttonHeight/2)];
						//						}
					}
					else {
						[appIndicatorButton setFrame:CGRectMake(appIndicatorButton.frame.origin.x, tempBtn.frame.origin.y + buttonHeight/2, appIndicatorButton.frame.size.width, buttonHeight/2)];
						break;
					}
					
				}else {
					break;
				}
			}
			
			for(int j = 0; j < i; j++) {
				AppointmentsDataObject *tempAppobj = [allDayObjArray objectAtIndex:j];
				
				//NSDate *tempStartDate = [formater dateFromString:[tempAppobj startDateTime]];
				
                //				tempStartDate = [self compairDateWithoutTimeDate1:tempStartDate date2:startDateOfWeek] == 1 ? startDateOfWeek : tempStartDate;
				
				NSDate *tempEndDate = [formater dateFromString:[tempAppobj dueDateTime]];
				tempEndDate = [self compairDateWithoutTimeDate1:tempEndDate date2:currentEndDateofWeek] == 3 ? currentEndDateofWeek : tempEndDate;
				
				
				CustomEventButton *tempBtn = (CustomEventButton *)[allWeekTimeImageView viewWithTag:Week_Allday_Event_TAG + j];
				
				if ([self compairDateWithoutTimeDate1:tempEndDate date2:currentStartDate] == 1) {
					//[appIndicatorButton setFrame:CGRectMake(appIndicatorButton.frame.origin.x, tempBtn.frame.origin.y, appIndicatorButton.frame.size.width, buttonHeight/2)];
                    [appIndicatorButton setFrame:CGRectMake(appIndicatorButton.frame.origin.x, tempBtn.frame.origin.y, appIndicatorButton.frame.size.width, buttonHeight)];//Steve - fixes bug where it makes AllDay event half the height.
					break;
				}else {
					continue;
				}
			}
		}
		
		allDayViewHeight =  allDayViewHeight <  appIndicatorButton.frame.origin.y + appIndicatorButton.frame.size.height ? appIndicatorButton.frame.origin.y + appIndicatorButton.frame.size.height: allDayViewHeight;
		
	}
}

#pragma mark -
#pragma mark Memory Management Methods
#pragma mark -

- (void )didReceiveMemoryWarning {
    NSLog(@"Did Receive memory warning");
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    //	self.lastSelectedEvent = nil;
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"EventNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.Elixir.LoadDayEvents" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    
    [super viewDidUnload];
}


@end

//
//  AddToDoAlertViewController_iOS7.h
//  Organizer
//
//  Created by Steven Abrams on 10/13/13.
//
//

#import <UIKit/UIKit.h>

@protocol toDoAlertDelegate <NSObject>

-(void)setStartDate:(NSString*)aStartDate todoID:(NSInteger)todoid;
-(void)setEndDate:(NSString*)aEndDate todoID:(NSInteger)todoid;

@end


@interface AddToDoStartEndViewController_iOS7 : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UIView *blurredBackgroundView;
@property (strong, nonatomic) IBOutlet UIImageView *blurredBackgroundImageView;
@property (strong, nonatomic) UIImage *nonBlurredImage; //Image of To Do
@property (strong, nonatomic) UIImage *blurredImage; //Image of To Do blurred
@property (assign, nonatomic) int todoVal;
@property (weak, nonatomic) id<toDoAlertDelegate> delegate;


-(id)initWithStartDateString:(NSString*) startDateStr endDateStr:(NSString*) endDateStr;


@end

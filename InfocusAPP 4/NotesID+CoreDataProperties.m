//
//  NotesID+CoreDataProperties.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotesID+CoreDataProperties.h"

@implementation NotesID (CoreDataProperties)

@dynamic pageID;
@dynamic pageName;
@dynamic pageNumber;
@dynamic drawingData;
@dynamic imageData;
@dynamic textData;
@dynamic stickyData;
@dynamic subNotesCount;
@dynamic projectID;

@end

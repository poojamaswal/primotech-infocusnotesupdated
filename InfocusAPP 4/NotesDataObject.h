//
//  NotesDataObject.h
//  Organizer
//
//  Created by Naresh Chouhan on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NotesDataObject : NSObject
{	
	NSInteger	notesID;
	NSString	*notesTitle; 
	NSString	*notesTitleType; 
	NSData		*notesTitleBinary; 
	NSData		*notesTitleBinaryLarge;
	
	NSData *firstImg;
	NSInteger FImgID;
	
	NSData *firstSkt;
	NSInteger FSktID;
	
	NSMutableArray   *notesMapDataArray;
	NSMutableArray   *notesTitleArray;
	NSMutableArray   *notesWebURArray;
	NSMutableArray   *notesPicDataArray;		
	NSMutableArray   *notessktchimageArrsy;
	NSMutableArray   *notesTextArray;
	NSMutableArray   *notesHwrtDataArray;
	NSMutableArray   *notesBMDataArray;
    NSInteger belongsToProjectID;
}


@property (nonatomic, strong) NSString		*notesTitleType; 
@property (nonatomic, strong) NSData		*notesTitleBinary; 
@property (nonatomic, strong) NSData		*notesTitleBinaryLarge;

@property(nonatomic,strong)NSMutableArray   *notesBMDataArray;
@property(nonatomic,strong)NSData *firstImg;
@property(nonatomic,strong)NSData *firstSkt;

@property(readwrite)NSInteger FSktID;
@property(readwrite) NSInteger FImgID;

@property(nonatomic,strong)NSMutableArray   *notesTitleArray;
@property(nonatomic,strong)NSMutableArray   *notesHwrtDataArray;
@property(nonatomic,strong)NSMutableArray   *notesTextArray;
@property(nonatomic,strong)NSMutableArray   *notesWebURArray;
@property(nonatomic,strong)NSMutableArray *notessktchimageArrsy;
@property (nonatomic, assign) NSInteger	notesID;
@property (nonatomic, strong) NSString	*notesTitle;
@property(nonatomic,strong)NSMutableArray  *notesMapDataArray;
@property(nonatomic,strong)NSMutableArray *notesPicDataArray;
@property (nonatomic, assign) NSInteger	belongsToProjectID;


@end






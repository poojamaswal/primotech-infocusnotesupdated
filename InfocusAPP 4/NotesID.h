//
//  NotesID.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 2/3/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DrawingData, ImageData, StickyData, TextData;

NS_ASSUME_NONNULL_BEGIN

@interface NotesID : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

-(void)saveDrawingInfo:(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name;
- (void)saveImageInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name;
- (void)saveTextInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name;
- (void)saveStickyInfo :(NSDictionary*)dict withContext:(NSManagedObjectContext*)context withName :(NSString*)name;

+ (BOOL)userAlReadyExist:(NSString*)name;
+ (NotesID*) createPage:(NSManagedObjectContext*)context withName:(NSString*)pageID withProjectID:(NSInteger)project_ID;
+ (NotesID*) changeValue:(NSString*)changeValue from:(NSString*)fromvalue;

@end

NS_ASSUME_NONNULL_END

#import "NotesID+CoreDataProperties.h"

//
//  EditProjectViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTreeNode.h"
#import "TODODataObject.h"
#import "ViewTODOScreen.h"
#import "MyTreeViewCell.h"
#import "TODODataObject.h"



@interface EditProjectViewCantroller : UIViewController 
{
    IBOutlet UITableView *editTable;
	IBOutlet UIButton *buttonLevelone;
	IBOutlet UIButton *buttonLeveltwo;
	IBOutlet UIButton *buttonLevelthree;
	IBOutlet UIButton *buttonLevelfour;
	
	NSMutableArray *getOneArray;
	NSMutableArray *getOneNameArray;
	NSMutableArray *getOneImgArray;
	
	NSMutableArray *getOneToDoArray;
	NSMutableArray *getOneToDoTitleArray;
	NSMutableArray *getOneToDoTextTypeArray;
	NSMutableArray *getOneToDoRootBinaryArray;
	NSMutableArray *getOneToDopriorityArray;
	NSMutableArray *getOneToDoRootBinaryLargeArray;
	
	
	NSMutableArray *getTwoArray;
	NSMutableArray *getTwoNameArray;
	NSMutableArray *getTwoImgArray;
	NSMutableArray *getTwoToDoArray;
	NSMutableArray *getTwoToDoTitleArray;
	NSMutableArray *getTwoToDoTextTypeArray;
	NSMutableArray *getTwoToDoRootBinaryArray;
	NSMutableArray *getTwoToDopriorityArray;
	NSMutableArray *getTwoToDoRootBinaryLargeArray;
	
	
	NSMutableArray *getThreeArray;
	NSMutableArray *getThreeNameArray;
	NSMutableArray *getThreeImgArray;
	NSMutableArray *getThreeToDoArray;
	NSMutableArray *getThreeToDoTitleArray;
	NSMutableArray *getThreeToDoTextTypeArray;
	NSMutableArray *getThreeToDoRootBinaryArray;
	NSMutableArray *getThreeToDopriorityArray;
	NSMutableArray *getThreeToDoRootBinaryLargeArray;
	
	NSMutableArray *getFourArray;
	NSMutableArray *getFourNameArray;
	NSMutableArray *getFourImgArray;
	NSMutableArray *getFourToDoArray;
	NSMutableArray *getFourToDoTitleArray;
	NSMutableArray *getFourToDoTextTypeArray;
	NSMutableArray *getFourToDoRootBinaryArray;
	NSMutableArray *getFourToDopriorityArray;
	NSMutableArray *getFourToDoRootBinaryLargeArray;
	
	
	
	
	
	
	
	MyTreeNode *treeNode;	
	MyTreeNode *node1;
	MyTreeNode *node2;
	MyTreeNode *node3;
	MyTreeNode *node4;
	
	MyTreeNode *nodeT1;
	MyTreeNode *nodeT2;
	MyTreeNode *nodeT3;
	MyTreeNode *nodeT4;
    MyTreeViewCell *treeCell;
	NSInteger EPIDval;
	
	
	TODODataObject  *toDODataObject;
	TODODataObject  *toDODataObjectOne;
	TODODataObject  *toDODataObjectTwo;
	TODODataObject  *toDODataObjectThree;
	ViewTODOScreen *viewToDOScreen;
	TODODataObject *tosoDataObj;
	NSInteger optnViewTWO;
    
    
    NSMutableArray *getLevelRootObjectTypeArray; // Anil
    NSMutableArray *pidleveloneObjectTypeArray; // Anil
    NSMutableArray *pidleveltwoObjectTypeArray; // Anil
    NSMutableArray *pidlevelthreeObjectTypeArray; // Anil
	
    IBOutlet UINavigationBar *navBar;

    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
}
@property(nonatomic,assign)NSInteger optnViewTWO;
@property(nonatomic,assign)NSInteger EPIDval;

@property(nonatomic,strong)TODODataObject  *toDODataObject;
@property(nonatomic,strong)TODODataObject  *toDODataObjectOne;
@property(nonatomic,strong)TODODataObject  *toDODataObjectTwo;
@property(nonatomic,strong)TODODataObject  *toDODataObjectThree;



@property(nonatomic,strong) NSMutableArray *getLevelRootObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidleveloneObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidleveltwoObjectTypeArray; // Anil
@property(nonatomic,strong) NSMutableArray *pidlevelthreeObjectTypeArray; // Anil



-(void)FindRecoredOne;
-(void)FindTodoOne;
-(void)FindRecoredTwo:(NSInteger)vtwo;
-(void)FindRecoredThree:(NSInteger)vthree;
-(void)FindRecoredFour:(NSInteger)vfour;

-(void)FindTodoTwo:(NSInteger)ttwo;
-(void)FindTodoThree:(NSInteger)tthree;
-(void)FindTodoFour:(NSInteger)tfour;
-(IBAction)cancel_Click:(id)sender;
-(void)gotoToday:(id)sender;
-(void)FindAllToDo:(NSInteger)todoVal;
-(void)EditFolder:(id)sender;
-(void)PlusNow:(id)sender;
-(void)FindNoteOne;
-(void)FindNoteTwo:(NSInteger)ttwo;
-(void)FindNoteThree:(NSInteger)tthree;
-(void)FindNoteFour:(NSInteger)tfour;




@end

//
//  SketchNoteViewController.h
//  Organizer
//
//  Created by STEVEN ABRAMS on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PaintingView.h"
#import "TODODataObject.h"
#import "PaintSketch.h"
//#import "SketchScrollView.h"

@interface SketchNoteViewController : UIViewController  {
    
    //Steve added
   // IBOutlet SketchScrollView *scrollView;
	IBOutlet PaintSketch     *drawImage;

    
	int controlIndex;
	CGAffineTransform oldViewTransform;
	UIColor *previousColor; 
	
	UIView   *aView;
	UISlider *redSlider;
	UISlider *greenSlider;
	UISlider *blueSlider;
	id		 parentVC;
	id       __unsafe_unretained delegate;
	BOOL     whatVAL;
    
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIToolbar *bottomBar;
    
    IBOutlet UIView *helpView;  //Steve
    IBOutlet UITapGestureRecognizer *tapHelpView;  //Steve
    IBOutlet UISwipeGestureRecognizer *swipeDownHelpView; //Steve
    
    IBOutlet UIImageView *helpImageiPhoneRegular; //Steve
    IBOutlet UIImageView *helpImageiPhone5; //Steve
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *saveButton;
    
    UIView             *colorPanelView;
}



@property(assign)BOOL whatVAL;
//@property(assign)BOOL isSettingNotesHWObject;

@property(unsafe_unretained)id delegate;
@property(nonatomic, strong)id parentVC;

-(IBAction) doneButton_Clicked:(id) sender;  
-(IBAction) backButton_Clicked:(id) sender;  
-(IBAction) drawButton_Clicked:(id) sender;  
-(IBAction) eraseButton_Clicked:(id) sender;  
-(IBAction) colorButton_Clicked:(id) sender;  
-(IBAction) clearButton_Clicked:(id) sender;  
- (IBAction)helpView_Tapped:(id)sender;  //Steve


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withParent:(id) parentVCLocal; 

@end

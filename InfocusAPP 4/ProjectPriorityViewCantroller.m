//
//  ProjectPriorityViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 12/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProjectPriorityViewCantroller.h"


@implementation ProjectPriorityViewCantroller

@synthesize doublePicker;
@synthesize fillingTypes;
@synthesize breadTypes,delegate ;


// The designated initializer. Override to perform setup that is required before the view is loaded.
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil setNewPriority:(NSString*) valueone
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
	{
		NSString *new= valueone;
        
		int n=[new length];
        
        if(![new isEqualToString:@"None"]){
		first = [new substringWithRange: NSMakeRange(0,1)];
		second = [new substringWithRange: NSMakeRange(1,n-1)];
        }
		NSLog(@"%@ %@",first,second);
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	NSArray *fillingArray = [[NSArray alloc] initWithObjects:@"None",@"A",@"B",@"C",nil];
	
	self.fillingTypes = fillingArray;
	
	NSArray *breadArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
	
	self.breadTypes = breadArray;
	[super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        doublePicker.frame = CGRectMake(0, 135, 320, 216); //(0, 123 - 44, 320, 216)
        self.priorityFixedLabel.frame = CGRectMake(20, 89 - 44, 74, 24);
        self.priorityLabel.frame = CGRectMake(96, 92 - 44, 72, 21);
        self.prioritySwitch.frame = CGRectMake(223, 89 - 44, 79, 27);

        
        self.title =@"Project Priority";
        
        [self.navigationController.navigationBar addSubview:doneButton];
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        //Back Button
        /*
         UIImage *backButtonImage = [UIImage imageNamed:@"BackBtn.png"];
         UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
         [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
         [backButtonNewNav addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
         
         UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
         
         [[self navigationItem] setLeftBarButtonItem:barButton];
         */
        
    }
    else{
        
        
        self.navigationController.navigationBarHidden = YES;
        
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];

    }
    
    
    // ***************** Steve End ***************************
    
    
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];

    /*
    if([first length]>0)
	[doublePicker selectRow:[fillingTypes indexOfObject:first] inComponent:0 animated:YES];
    if([second length]>0)
	[doublePicker selectRow:[breadTypes indexOfObject:second] inComponent:1 animated:YES];
    */
    
    if([first length]>0){
        [doublePicker selectRow:[fillingTypes indexOfObject:first] inComponent:0 animated:YES];
    }
    else {
        //Switcher set to OFF
        [self.prioritySwitch setOn:NO];
        
        //Shows Priority: None
        [doublePicker selectRow:0 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
    }
    
    
    if([second length]>0)
        [doublePicker selectRow:[breadTypes indexOfObject:second] inComponent:1 animated:YES];
    
    //NSLog(@"first = %@ second = %@",first,second);
    
    NSString *priorityLabelText = [first stringByAppendingString:second];
    
    if (first == NULL) self.priorityLabel.text = @"None";
    else self.priorityLabel.text = priorityLabelText;
    
    self.priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    
    //NSLog(@"priorityLabel.text = %@",self.priorityLabel.text);
    

}


- (IBAction)prioritySwitch_Changed:(id)sender {
    
    UISwitch *tSwitch = sender;
	
	if ([tSwitch isOn]) {
		//shows Priority: A 1
        [doublePicker selectRow:1 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
        
        NSInteger breadRow = [doublePicker selectedRowInComponent:kBreadComponent];
        NSInteger fillingRow = [doublePicker selectedRowInComponent:kFillingComponent];
        
        NSString *bread = [breadTypes objectAtIndex:breadRow];
        NSString *filling = [fillingTypes objectAtIndex:fillingRow];
        
        self.priorityLabel.text = [filling stringByAppendingString:bread];
        
    }
    else{
        //Shows Priority: None
        [doublePicker selectRow:0 inComponent:0 animated:YES];
        [doublePicker selectRow:0 inComponent:1 animated:YES];
        
        self.priorityLabel.text = @"None";
    }
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 2;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
	if (component == kBreadComponent)
		return[self.breadTypes count];
	return[self.fillingTypes count];
}


-(NSString *)pickerView:(UIPickerView *)pickerView
			titleForRow:(NSInteger)row
		   forComponent:(NSInteger)component {
	if (component == kBreadComponent)
		return [self. breadTypes objectAtIndex:row];
	return [self.fillingTypes objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    NSLog(@"didSelectRow");
    
    NSInteger breadRow = [doublePicker selectedRowInComponent:
	                      kBreadComponent];
	NSInteger fillingRow = [doublePicker selectedRowInComponent:
	                        kFillingComponent];
	NSString *bread = [breadTypes objectAtIndex:breadRow];
	NSString *filling = [fillingTypes objectAtIndex:fillingRow];
    
    
    if (component == kFillingComponent)
    {
        NSLog(@"row = %d ", row);
        NSLog(@"component = %d ", component);
        
        if (row == 0)
        {
            //if row == 0 then it's None and should only show "None" as label
            self.priorityLabel.text = filling;
            self.priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            
            //"None" Switch is OFF
            [self.prioritySwitch setOn:NO animated:YES];
        }
        
        if ((row > 0))
        {
            self.priorityLabel.text = [filling stringByAppendingString:bread];
            self.priorityLabel.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            
            [self.prioritySwitch setOn:YES animated:YES];
        }
        
    }
    else
    {
        self.priorityLabel.text = [filling stringByAppendingString:bread];
        
    }
    
    if ([self.priorityLabel.text length] > 2)
    {
        
        //remove the first 4 characters to avoid things like "None1", should be "None"
        NSMutableString *tempString = [NSMutableString stringWithString: self.priorityLabel.text];
        NSString *priorityLabel_Remove4Char = [tempString substringWithRange: NSMakeRange (0, 4)];
        
        if ([priorityLabel_Remove4Char isEqualToString:@"None"])
        {
            
            //if 1st 4 char are "None", then should label is "None". We don't want the priority number
            self.priorityLabel.text = priorityLabel_Remove4Char;
        }
    }
    
    
}


-(IBAction)Clicl_Cancel:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)Click_Save:(id)sender
{
	
	NSInteger breadRow = [doublePicker selectedRowInComponent:
	                      kBreadComponent];
	NSInteger fillingRow = [doublePicker selectedRowInComponent:
	                        kFillingComponent];
	NSString *bread = [breadTypes objectAtIndex:breadRow];
	NSString *filling = [fillingTypes objectAtIndex:fillingRow];
	
    //Converts "None" to ""
    if ([filling isEqual: @"None"]) {
        bread = @"";
    }
	
    [[self delegate] setPriorityNew:bread priority2:filling];
	[self.navigationController popViewControllerAnimated:YES];
	
	
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [doneButton removeFromSuperview];
        [backButton removeFromSuperview];
        
    }
    
}

- (void)viewDidUnload {
    navBar = nil;
    backButton = nil;
    doneButton = nil;
    _priorityFixedLabel = nil;
    _priorityLabel = nil;
    _prioritySwitch = nil;
    [self setPriorityLabel:nil];
    [self setPrioritySwitch:nil];
    [self setPriorityFixedLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





@end
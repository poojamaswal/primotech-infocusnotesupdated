
//
//  NotesTableCell.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "NotesTableCell.h"



@interface NotesTableCell ()
{
    
    __weak IBOutlet UILabel *_lblTextHeading;
}

@end
@implementation NotesTableCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setTextHeader :(NSString*)text_
{
    _lblTextHeading.text = text_;
}


@end

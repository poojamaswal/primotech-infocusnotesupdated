

#import <UIKit/UIKit.h>

typedef enum{
	RichTextEditorToolbarPresentationStyleModal,
	RichTextEditorToolbarPresentationStylePopover
}RichTextEditorToolbarPresentationStyle;

typedef enum{
	ParagraphIndentationIncrease,
	ParagraphIndentationDecrease
}ParagraphIndentation;

typedef enum{
	RichTextEditorFeatureNone							= 0,
	RichTextEditorFeatureFont							= 1 << 0,
	RichTextEditorFeatureFontSize						= 1 << 1,
	RichTextEditorFeatureBold							= 1 << 2,
	RichTextEditorFeatureItalic							= 1 << 3,
	RichTextEditorFeatureUnderline						= 1 << 4,
	RichTextEditorFeatureStrikeThrough					= 1 << 5,
	RichTextEditorFeatureTextAlignmentLeft				= 1 << 6,
	RichTextEditorFeatureTextAlignmentCenter			= 1 << 7,
	RichTextEditorFeatureTextAlignmentRight				= 1 << 8,
	RichTextEditorFeatureTextAlignmentJustified			= 1 << 9,
	RichTextEditorFeatureTextBackgroundColor			= 1 << 10,
	RichTextEditorFeatureTextForegroundColor			= 1 << 11,
	RichTextEditorFeatureParagraphIndentation			= 1 << 12,
	RichTextEditorFeatureParagraphFirstLineIndentation	= 1 << 13,
	RichTextEditorFeatureAll							= 1 << 14
}RichTextEditorFeature;

@protocol RichTextEditorToolbarDelegate <UIScrollViewDelegate>
- (void)richTextEditorToolbarDidSelectBold;
- (void)richTextEditorToolbarDidSelectItalic;
- (void)richTextEditorToolbarDidSelectUnderline;
- (void)richTextEditorToolbarDidSelectStrikeThrough;
- (void)richTextEditorToolbarDidSelectBulletPoint;
- (void)richTextEditorToolbarDidSelectParagraphFirstLineHeadIndent;
- (void)richTextEditorToolbarDidSelectParagraphIndentation:(ParagraphIndentation)paragraphIndentation;
- (void)richTextEditorToolbarDidSelectFontSize:(NSNumber *)fontSize;
- (void)richTextEditorToolbarDidSelectFontWithName:(NSString *)fontName;
- (void)richTextEditorToolbarDidSelectTextBackgroundColor:(UIColor *)color;
- (void)richTextEditorToolbarDidSelectTextForegroundColor:(UIColor *)color;
- (void)richTextEditorToolbarDidSelectTextAlignment:(NSTextAlignment)textAlignment;
@end

@protocol RichTextEditorToolbarDataSource <NSObject>
- (NSArray *)fontSizeSelectionForRichTextEditorToolbar;
- (NSArray *)fontFamilySelectionForRichTextEditorToolbar;
- (RichTextEditorToolbarPresentationStyle)presentationStyleForRichTextEditorToolbar;
- (UIModalPresentationStyle)modalPresentationStyleForRichTextEditorToolbar;
- (UIModalTransitionStyle)modalTransitionStyleForRichTextEditorToolbar;
- (UIViewController *)firsAvailableViewControllerForRichTextEditorToolbar;
- (RichTextEditorFeature)featuresEnabledForRichTextEditorToolbar;
@end

@interface RichTextEditorToolbar : UIScrollView

@property (nonatomic, weak) id <RichTextEditorToolbarDelegate> delegate;
@property (nonatomic, weak) id <RichTextEditorToolbarDataSource> dataSource;

- (id)initWithFrame:(CGRect)frame delegate:(id <RichTextEditorToolbarDelegate>)delegate dataSource:(id <RichTextEditorToolbarDataSource>)dataSource;
- (void)updateStateWithAttributes:(NSDictionary *)attributes;
- (void)redraw;

@end

//
//  LeveloneViewCantroller.h
//  Organizer
//
//  Created by Naresh Chauhan on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddProjectDataObject.h"

@interface LeveloneViewCantroller : UIViewController
{
	NSInteger getID;
	NSString *getName;
	NSInteger getbelongsTo;
	UIButton *Fbutton;
	UIButton *Fcheckbtn;
	AddProjectDataObject *projectdataobj;
	 UIButton *colorBTN;
	UIImageView *myImage;
		
    NSMutableArray	*pidArray1;
	NSMutableArray	*pnameArray1;
	NSMutableArray	*priorityArray1;
	NSMutableArray	*pcolorRArray1;
	NSMutableArray	*PcolorGArray1;
	NSMutableArray	*PcolorBArray1;
	NSMutableArray	*PleveArray1;
	NSMutableArray  *PbelongtoArray1;
	
	IBOutlet UIButton *myButton;
	IBOutlet UIButton *myButton1;
	IBOutlet UIButton *mainCHEKBTN;
	
	NSInteger	levleValue;
	NSInteger	belongsTo;
	CGRect myImageRect;
	int editOPt;
	int prntIDone;
	NSString *Sql ;
}
@property(nonatomic,assign)int prntIDone;
@property(assign)int editOPt;
@property(nonatomic,assign)NSInteger	     levleValue;
@property(nonatomic,assign)NSInteger	     belongsTo;
@property(nonatomic,assign)NSInteger         getbelongsTo;
@property(nonatomic,retain)NSMutableArray   *PbelongtoArray1;
@property(nonatomic,retain)NSMutableArray	*pidArray1;
@property(nonatomic,retain)NSMutableArray	*pnameArray1;
@property(nonatomic,retain)NSMutableArray	*priorityArray1;
@property(nonatomic,retain)NSMutableArray	*pcolorRArray1;
@property(nonatomic,retain)NSMutableArray	*PcolorGArray1;
@property(nonatomic,retain)NSMutableArray	*PcolorBArray1;
@property(nonatomic,retain)NSMutableArray	*PleveArray1;

@property(nonatomic,assign)NSInteger getID;
@property(nonatomic,retain)NSString *getName;

-(IBAction)cancel_btn:(id)sender;
-(void)fradio_click:(id)sender;
-(void)getLevelData:(NSInteger)idVal;
-(IBAction)save_level:(id)sender;
@end

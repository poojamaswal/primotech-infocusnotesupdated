//
//  StartEndCalendarViewController.h
//  Organizer
//
//  Created by Steven Abrams on 9/11/13.
//
//

#import <UIKit/UIKit.h>
#import "AppointmentViewController .h"

@interface StartEndCalendarViewController : UIViewController <AppointmentViewControllerDelegate>{
    
    id					__unsafe_unretained delegate;
    BOOL                isAllDayEvent;
    BOOL                isStartDateButton;
    BOOL                isEndDateButton;
    
    
}

@property(unsafe_unretained)  id                        delegate;  
@property (strong, nonatomic) IBOutlet UITableView      *myTableView;
@property (strong, nonatomic) IBOutlet UIDatePicker     *datePicker;
@property(strong, nonatomic) UILabel                    *startLabel;
@property(strong, nonatomic) UILabel                    *endLabel;
@property(strong, nonatomic) UISwitch                   *allDaySwitch;

@property(strong, nonatomic) NSString                   *startDateString;
@property(strong, nonatomic) NSString                   *endDateString;

@property(strong, nonatomic) NSDate                     *startDate_Date;
@property(strong, nonatomic) NSDate                     *endDate_Date;

@property (strong, nonatomic) UIButton                  *doneButton;

-(id)initWithStartDateString:(NSString*) startDateStr
                  endDateStr:(NSString*) endDateStr
                    isAllDay:(BOOL) allDay
              startDate_Date:(NSDate*) startDate_Date
                endDate_Date:(NSDate*) endDate_Date;

- (IBAction)datePickerValueChanged:(id)sender;

- (void)doneButtonClicked:(id)sender;

@end

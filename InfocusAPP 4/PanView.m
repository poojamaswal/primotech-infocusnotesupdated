
#import "PanView.h"

@implementation PanView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1.0;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    if (self.displayImage) {
        [self.displayImage drawInRect:CGRectMake(0, 0, self.drawingView.frame.size.width, self.drawingView.frame.size.height)];
    }
    [super drawRect:rect];
}

- (void)setDisplayImage:(UIImage *)displayImage {
    _displayImage = [self scaleImage:displayImage];
    [self.drawingView clear];
    [self setNeedsDisplay];
}

- (UIImage*)scaleImage:(UIImage*)image {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, self.drawingView.frame.size.width, self.drawingView.frame.size.height));
    UIGraphicsBeginImageContextWithOptions(newRect.size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newRect.size.width, newRect.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end

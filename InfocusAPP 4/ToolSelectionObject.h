

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    RectangleFill = 0,
    RectangleStroke = 1,
    EllipseFill = 2,
    EllipseStroke = 3,
    Line = 4
} ShapeType;

#pragma mark - Protocol
@protocol ToolSelectionDelegate <NSObject>

- (void)drawShapeOfType:(ShapeType)shapeType;
- (void)drawImageViewWithImage:(UIImage*)image withFrame:(CGRect)frame;
- (void)drawStickyWithText : (NSString *)str andFrame:(CGRect)frame;
- (void)openCameraRoll;

@end

#pragma mark - Interface
@interface ToolSelectionObject : NSObject

@property (nonatomic, assign) id<ToolSelectionDelegate> delegate;
@property (nonatomic, assign) UIImagePickerControllerSourceType imageSourceType;

+ (ToolSelectionObject*)sharedObject;
- (void)insertSelectedShape:(ShapeType)shapeType;
- (void)insertSelectedImage:(UIImage*)image;
- (void)insertSelectedSticky;
- (void)openCameraRoll;

@end



//
//  AddCalenderDataObject.h
//  Organizer
//
//  Created by Nibha Aggarwal on 2/10/12.
//  Copyright 2012 A1 Technology Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AddCalenderDataObject : NSObject {
    
	NSInteger calenderid;
	NSInteger belongstoid;
	NSString *calendername;
	NSString *calenderIMGname;
	NSMutableArray *Cnamearray;
	NSMutableArray *CcolorRarray;
	NSMutableArray *CcolorGarray;
	NSMutableArray *CcolorBarray;
	NSMutableArray *Cidarray;
	NSString *folderImage;
}

@property(nonatomic, assign) NSInteger calenderid;
@property(nonatomic, assign) NSInteger belongstoid;
@property(nonatomic, strong) NSString *calendername;
@property(nonatomic, strong) NSString *calenderIMGname;
@property(nonatomic, strong) NSMutableArray *Cnamearray;
@property(nonatomic, strong) NSMutableArray *Cidarray;
@property(nonatomic, strong) NSString *folderImage;

-(id)initWithCalId:(NSInteger) calenderID calName:(NSString *)calenderName;
//	calFolderImage:(NSString *)calenderFolderImage;

@end

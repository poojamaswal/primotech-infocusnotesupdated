//
//  appointmentPopupView.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface appointmentPopupView : UIView {
	id parentVC;
	AppointmentsDataObject *appaObj;
	CGRect visibleRectSV;
	CGRect buttonRect;
	CGPoint arrowPoint;
	NSInteger btnTag;
	BOOL isAllDay;
    BOOL is24HourClock; //Steve
}

//-(id)initWithAppObject:(AppointmentsDataObject*) appObj buttonRect:(CGRect) buttonRectLocal visibleRect:(CGRect) rect andParentVC:(id) parent withTag:(NSInteger) tagValue isAllDay:(BOOL) isAllDayLocal;

-(id)initWithAppObject:(AppointmentsDataObject*) appObj buttonRect:(CGRect) buttonRectLocal visibleRect:(CGRect) rect andParentVC:(id) parent withTag:(NSInteger) tagValue isAllDay:(BOOL) isAllDayLocal isAllDayShowing:(BOOL) isAllDayShowingNow;
@end

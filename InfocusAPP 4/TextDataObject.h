//
//  TextDataObject.h
//  Organizer
//
//  Created by Naresh Chouhan on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TextDataObject : NSObject 
{

	NSInteger textid;
	NSString *textinfo;
	
}

//Changed to ARC
/*
@property(nonatomic,retain)NSString *textinfo;
@property(readwrite)NSInteger textid;
*/

@property(nonatomic,strong)NSString *textinfo;
@property(readwrite)NSInteger textid;


@end

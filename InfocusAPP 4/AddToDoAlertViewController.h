//
//  AddToDoAlertViewController.h
//  Organizer
//
//  Created by Aman Gupta on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ListToDoViewController.h" //Steve

@protocol AlertDelegate <NSObject>

-(void) setAlertString_1:(NSString*) alertString_1 setAlertString_2:(NSString *) alertString_2 isAlertOn:(BOOL) isAlertOnOrOff;

@end

@interface AddToDoAlertViewController : UIViewController

{
    IBOutlet UINavigationBar *navBar;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *doneButton;
    
    IBOutlet UILabel *alertFixedLabel;
    IBOutlet UILabel *alertButtonFixedLabel;
    IBOutlet UILabel *secondAlertButtonFixedlabel;
  
}

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lblAlertDate;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lblSecondAlert;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *switchAlert;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *switchCalander;
@property (unsafe_unretained, nonatomic) IBOutlet UIDatePicker *datePicker;
@property(unsafe_unretained)			id		delegate;   
@property (nonatomic, strong) NSString *alertString;
@property (nonatomic, strong) NSString *alertStringSecond;

@property (strong, nonatomic) IBOutlet UIButton *btn_Alert;
@property (strong, nonatomic) IBOutlet UIButton *btn_SecondAlert;

@property(nonatomic, weak) id<AlertDelegate> alertDelegate;//Steve

- (IBAction)alertSwitch:(id)sender;
- (IBAction)showCalanderSwitch:(id)sender;
- (IBAction)dateChanged:(id)sender;
- (IBAction)backButton_Clicked:(id)sender;
- (IBAction)doneClicked:(id)sender;
//- (id)initWithAlertString:(NSString *) alert; //Steve commented
- (id)initWithAlertDate:(NSDate *) alert alertSecondDate:(NSDate *) alertSecond; //Steve changed

@end

id		__unsafe_unretained delegate;//Anil's Added
NSString *alertString;//Anil's Added
NSInteger selectedButton;
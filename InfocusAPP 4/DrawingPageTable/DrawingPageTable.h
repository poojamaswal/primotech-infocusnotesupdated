//
//  DrawingPageTable.h
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawingPageTableCell;


@protocol DrawingPagetableDelegate <NSObject>
- (void)addPage:(int)index;
- (void)deletePage:(int)index;
- (void)selectedPage:(int)index;
@end

@interface DrawingPageTable : UITableView <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) id<DrawingPagetableDelegate> delegateTable;


- (void)setCustomDefaults:(NSString*)str;
- (void)reloadDataAfterInsertDelete:(NSString*)str;

@end

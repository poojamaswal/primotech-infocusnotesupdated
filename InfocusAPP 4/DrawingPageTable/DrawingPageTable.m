//
//  DrawingPageTable.m
//  ACEDrawingViewDemo
//
//  Created by amit aswal on 1/11/16.
//  Copyright © 2016 Stefano Acerbetti. All rights reserved.
//

#import "DrawingPageTable.h"
#import "DrawingPageTableCell.h"
#import "DatabaseManager.h"

@interface DrawingPageTable ()<PageTableViewCellDelegate>
{
    NSArray *arrImages;
}

@end

@implementation DrawingPageTable

@synthesize delegateTable;


NSString * DrawingPageTableIdentifier = @"DrawingPageTableCell";
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(id)initTableWithFrame : (CGRect)frame_
{
    self = [super initWithFrame:frame_];
    if(self)
    {
        [self setCustomDefaults:@""];
    }
    return self;
}
- (void)setCustomDefaults:(NSString*)str
{
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.showsVerticalScrollIndicator = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.dataSource = (id)self;
    self.delegate = (id) self;
    self.bounces = YES;
    
    [self registerNib:[UINib nibWithNibName:DrawingPageTableIdentifier bundle:[NSBundle mainBundle]] forCellReuseIdentifier:DrawingPageTableIdentifier];
    
    
    arrImages = [[DatabaseManager sharedManager] fetchImagesOFSubPagesSavedIfAnyFromNotesID:str];
}

- (void)reloadDataAfterInsertDelete:(NSString*)str
{
    arrImages = [[DatabaseManager sharedManager] fetchImagesOFSubPagesSavedIfAnyFromNotesID:str];
    
    [self reloadData];
}

#pragma mark - Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrImages.count;
}

#pragma mark - Delegates


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DrawingPageTableCell *cell = (DrawingPageTableCell*)[tableView dequeueReusableCellWithIdentifier:DrawingPageTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[DrawingPageTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DrawingPageTableIdentifier];
    }
    
    [cell setCustomImage:[arrImages objectAtIndex:indexPath.row]] ;
    
    
    cell.addPageButton.tag = indexPath.row+1;
    cell.deletePageButton.tag = indexPath.row+1;
    cell.rowSelectionButton.tag = indexPath.row+1;
    
    cell.delegate = self;
    
    
    cell.imageView.layer.borderColor = [UIColor blackColor].CGColor;
    cell.imageView.layer.borderWidth = 1.0;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DrawingPageTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"%d",cell.addPageButton.tag);
    [delegateTable selectedPage:indexPath.row];
}

#pragma mark - PageTableViewCell Delegates
- (void)addPageAtIndex:(NSInteger)index
{
    [delegateTable addPage:index];
}

- (void)deletePageAtIndex:(NSInteger)index
{
    [delegateTable deletePage:index];
}

- (void)rowSelectedAtIndex:(NSInteger)index
{
    [delegateTable selectedPage:index];
}

@end

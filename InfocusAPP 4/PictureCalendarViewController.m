//
//  PictureCalendarViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 11/21/13.
//
//

#import "PictureCalendarViewController.h"
#import "CalendarParentController.h"
#import "MenuViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AddToDoStartEndViewController_iOS7.h"//Steve
#import <Accelerate/Accelerate.h> //Steve - for Blur
#import "UIImage+ImageEffects.h" //Steve - for Blur
#import <dispatch/dispatch.h>


@interface PictureCalendarViewController ()

@property (strong, nonatomic) IBOutlet UIBarButtonItem  *saveButton;
@property (strong, nonatomic) IBOutlet UIImageView      *imagePreviewImageView;
@property (strong, nonatomic) IBOutlet UITableView      *pictureTableView;
@property (strong, nonatomic) UIImage                   *originalImage;


@property (strong, nonatomic) IBOutlet UIView           *picturePreviewView;
@property (strong, nonatomic) IBOutlet UIView           *calendarMaskView;

@property (strong, nonatomic) IBOutlet UISwitch         *recommendedClaritySwitch;


@property (strong, nonatomic) UIImagePickerController   *imagePickerController;
@property (strong, nonatomic) UIImage                   *photoAlbumImage;
@property (strong, nonatomic) UIToolbar                 *toolBarForCropView;
@property (strong, nonatomic) UIView                    *cropSelectedPictureView;
@property (strong, nonatomic) UIImageView               *selectedImageView; //Selected imageview of Photo Album image picked
@property (strong, nonatomic) IBOutlet UIScrollView     *scrollview; //Scrollview for Storyboard and self.view
@property (strong, nonatomic) UIScrollView              *cropScrollview; //scrollview only for cropScrollView


@property (strong, nonatomic) IBOutlet UISlider         *blurSlider;
@property (strong, nonatomic) UIColor                   *tintColorForMask;

@property (strong, nonatomic) IBOutlet UIView           *scrollDownView;
@property (strong, nonatomic) IBOutlet UIImageView      *scrollDownImageView;
@property (strong, nonatomic) UIView                    *setImageHelpView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityOfBlur; //shows spinner



- (IBAction)saveButton_Clicked:(id)sender;
-(void)setupCalendarNumbers; //Set up Calendar numbers label in image
-(void)loadCropView; //a view that crops the image from photo library
-(void)cancelButton_Clicked;// cancels the image library photo picked
-(void)setButton_Clicked;//sets the image library photo picked
-(UIImage*) captureScreenShot; //Captures the screenshot of the image so it can use the users croppings for the image
-(void)calendarClarityForWallpaperRecommendedSettings;

- (IBAction)recommendedClaritySwitch_ValueChanged:(id)sender;
- (IBAction)blurSliderValueChanged:(id)sender;

-(void)setupHelpView;
-(void)helpView_Tapped; //help view for Non iPhone 5 devices so they know to scroll to adjust blur
-(void)helpViewForSetImage; //Help view after user picks a photo from photo album
-(void)helpViewHandleGesture; //Help view for SetImage

//blur image off main thread
- (void)applyBlurWithRadiusOnThread:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;


@end



@implementation PictureCalendarViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.autoresizingMask = NO;
    
    

    if(IS_IPAD)
    {
        _scrollview.contentSize = CGSizeMake(768, 959);
        _scrollview.frame = CGRectMake(0, 0, 768, 1024);
        _pictureTableView.frame = CGRectMake(_pictureTableView.frame.origin.x, _pictureTableView.frame.origin.y, 768, 86);
    }
    else
    {
        _scrollview.contentSize = CGSizeMake(320, 505);
        
        if (IS_IPHONE_5){
            _scrollview.frame = CGRectMake(0, 0, 320, 568);
        }
        else{
            _scrollview.frame = CGRectMake(0, 0, 320, 480);
        }
        _pictureTableView.frame = CGRectMake(_pictureTableView.frame.origin.x, _pictureTableView.frame.origin.y, 320, 86);
    }
    
    [self setupCalendarNumbers]; //Set up numbers label in image over CalendarMaskView
    
    
    
    _pictureTableView.delegate = self;
    _pictureTableView.dataSource = self;
    _pictureTableView.layer.borderWidth = 0.5;
    _pictureTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [_scrollview addSubview:_pictureTableView];
    
}


//Set up numbers label in image
-(void)setupCalendarNumbers{
    
    for (int i = 1; i <= 31; i++) {
        
        
        UILabel *calendarNumbersLabel = [[UILabel alloc]init];
        calendarNumbersLabel.textColor = [UIColor whiteColor];
        calendarNumbersLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        
        
        NSString *labelNumber = [NSString stringWithFormat:@"%d",i];
        calendarNumbersLabel.text = labelNumber;
        
        int x;
        int y;
        int gap;
        int xOriginalPosition;
        
        
        if (i == 1) { //initial values
            xOriginalPosition = _calendarMaskView.frame.origin.x + 10;
            x = xOriginalPosition;
            y = _calendarMaskView.frame.origin.y + 5;
            if (IS_IPAD)
            {
                gap = 45;
            }else
            {
               gap = 25;
            }
            
        }
        
        
        if (i > 1 && i <= 7){
            x = x + gap;
        }
        else if (i > 7 && i <= 14){
            
            if (i == 8) {
                x = xOriginalPosition;
                y = y + gap;
            }
            else{
                x = x + gap;
            }
            
            
        }
        else if (i > 14 && i <= 21){
            
            x = x + gap;
            
            if (i == 15) {
                x = xOriginalPosition;
                y = y + gap;
            }
            
        }
        else if (i > 21 && i <= 28){
            
            x = x + gap;
            
            if (i == 22) {
                x = xOriginalPosition;
                y = y + gap;
            }
            
        }
        else if (i > 28 && i <= 31){
            
             x = x + gap;
            
            if (i == 29) {
                x = xOriginalPosition;
                y = y + gap;
            }
            
        }
        
        calendarNumbersLabel.frame = CGRectMake(x, y, 21, 21);
        
        [_picturePreviewView addSubview:calendarNumbersLabel];
        
        
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [_activityOfBlur stopAnimating];
    if(IS_IPAD)
    {
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        _saveButton.tintColor = [UIColor whiteColor];
    }
    else
    {
        _saveButton.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.edgesForExtendedLayout = UIRectEdgeAll;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    _calendarMaskView.alpha = 0;

    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    NSString *defaultCalendarImageName = [sharedDefaults stringForKey:@"CalendarImageName"]; //Gets default name of Calendar Image
    
    
    ///////// Help Image & View //////////////////
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_PhotoCalendar"]) {
        if(IS_IPAD)
        {
             _scrollDownImageView.hidden = YES;
        }
        else
        {
            if (IS_IPHONE_5){
                
                _scrollDownImageView.hidden = YES;
            }
            else{
                
                _scrollDownImageView.hidden = NO;
                [self setupHelpView]; //sets up HelpView
            }
        }
    }
    else{
        
        _scrollDownImageView.hidden = YES;
    }
    ////////// Help Image & View End ///////////////
    
    
    
    if ([sharedDefaults boolForKey:@"CalendarClarityRecommended"]) {
        
        _recommendedClaritySwitch.on = YES;
    }
    else{
        
        _recommendedClaritySwitch.on = NO;
    }
    
    
    
    if ([defaultCalendarImageName isEqualToString:@"CustomCalendarPicture.jpg"]) { //****** Custom Calendar Pictures from Photo Albums *****
        
        //************** Search for Save Calendar Picture *********************

        //get the documents directory:
        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        
        //make a directory & file name to write the data to using the documents directory:
        NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
        NSString *fileName = [NSString stringWithFormat:@"%@/CustomCalendarPicture.jpg",directoryName];
        
        UIImage *customCalendarImage = [UIImage imageWithContentsOfFile:fileName]; //Get the image

        
        //set imageView with image found
        _imagePreviewImageView.image = customCalendarImage;
        _originalImage = customCalendarImage;
        
        
        
        if ([sharedDefaults boolForKey:@"CalendarClarityRecommended"]) {  // Recommended settings is on
            
            _recommendedClaritySwitch.on = YES;
            [sharedDefaults setBool:YES forKey:@"CalendarClarityRecommended"];
            
            _blurSlider.value = 8;
            [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
            
            
            //Apply Blur and mask to image
            UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
            [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:2.0 maskImage:nil]; ////updates value of _imagePreviewImageView.image
            
           // _imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
            
        }
        else{ //Custom Settings
            
            
            _blurSlider.value = [sharedDefaults floatForKey:@"CalendarImageBlur"];

            
            //Apply Blur and mask to image
            UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
            [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:2.0 maskImage:nil]; //updates value of _imagePreviewImageView.image
            
           // _imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
            
        }

        
    }
    else{ //********** Wallpaper Images ****************
        
        _imagePreviewImageView.image = [UIImage imageNamed:defaultCalendarImageName];
        _originalImage = [UIImage imageNamed:defaultCalendarImageName];
        
        
        //Check Recommended settings is on or off
        if ([sharedDefaults boolForKey:@"CalendarClarityRecommended"]) {
            
            [self calendarClarityForWallpaperRecommendedSettings]; //method that sets the calendar for each wallpaper
            
        }
        else{ //Custom Settings
            
            [self defaultCalendarClaritySettings]; //method sets default Calendar Clarity settings

        }
        
    }

    _calendarMaskView.alpha = 0;
}



-(void)setupHelpView{
    
    UITapGestureRecognizer *tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped)];
    UISwipeGestureRecognizer *swipeDownHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped)];
    UISwipeGestureRecognizer *swipeUpHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped)];
    UISwipeGestureRecognizer *swipeRightHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped)];
    UISwipeGestureRecognizer *swipeLeftHelpView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped)];
    
    swipeDownHelpView.direction = UISwipeGestureRecognizerDirectionDown;
    swipeUpHelpView.direction = UISwipeGestureRecognizerDirectionUp;
    swipeLeftHelpView.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRightHelpView.direction = UISwipeGestureRecognizerDirectionRight;
    
    [_scrollDownView addGestureRecognizer:swipeDownHelpView];
    [_scrollDownView addGestureRecognizer:swipeUpHelpView];
    [_scrollDownView addGestureRecognizer:swipeLeftHelpView];
    [_scrollDownView addGestureRecognizer:swipeRightHelpView];
    [_scrollDownView addGestureRecognizer:tapHelpView];
    
}


-(void)helpView_Tapped{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [_scrollDownView.layer addAnimation:transition forKey:kCATransitionFade];
    
    _scrollDownView.hidden = YES;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendar"];

}


-(void)defaultCalendarClaritySettings{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    _blurSlider.value = [sharedDefaults floatForKey:@"CalendarImageBlur"];
    
    //Apply Blur and mask to image
    UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
    [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:2.0 maskImage:nil]; ////updates value of _imagePreviewImageView.image
}



//if user clicks recommended settings, these are the settings
-(void)calendarClarityForWallpaperRecommendedSettings{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    //[_activityOfBlur stopAnimating];
    
    float standardBlurValue = 8.0;
    float saturationColor = 2.0;
    
    
    if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_1.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        //[self blurImageOnThread:_blurSlider.value];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
        
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_2.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
        
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_3.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_4.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_5.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_6.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_7.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
        
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_8.jpg"]) {
        
        _blurSlider.value = standardBlurValue + 5;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_9.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_10.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_11.jpg"]) {
        
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_12.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_13.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_14.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_15.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_16.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_17.jpg"]) {
  
        _blurSlider.value = standardBlurValue;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
       
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_18.jpg"]) {
 
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_19.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_20.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    else if ( [[sharedDefaults stringForKey:@"CalendarImageName"]  isEqual: @"CalendarImage_21.jpg"]) {
        
        _blurSlider.value = 0;
        [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
        
        
        //Apply Blur and mask to image
        //UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
        //[self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:saturationColor maskImage:nil]; ////updates value of _imagePreviewImageView.image
        
        //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
    }
    
    
    
}



#pragma mark - Table view data source

 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {

     return 1;
 }

 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
     return 2;
 }

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    tableView.backgroundColor = [UIColor clearColor];
    
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {

     static NSString *cellIdentifier = @"Cell";
     
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
     
     if (cell == nil) {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
     }
     
     
     //UITableViewCellStyleDefault
 
     if (indexPath.row == 0) {
         
         cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
         cell.selectionStyle =UITableViewCellSelectionStyleGray;
         
        cell.textLabel.text = @"Wallpaper";
     }
     else if (indexPath.row == 1){
         
         cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
         cell.selectionStyle =UITableViewCellSelectionStyleGray;
         
         cell.textLabel.text = @"Photo Album";
     }
 
 return cell;
 }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.row == 0) {
        
        [self performSegueWithIdentifier:@"ChooseWallpaper" sender:self];
    }
    else if (indexPath.row == 1){
        
        UIImagePickerControllerSourceType type = UIImagePickerControllerSourceTypePhotoLibrary;
        BOOL ok = [UIImagePickerController isSourceTypeAvailable:type];
        
        
        //********* Add UIAlert here??? *******
        if (!ok) {
            NSLog(@"alas");
            return;
        }
        
        //Opens Photo Albums (UIImagePickerController)
        _imagePickerController = [UIImagePickerController new];
        _imagePickerController.sourceType = type;
        _imagePickerController.allowsEditing = NO;
        _imagePickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:type];
        _imagePickerController.delegate = self;
        
        
        [self presentViewController:_imagePickerController animated:YES completion:nil];
    }
    
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.row == 0) {
        
        [self performSegueWithIdentifier:@"ChooseWallpaper" sender:self];
    }
    
    
}

//pooja-iPad
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    if(IS_IPAD)
    {
        [navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
        navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    _photoAlbumImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    //NSLog(@"image.size = %@", NSStringFromCGSize(_photoAlbumImage.size));

    
    [self loadCropView];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    _imagePickerController = nil;
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    _imagePickerController = nil;
}


//Crops the photo Album image selected - This view is Not in Storyboard.  All done programically
-(void)loadCropView{
    
    self.navigationController.navigationBarHidden = YES;
    
    //Add the view
    _cropSelectedPictureView = [[UIView alloc]initWithFrame:self.view.frame];
    _cropSelectedPictureView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_cropSelectedPictureView];
    
    //Add Scroll
    _cropScrollview = [[UIScrollView alloc]initWithFrame:self.view.frame];
    _cropScrollview.delegate = self;
    _cropScrollview.minimumZoomScale = 1;
    _cropScrollview.maximumZoomScale = 4;
    [_cropSelectedPictureView addSubview:_cropScrollview];
    
    
    if (_photoAlbumImage.size.height > _photoAlbumImage.size.width) { // image is Portrait stlye
        
        //Set image from photo album to a UIImageView to fit self.view since it's a Portrait Style image
        _selectedImageView = [[UIImageView alloc]initWithFrame:self.view.frame];
        [_selectedImageView setImage:_photoAlbumImage];
        _selectedImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill
        [_cropScrollview addSubview:_selectedImageView];
        
        _cropScrollview.contentSize = self.view.frame.size;//Scrollview Content size
    }
    else{ //image is Landscape Style
        
        //Set new size to Landscape by flipping coordinates
        CGSize newImageViewSize = CGSizeMake(self.view.frame.size.height, self.view.frame.size.width);
        
        //Set image from photo album to a UIImageView
        _selectedImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, newImageViewSize.height, newImageViewSize.width)]; //Put imageview in landscape
        [_selectedImageView setImage:_photoAlbumImage];
        _selectedImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill, UIViewContentModeScaleAspectFill
        [_cropScrollview addSubview:_selectedImageView];
        
        _cropScrollview.contentSize = newImageViewSize; //Scrollview Content size
        _cropScrollview.contentInset = UIEdgeInsetsMake(0, 300, 0, 300); //Allows ability to scroll sideways over a larger range
        
    }
    
    
    //Adds a bar on bottom of screen
    _toolBarForCropView = [UIToolbar new];
    _toolBarForCropView.frame = CGRectMake(0, self.view.frame.size.height - 44, 320, 44);
    _toolBarForCropView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
    _toolBarForCropView.barTintColor = [UIColor whiteColor];
    
    [_cropSelectedPictureView addSubview:_toolBarForCropView];
    
    
    //Cancel Buttons
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButton_Clicked)];
    cancelButton.tintColor = [UIColor blackColor];
    
    //Set Buttons
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Set" style:UIBarButtonItemStyleBordered target:self action:@selector(setButton_Clicked)];
    setButton.tintColor = [UIColor blackColor];
    
    //Fixed Bar Button
    UIBarButtonItem *fixedBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedBarButton.width = 42;
    
    //Flexible Bar Button
    UIBarButtonItem *flexibleBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *toolbarItemsArray = [[NSArray alloc]initWithObjects:fixedBarButton, cancelButton, flexibleBarButton, setButton, fixedBarButton, nil];
    _toolBarForCropView.items = toolbarItemsArray;
    
    
    
    [self helpViewForSetImage]; //brings up help view
    
}


-(void)helpViewForSetImage{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_PhotoCalendarSetImage"]) {
        
        
        _setImageHelpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _cropSelectedPictureView.frame.size.width, _cropSelectedPictureView.frame.size.height - 44)];
        _setImageHelpView.backgroundColor = [UIColor clearColor];
        
        UITapGestureRecognizer   *tapHelp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewHandleGesture)];
        UISwipeGestureRecognizer *swipeDownHelp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewHandleGesture)];
        UISwipeGestureRecognizer *swipeUpHelp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewHandleGesture)];
        UISwipeGestureRecognizer *swipeRightHelp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewHandleGesture)];
        UISwipeGestureRecognizer *swipeLeftHelp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewHandleGesture)];
        
        swipeDownHelp.direction = UISwipeGestureRecognizerDirectionDown;
        swipeUpHelp.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelp.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelp.direction = UISwipeGestureRecognizerDirectionRight;
        
        [_setImageHelpView addGestureRecognizer:swipeDownHelp];
        [_setImageHelpView addGestureRecognizer:swipeUpHelp];
        [_setImageHelpView addGestureRecognizer:swipeLeftHelp];
        [_setImageHelpView addGestureRecognizer:swipeRightHelp];
        [_setImageHelpView addGestureRecognizer:tapHelp];
        
        [_cropSelectedPictureView addSubview:_setImageHelpView];
        
    
        
        UIImage *zoomImage = [UIImage imageNamed:@"zoomHelpImage.png"];
        UIImageView *zoomImageView = [[UIImageView alloc]initWithFrame:CGRectMake(320/2 - zoomImage.size.width/2,
                                                                                 _toolBarForCropView.frame.origin.y - 375,
                                                                                 zoomImage.size.width,
                                                                                  zoomImage.size.height)];
        zoomImageView.image = zoomImage;
        [_setImageHelpView addSubview:zoomImageView];
        
        
        
        if (_photoAlbumImage.size.width > _photoAlbumImage.size.height) { //Landscape Image (Width > Heigh)
            
            UIImage *panImage = [UIImage imageNamed:@"panHelpImage"];
            
            //******* panImageView frame is not correct.  Y = 536 in reg iPhone
            
            UIImageView *panImageView = [[UIImageView alloc]initWithFrame:CGRectMake(320/2 -  panImage.size.width/2,
                                                                                     _toolBarForCropView.frame.origin.y - 200,
                                                                                     panImage.size.width,
                                                                                     panImage.size.height)];
            panImageView.image = panImage;
            [_setImageHelpView addSubview:panImageView];
            
        }
    }
    
}

-(void)helpViewHandleGesture{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [_setImageHelpView.layer addAnimation:transition forKey:kCATransitionFade];
    
    _setImageHelpView.hidden = YES;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"FirstLaunch_PhotoCalendarSetImage"];
}



// cancels the image library photo picked
-(void)cancelButton_Clicked{
    
    //[self helpViewHandleGesture]; //removes Help on SetImage view
     [_setImageHelpView removeFromSuperview];
    
    self.navigationController.navigationBarHidden = NO;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    _cropSelectedPictureView.frame = _cropSelectedPictureView.frame;
    _cropSelectedPictureView.frame = CGRectMake(_cropSelectedPictureView.frame.origin.x, _cropSelectedPictureView.frame.size.height,
                                                _cropSelectedPictureView.frame.size.width, _cropSelectedPictureView.frame.size.height);
    
    [UIView commitAnimations];
}


//sets the image library photo picked
-(void)setButton_Clicked{
    
   // [self helpViewHandleGesture]; //removes help on SetImage veiw
    [_setImageHelpView removeFromSuperview];
    

    self.navigationController.navigationBarHidden = NO;
    _imagePreviewImageView.image = _selectedImageView.image;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *customPhotoSelected = @"CustomCalendarPicture.jpg"; //save as custom name so can identify later
    [sharedDefaults setValue:customPhotoSelected forKey:@"CalendarImageName"];
    
    
    //capture the screenshot image user selected with it's cropping
    UIImage *updatedImage = [self captureScreenShot];
    
    //update the preview image
    _imagePreviewImageView.image = updatedImage;
    _originalImage = updatedImage;
    
    //Apply Blur and mask to image
    UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
    _imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];//Blur method
    
    
     //*************** Save  Photo to Documents as Not Blurred ***********************
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSData      *imageData = UIImageJPEGRepresentation(updatedImage, 1.0); //Convets image to NSData
    NSError     *error;
    
    
    //get the documents directory:
    NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    //make a directory & file name to write the data to using the documents directory:
    NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
    NSString *fileName = [NSString stringWithFormat:@"%@/CustomCalendarPicture.jpg",directoryName];
    
    //Create Directory Folder & then write content to file
    [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
    
    [imageData writeToFile:fileName atomically:YES];
    
    //*************** End Save  Photo to Documents  ***********************
    
    
    //Animation
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    _cropSelectedPictureView.frame = _cropSelectedPictureView.frame;
    _cropSelectedPictureView.frame = CGRectMake(_cropSelectedPictureView.frame.origin.x, _cropSelectedPictureView.frame.size.height,
                                                _cropSelectedPictureView.frame.size.width, _cropSelectedPictureView.frame.size.height);
    
    [UIView commitAnimations];
    
}


-(UIImage*) captureScreenShot{
    

    [_toolBarForCropView removeFromSuperview]; //remove Toolbar with the "Set" button before capturing screen shot
    [_setImageHelpView removeFromSuperview]; //remove set HelpView
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    UIGraphicsBeginImageContextWithOptions(window.frame.size, NO,[[UIScreen mainScreen] scale]);
    [self.view drawViewHierarchyInRect:window.frame afterScreenUpdates:YES];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();// Get the snapshot
    UIGraphicsEndImageContext();
    
    return snapshotImage;
    
}


-(UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    
    return _selectedImageView;
}


- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
    
}


-(void) scrollViewDidScroll:(UIScrollView *)scrollView{

    if (scrollView.contentOffset.y > -59) { //determine when scroll moves 5 points. Starts at -64
        
        [self helpView_Tapped];//remove help view
    }
    
}

- (IBAction)saveButton_Clicked:(id)sender {
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        [sharedDefaults setBool:NO forKey:@"CalendarTypeClassic"];
    }
    
    
    //Gets default name of Calendar Image
    NSString *defaultCalendarImageName = [sharedDefaults stringForKey:@"CalendarImageName"];
    NSString *defaultCalendarImageNameBlurred = [sharedDefaults stringForKey:@"CalendarImageName_Blurred"];
    

    if ([defaultCalendarImageName isEqualToString:@"CustomCalendarPicture.jpg"] || [defaultCalendarImageNameBlurred isEqualToString:@"CustomCalendarPicture.jpg"]){ //Custom image blur saved
        NSLog(@"here");
        
    }
    else{ //Wallpaper image blur saved
        
    }
    
    
    //Apply Blur and mask to image
    UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
    _imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];//Blur method
    
    
    //*************** Save  Photo to Documents as BLURRED ***********************
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSData      *imageDataBlurred = UIImageJPEGRepresentation(_imagePreviewImageView.image, 1.0); //Convets image to NSData
    NSError     *error;
    
    
    //get the documents directory:
    NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [path objectAtIndex:0];
    
    //make a directory & file name to write the data to using the documents directory:
    NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
    NSString *fileNameForBlurredImage = [NSString stringWithFormat:@"%@/CalendarImageName_Blurred.jpg",directoryName];
    
    //Create Directory Folder & then write content to file
    [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
    
    [imageDataBlurred writeToFile:fileNameForBlurredImage atomically:YES];
    
    [sharedDefaults setBool:YES forKey:@"CalendarImageName_Blurred.jpg"]; //now we know if blurred image was saved
    
    NSString *customPhotoSelectedBlurred = @"CalendarImageName_Blurred.jpg"; //save as custom name so can identify later
    [sharedDefaults setValue:customPhotoSelectedBlurred forKey:@"CalendarImageName_Blurred"];
    
    //*************** End Save  Photo to Documents  ***********************

    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)recommendedClaritySwitch_ValueChanged:(id)sender {
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (_recommendedClaritySwitch.on) {
        
         NSString *defaultCalendarImageName = [sharedDefaults stringForKey:@"CalendarImageName"]; //Gets default name of Calendar Image
        
        
        if ([defaultCalendarImageName isEqualToString:@"CustomCalendarPicture.jpg"]){
            
            [sharedDefaults setBool:YES forKey:@"CalendarClarityRecommended"];
            
            _blurSlider.value = 8;
            [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
            
            
            //Apply Blur and mask to image
            UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
            [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil]; ////updates value of _imagePreviewImageView.image
            //_imagePreviewImageView.image = [_originalImage applyBlurWithRadius:_blurSlider.value tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];//Blur method
        }
        else{//Wallpaper
            
            [sharedDefaults setBool:YES forKey:@"CalendarClarityRecommended"];
            [self calendarClarityForWallpaperRecommendedSettings]; //method for recommendated setting

        }
        
    }
    else{
        
        [sharedDefaults setBool:NO forKey:@"CalendarClarityRecommended"];
        [self defaultCalendarClaritySettings]; //Method for default settings set by user
    }
    
 
}

- (IBAction)blurSliderValueChanged:(id)sender {
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"CalendarClarityRecommended"]) {
        
        _recommendedClaritySwitch.on = NO; //shuts off recommended switch if user attempts to customize
        [sharedDefaults setBool:NO forKey:@"CalendarClarityRecommended"];
    }
    
    [sharedDefaults setFloat:_blurSlider.value forKey:@"CalendarImageBlur"];
    
    
    //Apply Blur and mask to image
    _tintColorForMask = [UIColor colorWithWhite:0.05 alpha:0.125];
    [self applyBlurWithRadiusOnThread:_blurSlider.value tintColor:_tintColorForMask saturationDeltaFactor:1.5 maskImage:nil]; ////updates value of _imagePreviewImageView.image
    
    
}



//Steve - copied Apple's blur technique, but added it on a thread.  Couldn't add the actual method on a thread since it returns an image. "return" doesn't work
//on Threads since it returns before the thread values change
- (void)applyBlurWithRadiusOnThread:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage
{
    
    CGSize size =  _originalImage.size;
    
    // Check pre-conditions.
    if (size.width < 1 || size.height < 1) {
        NSLog (@"*** error: invalid size: (%.2f x %.2f). Both dimensions must be >= 1: %@", size.width, size.height, _originalImage);
        return;
    }
    
    if (!_originalImage.CGImage) {
        NSLog (@"*** error: image must be backed by a CGImage: %@", self);
        return;
    }
    
    if (maskImage && !maskImage.CGImage) {
        NSLog (@"*** error: maskImage must be backed by a CGImage: %@", maskImage);
        return;
    }
    
    
    //Shows "Blur Processing..."
    //[self performSelector:@selector(blurProcessingViewAfterDelay) withObject:nil afterDelay:1.0];
    //_activityOfBlur.hidden = NO;
    //[_activityOfBlur startAnimating];//Spinner
    
    
    __block CGRect imageRect = { CGPointZero, _originalImage.size };
    __block UIImage *effectImage = [UIImage new];

    
    
    dispatch_queue_t fetchingEvents_queue = dispatch_queue_create("ApplyBlurWithRadiusThread", NULL);
    dispatch_async(fetchingEvents_queue, ^{
        
        
        BOOL hasBlur = blurRadius > __FLT_EPSILON__;
        BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
        
        if (hasBlur || hasSaturationChange) {
            UIGraphicsBeginImageContextWithOptions(_originalImage.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef effectInContext = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(effectInContext, 1.0, -1.0);
            CGContextTranslateCTM(effectInContext, 0, -_originalImage.size.height);
            CGContextDrawImage(effectInContext, imageRect, _originalImage.CGImage);
            
            vImage_Buffer effectInBuffer;
            effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
            effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
            effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
            effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
            
            UIGraphicsBeginImageContextWithOptions(_originalImage.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
            vImage_Buffer effectOutBuffer;
            effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
            effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
            effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
            effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
            
            if (hasBlur) {
                // A description of how to compute the box kernel width from the Gaussian
                // radius (aka standard deviation) appears in the SVG spec:
                // http://www.w3.org/TR/SVG/filters.html#feGaussianBlurElement
                //
                // For larger values of 's' (s >= 2.0), an approximation can be used: Three
                // successive box-blurs build a piece-wise quadratic convolution kernel, which
                // approximates the Gaussian kernel to within roughly 3%.
                //
                // let d = floor(s * 3*sqrt(2*pi)/4 + 0.5)
                //
                // ... if d is odd, use three box-blurs of size 'd', centered on the output pixel.
                //
                CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
                
                NSUInteger radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
                
                if (radius % 2 != 1) {
                    radius += 1; // force radius to be odd so that the three box-blur methodology works.
                }
                vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
                vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
                vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            }
            
            BOOL effectImageBuffersAreSwapped = NO;
            
            if (hasSaturationChange) {
                CGFloat s = saturationDeltaFactor;
                CGFloat floatingPointSaturationMatrix[] = {
                    0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                    0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                    0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                    0,                    0,                    0,  1,
                };
                const int32_t divisor = 256;
                NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
                int16_t saturationMatrix[matrixSize];
                
                for (NSUInteger i = 0; i < matrixSize; ++i) {
                    saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
                }
                
                if (hasBlur) {
                    vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                    effectImageBuffersAreSwapped = YES;
                }
                else {
                    vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                }
            }
            
            
            if (!effectImageBuffersAreSwapped)
                effectImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            if (effectImageBuffersAreSwapped)
                effectImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // Set up output context.
            UIGraphicsBeginImageContextWithOptions(_originalImage.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef outputContext = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(outputContext, 1.0, -1.0);
            CGContextTranslateCTM(outputContext, 0, -_originalImage.size.height);
            
            // Draw base image.
            CGContextDrawImage(outputContext, imageRect, _originalImage.CGImage);
            
            // Draw effect image.
            if (hasBlur) {
                CGContextSaveGState(outputContext);
                
                if (maskImage) {
                    CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
                }
                
                CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
                CGContextRestoreGState(outputContext);
            }
            
            // Add in color tint.
            if (tintColor) {
                CGContextSaveGState(outputContext);
                CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
                CGContextFillRect(outputContext, imageRect);
                CGContextRestoreGState(outputContext);
            }
            
            
            // Output image is ready.
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            _imagePreviewImageView.image = outputImage;
            //[_activityOfBlur stopAnimating];
            
            
        });
    });
    
    
}




-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    _originalImage = nil;
    _imagePreviewImageView.image = nil; //.image??
    _photoAlbumImage = nil;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@" PictureCalendarCV didReceiveMemoryWarning");
}




@end


//  CalendarParentController.m
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarParentController.h"
#import "AppointmentViewController .h"
#import "ToDoParentController.h"
#import "TabsViewController.h"
#import "OrganizerAppDelegate.h"
#import "ProjectMainViewCantroller.h"
#import "LIstToDoProjectViewController.h"
#import "QuantcastMeasurement.h"
#import "Flurry.h" //Steve
#import "SWRevealViewController.h" //Steve
#import "CalendarMonthViewController.h" //Steve
#import "Appirater.h" //Steve
#import "PasswordProtectAppViewController.h" //Steve
#import "CalendarViewController.h" //Steve
#import <Accelerate/Accelerate.h> //Steve - for Blur
#import "UIImage+ImageEffects.h" //Steve - for Blur
#import "NewFeaturesViewController.h" //Steve - shows new Features
#import <LocalAuthentication/LocalAuthentication.h> //Steve - TouchID
#import "PostToFaceBookViewController.h"

#define SUBVC_BG_TAG	901290

//static int preSelectedIndex = MONTH_BUTTON_TAG; //Start view
static int preSelectedIndex = MONTH_BUTTON_TAG; //Start view

@interface CalendarParentController()
{
    
    __weak IBOutlet UIView *_vwIPad;
    UISegmentedControl *segmentedControl;
    UISplitViewController* splitVC;
    UIWindow * window;
    
}
@property (strong, nonatomic) IBOutlet UIImageView      *calendarHelpImageView;//Steve
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *searchButton;//Steve
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *calendarsButton;//Steve
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *textToSpeakButton;//Steve
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *todayButton;//Steve

@property (strong, nonatomic) UIImageView               *backgroundPictureImageView; //Steve - the background picture

@property (strong, nonatomic) NSTimer *refreshTimer;




-(void)createSegments;
-(void)showSearchOptPopover:(BOOL) show ;
-(void)calendarPrivacySettingsInstructions; //Steve
-(void)openCalendarSelection;//Steve
-(IBAction)calendarChoices:(UIBarButtonItem *)sender; //Steve
-(void)settingsButton_Clicked;//Steve
- (void)applyBlurWithRadiusOnThread:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;//Steve
-(void)refreshCalendar; //Steve - uses NSTimer to refresh Calendar

@end

@implementation CalendarParentController
// aCalListViewController changed Class CalendarListViewController to CalendarList2ViewController

@synthesize aCalListViewController,aCalDailyViewController,aCalWeekViewController,aCalMonthViewController, notification;
@synthesize calling_VC;//Aman's Added
@synthesize isHandwritingView;
@synthesize aBottomToolBar;//Steve

#pragma mark -
#pragma mark VC Life Cycle
#pragma mark -


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
    }
    return self;
}

- (void)viewDidLoad  {
    [super viewDidLoad];
    //NSLog(@"viewDidLoad CalendarParentController");
    
    
    self.view.userInteractionEnabled = YES;
    isMenuClosed = YES; // if viewDidLoad method is called, then Menu is closed.  Used to set userEndabled to YES when Reveal button clicked (revealToggleHandle)
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
     
    [[QuantcastMeasurement sharedInstance] logEvent:@"Calendar" withLabels:Nil]; //Steve
    [Flurry logEvent:@"Calendar"]; //Steve
    

    
    //Steve - loads password viewController
    if ( ![sharedDefaults boolForKey:@"PasswordAccepted"] && [sharedDefaults boolForKey:@"NavigationNew"]){
        PasswordProtectAppViewController *passwordProtectAppViewController;
         //pooja-iPad
        if(IS_IPAD)
        {
            passwordProtectAppViewController=
            [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController_iPad" bundle:[NSBundle mainBundle]];
        }else
        {
            passwordProtectAppViewController=
        [[PasswordProtectAppViewController alloc]initWithNibName:@"PasswordProtectAppViewController" bundle:[NSBundle mainBundle]];
        
        }
        
        [self.navigationController pushViewController:passwordProtectAppViewController animated:NO];
       
    }
    
    
    //Steve added - selects the users default
    if ([sharedDefaults integerForKey:@"DefaultCalendar"] == LIST_BUTTON_TAG)
        preSelectedIndex = LIST_BUTTON_TAG;
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == DAY_BUTTON_TAG)
        preSelectedIndex = DAY_BUTTON_TAG;
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == WEEK_BUTTON_TAG)
        preSelectedIndex = WEEK_BUTTON_TAG;
    else if ([sharedDefaults integerForKey:@"DefaultCalendar"] == MONTH_BUTTON_TAG)
        preSelectedIndex = MONTH_BUTTON_TAG;

    //Steve - converts preSelectedIndex to UISegmentedControl values
    // See  Organizer_Prefix.pch for define list
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
     
        switch (preSelectedIndex) {
            case 70001: //LIST_BUTTON_TAG
                preSelectedIndex = 0;
                break;
               
            case 70002: //DAY_BUTTON_TAG
                preSelectedIndex = 1;
                break;
                
            case 70003: //WEEK_BUTTON_TAG
                preSelectedIndex = 2;
                break;
                
            case 70004: //MONTH_BUTTON_TAG
                preSelectedIndex = 3;
                break;
                
            default:
                break;
        }
        
    }
    
    //NSLog(@"preSelectedIndex = %d ", preSelectedIndex);
    
    //Alok Gupta added to remove icon for List Version
    if(IS_LIST_VERSION)
    {
        //Added Tags On XIB -> Home icon 1111, Note icon 1112,List Todo icon 1113,TODO icon 1114,Project Icon 1115
        [[self.view viewWithTag:1111] removeFromSuperview];
        [[self.view viewWithTag:1112] removeFromSuperview];
        [[self.view viewWithTag:1114] removeFromSuperview];
        [[self.view viewWithTag:1115] removeFromSuperview];
        [[self.view viewWithTag:1113] setFrame:CGRectMake(5,5,43,34)];
    }
    else if (IS_TO_DO_VERSION){ //Steve Added 
        //Tags On XIB -> Home icon 1111, Note icon 1112,List Todo icon 1113,TODO icon 1114,Project Icon 1115
        [[self.view viewWithTag:1111] removeFromSuperview];
        [[self.view viewWithTag:1112] removeFromSuperview];
        [[self.view viewWithTag:1113] removeFromSuperview];
        [[self.view viewWithTag:1115] removeFromSuperview];
        [[self.view viewWithTag:1114] setFrame:CGRectMake(5,5,43,34)];
    }
    else if (IS_PROJECTS_VER){ //Steve Added 
        //Tags On XIB -> Home icon 1111, Note icon 1112,List Todo icon 1113,TODO icon 1114,Project Icon 1115
        [[self.view viewWithTag:1111] removeFromSuperview];
        [[self.view viewWithTag:1112] removeFromSuperview];
        [[self.view viewWithTag:1113] removeFromSuperview];
        [[self.view viewWithTag:1114] removeFromSuperview];
        [[self.view viewWithTag:1115] setFrame:CGRectMake(5,5,43,34)];
    }
    
    
    //CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    
   // NSLog(@"frame is %@",NSStringFromCGRect(statusBarFrame));
    
	[self addAdditionalControlsInCalenderParent];
    
	appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	notification = [[UILocalNotification alloc] init];
    
    
    ////////// //////////////////////////////////////////////////////////
    ////////// Steve to request permission to access eventStore /////////
    /////////////////////////////////////////////////////////////////////
    
    //Steve added - notifies when events have been loaded to main thread
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.Elixir.EventStorePermisssionGranted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UploadSqlEventToEventStor)
                                                 name:@"com.Elixir.EventStorePermisssionGranted" object:nil];
    
   
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    eventStore = appDelegate.eventStore;
    if (eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
     
    
    //EKAuthorizationStatus authStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    //NSLog(@"EKAuthorizationStatus #1 = %d", authStatus);
    
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        // iOS 6 and later - request permission to eventStore
        //NSLog(@"iOS 6 --> CalendarParentController");
        
        //Cpmpletion Block used so it waits for permission to be granted before accessing eventStore
        //Otherwise will trigger an error
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
         {
             
             if (granted == TRUE) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //NSLog(@"granted == TRUE --> CalendarParentController");
                     // NSLog(@"EKAuthorizationStatus granted == True -->AuthStatus =  %d", authStatus);
                     
                     //Steve - resets eventStore once permission is granted
                     //Otherwise events won't show on 1st download until app is re-set or out of multi-tasking memory
                     [eventStore reset];
                     
                     appDelegate.isEventStorePermissionGranted = YES;
                     NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults]; 
                     
                     if ([sharedDefaults boolForKey:@"FirstLaunch_EventStore"])
                     {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"com.Elixir.EventStorePermisssionGranted" object:self];
                     }
                     
                 });
             }
             
             if (granted == FALSE) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //NSLog(@"granted == FALSE --> CalendarParentController");
                     
                     appDelegate.isEventStorePermissionGranted = NO;
                     [self calendarPrivacySettingsInstructions]; //Instructions on turning on Calendar in Privacy settings
                 });
             }
             
         
             
         }]; //end block after eventStore has permission
    } else {
        // iOS 5 - Permission is not needed
        //NSLog(@"iOS 5  --> CalendarParentController");
        
        appDelegate.isEventStorePermissionGranted = YES; //iOS 5 all apps automatically have permission
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"FirstLaunch_EventStore"])
        {
            //[self UploadSqlEventToEventStor];
            [self performSelector:@selector(UploadSqlEventToEventStor) withObject:nil afterDelay:0.02];
            
        }
        
    }
    
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    eventStore = appDelegate.eventStore;
    if (eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
    
     [eventStore refreshSourcesIfNecessary];  //Steve - Helps Refresh Calendar
    
    //NSLog(@"CalendarParentController --> appDelegate.isEventStorePermissionGranted = %d", appDelegate.isEventStorePermissionGranted);
    

    
    ////////////////////////////////////////////////////////////////////
    /////////////////////// Steve End //////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    
    BOOL isTouchIDAvail = [self isTouchIDAvailable];
    
    
    //Steve - loads New Features
    if ( [sharedDefaults boolForKey:@"NewFeature_3.2.0"] && isTouchIDAvail){
        
        NewFeaturesViewController *newFeaturesVC=
        [[NewFeaturesViewController alloc]initWithNibName:@"NewFeaturesViewController" bundle:[NSBundle mainBundle]];
        
        [self.navigationController presentViewController:newFeaturesVC animated:YES completion:nil];
    }
    

}

#pragma mark -- TO DO BarButton Action IPad
-(void)toDoBarButtonAction:(id)sender
{
    
   
    
}


//Sees if TouchID can be used on device
- (BOOL) isTouchIDAvailable {
    
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        
        NSLog(@"%@", [authError localizedDescription]);
        return NO;
    }
    return YES;
}



-(void)viewWillAppear:(BOOL)animated {
    
    
	[super viewWillAppear:YES];
    //NSLog(@"***** viewWillAppear CalendarParentController");
    
    [self.view setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];

    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    isViewAppearing = YES; //

    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        //NSLog(@"New Navigation");
        
        aNavBar.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
#pragma mark -- IPad Primotech
         //pooja-iPad
        if(IS_IPAD)
        {
            aBottomToolBar.hidden = YES;
            UIBarButtonItem * add = [[UIBarButtonItem alloc]initWithTitle:@"add" style:UIBarButtonItemStylePlain target:self action:nil];
            
            UIImage * imgSpeaker;
            UIButton * btnSpeaker;
            imgSpeaker = [UIImage imageNamed:@"speaker_icon.png"];
            btnSpeaker = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnSpeaker setImage:imgSpeaker forState:UIControlStateNormal];
            btnSpeaker.showsTouchWhenHighlighted = YES;
            btnSpeaker.frame = CGRectMake(0.0, 0.0, imgSpeaker.size.width, imgSpeaker.size.height);
            [btnSpeaker addTarget:self action:@selector(textToSpeak:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem * textToSpeech = [[UIBarButtonItem alloc]initWithCustomView:btnSpeaker];
            
            UIImage * imgHW;
            UIButton * btnHW;
            imgHW = [UIImage imageNamed:@"pencil_icon.png"];
            btnHW = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnHW setImage:imgHW forState:UIControlStateNormal];
            btnHW.showsTouchWhenHighlighted = YES;
            btnHW.frame = CGRectMake(0.0, 0.0, imgHW.size.width, imgHW.size.height);
            
            [btnHW addTarget:self action:@selector(inputOptionButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem * hw = [[UIBarButtonItem alloc]initWithCustomView:btnHW];
            
            UIImage * imgToday;
            UIButton * btnToday;
            imgToday = [UIImage imageNamed:@"today_icon.png"];
            btnToday = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnToday setImage:imgToday forState:UIControlStateNormal];
            btnToday.showsTouchWhenHighlighted = YES;
            btnToday.frame = CGRectMake(0.0, 0.0, imgToday.size.width, imgToday.size.height);
            
            [btnToday addTarget:self action:@selector(todayButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem * today = [[UIBarButtonItem alloc]initWithCustomView:btnToday];
            
            UIImage * imgSearch;
            UIButton * btnSearch;
            imgSearch = [UIImage imageNamed:@"search_icon.png"];
            btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnSearch setImage:imgSearch forState:UIControlStateNormal];
            btnSearch.showsTouchWhenHighlighted = YES;
            btnSearch.frame = CGRectMake(0.0, 0.0, imgSearch.size.width, imgSearch.size.height);
            
            [btnSearch addTarget:self action:@selector(searchBarButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem * search = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
            
            UIImage * imgSettings;
            UIButton * btnSettings;
            imgSettings = [UIImage imageNamed:@"setting_icon.png"];
            btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnSettings setImage:imgSettings forState:UIControlStateNormal];
            btnSettings.showsTouchWhenHighlighted = YES;
            btnSettings.frame = CGRectMake(0.0, 0.0, imgSettings.size.width, imgSettings.size.height);
            
            [btnSettings addTarget:self action:@selector(settingsButton_Clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem * settings = [[UIBarButtonItem alloc]initWithCustomView:btnSettings];
            
            NSArray * arrayRight = [NSArray arrayWithObjects:add,hw,settings, nil];
            self.navigationItem.rightBarButtonItems = arrayRight;
            
            //Steve
            NSArray *arrayLeft = [NSArray arrayWithObjects:today,textToSpeech,search, nil];
            self.navigationItem.leftBarButtonItems = arrayLeft;
          
            
            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            //Steve - removes background image by setting it to "nil"
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            
        }

        
        if (IS_IOS_7) {

            
            //clears background image and resets nav bar colors
            //
            if (_backgroundPictureImageView) {
                
                _backgroundPictureImageView.hidden = YES;
                [_backgroundPictureImageView removeFromSuperview];
                
                //removes background image by setting it to "nil"
                [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                 //pooja-iPad
                if(IS_IPAD)
                {
                    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
                    aBottomToolBar.hidden = YES;
                }
                else
                    [aBottomToolBar setBackgroundImage:nil forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
            }
            
            
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            if ( [sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Classic Calendar
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                     //pooja-iPad
                 if(!IS_IPAD)
                 {
                    //Add ToolBar Line
                    UIView *coverToolBarLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 1)];
                    coverToolBarLineView.backgroundColor = [UIColor lightGrayColor];
                    [aBottomToolBar addSubview:coverToolBarLineView];
                 }
                }
                else{ //Dark Theme
                    
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                }
            }
            else{ //Photo Calendar
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }
            
            
        
            //Steve TEST
            self.view.backgroundColor = [UIColor whiteColor];
            

            self.edgesForExtendedLayout = UIRectEdgeNone;
            self.edgesForExtendedLayout = UIRectEdgeAll;
            self.navigationController.navigationBar.translucent = YES;
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            
            aBottomToolBar.translucent = YES;
            [self.view bringSubviewToFront:aBottomToolBar];
            

            
            //Add Button
            addButton.hidden = YES;
            UIBarButtonItem *addButton_iOS7;
            UIBarButtonItem *revealButtonItem;
            
            addButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAppointment:)];
            self.navigationItem.rightBarButtonItem = addButton_iOS7;
            
            
            revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(revealToggleHandle)];
            
             //pooja-iPad
            if(IS_IPAD)
            {
            //self.navigationItem.leftBarButtonItem = nil; //Steve commented
            }
            else
            {
            self.navigationItem.leftBarButtonItem = revealButtonItem;
            }
            
            //Update Toolbar icon to Settings icon instead of Calendar icon
            UIImage *settingsIconImage = [UIImage imageNamed:@"SetttingTabIcon.png"];
            UIBarButtonItem *settingBarButton = [[UIBarButtonItem alloc]initWithImage:settingsIconImage style:UIBarButtonItemStyleBordered target:self action:@selector(settingsButton_Clicked)];
            
            NSMutableArray     *items = [aBottomToolBar.items mutableCopy];
            [items replaceObjectAtIndex:5 withObject:settingBarButton]; //replaces barbutton with Settings Image
            aBottomToolBar.items = items;
            
            

            
            if ( [sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Classic Calendar
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    aBottomToolBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0]];
                    
                    
                    addButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    revealButtonItem.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    
                    inputSelectorBarButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    _searchButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    _calendarHelpImageView.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    _textToSpeakButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                    _todayButton.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
                else{ //Dark Theme
                    
                    aBottomToolBar.tintColor = [UIColor whiteColor];
                    [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                    
                    addButton_iOS7.tintColor = [UIColor whiteColor];
                    revealButtonItem.tintColor = [UIColor whiteColor];
                    
                    inputSelectorBarButton.tintColor = [UIColor whiteColor];
                    _searchButton.tintColor = [UIColor whiteColor];
                    _calendarHelpImageView.tintColor = [UIColor whiteColor];
                    _textToSpeakButton.tintColor = [UIColor whiteColor];
                    _todayButton.tintColor = [UIColor whiteColor];
                }

            }
            else{ //Photo Calendar
                
                 //pooja-iPad
                if(IS_IPAD)
                {
                    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                }
                aBottomToolBar.tintColor = [UIColor whiteColor];
                [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
                
                addButton_iOS7.tintColor = [UIColor whiteColor];
                revealButtonItem.tintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
                
                inputSelectorBarButton.tintColor = [UIColor whiteColor];
                _searchButton.tintColor = [UIColor whiteColor];
                _calendarHelpImageView.tintColor = [UIColor whiteColor];
                _textToSpeakButton.tintColor = [UIColor whiteColor];
                _todayButton.tintColor = [UIColor whiteColor];
                
            }
            
            
            
            
            
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            
            if ( [sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Classic Calendar
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0f green:245.0/255.0f blue:245.0/255.0f alpha:1.0];
                    aBottomToolBar.barTintColor = [UIColor colorWithRed:245.0/255.0f green:245.0/255.0f blue:245.0/255.0f alpha:1.0];
                }
                else{ //Dark Theme
                    
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1];
                    aBottomToolBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1];
                }
                
            }
            else{ //Photo Calendar
   #pragma mark -- IPad Primotech
                 //pooja-iPad
                if(IS_IPAD)
                {
                     _backgroundPictureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
                    //[self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];//Steve commented
                    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];  //Steve - Sets a transparant NavBar
                    [self.navigationController.navigationBar setBarTintColor:[UIColor darkGrayColor]];
                    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
                }
                else
                {
                    if (IS_IPHONE_5) {
                        _backgroundPictureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
                    }
                    else{
                        _backgroundPictureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
                    }
                    
                    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];  //Sets a transparant NavBar
                    [aBottomToolBar setBackgroundImage:[UIImage new] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];//Sets a transparant Toolbar
                }

                
                //*********** For TESTING
                //[sharedDefaults setBool:NO forKey:@"CalendarImageBlurUpdate"];
                
                
                //Gets default name of Calendar Image
                NSString *defaultCalendarImageName = [sharedDefaults stringForKey:@"CalendarImageName"];
                NSString *defaultCalendarImageNameBlurred = [sharedDefaults stringForKey:@"CalendarImageName_Blurred"];

                
                 if ([defaultCalendarImageName isEqualToString:@"CustomCalendarPicture.jpg"] || [defaultCalendarImageNameBlurred isEqualToString:@"CustomCalendarPicture.jpg"]) {  //For Custom Calendar Pictures from Photo Albums
                    
                     
                     if ([sharedDefaults boolForKey:@"CalendarImageBlurUpdate"]) {  //blurred image was saved
                         
                         //************** Search for Save Calendar Picture *********************
                         //get the documents directory:
                         NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
                         NSString *documentsDirectory = [path objectAtIndex:0];
                         
                         //make a directory & file name to write the data to using the documents directory:
                         NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
                         NSString *fileName = [NSString stringWithFormat:@"%@/CalendarImageName_Blurred.jpg",directoryName];
                         
                         UIImage *customCalendarImage = [UIImage imageWithContentsOfFile:fileName]; //Get the image
                         
                         
                         //Set the image to the background of Calendar
                         [_backgroundPictureImageView setImage:customCalendarImage];
                         _backgroundPictureImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill, UIViewContentModeScaleAspectFill
                         _backgroundPictureImageView.alpha = 1;
                         
                         [self.view addSubview:_backgroundPictureImageView];
                     }
                     else{ //blurred image was not saved
                         
                         //************** Search for Save Calendar Picture (Not blurred) *********************
                         //get the documents directory:
                         NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
                         NSString *documentsDirectory = [path objectAtIndex:0];
                         
                         //make a directory & file name to write the data to using the documents directory:
                         NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
                         NSString *fileName = [NSString stringWithFormat:@"%@/CustomCalendarPicture.jpg",directoryName];
                         
                         UIImage *customCalendarImage = [UIImage imageWithContentsOfFile:fileName]; //Get the image
                         
                         
                         //Set the image to the background of Calendar
                         [_backgroundPictureImageView setImage:customCalendarImage];
                         _backgroundPictureImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill, UIViewContentModeScaleAspectFill
                         _backgroundPictureImageView.alpha = 1;
                         
                         [self.view addSubview:_backgroundPictureImageView];
                         
                         
                         ////////////  Blur Image  ///////////////
                         float blurImage = [sharedDefaults floatForKey:@"CalendarImageBlur"];
                         
                         //Apply Blur and mask to image
                         UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
                         [self applyBlurWithRadiusOnThread:blurImage tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];
                         
                         //performs save after delay
                         [self performSelector:@selector(SaveCustomBlurredImageAfterBlurThread) withObject:nil afterDelay:1.0];


                     }
  
                    
                }
                else{ //For Wallpaper Images
                    
                    if ([sharedDefaults boolForKey:@"CalendarImageBlurUpdate"]) {  //blurred image saved
                        
                        //************** Search for Save Calendar Picture *********************
                        //get the documents directory:
                        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
                        NSString *documentsDirectory = [path objectAtIndex:0];
                        
                        //make a directory & file name to write the data to using the documents directory:
                        NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
                        NSString *fileName = [NSString stringWithFormat:@"%@/CalendarImageName_Blurred.jpg",directoryName];
                        
                        UIImage *CalendarImageBlurred = [UIImage imageWithContentsOfFile:fileName]; //Get the image
                        
                        
                        //Set the image to the background of Calendar
                        [_backgroundPictureImageView setImage:CalendarImageBlurred];
                        _backgroundPictureImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill, UIViewContentModeScaleAspectFill
                        _backgroundPictureImageView.alpha = 1;
                        
                        [self.view addSubview:_backgroundPictureImageView];

                    }
                    else{ //blurred image Not saved
   
                        [_backgroundPictureImageView setImage:[UIImage imageNamed:defaultCalendarImageName]];
                        _backgroundPictureImageView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleAspectFit, UIViewContentModeScaleToFill
                        _backgroundPictureImageView.alpha = 1;
                        [self.view addSubview:_backgroundPictureImageView];
                        
                        
                        ////////////  Steve - Blur Image  ///////////////
                        float blurImage = [sharedDefaults floatForKey:@"CalendarImageBlur"];
                        
                        //Apply Blur and mask to image
                        UIColor *tintColor = [UIColor colorWithWhite:0.05 alpha:0.125];
                        [self applyBlurWithRadiusOnThread:blurImage tintColor:tintColor saturationDeltaFactor:1.5 maskImage:nil];
                        
                        //performs save after delay
                        [self performSelector:@selector(SaveWallpaperBlurredImageAfterBlurThread) withObject:nil afterDelay:1.0];

                    }
                    

                }
                
                
            }
            
            
        }
        else{ //iOS 6
            
            //Steve added - Keeps ToolBar items white, otherwise after going to "Settings", toobar items turns gray.
            //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
            [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
            
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            [sharedDefaults setBool:YES forKey:@"CalendarTypeClassic"]; //Sets to classic calendar
            
            
            //Sets Navigation item color
            [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
             UINavigationBar *navBarNew = [[self navigationController]navigationBar];
             UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
             [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            [aBottomToolBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
            
            //Plus Button
            self.navigationItem.rightBarButtonItem = nil;
            addButton.frame = CGRectMake(270, 44/2 - 29/2, 51, 29);
            [self.navigationController.navigationBar addSubview:addButton];
            
            
            
            UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon_OLD.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(revealToggleHandle)];
            
            revealButtonItem.tintColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];

            
            self.navigationItem.leftBarButtonItem = revealButtonItem;
            
        }
        
#pragma mark -- IPad Primotech
         //pooja-iPad
        if(IS_IPAD)
        {
            self.view.frame = CGRectMake(0, 0, 768, 960);
            self.rateAppView.frame = CGRectMake(244, 119, 280, 235);
        }
        else
        {
            if (IS_IPHONE_5) {
                self.view.frame = CGRectMake(0, 0, 320, 416 + 88); //Was (0, 0, 320, 416 + 88);
                aBottomToolBar.frame = CGRectMake(0, 372 + 88, 320, 44);
                self.rateAppView.frame = CGRectMake(20, 54 + 65, 280, 235); //Adjust rateAppView frame lower
            }
            else{
                self.view.frame = CGRectMake(0, 0, 320, 416);
                aBottomToolBar.frame = CGRectMake(0, 372, 320, 44);
                self.rateAppView.frame = CGRectMake(20, 54 + 65, 280, 235); //Adjust rateAppView frame lower
            }
        }
        if (IS_LIST_VERSION || IS_TO_DO_VERSION || IS_PROJECTS_VER) {
            self.rateAppView.frame = CGRectMake(20, 54 + 30, 280, 235); //Adjust rateAppView frame lower
        }
        
        
        //NSLog(@"self.view.frame =  %@", NSStringFromCGRect(self.view.frame));
        //NSLog(@"aNavBar.frame =  %@", NSStringFromCGRect(aNavBar.frame));
        //NSLog(@"aBottomToolBar.frame =  %@", NSStringFromCGRect(aBottomToolBar.frame));
        
        ///// Primo-iPad
#pragma mark -- IPad Primotech
      
        segmentedCalendarControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"List",@"Day",@"Week",@"Month", nil]];
        segmentedCalendarControl.momentary = NO;
        [segmentedCalendarControl addTarget:self action:@selector(changedSelectionForSegmentController:) forControlEvents:UIControlEventValueChanged];
        segmentedCalendarControl.selectedSegmentIndex = preSelectedIndex;
        
         //pooja-iPad
        if(IS_IPAD)
        {
            //segmentedCalendarControl.frame = CGRectMake(234, 44/2 - 40/2, 300, 40);//Steve commented
            int width = 300;
            int height = 36;
            segmentedCalendarControl.frame = CGRectMake(768/2 - width/2 + 7, 44/2 - height/2, width, height);//Steve
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            //UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedCalendarControl];//Steve commented
            //self.navigationItem.leftBarButtonItem = barButtonItem;//Steve commented
            [self.navigationController.navigationBar addSubview:segmentedCalendarControl];
           
        }
        else
        {
            segmentedCalendarControl.frame = CGRectMake(320/2 - 200/2 + 7, 44/2 - 29/2, 200, 29);
            segmentedCalendarControl.segmentedControlStyle = UISegmentedControlStyleBar;
            [self.navigationController.navigationBar addSubview:segmentedCalendarControl];
        }
        
        
       
        if (IS_IOS_7) {
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
                
                  if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                      
                      //segmentedCalendarControl.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                      segmentedCalendarControl.tintColor = [UIColor colorWithRed:50.0/255.0f green:50.0/255.0f blue:50.0/255.0f alpha:1.0];
                      segmentedControl.tintColor = [UIColor colorWithRed:50.0/255.0f green:50.0/255.0f blue:50.0/255.0f alpha:1.0];
                  }
                  else{ //Dark Theme
                      
                      segmentedCalendarControl.tintColor = [UIColor colorWithRed:210.0/255 green:210.0/255.0 blue:210.0/255.0 alpha:1.0];
                      segmentedControl.tintColor = [UIColor colorWithRed:210.0/255 green:210.0/255.0 blue:210.0/255.0 alpha:1.0];
                  }

            }
            else {//Photo Calendar
  #pragma mark -- IPad Primotech
                segmentedCalendarControl.tintColor = [UIColor whiteColor];
            }
            

        }
        else{
            segmentedCalendarControl.tintColor = [UIColor colorWithRed:85.0/255 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
        }
        
    }
    else{ //Old Navigation
        //NSLog(@"Old Navigation");
        
        aNavBar.hidden = NO;
        self.navigationController.navigationBarHidden = YES;
        
        self.rateAppView.frame = CGRectMake(20, 54 + 30, 280, 235); //Adjust rateAppView frame lower
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [aNavBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [aBottomToolBar setBackgroundImage:[UIImage imageNamed:@"ToolBarToDo.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
    }
    
    
    //NSLog(@"self.view.frame = %@", NSStringFromCGRect(self.view.frame));
    //NSLog(@"aBottomToolBar.frame  = %@", NSStringFromCGRect(aBottomToolBar.frame ));
  
    
    // ***************** Steve End ***************************
    
    // Steve - changes rateAppView's top title Label (rateTitleLabel) for each target
    if(IS_LIST_VERSION){
        self.rateTitleLabel.text = @"Thank you for using InFocus CheckList";
        self.rateTitleLabel.adjustsFontSizeToFitWidth = YES;
    }
    else if (IS_TO_DO_VERSION){
        self.rateTitleLabel.text = @"Thank you for using InFocus To Do";
        self.rateTitleLabel.adjustsFontSizeToFitWidth = YES;
    }
    else if (IS_PROJECTS_VER){
        self.rateTitleLabel.text = @"Thank you for using InFocus Projects";
        self.rateTitleLabel.adjustsFontSizeToFitWidth = YES;
    }
    else if (IS_NOTES_VER){
        self.rateTitleLabel.text = @"Thank you for using InFocus Notes";
        self.rateTitleLabel.adjustsFontSizeToFitWidth = YES;
    }
    else //Pro Version
        self.rateTitleLabel.text = @"Thank you for using InFocus Pro";

    

    
    
    OrganizerAppDelegate *appdel = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(!appdel.IsVTTDevice || !appDelegate.isVTTDeviceAuthorized)  //Steve was: if(IS_LITE_VERSION || !appdel.IsVTTDevice)
        [inputSelectorBarButton setImage:[UIImage imageNamed:@"PenIcon.png"]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(statusBarDidChangeFrame)
                                                 name:UIApplicationDidChangeStatusBarFrameNotification
                                               object:nil];
    
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
         currentSelectedBtnTag = preSelectedIndex;
        [self changedSelectionForSegmentController:nil];
    }
    else{
        currentSelectedBtnTag = preSelectedIndex;
        [(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:YES];
        [self changedSelection];
    }
    

    
    ///// steve - New Navigation helpView
    if ([sharedDefaults boolForKey:@"FirstLaunch_Calendar2"]) {
        

        if (IS_IOS_7) {
            if(IS_IPAD)
            {
                _calendarHelpImageView.frame = CGRectMake(0, 0, 768, 1024);
                _calendarHelpImageView.image = [UIImage imageNamed:@"CalendarHelp_iOS7.png"];
                _calendarHelpImageView.layer.cornerRadius = 5;
                _calendarHelpImageView.layer.shadowColor = [UIColor blackColor].CGColor;
                _calendarHelpImageView.layer.shadowRadius = 5.0;
                _calendarHelpImageView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
            }
            else
            {
                if (IS_IPHONE_5){
                    
                    _calendarHelpImageView.image = [UIImage imageNamed:@"CalendarHelp_iOS7_iPhone5.png"];
                    _calendarHelpImageView.layer.cornerRadius = 5;
                    _calendarHelpImageView.layer.shadowColor = [UIColor blackColor].CGColor;
                    _calendarHelpImageView.layer.shadowRadius = 5.0;
                    _calendarHelpImageView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
                    
                }
                else{
                    
                    _calendarHelpImageView.frame = CGRectMake(0, 0, 320, 480);
                    _calendarHelpImageView.image = [UIImage imageNamed:@"CalendarHelp_iOS7.png"];
                    _calendarHelpImageView.layer.cornerRadius = 5;
                    _calendarHelpImageView.layer.shadowColor = [UIColor blackColor].CGColor;
                    _calendarHelpImageView.layer.shadowRadius = 5.0;
                    _calendarHelpImageView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
                }
            }
            
        }
        else{ //iOS 6
            
            if (IS_IPHONE_5) {
                
                 _calendarHelpImageView.frame = CGRectMake(0, -65, 320, 568);
                 _calendarHelpImageView.image = [UIImage imageNamed:@"CalendarHelp_iOS6_iPhone5.png"];
                
            }
            else{
                
                //image name is declared in nib
                _calendarHelpImageView.frame = CGRectMake(0, -65, 320, 480);
            }
        }
    
        
        
        UITapGestureRecognizer *tapHelpViewNewNav = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewNewNav_Tapped)];
        UISwipeGestureRecognizer *swipeDownHelpViewNewNav = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewNewNav_Tapped)];
        UISwipeGestureRecognizer *swipeUpHelpViewNewNav = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewNewNav_Tapped)];
        UISwipeGestureRecognizer *swipeRightHelpViewNewNav = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewNewNav_Tapped)];
        UISwipeGestureRecognizer *swipeLeftHelpViewNewNav = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpViewNewNav_Tapped)];
        
        swipeDownHelpViewNewNav.direction = UISwipeGestureRecognizerDirectionDown;
        swipeUpHelpViewNewNav.direction = UISwipeGestureRecognizerDirectionUp;
        swipeLeftHelpViewNewNav.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRightHelpViewNewNav.direction = UISwipeGestureRecognizerDirectionRight;
        
        [helpViewNewNav addGestureRecognizer:swipeDownHelpViewNewNav];
        [helpViewNewNav addGestureRecognizer:swipeUpHelpViewNewNav];
        [helpViewNewNav addGestureRecognizer:swipeLeftHelpViewNewNav];
        [helpViewNewNav addGestureRecognizer:swipeRightHelpViewNewNav];
        [helpViewNewNav addGestureRecognizer:tapHelpViewNewNav];
        helpViewNewNav.hidden = NO;
        
        
        [self.view addSubview:helpViewNewNav];
        [helpViewNewNav bringSubviewToFront:self.view];
        
        isHelpShowing = YES;
    }
    
    

    
    
    //*****************************************************
    // ********** Steve added to Rate App ******************
    //*****************************************************
    if ([sharedDefaults boolForKey:@"RateApp"]){
        
        int rateNumber = 5; //How many times before rateView shows up
        
        if ([sharedDefaults integerForKey:@"CountUntilRate"] < rateNumber){
            
            int counter = [sharedDefaults integerForKey:@"CountUntilRate"] + 1; //Increments count
            
            [sharedDefaults setInteger:counter forKey:@"CountUntilRate"]; //Set new count
            [sharedDefaults synchronize];
            
              //NSLog(@"CountUntilRate 2 = %d", [sharedDefaults integerForKey:@"CountUntilRate"]);
        }
        else if( [sharedDefaults integerForKey:@"CountUntilRate"] >= rateNumber && !isHelpShowing) {
            
           // self.rateAppView_MainView.frame = CGRectMake(self.rateAppView_MainView.frame.origin.x, self.rateAppView_MainView.frame.origin.y, self.rateAppView_MainView.frame.size.width + 65, self.rateAppView_MainView.frame.size.height);
            
            self.navigationController.navigationBar.userInteractionEnabled = NO;
            self.rateAppView_MainView.userInteractionEnabled = YES;
            self.rateTextField.textAlignment = NSTextAlignmentJustified;
            
            
            //Setup & Show View
            self.rateAppView.hidden = NO;
            self.rateAppView.layer.cornerRadius = 5;
            self.rateAppView.layer.shadowColor = [UIColor blackColor].CGColor;
            self.rateAppView.layer.shadowOpacity = 0.5;
            self.rateAppView.layer.shadowRadius = 5.0;
            self.rateAppView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
            [self.view addSubview:self.rateAppView_MainView];
            
            
            //Custom "Never" Button
            [self.neverRateButton setTitle:@"Never" forState:UIControlStateNormal];
            [self.neverRateButton setTitleColor:[UIColor colorWithRed:220.0/256.0 green:220.0/256.0 blue:220.0/256.0 alpha:1.0] forState:UIControlStateNormal];
            [self.neverRateButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
            self.neverRateButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
            self.neverRateButton.layer.cornerRadius = 10.0;
            self.neverRateButton.layer.borderWidth = 1.0;
            self.neverRateButton.layer.borderColor = [[UIColor colorWithRed:220.0/256.0 green:220.0/256.0 blue:220.0/256.0 alpha:1.0]CGColor];
            self.neverRateButton.backgroundColor = [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
            
            
            //Custom Later" Button
            [self.laterRateButton setTitle:@"Later" forState:UIControlStateNormal];
            [self.laterRateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.laterRateButton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
            self.laterRateButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
            self.laterRateButton.layer.cornerRadius = 10.0;
            self.laterRateButton.layer.borderWidth = 1.0;
            self.laterRateButton.layer.borderColor = [[UIColor whiteColor]CGColor];
            self.laterRateButton.backgroundColor = [UIColor darkGrayColor];
            
            //Custom "Rate" Button
            [self.rateBtton setTitle:@"Rate" forState:UIControlStateNormal];
            [self.rateBtton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.rateBtton setTitleColor:[UIColor colorWithRed:150.0/256.0 green:150.0/256.0 blue:150.0/256.0 alpha:1.0] forState: UIControlStateHighlighted];
            self.rateBtton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
            self.rateBtton.layer.cornerRadius = 10.0;
            self.rateBtton.layer.borderWidth = 1.0;
            self.rateBtton.layer.borderColor = [[UIColor whiteColor]CGColor];
            self.rateBtton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0];
            
        }
    
    }

    
    //executes method every interval to refresh calendars like Google
    _refreshTimer = [NSTimer scheduledTimerWithTimeInterval:100.0
                                                     target:self
                                                   selector:@selector(refreshCalendar)
                                                   userInfo:nil
                     
                                                    repeats:YES];
    
    [self postToSocialNetworkMethod]; //Post to Twitter or Facebook


}



- (void) postToSocialNetworkMethod
{
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    //********** For TESTing only ************
    //[sharedDefaults setBool:YES forKey:@"PostToSocialNetworks"];
    //[sharedDefaults setInteger:6 forKey:@"CountUntilPostToSocialNetworks"];
    //****************************************
    
    
    if ([sharedDefaults boolForKey:@"PostToSocialNetworks"]){
        
        int rateNumber = 8; //How many times before PostToFacebook View shows up
        
        if ([sharedDefaults integerForKey:@"CountUntilPostToSocialNetworks"] < rateNumber){
            
            int counter = [sharedDefaults integerForKey:@"CountUntilPostToSocialNetworks"] + 1.0; //Increments count
            
            [sharedDefaults setInteger:counter forKey:@"CountUntilPostToSocialNetworks"]; //Set new count
            [sharedDefaults synchronize];
            
            //NSLog(@"CountUntilRate 2 = %d", [sharedDefaults integerForKey:@"CountUntilRate"]);
        }
        else if( [sharedDefaults integerForKey:@"CountUntilPostToSocialNetworks"] >= rateNumber) {
            
            self.navigationController.navigationBar.userInteractionEnabled = NO;
            
            PostToFaceBookViewController *postToFaceBookViewController = [[PostToFaceBookViewController alloc]initWithNibName:@"PostToFaceBookViewController" bundle:[NSBundle mainBundle]];
            
            
            [self addChildViewController:postToFaceBookViewController];
            [self.view addSubview:postToFaceBookViewController.view];
            [self.view bringSubviewToFront:postToFaceBookViewController.view];
            
        }
        
    }
    
}


-(void)SaveWallpaperBlurredImageAfterBlurThread{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
       
        
        //*************** Save  Photo to Documents as BLURRED ***********************
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSData      *imageDataBlurred = UIImageJPEGRepresentation(_backgroundPictureImageView.image, 1.0); //Convets image to NSData
        NSError     *error;
        
        
        //get the documents directory:
        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        
        //make a directory & file name to write the data to using the documents directory:
        NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
        NSString *fileNameForBlurredImage = [NSString stringWithFormat:@"%@/CalendarImageName_Blurred.jpg",directoryName];
        
        //Create Directory Folder & then write content to file
        [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
        
        [imageDataBlurred writeToFile:fileNameForBlurredImage atomically:YES];
        
        [sharedDefaults setBool:YES forKey:@"CalendarImageBlurUpdate"]; //now we know if blurred image was saved
        
        NSString *customPhotoSelectedBlurred = @"CalendarImageName_Blurred.jpg"; //save as custom name so can identify later
        [sharedDefaults setValue:customPhotoSelectedBlurred forKey:@"CalendarImageName_Blurred"];
        
        //*************** End Save  Photo to Documents  ***********************

        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Once blur thread is done, change background image
            [_backgroundPictureImageView setImage:_backgroundPictureImageView.image];
        });
 
        
    });
    
}


-(void)SaveCustomBlurredImageAfterBlurThread{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        
        
        //*************** Save  Photo to Documents as BLURRED ***********************
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSData      *imageDataBlurred = UIImageJPEGRepresentation(_backgroundPictureImageView.image, 1.0); //Convets image to NSData
        NSError     *error;
        
        
        //get the documents directory:
        NSArray *path = NSSearchPathForDirectoriesInDomains (NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        
        //make a directory & file name to write the data to using the documents directory:
        NSString *directoryName = [NSString stringWithFormat:@"%@/CalendarPictureFolder",documentsDirectory];
        NSString *fileNameForBlurredImage = [NSString stringWithFormat:@"%@/CalendarImageName_Blurred.jpg",directoryName];
        
        //Create Directory Folder & then write content to file
        [fileManager createDirectoryAtPath:directoryName withIntermediateDirectories:YES attributes:nil error:&error];
        
        [imageDataBlurred writeToFile:fileNameForBlurredImage atomically:YES];
        
        [sharedDefaults setBool:YES forKey:@"CalendarImageBlurUpdate"]; //now we know if blurred image was saved
        
        NSString *customPhotoSelectedBlurred = @"CalendarImageName_Blurred.jpg"; //save as custom name so can identify later
        [sharedDefaults setValue:customPhotoSelectedBlurred forKey:@"CalendarImageName_Blurred"];
        
        //*************** End Save  Photo to Documents  ***********************
        

        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Once blur thread is done, change background image
            [_backgroundPictureImageView setImage:_backgroundPictureImageView.image];
        });
        
        
    });
    
}



-(void) revealToggleHandle{
   // NSLog(@"revealToggleHandle");
    
    if (isMenuClosed){
        self.view.userInteractionEnabled = NO;
        isMenuClosed = NO;
    }
    else{
        self.view.userInteractionEnabled = YES;
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    //[self.invisibleView addGestureRecognizer:revealController.panGestureRecognizer];
    [revealController revealToggle:self];
}


-(void)refreshCalendar{
    //NSLog(@"refreshCalendar");
    
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    eventStore = appDelegate.eventStore;
    
    if (eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
    
    [eventStore refreshSourcesIfNecessary];  //Steve - helps slow syncing with Google

}


-(void)UploadSqlEventToEventStor{
   // NSLog(@"UploadSqlEventToEventStor");
   
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    progressView.hidden = NO;
    [progressView bringSubviewToFront:self.view];
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.5;
    [progressView.layer addAnimation:transition forKey:kCATransitionFromBottom];
    self.view.userInteractionEnabled=NO;
    [self.view addSubview:progressView];
    
    // ********** Steve commented for testing ************
    //*********** Add Back *******************************
    [sharedDefaults setBool:NO forKey:@"FirstLaunch_EventStore"];
    [sharedDefaults synchronize];
    
    
    
    dispatch_queue_t fetchingOldEvents_queue = dispatch_queue_create("Fetching Old Events", NULL);
    dispatch_async(fetchingOldEvents_queue, ^{
        
    //NSLog(@"dispatch_queue_create - Fetching Events");
    
    sqlite3_stmt *selectAppointment_Stmt = nil;
    	if(selectAppointment_Stmt == nil && appDelegate.database)
	{
       // NSLog(@"if(selectAppointment_Stmt == nil && appDelegate.database)");
        
		//NSString *Sql = @"Select a.AppointmentID, a.StartDateTime, a.DueDateTime,  a.ShortNotes, a.AppointmentTitle, a.AppointmentType, b.CalendarName, a.textLocName, a.isAllDay, a.Repeat, a.Alert, a.AlertSecond, a.relation_Field1 From AppointmentsTable  a Left Outer Join CalendarTable b on a.CalendarID = b.CalendarID And a.relation_Field1=  \"\"   order by a.DueDateTime";
        
       // NSString *Sql = @"Select a.AppointmentID, a.StartDateTime, a.DueDateTime,  a.ShortNotes, a.AppointmentTitle, a.AppointmentType,  a.textLocName, a.isAllDay, a.Repeat, a.Alert, a.AlertSecond, a.relation_Field1 From AppointmentsTable  a  where  a.relation_Field1= \"\"   order by a.DueDateTime";
        
        //Steve changed
        NSString *Sql = @"Select a.AppointmentID, a.StartDateTime, a.DueDateTime,  a.ShortNotes, a.AppointmentTitle, a.AppointmentType,  a.textLocName, a.isAllDay, a.Repeat, a.Alert, a.AlertSecond, a.relation_Field1 From AppointmentsTable  a  where  a.relation_Field1 ISNULL order by a.StartDateTime Asc";

        
         // NSLog(@"SQL --> %@",Sql);
        
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
		{
            [self PrgressViewRemove];
            return; //Steve added
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
           // NSLog(@"SQL OK");
            
            [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
           
            EKCalendar * calendar = [appDelegate.eventStore defaultCalendarForNewEvents];
			while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
			{
               // NSLog(@"SQL while");
                
                EKEvent * event = [EKEvent eventWithEventStore:appDelegate.eventStore];
 				int AppointmentID =  sqlite3_column_int(selectAppointment_Stmt, 0);
				
				
				const char* string3 =(const char*)sqlite3_column_text(selectAppointment_Stmt, 1);
				NSString* str3= string3 == NULL ? nil : [NSString stringWithUTF8String:string3];
                [event setStartDate:[dateFormatter dateFromString:str3]];
				
				const char* string4 =(const char*)sqlite3_column_text(selectAppointment_Stmt, 2);
                NSString* str4 = string4 == NULL ? nil : [NSString stringWithUTF8String:string4];
				[event setEndDate:[dateFormatter dateFromString:str4]];
                
                [event setNotes:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 3)]];
				
				[event setTitle:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 4)]];
                
                NSString * AppointmentType = [NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 5)];
                if([AppointmentType isEqualToString:@"H"])
                [event setTitle:[NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType]];
                
               /* const char* string13 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 7);
				NSString* str13 = string13 == NULL ? nil : [NSString stringWithUTF8String:string13];
				[appDataObject setCalendarName:str13];*/
                [event setCalendar:calendar];
               
                
                const char* string13 = (const char*)sqlite3_column_text(selectAppointment_Stmt, 6);
				NSString* str13 = string13 == NULL ? nil : [NSString stringWithUTF8String:string13];
				[event setLocation:str13];
                
				[event setAllDay:sqlite3_column_int(selectAppointment_Stmt, 7)];
                
                NSString *Repeat =[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 8)];
               
                //Alok Gupta Added for Repet evnets ////////////////////////////////
                if(![Repeat isEqualToString:@"Never"] && ![Repeat isEqualToString:@"None"] && ![Repeat isEqual:@""])
                {
                    EKRecurrenceFrequency freq;
                    int recurrenceInterval = 1;
                    if ([Repeat isEqualToString:@"Every Day"]) {
                        freq = EKRecurrenceFrequencyDaily;
                    }
                    else if([Repeat isEqualToString:@"Every Week"])
                    {
                        freq = EKRecurrenceFrequencyWeekly;
                    }
                    else if([Repeat isEqualToString:@"Every 2 Week"])
                    {
                        freq = EKRecurrenceFrequencyWeekly;
                        recurrenceInterval =2;
                    }
                    else if([Repeat isEqualToString:@"Every Month"])
                    {
                        freq = EKRecurrenceFrequencyMonthly;
                    }
                    else if([Repeat isEqualToString:@"Every Year"])
                    {
                        freq = EKRecurrenceFrequencyYearly;
                    }
                     
                    EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:freq interval:recurrenceInterval end:nil];
                    [event setRecurrenceRules:[NSArray arrayWithObjects:rule, nil]];
                }
                NSMutableArray * AlarmsArray = [[NSMutableArray alloc]
                                                init];
                //[AlarmsArray removeAllObjects];
				NSString *AlertStr=[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 9)];
                if(![AlertStr isEqualToString:@"None"] && ![AlertStr isEqualToString:@"Never"] && ![AlertStr isEqual:@""])
                [AlarmsArray addObject:[self setEventAlarm:AlertStr]];

				NSString *AlertSecondStr =[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectAppointment_Stmt, 10)];
                if(![AlertSecondStr isEqualToString:@"None"] && ![AlertSecondStr isEqualToString:@"Never"] && ![AlertSecondStr isEqual:@""] )
                [AlarmsArray addObject:[self setEventAlarm:AlertSecondStr]];
                
               
              //  NSLog(@"event.title = %@", event.title);
               // NSLog(@"event.startDate = %@", event.startDate);
                
               
                // ************ Steve commented. Needs to be fixed - Crashes App ***********************
               //*****ALok Kumar Gupta Uncommented.Setting alert issue have been fixed.*******
               if([AlarmsArray count]>0)
                  [event setAlarms:AlarmsArray];
                
                
                NSError *error = nil;
                BOOL result = [appDelegate.eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                
                if(!result)
                {
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error with adding event on ical calender" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
                    [alert show];
                    return;
                }
            else{
                
                // ************ Steve commented for testing.  Need to add back later ***********************
                [self EventsSyncEventStore:AppointmentType eventIdentifire:event.eventIdentifier EventID:AppointmentID];
             }
				
			}//while end
 		}//else end
		sqlite3_finalize(selectAppointment_Stmt);
		selectAppointment_Stmt = nil;
	} else {
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
	}
        [self removePrgressloading];
      
    });
    //dispatch_release(fetchingOldEvents_queue);
    

}
-(void)removePrgressloading{
       [self performSelectorOnMainThread:@selector(PrgressViewRemove) withObject:nil waitUntilDone:YES];
}
-(void)PrgressViewRemove{
    progressView.hidden = YES;
    [progressView removeFromSuperview];
    self.view.userInteractionEnabled =YES;

}
-(void)EventsSyncEventStore:(NSString *)AppointmentType eventIdentifire:(NSString *) eventIdent EventID:(int) eventID{
    
    sqlite3_stmt *deleteStmt = nil;
	
	if(deleteStmt == nil  && appDelegate.database) {
    const char *sql;
    if ([AppointmentType isEqual:@"H"])
       sql = "Update AppointmentsTable Set Relation_Field1=?  Where AppointmentID = ?";
    else
        sql = "delete from AppointmentsTable where AppointmentID = ?";
    
    if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
        NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
        
        if ([AppointmentType isEqual:@"H"]){
            sqlite3_bind_text(deleteStmt, 1, [eventIdent UTF8String], -1,SQLITE_TRANSIENT);
       sqlite3_bind_int(deleteStmt, 2, eventID);
            
        }
        else
        sqlite3_bind_int(deleteStmt, 1, eventID);     
	}
    else {
		NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
	}
	
	if (SQLITE_DONE != sqlite3_step(deleteStmt))
    {
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_reset(deleteStmt);
		
	}
    else
    {
		sqlite3_reset(deleteStmt);
	}
        
}



-(EKAlarm *)setEventAlarm:(NSString *) StrAlert{
     EKAlarm *alarm1;
    NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init]; 
    NSTimeInterval AlertTimeInterval = 0;
    NSDate *AbsAlertDate = [[NSDate alloc]init];
    if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
    {
        if ([StrAlert isEqualToString:@"5 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 5;
        }
        else if ([StrAlert isEqualToString:@"15 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 15;
        }
        else if ([StrAlert isEqualToString:@"30 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 30;
        }
        else if ([StrAlert isEqualToString:@"1 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 2;
        }
        else if ([StrAlert  isEqualToString:@"1 Day Before"])
        {
            AlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Day Before"])
        {
            AlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"On Date of Event"])
        {
        }
        else
        {
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
           AbsAlertDate =[dateFormatter dateFromString:StrAlert];
         }
        
       
        if(AlertTimeInterval >0)
        {
            alarm1 = [EKAlarm alarmWithRelativeOffset:-AlertTimeInterval];
            [myAlarmsArray addObject:alarm1];
        }
        else if (AbsAlertDate)
        {
            alarm1 = [EKAlarm alarmWithAbsoluteDate:AbsAlertDate];
            [myAlarmsArray addObject:alarm1];
        }
    }
    return alarm1;
}


-(void)calendarPrivacySettingsInstructions{
    //Instructions on turning on Calendar in Privacy settings
   // NSLog(@"calendarPrivacySettingsInstructions");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    

   // UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    //[aNavBarAccessHelpMenu setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        if(IS_IPAD)
        {
            calendarAccessView.frame = CGRectMake(0, 0, 768, 1024);
            calendarAccessScrollView.frame = CGRectMake(0, 44, 768, 958);
            [calendarAccessScrollView setContentSize:CGSizeMake(768, 1865 - 66)];
        }
        else
        {
            
            if (IS_IPHONE_5) {
                calendarAccessView.frame = CGRectMake(0, 0, 320, 568);
                calendarAccessScrollView.frame = CGRectMake(0, 44, 320, 414 + 88);
                [calendarAccessScrollView setContentSize:CGSizeMake(320, 1865 - 66)];
            }
            else{
                calendarAccessView.frame = CGRectMake(0, 0, 320, 480);
                calendarAccessScrollView.frame = CGRectMake(0, 44, 320, 414);
                [calendarAccessScrollView setContentSize:CGSizeMake(320, 1865 - 66)];
            }
        }
        
    }
    else{
        calendarAccessView.frame = CGRectMake(0, 0, 320, 480);
        calendarAccessScrollView.frame = CGRectMake(0, 44, 320, 480);
        [calendarAccessScrollView setContentSize:CGSizeMake(320, 1865)];
    }
    
    
    calendarAccessScrollView.userInteractionEnabled = YES;
    calendarAccessScrollView.bounces = NO;
    
    calendarAccessView.hidden = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.5f;
    [calendarAccessView.layer addAnimation:transition forKey:kCATransitionFromBottom];
    [self.view addSubview:calendarAccessView];
}

- (IBAction)doneButton_Clicked:(id)sender{
    //NSLog(@"doneButton_Clicked");
    
    calendarAccessView.hidden = YES;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [calendarAccessView.layer addAnimation:transition forKey:kCATransitionFromTop];
}


-(void)statusBarDidChangeFrame
{
    
    
    /*
    OrganizerAppDelegate  *appDel = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    if (appDel.isCallActive) {
        
        appDel.isCallActive = NO;
    }
    else{
        
        appDel.isCallActive = YES;
    }
     
    
    
    //CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    
    if (IS_IOS_7) {
        
        if (aCalListViewController) { //List
            
            aCalDailyViewController.view.frame = CGRectMake(0, 65, 320,aBottomToolBar.frame.origin.y-100);
        }
        
        if (aCalDailyViewController) { //Day
            
            if (IS_IPHONE_5)
                [aCalDailyViewController.view setFrame:CGRectMake(0, 65, 320, 424 + 36)];
            else
                [aCalDailyViewController.view setFrame:CGRectMake(0, 65, 320, 342-6 + 36)];
        }
        
        if (aCalWeekViewController) { //Week
            aCalWeekViewController.view.frame = CGRectMake(0, 65, 320,aBottomToolBar.frame.origin.y-100);
        }
        
        if (aCalMonthViewController) //Month
        {
            aCalMonthViewController.view.frame = CGRectMake(0, 65, 320,aBottomToolBar.frame.origin.y-100);
            
        }

    }
    else{ //iOS 6
     
        if (aCalListViewController) { //List
            
            aCalDailyViewController.view.frame = CGRectMake(0, 80, 320,aBottomToolBar.frame.origin.y-100);
        }
        
        if (aCalDailyViewController) { //Day
            aCalDailyViewController.view.frame = CGRectMake(0, 80, 320,aBottomToolBar.frame.origin.y-100);
        }
        
        if (aCalWeekViewController) { //Week
            aCalWeekViewController.view.frame = CGRectMake(0, 80, 320,aBottomToolBar.frame.origin.y-100);
        }
        
        if (aCalMonthViewController) //Month
        {
            aCalMonthViewController.view.frame = CGRectMake(0, 80, 320,aBottomToolBar.frame.origin.y-100);
            
        }

    }
    
     */

}


-(void)viewDidDisappear:(BOOL)animated{
   // NSLog(@"***** viewDidDisappear CalendarParentController");
    
    [searchBarViewController.view removeFromSuperview];
    searchBarViewController =nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.Elixir.EventStorePermisssionGranted" object:nil];
    

}



#pragma mark -
#pragma mark Private Methods
#pragma mark -

-(void)addAdditionalControlsInCalenderParent {
    //NSLog(@"addAdditionalControlsInCalenderParent CalendarParentController");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if (![sharedDefaults boolForKey:@"NavigationNew"]) //Old Nav.
        [self createSegments];
}

-(void)createSegments {
	//	UIImageView *bgViewForTable;
 	
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
	UIImage *listSegImage;
	UIImage *daySegImage;
	UIImage *weekSegImage;
	UIImage *monthSegImage;
	
	UIImage *listSegImage_hover;
	UIImage *daySegImage_hover;
	UIImage *weekSegImage_hover;
	UIImage *monthSegImage_hover;
	
	UIButton *listButton  = [[UIButton alloc] init];
	UIButton *dayButton   = [[UIButton alloc] init];
	UIButton *weekButton  = [[UIButton alloc] init];
	UIButton *monthButton = [[UIButton alloc] init];
	
	UIImage *tableBgImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"BackgroundButtons" ofType:@"png"]];
    UIImageView	*bgViewForButtons = [[UIImageView alloc] initWithImage:tableBgImage];
    
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        [bgViewForButtons setFrame:CGRectMake(0, 0, tableBgImage.size.width, tableBgImage.size.height - 2)];
    }else{
        [bgViewForButtons setFrame:CGRectMake(0, 44, tableBgImage.size.width, tableBgImage.size.height - 2)];
    }

 
	[self.view addSubview:bgViewForButtons];
	
	listSegImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"list" ofType:@"png"]];
	daySegImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"day" ofType:@"png"]];
	weekSegImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"week" ofType:@"png"]];
	monthSegImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"month" ofType:@"png"]];
	
	listSegImage_hover = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"list_hover" ofType:@"png"]];
	daySegImage_hover = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"day_hover" ofType:@"png"]];
	weekSegImage_hover = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"week_hover" ofType:@"png"]];
	monthSegImage_hover = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"month_hover" ofType:@"png"]];
	
     if ([sharedDefaults boolForKey:@"NavigationNew"]) {
           [listButton setFrame:CGRectMake(8, 5, listSegImage.size.width, listSegImage.size.height)];
     }else
         [listButton setFrame:CGRectMake(8, 49, listSegImage.size.width, listSegImage.size.height)];
	
	[dayButton setFrame:CGRectMake(listButton.frame.origin.x + listButton.frame.size.width , listButton.frame.origin.y, daySegImage.size.width, daySegImage.size.height)];
	
	[weekButton setFrame:CGRectMake(dayButton.frame.origin.x + dayButton.frame.size.width , dayButton.frame.origin.y, weekSegImage.size.width, weekSegImage.size.height)];
	
	[monthButton setFrame:CGRectMake(weekButton.frame.origin.x + weekButton.frame.size.width, weekButton.frame.origin.y, monthSegImage.size.width - 2, monthSegImage.size.height)];
	
	[listButton setImage:listSegImage forState:UIControlStateNormal];
	[listButton setImage:listSegImage_hover forState:UIControlStateSelected];
	[listButton setTag:LIST_BUTTON_TAG];
	[listButton addTarget:self action:@selector(listButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:listButton];
	
	[dayButton setImage:daySegImage forState:UIControlStateNormal];
	[dayButton setImage:daySegImage_hover forState:UIControlStateSelected];
	[dayButton addTarget:self action:@selector(dayButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[dayButton setTag:DAY_BUTTON_TAG];
	[self.view addSubview:dayButton];
	
	[weekButton setImage:weekSegImage forState:UIControlStateNormal];
	[weekButton setImage:weekSegImage_hover forState:UIControlStateSelected];
	[weekButton addTarget:self action:@selector(weekButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[weekButton setTag:WEEK_BUTTON_TAG];
	[self.view addSubview:weekButton];
	
	[monthButton setImage:monthSegImage forState:UIControlStateNormal];
	[monthButton setImage:monthSegImage_hover forState:UIControlStateSelected];
	[monthButton addTarget:self action:@selector(monthButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
	[monthButton setTag:MONTH_BUTTON_TAG];
	[self.view addSubview:monthButton];
	
	
}

//Steve - for NavigationNew segmented bar (List, Day, Week, Month)
-(void) changedSelectionForSegmentController: (id) sender{
    //NSLog(@"changedSelectionForSegmentController");
    
    
    
    ///////// Steve added - removes new Help view if user switches Calendar Views.  Otherwise view will be underneath Calendar
    
    int    selectedIndex;
    
    switch (segmentedCalendarControl.selectedSegmentIndex) {
        case 0: //LIST_BUTTON_TAG
            selectedIndex = 70001;
            break;
            
        case 1: //DAY_BUTTON_TAG
            selectedIndex = 70002;
            break;
            
        case 2: //WEEK_BUTTON_TAG
            selectedIndex = 70003;
            break;
            
        case 3: //MONTH_BUTTON_TAG
            selectedIndex = 70004;
            break;
            
        default:
            break;
    }

    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults integerForKey:@"DefaultCalendar"] != selectedIndex){
        [self helpViewNewNav_Tapped];
    }

    //////////// Steve End ////////////////////////////////////////


    
    //Steve - fixes new Nav where CalendarAccessView covers toolBar when changing SegmentController
    if (calendarAccessView) {
        calendarAccessView.hidden = YES;
    }

    //Steve added for NewNavigatoin
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
   // NSLog(@"selectedSegment = %d",selectedSegment);
    
    currentSelectedBtnTag = selectedSegment;
    
    // Added so can find correct view first time view appears
    if (isViewAppearing) {
         selectedSegment = preSelectedIndex;
        isViewAppearing = NO;
    }

    
    OrganizerAppDelegate  *appDel = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    [searchBarViewController.view removeFromSuperview];
    searchBarViewController =nil;
    
	[APPINSTANCE setIsLoded:NO] ;
    
	
    if (aCalListViewController) {
        
        if (aCalListViewController.sections.count > 0) {
            
            [aCalListViewController.view removeFromSuperview];
        }
        
        aCalListViewController = nil;
    }
	
	if (aCalDailyViewController) {
        [aCalDailyViewController.view removeFromSuperview];
        aCalDailyViewController = nil;
	}
	
    if (aCalWeekViewController) {
        [aCalWeekViewController.view removeFromSuperview];
        aCalWeekViewController = nil;
    }
	
    if (aCalMonthViewController) {
        [aCalMonthViewController.view removeFromSuperview];
        aCalMonthViewController = nil;
    }
    
    
    [self.view bringSubviewToFront:aBottomToolBar];
    
    
	switch (selectedSegment)
	{
		case 0: //List View
		{
			preSelectedIndex = 0;
            
            UIStoryboard *sb ;
            if(IS_IPAD)
            {
                sb = [UIStoryboard storyboardWithName:@"CalendarList2ViewControllerIPad" bundle:nil];
            }
            else
            {
                sb = [UIStoryboard storyboardWithName:@"CalendarList2ViewController" bundle:nil];
                
            }

            aCalListViewController = [sb instantiateViewControllerWithIdentifier:@"CalendarList2ViewController"];
		
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            
            
            if (IS_IOS_7) {
                
                if(!appDel.isCallActive)
                {
                    if(IS_IPAD)
                    {
                        [aCalListViewController.view setFrame:CGRectMake(0, 64, 768, 916)];
                    }
                    else
                    {
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 64, 320, 424 + 36)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 64, 320, 342-6 + 36)];
                    }
                }
                else
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 64, 320, 402 + 36)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 64, 320, 342-28)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                    }
                    
                }
            }
            else{ //iOS 6
                
                if(!appDel.isCallActive)
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 0, 320, 342-6 + 36)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 424)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                    }
                    
                }
                else
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 0, 320, 402 + 36)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 0, 320, 342-28)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                        else
                            [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                    }
                    
                }
            }
            

            
            aCalListViewController.view.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
            self.calling_VC = aCalListViewController;//Aman's Added
			[self.view addSubview:aCalListViewController.view];
            
            // *** Steve - Important - Adds to navigationController, otherwise it won't work
            [self addChildViewController:aCalListViewController];
            [self.navigationController didMoveToParentViewController:aCalListViewController];
            
            
			break;
		}
		case 1: //Day View
		{
			preSelectedIndex = 1;
            
			if(IS_IPAD)
            {
                aCalDailyViewController = [[CalendarDailyViewController alloc] initWithNibName:@"CalendarDailyViewController_IPad" bundle:nil];
            }
            else
            {
                if (IS_IPHONE_5)
                    aCalDailyViewController = [[CalendarDailyViewController alloc] initWithNibName:@"CalendarDailyViewController_iPhone5" bundle:[NSBundle mainBundle]];
                else
                    aCalDailyViewController = [[CalendarDailyViewController alloc] initWithNibName:@"CalendarDailyViewController" bundle:[NSBundle mainBundle]];
            }
            
            if (IS_IOS_7) {
                
                if(!appDel.isCallActive)
                {
                    if(IS_IPAD)
                    {
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 64, 768, 916)];
                    }
                    else
                    {
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 64, 320, 424 + 36)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 64, 320, 342-6 + 36)];
                    }
                }
                else //isCallActive
                {
                    if(IS_IPAD)
                    {
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 64, 768, 856)];
                    }
                    else
                    {
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 65, 320, 400)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 65, 320, 342-28 + 36)];
                    }
                }
            }
            else{ //iOS 6
                
                if(!appDel.isCallActive)
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if(IS_IPAD)
                        {
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)];
                        }
                        {
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 0, 320, 342-6 + 36)];
                        }
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 424)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                    }
                }
                else
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 0, 320, 402 + 36)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 0, 320, 342-28 + 36)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                        else
                            [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                    }
                    
                }
            }
            
            

            
            aCalDailyViewController.view.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
            
            self.calling_VC = aCalDailyViewController;//Aman's Added
            
            
            [self.view addSubview:aCalDailyViewController.view];
            
            // *** Steve - Important - Adds to navigationController, otherwise it won't work
            [self addChildViewController:aCalDailyViewController];
            [self.navigationController didMoveToParentViewController:aCalDailyViewController];
            
            
            break;
			
		}
		case 2: //Week View
		{
			preSelectedIndex = 2;
           if(IS_IPAD)
           {
               aCalWeekViewController = [[CalendarWeekViewController alloc] initWithNibName:@"CalendarWeekViewController_IPad" bundle:nil];
           }
           else{
            if (IS_IPHONE_5) {
                aCalWeekViewController = [[CalendarWeekViewController alloc] initWithNibName:@"CalendarWeekViewController_iPhone5" bundle:[NSBundle mainBundle]];
            }
            else
                aCalWeekViewController = [[CalendarWeekViewController alloc] initWithNibName:@"CalendarWeekViewController" bundle:[NSBundle mainBundle]];
           }
            
            
            if (IS_IOS_7)
            {
                
                
                if(!appDel.isCallActive)
                {
                    if(IS_IPAD)
                    {
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 64, 768, 916)];
                    }
                    else
                    {
                    if (IS_IPHONE_5)
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 64, 320, 424 + 36)]; //424
                    else
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 64, 320, 342-6 + 36)];
                    }
                }
                else //isCallActive
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 65, 320, 402 + 36)];
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 65, 320, 342-28 + 36)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80 , 320, 342-28)];
                    }
                    
                    
                }
            }
            else //iOS 6
            {
                if(!appDel.isCallActive)
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)]; //424
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 0, 320, 342-6 + 36)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 424)]; //424
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                    }
                    
                }
                else
                {
                    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 0, 320, 402 + 36)];
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 0, 320, 342-28 + 36)];
                    }
                    else{
                        if (IS_IPHONE_5)
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                        else
                            [aCalWeekViewController.view setFrame:CGRectMake(0, 80 , 320, 342-28)];
                    }
                    
                    
                }
            }
            
            

            
            
            self.calling_VC = aCalWeekViewController;//Aman's Added
            
			[self.view addSubview:aCalWeekViewController.view];
            
            // *** Steve - Important - Adds to navigationController, otherwise it won't work
            [self addChildViewController:aCalWeekViewController];
            [self.navigationController didMoveToParentViewController:aCalWeekViewController];
            
            
			break;
		}
		case 3:  //Month View
		{
			preSelectedIndex = 3;

            if(IS_IPAD)
            {
                aCalMonthViewController = [[CalendarMonthViewController alloc] initWithNibName:@"CalendarMonthViewController_iPad" bundle:[NSBundle mainBundle]];
            }
            else
            {
                aCalMonthViewController = [[CalendarMonthViewController alloc] initWithNibName:@"CalendarMonthViewController" bundle:[NSBundle mainBundle]];
            }
			
            aCalMonthViewController.toolBarDelegate = self; //Steve - for Protocol in Calendar Monthview
            
            if(!appDel.isCallActive)
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    
                    if (IS_IOS_7) {
                        if(IS_IPAD)
                        {
                            [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 768, 916)];
                        }
                        else
                        {
                            if (IS_IPHONE_5){
                                
                                [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)];
                                //[aCalMonthViewController.view setBounds:CGRectMake(0, 0, 320, 424 + 36)];
                            }
                            else{
                                
                                [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 342-6 + 36)];
                                [aCalMonthViewController.view setBounds:CGRectMake(0, 0, 320, 342-6 + 36)];
                            }
                        }
                    }
                    else{ //iOS 6
                        
                        if (IS_IPHONE_5)
                            [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 424 + 36)];
                        else
                            [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 342-6 + 36)];
                    }
                    
                    
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 424)];
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                }
                
            }
            else
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 402 + 36)];
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 0, 320, 342-28 + 36)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                }
                
            }
            
            self.calling_VC = aCalMonthViewController;//Aman's Added
            
            [self.view addSubview:aCalMonthViewController.view];
            
            // *** Steve - Important - Adds to navigationController, otherwise it won't work
            [self addChildViewController:aCalMonthViewController];
            [self.navigationController didMoveToParentViewController:aCalMonthViewController];
            
            
			break;
		}
		default:
			break;
	}

     
    
}




//For Old View with custom segmented bar (List, Day, Week, Month)
-(void)changedSelection {
   // NSLog(@"changedSelection");
	
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults]; //Steve added
 
    OrganizerAppDelegate  *appDel = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    [searchBarViewController.view removeFromSuperview];
     searchBarViewController =nil;
    
	[APPINSTANCE setIsLoded:NO] ;
	
    if (aCalListViewController) {
        [aCalListViewController.view removeFromSuperview];
        aCalListViewController = nil;
    }
	
	if (aCalDailyViewController) {
        [aCalDailyViewController.view removeFromSuperview];
        aCalDailyViewController = nil;
	}
	
    if (aCalWeekViewController) {
        [aCalWeekViewController.view removeFromSuperview];
        aCalWeekViewController = nil;
    }
	
    if (aCalMonthViewController) {
        [aCalMonthViewController.view removeFromSuperview];
        aCalMonthViewController = nil;
    }
    
    
	switch (currentSelectedBtnTag)
	{
		case LIST_BUTTON_TAG:
		{
			preSelectedIndex = LIST_BUTTON_TAG;
          
            /////// Steve added - Saves preSelectedIndex to wherever the user left off last
            //[sharedDefaults setInteger:preSelectedIndex forKey:@"DefaultCalendar"];
            //[sharedDefaults synchronize];
            UIStoryboard *sb ;
            sb = [UIStoryboard storyboardWithName:@"CalendarList2ViewController" bundle:nil];
            aCalListViewController = [sb instantiateViewControllerWithIdentifier:@"CalendarList2ViewController"];
            NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
            
            if(!appDel.isCallActive)
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalListViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 424)];
                    else
                        [aCalListViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-6)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 424)];
                    else
                        [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                }

            }
            else
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalListViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 402)];
                    else
                        [aCalListViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-28)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                    else
                        [aCalListViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                }
                
            }
            
            aCalListViewController.view.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
            self.calling_VC = aCalListViewController;//Aman's Added
			[self.view addSubview:aCalListViewController.view];
            
            
			break;
		}
		case DAY_BUTTON_TAG:
		{
			preSelectedIndex = DAY_BUTTON_TAG;
            
            /////// Steve added - Saves preSelectedIndex to wherever the user left off last
            //[sharedDefaults setInteger:preSelectedIndex forKey:@"DefaultCalendar"];
            //[sharedDefaults synchronize];
            
			
            if (IS_IPHONE_5) 
                aCalDailyViewController = [[CalendarDailyViewController alloc] initWithNibName:@"CalendarDailyViewController_iPhone5" bundle:[NSBundle mainBundle]];
            else
                aCalDailyViewController = [[CalendarDailyViewController alloc] initWithNibName:@"CalendarDailyViewController" bundle:[NSBundle mainBundle]];
            
            
			if(!appDel.isCallActive)
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 424)];
                    else
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-6)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 424)];
                    else
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                }
            }
            else
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 402)];
                    else
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-28)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                    else
                        [aCalDailyViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                }
                
            }
            
            aCalDailyViewController.view.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
            
            self.calling_VC = aCalDailyViewController;//Aman's Added
            
            
            [self.view addSubview:aCalDailyViewController.view];
            
            
            break;
			
		}
		case WEEK_BUTTON_TAG:
		{
			preSelectedIndex = WEEK_BUTTON_TAG;
            
            /////// Steve added - Saves preSelectedIndex to wherever the user left off last
            //[sharedDefaults setInteger:preSelectedIndex forKey:@"DefaultCalendar"];
            //[sharedDefaults synchronize];
            
            
            if (IS_IPHONE_5) {
                aCalWeekViewController = [[CalendarWeekViewController alloc] initWithNibName:@"CalendarWeekViewController_iPhone5" bundle:[NSBundle mainBundle]];
            }
            else
                aCalWeekViewController = [[CalendarWeekViewController alloc] initWithNibName:@"CalendarWeekViewController" bundle:[NSBundle mainBundle]];

    
            
            if(!appDel.isCallActive)
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 424)]; //424
                    else
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-6)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 424)]; //424
                    else
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                }

            }
            else
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 402)];
                    else
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80 - 44 , 320, 342-28)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                    else
                        [aCalWeekViewController.view setFrame:CGRectMake(0, 80 , 320, 342-28)];
                }

                
            }
            self.calling_VC = aCalWeekViewController;//Aman's Added
            
			[self.view addSubview:aCalWeekViewController.view];
            
            
			break;
		}
		case MONTH_BUTTON_TAG:
		{
			preSelectedIndex = MONTH_BUTTON_TAG;
            
            /////// Steve added - Saves preSelectedIndex to wherever the user left off last
            //[sharedDefaults setInteger:preSelectedIndex forKey:@"DefaultCalendar"];
            //[sharedDefaults synchronize];
            
			aCalMonthViewController = [[CalendarMonthViewController alloc] initWithNibName:@"CalendarMonthViewController" bundle:[NSBundle mainBundle]];
			aCalMonthViewController.toolBarDelegate = self; //Steve - for Protocal in Calendar Monthview
            
            if(!appDel.isCallActive)
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 424)];//424
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-6)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 424)];//424
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 342-6)];
                }
                
            }
            else
            {
                if ([sharedDefaults boolForKey:@"NavigationNew"]) {
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 402)];
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80 - 44, 320, 342-28)];
                }
                else{
                    if (IS_IPHONE_5)
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 402)];
                    else
                        [aCalMonthViewController.view setFrame:CGRectMake(0, 80, 320, 342-28)];
                }
                
            }
            
            self.calling_VC = aCalMonthViewController;//Aman's Added
            
            [self.view addSubview:aCalMonthViewController.view];
            
            
            
			break;
		}
		default:
			break;
	}
}

- (IBAction)textToSpeak:(id)sender {
    if(searchBarViewController){
        [searchBarViewController.view removeFromSuperview];
        searchBarViewController =nil;
    }

    [calling_VC eventToSpeak];
}

- (IBAction)helpView_Tapped:(id)sender {
    
    isHelpShowing = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationController.navigationBarHidden = NO;
        
    }
    
}

-(void) helpViewNewNav_Tapped{
    
    isHelpShowing = NO;
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [helpViewNewNav.layer addAnimation:transition forKey:kCATransitionFade];
    
    helpViewNewNav.hidden = YES;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"FirstLaunch_Calendar2"];
    [sharedDefaults synchronize];
}




#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -

-(void)listButton_Clicked:(UIButton*)sender {
    
    [(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:NO];
    currentSelectedBtnTag = sender.tag;
    [sender setSelected:YES];
    if ([APPINSTANCE isLoded]) {
        [self changedSelection];

    }
    
}

- (IBAction)listBtnClicked:(id)sender {
    LIstToDoProjectViewController *ptr_ListToDoViewController = [[LIstToDoProjectViewController alloc]initWithNibName:@"LIstToDoProjectViewController" bundle:nil];
    [self.navigationController pushViewController:ptr_ListToDoViewController animated:YES];
}

-(void)dayButton_Clicked:(UIButton*)sender {
	[(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:NO];
	currentSelectedBtnTag = sender.tag;
	[sender setSelected:YES];
	if ([APPINSTANCE isLoded]) {
		[self changedSelection];
	}
}

-(void)weekButton_Clicked:(UIButton*)sender {
	[(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:NO];
	currentSelectedBtnTag = sender.tag;
	[sender setSelected:YES];
	if ([APPINSTANCE isLoded]) {
		[self changedSelection];
	}
}

-(void)monthButton_Clicked:(UIButton*)sender {
	[(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:NO];
	currentSelectedBtnTag = sender.tag;
	[sender setSelected:YES];
	if ([APPINSTANCE isLoded]) {
		[self changedSelection];
	}
}

-(IBAction)todayButton_Clicked:(id)sender {
   // NSLog(@"todayButton_Clicked");
	
    NSUserDefaults *shareDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([shareDefaults boolForKey:@"NavigationNew"]) {
        
        currentSelectedBtnTag = preSelectedIndex;
        
       // NSLog(@"currentSelectedBtnTag = %d",currentSelectedBtnTag);
        
        switch (currentSelectedBtnTag)
        {
            case 0: //List
            {
                if(searchBarViewController){
                    [searchBarViewController.view removeFromSuperview];
                    searchBarViewController =nil;
                }
                [aCalListViewController gotoToday];
                break;
            }
            case 1: //Day
            {
                [aCalDailyViewController gotoToday];
                break;
            }
            case 2: //Week
            {
                [aCalWeekViewController gotoToday];
                break;
            }
            case 3: //Month
            {
                [aCalMonthViewController gotoToday];
                break;
            }
            default:
                break;
        }

    }
    else{
        switch (currentSelectedBtnTag)
        {
            case LIST_BUTTON_TAG:
            {
                if(searchBarViewController){
                    [searchBarViewController.view removeFromSuperview];
                    searchBarViewController =nil;
                }
                [aCalListViewController gotoToday];
                break;
            }
            case DAY_BUTTON_TAG:
            {
                [aCalDailyViewController gotoToday];
                break;
            }
            case WEEK_BUTTON_TAG:
            {
                [aCalWeekViewController gotoToday];
                break;
            }
            case MONTH_BUTTON_TAG:
            {
                [aCalMonthViewController gotoToday];
                break;
            }
            default:
                break;
        }
    }
	
	}

-(IBAction)optionButton_Clicked:(id)sender {
	[self showSearchOptPopover:NO];
}

-(IBAction)sortBarButton_Clicked:(id)sender {
	[self showSearchOptPopover:NO];
}

-(IBAction)searchBarButton_Clicked:(id)sender {
   // NSLog(@"searchBarButton_Clicked CalendarParentController");
    
    //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
    if(IS_IPAD)
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    else
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
	
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        currentSelectedBtnTag = 0;
        preSelectedIndex = 0;
        segmentedCalendarControl.selectedSegmentIndex = preSelectedIndex;
    }
    else{
        [(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:NO];
        currentSelectedBtnTag = LIST_BUTTON_TAG;
        [(UIButton *)[self.view viewWithTag:currentSelectedBtnTag] setSelected:YES];
    }
    
	
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) 
        [self changedSelectionForSegmentController:nil];
    else
        [self changedSelection];
    
    

	
	if (searchBarViewController) {
        
		if ([searchBarViewController.view alpha] == 0) {
            
			[self showSearchOptPopover:YES];
		}else {
            
			[self showSearchOptPopover:NO];
		}
	}else {
        
		[self showSearchOptPopover:YES];
	}
}

-(IBAction)inputOptionButton_Clicked:(id)sender {
    
    if(IS_IPAD)
    {
        ////Steve commented.  Don't need to hide since we are using Modal view, which covers the tabbar
       // OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
        //[myAppdelegate hideTabBar:myAppdelegate.tabBarController];
    }
	if (inputTypeSelectorViewController) {
		[inputTypeSelectorViewController.view removeFromSuperview];
		inputTypeSelectorViewController = nil;
	}
    
    
    if  (appDelegate.isEventStorePermissionGranted == YES) //Steve added to test if permission is granted to eventstore
    {
    
        if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized){
            
            inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(15, 292, 150, 200) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(45, 403) andParentVC:self fromMainScreen:YES];
            
            inputTypeSelectorViewController.view.tag = IPTYPE_VC_VIEW_TAG;
            [inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
            [self.view addSubview:inputTypeSelectorViewController.view];
        }
        else
        {
            if (IS_IOS_7) {
                
                AppointmentViewController *addEditCalendarViewController;
                
                //pooja-iPad
                if(IS_IPAD){
                    addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController2_iOS7_iPad" bundle:[NSBundle mainBundle]];
                }
                else{
                    addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController2_iOS7" bundle:[NSBundle mainBundle]];
                }

                
                [addEditCalendarViewController setAppointmentType:@"T"];
                addEditCalendarViewController.isAdd = YES;
                addEditCalendarViewController.isHandwritingShowing = YES;
                
                
                //Steve
                if (IS_IPAD) {
                   
                    [self.navigationController presentViewController:addEditCalendarViewController animated:YES completion:nil];//Modal View
                    
                }
                else{
                    [self.navigationController pushViewController:addEditCalendarViewController animated:NO];
                }
                
             
                
                //  selectedInputType = @"H";
                HandWritingViewController *handWritingViewController;
                
                //pooja-iPad
                if(IS_IPAD){
                    /* //Steve commented
                    //handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewControllerIPad"  bundle:[NSBundle mainBundle] withParent:addEditCalendarViewController isPenIcon:NO];
                    */
                }

                else{ //iPhone
                    handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:addEditCalendarViewController isPenIcon:NO];
                }

               
                
                //Steve
                if (!IS_IPAD) { //iPhone
                 
                    [self.navigationController pushViewController:handWritingViewController animated:NO];

                }
               
                
            }
            else{
                
                AppointmentViewController *addEditCalendarViewController = [[AppointmentViewController alloc] initWithNibName:@"AppointmentViewController2" bundle:[NSBundle mainBundle]];
                [addEditCalendarViewController setAppointmentType:@"T"];
                addEditCalendarViewController.isAdd = YES;
        
                [self.navigationController pushViewController:addEditCalendarViewController animated:NO];
                
                //  selectedInputType = @"H";
                HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:addEditCalendarViewController isPenIcon:NO];
        
                [self.navigationController pushViewController:handWritingViewController animated:NO];
            }
           
        }     
        
    }
    else
    {
        [self calendarPrivacySettingsInstructions]; //if no permission, then show instructions on how to turn it on
    }
    
    
}

-(IBAction)homeButton_Clicked:(id)sender {
    //NSLog(@"HomeButton_Clicked");
	[self.navigationController popToRootViewControllerAnimated:YES]; //Steve TEST commented 
}

-(IBAction)todoButton_Clicked:(id)sender {
	[self.navigationController popViewControllerAnimated:NO];
	
	OrganizerAppDelegate  *appDelegate1 = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	ToDoParentController *toDoParentController = [[ToDoParentController alloc] initWithNibName:@"ToDoParentController" bundle:[NSBundle mainBundle]];
	[appDelegate1.navigationController pushViewController:toDoParentController animated:YES];
}

-(IBAction)addAppointment:(id)sender
{
    if(IS_IPAD)
    {
        ////Steve commented.  Don't need to hide since we are using Modal view, which covers the tabbar
        //OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
        //[myAppdelegate hideTabBar:myAppdelegate.tabBarController];
    }
    
    if  (appDelegate.isEventStorePermissionGranted == YES)
    {
        
        if (IS_IOS_7) {
            
            
            AppointmentViewController *appContrl;
            
            
             //pooja-iPad
            if(IS_IPAD)
            {
                appContrl= [[AppointmentViewController alloc]initWithNibName:@"AppointmentViewController2_iOS7_iPad" bundle:[NSBundle mainBundle]];
            }
            else
            {
               appContrl= [[AppointmentViewController alloc]initWithNibName:@"AppointmentViewController2_iOS7" bundle:[NSBundle mainBundle]];
            }
            
            
            appContrl.aAppObj = nil;
            appContrl.isAdd = YES;
            
            
            //Steve
            if (IS_IPAD) {
                
                [self.navigationController presentViewController:appContrl animated:YES completion:nil];//Modal View
            }
            else{ //iPhone
                
                [self.navigationController pushViewController:appContrl animated:YES];
            }
            
        }
        else{
            
            AppointmentViewController* appContrl= [[AppointmentViewController alloc]initWithNibName:@"AppointmentViewController2" bundle:[NSBundle mainBundle]];
            appContrl.aAppObj = nil;
            appContrl.isAdd = YES;
            
            [self.navigationController pushViewController:appContrl animated:YES];
        }
        

    }
    else
    {
        [self calendarPrivacySettingsInstructions];
    }
    
    
}


-(void)showSearchOptPopover:(BOOL) show {
    //NSLog(@"showSearchOptPopover");
    
	if (show) {
        
		if (searchBarViewController) {
			searchBarViewController = nil;
             //pooja-iPad
			if(IS_IPAD)
            {
                searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController_iPad" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(40, 316, 500, 45) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(92, 396) withParentVC:self];
            }
            else
            {
			searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(40, 316, 300, 45) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(92, 396) withParentVC:self];
            }
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			[self.view addSubview:searchBarViewController.view];
			
		}else {
             //pooja-iPad
            if(IS_IPAD)
            {
                searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController_iPad" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(40, 316 - 64, 500, 45) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(92, 396) withParentVC:self];
            }
            else
            {
                searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(40, 316 - 64, 300, 45) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(92, 396) withParentVC:self];
            }
            //searchBarViewController = [[SearchBarViewController alloc] initWithNibName:@"SearchBarViewController" bundle:[NSBundle mainBundle] withContentFrame:CGRectMake(40, 200, 300, 45) andArrowDirection:@"D" andArrowStartPoint:CGPointMake(92, 396) withParentVC:self];
            
			[searchBarViewController.view setBackgroundColor:[UIColor clearColor]];
			
			[self.view addSubview:searchBarViewController.view];
		}
	} else {
		if (searchBarViewController) {
			if ([searchBarViewController.view alpha] == 1) {
				[searchBarViewController.view setAlpha:0];
			}else {
				[searchBarViewController.view removeFromSuperview];
				searchBarViewController = nil;
			}
		}
	}
    
    //[searchBarViewController.view setFrame:CGRectMake(0, 200, 320, 45)]; //Steve commented. Frame set in SearchBarViewController Class

	[self.view bringSubviewToFront:searchBarViewController.view];
}


#pragma mark -
#pragma mark Data Methods
#pragma mark -

#pragma mark -
#pragma mark Memory Management Methods
#pragma mark -

- (void)didReceiveMemoryWarning  {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload  {
    //NSLog(@"viewDidUnload CalendarParentController");
    
    helpView = nil;
    tapHelpView = nil;
    helpViewOverlay = nil;
    swipeDownHelpView = nil;
    calendarAccessView = nil;
    calendarAccessScrollView = nil;
    aNavBarAccessHelpMenu = nil;
    homeButton = nil;
    revealButton = nil;
     addButton = nil;
    helpViewNewNav = nil;
    helpViewChangeNavInstructions = nil;
    [self setRateAppView:nil];
    [self setNeverRateButton:nil];
    [self setLaterRateButton:nil];
    [self setRateBtton:nil];
    [self setRateAppView_MainView:nil];
    [self setRateTextField:nil];
    [self setRateTitleLabel:nil];
    [super viewDidUnload];
}

-(void) viewWillDisappear:(BOOL)animated{
   // NSLog(@"viewWillDisappear CalendarParentController");
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        self.navigationItem.rightBarButtonItem = nil;
        [addButton removeFromSuperview];
        [segmentedCalendarControl removeFromSuperview];
        
    }
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    
     [_refreshTimer invalidate]; //Steve - Cancel refreshing Calendar
}

- (void)dealloc  {
	[APPINSTANCE setIsLoded:NO];
	
	if(aCalListViewController) {
		[aCalListViewController.view removeFromSuperview];
	}
	if(aCalDailyViewController) {
		[aCalDailyViewController.view removeFromSuperview];
	}
	if(aCalWeekViewController) {
		[aCalWeekViewController.view removeFromSuperview];
	}
	if(aCalMonthViewController) {
		[aCalMonthViewController.view removeFromSuperview];
	}
    if(inputTypeSelectorViewController) {
		[inputTypeSelectorViewController.view removeFromSuperview];
	}
    //	if (notification) [notification release];
	
}

- (IBAction)neverRateButton_Clicked:(id)sender {
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.rateAppView_MainView.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.rateAppView_MainView.hidden = YES;
    
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"RateApp"];  //so app is never rated
    [sharedDefaults synchronize];
    
}

- (IBAction)laterRateButton_Clicked:(id)sender {
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.rateAppView_MainView.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.rateAppView_MainView.hidden = YES;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setInteger:0 forKey:@"CountUntilRate"]; //Reset to 0 so count can start over
    [sharedDefaults setBool:YES forKey:@"RateApp"];  //so app can be rated
    [sharedDefaults synchronize];
    
}

- (IBAction)rateButton_Clicked:(id)sender {
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.5;
    [self.rateAppView_MainView.layer addAnimation:transition forKey:kCATransitionFade];
    
    self.rateAppView_MainView.hidden = YES;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
  
    
    /////////////////////// Steve update for iOS 7 - Opens App Store to review App /////////////////////////////////////////

    if(IS_LIST_VERSION){
        
        [Appirater setAppId:@"584611125"];
        [Appirater rateApp];
        
    }
    else if (IS_TO_DO_VERSION){
        
        [Appirater setAppId:@"591343485"];
        [Appirater rateApp];
        
    }
    else if (IS_PROJECTS_VER){
        
        [Appirater setAppId:@"591634217"];
        [Appirater rateApp];
        
    }
    else if (IS_NOTES_VER){
        
        [Appirater setAppId:@"591713931"];
        [Appirater rateApp];
    }
    else{ //Pro Version
        
        //[Appirater setAppId:@"545904979"];
        //[Appirater rateApp];
        
        NSString *iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
        iOSAppStoreURLFormat = [NSString stringWithFormat:iOSAppStoreURLFormat,545904979];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iOSAppStoreURLFormat]];
        
    }
    
    ////////////////////////////// Steve end ////////////////////////////////////////////
    
    
    //Steve commented - doesn't work with iOS 7
    /*
    /////////////////////// Steve - Opens App Store to review App /////////////////////////////////////////
    
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=APP_ID";
    NSString *reviewURL;
    
    if(IS_LIST_VERSION)
        reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", 584611125]];
    else if (IS_TO_DO_VERSION)
        reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", 591343485]];
    else if (IS_PROJECTS_VER)
        reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", 591634217]];
    else if (IS_NOTES_VER)
        reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", 591713931]];
    else //Pro Version
        reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", 545904979]];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
    
    ////////////////////////////// Steve end ////////////////////////////////////////////
    */
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    [sharedDefaults setBool:NO forKey:@"RateApp"];  //so app won't be rated again
    [sharedDefaults synchronize];
    
}

- (IBAction)noteButtonClicked:(id)sender {
    NotesViewCantroller *notesViewCantroller =[[NotesViewCantroller alloc]initWithNibName:@"NotesViewCantroller" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:notesViewCantroller animated:YES];
    
}

- (IBAction)projectButtonClicked:(id)sender {
    ProjectMainViewCantroller *projectsViewController = [[ProjectMainViewCantroller alloc] initWithNibName:@"ProjectMainViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:projectsViewController animated:YES];
}


//************ STill working *******************
-(void)openCalendarSelection{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"FAQViewController" bundle:nil];
    UIViewController *faqViewController = [sb instantiateViewControllerWithIdentifier:@"FAQViewController"];
    
    [self presentViewController:faqViewController animated:YES completion:nil];
    
}


- (IBAction)calendarChoices:(UIBarButtonItem *)sender {
    
    UIStoryboard *storyBoard;
     //pooja-iPad
    if(IS_IPAD)
    {
        storyBoard = [UIStoryboard storyboardWithName:@"CalendarChoicesStoryboard_iPad" bundle:nil];
    }
    else
    {
        storyBoard = [UIStoryboard storyboardWithName:@"CalendarChoicesStoryboard" bundle:nil];
    }
    UIViewController *calendarVC = [storyBoard instantiateViewControllerWithIdentifier:@"CalendarChoicesStoryboard"];
    [self presentViewController:calendarVC animated:YES completion:nil];
    
}

-(void)settingsButton_Clicked{
    
    UIStoryboard *storyBoard;
    
     //pooja-iPad
    if(IS_IPAD)
    {
        storyBoard= [UIStoryboard storyboardWithName:@"CustomizeCalendarStoryboard_iPad" bundle:[NSBundle mainBundle]];
    }
    else
    {
     storyBoard= [UIStoryboard storyboardWithName:@"CustomizeCalendarStoryboard" bundle:nil];
    }
    
    CustomizeCalendarStoryboardViewController *customizeCalendarSBVC = (CustomizeCalendarStoryboardViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"CustomizeCalendarStoryboard"];

    [self.navigationController presentViewController:customizeCalendarSBVC animated:YES completion:nil];
   

}


//Steve - works with protocal on Calendar Month View
-(void)SetToolBarToFrontPosition:(BOOL) isToolBarFront{
    
    if (isToolBarFront)
        [self.view bringSubviewToFront:aBottomToolBar];
    
}


//Steve - copied Apple's blur technique, but added it on a thread.  Couldn't add the actual method on a thread since it returns an image. "return" doesn't work
//on Threads since it returns before the thread values change
- (void)applyBlurWithRadiusOnThread:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage
{
    
    CGSize size =  _backgroundPictureImageView.frame.size;
    
    // Check pre-conditions.
    if (size.width < 1 || size.height < 1) {
        NSLog (@"*** error: invalid size: (%.2f x %.2f). Both dimensions must be >= 1: %@", size.width, size.height, _backgroundPictureImageView);
        return;
    }
    
    if (!_backgroundPictureImageView.image.CGImage) {
        NSLog (@"*** error: image must be backed by a CGImage: %@", self);
        return;
    }
    
    if (maskImage && !maskImage.CGImage) {
        NSLog (@"*** error: maskImage must be backed by a CGImage: %@", maskImage);
        return;
    }
    
    
    //Shows "Blur Processing..."
    //[self performSelector:@selector(blurProcessingViewAfterDelay) withObject:nil afterDelay:1.0];
    //_activityOfBlur.hidden = NO;
    //[_activityOfBlur startAnimating];//Spinner
    
    
    __block CGRect imageRect = { CGPointZero, _backgroundPictureImageView.frame.size };
    __block UIImage *effectImage = [UIImage new];
    
    
    
    dispatch_queue_t fetchingEvents_queue = dispatch_queue_create("ApplyBlurWithRadiusThread", NULL);
    dispatch_async(fetchingEvents_queue, ^{
        
        
        BOOL hasBlur = blurRadius > __FLT_EPSILON__;
        BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
        
        if (hasBlur || hasSaturationChange) {
            UIGraphicsBeginImageContextWithOptions(_backgroundPictureImageView.frame.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef effectInContext = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(effectInContext, 1.0, -1.0);
            CGContextTranslateCTM(effectInContext, 0, -_backgroundPictureImageView.frame.size.height);
            CGContextDrawImage(effectInContext, imageRect, _backgroundPictureImageView.image.CGImage);
            
            vImage_Buffer effectInBuffer;
            effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
            effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
            effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
            effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
            
            UIGraphicsBeginImageContextWithOptions(_backgroundPictureImageView.frame.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
            vImage_Buffer effectOutBuffer;
            effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
            effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
            effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
            effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
            
            if (hasBlur) {
                // A description of how to compute the box kernel width from the Gaussian
                // radius (aka standard deviation) appears in the SVG spec:
                // http://www.w3.org/TR/SVG/filters.html#feGaussianBlurElement
                //
                // For larger values of 's' (s >= 2.0), an approximation can be used: Three
                // successive box-blurs build a piece-wise quadratic convolution kernel, which
                // approximates the Gaussian kernel to within roughly 3%.
                //
                // let d = floor(s * 3*sqrt(2*pi)/4 + 0.5)
                //
                // ... if d is odd, use three box-blurs of size 'd', centered on the output pixel.
                //
                CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
                
                NSUInteger radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
                
                if (radius % 2 != 1) {
                    radius += 1; // force radius to be odd so that the three box-blur methodology works.
                }
                vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
                vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
                vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            }
            
            BOOL effectImageBuffersAreSwapped = NO;
            
            if (hasSaturationChange) {
                CGFloat s = saturationDeltaFactor;
                CGFloat floatingPointSaturationMatrix[] = {
                    0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                    0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                    0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                    0,                    0,                    0,  1,
                };
                const int32_t divisor = 256;
                NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
                int16_t saturationMatrix[matrixSize];
                
                for (NSUInteger i = 0; i < matrixSize; ++i) {
                    saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
                }
                
                if (hasBlur) {
                    vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                    effectImageBuffersAreSwapped = YES;
                }
                else {
                    vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                }
            }
            
            
            if (!effectImageBuffersAreSwapped)
                effectImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            if (effectImageBuffersAreSwapped)
                effectImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // Set up output context.
            UIGraphicsBeginImageContextWithOptions(_backgroundPictureImageView.frame.size, NO, [[UIScreen mainScreen] scale]);
            CGContextRef outputContext = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(outputContext, 1.0, -1.0);
            CGContextTranslateCTM(outputContext, 0, -_backgroundPictureImageView.frame.size.height);
            
            // Draw base image.
            CGContextDrawImage(outputContext, imageRect, _backgroundPictureImageView.image.CGImage);
            
            // Draw effect image.
            if (hasBlur) {
                CGContextSaveGState(outputContext);
                
                if (maskImage) {
                    CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
                }
                
                CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
                CGContextRestoreGState(outputContext);
            }
            
            // Add in color tint.
            if (tintColor) {
                CGContextSaveGState(outputContext);
                CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
                CGContextFillRect(outputContext, imageRect);
                CGContextRestoreGState(outputContext);
            }
            
            
            // Output image is ready.
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            _backgroundPictureImageView.image = outputImage;
            
        });
    });
    
    
}










@end

//
//  NotesDetailViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesDetailViewCantroller.h"


@implementation NotesDetailViewCantroller

@synthesize fetchNotesObject, newTable;
@synthesize navBar,aWebView;
@synthesize GETValue;
@synthesize IDValueArray;



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	NSLog(@"%d",GETValue);
	NSLog(@"%@",IDValueArray);
	
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 160;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{	
	return [IDValueArray count];
	//return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
	
	return 1;
	
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = 
	[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	}	
	
	
	for (UIView *subView in [cell.contentView subviews])
	{
		[subView removeFromSuperview];
	}
	
	
	if(GETValue == 111)
	{
	
	UIImage *titleImage = [[UIImage alloc ] initWithData:
						   [[IDValueArray objectAtIndex:indexPath.section]imginfo]];
	UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
	[titleImageView.layer setBorderWidth:1.0];
	[titleImageView.layer setCornerRadius:5.0];
	titleImageView.layer.masksToBounds = YES;
	//titleImageView.tag =222;
	//[titleImageView addTarget:self action:@selector(showFullPicture:) forControlEvents:UIControlEventTouchUpInside];
	[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	[cell.contentView addSubview:titleImageView];
	[titleImage release];
	[titleImageView release];	
		
	}
	else if(GETValue == 222)
	{
	
		UIImage *titleImage = [[UIImage alloc ] initWithData:
							   [[IDValueArray objectAtIndex:indexPath.section]sktinfo]];
		UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
		[titleImageView.layer setBorderWidth:1.0];
		[titleImageView.layer setCornerRadius:5.0];
		titleImageView.layer.masksToBounds = YES;
				[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
		[cell.contentView addSubview:titleImageView];
		[titleImage release];
		[titleImageView release];
	
	
	
	}
	
	else if(GETValue == 333)
	{
		
		UIImage *titleImage = [[UIImage alloc ] initWithData:
							   [[IDValueArray objectAtIndex:indexPath.section]mapinfo]];
		UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
		[titleImageView.layer setBorderWidth:1.0];
		[titleImageView.layer setCornerRadius:5.0];
		titleImageView.layer.masksToBounds = YES;
		[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
		[cell.contentView addSubview:titleImageView];
		[titleImage release];
		[titleImageView release];
		
	}
	else if(GETValue == 444)
	{
		
		UIImage *titleImage = [[UIImage alloc ] initWithData:
							   [[IDValueArray objectAtIndex:indexPath.section]hwrtinfo]];
		UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
		[titleImageView.layer setBorderWidth:1.0];
		[titleImageView.layer setCornerRadius:5.0];
		titleImageView.layer.masksToBounds = YES;
		[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
		[cell.contentView addSubview:titleImageView];
		[titleImage release];
		[titleImageView release];
		
	}
	
	else if(GETValue == 555)
	{
		
		
		
		
		UITextView *detailLabel = [[UITextView alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
		detailLabel.tag = 20;
		[cell.contentView addSubview:detailLabel];
		detailLabel.layer.borderWidth = 1;
		detailLabel.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
		detailLabel.layer.cornerRadius = 10;
		detailLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
		NSString *cellValue =[[IDValueArray objectAtIndex:indexPath.section]textinfo];
		detailLabel.text=cellValue;
		detailLabel.editable=NO;
		[cell.contentView addSubview:detailLabel];
		[detailLabel release];
		
	}
	else if(GETValue == 666)
	{
		
		UIImage *titleImage = [[UIImage alloc ] initWithData:
							   [[IDValueArray objectAtIndex:indexPath.section]webImg]];
		UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(13, 13, 275, 133)];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
		[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
		[titleImageView.layer setBorderWidth:1.0];
		[titleImageView.layer setCornerRadius:5.0];
		titleImageView.layer.masksToBounds = YES;
		[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
		[cell.contentView addSubview:titleImageView];
		[titleImage release];
		[titleImageView release];
		
				
	}
	
	
	
	return cell;
	
}






















	
	
	/*NSLog(@"%d",indexNo);
	if(indexNo == 1)
	{	
		imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 44.0, 320.0, 480.0)];
		//imageView.image = [UIImage imageWithData:fetchNotesObject.notesPicBinaryimgLarge];
		imageView.userInteractionEnabled = YES;
		imageView.opaque = YES;
		[self.view addSubview:imageView];
	}
	else if(indexNo == 2)
	{	
		imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 44.0, 320.0, 480.0)];
		imageView.image = [UIImage imageWithData:fetchNotesObject.notessktchBinaryimgLarge];
		imageView.userInteractionEnabled = YES;
		imageView.opaque = YES;
		[self.view addSubview:imageView];
	}
	else if(indexNo == 3)
	{	
		imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 44.0, 320.0, 480.0)];
		//imageView.image = [UIImage imageWithData:fetchNotesObject.notesMapBinaryimgLarge];
		imageView.userInteractionEnabled = YES;
		imageView.opaque = YES;
		[self.view addSubview:imageView];
	}
	else
	{
		aWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 44, 320, 480)];//init and create the UIWebView
		aWebView.autoresizesSubviews = YES;
		aWebView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
		//set the web view delegates for the web view to be itself
		[aWebView setDelegate:self];
		//Set the URL to go to for your UIWebView
		NSString *urlAddress = fetchNotesObject.notesWebURL; //i get 1st error here
		//Create a URL object.
		NSURL *url = [NSURL URLWithString:urlAddress];
		//URL Requst Object
		NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
		//load the URL into the web view.
		[aWebView loadRequest:requestObj];
		//add the web view to the content view
		[self.view addSubview:aWebView];
	 
	}	*/


-(IBAction)BackButton_Clicked:(id)sender 
{
	[self.navigationController popViewControllerAnimated:YES];
}



/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	 [aWebView release];
	[newTable release];

}
@end







//
//  iCloudBackupViewController.h
//  Organizer
//
//  Created by Primo on 7/2/15.
//
//

#import <UIKit/UIKit.h>

@interface iCloudBackupViewController : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar_icloud;

@end

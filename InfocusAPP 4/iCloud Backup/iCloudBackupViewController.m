//
//  iCloudBackupViewController.m
//  Organizer
//
//  Created by Primo on 7/2/15.
//
//

#import "iCloudBackupViewController.h"
#import "OrganizerAppDelegate.h"
#import <iCloud/iCloud.h>


@interface iCloudBackupViewController ()<iCloudDelegate>
{
    //** Cloud
    iCloud* cloud;
    
    BOOL cloudIsAvailable;
    BOOL deleteAll;
    BOOL isAllLooksDelete;
    
    
    NSMutableArray *fileNameList;
    NSMutableArray *fileObjectList;
    UIRefreshControl *refreshControl;
    
    NSString *fileText;
    NSString *fileTitle;
    
    
    IBOutlet UIActivityIndicatorView*_activity;
    
}
@end

@implementation iCloudBackupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self hideActivityIndicator];
    
    //** icloud
    [self initializeiCloudDelegatesAndMethod];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navBar_icloud setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navBar_icloud.tintColor = [UIColor whiteColor];
    
    self.navBar_icloud.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
    self.navBar_icloud.translucent=YES;
    
    self.navBar_icloud.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBarHidden = YES;
}
-(IBAction)DoneButtonClicked:(id)sender{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(void)hideActivityIndicator
{
    _activity.hidden=TRUE;
    [_activity stopAnimating];

}
-(void)showActivityIndicator{
    _activity.hidden=FALSE;
    [_activity startAnimating];

}

#pragma mark -
#pragma mark -Cloud

-(void)initializeiCloudDelegatesAndMethod{
    //** icloud
    [[iCloud sharedCloud] setDelegate:self];
    [[iCloud sharedCloud] setVerboseLogging:YES]; // We want detailed feedback about what's going on with iCloud, this is OFF by default
    [[iCloud sharedCloud] updateFiles]; // Force iCloud Update: This is done automatically when changes are made, but we want to make sure the view is always updated when presented
    cloudIsAvailable = [[iCloud sharedCloud] checkCloudAvailability];
    
    
    // Setup File List
    if (fileNameList == nil) fileNameList = [NSMutableArray array];
    if (fileObjectList == nil) fileObjectList = [NSMutableArray array];
    
    if (cloudIsAvailable)
    {
        cloud = [[iCloud sharedCloud] init];
        
        
    }
    else
    {
        UIAlertView*alert = [[UIAlertView alloc]initWithTitle:@"" message:@"iCloud is Unavailable Please turn on iCloud from settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        alert.tag = 300;
        [alert show];
        
    }
}

#pragma mark UploadBackUp To iCloud
-(IBAction)upLoadDataBaseToiCloud:(id)sender
{
    [self showActivityIndicator];

    
    __block NSData* dt=nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                            NSUserDomainMask,
                                                                            YES) lastObject];
        //******* Upload To iCloud
        __block NSString *DB = [documentsDirectory stringByAppendingPathComponent:DATABASE_FILE_NAME];
        
        dt = [NSData dataWithContentsOfFile:DB];
        
        dispatch_async(dispatch_get_main_queue(), ^()
                       {
                           if([dt isEqualToData:NULL]){
                               NSLog(@"null data") ;
                           }
                           
                           [[iCloud sharedCloud] saveAndCloseDocumentWithName:DATABASE_FILE_NAME withContent:dt completion:^(UIDocument *cloudDocument, NSData *documentData, NSError *error) {
                               if (error == nil)
                               {
                                   
                                   NSLog(@"iCloud Document, %@, saved with text: %@", cloudDocument.fileURL.lastPathComponent, [[NSString alloc] initWithData:documentData encoding:NSUTF8StringEncoding]);
                                [self hideActivityIndicator];
                                   
                                   UIAlertView *alertUploaded = [[UIAlertView alloc]initWithTitle:@"Uploaded" message:@"Backup has been uploaded successfully!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                   alertUploaded.tag = 300;
                                   [alertUploaded show];
                               
                               }
                               
                               else
                               {
                                   NSLog(@"iCloud Document save error: %@", error);
                                   
                                   [self hideActivityIndicator];

                               }
                           }];
                       });
    });
}

#pragma mark Download backUp Data
-(IBAction)downloadDataBaseFromiCloud:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please upload your changes before downloading backup files or you will lose the changes you have made." delegate:self cancelButtonTitle:@"Upload changes" otherButtonTitles:@"Download", nil ];
    alert.tag=800;
    [alert show];

}


-(void)downloadBackUpFiles{
    
    NSLog(@"Download from iCloud...");
    [self showActivityIndicator];
    
    if(![fileNameList count]==0){
        
        [[iCloud sharedCloud] retrieveCloudDocumentWithName:[fileNameList objectAtIndex:0] completion:^(UIDocument *cloudDocument, NSData *documentData, NSError *error) {
            if (!error) {
                fileText = [[NSString alloc] initWithData:documentData encoding:NSUTF8StringEncoding];
                fileTitle = cloudDocument.fileURL.lastPathComponent;
                
                [[iCloud sharedCloud] documentStateForFile:fileTitle completion:^(UIDocumentState *documentState, NSString *userReadableDocumentState, NSError *error) {
                    if (!error) {
                        if (*documentState == UIDocumentStateInConflict) {
                            
                            NSData *fileData = documentData;
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];//m_nImageIndex
                            NSString *localPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:DATABASE_FILE_NAME]];
                            NSLog(@"%@",localPath);
                            [fileData writeToFile:localPath atomically:NO];
                            
                            [self hideActivityIndicator];
                            
                            
                        } else {
                            NSLog(@"Download");
                            
                            NSData *fileData = documentData;
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *localPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:DATABASE_FILE_NAME]];
                            NSLog(@"%@",localPath);
                            [fileData writeToFile:localPath atomically:NO];
                            
                            [self hideActivityIndicator];
                            
                            UIAlertView*alertUpload = [[UIAlertView alloc]initWithTitle:@"Downloaded" message:@"Backup has been downloaded successfully!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                            alertUpload.tag = 200;
                            [alertUpload show];
                            
                            
                        }
                    } else {
                        NSLog(@"Error retrieveing document state: %@", error);
                        [self hideActivityIndicator];
                    }
                }];
            } else {
                NSLog(@"Error retrieveing document: %@", error);
                [self hideActivityIndicator];
                
            }
        }];
    }
    
}


#pragma mark - iCloud Delegates

- (void)iCloudDidFinishInitializingWitUbiquityToken:(id)cloudToken withUbiquityContainer:(NSURL *)ubiquityContainer {
    NSLog(@"Ubiquity container initialized. You may proceed to perform document operations.");
}

- (void)iCloudAvailabilityDidChangeToState:(BOOL)cloudIsAvailable withUbiquityToken:(id)ubiquityToken withUbiquityContainer:(NSURL *)ubiquityContainer {
    NSLog(@"chk for iCloud");
}

- (void)iCloudFilesDidChange:(NSMutableArray *)files withNewFileNames:(NSMutableArray *)fileNames {
    // Get the query results
    NSLog(@"Files: %@", fileNames);
    
    fileNameList = fileNames; // A list of the file names
    fileObjectList = files; // A list of NSMetadata objects with detailed metadata
    
    [refreshControl endRefreshing];
    
}

- (void)refreshCloudList {
    [[iCloud sharedCloud] updateFiles];
}

- (void)refreshCloudListAfterSetup {
    // Reclaim delegate and then update files
    [[iCloud sharedCloud] setDelegate:self];
    [[iCloud sharedCloud] updateFiles];
}


#pragma mark -
#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 200)
    {
        
        OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate initializeDatabase];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if(alertView.tag == 300)
    {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (alertView.tag==800){
        if(buttonIndex==1){
            [self downloadBackUpFiles];
        }
        else{
            [self upLoadDataBaseToiCloud:nil];
        }
    }

    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

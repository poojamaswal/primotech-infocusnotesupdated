//
//  CalendarDailyViewController.h
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h> //Steve
#import <EventKitUI/EventKitUI.h> //Steve
#import "GetAllDataObjectsClass.h"
#import "AppointmentsDataObject.h"
#import "CustomeScrollView.h"
#import "OrganizerAppDelegate.h"
//#import "CalendarParentController.h"


@class OrganizerAppDelegate;
//@class CalendarParentController;

@interface CalendarDailyViewController:UIViewController<UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
	IBOutlet CustomeScrollView	*dayTimeScrollView;
    
	IBOutlet UIImageView	*dayTimeImageView;
	IBOutlet UIImageView	*allDayTimeImageView;
	IBOutlet UILabel		*dayNameLabel;
    NSDate					*Date;
    NSString				*currentDate;
	NSDateFormatter			*formater;
	NSMutableArray			*dayObjArray;
    NSMutableArray          *dayObjArrayToShowEventPopup; //Steve
	NSMutableArray			*allDayObjArray;
	UIButton				*lastSelectedEvent;
	IBOutlet UIImageView	*currentTimeImageView;
    OrganizerAppDelegate* appDelegate;
    
    //Steve added to recognize taps
    UITapGestureRecognizer *tap;
    
    //Steve added: recognize swipes
    UISwipeGestureRecognizer    *swipeRight;
    UISwipeGestureRecognizer    *swipeLeft;
    UISwipeGestureRecognizer    *swipeDown;
    UISwipeGestureRecognizer    *swipeUp;
    EKEventStore                *eventStore;
    IBOutlet UIView             *loadingView;
    IBOutlet UIScrollView       *calendarAccessScrollView; //Steve test
    IBOutlet UIView             *calendarAccessView; //Steve test
    IBOutlet UINavigationBar    *aNavBarAccessHelpMenu;
    BOOL                        isRefresh;//Steve 
}


@property(nonatomic, strong) UIButton               *lastSelectedEvent;
@property(nonatomic, strong) NSMutableArray			*dayObjArray;
@property(nonatomic, strong) NSMutableArray			*dayObjArrayToShowEventPopup; //Steve - to show values in popup, which may differ than dayview times & Dates
@property(nonatomic, strong) NSMutableArray			*allDayObjArray;
@property(nonatomic, strong) UIImageView			*currentTimeImageView;
@property(nonatomic, strong) NSMutableString        *strTextToSpeak;//Aman's Added
@property(nonatomic, strong) NSDate                 *strtDate ;//Aman's Added
@property(nonatomic, strong) NSDate                 *endDate ;//Aman's Added
@property(nonatomic, strong) NSMutableArray         *arrM_finalEvents;//Aman's Added
@property(nonatomic, strong) NSMutableString        *str_todaysDate;
@property(nonatomic, strong) NSMutableString        *currentCalanderDate ;//Aman's Added



-(IBAction)nextButton_Clicked:(id) sender;
-(IBAction)previousButton_Clicked:(id) sender;
-(void)gotoToday;
-(void)showData;
-(void)showDataAllDay;
-(void)hidePopups;
-(void)eventToSpeak;//Aman's Added
-(NSMutableString *)finalString;
-(NSInteger)numberofAppointments;
-(BOOL)hasConnectivity;
-(NSString *)callToSpeaktext:(NSString *)callingText;//Aman's Added
- (void) refreshViews; //Steve added
-(NSString *)getfrequency:(int) freq:(int) interval; //alok added
- (IBAction)doneButton_Clicked:(id)sender;


@end

//
//  TextFieldInputView.m
//  CIO-PS
//
//  Created by Gaurav Goyal on 04/06/15.
//  Copyright (c) 2015 CanvasM. All rights reserved.
//

#import "TextFieldInputView.h"

@interface TextFieldInputView () <UITextFieldDelegate>

@end

@implementation TextFieldInputView

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.textField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(inputViewDidEndEditing:)]) {
        [self.delegate inputViewDidEndEditing:textField.text];
    }
    [self.textField resignFirstResponder];
}

@end

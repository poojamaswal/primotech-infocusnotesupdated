//
//  NotesDetailViewCantroller.h
//  Organizer
//
//  Created by Naresh Chouhan on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesDataObject.h"


@interface NotesDetailViewCantroller : UIViewController 
{
	
NotesDataObject *fetchNotesObject;

		NSInteger GETValue;
	    NSMutableArray *IDValueArray;
	    IBOutlet UITableView *newTable;
        IBOutlet UINavigationBar *navBar;
		UIWebView *aWebView;

}
@property(nonatomic,retain)NotesDataObject *fetchNotesObject;
@property(nonatomic,retain)UITableView *newTable;
@property(nonatomic,retain)NSMutableArray *IDValueArray;
@property(readwrite)NSInteger GETValue;
@property(nonatomic,retain)UINavigationBar *navBar;
@property(nonatomic,retain)UIWebView *aWebView;


//-(id)initWithNotesObject:(NotesDataObject *) notesObj;
-(IBAction)BackButton_Clicked:(id)sender ;
@end

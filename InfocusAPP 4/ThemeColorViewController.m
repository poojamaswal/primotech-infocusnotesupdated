//
//  ThemeColorViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 12/16/13.
//
//

#import "ThemeColorViewController.h"

@interface ThemeColorViewController ()

@property (strong, nonatomic) IBOutlet UISwitch *lightThemeColorSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *darkThemeColorSwitch;


- (IBAction)lightThemeColorSwitchValueDidChange:(id)sender;
- (IBAction)darkThemeColorSwitchValueDidChange:(id)sender;


@end

@implementation ThemeColorViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Note: This feature Only available on iOS 7
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Switch on
        
        _lightThemeColorSwitch.on = YES;
        _darkThemeColorSwitch.on = NO;
    }
    else{
        
        _lightThemeColorSwitch.on = NO;
        _darkThemeColorSwitch.on = YES;
    }
    
}

- (IBAction)lightThemeColorSwitchValueDidChange:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (_lightThemeColorSwitch.on) {
        
        [_darkThemeColorSwitch setOn:NO animated:YES];
        
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorRed"];
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorGreen"];
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorBlue"];
        
    }
    else{
        
        [_darkThemeColorSwitch setOn:YES animated:YES];
        
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorRed"];
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorGreen"];
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorBlue"];
    }
}


- (IBAction)darkThemeColorSwitchValueDidChange:(id)sender {
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (_darkThemeColorSwitch.on) {
        
        [_lightThemeColorSwitch setOn:NO animated:YES];
        
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorRed"];
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorGreen"];
        [sharedDefaults setFloat:66.0 forKey:@"DefaultAppThemeColorBlue"];
    }
    else{
        
        [_lightThemeColorSwitch setOn:YES animated:YES];
        
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorRed"];
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorGreen"];
        [sharedDefaults setFloat:245.0 forKey:@"DefaultAppThemeColorBlue"];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

//
//  CalendarTableViewController.h
//  Organizer
//
//  Created by Steven Abrams on 1/31/13.
//
//

#import <UIKit/UIKit.h>

@interface CalendarSettingsViewController : UITableViewController{
    
    IBOutlet UISwitch *listViewSwitch;
    IBOutlet UISwitch *dayViewSwitch;
    IBOutlet UISwitch *weekViewSwitch;
    IBOutlet UISwitch *monthViewSwitch;
    
}

@end

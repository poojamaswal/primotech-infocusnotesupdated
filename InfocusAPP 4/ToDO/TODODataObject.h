//
//  TODODataObject.h
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TODODataObject : NSObject {
	NSInteger	todoID;
	NSString	*todoTitle; 
	NSString	*todoTitleType; 
	NSData		*todoTitleBinary; 
	NSData		*todoTitleBinaryLarge;
	NSInteger   calID;
	NSInteger   projID;
	NSString	*startDateTime; 
	NSString	*dueDateTime; 
	NSString	*createDateTime; 
	NSString	*modifyDateTime; 
	NSInteger	toDoProgress;
	NSString	*priority;
	NSString	*sortingDateTime;
	BOOL		isStarred;
	NSInteger	manuallySortedTodoID;
	
	NSString    *projectName;
	NSString    *calendarName;
	
	NSInteger projPrntID;
	NSInteger projLevel;
    NSString *fullprojPath;
    NSString *alert;   // Anil's Addition // For first alert
    NSString *alertSecond;   // Anil's Addition // For second Alert
    BOOL isAlertOn; // Anil's Addition // Checks if alert is on or off
    NSString    *FolderImage;
    
}
@property(nonatomic,assign)NSInteger projLevel;
@property(nonatomic,assign)NSInteger projPrntID;
@property (nonatomic, assign) NSInteger		todoID;
@property (nonatomic, strong) NSString		*todoTitle;
@property (nonatomic, strong) NSString		*todoTitleType; 
@property (nonatomic, strong) NSData		*todoTitleBinary; 
@property (nonatomic, strong) NSData		*todoTitleBinaryLarge;
@property (nonatomic, assign) NSInteger		calID;
@property (nonatomic, assign) NSInteger		projID;
@property (nonatomic, strong) NSString		*startDateTime;
@property (nonatomic, strong) NSString		*dueDateTime;
@property (nonatomic, strong) NSString		*createDateTime;
@property (nonatomic, strong) NSString		*modifyDateTime; 
@property (nonatomic, assign) NSInteger		toDoProgress;
@property (nonatomic, strong) NSString		*priority;
@property (nonatomic, strong) NSString		*sortingDateTime; 
@property (nonatomic, readwrite) BOOL		isStarred;
@property (nonatomic, assign) NSInteger		manuallySortedTodoID;
@property (nonatomic, strong) NSString		*projectName;
@property (nonatomic, strong) NSString		*calendarName;
@property(nonatomic,strong)NSString *fullprojPath;
@property(nonatomic,strong)NSString *alert;
@property(nonatomic,strong)NSString *alertSecond;
@property (nonatomic, readwrite) BOOL		isAlertOn;
@property(nonatomic,strong)NSDate *modifyDate;
@property(nonatomic,strong)NSString *FolderImage;

-(id)initWithToDoId:(NSInteger) toDoIdLocal todoTitle:(NSString*)todoTitleLocal todoTitleType:(NSString *) todoTitleTypeLocal todoTitleBinary:(NSData *) todoTitleBinaryLocal calid:(NSInteger) calendarIDLocal projectID:(NSInteger) projectsIDLocal startDate:(NSString *)startDateLocal dueDate:(NSString*) dueDateLocal carestedDate:(NSString *) carestedDateLocal modifyDate:(NSString*) modifyDateLocal toDoProgress:(NSInteger) toDoProgressLocal piority:(NSString *) priorityLocal sortingColumn:(NSString *) sortingDateTimeLocal isStarred:(BOOL) isStarredLocal manuallySortTodoID:(NSInteger) manuallySortTodoIDLocal projPrntID:(NSInteger)ProjPrntIDLocal projLevel:(NSInteger)projLevelLocal fullprojPath:(NSString *)fullprojPathLocal alert:(NSString *)alertLocal alertSecond:(NSString *)alertSecondLocal isAlertOn:(BOOL)isAlertOnLocal;

@end

//
//  WEPopoverContentViewController.m
//  WEPopover
//
//  Created by Werner Altewischer on 06/11/10.
//  Copyright 2010 Werner IT Consultancy. All rights reserved.
//


#define TABLE_ROW_HEIGHT_IPHONE		55  //55


#import "WEPopoverContentViewController.h"
#import "TODODataObject.h"


@implementation WEPopoverContentViewController


#pragma mark -
#pragma mark Initialization


- (id)initWithStyle:(UITableViewStyle)style ToDoObject:(TODODataObject*) todoObj{
    NSLog(@"WEPopoverContentViewController initWithStyle");
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
		self.contentSizeForViewInPopover = CGSizeMake(250, 1 * 135 - 1); //CGSizeMake(100, 1 * 44 - 1);
                                                      //250  //100
        todoDataObj = todoObj;
        //NSLog(@"todoDataObj.todoTitle = %@", todoDataObj.todoTitle);
        
        /*
        if ([arrowDirection isEqualToString:@"D"])
        {
            tblInputTypeSelection.layer.shadowColor = [[UIColor grayColor] CGColor];
            tblInputTypeSelection.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            tblInputTypeSelection.layer.shadowOpacity = 0.5f;
            tblInputTypeSelection.layer.shadowRadius = 5.0f;
            tblInputTypeSelection.layer.masksToBounds = NO;
            tblInputTypeSelection.clipsToBounds = NO;
        }
        else {
            tblInputTypeSelection.layer.shadowColor = [[UIColor grayColor] CGColor];
            tblInputTypeSelection.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
            tblInputTypeSelection.layer.shadowOpacity = 0.5f;
            tblInputTypeSelection.layer.shadowRadius = 5.0f;
            tblInputTypeSelection.layer.masksToBounds = NO;
            tblInputTypeSelection.clipsToBounds = NO;
        }
         */
       
        
    }
    return self;
}


#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
     NSLog(@"WEPopoverContentViewController viewDidLoad");

	self.tableView.rowHeight = 140.0; //44
	//self.view.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]; //Steve
    self.tableView.layer.cornerRadius = 5.0; //Steve
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     NSLog(@"WEPopoverContentViewController cellForRowAtIndexPath");
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.highlighted = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if ([todoDataObj.todoTitleType isEqualToString:@"H"])
    {
        //Clears the cell text
        cell.textLabel.text = nil;
        
        UIImageView *titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.contentSizeForViewInPopover.width,self.tableView.rowHeight)];
        [titleImageView setBackgroundColor:[UIColor clearColor]];
        
        UIImage *titleImage = [[UIImage alloc] initWithData:[todoDataObj todoTitleBinaryLarge]];
        [titleImageView setImage:titleImage];
        
        //toDoProgress
        if (todoDataObj.toDoProgress == 100)
        {
            titleImageView.alpha=0.50;
        }
        
        [cell.contentView addSubview:titleImageView];
        
    }
    
    else
    {
        //Clears the cell text
        cell.textLabel.text = nil;
        
        UITextView *titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0,0,self.contentSizeForViewInPopover.width,self.tableView.rowHeight)];
        [titleTextView setBackgroundColor:[UIColor clearColor]];
        titleTextView.font =  [UIFont fontWithName:@"Noteworthy-Bold" size:18];
        [titleTextView setTextColor:[UIColor blackColor]];
        [titleTextView setUserInteractionEnabled:NO];
        [titleTextView setScrollEnabled:YES];
        [titleTextView setText:[todoDataObj todoTitle]];
        
        if(todoDataObj.toDoProgress==100)
        {
            titleTextView.textColor = [UIColor grayColor];
        }
        
        [cell.contentView addSubview:titleTextView];
    }
    
    
    
    
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 }
 */
/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */



@end


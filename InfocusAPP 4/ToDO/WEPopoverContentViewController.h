//
//  WEPopoverContentViewController.h
//  WEPopover
//
//  Created by Werner Altewischer on 06/11/10.
//  Copyright 2010 Werner IT Consultancy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TODODataObject.h"



 @interface WEPopoverContentViewController : UITableViewController {
     
     TODODataObject  *todoDataObj; //Steve
 
 }

//Steve
- (id)initWithStyle:(UITableViewStyle)style ToDoObject:(TODODataObject*) todoObj;


@end

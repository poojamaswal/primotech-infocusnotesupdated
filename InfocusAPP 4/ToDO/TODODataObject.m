//
//  TODODataObject.m
//  Organizer
//
//  Created by Nilesh Jaiswal on 4/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TODODataObject.h"


@implementation TODODataObject
@synthesize todoID, toDoProgress, manuallySortedTodoID, isStarred,projPrntID,projLevel;
@synthesize todoTitle, startDateTime, dueDateTime, createDateTime, modifyDateTime, todoTitleType, todoTitleBinary,todoTitleBinaryLarge, calID, projID, sortingDateTime, projectName, calendarName ,fullprojPath,alert,alertSecond,isAlertOn,FolderImage; 
@synthesize modifyDate;
@synthesize priority = mpriority;


-(id)initWithToDoId:(NSInteger) toDoIdLocal todoTitle:(NSString*)todoTitleLocal todoTitleType:(NSString *) todoTitleTypeLocal todoTitleBinary:(NSData *) todoTitleBinaryLocal calid:(NSInteger) calendarIDLocal projectID:(NSInteger) projectsIDLocal startDate:(NSString *)startDateLocal dueDate:(NSString*) dueDateLocal carestedDate:(NSString *) carestedDateLocal modifyDate:(NSString*) modifyDateLocal toDoProgress:(NSInteger) toDoProgressLocal piority:(NSString *) priorityLocal sortingColumn:(NSString *) sortingDateTimeLocal isStarred:(BOOL) isStarredLocal manuallySortTodoID:(NSInteger) manuallySortTodoIDLocal projPrntID:(NSInteger)ProjPrntIDLocal projLevel:(NSInteger)projLevelLocal fullprojPath:(NSString *)fullprojPathLocal alert:(NSString *)alertLocal alertSecond:(NSString *)alertSecondLocal isAlertOn:(BOOL)isAlertOnLocal;{
	if (self = [super init]) {
		self.todoID = toDoIdLocal; 
		self.calID = calendarIDLocal; 
		self.projID = projectsIDLocal; 
		self.toDoProgress = toDoProgressLocal;
		self.todoTitle = todoTitleLocal; 
		self.todoTitleType = todoTitleTypeLocal;
		self.todoTitleBinary = todoTitleBinaryLocal;
		self.startDateTime = startDateLocal; 
		self.dueDateTime = dueDateLocal; 
		self.createDateTime = carestedDateLocal; 
		self.modifyDateTime = modifyDateLocal; 
		self.priority = priorityLocal;
		self.sortingDateTime = sortingDateTimeLocal;
		self.manuallySortedTodoID = manuallySortTodoIDLocal;
		self.isStarred = isStarredLocal;
		self.projPrntID=ProjPrntIDLocal;
		self.projLevel=projLevelLocal;
        self.fullprojPath=fullprojPathLocal;
        self.alert = alertLocal;
        self.alertSecond = alertSecondLocal;
        self.isAlertOn = isAlertOnLocal;
        //self.FolderImage = FolderImage;
		
	}
	return self;
}


@end

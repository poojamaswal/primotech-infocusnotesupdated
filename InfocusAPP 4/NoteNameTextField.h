//
//  NoteNameTextField.h
//  testKeboard
//
//  Created by Gaurav Goyal on 05/06/15.
//  Copyright (c) 2015 CanvasM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteNameTextFieldDelegate <NSObject>
- (void)noteNameDidBeginEditing:(UITextField*)textField;
- (void)noteNameDidEndEditing:(UITextField*)textField;

@end

@interface NoteNameTextField : UILabel

@property (nonatomic, weak) IBOutlet UIView *inputView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *inputViewTopConstraint;
@property (nonatomic, weak) IBOutlet UITextField *associatedTextField;

@property (nonatomic, assign) id<NoteNameTextFieldDelegate> delegate;

@end



#import "ACEViewController.h"
#import "ACEDrawingView.h"
#import "SelectColorPalletVC.h"
#import "NoteNameTextField.h"
#import <QuartzCore/QuartzCore.h>
#import "ToolSelectionObject.h"
#import "SPUserResizableView.h"
#import "ImagePickerViewController.h"
#import "DrawingDefaults.h"
#import "PanView.h"
#import "NotesID.h"
#import "DatabaseManager.h"
#import "DrawingData.h"
#import "DrawingPageListView.h"
#import "TextData.h"
#import "UIImage+Drawing.h"
#import "ImageData.h"
#import "StickyData.h"
#import "OrganizerAppDelegate.h"

int const TagActionSheetImage   = 100;
int const TagActionSheetTool    = 101;
int const TagUndoButton         = 102;
int const TagTextButton         = 103;
int const TagPenButton          = 104;
int const TagBrushButton        = 105;
int const TagEraserButton       = 106;
int const TagPhotoButton        = 107;
int const TagStickyButton       = 108;
int const TagPageListButton     = 109;


int const ImageYValue  = 5;
#define IS_CAMERA_AVAILABLE [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
#define IS_IPAD ([[UIScreen mainScreen]bounds].size.height == 1024)


#define ERASER_SIZE_1 5.0
#define ERASER_SIZE_2 10.0
#define ERASER_SIZE_3 15.0
#define ERASER_SIZE_4 20.0
#define ERASER_SIZE_5 25.0
#define ERASER_SIZE_6 30.0





@interface ACEViewController ()<UIActionSheetDelegate, ACEDrawingViewDelegate,ColorPalletDelegate,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,DrawingPageListViewDelegate>
{
    
    __weak IBOutlet RichTextEditor *_textEditor;
    UIButton *_previousSelectedButton;
    
    __weak IBOutlet UIButton *_btnShape;
    __weak IBOutlet UIButton *_btnPen;
    __weak IBOutlet UIButton *btnPanView;
    
    
    __weak IBOutlet UIButton *_btnCamera;
    SPUserResizableView *_userResizableView;
    
    int intResizableViewCounterImage;
    int intResizableViewCounterSticky;
    
    DrawingPageListView * _drawingListView;
    NSString *strValueSelectedFromDatabase;
    int intIndexSelectedFromDatabase;
    int intTotalCountFromDatabase;
    BOOL _boolNewRecord;
    BOOL zoomView;
    
    __weak IBOutlet UIButton *btnDissableView;
    BOOL _boolNewNotepage;
    
    NSMutableArray*panImagesArr;
    

}



//Note Name
@property (weak, nonatomic) IBOutlet UITextField *txtFldNoteName;
@property (weak, nonatomic) IBOutlet UIView *vwTxtNoteName;
@property (nonatomic, weak) IBOutlet UIButton *noteName;
@property (nonatomic, weak) IBOutlet UILabel *noteDateLabel;
@property (nonatomic, strong) UIPopoverController *settingsPopoverController;

//Eraser
@property (weak, nonatomic) IBOutlet UIView *vwEraserSizes;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sizeSegmentController;



//PAN Draw
@property (nonatomic, strong) CAShapeLayer *selectionShapeLayer;
@property (nonatomic, assign) CGRect currentFrame;



//Marchin Ants
@property (nonatomic) CGPoint startPoint;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;


@end

@implementation ACEViewController

@synthesize prntVAL,isFetchDataOnly;
@synthesize  tempTitleArray,tempWebImgTArray,tempBookMarksArray,tempHWRTArray,tempMapArray,tempImgArray,temptxtArray,tempSktArray;
@synthesize notesTitleBinary,notesTitleBinaryLarge,strValueSelectedFromDatabase;


- (void)startNewDrawingFromInsideAfterDelete:(BOOL)value
{
    [self setNoteDependentValues];
    
    [self initialViewSettings];
    
    for (int i=0; i<self.drawingView.subviews.count; i++)
    {
        SPUserResizableView *view = [self.drawingView.subviews objectAtIndex:i];
        [view removeFromSuperview];
    }
    
    //    [[DrawingDefaults sharedObject].dictResizableSticky removeAllObjects];
    //    [[DrawingDefaults sharedObject].dictResizableData removeAllObjects];
    
    [self.drawingView clear];
    
    [self updateButtonStatus];
    
    [DrawingDefaults sharedObject].intNotesSubNotesCounter = [DrawingDefaults sharedObject].intNotesSubNotesCounter + 1;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if (IS_IPAD)
    {
        OrganizerAppDelegate  *myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
        myAppdelegate.tabBarController.tabBar.hidden=YES;
    }
    self.navigationController.navigationBarHidden=YES;
    //self.zoomScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    //  self.zoomScrollView.minimumZoomScale=0.5;
    self.zoomScrollView.maximumZoomScale=6.0;
    self.zoomScrollView.scrollEnabled=NO;
    self.zoomScrollView.userInteractionEnabled=YES;
    self.zoomScrollView.canCancelContentTouches=YES;
    self.zoomScrollView.delaysContentTouches=NO;
    
    
    
    firstTime=YES;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.zoomScrollView addGestureRecognizer:singleTap];
    
    panImagesArr=[[NSMutableArray alloc]init];
    
    

    pickerimage=[[UIImagePickerController alloc]init];
    pickerimage.delegate=self;
    pickerimage.allowsEditing=YES;
    
    
    [DrawingDefaults sharedObject].dictResizableData = [NSMutableDictionary dictionary];
    [DrawingDefaults sharedObject].dictResizableSticky = [NSMutableDictionary dictionary];
    
    // set the delegate
    self.drawingView.delegate = self;

    self.drawingView.isResizableImage = NO;
    
    
    
    
    // Tool selection
    [ToolSelectionObject sharedObject].delegate = (id)self;
    
    [_noteName setTitle:strValueSelectedFromDatabase forState:UIControlStateNormal];
    
    [self setNoteDependentValues];
    
    if([DrawingDefaults sharedObject].intNotesSubNotesCount>0)
        [DrawingDefaults sharedObject].intNotesSubNotesCounter = [DrawingDefaults sharedObject].intNotesSubNotesCount;
    else
        [DrawingDefaults sharedObject].intNotesSubNotesCounter =1;
    
    [self loadDataFromDatabaseWithIndex:-1];
    
    
    /*
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),  ^{
     //this block runs on a background thread; Do heavy operation here
     NSLog(@"dispatch_get_global_queue");
     
     dispatch_async(dispatch_get_main_queue(), ^{
     //This block runs on main thread, so update UI
     NSLog(@"dispatch_get_main_queue");
     });
     });
     */
    
    
    tempSktArray	   =	[[NSMutableArray alloc]init];
    tempImgArray       =	[[NSMutableArray alloc]init];
    tempMapArray       =	[[NSMutableArray alloc]init];
    
    temptxtArray       =	[[NSMutableArray alloc]init];
    tempHWRTArray      =	[[NSMutableArray alloc]init];
    tempTitleArray     =	[[NSMutableArray alloc]init];
    tempWebImgTArray   =	[[NSMutableArray alloc]init];
    tempBookMarksArray =	[[NSMutableArray alloc]init];
    
    //Set Default Eraser Size
    [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_1 forKey:@"EraserSizeLastSelected-Zoom"];
     [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_1 forKey:@"EraserSizeLastSelected"];
  
    
}

- (void)setNoteDependentValues
{
    intResizableViewCounterImage=1;
    intResizableViewCounterSticky=1;
    // start with a black pen
    self.lineWidthSlider.value = self.drawingView.lineWidth;
    
    [self setPenDefaultsColorValues];
    //Initial View settings
    [self initialViewSettings];
    
    _textEditor.text = [NSString stringWithFormat:@"\n\n"];
    [DrawingDefaults sharedObject].strNoteName = strValueSelectedFromDatabase;
    
    _boolNewNotepage = FALSE;
    
    
    [DrawingDefaults sharedObject].intNotesSubNotesCount = [[DatabaseManager sharedManager] getCountForSubNotesForNoteID:_noteName.titleLabel.text];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSLog(@"View appear");
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        
        if (textAction)
        {
            [_textEditor becomeFirstResponder];
        }
    }
    
//    if(_userResizableView)
//        [self userResizableViewDidEndEditing:_userResizableView];
    
    for(int i=0;i<self.drawingView.subviews.count;i++)
    {
        SPUserResizableView *view = [self.drawingView.subviews objectAtIndex:i];
        if([view isKindOfClass:[SPUserResizableView class]])
        {
              [self userResizableViewDidEndEditing:_userResizableView];
        }
        
        
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [self removeAllImageView];
    OrganizerAppDelegate  *myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
    myAppdelegate.tabBarController.tabBar.hidden=NO;
    [self setCurrentFrame:CGRectNull];
    
    NSLog(@"View Dis-appear");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark Mouse Events
 
 - (void)mouseDown:(NSEvent *)theEvent
 {
 self.startPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
 
 // create and configure shape layer
 
 self.shapeLayer = [CAShapeLayer layer];
 self.shapeLayer.lineWidth = 1.0;
 self.shapeLayer.strokeColor = [[NSColor blackColor] CGColor];
 self.shapeLayer.fillColor = [[NSColor clearColor] CGColor];
 self.shapeLayer.lineDashPattern = @[@10, @5];
 [self.layer addSublayer:self.shapeLayer];
 
 // create animation for the layer
 
 CABasicAnimation *dashAnimation;
 dashAnimation = [CABasicAnimation animationWithKeyPath:@"lineDashPhase"];
 [dashAnimation setFromValue:@0.0f];
 [dashAnimation setToValue:@15.0f];
 [dashAnimation setDuration:0.75f];
 [dashAnimation setRepeatCount:HUGE_VALF];
 [self.shapeLayer addAnimation:dashAnimation forKey:@"linePhase"];
 }
 
 - (void)mouseDragged:()theEvent
 {
 CGPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
 
 // create path for the shape layer
 
 CGMutablePathRef path = CGPathCreateMutable();
 CGPathMoveToPoint(path, NULL, self.startPoint.x, self.startPoint.y);
 CGPathAddLineToPoint(path, NULL, self.startPoint.x, point.y);
 CGPathAddLineToPoint(path, NULL, point.x, point.y);
 CGPathAddLineToPoint(path, NULL, point.x, self.startPoint.y);
 CGPathCloseSubpath(path);
 
 // set the shape layer's path
 
 self.shapeLayer.path = path;
 
 CGPathRelease(path);
 }
 
 - (void)mouseUp:(NSEvent *)theEvent
 {
 [self.shapeLayer removeFromSuperlayer];
 self.shapeLayer = nil;
 }
 */


#pragma mark TextEditing Settings
-(void)disableTextViewEditing{
    
    //[self.view sendSubviewToBack:_textEditor];
    [_textEditor setUserInteractionEnabled:NO];
    [_textEditor resignFirstResponder];
}

#pragma mark Initial View Settings

- (void)loadDataFromDatabaseWithIndex :(int)index
{
    NotesID *nm = [DatabaseManager pageForName:_noteName.titleLabel.text];
     checkProject_ID=[nm.projectID integerValue];
    
    // DrawingData
    NSSet *aDrawingDataSet = nm.drawingData;
    NSArray *arr_DrawingData = [NSArray arrayWithArray:[aDrawingDataSet allObjects]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    arr_DrawingData = [arr_DrawingData sortedArrayUsingDescriptors:@[sortDescriptor]];
    DrawingData *dataDrawing;
    if(arr_DrawingData.count>0)
    {
        if(index<0)
            dataDrawing = [arr_DrawingData lastObject];
        else if(index<arr_DrawingData.count)
            dataDrawing = [arr_DrawingData objectAtIndex:index];
        
        if(dataDrawing)
        {
            UIImage *image_         =    [UIImage imageWithView:self.drawingView fromRect:self.drawingView.frame];//[UIImage imageWithData:dataDrawing.drawingImageData scale:1.0];
            CGSize size = image_.size;
            
            image_ = [UIImage imageWithImage:[UIImage imageWithData:dataDrawing.drawingImageData] scaledToSize:size];
            
            //        CALayer *newLayer = [CALayer layer];
            //        newLayer.frame = self.drawingView.bounds;
            //        newLayer.contents = (__bridge id)(image_.CGImage);
            //        [self.drawingView.layer addSublayer:newLayer];
            [self.drawingView loadImage: image_];
        }
    }
    
    
    // textViewData
    NSSet *aTestDataSet = nm.textData;
    NSArray *arr_TextData = [NSArray arrayWithArray:[aTestDataSet allObjects]];
    sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    arr_TextData = [arr_TextData sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    if(arr_TextData.count>0)
    {
        TextData *textData;
        
        if(index<0)
            textData = [arr_TextData lastObject];
        else if(index<arr_DrawingData.count)
            textData = [arr_TextData objectAtIndex:index];
        
        
        if(textData)
        {
            _textEditor.attributedText = textData.textData;
            
            // setUserInteraction no and bring to front
            [self.view bringSubviewToFront:_textEditor];
            [_textEditor setUserInteractionEnabled:NO];
            
        }
    }
    
    
    NSSet *aImageDataSet = nm.imageData;
    NSArray *arr_DownloadedData = [NSArray arrayWithArray:[aImageDataSet allObjects]];
    sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    arr_DownloadedData = [arr_DownloadedData sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    if(arr_DownloadedData.count>0)
        dataDrawing = [[[DatabaseManager sharedManager] arrayForDrawingDataForPresentNoteID] objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
    
    NSLog(@"%@",((ImageData*)[arr_DownloadedData firstObject]).savedDate);
    NSLog(@"%@",dataDrawing.savedDate);
    NSPredicate *predicate_ = [NSPredicate predicateWithFormat:@"savedDate=%@",dataDrawing.savedDate];
    if(predicate_)
        arr_DownloadedData =[arr_DownloadedData filteredArrayUsingPredicate:predicate_];
    
    
    if(arr_DownloadedData.count>0)
    {
        for(int i=0;i<arr_DownloadedData.count;i++)
        {
            CGRect frame        = CGRectFromString(((ImageData*)[arr_DownloadedData objectAtIndex:i]).imageCoordinates);
            NSData *data        = ((ImageData*)[arr_DownloadedData objectAtIndex:i]).imageData;
            UIImage *image      = [UIImage imageWithData:data];
            [self drawImageViewWithImage:image withFrame:frame];
        }
        
    }
    
    
    
    
    
    NSSet *aStickyDataSet = nm.stickyData;
    NSArray *arr_StickyData = [NSArray arrayWithArray:[aStickyDataSet allObjects]];
    sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"savedDate" ascending:YES];
    arr_StickyData = [arr_StickyData sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    if(arr_StickyData.count>0)
        dataDrawing = [[[DatabaseManager sharedManager] arrayForDrawingDataForPresentNoteID] objectAtIndex:[DrawingDefaults sharedObject].intNotesSubNotesCounter-1];
    
    NSLog(@"%@",((StickyData*)[arr_StickyData firstObject]).savedDate);
    NSLog(@"%@",dataDrawing.savedDate);
    predicate_=nil;
    predicate_ = [NSPredicate predicateWithFormat:@"savedDate=%@",dataDrawing.savedDate ];
    if(predicate_)
        arr_StickyData =[arr_StickyData filteredArrayUsingPredicate:predicate_];
    
    if(arr_StickyData.count>0)
    {
        for(int i=0;i<arr_StickyData.count;i++)
        {
            CGRect frame        = CGRectFromString(((StickyData*)[arr_StickyData objectAtIndex:i]).stickyCoordinates);
            NSString *str        = ((StickyData*)[arr_StickyData objectAtIndex:i]).stickyText;
            [self drawStickyWithText:str andFrame:frame];
        }
        
    }
    
    
    NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData];
    SPUserResizableView *tempView = nil;
    
    for(int i=0;i<arrayCurrentValues.count;i++)
    {
        tempView = [arrayCurrentValues objectAtIndex:i];
        [tempView hideEditingHandles];
    }
    
    arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData];
    for(int i=0;i<arrayCurrentValues.count;i++)
    {
        tempView = [arrayCurrentValues objectAtIndex:i];
        [tempView hideEditingHandles];
    }
    
}

-(void)initialViewSettings{
    
    //
    [self setSelectedButton:_btnPen];
    
    //Set Current Date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    dateFormatter.dateFormat = @"MMM dd, yyyy, hh:mm a";
    
    self.noteDateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    _txtFldNoteName.delegate=self;
    
}
- (void)createNotesListView
{
    if(!_drawingListView)
    {
        [self showDrawingListView];
    }
    else
    {
        [self hideDrawingListView];
    }
}
/*
 - (void)hideUnhideButtonDissableView :(BOOL)value
 {
 if(value)
 {
 [self.view bringSubviewToFront:btnDissableView];
 btnDissableView.hidden=NO;
 btnDissableView.alpha = 0.0;
 [UIView animateWithDuration:0.3 animations:^{
 btnDissableView.alpha = 0.5;
 }];
 }
 else
 {
 [UIView animateWithDuration:0.3 animations:^{
 btnDissableView.alpha=0.0;
 } completion:^(BOOL finished) {
 btnDissableView.hidden=YES;
 [self.view sendSubviewToBack:btnDissableView];
 }];
 }
 }
 */

#pragma mark - Setter / Getter
-(void)setRowSelected : (NSString*)valueSelected atSelectedIndex: (int)indexSelected withTotalCount:(int)totalCount
{
    _boolNewRecord = FALSE;
    strValueSelectedFromDatabase = valueSelected;
    intIndexSelectedFromDatabase = indexSelected;
    intTotalCountFromDatabase = indexSelected;
    
}

- (void)newRecordOrExistingRecord:(BOOL)boolValue withTotalCount:(int)totalCount
{
    _boolNewRecord = boolValue;
    if(_boolNewRecord)
    {
        
        strValueSelectedFromDatabase = [NSString stringWithFormat:@"Note %d",totalCount];
    }
}

#pragma mark -- Call Back Delegate ColorPalletSelection
-(void)setColorPalletValues:(BOOL)isHighlighter
{
    
    if (zoomView)
    {
        
        self.panView.drawingView.lineWidth =[[[DrawingDefaults sharedObject].dictCollorPalletValues valueForKey:@"Size"] floatValue];
        self.panView.drawingView.lineAlpha =[[[DrawingDefaults sharedObject].dictCollorPalletValues valueForKey:@"Alpha"] floatValue]/100;
        
        CGFloat red = (CGFloat)[[[DrawingDefaults sharedObject].dictCollorPalletValues valueForKey:@"R"] floatValue];
        CGFloat green = (CGFloat)[[[DrawingDefaults sharedObject].dictCollorPalletValues valueForKey:@"G"] floatValue];
        CGFloat blue = (CGFloat)[[[DrawingDefaults sharedObject].dictCollorPalletValues valueForKey:@"B"] floatValue];
        
        //Set selected Color to Pen
        
        self.panView.drawingView.lineColor=[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:self.panView.drawingView.lineAlpha];
        
    }
    
    else
    {
        
        if(isHighlighter){
            
            [self setBrushDefaultColorValues];
            
        }
        else{
            [self setPenDefaultsColorValues];
            
        }
    }
    
}

-(void)setPenDefaultsColorValues{
    NSLog(@"Dict Color Pallet: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]);
    
    // init the preview image
    NSDictionary* dictDetails = [[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"];
    
    if([dictDetails count]==0)
        
    {
        self.previewImageView.layer.borderColor = [[UIColor blackColor] CGColor];
        //    self.previewImageView.layer.borderWidth = 2.0f;
         self.drawingView.lineColor =[UIColor blackColor];
        
    }
    
    else{
        
        self.drawingView.lineWidth = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"Size"] floatValue];
        self.drawingView.lineAlpha =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"Alpha"] floatValue]/100;
        
        
        CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"R"] floatValue];
        CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"G"] floatValue];
        CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"B"] floatValue];
        
        
        
        //Set selected Color to Pen
        self.drawingView.lineColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:self.drawingView.lineAlpha];
    }
    
}

-(void)setBrushDefaultColorValues{
    
    NSLog(@"Dict Color Pallet: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]);
    
    
    // init the preview image
    NSDictionary* dictDetails = [[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"];
    
    if([dictDetails count]==0)
        
    {
        
        self.previewImageView.layer.borderColor = [[UIColor blackColor] CGColor];
        //    self.previewImageView.layer.borderWidth = 2.0f;
        self.drawingView.lineAlpha =0.5;
         self.drawingView.lineColor =[UIColor yellowColor];
    }
    
    else{
        
        
        self.drawingView.lineWidth = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"Size"] floatValue];
        self.drawingView.lineAlpha =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"Alpha"] floatValue]/100;
        
        
        CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"R"] floatValue];
        CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"G"] floatValue];
        CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"B"] floatValue];
        
        
        
        //Set selected Color to Brush
        self.drawingView.lineColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:self.drawingView.lineAlpha];
    }
    
}

#pragma mark - Database Methods
//[self setCurrentFrame:CGRectNull];
- (void)saveToDatabase
{
    if(1)
    {
        
        NSLog(@"%@",[DrawingDefaults sharedObject].dictResizableData);
        
        NSMutableDictionary *dictImagesData = [[NSMutableDictionary alloc]init];
        NSMutableArray *arrImage = [[NSMutableArray alloc]init];
        NSMutableArray *arrFrames = [[NSMutableArray alloc]init];
        NSMutableArray *arrID = [[NSMutableArray alloc]init];
        
        for(int i=0;i<((NSArray*)[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData]).count; i++ )
        {
            UIImage *image_ = [UIImage imageWithView:((SPUserResizableView*)[[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData] objectAtIndex:i]).contentViewCustom fromPoint:CGPointZero];
            [arrImage addObject:UIImagePNGRepresentation(image_)];
            [arrFrames addObject:NSStringFromCGRect(((SPUserResizableView*)[[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData] objectAtIndex:i]).frame)];
            
            [arrID addObject:[[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyID] objectAtIndex:i]];
        }
        
        if(arrFrames.count>0)
        {
            [dictImagesData setObject:arrFrames forKey:KeyFrames];
            [dictImagesData setObject:arrImage forKey:KeyData];
            [dictImagesData setObject:arrID forKey:KeyID];
        }
        
        
        // -------- sticky
        NSMutableDictionary *dictStickyData = [[NSMutableDictionary alloc]init];
        NSMutableArray *arrText = [[NSMutableArray alloc]init];
        NSMutableArray *arrFramesSticky = [[NSMutableArray alloc]init];
        [arrID removeAllObjects];
        
        for(int i=0;i<((NSArray*)[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData]).count; i++ )
        {
            [arrText addObject:((UITextField*)((SPUserResizableView*)[[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData] objectAtIndex:i]).contentViewCustom).text];
            [arrFramesSticky addObject:NSStringFromCGRect(((SPUserResizableView*)[[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData] objectAtIndex:i]).frame)];
            [arrID addObject:[[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyID] objectAtIndex:i]];
        }
        
        if(arrFramesSticky.count>0)
        {
            [dictStickyData setObject:arrFramesSticky forKey:KeyFrames];
            [dictStickyData setObject:arrText forKey:KeyData];
            [dictStickyData setObject:arrID forKey:KeyID];
        }
        
        
        for(int i=0;i<self.drawingView.subviews.count;i++)
        {
            SPUserResizableView *view = [self.drawingView.subviews objectAtIndex:i];
            if([view isKindOfClass:[SPUserResizableView class]])
            {
                [view setHidden:YES];
            }
        }
        
        UIImage *drawingImage = [UIImage imageWithView:self.drawingView fromPoint:CGPointZero];
        
        NSData *imageData = UIImagePNGRepresentation(drawingImage); //UIImagePNGRepresentation(drawingImage); // 0.7 is JPG quality
        
        for(int i=0;i<self.drawingView.subviews.count;i++)
        {
            SPUserResizableView *view = [self.drawingView.subviews objectAtIndex:i];
            if([view isKindOfClass:[SPUserResizableView class]])
            {
                [view setHidden:NO];
            }
        }
        
        UIImage *drawingImageIcon = [UIImage imageWithView:self.drawingView fromPoint:CGPointZero];
        
        drawingImageIcon = [UIImage imageWithImage:drawingImageIcon scaledToSize:CGSizeMake(300, 300)];
        
        NSData *imageDataIcon = UIImagePNGRepresentation(drawingImageIcon); //
        
        NSDictionary *dictDrawingData = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:imageData,imageDataIcon,nil] forKeys:[NSArray arrayWithObjects:KeyData,KeyIconData,nil]];
        
        NSDictionary *dictTextData = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:_textEditor.attributedText,nil] forKeys:[NSArray arrayWithObjects:KeyData,nil]];
        
        
        
        if(!_boolNewRecord && ![NotesID userAlReadyExist:_noteName.titleLabel.text])
        {
            NotesID *notesID = [NotesID changeValue:_noteName.titleLabel.text from:strValueSelectedFromDatabase];
            
            [[DatabaseManager sharedManager] deleteImagesWithPageID:_noteName.titleLabel.text];
            [[DatabaseManager sharedManager] deleteStickiesWithPageID:_noteName.titleLabel.text];
            
            [DrawingDefaults sharedObject].strNoteName = _noteName.titleLabel.text;
            
            [notesID saveDrawingInfo:dictDrawingData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            [notesID saveTextInfo:dictTextData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            for(int i=0;i<((NSArray*)[dictImagesData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictImagesData valueForKey:KeyFrames] objectAtIndex:i],[[dictImagesData valueForKey:KeyData] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData, nil]];
                
                [notesID saveImageInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
            
            
            for(int i=0;i<((NSArray*)[dictStickyData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictStickyData valueForKey:KeyFrames] objectAtIndex:i],[[dictStickyData valueForKey:KeyData] objectAtIndex:i],[[dictStickyData valueForKey:KeyID] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData,KeyID, nil]];
                
                [notesID saveStickyInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
            
            
        }
        else if (![NotesID userAlReadyExist:_noteName.titleLabel.text])
        {
            // NotesID *newUserID = [NotesID createPage:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            NotesID *newUserID;
            if (_isComingFromProjectModule)
            {
                newUserID = [NotesID createPage:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text withProjectID:prntVAL];
            }else
            {
                newUserID = [NotesID createPage:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text withProjectID:0];
            }
            
           
            
            [[DatabaseManager sharedManager] deleteImagesWithPageID:_noteName.titleLabel.text];
            [[DatabaseManager sharedManager] deleteStickiesWithPageID:_noteName.titleLabel.text];
            
            [newUserID saveDrawingInfo:dictDrawingData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            [newUserID saveTextInfo:dictTextData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            // ----- Saving Images
            
            for(int i=0;i<((NSArray*)[dictImagesData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictImagesData valueForKey:KeyFrames] objectAtIndex:i],[[dictImagesData valueForKey:KeyData] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData, nil]];
                
                [newUserID saveImageInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
            
            // ----- Saving Stickies
            
            
            for(int i=0;i<((NSArray*)[dictStickyData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictStickyData valueForKey:KeyFrames] objectAtIndex:i],[[dictStickyData valueForKey:KeyData] objectAtIndex:i],[[dictStickyData valueForKey:KeyID] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData,KeyID, nil]];
                
                [newUserID saveStickyInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
            
        }
        else
        {
            NotesID *_noteID = [DatabaseManager pageForName:_noteName.titleLabel.text];
            
            [[DatabaseManager sharedManager] deleteImagesWithPageID:_noteName.titleLabel.text];
            [[DatabaseManager sharedManager] deleteStickiesWithPageID:_noteName.titleLabel.text];
            
            [_noteID saveDrawingInfo:dictDrawingData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            [_noteID saveTextInfo:dictTextData withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            
            
            for(int i=0;i<((NSArray*)[dictImagesData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictImagesData valueForKey:KeyFrames] objectAtIndex:i],[[dictImagesData valueForKey:KeyData] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData, nil]];
                
                [_noteID saveImageInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
            
            
            
            for(int i=0;i<((NSArray*)[dictStickyData valueForKey:KeyFrames]).count;i++)
            {
                NSDictionary *dict = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[dictStickyData valueForKey:KeyFrames] objectAtIndex:i],[[dictStickyData valueForKey:KeyData] objectAtIndex:i],[[dictStickyData valueForKey:KeyID] objectAtIndex:i], nil] forKeys:[NSArray arrayWithObjects:KeyFrames,KeyData,KeyID, nil]];
                
                [_noteID saveStickyInfo:dict withContext:[DatabaseManager sharedManager].managedObjectContext withName:_noteName.titleLabel.text];
            }
        }
    }
    else
    {
        //        NotesID *nm = [self pageForName:_txtFldNoteName.text];
        //        NSSet *docSet = nm.drawingData;
        //        NSArray *arar_DownloadedData = [NSArray arrayWithArray:[docSet allObjects]];
    }
    
    
    
    
}

- (void)fetchPage :(NSString*)strValue
{
    //    NotesID *nm = [self pageForName:_txtFldNoteName.text];
    //    NSSet *docSet = nm.drawingData;
    //    NSArray *arar_DownloadedData = [NSArray arrayWithArray:[docSet allObjects]];
    //
    //    DrawingData *data = [arar_DownloadedData objectAtIndex:0];
    //    UIImage *image_=    [UIImage imageWithData:data.drawingImageData];
    //
    //    for(int i=0;i<arar_DownloadedData.count;i++)
    //    {
    //        NSLog(@"Experience :: %@",((ProfessionalInfo*)[arar_DownloadedData objectAtIndex:i]).experience);
    //        NSLog(@"Field :: %@",((ProfessionalInfo*)[arar_DownloadedData objectAtIndex:i]).field);
    //
    //    }
    
}



- (NSEntityDescription *)getNotesIDEntityDescriptor
{
    return [NSEntityDescription entityForName:@"NotesID" inManagedObjectContext:[DatabaseManager sharedManager].managedObjectContext];
}

- (UIImage *)captureView :(UIView*)view
{
    UIGraphicsBeginImageContextWithOptions(view.layer.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [view drawViewHierarchyInRect:self.drawingView.bounds afterScreenUpdates:YES];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
    
    //[UIImage re];
}
- (UIImage *)pb_takeSnapshot
{
    UIGraphicsBeginImageContextWithOptions(self.drawingView.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self.drawingView drawViewHierarchyInRect:self.drawingView.bounds afterScreenUpdates:YES];
    
    // old style [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
#pragma mark - ToolBar Button Selection State
- (IBAction)backButtonAction:(id)sender
{
    
    [self setCurrentFrame:CGRectNull];//For PAN Green Box
    [self saveToDatabase];
    
    if (_isComingFromProjectModule && !isFetchDataOnly)
    {
        
        [self saveNotewithProject];
    }else if(_isComingFromProjectModule && isFetchDataOnly)
    {
        
        if ([strValueSelectedFromDatabase isEqualToString:_noteName.titleLabel.text])
        {
            NSLog(@"No need to update");
        }
        else
        {
            [self updateNotesTittleOnly];
        }
        
    }
    
    else if (!_isComingFromProjectModule)
    {
        
        
        if (checkProject_ID !=0)
        {
            
            if ([strValueSelectedFromDatabase isEqualToString:_noteName.titleLabel.text])
            {
                NSLog(@"No need to update");
            }
            else
            {
                [self updateNotesTittleOnly];
            }
            
        }
        
    }
    
    if (IS_IPAD)
    {
        if (_isComingFromProjectModule  || isFetchDataOnly)
            
        {
           
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
    else
    {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

#pragma mark - ToolBar Button Selection State
- (void)setSelectedButton:(UIButton*)sender {
    
    _vwTxtNoteName.hidden=TRUE;
    if (_previousSelectedButton) {
        _previousSelectedButton.selected = NO;
    }
    
    sender.selected = YES;
    _previousSelectedButton = sender;
    
    if(sender.tag ==TagEraserButton)
        _vwEraserSizes.hidden=FALSE;
    else
        _vwEraserSizes.hidden=TRUE;
    
    if(_drawingListView)
        [self hideDrawingListView];
    
    if(sender.tag!=TagTextButton)
        [self disableTextViewEditing];
    
}


- (IBAction)penButtonAction:(id)sender
{
    
    
    if (zoomView)
    {
        self.panView.drawingView.drawTool = ACEDrawingToolTypePen;
        
    }else
    {
        self.drawingView.drawTool = ACEDrawingToolTypePen;
        
    }
    
    if([sender isSelected])
    {
        SelectColorPalletVC *aSelectColorPalletVC ;
        
        if(IS_IPAD)
            aSelectColorPalletVC = [[SelectColorPalletVC alloc]initWithNibName:@"SelectColorPalletVC" bundle:nil];
        else
            aSelectColorPalletVC = [[SelectColorPalletVC alloc]initWithNibName:@"SelectColorPAlletVC-iPhn" bundle:nil];
        
        
        aSelectColorPalletVC.delegateColorPallet=self;
        aSelectColorPalletVC.isHighlighter= FALSE;
        self.definesPresentationContext = YES; //self is presenting view controller
        aSelectColorPalletVC.view.backgroundColor = [UIColor clearColor];
        aSelectColorPalletVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:aSelectColorPalletVC animated:YES completion:^{
        }];
    }
    else
    {
        //        //[sender setSelected:NO];
        UIButton* penButton = (UIButton*)sender;
        [self setSelectedButton:penButton];
        
        [self setPenDefaultsColorValues];
    }
    
    
}



- (IBAction)brushAction:(id)sender
{
    
    self.drawingView.drawTool = ACEDrawingToolTypePen;
    
    if([sender isSelected])
    {
        SelectColorPalletVC *aSelectColorPalletVC ;
        
        if(IS_IPAD)
            aSelectColorPalletVC = [[SelectColorPalletVC alloc]initWithNibName:@"SelectColorPalletVC" bundle:nil];
        else
            aSelectColorPalletVC = [[SelectColorPalletVC alloc]initWithNibName:@"SelectColorPAlletVC-iPhn" bundle:nil];
        
        
        aSelectColorPalletVC.delegateColorPallet=self;
        aSelectColorPalletVC.isHighlighter=TRUE;
        self.definesPresentationContext = YES; //self is presenting view controller
        aSelectColorPalletVC.view.backgroundColor = [UIColor clearColor];
        aSelectColorPalletVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:aSelectColorPalletVC animated:YES completion:^{
        }];
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* brushButton = (UIButton*)sender;
        [self setSelectedButton:brushButton];
        
        
        [self setBrushDefaultColorValues];
    }
    
    
    
}

- (IBAction)eraserAction:(id)sender
{
    if (zoomView)
    {
        
        self.panView.drawingView.drawTool = ACEDrawingToolTypeEraser;
        self.panView.drawingView.lineWidth  = [[NSUserDefaults standardUserDefaults]floatForKey:@"EraserSizeLastSelected-Zoom"];
        self.panView.drawingView.drawTool = ACEDrawingToolTypeEraser;
        
    
        //Set selected Color to Pen
        self.panView.drawingView.lineColor =[UIColor whiteColor];

    }
    
    else
    {
        self.drawingView.lineWidth  = [[NSUserDefaults standardUserDefaults]floatForKey:@"EraserSizeLastSelected"];
        self.drawingView.drawTool = ACEDrawingToolTypeEraser;
        
        
        //Set selected Color to Pen
        self.drawingView.lineColor =[UIColor whiteColor];
    }
    
    if([sender isSelected])
    {
        _vwEraserSizes.hidden=FALSE;
        [DrawingDefaults sharedObject].isEraserSelected = YES;
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* eraserButton = (UIButton*)sender;
        [self setSelectedButton:eraserButton];
        [DrawingDefaults sharedObject].isEraserSelected = YES;
        
    }
}


- (IBAction)eraserSizeValueChanged:(UISegmentedControl *)sender{
    
    NSInteger selectedIndex = self.sizeSegmentController.selectedSegmentIndex;
    
    
    if (selectedIndex == 0) {
        
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_1;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_1 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else
        {
            self.drawingView.lineWidth  = ERASER_SIZE_1;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_1 forKey:@"EraserSizeLastSelected"];
        }

    }
    else if (selectedIndex == 1) {
        
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_2;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_2 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else{
        self.drawingView.lineWidth  = ERASER_SIZE_2;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_2 forKey:@"EraserSizeLastSelected"];
        }
    }
    else if (selectedIndex == 2) {
        
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_3;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_3 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else{
        self.drawingView.lineWidth  = ERASER_SIZE_3;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_3 forKey:@"EraserSizeLastSelected"];
        }
    }
    else if (selectedIndex == 3) {
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_4;
           [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_4 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else{
        self.drawingView.lineWidth  = ERASER_SIZE_4;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_4 forKey:@"EraserSizeLastSelected"];
        }
    }
    else if (selectedIndex == 4) {
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_5;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_5 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else{
        self.drawingView.lineWidth = ERASER_SIZE_5;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_5 forKey:@"EraserSizeLastSelected"];
        }
    }
    else if (selectedIndex == 5) {
        if(zoomView){
            self.panView.drawingView.lineWidth  = ERASER_SIZE_6;
            [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_6 forKey:@"EraserSizeLastSelected-Zoom"];
        }
        else{
        self.drawingView.lineWidth  = ERASER_SIZE_6;
        [[NSUserDefaults standardUserDefaults]setFloat:ERASER_SIZE_6 forKey:@"EraserSizeLastSelected"];
        }
    }
    
    _vwEraserSizes.hidden=TRUE;
    
}

- (void)updateButtonStatus
{
    self.undoButton.enabled = [self.drawingView canUndo];
    self.redoButton.enabled = [self.drawingView canRedo];
    
}
- (IBAction)textAction:(id)sender
{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        textAction=true;
    }
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"selectedFont"])
        [_textEditor selectedfont:[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedFont"]];
    
    if([sender isSelected])
    {
        
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* textButton = (UIButton*)sender;
        [self setSelectedButton:textButton];
        
        
        [self.view bringSubviewToFront:_textEditor];
        [_textEditor setUserInteractionEnabled:YES];
        [_textEditor becomeFirstResponder];
    }
    
}

- (IBAction)undeAction:(id)sender
{
    if([sender isSelected])
    {
        [self.drawingView undoLatestStep];
        [self updateButtonStatus];
        
        //        if(((ACEDrawingView*)[[[DrawingDefaults sharedObject].dictResizableData valueForKey:@"data"] objectAtIndex:0]).isResizableImage)
        //        {
        //            [[[[DrawingDefaults sharedObject].dictResizableData valueForKey:@"data"] objectAtIndex:0] undoLatestStep];
        //        }
        //        else
        //        {
        //
        //        }
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* eraserButton = (UIButton*)sender;
        [self setSelectedButton:eraserButton];
        
    }
    
    
    
}


- (IBAction)photoAction:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([sender isSelected])
        {
            UIBarButtonItem *btn =
            [[UIBarButtonItem alloc]initWithCustomView:_btnCamera];
            
            UINavigationController *shapeNavigationController = [[UIStoryboard storyboardWithName:@"Notes" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"PhotoNavigationController"];
            
            self.settingsPopoverController = [[UIPopoverController alloc] initWithContentViewController:shapeNavigationController];
            [self.settingsPopoverController setPopoverContentSize:CGSizeMake(250, 350)];
            [self.settingsPopoverController presentPopoverFromBarButtonItem:btn permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
        else
        {
            //[sender setSelected:NO];
            UIButton* stickyButton = (UIButton*)sender;
            [self setSelectedButton:stickyButton];
        }
        
    }else if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        textAction=false;
        
        
        UIButton* stickyButton = (UIButton*)sender;
        [self setSelectedButton:stickyButton];
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a tool" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil  otherButtonTitles:@"Camera",@"Gallery",nil];
        
        [actionSheet setTag:TagActionSheetImage];
        [actionSheet showInView:self.view];
        
    }
}

- (IBAction)stickyAction:(id)sender
{
    [self disableTextViewEditing];
    [[ToolSelectionObject sharedObject] insertSelectedSticky];
    if([sender isSelected])
    {
        
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* stickyButton = (UIButton*)sender;
        [self setSelectedButton:stickyButton];
        
    }
}
- (IBAction)shapesAction:(id)sender
{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([sender isSelected])
        {
            UIButton* shapeButton = (UIButton*)sender;
            [self setSelectedButton:shapeButton];
            
            UIBarButtonItem *btn = [[UIBarButtonItem alloc]initWithCustomView:_btnShape];
            
            UINavigationController *shapeNavigationController = [[UIStoryboard storyboardWithName:@"Notes" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ShapeNavigationController"];
            
            self.settingsPopoverController = [[UIPopoverController alloc] initWithContentViewController:shapeNavigationController];
            [self.settingsPopoverController setPopoverContentSize:CGSizeMake(250, 275)];
            [self.settingsPopoverController presentPopoverFromBarButtonItem:btn permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
        else
        {
            //[sender setSelected:NO];
            UIButton* stickyButton = (UIButton*)sender;
            [self setSelectedButton:stickyButton];
        }
    }else if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        //[sender setSelected:NO];
        UIButton* stickyButton = (UIButton*)sender;
        [self setSelectedButton:stickyButton];
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a tool" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil  otherButtonTitles:@"Line",@"Rect (Stroke)", @"Rect (Fill)",@"Ellipse (Stroke)", @"Ellipse (Fill)",@"Sticky",nil];
        
        [actionSheet setTag:TagActionSheetTool];
        [actionSheet showInView:self.view];
        
    }
    
    
    
}
#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex != buttonIndex) {
        if (actionSheet.tag == TagActionSheetImage) {
            
            if (buttonIndex==0)
            {
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You don't have a camera for this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [noCameraAlert show];
                }
                else
                {
                    pickerimage.sourceType=UIImagePickerControllerSourceTypeCamera;
                    
                    [self presentViewController:pickerimage animated:YES completion:nil];
                }
            }
            else if (buttonIndex==1)
            {         pickerimage.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                
                [self presentViewController:pickerimage animated:YES completion:nil];
            }
            
        } else if(actionSheet.tag==TagActionSheetTool) {
            
            self.toolButton.title = [actionSheet buttonTitleAtIndex:buttonIndex];
            switch (buttonIndex) {
                case 0:
                    self.drawingView.drawTool = ACEDrawingToolTypeLine;
                    break;
                    
                case 1:
                    self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke;
                    break;
                    
                case 2:
                    self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill;
                    break;
                    
                case 3:
                    self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke;
                    break;
                    
                case 4:
                    self.drawingView.drawTool = ACEDrawingToolTypeEllipseFill;
                    break;
                    
                case 5:
                    [self performSelector:@selector(stickyAction:) withObject:nil afterDelay:0];
                      break;
            }
            
            
        }
    }
}


#pragma mark -- ImagePickerController to pick cover image

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    [self drawImageViewWithImage:info[UIImagePickerControllerOriginalImage] withFrame:CGRectMake(0, 0, 150, 150)];
    
    //    _chosenImage = info[UIImagePickerControllerOriginalImage];
    //    NSLog(@"%@",_chosenImage);
    //    [_coverImgBtn setBackgroundImage:_chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark -- select CoverImage
- (IBAction)selectCoverImageAction:(UIButton *)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}


-(void)setNewPageEnabled:(id)sender
{
    [sender setUserInteractionEnabled:YES];
}
- (IBAction)newPageAction:(id)sender
{
    if(IS_IPAD){
    btnPanView.selected=TRUE;
    [self panAction:btnPanView];
    }
    
    [sender setUserInteractionEnabled:NO];
    
    [self performSelector:@selector(setNewPageEnabled:) withObject:sender afterDelay:1.0];
    
    //    [UIView animateWithDuration:1.0 animations:^{
    //        CGRect tableFrame = self.pagesTableView.frame;
    //        if (tableFrame.origin.x == self.view.frame.size.width) {
    //            tableFrame.origin.x = self.view.frame.size.width - tableFrame.size.width;
    //            [self.pagesTableView reloadData];
    //        }
    //        else {
    //            tableFrame.origin.x = self.view.frame.size.width;
    //        }
    //        self.pagesTableView.frame = tableFrame;
    //    }];
    
    if([sender isSelected])
    {
        [self createNotesListView];
    }
    else
    {
        //[sender setSelected:NO];
        UIButton* newPageButton = (UIButton*)sender;
        [self setSelectedButton:newPageButton];
    }
}

- (void)keyboardWillChange:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil]; //this is it!
    
    CGRect frame=_vwTxtNoteName.frame;
    NSLog(@"%f",self.view.frame.size.height);
    
    NSLog(@"%f",self.view.frame.size.height-(keyboardRect.size.height+50));
    frame.origin.y=self.view.frame.size.height-(keyboardRect.size.height+55);
    _vwTxtNoteName.frame=frame;
    
    
}

//PAN
- (void)setCurrentFrame:(CGRect)currentFrame {
    _currentFrame = currentFrame;
    [self updateSelectionLayerPosition];
}

#pragma mark - Layer Selection methods
- (CAShapeLayer *)selectionShapeLayer {
    if (!_selectionShapeLayer) {
        _selectionShapeLayer = [CAShapeLayer layer];
        _selectionShapeLayer.lineWidth = 2;
        _selectionShapeLayer.strokeColor = [UIColor greenColor].CGColor;
        _selectionShapeLayer.fillColor = [UIColor clearColor].CGColor;
        [self.drawingView.layer addSublayer:_selectionShapeLayer];
        _selectionShapeLayer.hidden = YES;
    }
    
    return _selectionShapeLayer;
}

- (void)updateSelectionLayerPosition {
    if (CGRectIsNull(self.currentFrame)) {
        self.selectionShapeLayer.hidden = YES;
    }
    else {
        
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.currentFrame.origin.x-2, self.currentFrame.origin.y-2, self.currentFrame.size.width+4, self.currentFrame.size.height+4)];
        self.selectionShapeLayer.path = bezierPath.CGPath;
        self.selectionShapeLayer.hidden = NO;
        
        
    }
}

- (void)moveFrameForward {
    CGRect newFrame = self.currentFrame;
    newFrame.origin.x = self.currentFrame.origin.x + self.currentFrame.size.width;
    // newFrame.size = CGSizeMake(self.defaultWidthOfPanDrawing, self.defaultHeightOfPanDrawing);
    if (newFrame.origin.x >= self.panView.frame.size.width) {
        newFrame.origin.x = 0;
        newFrame.origin.y = self.currentFrame.origin.y + newFrame.size.height;
        if (newFrame.origin.y >= self.panView.frame.size.height) {
            newFrame.origin.y = 0;
        }
        else if ((newFrame.origin.y + newFrame.size.height) > self.panView.frame.size.height) {
            newFrame.origin.y = self.panView.frame.size.height - newFrame.size.height;
        }
    }
    else if ((newFrame.origin.x + newFrame.size.width) > self.panView.frame.size.width) {
        newFrame.origin.x = self.panView.frame.size.width - newFrame.size.width;
    }
    self.currentFrame = newFrame;
    
}

- (void)moveFrameBackward {
    CGRect newFrame = self.currentFrame;
    newFrame.origin.x = self.currentFrame.origin.x - self.currentFrame.size.width;
    //  newFrame.size = CGSizeMake(self.defaultWidthOfPanDrawing, self.defaultHeightOfPanDrawing);
    if ((newFrame.origin.x + newFrame.size.width) <= 0) {
        newFrame.origin.x = self.panView.frame.size.width - newFrame.size.width;
        newFrame.origin.y = self.currentFrame.origin.y - newFrame.size.height;
        if ((newFrame.origin.y + newFrame.size.height) <= 0) {
            newFrame.origin.y = self.panView.frame.size.height - newFrame.size.height;
        }
        else if (newFrame.origin.y < 0) {
            newFrame.origin.y = 0;
        }
    }
    else if (newFrame.origin.x < 0) {
        newFrame.origin.x = 0;
    }
    self.currentFrame = newFrame;
}

#pragma mark - Touch events
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.view == self.drawingView)
    {
        CGPoint touchPoint = [touch locationInView:self.drawingView];
        self.currentFrame = CGRectMake(touchPoint.x, touchPoint.y, 150, 60);
        [self setCurrentFrame:self.currentFrame];
        
        
        // remove handles of resizableView
        for (int i=0; i<((NSArray*)[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData]).count; i++)
        {
            SPUserResizableView *view = [[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData] objectAtIndex:i];
            [view hideEditingHandles];
        }
    }
    
}


- (void)customTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _vwEraserSizes.hidden=TRUE;
    
    // remove handles of resizableView
    for(int i=0; i<((NSArray*)[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData]).count; i++)
    {
        SPUserResizableView *view = [[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData] objectAtIndex:i];
        [view hideEditingHandles];
    }
    for(int i=0; i<((NSArray*)[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData]).count; i++)
    {
        SPUserResizableView *view = [[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData] objectAtIndex:i];
        [view hideEditingHandles];
    }
}

#pragma mark PAN Button Action
- (IBAction)panAction:(UIButton *)sender {
    
    if (sender.selected)
    {
        
        
        self.drawingView.userInteractionEnabled=TRUE;
        zoomView=FALSE;
        sender.selected=NO;
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect currentRect = self.panView.frame;
            currentRect.origin.y =  self.view.frame.size.height+15;
            [self.panView setAlpha:1];
            [self.panView setFrame:currentRect];
            
            
            CGRect buttonFrame = sender.frame;
            buttonFrame.origin.y = self.view.frame.size.height-buttonFrame.size.height-10;
            sender.frame = buttonFrame;
            
        }];
        
        [self.panView.drawingView clear];
        [self setCurrentFrame:CGRectNull];
    }
    
    else
    {
        
        zoomView=TRUE;
        sender.selected=YES;
        
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.drawingView.userInteractionEnabled=FALSE;
            
            CGRect currentRect = self.panView.frame;
            
            self.panView.backgroundColor=[UIColor whiteColor];
            self.panView.layer.borderWidth=3.0;
            self.panView.layer.borderColor=[UIColor darkGrayColor].CGColor;
            
            self.panView.drawingView.lineWidth=30.0;
            
            self.panView.drawingView.delegate=self;
            
            currentRect.origin.y =  self.view.frame.size.height-200;
            [self.panView setFrame:currentRect];
            
            CGRect buttonFrame = sender.frame;
            buttonFrame.origin.y = self.panView.frame.origin.y - buttonFrame.size.height;
            sender.frame = buttonFrame;
            
        }];
        
        CGRect viewFrame=CGRectMake((self.drawingView.frame.size.width-150)/2, self.drawingView.frame.size.height/2,150, 60);
        previousFrame=viewFrame;
        [self setCurrentFrame:viewFrame];
        
    }
    
}

#pragma mark Change Note Name
- (IBAction)changeNoteNameBtnAction:(id)sender
{
    
    [self.view bringSubviewToFront:_vwTxtNoteName];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    
    _vwTxtNoteName.hidden=NO;
    [_txtFldNoteName becomeFirstResponder];
    
}

#pragma mark - NoteNameTextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect frame=_vwTxtNoteName.frame;
    frame.origin.y=661.0;
    _vwTxtNoteName.frame=frame;
    _txtFldNoteName.text=_noteName.titleLabel.text;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    NSLog(@"%@",textField.text);
    [_noteName setTitle:textField.text forState:UIControlStateNormal];
    [DrawingDefaults sharedObject].strNoteName = textField.text;
    
    [textField resignFirstResponder];
    _vwTxtNoteName.hidden=YES;
    return YES;
}
- (void)keyboardDidHide:(NSNotification *)aNotification {
    [_txtFldNoteName resignFirstResponder];
    [_noteName setTitle:_txtFldNoteName.text forState:UIControlStateNormal];
    [DrawingDefaults sharedObject].strNoteName = _txtFldNoteName.text;
    _vwTxtNoteName.hidden=YES;
}


- (IBAction)takeScreenshot:(id)sender
{
    // show the preview image
    self.previewImageView.image = self.drawingView.image;
    self.previewImageView.hidden = NO;
    
    // close it after 3 seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        self.previewImageView.hidden = YES;
    });
}

- (IBAction)undo:(id)sender
{
    
    if([sender isSelected])
    {
        [sender setSelected:YES];
        // work here
    }
    else
    {
        [sender setSelected:YES];
    }
    
    
    [self.drawingView undoLatestStep];
    [self updateButtonStatus];
}

- (IBAction)redo:(id)sender
{
    [self.drawingView redoLatestStep];
    [self updateButtonStatus];
}

- (IBAction)clear:(id)sender
{
    [self.drawingView clear];
    [self updateButtonStatus];
}


#pragma mark - ACEDrawing View Delegate

- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool;
{
//    [self updateButtonStatus];
    
    if (!self.panView.drawingView)
    {
        [self updateButtonStatus];
    }else
    {
        if (zoomView)
        {
            panImage=self.panView.drawingView.image;
            
            self.panImgVw.image=panImage;
            self.panImgVw.contentMode=UIViewContentModeScaleAspectFit;
            self.panImgVw.hidden=NO;
            
        }
        
    }
}



#pragma mark - Settings

//- (IBAction)colorChange:(id)sender
//{
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Selet a color"
//                                                             delegate:self
//                                                    cancelButtonTitle:@"Cancel"
//                                               destructiveButtonTitle:nil
//                                                    otherButtonTitles:@"Black", @"Red", @"Green", @"Blue", nil];
//
//    [actionSheet setTag:TagActionSheetColor];
//    [actionSheet showInView:self.view];
//}

- (IBAction)toolChange:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Selet a tool"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Pen", @"Line",
                                  @"Rect (Stroke)", @"Rect (Fill)",
                                  @"Ellipse (Stroke)", @"Ellipse (Fill)",
                                  @"Eraser", @"Text", @"Text (Multiline)",
                                  nil];
    
    [actionSheet setTag:TagActionSheetTool];
    [actionSheet showInView:self.view];
}

- (IBAction)toggleWidthSlider:(id)sender
{
    // toggle the slider
    self.lineWidthSlider.hidden = !self.lineWidthSlider.hidden;
    self.lineAlphaSlider.hidden = YES;
}


- (IBAction)widthChange:(UISlider *)sender
{
    self.drawingView.lineWidth = sender.value;
}

- (IBAction)toggleAlphaSlider:(id)sender
{
    // toggle the slider
    self.lineAlphaSlider.hidden = !self.lineAlphaSlider.hidden;
    self.lineWidthSlider.hidden = YES;
}

- (IBAction)alphaChange:(UISlider *)sender
{
    self.drawingView.lineAlpha = sender.value;
}


#pragma mark - ToolSelection Delegate
- (void)drawShapeOfType:(ShapeType)shapeType
{
    switch (shapeType) {
        case RectangleFill:
            self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill;
            break;
            
        case RectangleStroke:
            self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke;
            break;
            
        case EllipseFill:
            self.drawingView.drawTool = ACEDrawingToolTypeEllipseFill;
            break;
            
        case EllipseStroke:
            self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke;
            break;
            
        case Line:
            self.drawingView.drawTool = ACEDrawingToolTypeLine;
            break;
    }
    
    
}

- (void)drawImageViewWithImage:(UIImage *)image withFrame:(CGRect)frame
{
    _userResizableView = [[SPUserResizableView alloc] initWithFrame:frame];
    _userResizableView.viewType = SPUserResizableViewTypeImage;
    UIImageView *contentView = [[UIImageView alloc] initWithImage:image];
    _userResizableView.contentViewCustom = contentView;
    _userResizableView.delegate = (id)self;
    [_userResizableView showEditingHandles];
    [_userResizableView setBackgroundColor:[UIColor clearColor]];
    [self.drawingView addSubview:_userResizableView];
    _userResizableView.viewNumber = intResizableViewCounterImage;
    _userResizableView.isResizableImage = YES;
    //[_userResizableView setDrawingView];
    intResizableViewCounterImage++;
    
    NSArray *arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData];
    NSMutableArray *arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    [arrayCurrentValues addObject:_userResizableView];
    [[DrawingDefaults sharedObject].dictResizableData setValue:arrayCurrentValues forKey:KeyData];
    
    arrayPreviousValues = nil;
    arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyFrames];
    
    arrayCurrentValues=nil;
    arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    
    UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:_userResizableView.frame];
    [arrayCurrentValues addObject:imgRect];
    [[DrawingDefaults sharedObject].dictResizableData setValue:arrayCurrentValues forKey:KeyFrames];
    
    
    arrayPreviousValues = nil;
    arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyID];
    
    arrayCurrentValues=nil;
    arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    
    NSNumber *number = [NSNumber numberWithInt:[DrawingDefaults sharedObject].intNotesSubNotesCounter];
    [arrayCurrentValues addObject:number];
    [[DrawingDefaults sharedObject].dictResizableData setValue:arrayCurrentValues forKey:KeyID];
    
    
}
- (void)openCameraRoll
{
    // Check Camera availability
    if (IS_CAMERA_AVAILABLE) {
        // Check Camera availability
        [ToolSelectionObject sharedObject].imageSourceType = UIImagePickerControllerSourceTypeCamera;
        
        ImagePickerViewController *imagePickerViewController = [[ImagePickerViewController alloc] init];
        [self presentViewController:imagePickerViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Camera is not available." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView show];
    }
    
}

- (void)drawStickyWithText : (NSString *)str andFrame:(CGRect)frame
{
    
    
    
    //    self.drawingView.drawTool = ACEDrawingToolTypeSticky;
    
    _userResizableView = [[SPUserResizableView alloc] initWithFrame:frame];
    _userResizableView.viewType = SPUserResizableViewTypeSticky;
    //    _userResizableView.center = self.drawingView.center;
    _userResizableView.viewNumber=intResizableViewCounterSticky;
    UITextView *contentView = [[UITextView alloc] initWithFrame:frame];
    contentView.text =str;
    [contentView setFont:[UIFont systemFontOfSize:18]];
    contentView.userInteractionEnabled=NO;
    contentView.delegate=self;
    contentView.backgroundColor = [UIColor yellowColor];
    _userResizableView.contentViewCustom = contentView;
    _userResizableView.delegate = (id)self;
    [_userResizableView showEditingHandles];
    [self.drawingView addSubview:_userResizableView];
    intResizableViewCounterSticky++;
    
    NSArray *arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData];
    NSMutableArray *arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    [arrayCurrentValues addObject:_userResizableView];
    [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyData];
    
    arrayPreviousValues = nil;
    arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyFrames];
    
    arrayCurrentValues=nil;
    arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    
    UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:_userResizableView.frame];
    [arrayCurrentValues addObject:imgRect];
    [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyFrames];
    
    
    
    
    arrayPreviousValues = nil;
    arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyText];
    
    arrayCurrentValues=nil;
    arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    
    [arrayCurrentValues addObject:contentView.text];
    [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyText];
    
    
    arrayPreviousValues = nil;
    arrayPreviousValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyID];
    
    arrayCurrentValues=nil;
    arrayCurrentValues = [[NSMutableArray alloc]initWithArray:arrayPreviousValues];
    
    NSNumber *number = [NSNumber numberWithInt:[DrawingDefaults sharedObject].intNotesSubNotesCounter];
    [arrayCurrentValues addObject:number];
    [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyID];
    
}

#pragma mark - DrawingView delegates
-(void)drawingView:(ACEDrawingView *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool
{
    //    [_userResizableView hideEditingHandles];
}

#pragma mark - SPUserResizableView Delegate

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView
{
    
    
    if(userResizableView.isResizableImage)
    {
        CGRect frame        = userResizableView.frame;
        frame.origin.y      = userResizableView.frame.origin.y+ImageYValue;
        frame.origin.x      = userResizableView.frame.origin.x+5;
        frame.size.height   = userResizableView.frame.size.height-5;
        frame.size.width    = userResizableView.frame.size.width-5;
        
        
        NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyFrames];
        
        int index = [[[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData] indexOfObject:userResizableView];
        
        [arrayCurrentValues replaceObjectAtIndex:index withObject:[UIBezierPath bezierPathWithRect:frame]];
        //        [arrayCurrentValues replaceObjectAtIndex:userResizableView.viewNumber-1 withObject:[UIBezierPath bezierPathWithRect:frame]];
        
        _textEditor.textContainer.exclusionPaths = arrayCurrentValues;
        
        [[DrawingDefaults sharedObject].dictResizableData setValue:arrayCurrentValues forKey:KeyFrames];
        
    }
    else
    {
        CGRect frame = userResizableView.frame;
        frame.origin.y = userResizableView.frame.origin.y+ImageYValue;
        frame.origin.x = userResizableView.frame.origin.x+5;
        frame.size.height = userResizableView.frame.size.height-5;
        frame.size.width = userResizableView.frame.size.width-5;
        
        NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyFrames];
        
        int index = [[[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData] indexOfObject:userResizableView];
        
        [arrayCurrentValues replaceObjectAtIndex:index withObject:[UIBezierPath bezierPathWithRect:frame]];
        
        _textEditor.textContainer.exclusionPaths = arrayCurrentValues;
        
        [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyFrames];
        
        
    }
    
}

- (void)closeButtonAction:(SPUserResizableView*)resizableView
{
    if(resizableView.isResizableImage)
    {
        
        // remove rezizableObject
        NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyData];
        int indexToBeRemoved = [arrayCurrentValues indexOfObject:resizableView];
        SPUserResizableView *tempView = [arrayCurrentValues objectAtIndex:indexToBeRemoved];
        [tempView removeFromSuperview];
        [arrayCurrentValues removeObjectAtIndex:[arrayCurrentValues indexOfObject:resizableView]];
        [[DrawingDefaults sharedObject].dictResizableData setValue:arrayCurrentValues forKey:KeyData];
        intResizableViewCounterImage--;
        
        // remove frame of rresizable object
        arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableData valueForKey:KeyFrames];
        [arrayCurrentValues removeObjectAtIndex:indexToBeRemoved];
        
        
    }
    else
    {
        // remove rezizableObject
        NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData];
        int indexToBeRemoved = [arrayCurrentValues indexOfObject:resizableView];
        SPUserResizableView *tempView = [arrayCurrentValues objectAtIndex:indexToBeRemoved];
        [tempView removeFromSuperview];
        [arrayCurrentValues removeObjectAtIndex:[arrayCurrentValues indexOfObject:resizableView]];
        [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyData];
        intResizableViewCounterSticky--;
        
        // remove frame of rresizable object
        arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyFrames];
        [arrayCurrentValues removeObjectAtIndex:indexToBeRemoved];
        
        // remove text of rresizable object
        arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyText];
        [arrayCurrentValues removeObjectAtIndex:indexToBeRemoved];
        
        arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyID];
        [arrayCurrentValues removeObjectAtIndex:indexToBeRemoved];
        
        
        
    }
}


#pragma mark - UITextView delegates
-(void)textViewDidEndEditing:(UITextView *)textView
{
    NSMutableArray *arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyData];
    int indexToBeRemoved = (int)[arrayCurrentValues indexOfObject:textView.superview];
    
    
    arrayCurrentValues = [[DrawingDefaults sharedObject].dictResizableSticky valueForKey:KeyText];
    [arrayCurrentValues replaceObjectAtIndex:indexToBeRemoved withObject:textView.text];
    [[DrawingDefaults sharedObject].dictResizableSticky setValue:arrayCurrentValues forKey:KeyText];
    
}

- (void)hideDrawingListView
{
    
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame = _drawingListView.frame;
        frame.origin.x = self.drawingView.frame.size.width;
        _drawingListView.frame =frame;
    } completion:^(BOOL finished) {
        [_drawingListView removeFromSuperview];
        _drawingListView=nil;
        
    }];
}

- (void)showDrawingListView
{
    [self saveToDatabase];
    
    float width,height,x,y;
    if (IS_IPAD)
    {
        width = (self.drawingView.frame.size.width*20)/100;
        height = self.drawingView.frame.size.height;
        y = 115;//self.drawingView.frame.origin.y;
        x = self.drawingView.frame.size.width;// - (self.drawingView.frame.size.width*20)/100;
    }else
    {
        width = 120;
        height = self.view.frame.size.height-115;
        y = 115;//self.drawingView.frame.origin.y;
        x = self.drawingView.frame.size.width;// - (self.drawingView.frame.size.width*20)/100;
    }
    
    _drawingListView = [[[NSBundle mainBundle] loadNibNamed:@"DrawingPageListView" owner:self options:nil] lastObject];
    _drawingListView.frame = CGRectMake(x,y,width,height);
    [self.view addSubview:_drawingListView];
    [_drawingListView commonInitialization:_noteName.titleLabel.text];
    _drawingListView.delegate = self;
    
    
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame = _drawingListView.frame;
        frame.origin.x = self.drawingView.frame.size.width - (self.drawingView.frame.size.width*20)/100;
        _drawingListView.frame =frame;
        
    }];
}


#pragma mark - DrawingPageListView delegates

-(void)addDrawingPage:(int)index
{
    //    NSDictionary *_dictData = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:[[DatabaseManager sharedManager] fetchNumberOFPagesSavedIfAny],nil] forKeys:[NSArray arrayWithObjects:KeyNotesData, nil]];
    //    [self newRecordOrExistingRecord:YES withTotalCount:((NSArray*)[_dictData valueForKey:KeyNotesData]).count];
    
    
    [self startNewDrawingFromInsideAfterDelete:NO];
    [[DrawingDefaults sharedObject].dictResizableSticky removeAllObjects];
    [[DrawingDefaults sharedObject].dictResizableData removeAllObjects];
    [_drawingListView reloadDataAfterInsertDelete:@""];
    [_userResizableView removeFromSuperview];
}

-(void)deleteDrawingPage:(int)index
{
    NSString *str =  [DrawingDefaults sharedObject].strNoteName;
    [self startNewDrawingFromInsideAfterDelete:YES];
    [[DatabaseManager sharedManager] deleteNotesWithID:str withIndex:index-1];
    
    
    [[DrawingDefaults sharedObject].dictResizableSticky removeAllObjects];
    [[DrawingDefaults sharedObject].dictResizableData removeAllObjects];
    [DrawingDefaults sharedObject].intNotesSubNotesCounter = [DrawingDefaults sharedObject].intNotesSubNotesCount;
    [self loadDataFromDatabaseWithIndex:-1];
    
    
    [_drawingListView reloadDataAfterInsertDelete:str];
}

- (void)selectedDrawingPage:(int)index
{
    NSLog(@"index %d",index);
    [DrawingDefaults sharedObject].intNotesSubNotesCounter = index;
    
    for (SPUserResizableView *view in self.drawingView.subviews) {
        [view removeFromSuperview];
    }
    //    {
    //        SPUserResizableView *view = [self.drawingView.subviews objectAtIndex:i];
    //        [view removeFromSuperview];
    //    }
    
    [[DrawingDefaults sharedObject].dictResizableSticky removeAllObjects];
    [[DrawingDefaults sharedObject].dictResizableData removeAllObjects];
    
    [self loadDataFromDatabaseWithIndex:index-1];
}


- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withScale:(float)scale withCenter:(CGPoint)center
{
    CGRect zoomRect;
    
    zoomRect.size.height = scrollView.frame.size.height / scale;
    zoomRect.size.width  = scrollView.frame.size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return  self.drawingView;
}


-(void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    if (zoomView)
    {
        
        CGPoint touchPoint = [gesture locationInView:self.drawingView];
        self.currentFrame = CGRectMake(touchPoint.x, touchPoint.y, 150, 60);
        
        [self setCurrentFrame:self.currentFrame];
        
        self.panImgVw.frame=self.currentFrame;
        self.panImgVw.image=nil;
        
        if (firstTime)
        {
            
            [self makeImageFromData:previousFrame Image:panImage];
            previousFrame=self.currentFrame;
        }
        
        
        panImage=nil;
        
        [self.panView.drawingView clear];
        
    }
}

-(void)makeImageFromData:(CGRect)frameImage Image:(UIImage *)panImageSend
{
    tempImgVw=[[UIImageView alloc]initWithFrame:frameImage];
    tempImgVw.image=panImageSend;
    tempImgVw.contentMode=UIViewContentModeScaleAspectFit;
    tempImgVw.backgroundColor=[UIColor clearColor];
    
    [self.drawingView addSubview:tempImgVw];
    
    [self.drawingView bringSubviewToFront:tempImgVw];
}

-(void)removeAllImageView
{
    [self.drawingView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIImageView class]])
        {
            if (obj==tempImgVw)
            {
                [obj removeFromSuperview];
            }
            
        }
    }];
    
}

-(void)saveNotewithProject
{
    NSLog(@"%ld",(long)prntVAL);
    
    
    NSLog(@"%@",_noteName.titleLabel.text);
    
    NSLog(@"%@",self.notesTitleBinary);
    NSLog(@"%@",self.notesTitleBinaryLarge);
    
    NSLog(@"tempArr = %@ tempImgArr = %@  tempSktArray = %@ tempWebImgTArray = %@ temptxtArray = %@ tempHWRTArray = %@ tempBookMarksArray = %@",tempMapArray,tempImgArray,tempSktArray,tempWebImgTArray,temptxtArray,tempHWRTArray,tempBookMarksArray);
    
    
    NotesDataObject *NotesObj =[[NotesDataObject alloc]init];
    
    [NotesObj setNotesTitleType:@"T"];
    
    [NotesObj setNotesTitleBinary:self.notesTitleBinary];
    [NotesObj setNotesTitleBinaryLarge:self.notesTitleBinaryLarge];
    [NotesObj setNotesMapDataArray:tempMapArray];
    [NotesObj setNotesPicDataArray:tempImgArray];
    [NotesObj setNotessktchimageArrsy:tempSktArray];
    [NotesObj setNotesWebURArray:tempWebImgTArray];
    [NotesObj setNotesTextArray:temptxtArray];
    [NotesObj setNotesHwrtDataArray:tempHWRTArray];
    [NotesObj setNotesBMDataArray:tempBookMarksArray];
    
    [NotesObj setNotesTitle:_noteName.titleLabel.text];
    
    
    [NotesObj setBelongsToProjectID:prntVAL];
    
    BOOL isInserted = [AllInsertDataMethods insertNotesDataValues:NotesObj];
    if (isInserted == YES)
    {
        
        NSLog(@"data save");
    }
    
}

-(void)updateNotesTittleOnly
{
    
    BOOL isInserted = [AllInsertDataMethods updateNotesTitleOnly:_noteName.titleLabel.text oldTittle:strValueSelectedFromDatabase];
    
    if (isInserted == YES)
    {
        
        NSLog(@"data update");
    }
}

@end

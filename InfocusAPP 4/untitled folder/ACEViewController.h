

#import <UIKit/UIKit.h>
#import "RichTextEditor.h"
#import "ToolSelectionObject.h"
#import "PanView.h"


@class ACEDrawingView;

@interface ACEViewController : UIViewController <RichTextEditorDataSource,ToolSelectionDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,UIScrollViewDelegate>
{
    UIImagePickerController* pickerimage;
    
    
    BOOL textAction;
    
    
    BOOL firstTime;
    CGRect previousFrame;
    UIImage * panImage;
    UIImageView *tempImgVw;
    NSInteger prntVAL;
    
    
    NSMutableArray *tempTitleArray;
    NSMutableArray *tempImgArray;
    NSMutableArray *tempMapArray;
    NSMutableArray *tempSktArray;
    NSMutableArray *temptxtArray;
    NSMutableArray *tempHWRTArray;
    NSMutableArray *tempWebImgTArray;
    NSMutableArray *tempBookMarksArray;
    
    NSData *notesTitleBinary;
    NSData *notesTitleBinaryLarge;
    
    NSInteger checkProject_ID;
    
    
}


@property BOOL isComingFromProjectModule;
@property BOOL isFetchDataOnly;
@property(nonatomic,assign)NSInteger prntVAL;

@property(nonatomic,strong)NSData *notesTitleBinary;
@property(nonatomic,strong)NSData *notesTitleBinaryLarge;

@property(strong,nonatomic)NSMutableArray *tempTitleArray;
@property(strong,nonatomic)NSMutableArray *tempImgArray;
@property(strong,nonatomic)NSMutableArray *tempMapArray;
@property(strong,nonatomic)NSMutableArray *tempSktArray;
@property(strong,nonatomic)NSMutableArray *temptxtArray;
@property(strong,nonatomic)NSMutableArray *tempHWRTArray;
@property(strong,nonatomic)NSMutableArray *tempWebImgTArray;
@property(strong,nonatomic)NSMutableArray *tempBookMarksArray;





@property(strong,nonatomic)NSString *strValueSelectedFromDatabase;

@property (nonatomic, weak) IBOutlet UITableView *pagesTableView;
@property (nonatomic, unsafe_unretained) IBOutlet ACEDrawingView *drawingView;
@property (nonatomic, unsafe_unretained) IBOutlet UISlider *lineWidthSlider;
@property (nonatomic, unsafe_unretained) IBOutlet UISlider *lineAlphaSlider;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *previewImageView;

@property (nonatomic, unsafe_unretained) IBOutlet UIBarButtonItem *undoButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIBarButtonItem *redoButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIBarButtonItem *colorButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIBarButtonItem *toolButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIBarButtonItem *alphaButton;
@property (nonatomic, assign) BOOL boolNewNotepage;

//@property BOOL isComingFromProjectModule;

@property (strong, nonatomic) IBOutlet UIScrollView *zoomScrollView;

//PAN View
@property (strong, nonatomic) IBOutlet PanView *panView;
//@property (strong, nonatomic) IBOutlet ACEDrawingView *aceDrawingView;
- (IBAction)panAction:(UIButton*)sender;
//@property(nonatomic,assign)NSInteger prntVAL;
@property (strong, nonatomic) IBOutlet UIImageView *panImgVw;




// actions
- (IBAction)undo:(id)sender;
- (IBAction)redo:(id)sender;
- (IBAction)clear:(id)sender;
- (IBAction)takeScreenshot:(id)sender;

// settings
//- (IBAction)colorChange:(id)sender;
- (IBAction)toolChange:(id)sender;
- (IBAction)toggleWidthSlider:(id)sender;
- (IBAction)widthChange:(UISlider *)sender;
- (IBAction)toggleAlphaSlider:(id)sender;
- (IBAction)alphaChange:(UISlider *)sender;

-(void)setRowSelected : (NSString*)valueSelected atSelectedIndex: (int)indexSelected withTotalCount:(int)totalCount;
- (void)newRecordOrExistingRecord:(BOOL)boolValue withTotalCount:(int)totalCount;
@end

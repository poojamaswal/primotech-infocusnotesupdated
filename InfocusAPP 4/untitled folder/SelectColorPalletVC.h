

#import <UIKit/UIKit.h>

@protocol ColorPalletDelegate <NSObject>
@required
-(void)setColorPalletValues:(BOOL)isHighlighter;
@end


@interface SelectColorPalletVC : UIViewController 

@property(nonatomic, assign)id<ColorPalletDelegate> delegateColorPallet;
@property (nonatomic, assign) BOOL isHighlighter;



@end

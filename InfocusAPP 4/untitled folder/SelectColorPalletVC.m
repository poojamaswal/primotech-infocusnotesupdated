

#import "SelectColorPalletVC.h"
#import "DrawingDefaults.h"

@interface SelectColorPalletVC ()



@property (weak, nonatomic) IBOutlet UIView *_vwColorPallet;

@property (weak, nonatomic) IBOutlet UIView *selectedColorView;
@property (weak, nonatomic) IBOutlet UISlider *colorRCompSlider;
@property (weak, nonatomic) IBOutlet UISlider *colorGCompSlider;
@property (weak, nonatomic) IBOutlet UISlider *colorBCompSlider;
@property (weak, nonatomic) IBOutlet UISlider *alphaCompSlider;
@property (weak, nonatomic) IBOutlet UISlider *sizeSlider;


@property (nonatomic, weak) IBOutlet UILabel *alphaHeaderLabel;
@property (nonatomic, weak) IBOutlet UILabel *alphaValueLabel;

@property (nonatomic, weak) IBOutlet UILabel *colorRValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *colorGValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *colorBValueLabel;

@property (nonatomic, weak) IBOutlet UILabel *sizeHeaderLabel;
@property (nonatomic, weak) IBOutlet UILabel *sizeValueLabel;

- (IBAction)colorComponentValueChanged:(UISlider *)sender;
- (IBAction)sizeValueChanged:(UISlider *)sender;
- (IBAction)alphaValueChanged:(UISlider *)sender;



@end

@implementation SelectColorPalletVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // memory
    [DrawingDefaults sharedObject].dictCollorPalletValues = [NSMutableDictionary dictionary];
    [self setSelectedColorValues];
}

-(void)setSelectedColorValues{
    
    NSLog(@"Dict Color Pallet: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]);
    NSLog(@"Dict Color Pallet: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]);
    
     //BRUSH COLOR SELECTION
    if(self.isHighlighter){
        
        NSDictionary* dictDetails = [[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"];
        
        if([dictDetails count]==0)
            
        {
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.sizeValueLabel.text floatValue]] forKey:@"Size"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorRValueLabel.text forKey:@"R"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorGValueLabel.text forKey:@"G"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorBValueLabel.text forKey:@"B"];
            
            
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: 50]
                                                                      forKey:@"Alpha"];
            [_alphaHeaderLabel setHidden:TRUE];
            [_alphaValueLabel setHidden:TRUE];
            [_alphaCompSlider setHidden:TRUE];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[DrawingDefaults sharedObject].dictCollorPalletValues forKey:@"Brush"];
            
           
            
            CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"R"] floatValue];
            CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"G"] floatValue];
            CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"B"] floatValue];
            
            CGFloat size =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"Size"]floatValue];
            NSString *myString = [[NSNumber numberWithFloat:size] stringValue];
            self.sizeValueLabel.text = myString;
            
            //Set selected Color to Brush
            self.selectedColorView.backgroundColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:0.5];
            
            
        }
        
        else{
            
            CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"R"] floatValue];
            CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"G"] floatValue];
            CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"B"] floatValue];
            
            CGFloat size =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Brush"]valueForKey:@"Size"]floatValue];
            NSString *myString = [[NSNumber numberWithFloat:size] stringValue];
            self.sizeValueLabel.text = myString;
            
            //Set selected Color to Brush
            self.selectedColorView.backgroundColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:0.5];
            
        }
    }
    
    //PEN COLOR SELECTION
    else{
       
        NSDictionary* dictDetails = [[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"];
        
        if([dictDetails count]==0)
            
        {
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.sizeValueLabel.text floatValue]] forKey:@"Size"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorRValueLabel.text forKey:@"R"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorGValueLabel.text forKey:@"G"];
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorBValueLabel.text forKey:@"B"];
            
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.alphaValueLabel.text floatValue]]forKey:@"Alpha"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[DrawingDefaults sharedObject].dictCollorPalletValues forKey:@"Pen"];
        
            CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"R"] floatValue];
            CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"G"] floatValue];
            CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"B"] floatValue];
            
            CGFloat size =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"Size"]floatValue];
            NSString *myString = [[NSNumber numberWithFloat:size] stringValue];
            self.sizeValueLabel.text = myString;
            
            //Set selected Color to Pen
            self.selectedColorView.backgroundColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"Alpha"] floatValue]];
        
        }
        
        
        else{
            
            
            CGFloat red = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"R"] floatValue];
            CGFloat green = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"G"] floatValue];
            CGFloat blue = (CGFloat)[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"B"] floatValue];
            
            CGFloat size =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"Pen"]valueForKey:@"Size"]floatValue];
            NSString *myString = [[NSNumber numberWithFloat:size] stringValue];
            self.sizeValueLabel.text = myString;
           
           
            //Set selected Color to Pen
            self.selectedColorView.backgroundColor =[UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:50.0];
          
            
        }

    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissVC:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
        
        [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.sizeValueLabel.text floatValue]]
                                                                  forKey:@"Size"];
        [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorRValueLabel.text
                                                                  forKey:@"R"];
        [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorGValueLabel.text
                                                                  forKey:@"G"];
        [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorBValueLabel.text
                                                                  forKey:@"B"];
        
        
        if(self.isHighlighter){
            
            [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: 50]
                                                                      forKey:@"Alpha"];
            [[NSUserDefaults standardUserDefaults] setObject:[DrawingDefaults sharedObject].dictCollorPalletValues forKey:@"Brush"];
            [self.delegateColorPallet setColorPalletValues:TRUE];

        }
        else{
            
        [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.alphaValueLabel.text floatValue]] forKey:@"Alpha"];
        [[NSUserDefaults standardUserDefaults] setObject:[DrawingDefaults sharedObject].dictCollorPalletValues forKey:@"Pen"];
        [self.delegateColorPallet setColorPalletValues:FALSE];

        }
        
    }];
}

#pragma mark - Size Value Change Actions
- (IBAction)sizePalletValueChanged:(id)sender {
    
    UIButton*btn =(UIButton*)sender ;
    self.sizeValueLabel.text= [NSString stringWithFormat:@"%ld",(long)btn.tag];
    self.sizeSlider.value = btn.tag;
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: [self.sizeValueLabel.text floatValue]]
                                   forKey:@"Size"];
    
    
}


#pragma mark - Color Pallet Value Change Actions
- (IBAction)colorPalletColorValueChanged:(id)sender {
    
    UIButton*btn =(UIButton*)sender ;
    [self updateColorPalleteWithColor:btn.backgroundColor];
    
    
    
    [UIView animateWithDuration:0.2 animations:^{
        [__vwColorPallet setBackgroundColor: btn.backgroundColor];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
         [__vwColorPallet setBackgroundColor: [UIColor whiteColor]];
        }];
        
    }];
}


#pragma mark - Slider Value Change Actions
- (IBAction)colorComponentValueChanged:(UISlider *)sender {
    [self updateColorPalleteFromColorComponentsR:self.colorRCompSlider.value/255.0 G:self.colorGCompSlider.value/255.0 B:self.colorBCompSlider.value/255.0];
}

- (IBAction)sizeValueChanged:(UISlider *)sender {
    self.sizeValueLabel.text = [NSString stringWithFormat:@"%.0f", sender.value];
    //self.superBarButton.lineWidth = sender.value;
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: sender.value]forKey:@"Size"];
}

- (IBAction)alphaValueChanged:(UISlider *)sender {
    self.alphaValueLabel.text = [NSString stringWithFormat:@"%.0f%%", sender.value];
    //self.superBarButton.lineAlpha = (sender.value/100);
    self.selectedColorView.backgroundColor = [self.selectedColorView.backgroundColor colorWithAlphaComponent:(sender.value/100)];
    
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:[NSNumber numberWithFloat: sender.value]forKey:@"Alpha"];
    
    
}

#pragma mark - Update Color Pallete
- (void)updateColorPalleteWithColor:(UIColor*)color {
    if (!color) {
        return;
    }
    
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    if ([color respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [color getRed:&red green:&green blue:&blue alpha:&alpha];
    } else {
        const CGFloat *components = CGColorGetComponents(color.CGColor);
        red = components[0];
        green = components[1];
        blue = components[2];
        alpha = components[3];
    }
    
    [self updateColorPalleteFromColorComponentsR:red G:green B:blue];
}

- (void)updateColorPalleteFromColorComponentsR:(CGFloat)valueR G:(CGFloat)valueG B:(CGFloat)valueB {
    UIColor *color = [UIColor colorWithRed:valueR green:valueG blue:valueB alpha:(self.alphaCompSlider.value/100)];
    
    if(self.isHighlighter){
      self.selectedColorView.backgroundColor = [UIColor colorWithRed:valueR green:valueG blue:valueB alpha:0.5];
    }
     else{
          self.selectedColorView.backgroundColor = color;
         
     }
        
    // self.superBarButton.lineColor = color;
    
    self.colorRCompSlider.value = valueR*255.0;
    self.colorGCompSlider.value = valueG*255.0;
    self.colorBCompSlider.value = valueB*255.0;
    
    self.colorRValueLabel.text = [NSString stringWithFormat:@"%.0f", self.colorRCompSlider.value];
    self.colorGValueLabel.text = [NSString stringWithFormat:@"%.0f", self.colorGCompSlider.value];
    self.colorBValueLabel.text = [NSString stringWithFormat:@"%.0f", self.colorBCompSlider.value];
    
    
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorRValueLabel.text
                                   forKey:@"R"];
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorGValueLabel.text
                                   forKey:@"G"];
    [[DrawingDefaults sharedObject].dictCollorPalletValues setObject:self.colorBValueLabel.text
                                   forKey:@"B"];
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

 //
//  FAQViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    if (IS_IOS_7) {
        

        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        //self.view.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:228.0/255.0  blue:228.0/255.0  alpha:1.0];
        self.view.backgroundColor = [UIColor whiteColor];
        TblViewFAQ.backgroundColor = [UIColor whiteColor];
        
        
        backButton.hidden = YES;
        doneButton.hidden = YES;
        
        self.navigationController.navigationBar.translucent = YES;
        //sets Nav bar to match a black one with 75% alpha, like Black Translucent in iOS 7
        if(IS_IPAD)
        {
            [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
            self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
        self.navigationItem.title = @"FAQ";
        }
        else
        {
            self.navigationController.navigationBar.barTintColor = [UIColor
                                                                    colorWithRed:66.0/255.0
                                                                    green:66.0/255.0
                                                                    blue:66.0/255.0
                                                                    alpha:1.0];
            
            //Done Button
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(backButton_Clicked:)];
            
            self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
            
            UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"FAQ"];
            item.hidesBackButton = YES;
            
            item.rightBarButtonItem = doneButton_iOS7;
            item.rightBarButtonItem.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;
        }
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        

        
        
        
    }
    else{ //iOS 6
        
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
        
        
        //Sets Navigation item color
        [[UIBarButtonItem appearanceWhenContainedIn: [UINavigationBar class], nil] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [self.navigationController.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }


    if (IS_LIST_VERSION) {
        //[TblViewFAQ beginUpdates];
        //[TblViewFAQ deleteRowsAtIndexPaths:[NSArray arrayWithObjects:0,1,2,3,nil]  withRowAnimation:UITableViewRowAnimationTop];
         //[TblViewFAQ deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]]];
        //[TblViewFAQ deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:r inSection:s]]];
        //[TblViewFAQ endUpdates];
      //  [TblViewFAQ reloadData];
    }
    
    
}

- (IBAction)backButton_Clicked:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"heightForRowAtIndexPath");
    
    //Steve - gets rid of how to get a refund FAQ since app is Free
    if (indexPath.row == 9) {
        return 0.0;
    }
    
    if (IS_TO_DO_VERSION){
        switch (indexPath.row) {
            case 4:
                return 44.0;
                break;
                
        //    case 9:
          //      return 44.0;
            //    break;
                
            case 10:
                return 44.0;
                break;
                
            default:
                return 0.0; //so other cells don't show
                break;
        }
    }
    
    if (IS_LIST_VERSION){
        switch (indexPath.row) {
            case 1:
                return 44.0;
                break;
                
            case 2:
                return 44.0;
                break;
                
            case 3:
                return 44.0;
                break;
                
        //    case 9:
         //       return 44.0;
          //      break;
                
            case 10:
                return 44.0;
                break;
                
            default:
                return 0.0; //so other cells don't show
                break;
        }
    }
    
    if (IS_PROJECTS_VER){
        switch (indexPath.row) {
            case 5:
                return 44.0;
                break;
                
            case 7:
                return 44.0;
                break;
                
           // case 9:
            //    return 44.0;
             //   break;
                
            case 10:
                return 44.0;
                break;
                
            default:
                return 0.0; //so other cells don't show
                break;
        }
    }
    
    if (IS_NOTES_VER){
        switch (indexPath.row) {
            case 8:
                return 44.0;
                break;
                
            case 9:
                return 44.0;
                break;
                
           // case 10:
           //     return 44.0;
             //   break;
                
            default:
                return 0.0; //so other cells don't show
                break;
        }
    }
    
    return 44.0; // for Pro version
}



- (void)viewDidUnload
{
    TblViewFAQ = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

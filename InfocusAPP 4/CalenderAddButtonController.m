//
//  CalenderAddButtonController.m
//  Organizer
//
//  Created by Nibha Aggarwal on 2/10/12.
//  Copyright 2012 A1 Technology Pvt Ltd. All rights reserved.
//

#import "CalenderAddButtonController.h"
#import "OrganizerAppDelegate.h"
#import "RGBColorViewCantroller.h"
#import "SelectProjectColorViewCantroller.h"
#import "AppointmentsDataObject.h"
@implementation CalenderAddButtonController
@synthesize delegate;
@synthesize calObj;

-(id)initWithDataObj: (CalendarDataObject*) caldata {
    if(IS_IPAD)
    {
        if (self = [super initWithNibName:@"CalenderAddButtonController_iPad" bundle:[NSBundle mainBundle]]) {
            IsEdit =YES;
            self.calObj = caldata;
            CalIdentifier = caldata.calendarIdentifire;
        }
    }
    else
    {
    if (self = [super initWithNibName:@"CalenderAddButtonController" bundle:[NSBundle mainBundle]]) {
        IsEdit =YES;
        self.calObj = caldata;
        CalIdentifier = caldata.calendarIdentifire;
    }
    }
    return self;
}
-(void)viewDidLoad
{
	[super viewDidLoad];
	[ColorButton.layer setBorderWidth:.24];
	[ColorButton.layer setCornerRadius:7.50];
	[ColorButton.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	ColorButton.layer.masksToBounds = YES;
	
    if(IsEdit){
        [[navBar.items objectAtIndex:0] setTitle:@"Edit Calendar"];
        calName.text = calObj.calendarName;
        NSArray * temp = [calObj.calendarName componentsSeparatedByString:@"~"];
        if([temp count]>=2){
            calName.text=[temp objectAtIndex:0];
            CalIdentifier = [temp objectAtIndex:1];
        }
        
        
        redVal =calObj.calendarColorRed;
        greenVal =calObj.calendarColorGreen;
        blueVal = calObj.calendarColorBlue;
    }else
    {
        [DeleteButton removeFromSuperview];
        redVal =193/255.0;
        greenVal =136/255.0;
        blueVal = 12/255.0;
	}
    
	UIColor *acolor = [UIColor colorWithRed:redVal green:greenVal blue:blueVal alpha:1];
	[ColorButton setBackgroundColor:acolor];
}

-(void)viewWillAppear:(BOOL)animated
{
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        cancelButton.hidden = YES;
        
        calendarButton.frame = CGRectMake(27, 129 - 44, 265, 40);
        calName.frame = CGRectMake(37, 130 - 44, 249, 35);
        calendarColorButton.frame = CGRectMake(27, 183 - 44, 265, 40);
        calendarColorLabel.frame = CGRectMake(68, 193 - 44, 137, 21);
        calendarColorDisclBtn.frame = CGRectMake(257, 189 - 44, 29, 29);
        DeleteButton.frame = CGRectMake(38, 263 - 44, 244, 44);
        ColorButton.frame = CGRectMake(42, 194 - 44, 17, 17);

        
        
        //NSLog(@"shareButton =  %@", NSStringFromCGRect( shareButton.frame));
        
        self.title =@"Add Calendar";
        
        [self.navigationController.navigationBar addSubview:saveButton];
        
        [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
        
        
        
        //Back Button
        /*
        UIImage *backButtonImage = [UIImage imageNamed:@"CancelSmallBtn.png"];
        UIButton *backButtonNewNav = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
        [backButtonNewNav setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [backButtonNewNav addTarget:self action:@selector(cancelButton_Click:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonNewNav];
        
        [[self navigationItem] setLeftBarButtonItem:barButton];
        */
        
    }
    else{
        
        if (!IS_IPHONE_5) { //Not iPhone5
            //shareButton.frame = CGRectMake(105, 416, 111, 37);
        }
        
        self.navigationController.navigationBarHidden = YES;
        
        //backButton = [[UIButton alloc]init];
        //backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    

    
    //UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
    //[navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
 	[calName resignFirstResponder];
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
	[calName becomeFirstResponder];
}

- (IBAction)textFieldDidEndEditing:(UITextField *)textField {
	[textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}


-(IBAction) saveButton_Click:(id)sender
{
    NSString *cal =calName.text;
    if(cal.length<1){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter calendar name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
        [alert show];
        return;
    }
    else{
        NSArray * temp = [cal componentsSeparatedByString:@"~"];
        if([temp count]>1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Calendar name should not be cantain \"~\" symbol." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
            [alert show];
            return;
        }
        
    }
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKSource *theSource = nil;
    NSError *error = nil;
    BOOL result=NO;
    
    UIColor *calcolor = [UIColor colorWithRed:redVal
                                        green:greenVal blue:blueVal alpha:1];
    if(!IsEdit)
    {
        EKCalendar *calendar = [EKCalendar calendarWithEventStore:eventStore];
        for (EKSource *source in eventStore.sources) {
            if (source.sourceType == EKSourceTypeCalDAV) {
                theSource = source;
                NSLog(@"%@",theSource.sourceIdentifier);
                break;
            }
        }
        
        if (theSource) {
            calendar.source = theSource;
            // for calender name
            /*  for (EKCalendar *thisCalendar in theSource.calendars){
             NSLog(@"THis cal->%@ ->%@",thisCalendar.title,thisCalendar.calendarIdentifier);
             
             }*/
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Sorry, iCal source not available on device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil ] ;
            [alert show];
            return;
        }
        calendar.title = calName.text;
        NSLog(@"calName : %@",calName.text);
        
        calendar.CGColor = calcolor.CGColor;
        result = [eventStore saveCalendar:calendar commit:YES error:&error];
        CalIdentifier = calendar.calendarIdentifier;
    }
    else{
        EKCalendar *calendar1 = [eventStore calendarWithIdentifier:CalIdentifier];
        calendar1.title = calName.text;
        calendar1.CGColor = calcolor.CGColor;
        result = [eventStore saveCalendar:calendar1 commit:YES error:&error];
    }
    
    
    if (result) {
        
    } else {
        NSLog(@"Error saving calendar: %@.", error);
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error saving calendar." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil] ;
        [alert show];
        return;
    }
	[self.navigationController popViewControllerAnimated:YES];
}


-(IBAction) cancelButton_Click:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


-(void)setNewRGBColor:(float)red greenColor:(float)green blueColor:(float)blue
{
	redVal =red;
	greenVal =green;
	blueVal = blue;
	
	UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:1];
	[ColorButton setBackgroundColor:acolor];
}
-(IBAction)DeleteCalendarBtnClicked:(id)sender{
    
    if(calObj.calendarID==100){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry! This is Infocus default calendar you can't delete this calendar." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    UIActionSheet *ActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure to delete this calendar?\n All events associated with the calendar will also be deleted." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Calendar" otherButtonTitles:nil,nil];
    [ActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [ActionSheet showInView:self.view];
    
     }

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex ==0){
            EKEventStore *eventStore = [[EKEventStore alloc] init];
            EKCalendar *calendar = [eventStore calendarWithIdentifier:CalIdentifier];
            if (calendar) {
                NSError *error = nil;
                BOOL result = [eventStore removeCalendar:calendar commit:YES error:&error];
                if (result) {
                    
               [self.navigationController popViewControllerAnimated:YES];     
                } else {
                    NSLog(@"Deleting calendar failed: %@.", error);
                }
            }
            
    }
    
}

-(IBAction)rgb_click:(id)sender
{
	RGBColorViewCantroller *RGBview = [[RGBColorViewCantroller alloc] initWithNibName:@"RGBColorViewCantroller" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:RGBview animated:YES];
	[RGBview setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        [saveButton removeFromSuperview];
        [cancelButton removeFromSuperview];
    }

    
}

- (void)viewDidUnload 
{
    navBar = nil;
    saveButton = nil;
    cancelButton = nil;
    calendarButton = nil;
    calendarColorButton = nil;
    calendarColorDisclBtn = nil;
    calendarColorLabel = nil;
    [super viewDidUnload];
}




@end

//
//  NoteNameTextField.m
//  testKeboard
//
//  Created by Gaurav Goyal on 05/06/15.
//  Copyright (c) 2015 CanvasM. All rights reserved.
//

#import "NoteNameTextField.h"

@interface NoteNameTextField () <UITextFieldDelegate>

@end

@implementation NoteNameTextField

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self registerEvents];
    }
    return self;
}

- (BOOL)resignFirstResponder {
    [self.associatedTextField resignFirstResponder];
    return YES;
}

- (void)registerEvents {
    
    //Add tap event on label
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnce:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
    
    //AddObserver for Keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)tappedOnce:(UITapGestureRecognizer*)sender {
    self.inputView.layer.borderColor = [UIColor grayColor].CGColor;
    self.inputView.layer.borderWidth = 0.5;
    
    self.associatedTextField.text = self.text;
    if ([self.delegate respondsToSelector:@selector(noteNameDidBeginEditing:)]) {
        [self.delegate noteNameDidBeginEditing:self.associatedTextField];
    }
    [self.associatedTextField becomeFirstResponder];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.inputView.hidden = NO;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.inputView.hidden = YES;
    self.text = self.associatedTextField.text;
    if ([self.delegate respondsToSelector:@selector(noteNameDidEndEditing:)]) {
        [self.delegate noteNameDidEndEditing:self.associatedTextField];
    }
}

#pragma mark - Keyboard notification events
- (void)keyboardWillShow:(NSNotification *)notification {
    CGFloat animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect keyboardFrame = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        keyboardFrame.origin.y = keyboardFrame.size.height + self.inputView.frame.size.height;
        self.inputViewTopConstraint.constant = -keyboardFrame.origin.y;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    CGFloat animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        self.inputViewTopConstraint.constant = 0;
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

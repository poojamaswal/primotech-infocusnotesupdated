//
//  HwrtDataObject.h
//  Organizer
//
//  Created by Naresh Chouhan on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HwrtDataObject : NSObject
{	
	NSInteger hwrtid;
	NSData *hwrtinfo;
}

@property(readwrite)NSInteger hwrtid;

@property(nonatomic,retain)NSData *hwrtinfo;
@end

//
//  AddNewNotesViewCantroller.m
//  Organizer
//
//  Created by Naresh Chouhan on 7/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddNewNotesViewCantroller.h"
#import "TakePictureViewCantroller.h"
#import "AllInsertDataMethods.h"
#import "UIImage+Resize.h"
#import "NotesTextViewCantroller.h"
#import "TextDataObject.h"

#import "OrganizerAppDelegate.h"
#import "NotesDataObject.h"
#import "SketchNoteViewController.h"
#import "ProjectListViewController.h"

//Steve - Preview Tags
#define KTextPreviewTag     999
#define KStickyPreviewTag   899
#define KPicturePreviewTag  599
#define KSketchPreviewTag   699

//Steve - Alert Tags
#define KTextAlert          2000
#define KStickyAlert        2001
#define KPictureAlert       2002
#define KSketchAlert        2003



@interface AddNewNotesViewCantroller()
-(void)setLabelValues;
@end


@implementation AddNewNotesViewCantroller


@synthesize inputTypeSelectorViewController, notsDataObj;
@synthesize titleTextField, tempTitlTypeeArray;

@synthesize imgPicker;
@synthesize webLabel;
@synthesize picLabel;
@synthesize picImage;
@synthesize showImage;
@synthesize imageSKTch;
@synthesize skLabel;
@synthesize mapLabel;

@synthesize webImageArray;
@synthesize webClipImage;
@synthesize tempImgArray;
@synthesize tempMapArray;
@synthesize tempSktArray;
@synthesize aNotesObj;
@synthesize tempURLArray;
@synthesize temptxtArray;
@synthesize tempHWRTArray;
@synthesize TakePicbtn,Takesktcbtn,Takemapbtn,voicetextbtn;
@synthesize Takehwrtbtn,Takewebbtn,TakeTextbtn,fView;
@synthesize Flag,tempTitleArray,GetNoteID,tempWebImgTArray,tempBookMarksArray;
@synthesize notesTitleBinary,notesTitleBinaryLarge,notesTitle,notesTitleType;
@synthesize projectComponentsArray;
@synthesize projectNameLabel,prntVAL;
@synthesize isComingFromProjectModule;


-(id)initWithDataObj:(NotesDataObject *)notesDataObjLocal {
    //NSLog(@"initWithDataObj");
    
    NSString *viewId;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        viewId = @"AddNewNotesViewController_iPad";
    }else{
        viewId = @"AddNewNotesViewController2";
    }
    
	if (self = [super initWithNibName:viewId bundle:[NSBundle mainBundle]]){
		notsDataObj = notesDataObjLocal;
        projectComponentsArray = nil;
        
	}
	return self;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == titleTextField)
    {
        
        self.notesTitleType = @"T";
        [titleTextField setBackground:nil];

        return YES;
    }
    
    return NO;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    //NSLog(@"viewDidLoad");
    [super viewDidLoad];
	
	tempSktArray	   =	[[NSMutableArray alloc]init];
	tempImgArray       =	[[NSMutableArray alloc]init];
	tempMapArray       =	[[NSMutableArray alloc]init];
	tempURLArray       =	[[NSMutableArray alloc]init];
    temptxtArray       =	[[NSMutableArray alloc]init];
	tempHWRTArray      =	[[NSMutableArray alloc]init];
	tempTitleArray     =	[[NSMutableArray alloc]init];
	tempWebImgTArray   =	[[NSMutableArray alloc]init];
	tempBookMarksArray =	[[NSMutableArray alloc]init];
	tempTitlTypeeArray =	[[NSMutableArray alloc]init];
    
    //Steve - setup new arrays
    textArrayofItems = [[NSMutableArray alloc]init];
    stickyArrayOfItems = [[NSMutableArray alloc]init];
    pictureArrayOfItems = [[NSMutableArray alloc]init];
    sketchArrayOfItems = [[NSMutableArray alloc]init];
    
    
    //Steve - initialize objects
    titleTextField = [[UITextField alloc] init];
    titleTextField.delegate = self;
    inputSelecterButton = [[UIButton alloc]init];
    
    projectFixedLabel = [[UILabel alloc]init];
    projectNameLabel = [[UILabel alloc]init];
    
    textFixedLabel = [[UILabel alloc]init];
    stickyFixedLabel = [[UILabel alloc]init];
    pictureFixedLabel = [[UILabel alloc]init];
    sketchFixedLabel = [[UILabel alloc]init];
    
    DeleteBtn = [[UIButton alloc]init];
	
	fView.alpha=1;


	if(Flag == 1) //Flag means its coming from "Edit" mode
	{
        DeleteBtn.hidden = FALSE;
        
		[self showDataEdit];
        
		//NSLog(@"%d",[notsDataObj.notesTitleArray count]);
        self.notesTitleType =[tempTitlTypeeArray objectAtIndex:0];
        
		if ([[tempTitlTypeeArray objectAtIndex:0] isEqualToString:@"H"]) 
		{
            self.notesTitleBinary= [NSData dataWithData:[tempTitleArray objectAtIndex:0]];

			UIImage *titleImage = [[UIImage alloc ] initWithData:self.notesTitleBinary];
            
            [titleTextField setBackground:titleImage];
            [titleTextField setText:@""];
            [titleTextField resignFirstResponder];
			
		}
		else
		{	
			titleTextField.text = [tempTitleArray objectAtIndex:0]; 
		}
        
        
        
        
      
        projectNameLabel.tag = notsDataObj.belongsToProjectID;
        
          NSLog(@"%ld",(long)projectNameLabel.tag);
            prntVAL = projectNameLabel.tag;
              
        [self getFolderName];
        
	}
	else 
	{
        DeleteBtn.hidden = TRUE;
        
		[self showData];
        
        if(isComingFromProjectModule)
        {
            
            [self getFolderNameForProject];
        }
        
	}
	
	
}


-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear");
	[super viewWillAppear:YES];
    

    
//***********************************************************
// **************** Steve added ******************************
// *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        navBar.hidden = YES;
        backButton.hidden = YES;
        self.navigationController.navigationBarHidden = NO;
        
        //self.title =@"Add/Edit Note";
        
        
        if (IS_IOS_7) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            self.edgesForExtendedLayout = UIRectEdgeAll;//UIRectEdgeNone

            
            
            //done Button
            doneButton.hidden = YES;
            UIBarButtonItem *doneButton_iOS7 = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                target:self
                                                action:@selector(save_buttonClicked:)];
            
            self.navigationItem.rightBarButtonItem = doneButton_iOS7;

            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                self.view.backgroundColor = [UIColor blackColor];
                self.noteTableView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
                self.noteTableView.separatorStyle = UITableViewCellSeparatorStyleNone; //UITableViewCellSeparatorStyleSingleLine
                //self.noteTableView.separatorColor = [UIColor lightGrayColor];
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                
                self.view.backgroundColor = [UIColor blackColor];
                self.noteTableView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
                self.noteTableView.separatorStyle = UITableViewCellSeparatorStyleNone; //UITableViewCellSeparatorStyleSingleLine
                //self.noteTableView.separatorColor = [UIColor lightGrayColor];
                
                
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.translucent = YES;
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
             doneButton_iOS7.tintColor = [UIColor whiteColor];
            ////////////////////// Light Theme vs Dark Theme  END ////////////////////////////////////////////////
            
            
            
            if (IS_IPHONE_5)
                self.noteTableView.frame = CGRectMake(0, 0, 320, 416 + 88 + 65);
            else
                self.noteTableView.frame = CGRectMake(0, 0, 320, 416 + 65);
            
            
            self.previewView_Main.frame = CGRectMake(0, 64, 320, 568);
            
            
        }
        else{ //iOS 6
            
            self.view.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            self.view.backgroundColor = [UIColor yellowColor];
         
            self.noteTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            self.noteTableView.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0];
            
            //Save Button
            //UIImage *saveButtonImage = [UIImage imageNamed:@"Done.png"];
            UIImage *saveButtonImage = [UIImage imageNamed:@"SaveSmallBtn.png"];
            doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
            [doneButton setBackgroundImage:saveButtonImage forState:UIControlStateNormal];
            [doneButton addTarget:self action:@selector(save_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
            [[self navigationItem] setRightBarButtonItem:barButton];
            [self.navigationController.navigationBar addSubview:doneButton];
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];

        }

        
        if (Flag) {
            self.title =@"Edit Note";
            // [self.navigationController.navigationBar addSubview:saveButton];
        }
        else{
            self.title =@"Add Note";
            // [self.navigationController.navigationBar addSubview:doneButton];
        }
        
        
        
    }
    else{
        
        
        self.navigationController.navigationBarHidden = YES;
        
        //backButton = [[UIButton alloc]init];
        backButton.hidden = NO;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        //Steve added to keep images white on Nav & toolbar
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn: [UIToolbar class], nil] setTintColor:[UIColor whiteColor]];
        
    }
    
    
// ***************** Steve End ***************************

    
	
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
	if(!appdelegate.IsVTTDevice || !appdelegate.isVTTDeviceAuthorized || IS_LITE_VERSION)
        [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon_pen.png"] forState:UIControlStateNormal];
    
    //Steve test
    // self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    titleTextField.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    
	/*if ([self.notesTitleType isEqualToString:@"H"])
     {
     UIImage *titleImage = [[UIImage alloc ] initWithData:self.notesTitleBinary];
     UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(25,55,239,24)];
     [titleImageView setImage:titleImage forState:UIControlStateNormal];
     [titleImageView setImage:titleImage forState:UIControlStateHighlighted];
     [self.view addSubview:titleImageView];
     
     //[starButton setFrame:CGRectMake(300 - staredImage.size.width - 5, 150/2 - staredImage.size.height/2, staredImage.size.width, staredImage.size.height)];
     
     [titleImage release];
     [titleImageView release];
     }*/
    
    
	[self setLabelValues];
	
	
	[self makeImageIcon];
	[self makeSkatchIcon];
	[self makeMapIcon];
	[self makeHwrtIcon];
	[self makeTextIcon];
	[self makeWebIcon];
    
    //Steve added
    //Releases keyboard so user can see other buttons
    UIPanGestureRecognizer  *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
    pan = panGesture;
    [self.view addGestureRecognizer:pan];
    
    //Steve added
    titleTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    titleTextField.returnKeyType = UIReturnKeyDone;
    //titleTextField.textColor = [UIColor blueColor];
    
    [titleTextField setFont:[UIFont systemFontOfSize:16]];
    [titleTextField setTextAlignment:NSTextAlignmentLeft];
    [titleTextField  setBackgroundColor:[UIColor clearColor]];
    titleTextField.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    
	fView.alpha = 1.0;
    TakeTextbtn.hidden = false;
    
}


-(void)getFolderNameForProject
{
    if(prntVAL)
    {
        myArray=[[NSMutableArray alloc]init ];
        y=[self fetch_level_one:prntVAL];
        yy=Pname;
        [myArray addObject:yy];
        if((y)&&(y!=0)) 
        {
            x= [self fetch_level_one:y];
            xx=Pname;
            [myArray addObject:xx];
            if((x)&&(x!=0)) 
            { 
                u=[self fetch_level_one:x];
                uu=Pname;
                [myArray addObject:uu];
                if((u)&&(u!=0)) 
                { 
                    v= [self fetch_level_one:u];
                    vv=Pname;
                    [myArray addObject:vv];
                }
            }
        }
    }
    
     NSString *finalText=[[NSString alloc]init];
    for(int k=[myArray count] - 1;k>=0; k--)
    {
        finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
    }
    
    if ( [finalText length] > 0)
        finalText = [finalText substringToIndex:[finalText length] - 1];
    
      projectNameLabel.text = finalText;
    
      [myArray removeAllObjects];
    
}

 -(void)getFolderName
{
    if(notsDataObj.belongsToProjectID)
    {
        myArray=[[NSMutableArray alloc]init ];
        y=[self fetch_level_one:notsDataObj.belongsToProjectID];
        yy=Pname;
        [myArray addObject:yy];
        if((y)&&(y!=0)) 
        {
            x= [self fetch_level_one:y];
            xx=Pname;
            [myArray addObject:xx];
            if((x)&&(x!=0)) 
            { 
                u=[self fetch_level_one:x];
                uu=Pname;
                [myArray addObject:uu];
                if((u)&&(u!=0)) 
                { 
                    v= [self fetch_level_one:u];
                    vv=Pname;
                    [myArray addObject:vv];
                }
            }
        }
    }
    
    NSString *finalText=[[NSString alloc]init];   
    for(int k=[myArray count] - 1;k>=0; k--)
    {
        finalText = [finalText stringByAppendingString:[NSString stringWithFormat:@"%@/",[myArray objectAtIndex:k]]];
    }
    
    if ( [finalText length] > 0)
        finalText = [finalText substringToIndex:[finalText length] - 1];
    
   // NSLog(@"Final String %@", finalText);
    
    projectNameLabel.text = finalText;
    
    [myArray removeAllObjects];
  
}

-(NSInteger)fetch_level_one:(NSInteger)projid
{
     OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3_stmt *selected_stmt = nil;
    
    if (selected_stmt == nil && appDelegate.database)
    {
        Sql = [[NSString alloc]initWithFormat:@"SELECT  belongstoId,projectName from ProjectMaster where projectID =%d",projid];
        //NSLog(@"%@",Sql);
        if (sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selected_stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(selected_stmt) == SQLITE_ROW) 
            {
                retVal= sqlite3_column_int(selected_stmt, 0);
			    Pname= [NSString stringWithUTF8String:(char *) sqlite3_column_text(selected_stmt,1)];
                //NSLog(@"%d   =  %@",retVal,Pname);
                
            }
            sqlite3_finalize(selected_stmt);
            selected_stmt = nil;
        }
    }
    return retVal;
}

//Steve added for Help views
-(void)viewDidAppear:(BOOL)animated{
    //Steve added
    //If first time running App, show some "Help" Views
    
    /*
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"FirstLaunch_NotesAdd"]) 
    {
        [self.view removeGestureRecognizer:pan]; //removes pan gesture since it conflicts with swipeDownHelpView gesture
        
        tapHelpView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        UISwipeGestureRecognizer  *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(helpView_Tapped:)];
        swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
        swipeDownHelpView = swipeDownGesture;
        
        [helpView addGestureRecognizer:tapHelpView];  
        [helpView addGestureRecognizer:swipeDownHelpView]; 
        helpView.hidden = NO; 
        
        CATransition  *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
        transition.duration = 0.0;
        [helpView.layer addAnimation:transition forKey:kCATransitionFromBottom];
        [self.view addSubview:helpView];
        
        [sharedDefaults setBool:NO forKey:@"FirstLaunch_NotesAdd"];
        [sharedDefaults synchronize];     
    } 
    */
}

//Steve added
//Releases keyboard so user can see other buttons

- (void)panRecognized:(UIPanGestureRecognizer *)gestureRecognizer {
    switch ([gestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
		{
			[titleTextField resignFirstResponder];
		}
            break;
        case UIGestureRecognizerStateEnded:
		{
			[titleTextField resignFirstResponder];
		}
            break;
            
        default:
            break;
    }
	
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [titleTextField resignFirstResponder];
    
}

#pragma mark -
#pragma mark Private Methods
#pragma mark -


-(void)setLabelValues 
{
    // NSLog(@"setLabelValues");
    
	if ([notesTitleType isEqualToString: @"T"] || [notesTitleType isEqualToString: @"V"]) {
		if (notesTitle && (![notesTitle isEqualToString:@""])) {
            [titleTextField setBackground:nil];
            [titleTextField setText:notesTitle];
			//[titleTextField setTextColor:[UIColor blueColor]];
			titleTextField.layer.borderWidth = 0.0;
		}
		//[titleTextField becomeFirstResponder];
	}else if ([notesTitleType isEqualToString: @"H"] && notesTitleBinary) {
		[titleTextField setPlaceholder: @""];
		UIImage *tempTitleImage = [[UIImage alloc] initWithData:notesTitleBinary];
		[titleTextField setBackground:tempTitleImage];
		[titleTextField setText:@""];
		[titleTextField resignFirstResponder];
	}else if (([notesTitleType isEqualToString: @""] || notesTitleType == nil)){
		[titleTextField setPlaceholder: @"Title"];
		notesTitleType = @"T";
		notesTitleBinary = nil;
	}
}



-(IBAction)hideShow:(id)sender
{
    //NSLog(@"hideShow");
	
	if ([sender isSelected]) 
	{
		[sender setSelected:NO];
		
	}
	else 
	{
		[sender setSelected:YES];
	}
	
	if(fView.alpha==0)
	{
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5];
		fView.alpha=1;
		[UIView commitAnimations];
		
	}
	else 
	{
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.5];
		fView.alpha=0;
		[UIView commitAnimations];
	}
    
    fView.alpha=1;

}

-(void)showData
{
    //NSLog(@"showData");
    
	//NSLog(@"Total Number Skt ==>%d",[tempSktArray count]);
	//NSLog(@"Total Number  Img ==>%d",[tempImgArray count]);
	//NSLog(@"Total Number map ==>%d",[tempMapArray count]);
	//NSLog(@"Total Number tempWebImgTArray  ==>%d",[tempWebImgTArray count]);
	//NSLog(@"Total Number TEXT ==>%d",[temptxtArray count]);
	//NSLog(@"Total Number TEXT ==>%d",[tempHWRTArray count]);
}

-(void)showDataEdit
{	
	//NSLog(@"showDataEdit");
    
	for (int i = 0; i < [notsDataObj.notesTitleArray  count]; i++)
	{
		if([[[notsDataObj.notesTitleArray objectAtIndex:i]notesTitleType]isEqualToString:@"H"])
		{
			[tempTitleArray addObject:[[notsDataObj.notesTitleArray objectAtIndex:i]notesTitleBinary]];
			[tempTitlTypeeArray addObject:[[notsDataObj.notesTitleArray objectAtIndex:i]notesTitleType]];
		}
		else
		{
			[tempTitleArray addObject:[[notsDataObj.notesTitleArray objectAtIndex:i]notesTitle]];
			[tempTitlTypeeArray addObject:[[notsDataObj.notesTitleArray objectAtIndex:i]notesTitleType]];
		}
	}
	
	
	
	for (int i = 0; i < [notsDataObj.notessktchimageArrsy count]; i++) 
	{
		[tempSktArray addObject:[[notsDataObj.notessktchimageArrsy objectAtIndex:i]sktinfo]];
	}
	
	for (int i = 0; i < [notsDataObj.notesPicDataArray count]; i++) 
	{
		[tempImgArray addObject:[[notsDataObj.notesPicDataArray objectAtIndex:i]imginfo]];
	}
	
	for (int i = 0; i < [notsDataObj.notesHwrtDataArray count]; i++) 
	{
		[tempHWRTArray addObject:[[notsDataObj.notesHwrtDataArray objectAtIndex:i]hwrtinfo]];
	}
	
	for (int i = 0; i < [notsDataObj.notesTextArray count]; i++) {
		[temptxtArray addObject:[[notsDataObj.notesTextArray objectAtIndex:i]textinfo]];
	}
	

	
	//NSLog(@"Total Number Skt ==>%d",  [tempSktArray count]);
	//NSLog(@"Total Number  Img ==>%d", [tempImgArray count]);
	//NSLog(@"Total Number map ==>%d",  [tempMapArray count]);
	//NSLog(@"Total Number tempWebImgTArray ==>%d",  [tempWebImgTArray count]);
	//NSLog(@"Total Number TEXT ==>%d", [temptxtArray count]);
	//NSLog(@"Total Number TEXT ==>%d", [tempHWRTArray count]);
}


//********* Show Picture Preview ********************
//-(void)ShpwImg:(id)sender
-(void)ShpwImg:(NSInteger)tagSent
{
    //NSLog(@"ShpwImg");
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.previewView_Main.userInteractionEnabled = YES;
    self.previewView_Main.tag = KPicturePreviewTag;
    
    
    //************** Setup & Show View **********************
    self.previewView.hidden = NO;
    self.previewView.layer.cornerRadius = 5;
    self.previewView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.previewView.layer.shadowOpacity = 0.5;
    self.previewView.layer.shadowRadius = 5.0;
    self.previewView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    self.previewView.alpha = 0.0;
    [self.view addSubview:self.previewView_Main];
    
    //************** Show Picture **********************
    pictureImage = [[UIImage alloc]initWithData:[tempImgArray objectAtIndex:tagSent]];
    //pictureImage.
    
	UIButton *pictureButton = [[UIButton alloc] initWithFrame:CGRectMake(30,50,220,220)];
	[pictureButton setBackgroundImage:pictureImage forState:UIControlStateNormal];
	[self.view bringSubviewToFront:pictureButton];
    
	[pictureButton setBackgroundImage:pictureImage forState:UIControlStateHighlighted];
	[pictureButton.layer setBorderWidth:1.0];
	[pictureButton.layer setCornerRadius:5.0];
	pictureButton.layer.masksToBounds = YES;
	[pictureButton.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
    pictureButton.backgroundColor = [UIColor clearColor];
    
    //[pictureButton addTarget:self action:@selector(showFullImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:pictureButton];
    
    
    //************** Label "X" - closes view **********************
    self.xLabel.layer.cornerRadius = 5;
    
    
    //************** Invisible Button around "X"  **********************
    //Invisible Button around "X" Close lablel - to remove view
    UIButton *xLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.xLabel.frame.origin.x, self.xLabel.frame.origin.y, self.xLabel.frame.size.width, self.xLabel.frame.size.height)];
    [xLabelInvisibleButton setUserInteractionEnabled:YES];
    [xLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [xLabelInvisibleButton addTarget:self action:@selector(closeTextPreview) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:xLabelInvisibleButton];
    
    
    //************** Edit Label  **********************
    //On NIB
    self.editLabelPreviewView.hidden = YES;
    
    
    //************** Invisible Button Edit Label  **********************
    UIButton *EditLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.editLabelPreviewView.frame.origin.x, self.editLabelPreviewView.frame.origin.y, self.editLabelPreviewView.frame.size.width, self.editLabelPreviewView.frame.size.height)];
    [EditLabelInvisibleButton setUserInteractionEnabled:YES];
    [EditLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    //[EditLabelInvisibleButton addTarget:self action:@selector(showFullImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:EditLabelInvisibleButton];
    
    
    //************** Delete Label  **********************
    //On NIB
    
    
    //************** Invisible Button Delete Label  **********************
    UIButton *deleteLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.deleteLabelPreviewView.frame.origin.x, self.deleteLabelPreviewView.frame.origin.y, self.deleteLabelPreviewView.frame.size.width, self.deleteLabelPreviewView.frame.size.height)];
    [deleteLabelInvisibleButton setUserInteractionEnabled:YES];
    [deleteLabelInvisibleButton setTag:tagSent];
    [deleteLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [deleteLabelInvisibleButton addTarget:self action:@selector(aImg:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:deleteLabelInvisibleButton];
    
    
    //************** Fade In Animation  **********************
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.previewView.alpha = 1.0;
    [UIView commitAnimations];
    
}


//********* Show Sketch Preview ********************
//-(void)ShowSkt:(id)sender
-(void)ShowSkt:(NSInteger)tagSent
{
   // NSLog(@"ShowSkt");
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.previewView_Main.userInteractionEnabled = YES;
    self.previewView_Main.tag = KSketchPreviewTag;
    
    
    //************** Setup & Show View **********************
    self.previewView.hidden = NO;
    self.previewView.layer.cornerRadius = 5;
    self.previewView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.previewView.layer.shadowOpacity = 0.5;
    self.previewView.layer.shadowRadius = 5.0;
    self.previewView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    self.previewView.alpha = 0.0;
    [self.view addSubview:self.previewView_Main];
    
    //************** Show Sketch **********************
    UIImage *sketchImage = [[UIImage alloc]initWithData:[tempSktArray objectAtIndex:tagSent]];
    
	UIButton *sketchButton = [[UIButton alloc] initWithFrame:CGRectMake(30,50,220,220)];
	[sketchButton setBackgroundImage:sketchImage forState:UIControlStateNormal];
	[self.view bringSubviewToFront:sketchButton];
    
	[sketchButton setBackgroundImage:sketchImage forState:UIControlStateHighlighted];
	[sketchButton.layer setBorderWidth:1.0];
	[sketchButton.layer setCornerRadius:5.0];
	sketchButton.layer.masksToBounds = YES;
	[sketchButton.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
    
    //[sketchButton addTarget:self action:@selector(notesEditText_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:sketchButton];
    
    
    //************** Label "X" - closes view **********************
    self.xLabel.layer.cornerRadius = 5;
    
    
    //************** Invisible Button around "X"  **********************
    //Invisible Button around "X" Close lablel - to remove view
    UIButton *xLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.xLabel.frame.origin.x, self.xLabel.frame.origin.y, self.xLabel.frame.size.width, self.xLabel.frame.size.height)];
    [xLabelInvisibleButton setUserInteractionEnabled:YES];
    [xLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [xLabelInvisibleButton addTarget:self action:@selector(closeTextPreview) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:xLabelInvisibleButton];
    
    
    //************** Edit Label  **********************
    //On NIB
    self.editLabelPreviewView.hidden = YES;
    
    
    //************** Invisible Button Edit Label  **********************
    UIButton *EditLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.editLabelPreviewView.frame.origin.x, self.editLabelPreviewView.frame.origin.y, self.editLabelPreviewView.frame.size.width, self.editLabelPreviewView.frame.size.height)];
    [EditLabelInvisibleButton setUserInteractionEnabled:YES];
    [EditLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [EditLabelInvisibleButton addTarget:self action:@selector(notesEditText_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:EditLabelInvisibleButton];
    
    
    //************** Delete Label  **********************
    //On NIB
    
    
    //************** Invisible Button Delete Label  **********************
    UIButton *deleteLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.deleteLabelPreviewView.frame.origin.x, self.deleteLabelPreviewView.frame.origin.y, self.deleteLabelPreviewView.frame.size.width, self.deleteLabelPreviewView.frame.size.height)];
    [deleteLabelInvisibleButton setUserInteractionEnabled:YES];
    [deleteLabelInvisibleButton setTag:tagSent];
    [deleteLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [deleteLabelInvisibleButton addTarget:self action:@selector(aSKT:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:deleteLabelInvisibleButton];
    
    
    //************** Fade In Animation  **********************
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.previewView.alpha = 1.0;
    [UIView commitAnimations];

    }


-(void)ShowMAP:(id)sender
{
	 //NSLog(@"ShowMAP");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
	
	UIImage *btnMainimg=[UIImage imageNamed:@"PlaceHolderPopOver"];
	UIButton *buttonMain = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonMain setBackgroundImage:btnMainimg forState:UIControlStateNormal];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        buttonMain.frame = CGRectMake(15.50,160.0 - 44,289.0,280.0);
	else
        buttonMain.frame = CGRectMake(15.50,160.0,289.0,280.0);

    
	[self.view addSubview:buttonMain]; 	
	[self.view bringSubviewToFront:buttonMain];
	
	
	UIImage *titleImage = [[UIImage alloc]initWithData:[tempMapArray objectAtIndex:[sender tag]]];
	UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(1,29.0,286.0,249.0)];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
	[self.view bringSubviewToFront:titleImageView];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
	[titleImageView.layer setBorderWidth:1.0];
	[titleImageView.layer setCornerRadius:5.0];
	titleImageView.layer.masksToBounds = YES;
	[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	buttonMain.tag = 799;
	
	
	
	UIImage *btn1img=[UIImage imageNamed:@"1315208771_back.png"];
	UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[button1 addTarget:self 
				action:@selector(bMAP:)
	  forControlEvents:UIControlEventTouchDown];
	[button1 setBackgroundImage:btn1img forState:UIControlStateNormal];
	button1.frame = CGRectMake(05.0, 5.0, 20.0, 20.0);
    [buttonMain addSubview:button1];
	
	
	UIImage *btnimg=[UIImage imageNamed:@"1315209445_Gnome-Edit-Delete-32.png"];
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	[button addTarget:self 
			   action:@selector(aMAP:)
	 forControlEvents:UIControlEventTouchDown];
	[button setBackgroundImage:btnimg forState:UIControlStateNormal];
	button.tag = [sender tag]; //Steve added back - needed to delete the correct image/text/sketch
	button.frame = CGRectMake(265.0, 5.0, 20.0, 20.0);
    [buttonMain addSubview:button];
	
	[buttonMain addSubview:titleImageView];

}


//********* Show Sticky Note Preview ********************
//-(void)ShowHWT:(id)sender
-(void)ShowHWT:(NSInteger)tagSent
{
	//NSLog(@"ShowHWT");
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.previewView_Main.userInteractionEnabled = YES;
    self.previewView_Main.tag = KStickyPreviewTag;
    
    
    //************** Setup & Show View **********************
    self.previewView.hidden = NO;
    self.previewView.layer.cornerRadius = 5;
    self.previewView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.previewView.layer.shadowOpacity = 0.5;
    self.previewView.layer.shadowRadius = 5.0;
    self.previewView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    self.previewView.alpha = 0.0;
    [self.view addSubview:self.previewView_Main];
    
    //************** Show Sticky **********************
    UIImage *stickyImage = [[UIImage alloc]initWithData:[tempHWRTArray objectAtIndex:tagSent]];
    
	UIButton *stickyButton = [[UIButton alloc] initWithFrame:CGRectMake(30,50,220,220)];
	[stickyButton setBackgroundImage:stickyImage forState:UIControlStateNormal];
	[self.view bringSubviewToFront:stickyButton];
    
	[stickyButton setBackgroundImage:stickyImage forState:UIControlStateHighlighted];
	[stickyButton.layer setBorderWidth:1.0];
	[stickyButton.layer setCornerRadius:5.0];
	stickyButton.layer.masksToBounds = YES;
	[stickyButton.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
    [self.previewView addSubview:stickyButton];
    
       
    //************** Label "X" - closes view **********************
    self.xLabel.layer.cornerRadius = 5;
    
    
    //************** Invisible Button around "X"  **********************
    //Invisible Button around "X" Close lablel - to remove view
    UIButton *xLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.xLabel.frame.origin.x, self.xLabel.frame.origin.y, self.xLabel.frame.size.width, self.xLabel.frame.size.height)];
    [xLabelInvisibleButton setUserInteractionEnabled:YES];
    [xLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [xLabelInvisibleButton addTarget:self action:@selector(closeTextPreview) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:xLabelInvisibleButton];
    
    
    //************** Edit Label  **********************
    //On NIB
     self.editLabelPreviewView.hidden = YES;
    
    
    //************** Invisible Button Edit Label  **********************
    UIButton *EditLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.editLabelPreviewView.frame.origin.x, self.editLabelPreviewView.frame.origin.y, self.editLabelPreviewView.frame.size.width, self.editLabelPreviewView.frame.size.height)];
    [EditLabelInvisibleButton setUserInteractionEnabled:YES];
    [EditLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    //[EditLabelInvisibleButton addTarget:self action:@selector(notesEditText_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:EditLabelInvisibleButton];
    
    
    //************** Delete Label  **********************
    //On NIB
    
    
    //************** Invisible Button Delete Label  **********************
    UIButton *deleteLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.deleteLabelPreviewView.frame.origin.x, self.deleteLabelPreviewView.frame.origin.y, self.deleteLabelPreviewView.frame.size.width, self.deleteLabelPreviewView.frame.size.height)];
    [deleteLabelInvisibleButton setUserInteractionEnabled:YES];
    [deleteLabelInvisibleButton setTag:tagSent];
    [deleteLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [deleteLabelInvisibleButton addTarget:self action:@selector(aHWT:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:deleteLabelInvisibleButton];
    
    
    //************** Fade In Animation  **********************
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.previewView.alpha = 1.0;
    [UIView commitAnimations];

    }


//-(void)ShowTEXT:(id)sender
-(void)ShowTEXT:(NSInteger)tagSent
{
    //NSLog(@"ShowTEXT");
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.previewView_Main.userInteractionEnabled = YES;
    self.previewView_Main.tag = KTextPreviewTag;
    
    
    //************** Setup & Show View **********************
    self.previewView.hidden = NO;
    self.previewView.layer.cornerRadius = 5;
    self.previewView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.previewView.layer.shadowOpacity = 0.5;
    self.previewView.layer.shadowRadius = 5.0;
    self.previewView.layer.shadowOffset = CGSizeMake(-2.0f, 2.0f);
    self.previewView.alpha = 0.0;
    [self.view addSubview:self.previewView_Main];
    
    //************** Show Text **********************
    UITextView *detailLabel = [[UITextView alloc] initWithFrame:CGRectMake(30,50,220,220)];
	detailLabel.layer.borderWidth = 1;
	detailLabel.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9] CGColor];
	detailLabel.layer.cornerRadius = 10;
	detailLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	//NSString *cellValue =[temptxtArray objectAtIndex:[sender tag]];
    NSString *cellValue =[temptxtArray objectAtIndex:tagSent];
	detailLabel.text=cellValue;
	detailLabel.editable=NO;
    detailLabel.alpha = 1.0;
    [self.previewView addSubview:detailLabel];
    
    
    //************** Invisible Button around Text Label **********************
    //Invisible Button around Text Label to Edit the Note
    UIButton *detailLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(detailLabel.frame.origin.x, detailLabel.frame.origin.y, detailLabel.frame.size.width, detailLabel.frame.size.height)];
    [detailLabelInvisibleButton setUserInteractionEnabled:YES];
    //[detailLabelInvisibleButton setTag:[sender tag]];
    [detailLabelInvisibleButton setTag:tagSent];
    [detailLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [detailLabelInvisibleButton addTarget:self action:@selector(notesEditText_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:detailLabelInvisibleButton];
    
    //************** Label "X" - closes view **********************
    self.xLabel.layer.cornerRadius = 5;
    
    
    //************** Invisible Button around "X"  **********************
    //Invisible Button around "X" Close lablel - to remove view
    UIButton *xLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.xLabel.frame.origin.x, self.xLabel.frame.origin.y, self.xLabel.frame.size.width, self.xLabel.frame.size.height)];
    [xLabelInvisibleButton setUserInteractionEnabled:YES];
    [xLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [xLabelInvisibleButton addTarget:self action:@selector(closeTextPreview) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:xLabelInvisibleButton];
    
    
    //************** Edit Label  **********************
    //On NIB
     self.editLabelPreviewView.hidden = NO;

    
    //************** Invisible Button Edit Label  **********************
    UIButton *EditLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.editLabelPreviewView.frame.origin.x, self.editLabelPreviewView.frame.origin.y, self.editLabelPreviewView.frame.size.width, self.editLabelPreviewView.frame.size.height)];
    [EditLabelInvisibleButton setUserInteractionEnabled:YES];
    [EditLabelInvisibleButton setTag:tagSent];
    [EditLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [EditLabelInvisibleButton addTarget:self action:@selector(notesEditText_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:EditLabelInvisibleButton];
    
    
    //************** Delete Label  **********************
    //On NIB
    
    
    //************** Invisible Button Delete Label  **********************
    UIButton *deleteLabelInvisibleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.deleteLabelPreviewView.frame.origin.x, self.deleteLabelPreviewView.frame.origin.y, self.deleteLabelPreviewView.frame.size.width, self.deleteLabelPreviewView.frame.size.height)];
    [deleteLabelInvisibleButton setUserInteractionEnabled:YES];
    [deleteLabelInvisibleButton setTag:tagSent];
    [deleteLabelInvisibleButton setBackgroundColor:[UIColor clearColor]];
    [deleteLabelInvisibleButton addTarget:self action:@selector(aTEXT:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewView addSubview:deleteLabelInvisibleButton];
    
    
    //************** Fade In Animation  **********************
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.previewView.alpha = 1.0;
    [UIView commitAnimations];

    
   }


-(void)ShowMyWEB:(id)sender
{
	//NSLog(@"ShowMyWEB");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
	
	UIImage *btnMainimg=[UIImage imageNamed:@"PlaceHolderPopOver"];
	UIButton *buttonMain = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonMain setBackgroundImage:btnMainimg forState:UIControlStateNormal];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
        buttonMain.frame = CGRectMake(15.50,160.0 - 44,289.0,280.0);
	else
        buttonMain.frame = CGRectMake(15.50,160.0,289.0,280.0);
    
    
	[self.view addSubview:buttonMain]; 	
	[self.view bringSubviewToFront:buttonMain];
	
	
	UIImage *titleImage = [[UIImage alloc]initWithData:[tempWebImgTArray objectAtIndex:[sender tag]]];
	UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(1,29.0,286.0,249.0)];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
	[self.view bringSubviewToFront:titleImageView];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
	[titleImageView.layer setBorderWidth:1.0];
	[titleImageView.layer setCornerRadius:5.0];
	titleImageView.layer.masksToBounds = YES;
	[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	buttonMain.tag=1009;
	
	
	UIImage *btn1img=[UIImage imageNamed:@"1315208771_back.png"];
	UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[button1 addTarget:self 
				action:@selector(bWEB:)
	  forControlEvents:UIControlEventTouchDown];
	[button1 setBackgroundImage:btn1img forState:UIControlStateNormal];
	button1.frame = CGRectMake(05.0, 5.0, 20.0, 20.0);
    [buttonMain addSubview:button1];
	
	
	UIImage *btnimg=[UIImage imageNamed:@"1315209445_Gnome-Edit-Delete-32.png"];
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	[button addTarget:self 
			   action:@selector(aWEB:)
	 forControlEvents:UIControlEventTouchDown];
	[button setBackgroundImage:btnimg forState:UIControlStateNormal];
	button.tag = [sender tag]; //Steve added back - needed to delete the correct image/text/sketch
	button.frame = CGRectMake(265.0, 5.0, 20.0, 20.0);
    [buttonMain addSubview:button];
	
	[buttonMain bringSubviewToFront:titleImageView];
	[buttonMain addSubview:titleImageView];
	
}



-(void)closeTextPreview{
    
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [self.previewView_Main removeFromSuperview];
  
}


-(void)aImg:(id)sender
{
	//NSLog(@"aImg");
    //NSLog(@"sender.tag = %d",[sender tag]);
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to Delete this Picture?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = KPictureAlert;
    [alert show];
    
    senderTagInteger = [sender tag];
    
}

-(void)bImg:(id)sender
{
    //NSLog(@"bImg");
    
	[[self.view viewWithTag:KPicturePreviewTag] removeFromSuperview];
	[self makeImageIcon];
}
-(void)aSKT:(id)sender
{
    //NSLog(@"aSKT");
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to Delete this Sketch?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = KSketchAlert;
    [alert show];
    
    senderTagInteger = [sender tag];
	
}

-(void)bSKT:(id)sender
{
    //NSLog(@"bSKT");
    
    
	[[self.view viewWithTag:KSketchPreviewTag] removeFromSuperview];
	[self makeSkatchIcon];
}

-(void)aMAP:(id)sender
{
    ///NSLog(@"aMAP");
    
	[tempMapArray removeObjectAtIndex:[sender tag]];
	[[self.view viewWithTag:799] removeFromSuperview];
	[self makeMapIcon];
	
}

-(void)bMAP:(id)sender
{
     //NSLog(@"bMAP");
    
	[[self.view viewWithTag:799] removeFromSuperview];
	[self makeMapIcon];
	
}

-(void)aHWT:(id)sender
{
     //NSLog(@"aHWT");
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to Delete this Sticky Note?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = KStickyAlert;
    [alert show];
    
    senderTagInteger = [sender tag];
	
}

-(void)bHWT:(id)sender
{
    //NSLog(@"bHWT");
    
	[[self.view viewWithTag:KStickyPreviewTag] removeFromSuperview];
	[self makeHwrtIcon];
	
}

-(void)aTEXT:(id)sender
{
    //NSLog(@"aTEXT");
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to Delete this text?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = KTextAlert;
    [alert show];
    
    senderTagInteger = [sender tag];
    
}


-(void)bTEXT:(id)sender
{
    //NSLog(@"bTEXT");
    
	[[self.view viewWithTag:KTextPreviewTag] removeFromSuperview];
	[self makeTextIcon];
}


-(void)aWEB:(id)sender
{
	
	[tempWebImgTArray removeObjectAtIndex:[sender tag]];
	[[self.view viewWithTag:1009] removeFromSuperview];
	[self makeWebIcon];
    
}

-(void)bWEB:(id)sender
{
	[[self.view viewWithTag:1009] removeFromSuperview];
	[self makeWebIcon];
}


//**************** Handle Alert For Deleting items (Text, Sticky, Pic, Sketch) **********************

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == KTextAlert) {
        
        if(buttonIndex==1){
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            [temptxtArray removeObjectAtIndex:senderTagInteger];
            [[self.view viewWithTag:KTextPreviewTag] removeFromSuperview];
            [self makeTextIcon];
        }

    }
    else if (alertView.tag == KStickyAlert){
        
        if (buttonIndex == 1) {
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            [tempHWRTArray removeObjectAtIndex:senderTagInteger];
            [[self.view viewWithTag:KStickyPreviewTag] removeFromSuperview];
            [self makeHwrtIcon];
        }
    }
    else if (alertView.tag == KPictureAlert){
        
        if(buttonIndex==1){
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            [tempImgArray removeObjectAtIndex:senderTagInteger];
            [[self.view viewWithTag:KPicturePreviewTag] removeFromSuperview];
            [self makeImageIcon];
            
        }
        
    }
    else if (alertView.tag == KSketchAlert){
        
        if(buttonIndex==1){
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            [tempSktArray removeObjectAtIndex:senderTagInteger];
            [[self.view viewWithTag:KSketchPreviewTag] removeFromSuperview];
            [self makeSkatchIcon];
            
        }
        
    }
    

}



-(void)makeImageIcon
{
    //NSLog(@"makeImageIcon");
    
	for (UIButton *tempBtn in [self.TakePicbtn subviews]) 
	{
        if([tempBtn isKindOfClass:[UIButton class]])
		[tempBtn removeFromSuperview];
	}
    
    //Steve - Clears array so it can be updated - needed when adding and deleting items
    if (pictureArrayOfItems.count > 0) {
        [pictureArrayOfItems removeAllObjects];
    }
    
	
	int countimg=[tempImgArray  count];
	int a;
	int b=0;
	
	if(countimg>0)
	{
		for(a=0;a<countimg;a++)
		{
			
			
			UIImage *titleImage = [[UIImage alloc]initWithData:[tempImgArray objectAtIndex:a]];
            
            ListItem *item = [[ListItem alloc] initWithFrame:CGRectZero image:titleImage text:@"Picture"];
            item.tag = a;
            
            //NSLog(@" titleImage Picture = %@", titleImage);
            
            [pictureArrayOfItems addObject:item];
            
            /*
			UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(100.0+b, 05.0,32.0, 28.0)];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
			[titleImageView.layer setBorderWidth:1.0];
			[titleImageView.layer setCornerRadius:5.0];
			titleImageView.layer.masksToBounds = YES;
			[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			[self.TakePicbtn addSubview:titleImageView];
			titleImageView.tag=a;
			
			[titleImageView addTarget:self action:@selector(ShpwImg:) forControlEvents:UIControlEventTouchUpInside];
			
			b=b+38;
			
			if(a==3)
				break;
			*/
		}
		
	}
    
    //NSLog(@"pictureArrayOfItems.count =  %d", pictureArrayOfItems.count);
    //NSLog(@"tempImgArray.count =  %d", tempImgArray.count);
    
    [self.noteTableView reloadData];
}


-(void)makeSkatchIcon
{
    //NSLog(@"makeSkatchIcon");
    
	
	for (UIButton *tempBtn in [self.Takesktcbtn subviews]) {
        if([tempBtn isKindOfClass:[UIButton class]])
		[tempBtn removeFromSuperview];
	}
    
    //Steve - Clears array so it can be updated - needed when adding and deleting items
    if (sketchArrayOfItems.count > 0) {
        [sketchArrayOfItems removeAllObjects];
    }

    
	
	int countSkt=[tempSktArray  count];
	int k;
	int l=0;
	if(countSkt>0)
	{
		for(k=0;k<countSkt;k++)
		{
			
			UIImage *titleImage = [[UIImage alloc]initWithData:[tempSktArray objectAtIndex:k]];
            
            ListItem *item = [[ListItem alloc] initWithFrame:CGRectZero image:titleImage text:@"Sketch"];
            item.tag = k;
            
            //NSLog(@" titleImage Picture = %@", titleImage);
            
            [sketchArrayOfItems addObject:item];
            
            
            /*
			UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(100.0+l, 05.0,32.0, 28.0)];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
			[titleImageView.layer setBorderWidth:1.0];
			[titleImageView.layer setCornerRadius:5.0];
			titleImageView.layer.masksToBounds = YES;
			[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			titleImageView.tag=k;
			
			[titleImageView addTarget:self action:@selector(ShowSkt:) forControlEvents:UIControlEventTouchUpInside];
			
			[self.Takesktcbtn addSubview:titleImageView];
            //Steve commented out for ARC
			//[titleImage release];
			//[titleImageView release];
			l=l+38;
			if(k==3)
				break;
             */
		}
		
	}
	
    [self.noteTableView reloadData];
	
}


-(void)makeMapIcon
{
	for (UIButton *tempBtn in [self.Takemapbtn subviews]) {
        if([tempBtn isKindOfClass:[UIButton class]])
		[tempBtn removeFromSuperview];
	}
	
	int countMAP=[tempMapArray  count];
	int m;
	int n=0;
	if(countMAP>0)
	{
		for(m=0;m<countMAP;m++)
		{
			
			UIImage *titleImage = [[UIImage alloc]initWithData:[tempMapArray objectAtIndex:m]];
			UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(100.0+n, 05.0,32.0, 28.0)];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
			[titleImageView.layer setBorderWidth:1.0];
			[titleImageView.layer setCornerRadius:5.0];
			titleImageView.layer.masksToBounds = YES;
			[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			titleImageView.tag=m;
			
			[titleImageView addTarget:self action:@selector(ShowMAP:) forControlEvents:UIControlEventTouchUpInside];
			
			[self.Takemapbtn addSubview:titleImageView];
            //Steve commented out for ARC
			//[titleImage release];
			//[titleImageView release];
			n=n+38;
			if(m==3)
				break;
			
		}
		
	}
	
	
}


-(void)makeHwrtIcon
{
    //NSLog(@"makeHwrtIcon");
    
	for (UIButton *tempBtn in [self.Takehwrtbtn subviews]) {
        if([tempBtn isKindOfClass:[UIButton class]])
		[tempBtn removeFromSuperview];
	}
    
    //Steve - Clears array so it can be updated - needed when adding and deleting items
    if (stickyArrayOfItems.count > 0) {
        [stickyArrayOfItems removeAllObjects];
    }
	
	int countHWT=[tempHWRTArray  count];
	int h;
	int p=0;
	if(countHWT>0)
	{
		for(h=0;h<countHWT;h++)
		{
			
			UIImage *titleImage = [[UIImage alloc]initWithData:[tempHWRTArray objectAtIndex:h]];
            ListItem *item = [[ListItem alloc] initWithFrame:CGRectZero image:titleImage text:@"Sticky"];
            item.tag = h;
            
            [stickyArrayOfItems addObject:item];
            
            /*
			UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(100.0+p, 05.0,32.0, 28.0)];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
			[titleImageView.layer setBorderWidth:1.0];
			[titleImageView.layer setCornerRadius:5.0];
			titleImageView.layer.masksToBounds = YES;
			[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			titleImageView.tag=h;
			
			[titleImageView addTarget:self action:@selector(ShowHWT:) forControlEvents:UIControlEventTouchUpInside];
			
			[self.Takehwrtbtn addSubview:titleImageView];
        
			p=p+38;
			//if(h==3)
			//	break;
			*/
		}
	}
	
    [self.noteTableView reloadData];
	
}


-(void)makeTextIcon
{
     //NSLog(@"makeTextIcon");
	
	for (UIButton *tempBtn in [self.TakeTextbtn subviews]) {
        
        if([tempBtn isKindOfClass:[UIButton class]]){
            [tempBtn removeFromSuperview];
        }
	}
    
    //Steve - Clears array so it can be updated - needed when adding and deleting items
    if (textArrayofItems.count > 0) {
        [textArrayofItems removeAllObjects];
    }
    
    
	int countTXT=[temptxtArray  count];
	int t;
	int s=0;
	if(countTXT>0)
	{
		for(t=0;t<countTXT;t++)
		{
            
            NSString *text = [temptxtArray objectAtIndex:t];
            
            //Converts the text to an image
            UIGraphicsBeginImageContext(CGSizeMake(110, 110));
            [text drawInRect:CGRectMake(7, 7, 100, 100) withFont:[UIFont systemFontOfSize:12] lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentLeft];
            
            UIImage *titleImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            //labeled as "Text: in ListItem view.
            ListItem *item = [[ListItem alloc] initWithFrame:CGRectZero image:titleImage text:@"Text"];
            item.tag = t;
            
            [textArrayofItems addObject:item];
            
		}
		
	}
	
    [self.noteTableView reloadData];
	
}

-(void)makeWebIcon
{
	
	for (UIButton *tempBtn in [self.Takewebbtn subviews]) {
        if([tempBtn isKindOfClass:[UIButton class]])
		[tempBtn removeFromSuperview];
	}
	
	int countWEB=[tempWebImgTArray  count];
	int w;
	int y=0;
	if(countWEB>0)
	{
		for(w=0;w<countWEB;w++)
		{
			
			UIImage *titleImage = [[UIImage alloc]initWithData:[tempWebImgTArray objectAtIndex:w]];
			UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(100.0+y, 05.0,32.0, 28.0)];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
			[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
			[titleImageView.layer setBorderWidth:1.0];
			[titleImageView.layer setCornerRadius:5.0];
			titleImageView.layer.masksToBounds = YES;
			[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
			titleImageView.tag=w;
			[titleImageView addTarget:self action:@selector(ShowMyWEB:) forControlEvents:UIControlEventTouchUpInside];
			[self.Takewebbtn addSubview:titleImageView];

			y=y+38;
			if(w==3)
				break;
			
		}
		
	}
	
	
}




- (BOOL)textFieldShouldReturn:(UITextField *)theTextField 
{
	[titleTextField resignFirstResponder];
	return YES;
}

-(IBAction)homeButton_Clicked:(id)sender 
{
	[self.navigationController popViewControllerAnimated:YES];
}


-(IBAction) inputTypeSelectorButton_Clicked:(id)sender {
   // NSLog(@"inputTypeSelectorButton_Clicked");
    
	[titleTextField resignFirstResponder];
	
	if (inputTypeSelectorViewController) 
	{
		inputTypeSelectorViewController = nil;
	}
    
	OrganizerAppDelegate *appDelegate = (OrganizerAppDelegate *) [[UIApplication sharedApplication] delegate];
    
	if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized){
        
	inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 97, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(270, 85) andParentVC:self fromMainScreen:NO];
	
	[inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
	[self.view addSubview:inputTypeSelectorViewController.view];
	[self.view bringSubviewToFront:inputTypeSelectorViewController.view];
    
    
}else 
{
    HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
    
    NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
    [stringList setValue:@"Projects" forKey:@"Viewcontroller"];
    [stringList synchronize];
    
   // [appDelegate.navigationController pushViewController:handWritingViewController animated:NO];
    [self.navigationController pushViewController:handWritingViewController animated:NO];
}
}






//For Take a Picture//
-(IBAction)takePictureButton_Clicked:(id)sender
{
	TakePictureViewCantroller *PictureView= [[TakePictureViewCantroller alloc] initWithNibName:@"TakePictureViewCantroller" bundle:nil];	
	[[self navigationController] pushViewController:PictureView animated:YES];
	[PictureView setDelegate:self];
								
}

-(IBAction)photoAlbum_Clicked:(id)sender
{
	//NSLog(@"Take a Picture");
}

-(IBAction)sketch_Clicked:(id)sender
{
    NSString *viewId;
    if (IS_IPAD) {
        viewId = @"SketchNoteViewController_iPad";
    }else{
        viewId = @"SketchNoteViewController";
    }
    SketchNoteViewController * sketchView = [[SketchNoteViewController alloc] initWithNibName:viewId bundle:Nil];
    [[self navigationController] pushViewController:sketchView animated:YES];
    [sketchView setDelegate:self];
	
}




-(IBAction)notesText_Clicked:(id)sender
{
 
    //-(id)initWithDataString:(NSString *)text forObjectAtIndex:(int)index isEdit:(BOOL)isEdit
    
    //NotesTextViewCantroller *notesText =[[NotesTextViewCantroller alloc] initWithDataString:@"None" forObjectAtIndex:0 isEdit:NO];
 
    NotesTextViewCantroller *notesText =[[NotesTextViewCantroller alloc] init];
    
    notesText.noteTextDelegate = self;
    
    [self.navigationController pushViewController:notesText animated:YES];
    
    //Steve commented
    /*
    NSString *textString = @"None";

    NotesTextViewCantroller *notesText =[[NotesTextViewCantroller alloc] initWithDataString:textString forObjectAtIndex:nil isEdit:NO];
    
    [notesText setDelegate:self];
    
	[[self navigationController] pushViewController:notesText animated:YES];
	
	*/
	
}

//Steve - allows user to Edit text
-(void)notesEditText_Clicked:(id)sender {
 
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [self.previewView_Main removeFromSuperview];
    
    int indexForNotesTextArray = [sender tag];
    NSString *textString = temptxtArray[indexForNotesTextArray];
    
    NotesTextViewCantroller *notesText =[[NotesTextViewCantroller alloc] initWithDataString:textString forObjectAtIndex:indexForNotesTextArray isEdit:YES];
    
	[[self navigationController] pushViewController:notesText animated:YES];
	
	//[notesText setDelegate:self];
    notesText.noteTextDelegate = self;
    
    [mainButtonImage removeFromSuperview];
}

-(IBAction)notesHwrt_clicked:(id)sender
{
	
	HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];	
	handWritingViewController.whatVAL=YES;
    handWritingViewController.isSettingNotesHWObject = YES;

    [self.navigationController pushViewController:handWritingViewController animated:NO];
    
}

- (IBAction)helpView_Tapped:(id)sender
{
    //NSLog(@"helpView_Tapped");
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"])
    {
        self.navigationController.navigationBarHidden = NO;
    }
    
    [helpView removeGestureRecognizer:swipeDownHelpView]; //remove this since done with it
    [self.view addGestureRecognizer:pan];  //Add back pan gesture
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    transition.duration = 0.5;
    [helpView.layer addAnimation:transition forKey:kCATransitionFromTop];
    
    helpView.hidden = YES;
}


#pragma mark Save Button
/* save button action By  Naresh chouhan    */

-(IBAction) save_buttonClicked:(id)sender
{
    
    if ([self.notesTitleType isEqualToString:@"H"]) {
		if (self.notesTitleBinary == nil) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a Note." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			return;
		}
	}else {
		if (titleTextField.text == nil || [titleTextField.text isEqualToString:@""] || [titleTextField.text isEqualToString:@"Title"] ||([[titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]<=0)) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a Note." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			return;
		}
		if (self.notesTitleType == nil || [self.notesTitleType isEqualToString:@""]) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save a Note." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			return;
		}
	}
 		
	NotesDataObject *NotesObj =[[NotesDataObject alloc]init];
	[NotesObj setNotesTitle:titleTextField.text];
    
	//NSLog(@"Note_>>%@",self.notesTitleType);
    if(![self.notesTitleType isEqualToString:@"H"] )
	{
		
		[NotesObj setNotesTitleType:@"T"];
	}
	else
	{
		[NotesObj setNotesTitleType:self.notesTitleType];
	}
    
     NSLog(@"%@",self.notesTitleBinary);
    NSLog(@"%@",self.notesTitleBinaryLarge);
    
    NSLog(@"tempArr = %@ tempImgArr = %@  tempSktArray = %@ tempWebImgTArray = %@ temptxtArray = %@ tempHWRTArray = %@ tempBookMarksArray = %@",tempMapArray,tempImgArray,tempSktArray,tempWebImgTArray,temptxtArray,tempHWRTArray,tempBookMarksArray);
    
 	[NotesObj setNotesTitleBinary:self.notesTitleBinary];
	[NotesObj setNotesTitleBinaryLarge:self.notesTitleBinaryLarge];
	[NotesObj setNotesMapDataArray:tempMapArray];	
	[NotesObj setNotesPicDataArray:tempImgArray];
	[NotesObj setNotessktchimageArrsy:tempSktArray];
	[NotesObj setNotesWebURArray:tempWebImgTArray];
	[NotesObj setNotesTextArray:temptxtArray];
	[NotesObj setNotesHwrtDataArray:tempHWRTArray];
	[NotesObj setNotesBMDataArray:tempBookMarksArray];
    [NotesObj setBelongsToProjectID:prntVAL];
	
 	
	if(Flag == 1)
	{
 		
		[self DeleteMaster];
		[self DeletePicture];
		[self DeleteSketch];
		[self DeleteMap];
		[self DeleteHandwrite];
		[self Deletetext];
		[self DeleteWeb];
 		
		BOOL isInserted = [AllInsertDataMethods insertNotesDataValues:NotesObj];
		if (isInserted == YES) 
		{

            int count = [self.navigationController.viewControllers count];
             [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
		}	
	}
	else {
		
		BOOL isInserted = [AllInsertDataMethods insertNotesDataValues:NotesObj];
		if (isInserted == YES) 
		{  
            int count = [self.navigationController.viewControllers count];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 2] animated:YES];

		}
 		
	}	
}

-(BOOL)DeleteMaster
{
     //NSLog(@"DeleteMaster");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesMaster = nil;
	
	if(deleteNotesMaster == nil) {
		const char *sql = "delete from NotesMaster where NoteID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesMaster, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesMaster, 1, GetNoteID);
    
    //NSLog(@"GetNoteID = %d",GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesMaster)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesMaster);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesMaster);
		return YES;
	}
	return YES;
}

-(BOOL)DeletePicture
{
	//NSLog(@" DeletePicture");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesPicture = nil;
	
	if(deleteNotesPicture == nil) {
		const char *sql = "delete from NotesImgInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesPicture, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesPicture, 1, GetNoteID);
    
    // NSLog(@"GetNoteID DeletePicture= %d",GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesPicture)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesPicture);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesPicture);
		return YES;
	}
	return YES;	
	
}

-(BOOL)DeleteSketch
{
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesSketch = nil;
	
	if(deleteNotesSketch == nil) {
		const char *sql = "delete from NotesSktInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesSketch, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesSketch, 1, GetNoteID);
    
     //NSLog(@"GetNoteID deleteNotesSketch= %d",GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesSketch)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesSketch);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesSketch);
		return YES;
	}
	return YES;	
 	
}
-(BOOL)DeleteMap
{
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesMap = nil;
	
	if(deleteNotesMap == nil) {
		const char *sql = "delete from NotesMapInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesMap, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesMap, 1, GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesMap)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesMap);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesMap);
		return YES;
	}
	return YES;	
	
}
-(BOOL)DeleteHandwrite
{
	//NSLog(@" DeleteHandwrite");
    
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesHwrt = nil;
	
	if(deleteNotesHwrt == nil) {
		const char *sql = "delete from NotesHwrtInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesHwrt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesHwrt, 1, GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesHwrt)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesHwrt);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesHwrt);
		return YES;
	}
	return YES;	
}
-(BOOL)Deletetext
{
    //NSLog(@"Deletetext");
	
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate]; 
	
	sqlite3_stmt *deleteNotesText = nil;
	
	if(deleteNotesText == nil) {
		const char *sql = "delete from NotesTextInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesText, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesText, 1, GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesText)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesText);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesText);
		return YES;
	}
	return YES;	
}
-(BOOL)DeleteWeb
{
 	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	sqlite3_stmt *deleteNotesWeb = nil;
	
	if(deleteNotesWeb == nil) {
		const char *sql = "delete from NotesWebInfo where NotesID = ?";
		if(sqlite3_prepare_v2(appDelegate.database, sql, -1, &deleteNotesWeb, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(appDelegate.database));
	}
	//When binding parameters, index starts from 1 and not zero.
	sqlite3_bind_int(deleteNotesWeb, 1, GetNoteID);
	
	if (SQLITE_DONE != sqlite3_step(deleteNotesWeb)){
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(appDelegate.database));
		sqlite3_finalize(deleteNotesWeb);
		return NO;
	} else {
		sqlite3_finalize(deleteNotesWeb);
		return YES;
	}
	return YES;	
}

- (IBAction)prjButtonClicked:(id)sender 
{
     ProjectListViewController *projectListViewController = [[ProjectListViewController alloc] initWithNibName:@"ProjectListViewController" bundle:[NSBundle mainBundle] withProjectID:projectNameLabel.tag];
	[projectListViewController setDelegate:self];
	[self.navigationController pushViewController:projectListViewController animated:YES];

}


-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

- (IBAction)DeleteBtnClicked:(id)sender 
{
    //NSLog(@"DeleteBtnClicked");
    
    BOOL isDeleted;
    
    isDeleted = [AllInsertDataMethods deleteNoteWithID:[notsDataObj notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteHandwritingObjectsWithID:[notsDataObj notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteImageObjectsWithID:[notsDataObj notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteTextObjectsWithID:[notsDataObj notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteWebObjectsWithID:[notsDataObj notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteMapObjectsWithID:[notsDataObj notesID]];
    //     isDeleted = [AllInsertDataMethods deleteNoteBMObjectsWithID:[[notesTitleDataArray objectAtIndex:indexPath.row]notesID]];
    isDeleted = [AllInsertDataMethods deleteNoteSketchObjectsWithID:[notsDataObj notesID]];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]) {
        
        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
    }
    else{
        OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate.navigationController popViewControllerAnimated:NO];
        [appDelegate.navigationController popViewControllerAnimated:YES];
    }

}

#pragma mark -
#pragma mark Protocal Methods
#pragma mark -

-(void)setProjectName:(NSString *) projName projID:(NSInteger) projId prentID:(NSInteger)prentID
{
	projectNameLabel.text = projName;
	projectNameLabel.tag = projId;
    prntVAL=projId;
}


//Steve
-(void)setTextToArray:(NSString *) textString forIndexForTextArray:(int) indexForArray isTextBeingEdited:(BOOL) isTextEdited{
    
    if (isTextEdited) {
        
        [temptxtArray replaceObjectAtIndex:indexForArray withObject:textString];
    }
    else{
        
         [temptxtArray addObject:textString];
    }
    
}

#pragma mark -
#pragma mark TableView Methods
#pragma mark -

// Customize the number of sections in the table view.

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 14;
    }
    return 7.0;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 7.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numberSections;
    
    if (Flag == 1) { //Coming from "Edit" Mode
        numberSections = 7;
    }
    else{  //Coming from "Add" Mode
        numberSections = 6;
    }
    
    return numberSections;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (IS_IOS_7) {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{ //iOS 6
        
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
    
    if (indexPath.section == 6 && !IS_IOS_7) {
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
        cell.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    }
    
    
    //Steve - removes the borders & cell background from the deleteButton cell
    if([cell.reuseIdentifier isEqualToString:@"deleteButtonCell"]){
        cell.backgroundView = nil;
        
    }
    
    
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    //NSLog(@" section = %d", section);
    
    
    int numberOfRows;
    
    
    if (section == 0) {
        
        numberOfRows = 1;
    }
    else if (section == 1) {
        
        numberOfRows = 1;
    }
    else if (section == 2) {
        
        if (temptxtArray.count > 0)
            numberOfRows = 2;
        else
            numberOfRows = 1;
    }
    else if (section == 3) {
        
        if (tempHWRTArray.count > 0)
            numberOfRows = 2;
        else
            numberOfRows = 1;
    }
    else if (section == 4) {
        
         if (tempImgArray.count > 0)
             numberOfRows = 2;
         else
             numberOfRows = 1;
    }
    else if (section == 5) {
        
        if (tempSktArray.count > 0)
            numberOfRows = 2;
        else
            numberOfRows = 1;
    }
    else if (section == 6) {
        numberOfRows = 1;
    }
    

    
    return numberOfRows;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    
   // NSLog(@" indexPath = %@", indexPath);
    //NSLog(@" section = %d, row = %d", indexPath.section, indexPath.row);
    
    int height;
    int extraHeight = 90;
    
    if (indexPath.section == 0 && indexPath.row == 0) { //Title
        height = 44;
    }
    else if (indexPath.section == 1 && indexPath.row == 0){ //Project
        height = 44;
    }
    else if (indexPath.section == 2 && indexPath.row ==1 ){ //Text
        
        if (temptxtArray.count > 0)
            height = extraHeight;
        else
            height = 0;
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ //Sticky
        
        if (tempHWRTArray.count > 0)
            height = extraHeight;
        else
            height = 0;
    }
    else if (indexPath.section == 4 && indexPath.row == 1){ //Picture
        
        if (tempImgArray.count > 0)
            height = extraHeight;
        else
            height = 0;
    }
    else if (indexPath.section == 5 && indexPath.row == 1){ //Sketch
        
        if (tempSktArray.count > 0)
            height = extraHeight;
        else
            height = 0;
    }
    else if (Flag == 1 && indexPath.row == 6){ //Delete row
        
        if (Flag == 1)
            height = 60;
        else
            height = 0;
    }
    else{
        
        height = 44;
    }

    
    return height;
    
    //return 40;
	
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    static NSString *cellIdentifier;
    
    if (indexPath.section == 6) {
        cellIdentifier = @"deleteButtonCell";
    }
    else{
       cellIdentifier = @"cell";
    }

    
    UITableViewCell *cell = [self.noteTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    for (UIView  *subViews in [cell.contentView subviews]) {
		[subViews removeFromSuperview];
	}
     

    
    POHorizontalList *list;
    
    //**************** setup Row Data ****************************
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;


    
    int height = 9; //Height of labels
    
    if (indexPath.section == 0) { //Title
        
         [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        titleTextField.frame = CGRectMake(10, height + 2, 240, 24);
        [cell.contentView addSubview:titleTextField];
        
        if(IS_IPAD)
        inputSelecterButton.frame = CGRectMake(420, 5, 30, 30);
        else
            inputSelecterButton.frame = CGRectMake(280, 5, 30, 30);
        
        [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:inputSelecterButton];
        
    }
    else if (indexPath.section == 1) { //Projects
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        projectFixedLabel.frame = CGRectMake(10, height, 110, 24);
        projectFixedLabel.text = @"Project";
        [projectFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        projectFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:projectFixedLabel];
        
        if ([projectNameLabel.text isEqualToString:@""] || projectNameLabel.text == nil) {
            projectNameLabel.text = @"None";
        }
        
        projectNameLabel.frame = CGRectMake(135, height, 152, 24);
        projectNameLabel.backgroundColor = [UIColor clearColor];
        projectNameLabel.textAlignment = NSTextAlignmentRight;
        projectNameLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:projectNameLabel];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0) { //Text Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        
        //Label "Text Note"
        UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 100, 20)];
        textLabel.text = @"Text Note";
        textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        textLabel.textColor = [UIColor blackColor];
        textLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        
        
        //"Add" Label
        UILabel *addLabel = [[UILabel alloc]init];
        
        if (textArrayofItems.count > 0) {
            
            addLabel.text = @"Add Another";
        }
        else{
            addLabel.text = @"Add";
        }
        
        addLabel.frame = CGRectMake(135, height, 152, 24);
        addLabel.backgroundColor = [UIColor clearColor];
        addLabel.textAlignment = NSTextAlignmentRight;
        addLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:addLabel];
        
    }
    
    else if (indexPath.section == 2 && indexPath.row == 1) { //Text Image
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (textArrayofItems.count > 0) {
            
            //View with images of textArrayofItems
            list = [[POHorizontalList alloc] initWithFrame:CGRectMake(0.0, 10.0, 320.0, 80.0) title:nil items:textArrayofItems];
            [list setDelegate:self];
            [cell.contentView addSubview:list];
            
        }

        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //Sticky Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        //Label "Sticky Note"
        UILabel *stickyLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 100, 20)];
        stickyLabel.text = @"Sticky Note";
        stickyLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        stickyLabel.textColor = [UIColor blackColor];
        stickyLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:stickyLabel];
        
        //"Add" Label
        UILabel *addStickyLabel = [[UILabel alloc]init];
        
        if (stickyArrayOfItems.count > 0){
            
            addStickyLabel.text = @"Add Another";
            
        }
        else{
            addStickyLabel.text = @"Add";
        }
        
        addStickyLabel.frame = CGRectMake(135, height, 152, 24);
        addStickyLabel.backgroundColor = [UIColor clearColor];
        addStickyLabel.textAlignment = NSTextAlignmentRight;
        addStickyLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:addStickyLabel];


    }
    
    else if (indexPath.section == 3 && indexPath.row == 1){ //Sticky Image
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (stickyArrayOfItems.count > 0){
            
            //View with images
            list = [[POHorizontalList alloc] initWithFrame:CGRectMake(0.0, 10.0, 320.0, 80.0) title:nil items:stickyArrayOfItems];
            [list setDelegate:self];
            [cell.contentView addSubview:list];

        }

    }
    else if (indexPath.section == 4 && indexPath.row == 0){ //Pictures Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        
        //Label "Text Note"
        UILabel *picturesLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 100, 20)];
        picturesLabel.text = @"Pictures";
        picturesLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        picturesLabel.textColor = [UIColor blackColor];
        picturesLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:picturesLabel];
        
        
        //"Add" Label
        UILabel *addPictureLabel = [[UILabel alloc]init];
        
        if (pictureArrayOfItems.count > 0){
            
            addPictureLabel.text = @"Add Another";
        }
        else{
            addPictureLabel.text = @"Add";
        }
        
        addPictureLabel.frame = CGRectMake(135, height, 152, 24);
        addPictureLabel.backgroundColor = [UIColor clearColor];
        addPictureLabel.textAlignment = NSTextAlignmentRight;
        addPictureLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:addPictureLabel];
        
    }
    
    else if (indexPath.section == 4 && indexPath.row == 1){ //Pictures Image
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        
        //View with images
        list = [[POHorizontalList alloc] initWithFrame:CGRectMake(0.0, 10.0, 320.0, 80.0) title:nil items:pictureArrayOfItems];
        [list setDelegate:self];
        [cell.contentView addSubview:list];
        

    }
    
    else if (indexPath.section == 5 && indexPath.row == 0){  //Sketch Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        
        //Label "Text Note"
        UILabel *sketchLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, height, 100, 20)];
        sketchLabel.text = @"Sketch";
        sketchLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        sketchLabel.textColor = [UIColor blackColor];
        sketchLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:sketchLabel];
        
        
        //"Add" Label
        UILabel *addSketchLabel = [[UILabel alloc]init];
        
        if (sketchArrayOfItems.count > 0){
            
            addSketchLabel.text = @"Add Another";
        }
        else{
            addSketchLabel.text = @"Add";
        }
        
        addSketchLabel.frame = CGRectMake(135, height, 152, 24);
        addSketchLabel.backgroundColor = [UIColor clearColor];
        addSketchLabel.textAlignment = NSTextAlignmentRight;
        addSketchLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:addSketchLabel];

    }
    
    else if (indexPath.section == 5 && indexPath.row == 1){  //Sketch Image
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (sketchArrayOfItems.count > 0){
            
            //View with images
            list = [[POHorizontalList alloc] initWithFrame:CGRectMake(0.0, 10.0, 320.0, 80.0) title:nil items:sketchArrayOfItems];
            [list setDelegate:self];
            [cell.contentView addSubview:list];
        }

     }
    
    else if (indexPath.section == 6) { //Delete Button
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (IS_IOS_7) {
  
            int cellWidth = cell.frame.size.width;
            int cellHeight = cell.frame.size.height;
            int buttonWidth = 150;
            int buttonHeight = 35;
            
            UIButton *deleteButtonCustom = [UIButton buttonWithType:UIButtonTypeSystem];
            
            [deleteButtonCustom setTitle:@"Delete" forState:UIControlStateNormal];
            deleteButtonCustom.tintColor = [UIColor redColor];
            deleteButtonCustom.titleLabel.textAlignment = NSTextAlignmentCenter;
            deleteButtonCustom.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18]; //HelveticaNeue-Bold
            deleteButtonCustom.backgroundColor = [UIColor clearColor];
            deleteButtonCustom.frame =
            CGRectMake(cellWidth/2 - buttonWidth/2, cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
            
            [deleteButtonCustom addTarget:self
                                   action:@selector(DeleteBtnClicked:)
                         forControlEvents:UIControlEventTouchDown];
            
            [cell.contentView addSubview:deleteButtonCustom];

        }
        else{ //iOS 6
    
            UIImage *deleteButtonImage = [UIImage imageNamed:@"DeleteBtn.png"];
            
            int cellWidth = cell.frame.size.width;
            int deleteButtonWidth = deleteButtonImage.size.width;
            
            DeleteBtn.frame = CGRectMake(cellWidth/2 - deleteButtonWidth/2, height - 5, deleteButtonWidth, 41);
            [DeleteBtn setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
            [DeleteBtn addTarget:self action:@selector(DeleteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:DeleteBtn];
            
            
        }
        

    }


    
    if (IS_IOS_7) {
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else{ //iOS 6
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }

    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (indexPath.section == 1 && indexPath.row == 0) { //Projects
        
        [titleTextField resignFirstResponder];
        [self prjButtonClicked:self];
        
    }
	else if (indexPath.section == 2 && indexPath.row == 0){ //Text
        
        [self notesText_Clicked:self];
        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //Sticky Notes
        
        [self notesHwrt_clicked:self];
    }
    else if (indexPath.section == 4 && indexPath.row == 0) { //Pictures
        
        [self takePictureButton_Clicked:self];
        
    }
    else if (indexPath.section == 5 && indexPath.row == 0){ //Sketch
        
        [self sketch_Clicked:self];
        
    }
       
}


- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    

}

#pragma mark  POHorizontalListDelegate

- (void) didSelectItem:(ListItem *)item {
    //NSLog(@"Item Title =  %@", item.imageTitle);
   // NSLog(@"item tag = %d ", item.tag);
    
    if ([item.imageTitle isEqualToString:@"Text"]) {
        [self ShowTEXT:item.tag];
    }
    else if ([item.imageTitle isEqualToString:@"Sticky"]){
        [self ShowHWT:item.tag];
    }
    else if ([item.imageTitle isEqualToString:@"Picture"]){
        [self ShpwImg:item.tag];
    }
    else if ([item.imageTitle isEqualToString:@"Sketch"]){
        [self ShowSkt:item.tag];
    }
    
}



-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:YES];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        [doneButton removeFromSuperview];
        [backButton removeFromSuperview];

    }
    
}


- (void)viewDidUnload 
{
    DeleteBtn = nil;


    navBar = nil;
    helpView = nil;
    tapHelpView = nil;
    backButton = nil;
    doneButton = nil;
    titleButton = nil;
    projectFixedLabel = nil;
    projectButton = nil;
    projectDisclosureButton = nil;
    textFixedLabel = nil;
    textDisclosureButton = nil;
    stickyFixedLabel = nil;
    stickyDisclosureButton = nil;
    pictureFixedLabel = nil;
    pictureDisclosureButton = nil;
    sketchFixedLabel = nil;
    sketchDisclosureButton = nil;
    [self setPreviewView_Main:nil];
    [self setPreviewView:nil];
    [self setXLabel:nil];
    [self setDeleteLabelPreviewView:nil];
    [self setEditLabelPreviewView:nil];
    [super viewDidUnload];
}



@end


























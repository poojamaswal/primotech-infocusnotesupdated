//
//  TabsViewController.m
//  Organizer
//
//  Created by STEVEN ABRAMS on 1/2/14.
//
//

#import "TabsViewController.h"
#import "ToDoParentController.h"
#import "OrganizerAppDelegate.h"

@interface TabsViewController ()

@property(nonatomic, assign) int position_1;
@property(nonatomic, assign) int position_2;
@property(nonatomic, assign) int position_3;
@property(nonatomic, assign) int position_4;
@property(nonatomic, assign) int position_5;
@property(nonatomic, assign) int position_6;
@property(nonatomic, assign) int position_7;
@property(nonatomic, assign) int position_8;
@property(nonatomic, assign) int position_9;
@property(nonatomic, assign) int position_10;

@property(nonatomic, assign) BOOL pos_1_Bool;
@property(nonatomic, assign) BOOL pos_2_Bool;
@property(nonatomic, assign) BOOL pos_3_Bool;
@property(nonatomic, assign) BOOL pos_4_Bool;
@property(nonatomic, assign) BOOL pos_5_Bool;
@property(nonatomic, assign) BOOL pos_6_Bool;
@property(nonatomic, assign) BOOL pos_7_Bool;
@property(nonatomic, assign) BOOL pos_8_Bool;
@property(nonatomic, assign) BOOL pos_9_Bool;
@property(nonatomic, assign) BOOL pos_10_Bool;

@property (strong, nonatomic) IBOutlet UIBarButtonItem  *editButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem  *doneButton;
@property (assign, nonatomic) BOOL                      isEditing;
@property (strong, nonatomic) IBOutlet UITableView      *tableView;
@property (assign, nonatomic) int                       countRowsInEditMode;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;


- (IBAction)doneButton_Clicked:(id)sender;
- (IBAction)editButton_Clicked:(id)sender;

-(void)findCurrentPositionValues;
-(NSString*)findButtonForPosition: (int)buttonInt;

@end



@implementation TabsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"TabsViewController");
    
    if(IS_IPAD)
        self.title = @" ";
    else
        self.title = @"Organize Tabs";
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    //pooja-iPad
    if(IS_IPAD)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = YES;
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
    }
    //self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blueColor]};
    _navBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tableView.backgroundColor = [UIColor clearColor];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (_isEditing) { //Editing Mode
        
        if (indexPath.row == 0 && [sharedDefaults boolForKey:@"BoolPosition_1"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_1"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 0 && ![sharedDefaults boolForKey:@"BoolPosition_1"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_1"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 1 && [sharedDefaults boolForKey:@"BoolPosition_2"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_2"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 1 && ![sharedDefaults boolForKey:@"BoolPosition_2"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_2"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 2 && [sharedDefaults boolForKey:@"BoolPosition_3"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_3"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 2 && ![sharedDefaults boolForKey:@"BoolPosition_3"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_3"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 3 && [sharedDefaults boolForKey:@"BoolPosition_4"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_4"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 3 && ![sharedDefaults boolForKey:@"BoolPosition_4"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_4"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 4 && [sharedDefaults boolForKey:@"BoolPosition_5"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_5"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 4 && ![sharedDefaults boolForKey:@"BoolPosition_5"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_5"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 5 && [sharedDefaults boolForKey:@"BoolPosition_6"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_6"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 5 && ![sharedDefaults boolForKey:@"BoolPosition_6"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_6"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 6 && [sharedDefaults boolForKey:@"BoolPosition_7"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_7"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 6 && ![sharedDefaults boolForKey:@"BoolPosition_7"]) {

            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_7"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 7 && [sharedDefaults boolForKey:@"BoolPosition_8"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_8"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 7 && ![sharedDefaults boolForKey:@"BoolPosition_8"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_8"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 8 && [sharedDefaults boolForKey:@"BoolPosition_9"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_9"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 8 && ![sharedDefaults boolForKey:@"BoolPosition_9"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_9"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 9 && [sharedDefaults boolForKey:@"BoolPosition_10"]) {
            
            cell.hidden = NO;
            _countRowsInEditMode = _countRowsInEditMode + 1;
            
            int tabID = [sharedDefaults integerForKey:@"Position_10"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 9 && ![sharedDefaults boolForKey:@"BoolPosition_10"]) {
            
            cell.hidden = YES;
            
            int tabID = [sharedDefaults integerForKey:@"Position_10"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }

        
    }
    else{ //Regular View
        
        if (indexPath.row == 0 && [sharedDefaults boolForKey:@"BoolPosition_1"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_1"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 0 && ![sharedDefaults boolForKey:@"BoolPosition_1"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_1"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 1 && [sharedDefaults boolForKey:@"BoolPosition_2"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_2"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 1 && ![sharedDefaults boolForKey:@"BoolPosition_2"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_2"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 2 && [sharedDefaults boolForKey:@"BoolPosition_3"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_3"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 2 && ![sharedDefaults boolForKey:@"BoolPosition_3"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_3"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 3 && [sharedDefaults boolForKey:@"BoolPosition_4"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_4"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 3 && ![sharedDefaults boolForKey:@"BoolPosition_4"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_4"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 4 && [sharedDefaults boolForKey:@"BoolPosition_5"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_5"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 4 && ![sharedDefaults boolForKey:@"BoolPosition_5"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_5"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 5 && [sharedDefaults boolForKey:@"BoolPosition_6"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_6"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 5 && ![sharedDefaults boolForKey:@"BoolPosition_6"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_6"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 6 && [sharedDefaults boolForKey:@"BoolPosition_7"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_7"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 6 && ![sharedDefaults boolForKey:@"BoolPosition_7"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_7"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 7 && [sharedDefaults boolForKey:@"BoolPosition_8"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_8"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 7 && ![sharedDefaults boolForKey:@"BoolPosition_8"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_8"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 8 && [sharedDefaults boolForKey:@"BoolPosition_9"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_9"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 8 && ![sharedDefaults boolForKey:@"BoolPosition_9"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_9"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 9 && [sharedDefaults boolForKey:@"BoolPosition_10"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_10"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }
        else if (indexPath.row == 9 && ![sharedDefaults boolForKey:@"BoolPosition_10"]) {
            
            cell.hidden = NO;
            
            cell.accessoryType =UITableViewCellAccessoryNone;
            cell.selectionStyle =UITableViewCellSelectionStyleGray;
            
            int tabID = [sharedDefaults integerForKey:@"Position_10"];
            cell.textLabel.text = [self findButtonForPosition: tabID];
        }

        
    }
    
    
   // return tabID;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self findCurrentPositionValues]; //finds current Position Values (int & Bool)
  
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //value_Index=indexPath.row;
    
    //OrganizerAppDelegate *app_Delegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication]delegate];
  //  UISplitViewController *splitVC=[[UISplitViewController alloc]init];
  // splitVC=app_Delegate.splitVCToDo;
    
   // UINavigationController *nav=[[UINavigationController alloc]init];
    
  //  ToDoParentController *toDoParentView=[[ToDoParentController alloc]initWithNibName:@"ToDoParentControllerIPad" bundle:[NSBundle mainBundle]];
  //  toDoParentView.selectedButtonIndex=value_Index;
   // nav.viewControllers=@[toDoParentView];
    
 //   [self.navigationController pushViewController:toDoParentView animated:NO];
    
  //  [splitVC showDetailViewController:nav sender:NULL];
    //splitVC.viewControllers=@[[splitVC.viewControllers objectAtIndex:0] ,nav];
    
    
    if (indexPath.row == 0 && [sharedDefaults boolForKey:@"BoolPosition_1"])
    { //Row 0
        
        
        if (!_pos_2_Bool && !_pos_3_Bool && !_pos_4_Bool && !_pos_5_Bool && !_pos_6_Bool && !_pos_7_Bool
            && !_pos_8_Bool && !_pos_9_Bool && !_pos_10_Bool)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"You must have at least one item checked"
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles: nil];
           // [alert show];
            return;
        }
        
        int tabID = [sharedDefaults integerForKey:@"Position_1"];
       value_Index=tabID;
        // NSLog(@"%d",value_Index);

        
        [self sentData];
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_1 forKey:@"Position_10"];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];

        
        // keeps Bool values as positions move up
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];

        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        
        
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
       
        
    
    }
    else if (indexPath.row == 0 && ![sharedDefaults boolForKey:@"BoolPosition_1"]) { //row 0
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        [tableView reloadData];
    }
    
    if (indexPath.row == 1 && [sharedDefaults boolForKey:@"BoolPosition_2"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        int tabID = [sharedDefaults integerForKey:@"Position_2"];
        value_Index=tabID;
       // NSLog(@"%d",value_Index);
        
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_2 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        

        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
       
        
       
        
    }
    else if (indexPath.row == 1 && ![sharedDefaults boolForKey:@"BoolPosition_2"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];

        
        //************ animate cell reloading **********************
        // Build the two index paths
        //NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:1 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects: indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        

    }
    if (indexPath.row == 2 && [sharedDefaults boolForKey:@"BoolPosition_3"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        int tabID = [sharedDefaults integerForKey:@"Position_3"];
        value_Index=tabID;
       // NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_3 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];

        
    }
    else if (indexPath.row == 2 && ![sharedDefaults boolForKey:@"BoolPosition_3"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
         if (_pos_2_Bool)
         [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
         else
         [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];

    }
    if (indexPath.row == 3 && [sharedDefaults boolForKey:@"BoolPosition_4"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
       
        int tabID = [sharedDefaults integerForKey:@"Position_4"];
       value_Index=tabID;
       //  NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        NSLog(@"the value of key:%@",sharedDefaults);
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_4 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:3 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 3 && ![sharedDefaults boolForKey:@"BoolPosition_4"]) {
        
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];

        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
         [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
         [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:3 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        
    }
    if (indexPath.row == 4 && [sharedDefaults boolForKey:@"BoolPosition_5"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        
        int tabID = [sharedDefaults integerForKey:@"Position_5"];
        value_Index=tabID;
        // NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_5 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:4 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 4 && ![sharedDefaults boolForKey:@"BoolPosition_5"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];

        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:4 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    if (indexPath.row == 5 && [sharedDefaults boolForKey:@"BoolPosition_6"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        int tabID = [sharedDefaults integerForKey:@"Position_6"];
        value_Index=tabID;
         // NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_6 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:5 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 5 && ![sharedDefaults boolForKey:@"BoolPosition_6"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_6"];
        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
         if (_pos_5_Bool)
             [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
         else
             [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
    
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:5 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];

    }
    if (indexPath.row == 6 && [sharedDefaults boolForKey:@"BoolPosition_7"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        int tabID = [sharedDefaults integerForKey:@"Position_7"];
        value_Index=tabID;
        NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_7 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:6 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 6 && ![sharedDefaults boolForKey:@"BoolPosition_7"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_7"];
        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
         if (_pos_6_Bool)
             [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
         else
             [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        

        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:6 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        

    }
    if (indexPath.row == 7 && [sharedDefaults boolForKey:@"BoolPosition_8"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        
        int tabID = [sharedDefaults integerForKey:@"Position_8"];
        value_Index=tabID;
        //NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_8 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:7 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 7 && ![sharedDefaults boolForKey:@"BoolPosition_8"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_8"];

        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
         if (_pos_7_Bool)
             [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
         else
             [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];

        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:7 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];

    }
    if (indexPath.row == 8 && [sharedDefaults boolForKey:@"BoolPosition_9"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        int tabID = [sharedDefaults integerForKey:@"Position_9"];
        value_Index=tabID;
       // NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        [self sentData];
        
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        //Every row value moves up & Bool Value follows
        [sharedDefaults setInteger:_position_1 forKey:@"Position_1"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_9"];
        
        //Position 1 changes to 10
        [sharedDefaults setInteger:_position_9 forKey:@"Position_10"];
        
        // keeps Bool values as positions move up
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_1"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_1"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_9_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_10_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:8 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (indexPath.row == 8 && ![sharedDefaults boolForKey:@"BoolPosition_9"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_9"];
        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
         if (_pos_8_Bool)
         [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
         else
         [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:8 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    if (indexPath.row == 9 && [sharedDefaults boolForKey:@"BoolPosition_10"]) {
        
        [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        [tableView reloadData];
        
        
        int tabID = [sharedDefaults integerForKey:@"Position_10"];
        value_Index=tabID;
        NSLog(@"%d",value_Index);
        
        NSString *str=[self findButtonForPosition:tabID];
        NSLog(@"%@",str);
        [self sentData];
        
    }
    else if (indexPath.row == 9 && ![sharedDefaults boolForKey:@"BoolPosition_10"]) {
        
        [sharedDefaults setBool:YES forKey:@"BoolPosition_10"];
        [sharedDefaults setInteger:_position_10 forKey:@"Position_1"];//Position changes to 1st row
        
        //moves cell to first row
        [tableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //Move rows down
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
        [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
        [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
        [sharedDefaults setInteger:_position_5 forKey:@"Position_6"];
        [sharedDefaults setInteger:_position_6 forKey:@"Position_7"];
        [sharedDefaults setInteger:_position_7 forKey:@"Position_8"];
        [sharedDefaults setInteger:_position_8 forKey:@"Position_9"];
        [sharedDefaults setInteger:_position_9 forKey:@"Position_10"];
        
        
        // keeps Bool values as positions moves down
        if (_pos_1_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_2"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_2"];
        
        if (_pos_2_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_3"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_3"];
        
        if (_pos_3_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_4"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_4"];
        
        if (_pos_4_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_5"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_5"];
        
        if (_pos_5_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_6"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_6"];
        
        if (_pos_6_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_7"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_7"];
        
        if (_pos_7_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_8"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_8"];
        
        if (_pos_8_Bool)
            [sharedDefaults setBool:YES forKey:@"BoolPosition_9"];
        else
            [sharedDefaults setBool:NO forKey:@"BoolPosition_9"];
        
         if (_pos_9_Bool)
         [sharedDefaults setBool:YES forKey:@"BoolPosition_10"];
         else
         [sharedDefaults setBool:NO forKey:@"BoolPosition_10"];
        
        
        //************ animate cell reloading **********************
        // Build the two index paths
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:9 inSection:0];
        
        // Add them in an index path array
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
        [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        

    }
    
    
    
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    //NSLog(@"count rows = %d", _countRowsInEditMode);
    //NSLog(@" fromIndexPath = %d", fromIndexPath.row);
    //NSLog(@" toIndexPath = %d", toIndexPath.row);
    
    
    /// Steve - fixes iOS 8 bug where you can move past the number of rows that exist on Edit.
    int toIndexMoveRow = toIndexPath.row;
    
    if (toIndexPath.row > _countRowsInEditMode - 1) {
        toIndexMoveRow = _countRowsInEditMode - 1;
    }
    
    
    [self findCurrentPositionValues]; //finds the position Values
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (_countRowsInEditMode == 2) {
        
        [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
        [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
    }
    else if (_countRowsInEditMode == 3){
        
        if (fromIndexPath.row == 0 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 0){
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_1"];
        }
        
    }
    else if (_countRowsInEditMode == 4){
        
        if (fromIndexPath.row == 0 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 0){
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_1"];
        }

        else if (fromIndexPath.row == 2 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }

        
    }
    else if (_countRowsInEditMode == 5){
        
        if (fromIndexPath.row == 0 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 0 && toIndexMoveRow == 4) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_1"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        if (fromIndexPath.row == 1 && toIndexMoveRow == 4) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 0){
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_1"];
        }
        
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 2 && toIndexMoveRow == 4) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 3 && toIndexMoveRow == 4) {
            
            [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        }
        else if (fromIndexPath.row == 4 && toIndexMoveRow == 0) {
            
            [sharedDefaults setInteger:_position_1 forKey:@"Position_2"];
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_1"];
        }
        else if (fromIndexPath.row == 4 && toIndexMoveRow == 1) {
            
            [sharedDefaults setInteger:_position_2 forKey:@"Position_3"];
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_2"];
        }
        else if (fromIndexPath.row == 4 && toIndexMoveRow == 2) {
            
            [sharedDefaults setInteger:_position_3 forKey:@"Position_4"];
            [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_3"];
        }
        else if (fromIndexPath.row == 4 && toIndexMoveRow == 3) {
            
            [sharedDefaults setInteger:_position_4 forKey:@"Position_5"];
            [sharedDefaults setInteger:_position_5 forKey:@"Position_4"];
        }
        
    }
    //return indexPath;
}



-(void)findCurrentPositionValues{
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Keeps track of current positions
    _position_1 = [sharedDefaults integerForKey:@"Position_1"];
    _position_2 = [sharedDefaults integerForKey:@"Position_2"];
    _position_3 = [sharedDefaults integerForKey:@"Position_3"];
    _position_4 = [sharedDefaults integerForKey:@"Position_4"];
    _position_5 = [sharedDefaults integerForKey:@"Position_5"];
    _position_6 = [sharedDefaults integerForKey:@"Position_6"];
    _position_7 = [sharedDefaults integerForKey:@"Position_7"];
    _position_8 = [sharedDefaults integerForKey:@"Position_8"];
    _position_9 = [sharedDefaults integerForKey:@"Position_9"];
    _position_10 = [sharedDefaults integerForKey:@"Position_10"];
    
    
    //keeps track of current position Bool
    _pos_1_Bool = [sharedDefaults boolForKey:@"BoolPosition_1"];
    _pos_2_Bool = [sharedDefaults boolForKey:@"BoolPosition_2"];
    _pos_3_Bool = [sharedDefaults boolForKey:@"BoolPosition_3"];
    _pos_4_Bool = [sharedDefaults boolForKey:@"BoolPosition_4"];
    _pos_5_Bool = [sharedDefaults boolForKey:@"BoolPosition_5"];
    _pos_6_Bool = [sharedDefaults boolForKey:@"BoolPosition_6"];
    _pos_7_Bool = [sharedDefaults boolForKey:@"BoolPosition_7"];
    _pos_8_Bool = [sharedDefaults boolForKey:@"BoolPosition_8"];
    _pos_9_Bool = [sharedDefaults boolForKey:@"BoolPosition_9"];
    _pos_10_Bool = [sharedDefaults boolForKey:@"BoolPosition_10"];
    
}

-(NSString *)findButtonForPosition: (int)buttonInt {
    
    
    /*
     SelectedButtonIndex:
     1 = Today
     2 = Projects
     3 = Starred
     4 = Overdue
     5 = Priority
     6 = Due Date
     7 = Progress
     8 = Done
     9 = Created
     100 = All
     */
    
   // NSLog(@"%d",buttonInt);
    NSString *tabNameString;

    
        switch (buttonInt) {
        case 1:
            tabNameString = @"Today";
            break;
        case 2:
            tabNameString = @"Projects";
            break;
        case 3:
            tabNameString = @"Starred";
            break;
        case 4:
            tabNameString = @"Over Due";
            break;
        case 5:
            tabNameString = @"Priority";
            break;
        case 6:
            tabNameString = @"Due Date";
            break;
        case 7:
            tabNameString = @"Progress";
            break;
        case 8:
            tabNameString = @"Done";
            break;
        case 9:
            tabNameString = @"Date Created";
            break;
        case 100:
            tabNameString = @"All";
            break;
            
        default:
            NSLog(@"find Button Error");
            break;
    }
    //return buttonInt
   // NSLog(@"%@",tabNameString);
    return tabNameString;
    
    
}



- (IBAction)doneButton_Clicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)editButton_Clicked:(id)sender {
    
    
    if ([_editButton.title isEqualToString:@"Edit"]) {
        
        _countRowsInEditMode = 0;
        _isEditing = YES;
        
        [_tableView reloadData]; //reloads to get _countRowsInEditMode (number of rows)
        
        //delays edit so can check how many checked items first.  If 6 and over, edit won't work
        [self performSelector:@selector(checkTableRowCountWithDelay) withObject:self afterDelay:0.10];
        
    }
    else if ([_editButton.title isEqualToString:@"Done"]) {
        
        _countRowsInEditMode = 0;
        
        _editButton.title = @"Edit";
        _doneButton.enabled = YES;
        _isEditing = NO;
        
        
        [super setEditing:NO animated:NO];
        [self.tableView setEditing:NO animated:NO];
        
        [_tableView reloadData];
        

    }
}

-(void)checkTableRowCountWithDelay{
    
    if (_countRowsInEditMode >= 6) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"Edit/Sorting is limited to 5 checked items."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles: nil];
        _editButton.title = @"Edit";
        _doneButton.enabled = YES;
        _isEditing = NO;
        
        [_tableView reloadData];
        _countRowsInEditMode = 0;
        
        [alert show];
    }
    else{
        
        _countRowsInEditMode = 0;
        
        _editButton.title = @"Done";
        _doneButton.enabled = NO;
        _isEditing = YES;
        
        [super setEditing:YES animated:NO];
        [_tableView setEditing:YES animated:NO];
        
        [_tableView reloadData];
    }
}

-(void)sentData
{
    
    OrganizerAppDelegate *app_Delegate=(OrganizerAppDelegate*)[[UIApplication sharedApplication]delegate];
    UISplitViewController *splitVC=[[UISplitViewController alloc]init];
    splitVC=app_Delegate.splitVCToDo;
    
    
   // NSLog(@"%d",splitVC.viewControllers.count);

    UINavigationController *nav=[[UINavigationController alloc]init];
    
    ToDoParentController *toDoParentView=[[ToDoParentController alloc]initWithNibName:@"ToDoParentControllerIPad" bundle:[NSBundle mainBundle]];
   
    toDoParentView.selectedButtonIndex=value_Index;
    NSLog(@"toDoParentView.selectedButtonIndex = %d",toDoParentView.selectedButtonIndex);
    nav.viewControllers=@[toDoParentView];
    [[splitVC.viewControllers objectAtIndex:0] removeFromParentViewController];
    [splitVC setViewControllers:@[[splitVC.viewControllers objectAtIndex:0] ,nav]];


}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  AppointmentViewController .m
//  Organizer
//
//  Created by Nidhi Ms. Aggarwal on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppointmentViewController .h"
#import "AddEditStartDateEndDateVC.h"
#import "AddEditTitleViewController.h"
#import "AddRepeatViewController.h"
#import "AddCalenderViewController.h"
#import "AddAlertViewController.h"
#import "CalendarDataObject.h"
#import "OrganizerAppDelegate.h"
#import "NSDate-Utilities.h"
#import "LocationListViewController.h"
#import "StartEndCalendarViewController.h"
#import "UIImage+ImageEffects.h"//Steve
#import "HandWritingViewController.h" //Steve


@interface AppointmentViewController()

@property (strong, nonatomic) UISwitch  *timeZoneSwitch;//Steve
@property (strong, nonatomic) UITextView *timeZoneTextField;//Steve
@property (strong, nonatomic) UILabel *timeZoneFixedLabel;//Steve
@property (assign, nonatomic) BOOL isiCloudCalendar; //Steve
@property (nonatomic, strong) UIImage *screenShotImage; //Steve - screenshot of AppointmentViewController

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar_iPad;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton_iPad;


-(void)setLabelValues;
-(void) animateAlertSectionToTop;//Steve
-(void) timeZoneSwitchedChanged;//Steve
-(NSArray*)findsCalendarsToShow;//Steve

@end


@implementation AppointmentViewController
@synthesize notesLabel;
@synthesize isAdd, isAllDay;
@synthesize aAppObj, aLocObj;
@synthesize appointmentTitle;
@synthesize appointmentType;
@synthesize appointmentBinary;
@synthesize appointmentBinaryLarge;

@synthesize titleTxtFld, delegate;
@synthesize locationtext;
@synthesize locationID;
@synthesize locTxtFld,GetappID;
@synthesize eventStore, event, relation_Field1;  //Steve added


#pragma mark -
#pragma mark ViewController Life Cycle
#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        isAdd = YES;
    }
    return self;
}

-(id)initWithDataObj: (AppointmentsDataObject *) appData
{
    
    if (IS_IOS_7) {
        if(IS_IPAD)
        {
            self = [super initWithNibName:@"AppointmentViewController2_iOS7_iPad" bundle:[NSBundle mainBundle]];
        }
        else
        {
            self = [super initWithNibName:@"AppointmentViewController2_iOS7" bundle:[NSBundle mainBundle]];
        }
        
        
        if (self)
        {
            isAdd = NO;
            self.aAppObj = appData;
            
            // NSLog(@" self.aAppObj = %@",self.aAppObj.relation_Field1);
        }
        
    }
    else{
        self = [super initWithNibName:@"AppointmentViewController2" bundle:[NSBundle mainBundle]];
        if (self)
        {
            isAdd = NO;
            self.aAppObj = appData;
            
            // NSLog(@" self.aAppObj = %@",self.aAppObj.relation_Field1);
        }
    }
    return self;
}

- (void)viewDidLoad
{
    //NSLog(@"viewDidLoad AppointmentViewController");
    [super viewDidLoad];
    
    //NSLog(@"viewDidLoad self.aAppObj = %@", self.aAppObj);
    
    
    //Steve - needed to fix bug in deleting repeated future events
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    eventStore = appDelegate.eventStore;
    if (eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
    
    
    
    //[NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    //[NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]]; //Steve
    
    //Steve - initialize objects
    titleTxtFld = [[UITextField alloc]init];
    inputSelecterButton = [[UIButton alloc]init];
    locTxtFld = [[UITextField alloc]init];
    
    startLabel = [[UILabel alloc]init];
    endLabel = [[UILabel alloc]init];
    startsLabel = [[UILabel alloc]init];
    endsLabel = [[UILabel alloc]init];
    
    repeatFixedLabel = [[UILabel alloc]init];
    repeatLabel = [[UILabel alloc]init];
    
    alert_1_FixedLabel = [[UILabel alloc]init];
    alertsLabel = [[UILabel alloc]init];
    secondalert_lbl = [[UILabel alloc]init];
    secondalertsLabel = [[UILabel alloc]init];
    
    notes_lbl = [[UILabel alloc]init];
    notesLabel = [[UILabel alloc]init];
    
    calendar_lbl = [[UILabel alloc]init];
    calendarLabel = [[UILabel alloc]init];
    
    self.startDatePicker = [[UIDatePicker alloc]init]; //Steve
    self.endDatePicker = [[UIDatePicker alloc]init]; //Steve
    
    //self.customAlert_1_Switch = [[UISwitch alloc]init]; //Steve
    //self.customAlert_2_Switch = [[UISwitch alloc]init]; //Steve
    
    self.alertPickerArray = [[NSMutableArray alloc] initWithObjects:@"None",@"5 Minutes Before",@"15 Minutes Before",@"30 Minutes Before",@"1 Hour Before",
                             @"2 Hour Before",@"1 Day Before",@"2 Day Before",
                             @"On Date of Event", nil];
    
    self.alert_1_Picker = [[UIDatePicker alloc]init]; //Steve
    self.alert_2_Picker = [[UIDatePicker alloc]init]; //Steve
    self.alert_1_Picker.hidden = YES;
    self.alert_2_Picker.hidden = YES;
    
    
    self.alertCustomPicker_1  = [[UIPickerView alloc]init]; //Steve
    self.alertCustomPicker_2  = [[UIPickerView alloc]init]; //Steve
    self.alertCustomPicker_1.hidden = YES;
    self.alertCustomPicker_2 .hidden = YES;
    
    
    self.startDateInternationalCompatible = [[NSDate alloc]init]; //Steve - keeps a date so it is international compatible & easily converted
    self.endDateInternationalCompatible = [[NSDate alloc]init]; //Steve
    
    NSArray  *segmentedControlArray = [[NSArray alloc] initWithObjects:@"Quick", @"Custom", nil];
    self.Alert_1segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentedControlArray];
    self.Alert_2segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentedControlArray];
    
    [self.Alert_1segmentedControl setSelectedSegmentIndex:0]; //Steve - sets SegmentedControl to "Quick"
    [self.Alert_2segmentedControl setSelectedSegmentIndex:0]; //Steve - sets SegmentedControl to "Quick"
    
    self.Alert_1segmentedControl.hidden = YES;
    self.Alert_2segmentedControl.hidden = YES;
    
    
    self.allDaySwitch = [[UISwitch alloc]init];
    
    _timeZoneSwitch = [[UISwitch alloc]init]; //Steve
    
    
    self.alertCustomPicker_1.delegate = self;
    self.alertCustomPicker_2.delegate = self;
    self.alertCustomPicker_1.dataSource = self;
    self.alertCustomPicker_2.dataSource = self;
    
    titleTxtFld.delegate =self;
    //locTxtFld.delegate = self;
    
    deleteButton = [[UIButton alloc]init];
    
    caleIdentifire =@"";
    
    
    //Steve iPad - Center Start/End pickers.  Others centered in RowForCell
    if (IS_IPAD) {
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width; //Finds screen width
        
        
        self.startDatePicker.frame =
        CGRectMake(screenWidth/2 - self.startDatePicker.frame.size.width/2, 0, self.startDatePicker.frame.size.width, self.startDatePicker.frame.size.height);
        self.endDatePicker.frame =
        CGRectMake(screenWidth/2 - self.endDatePicker.frame.size.width/2, 0, self.endDatePicker.frame.size.width, self.endDatePicker.frame.size.height);
        
    }
    
    
    appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    defaultcal = [[CalendarDataObject alloc]init];
    inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 97, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(270, 85) andParentVC:self fromMainScreen:NO];
    
    if(isAdd)
    {
        updateButton.hidden = YES;
        deleteButton.hidden = YES;
        saveButton.hidden = NO;
    }
    else
    {
        updateButton.hidden = NO;
        deleteButton.hidden = NO;
        saveButton.hidden = YES;
    }
    
    if (aAppObj && isAdd == NO)
    {
        //NSLog(@"aAppObj && isAdd == NO");
        
        //Steve - turns time zone switch on or off, depending on the value
        if (aAppObj.timeZoneName)
            _timeZoneSwitch.on = NO;
        else //nil is fixed
            _timeZoneSwitch.on = YES;
        
        
        
        if([aAppObj.appointmentType isEqualToString:@"H"])
        {
            if(aAppObj.appointmentBinary!= nil){
                [titleTxtFld setPlaceholder:@""];
                
                UIImage *tempTitleImage = [[UIImage alloc] initWithData:aAppObj.appointmentBinary];
                [titleTxtFld setBackground:tempTitleImage];
                
                self.appointmentType = @"H";
                self.appointmentTitle = @"";
                self.appointmentBinary = aAppObj.appointmentBinary;
                self.appointmentBinaryLarge = aAppObj.appointmentBinaryLarge;
                
                [titleTxtFld setText:@""];
            }else{
                [titleTxtFld setText:aAppObj.appointmentTitle];
                titleTxtFld.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
                self.appointmentType = @"H";
                self.appointmentTitle = aAppObj.appointmentTitle;
            }
            [titleTxtFld resignFirstResponder];
        }
        else if ([aAppObj.appointmentType isEqualToString:@"T"] || [aAppObj.appointmentType isEqualToString:@"V"]) {
            if (aAppObj.appointmentTitle && (![aAppObj.appointmentTitle isEqualToString:@""])) {
                
                //*******************************************
                //************ Steve added ******************
                // eventStore - finds the exisitng event on eventStore with an eventIdentifier saved on SQL
                
                self.relation_Field1 = aAppObj.relation_Field1;
                
                //NSLog(@"aAppObj.relation_Field1 = %@", aAppObj.relation_Field1);
                //NSLog(@"aAppObj.appointmentTitle = %@", aAppObj.appointmentTitle);
                
                NSString *my_Identifier = aAppObj.relation_Field1;              //eventIdentifier saved on SQL
                //event = [self.eventStore eventWithIdentifier:my_Identifier]; //Steve commented
                event = [eventStore eventWithIdentifier:my_Identifier];//finds event
                
                //************ Steve end ******************
                //*******************************************
                
                [titleImgView setImage:nil];
                [titleTxtFld setText:aAppObj.appointmentTitle];
                titleTxtFld.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
                
                self.appointmentType =  aAppObj.appointmentType;
                self.appointmentTitle =  aAppObj.appointmentTitle;
                self.appointmentBinary = aAppObj.appointmentBinary;
                self.appointmentBinaryLarge = aAppObj.appointmentBinaryLarge;
                
            }
            else {
                [titleTxtFld setPlaceholder:@"Title"];
                appointmentTitle = @"T";
                appointmentBinary = nil;
                //[titleLabel setText:@"Title"];
            }
        }
        else if ([aAppObj.appointmentType isEqualToString:@"T"] || [aAppObj.appointmentType isEqualToString:@"V"])  {
            if (aAppObj.appointmentTitle && ![ aAppObj.appointmentTitle isEqualToString:@""]) {
                //[titleTxtFld resignFirstResponder];///*******
                [titleImgView setImage:nil];
                [titleTxtFld setText:aAppObj.appointmentTitle];
                titleTxtFld.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
                
                self.appointmentType = aAppObj.appointmentType;
                self.appointmentTitle = aAppObj.appointmentTitle;
                self.appointmentBinary = aAppObj.appointmentBinary;
                self.appointmentBinaryLarge = aAppObj.appointmentBinaryLarge;
            }
            else
            {
                [titleTxtFld setPlaceholder:@"Title"];
                [titleTxtFld setTextColor:[UIColor lightGrayColor]];
                [titleTxtFld resignFirstResponder];
                
            }
        }
        else {
            [titleTxtFld setPlaceholder:@"Title"];
            [titleTxtFld setTextColor:[UIColor lightGrayColor]];
            [titleTxtFld resignFirstResponder];
        }
        
        if([aAppObj.textLocName isEqualToString:@""])
        {
            [locTxtFld setTextColor:[UIColor lightGrayColor]];
            locTxtFld.placeholder  = @"Location";
            [locTxtFld resignFirstResponder];
            //locTxtFld.tag  = 0;
        }
        else
        {
            //			self.aLocObj = [GetAllDataObjectsClass getLocationDataObjectForLocationID:[aAppObj locID]];
            locTxtFld.text  = [aAppObj textLocName];//aLocObj == nil ? @"Loca" :[NSString stringWithFormat:@"%@, %@, %@",[aLocObj locationAddress],[aLocObj locationCity],[aLocObj locationState]];
            [locTxtFld setTextColor:[UIColor blueColor]];
            //locTxtFld.tag  = [aAppObj locID];
        }
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSDate *strtDate = [dateFormatter dateFromString:[aAppObj startDateTime]];
        NSDate *endDate = [dateFormatter dateFromString:[aAppObj dueDateTime]];
        
        self.startDateInternationalCompatible = strtDate; //Steve - keeps this separate from the label so dates can be used in any region
        self.endDateInternationalCompatible = endDate; //Steve
        
        //NSLog(@" self.startDateInternationalCompatible = %@", self.startDateInternationalCompatible);
        //NSLog(@" self.startDateInternationalCompatible = %@", [self.startDateInternationalCompatible descriptionWithLocale:@"GMT"]);
        //NSLog(@" self.endDateInternationalCompatible = %@", self.endDateInternationalCompatible);
        
        
        //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"]; //Steve commented.  Does not conform to international styles like Europe
        [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];//Steve
        
        NSString *strStrtDate = [dateFormatter stringFromDate:strtDate];
        NSString *strEndDate = [dateFormatter stringFromDate:endDate];
        
        [startsLabel setTextColor:[UIColor darkGrayColor]];
        [endsLabel setTextColor:[UIColor darkGrayColor]];
        startsLabel.text = strStrtDate;
        endsLabel.text = strEndDate;
        
        
        //repeatLabel.text = [[aAppObj repeat] isEqualToString:@""] == YES ? @"None": [aAppObj repeat];
        //Steve fixed
        if ([aAppObj.repeat isEqualToString:@""] || [aAppObj.repeat isEqualToString:@"None"] || aAppObj.repeat == nil) {
            repeatLabel.text = @"Never";
        }
        else{
            repeatLabel.text = aAppObj.repeat;
        }
        
        
        repeatLabel.textColor = [[aAppObj repeat] isEqualToString:@""] == YES ? [UIColor lightGrayColor]: [UIColor darkGrayColor];
        
        
        //Steve - keeps a copy in Date format to handle picker & internationa dates from crashing picker
        //if "Quick" is the data, this will be nil
        self.alert_1_Date = [dateFormatter dateFromString:aAppObj.alert];
        self.alert_2_Date = [dateFormatter dateFromString:aAppObj.alertSecond];
        
        
        
        /////// Sets appropriate value to the alert 1 label
        if (aAppObj.alert == nil && self.alert_1_Date == nil) //No Date
            alertsLabel.text = @"None";
        else if (self.alert_1_Date == nil && aAppObj.alert != nil) //aAppObj Data
            alertsLabel.text = aAppObj.alert;
        else if (self.alert_1_Date != nil)
            alertsLabel.text = [dateFormatter stringFromDate:self.alert_1_Date]; //Date
        
        
        /////// Sets appropriate value to the alert 2 label
        if (aAppObj.alertSecond == nil && self.alert_2_Date == nil) //No Date
            secondalertsLabel.text = @"None";
        else if (self.alert_2_Date == nil && aAppObj.alertSecond != nil) //aAppObj Data
            secondalertsLabel.text = aAppObj.alertSecond;
        else if (self.alert_2_Date != nil)
            secondalertsLabel.text = [dateFormatter stringFromDate:self.alert_2_Date]; //Date
        
        
        
        //NSLog(@" self.alert_1_Date = %@", self.alert_1_Date);
        //NSLog(@" self.alert_2_Date = %@", self.alert_2_Date);
        
        //NSLog(@" alertsLabel.text = %@", alertsLabel.text);
        //NSLog(@" secondalertsLabel.text = %@", secondalertsLabel.text);
        
        
        //Steve added - if alert_Date is Not Nil it will show "Custom" picker with Dates
        if (self.alert_1_Date) {
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:1]; //Steve - sets SegmentedControl to "Custom"
            self.alert_1_Picker.date = self.alert_1_Date;
        }
        
        if (self.alert_2_Date) {
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:1]; //Steve - sets SegmentedControl to "Custom"
            self.alert_2_Picker.date = self.alert_2_Date;
        }
        
        
        //******************************************************
        //"None",@"5 Minutes Before",@"15 Minutes Before",@"30 Minutes Before",@"1 Hour Before",
        // @"2 Hour Before",@"1 Day Before",@"2 Day Before",
        // @"On Date of Event", nil];
        
        
        //Steve - sets "Quick" picker Alert 1
        if ([alertsLabel.text isEqualToString:@"None"]) {
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:0 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"5 Minutes Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:1 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"15 Minutes Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:2 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"30 Minutes Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:3 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"1 Hour Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:4 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"1 Day Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:5 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"2 Day Before"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:6 inComponent:0 animated:YES];
        }
        else if ([alertsLabel.text isEqualToString:@"On Date of Event"]){
            
            [self.Alert_1segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_1 selectRow:7 inComponent:0 animated:YES];
        }
        
        
        //Steve - sets "Quick" picker Alert 2
        if ([secondalertsLabel.text isEqualToString:@"None"]) {
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:0 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"5 Minutes Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:1 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"15 Minutes Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:2 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"30 Minutes Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:3 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"1 Hour Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:4 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"1 Day Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:5 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"2 Day Before"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:6 inComponent:0 animated:YES];
        }
        else if ([secondalertsLabel.text isEqualToString:@"On Date of Event"]){
            
            [self.Alert_2segmentedControl setSelectedSegmentIndex:0];
            [self.alertCustomPicker_2 selectRow:7 inComponent:0 animated:YES];
        }
        
        
        
        
        
        notesLabel.text = [[aAppObj shortNotes] isEqualToString:@""] == YES ? @"None": [aAppObj shortNotes];
        notesLabel.textColor = [[aAppObj shortNotes] isEqualToString:@""] == YES ? [UIColor lightGrayColor]: [UIColor darkGrayColor];
        
        
        if (![aAppObj relation_Field1])
        {
            calendarLabel.text = @"None";
            [calendarLabel setTextColor:[UIColor lightGrayColor]];
            calendarLabel.tag =  0;
        }
        else
        {
            [calendarLabel setTextColor:[UIColor darkGrayColor]];
            
            calendarLabel.text = [self getCalendarName:[aAppObj calendarName]];
            [defaultcal setCalendarName:calendarLabel.text];
            [defaultcal setCalendarIdentifire:caleIdentifire];
            calendarLabel.tag =  [aAppObj calID];
        }
    }
    else  //isAdd == YES
    {
        
        _timeZoneSwitch.on = YES; //Steve - defaults to switch on.  Sets timezone to float so time never changes
        self.allDaySwitch.on = NO; //Steve - initial value
        
        
        EKCalendar * cal = [eventStore defaultCalendarForNewEvents];
        [defaultcal setCalendarName:cal.title];
        [defaultcal setCalendarIdentifire:cal.calendarIdentifier];
        const CGFloat *component = CGColorGetComponents(cal.CGColor);
        if(component[0])
        [defaultcal setCalendarColorRed:component[0]];
        if(component[1])
        [defaultcal setCalendarColorGreen:component[1]];
        if(component[2])
        [defaultcal setCalendarColorBlue:component[2]];
        [defaultcal setCalendarSource:cal.source.title]; //Steve
        
        
        //Steve - Determines if Cal is "iCloud".  If not, don't show Fixed Date & Time
        if ([defaultcal.calendarSource isEqualToString:@"iCloud"]) {
            _isiCloudCalendar = YES;
            _timeZoneTextField.hidden = NO;
            _timeZoneFixedLabel.hidden = NO;
            _timeZoneSwitch.hidden = NO;
        }
        else{
            _isiCloudCalendar = NO;
            _timeZoneTextField.hidden = YES;
            _timeZoneFixedLabel.hidden = YES;
            _timeZoneSwitch.hidden = YES;
        }
        
        calendarLabel.text = defaultcal.calendarName;
        [calendarLabel setTextColor:[UIColor darkGrayColor]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        //Gets currentVisibleDate
        NSDate *currentVisibleDate_Date = [dateFormatter dateFromString:appDelegate.currentVisibleDate];
        
        //removes time from currentVisibleDate
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *currentVisibleDateNoTime = [dateFormatter stringFromDate:currentVisibleDate_Date];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"HH:mm:ss +0000"];
        
        //Get current Time
        NSDate      *localTimeOnly = [NSDate date];
        NSString    *localTime = [timeFormatter stringFromDate:localTimeOnly];
        
        
        //Final Date String that shows CurrentVisibleDate with current Time
        NSString *finalDate_String = [currentVisibleDateNoTime stringByAppendingFormat:@" %@",localTime];
        
        //Convert Finale Date string back to Date format
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        NSDate   *finalDate_Date = [dateFormatter dateFromString:finalDate_String];
        
        
        self.startDateInternationalCompatible = finalDate_Date; //keeps correct time so it can change Regions later
        self.endDateInternationalCompatible = [finalDate_Date dateByAddingHours:1];
        
        
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        NSString *localDateString = [dateFormatter stringFromDate:finalDate_Date]; //Local Date String with current locale formatting
        
        [startsLabel setTextColor:[UIColor darkGrayColor]];
        [endsLabel setTextColor:[UIColor darkGrayColor]];
        
        //set label values
        [startsLabel setText:localDateString];
        [endsLabel setText:[dateFormatter stringFromDate:[finalDate_Date dateByAddingHours:1]]];
        
        
        if (IS_IOS_7) {
            
            //Steve - setup alert pickers date
            self.alert_1_Date = self.startDateInternationalCompatible;
            self.alert_2_Date = self.startDateInternationalCompatible;
            
            //Set alert pickers
            self.alert_1_Picker.date = self.alert_1_Date;
            self.alert_2_Picker.date = self.alert_2_Date;
            
            /*
             NSLog(@" self.alert_1_Date = %@", self.alert_1_Date);
             NSLog(@" self.alert_2_Date = %@", self.alert_2_Date);
             
             NSLog(@" self.alert_1_Picker.date = %@", self.alert_1_Picker.date);
             NSLog(@" self.alert_2_Picker.date = %@", self.alert_2_Picker.date);
             */
        }
        
        
        
    }
    isAllDay = aAppObj.isAllDay;
    
    if (isAllDay)
        self.allDaySwitch.on = YES;
    else
        self.allDaySwitch.on = NO;
    
    
    locTxtFld.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
    isAllDay = aAppObj.isAllDay;
    [titleTxtFld resignFirstResponder];
    [locTxtFld resignFirstResponder];
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    
    
    NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
    [stringList setValue:@"Calender" forKey:@"Viewcontroller"];
    [stringList synchronize];
    NSLog(@"%@",[stringList valueForKey:@"Viewcontroller"]);
    
    
    //NSLog(@"viewWillAppear AppointmentViewController");
    /*  if(IS_IPAD)
     {
     OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
     [myAppdelegate hideTabBar:myAppdelegate.tabBarController];
     }
     */
    
    //***********************************************************
    // **************** Steve added ******************************
    // *********** Facebook style Naviagation *******************
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        
        
        if (IS_IOS_7) {
            
            self.edgesForExtendedLayout = UIRectEdgeNone;//UIRectEdgeNone
            self.navigationController.navigationBar.translucent = NO;
            
            
            //Steve
            if (IS_IPAD) {
                
                
                
                // [[UIApplication sharedApplication] setStatusBarHidden:YES];
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
                
                navBar.hidden = YES;
                
                backButton.hidden = YES;
                _doneButton_iPad.tintColor = [UIColor whiteColor];
                cancelButton_Ipad.tintColor = [UIColor whiteColor];
                
                //Steve - setup Table
                
            }
            else{ //iPhone
                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                
                self.navigationController.navigationBarHidden = NO;
                navBar.hidden = YES;
                backButton.hidden = YES;
                
            }
            
            UIBarButtonItem *doneButton_iOS7;
            UIBarButtonItem *updateButton_iOS7;
            
            
            //done Button
            saveButton.hidden = YES;
            
            doneButton_iOS7 = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                               target:self
                               action:@selector(save_buttonClicked:)];
            doneButton_iOS7.tintColor=[UIColor whiteColor];
            
            
            //update Button
            updateButton.hidden = YES;
            updateButton_iOS7 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(update_buttonClicked:)];
            
            
            
            ////////////////////// Light Theme vs Dark Theme ////////////////////////////////////////////////
            
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                
                
                
                self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0  blue:245.0/255.0  alpha:1.0];
                self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
                
                doneButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                updateButton_iOS7.tintColor = [UIColor colorWithRed:50.0/255.0f green:125.0/255.0f blue:255.0/255.0f alpha:1.0];
                
            }
            else{ //Dark Theme
                
                if(IS_IPAD)
                {
                    
                    //Steve
                    _navBar_iPad.translucent = NO;
                    [_navBar_iPad setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    _navBar_iPad.tintColor = [UIColor whiteColor];
                    _navBar_iPad.barTintColor = [UIColor darkGrayColor];
                    _navBar_iPad.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
                    
                    _doneButton_iPad.tintColor = [UIColor whiteColor];
                    
                    
                    
                }
                else
                {
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    
                    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0  blue:66.0/255.0  alpha:0.7];
                    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
                    
                    doneButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                    updateButton_iOS7.tintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
                }
            }
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            
            
            if (isAdd) {
                
                if (IS_IPAD) {
                    OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
                    myAppdelegate.tabBarController.tabBar.hidden=YES;
                    self.navigationController.navigationBarHidden = YES;
                    [[_navBar_iPad.items objectAtIndex:0] setTitle:@"Add Event"];
                    [_doneButton_iPad setAction:@selector(save_buttonClicked:)];
                }
                else{ //iPhone
                    OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
                    myAppdelegate.tabBarController.tabBar.hidden=YES;
                    self.navigationController.navigationBarHidden = NO;
                    self.title =@"Add Event";
                    self.navigationItem.rightBarButtonItem = doneButton_iOS7;
                }
                
            }
            else{ //Edit Event
                
                //Steve
                if (IS_IPAD) {
                    self.navigationController.navigationBarHidden=NO;
                    [[_navBar_iPad.items objectAtIndex:0] setTitle:@"Edit Event"];
                    [_doneButton_iPad setAction:@selector(update_buttonClicked:)]; //Change button
                }
                else{ //iPhone
                    
                    self.title =@"Edit Event";
                    self.navigationItem.rightBarButtonItem = updateButton_iOS7;
                }
                
            }
            
            
            if(IS_IPAD)
            {
                self.view.frame = CGRectMake(0, 0, 768, 1024);
            }
            else
            {
                
                if (IS_IPHONE_5) {
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                }
                else {
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                }
            }
            
        }
        else{ //iOS 6
            
            UINavigationBar *navBarNew = [[self navigationController]navigationBar];
            UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
            [navBarNew setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
            //Steve - changes bar button items to black.  Must change to white after or it will change all icons to dark gray
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
            
            if (isAdd) {
                self.title =@"Add Event";
                [self.navigationController.navigationBar addSubview:saveButton];
            }
            else{
                self.title =@"Edit Event";
                [self.navigationController.navigationBar addSubview:updateButton];
            }
            
            
            //Done Button
            if (isAdd) {  //Add
                UIImage *saveButtonImage = [UIImage imageNamed:@"Done.png"];
                saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
                [saveButton setBackgroundImage:saveButtonImage forState:UIControlStateNormal];
                [saveButton addTarget:self action:@selector(save_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
                
                [[self navigationItem] setRightBarButtonItem:barButton];
            }
            else{   //Update
                UIImage *updateButtonImage = [UIImage imageNamed:@"Done.png"];
                updateButton = [[UIButton alloc] initWithFrame:CGRectMake(0,5,51,29)];
                [updateButton setBackgroundImage:updateButtonImage forState:UIControlStateNormal];
                [updateButton addTarget:self action:@selector(update_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:updateButton];
                
                [[self navigationItem] setRightBarButtonItem:barButton];
            }
            
            [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0]];
            
            
        }
        
        
    }
    else{
        
        self.navigationController.navigationBarHidden = YES;
        
        UIImage *backgroundImage = [UIImage imageNamed:@"NavBar.png"];
        [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
    }
    
    
    // ***************** Steve End ***************************
    
    if(!isAdd)
        [[navBar.items objectAtIndex:0] setTitle:@"Edit Appointment"];
    
    OrganizerAppDelegate *appdelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(!appdelegate.IsVTTDevice || !appDelegate.isVTTDeviceAuthorized) //Steve was: if(!appdelegate.IsVTTDevice || IS_LITE_VERSION)
        [inputSelecterButton setImage:[UIImage imageNamed:@"input_icon_pen.png"] forState:UIControlStateNormal];
    
    
    
    [titleTxtFld resignFirstResponder];
    [locTxtFld resignFirstResponder];
    
    [self setLabelValues];
    self.view.backgroundColor = [UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0f];
    
    
    //Steve - disables user interaction with view while using handwriting
    if (IS_IPAD && self.isHandwritingShowing) {
        
        self.myTableView.userInteractionEnabled = NO;
    }
    
    self.titleTxtFld.tintColor = [UIColor blackColor];
    self.locTxtFld.tintColor = [UIColor blackColor];
}

- (void) viewDidAppear:(BOOL)animated{
    
    
    //Steve
    if (IS_IPAD && self.isHandwritingShowing) {
        
        self.myTableView.userInteractionEnabled = NO;
        //HandWritingViewController
        HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController_iPad"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
        handWritingViewController.handwritingForiPadDelegate = self;
        
        NSUserDefaults *stringList=[NSUserDefaults standardUserDefaults];
        [stringList setValue:@"Calender" forKey:@"Viewcontroller"];
        [stringList synchronize];
        
        [self addChildViewController:handWritingViewController];
        [self.view addSubview:handWritingViewController.view];
        [self.view bringSubviewToFront:handWritingViewController.view];
        handWritingViewController.view.hidden = YES;
        
        //animates the handWritingViewController.view
        [self performSelector:@selector(fadeInHandwritingView:) withObject:handWritingViewController.view afterDelay:0.0];
        
        
        //Steve - adds the blurr image of the screehshot
        int blurredViewWidth = [[UIScreen mainScreen] bounds].size.width;
        int blurredViewHeight = [[UIScreen mainScreen] bounds].size.height - 320;
        
        UIImage *image = [self captureScreenShotAndblurImage];
        self.blurredImageView = [[UIImageView alloc]initWithImage:image];
        self.blurredImageView.frame = CGRectMake(0, 0, blurredViewWidth, blurredViewHeight); //Change frame size so it doesn't cover HandwritingView
        [self.view addSubview:self.blurredImageView];
        self.blurredImageView.hidden = YES;
        
        //Animates self.blurredImageView
        [self performSelector:@selector(fadeInBlurredImage) withObject:nil afterDelay:0.0];
        
        
        
        
        //UIImageView *testImageView = self.blurredImageView;
    }
}


- (void) fadeInHandwritingView:(UIView*) handwritingView{
    
    CATransition  *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    transition.duration = 0.50;
    [handwritingView.layer addAnimation:transition forKey:nil];
    handwritingView.hidden = NO;
    
}

- (void) fadeInBlurredImage{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.blurredImageView.layer addAnimation:transition forKey:nil];
    self.blurredImageView.hidden = NO;
    
}


-(NSString*)getCalendarName:(NSString *)calname
{
    NSArray * temp = [calname componentsSeparatedByString:@"~"];
    
    if([temp count]>=2){
        calname =[temp objectAtIndex:0];
        caleIdentifire = [temp objectAtIndex:1];
    }
    return calname;
}




#pragma mark -
#pragma mark AlertView Delegate
#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (!isShowEndDatePicker) {
        [titleTxtFld becomeFirstResponder];
    }
    
}


#pragma mark -
#pragma mark Private Methods
#pragma mark -
-(void)setLabelValues
{
    if ([appointmentType isEqualToString: @"T"] || [appointmentType isEqualToString: @"V"]) {
        if (appointmentTitle && (![appointmentTitle isEqualToString:@""])) {
            [titleTxtFld setBackground:nil];
            [titleTxtFld setText:appointmentTitle];
            titleTxtFld.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
        }
    }
    else if ([appointmentType isEqualToString: @"H"] && appointmentBinary) {
        [titleTxtFld setPlaceholder: @""];
        UIImage *tempTitleImage = [[UIImage alloc] initWithData:appointmentBinary];
        [titleTxtFld setBackground:tempTitleImage];
        [titleTxtFld setText:@""];
        [titleTxtFld resignFirstResponder];
    }
    else if (([appointmentType isEqualToString: @""] || appointmentType == nil)){
        [titleTxtFld setPlaceholder: @"Title"];
        appointmentType = @"T";
        appointmentBinary = nil;
    }
}

#pragma mark -
#pragma mark TextField Delegate Methods
#pragma mark -

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
    if ([appointmentType  isEqualToString:@"H"]) {
        [titleTxtFld resignFirstResponder];
    }
    if (appointmentTitle) {
        appointmentTitle = nil;
    }
    
    textField.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    appointmentTitle = textField.text;
    //	[locTxtFld resignFirstResponder];
}

- (IBAction)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([appointmentType  isEqualToString:@"H"]) {
        [textField resignFirstResponder];
        return YES;
    }
    if (appointmentTitle) {
        appointmentTitle = nil;
    }
    textField.textColor = [UIColor colorWithRed:14.0f/255.0f green:83.0f/255.0f blue:167.0f/255.0f alpha:1.0f];//Steve
    appointmentTitle = textField.text;
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark text field delegagte

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == titleTxtFld)
    {
        
        self.appointmentType = @"T";
        
        [titleTxtFld setBackground:nil];
        
        return YES;
    }
    
    return NO;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [titleTxtFld resignFirstResponder];
    [locTxtFld resignFirstResponder];
    
    
}



#pragma mark -
#pragma mark Delegate Methods
#pragma mark -

-(void)setNote:(NSString*)anote {
    notesLabel.text = anote;
    [notesLabel setTextColor:[UIColor darkGrayColor]];
}

-(void)setStartDateLabel:(NSString*)aStartDate {
    startsLabel.text = aStartDate;
    [startsLabel setTextColor:[UIColor darkGrayColor]];
}

-(void)setEndDateLabel:(NSString*)aEndDate {
    endsLabel.text = aEndDate;
    [endsLabel setTextColor:[UIColor darkGrayColor]];
}

-(void)setIsAllDay:(BOOL)isallDay {
    isAllDay = isallDay;
}


//Steve - for iOS 6 keeps startDate in Date format for international conpatibility
-(void)setStartDateInternational:(NSDate*)startDate_DateFormat{
    
    self.startDateInternationalCompatible = startDate_DateFormat;
    
    //NSLog(@" self.startDateInternationalCompatible =%@", self.startDateInternationalCompatible);
    //NSLog(@" self.startDateInternationalCompatible =%@", [self.startDateInternationalCompatible descriptionWithLocale:@"GMT"]);
    
}


//Steve - for iOS 6 keeps endDate in Date format for international conpatibility
-(void)setEndDateInternational:(NSDate*)endDate_DateFormat{
    
    self.endDateInternationalCompatible = endDate_DateFormat;
    
    // NSLog(@" self.endDateInternationalCompatible) =%@", self.endDateInternationalCompatible);
}


//-(void)setLocObject:(LocationDataObject*)locObj selectedval:(NSString*)val {
//    self.aLocObj = locObj;
//    locTxtFld.text = val;
//	locTxtFld.tag = [locObj locationID];
//	[locTxtFld setTextColor:[UIColor blueColor]];
//}

-(void)setRepeatString:(NSString *) repeat {
    repeatLabel.text =  repeat;
    [repeatLabel setTextColor:[UIColor grayColor]];
}

-(void)setAlertString:(NSString *) alert {
    alertsLabel.text =  alert;
    [alertsLabel setTextColor:[UIColor grayColor]];
}
-(void)setSecondAlertString:(NSString *)  alert
{
    secondalertsLabel.text =  alert;
    [secondalertsLabel setTextColor:[UIColor grayColor]];
}



-(void)setCalendarName:(NSString *) calName calID:(NSInteger) calId {
    calendarLabel.text = [self getCalendarName:calName];
    calendarLabel.tag = 0;
    calendarLabel.tag = calId;
    [calendarLabel setTextColor:[UIColor grayColor]];
}

-(void)setCalendarData:(CalendarDataObject *) calObj {
    
    defaultcal = nil;
    defaultcal = calObj;
    
    calendarLabel.text = calObj.calendarName;
    [calendarLabel setTextColor:[UIColor grayColor]];
    
    // NSLog(@" defaultcal.calendarSource = %@", defaultcal.calendarSource);
    
    if ([defaultcal.calendarSource  isEqualToString:@"iCloud"]) {
        
        _isiCloudCalendar = YES;
        _timeZoneTextField.hidden = NO;
        _timeZoneFixedLabel.hidden = NO;
        _timeZoneSwitch.hidden = NO;
    }
    else{
        
        _isiCloudCalendar = NO;
        _timeZoneTextField.hidden = YES;
        _timeZoneFixedLabel.hidden = YES;
        _timeZoneSwitch.hidden = YES;
    }
    
    
    [self.myTableView reloadData];
    
}


#pragma mark -
#pragma mark Button Clicked Methods
#pragma mark -
//-(IBAction)selectLocationButton_Clicked:(id)sender
//{
//    LocationListViewController *locationViewController = [[LocationListViewController alloc] initWithNibName:@"LocationListViewController" bundle:[NSBundle mainBundle] withLocationID:[locationID intValue]];
//    [locationViewController setDelegate:self];
//	[self.navigationController pushViewController:locationViewController animated:YES];
//	[locationViewController release];
//}

-(IBAction) detDiscBtn_Clicked:(UIButton *)sender {
    
    switch (sender.tag) {
        case 1:
            //			[self titleLocatio_buttonClicked:nil];
            break;
        case 2:
            [self startsEnds_buttonClicked:nil];
            break;
        case 3:
            [self repeat_buttonClicked:nil];
            break;
        case 4:
            [self alerts_buttonClicked:nil];
            break;
        case 5:
            [self notes_buttonClicked:nil];
            break;
        case 6:
            [self calendar_buttonClicked:nil];
        case 7:
            [self alertsecond_buttonClicked:nil];
            break;
        default:
            break;
    }
}

//-(IBAction) titleLocatio_buttonClicked:(id)sender {
//
//	AddEditTitleViewController *addEditTitleViewController = [[AddEditTitleViewController alloc] initWithAppType:appointmentType appTitle:titleLabel.text appBinary:appointmentBinary appBinaryLarge:appointmentBinaryLarge locationID:@"" LocText:locationLabel.text];
//  [addEditTitleViewController setDelegate:self];
//	[self.navigationController pushViewController:addEditTitleViewController animated:YES];
//	[addEditTitleViewController release];
//}

-(IBAction) startsEnds_buttonClicked:(id)sender {
    
    //AddEditStartDateEndDateVC *addEditStartDateEndDateVC= [[AddEditStartDateEndDateVC alloc] initWithStartDateString:startsLabel.text == nil ? @"" : startsLabel.text endDateStr:endsLabel.text == nil ? @"" : endsLabel.text isAllDay: isAllDay];
    
    StartEndCalendarViewController *startEndCalendarViewController =
    [[StartEndCalendarViewController alloc] initWithStartDateString:startsLabel.text == nil ? @"" : startsLabel.text
                                                         endDateStr:endsLabel.text == nil ? @"" : endsLabel.text
                                                           isAllDay:isAllDay
                                                     startDate_Date:self.startDateInternationalCompatible
                                                       endDate_Date:self.endDateInternationalCompatible];
    
    [startEndCalendarViewController setDelegate:self];
    [self.navigationController pushViewController:startEndCalendarViewController animated:NO];
}

-(IBAction) repeat_buttonClicked:(id)sender {
    
    if (IS_IPAD) {
        
        AddRepeatViewController *addRepeatViewControler = [[AddRepeatViewController alloc]initWithRepeatText:repeatLabel.text == nil ? @"" : repeatLabel.text];
        [self presentViewController:addRepeatViewControler animated:YES completion:nil];
        [addRepeatViewControler setDelegate:self];
    }
    else{ //iPhone
        
        AddRepeatViewController *addRepeatViewControler = [[AddRepeatViewController alloc]initWithRepeatText:repeatLabel.text == nil ? @"" : repeatLabel.text];
        [self.navigationController pushViewController:addRepeatViewControler animated:YES];
        [addRepeatViewControler setDelegate:self];
    }
}


-(IBAction) alerts_buttonClicked:(id)sender {
    
    appDelegate.alertStr=@"firstClicked";
    
    //NSLog(@"appDelegate.alertStr = %@",appDelegate.alertStr);
    //AddAlertViewController *addAlertViewController = [[AddAlertViewController alloc] initWithAlertString:alertsLabel.text == nil ? @"" : startsLabel.text];
    
    //Steve
    AddAlertViewController *addAlertViewController = [[AddAlertViewController alloc] initWithAlertString:alertsLabel.text == nil ? @"" : startsLabel.text alertDate:self.startDateInternationalCompatible];
    
    
    [self.navigationController pushViewController:addAlertViewController animated:YES];
    [addAlertViewController setDelegate:self];
}


-(IBAction) alertsecond_buttonClicked:(id)sender{
    
    appDelegate.alertStr=@"secondClicked";
    
    //AddAlertViewController *addAlertViewController = [[AddAlertViewController alloc] initWithAlertString:secondalertsLabel.text == nil ? @"" : startsLabel.text];//Anil's ///
    
    //Steve
    AddAlertViewController *addAlertViewController = [[AddAlertViewController alloc] initWithAlertString:secondalertsLabel.text == nil ? @"" : startsLabel.text alertDate:self.startDateInternationalCompatible];
    
    
    [self.navigationController pushViewController:addAlertViewController animated:YES];
    [addAlertViewController setDelegate:self];
    
}

-(IBAction) notes_buttonClicked:(id)sender {
    
    //Steve
    if (IS_IPAD) {
        
        AddEditNotesViewController *addEditStartNotesCntrl = [[AddEditNotesViewController alloc]initWithNibName:@"AddEditNotesViewController" bundle:[NSBundle mainBundle]];
        addEditStartNotesCntrl.previousNotes=notesLabel.text;
        [self presentViewController:addEditStartNotesCntrl animated:YES completion:nil];
        [addEditStartNotesCntrl setDelegate:self];
    }
    else{ //iPhone
        
        AddEditNotesViewController *addEditStartNotesCntrl = [[AddEditNotesViewController alloc]initWithNibName:@"AddEditNotesViewController" bundle:[NSBundle mainBundle]];
        addEditStartNotesCntrl.previousNotes=notesLabel.text;
        [self.navigationController pushViewController:addEditStartNotesCntrl animated:YES];
        [addEditStartNotesCntrl setDelegate:self];
    }
    
}

-(IBAction) calendar_buttonClicked:(id)sender {
    
    //Steve
    if (IS_IPAD) {
        
        AddCalenderViewController *addCalenderViewController = [[AddCalenderViewController alloc] initWithCalendar:defaultcal];
        
        
        //AddCalenderViewController *addCalenderViewController = [[AddCalenderViewController alloc]initWithNibName:@"AddCalenderViewController_iPad2" bundle:[NSBundle mainBundle]];
        [self presentViewController:addCalenderViewController animated:YES completion:nil];
        [addCalenderViewController setDelegate:self];
    }
    else{ //iPhone
        
        //Alok Added to send cal obj
        AddCalenderViewController *addCalenderViewController = [[AddCalenderViewController alloc] initWithCalendar:defaultcal];
        [self.navigationController pushViewController:addCalenderViewController animated:YES];
        [addCalenderViewController setDelegate:self];
    }
    
    
}

-(IBAction) backClicked:(id)sender {
    
    if (IS_IPAD) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else{ //iPhone
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
    /* //Steve comment
     if(IS_IPAD)
     {
     OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
     [myAppdelegate showTabBar:myAppdelegate.tabBarController];
     }
     */
}

-(IBAction) inputTypeSelectorButton_Clicked:(id)sender {
    [titleTxtFld resignFirstResponder];
    
    if (inputTypeSelectorViewController) {
        inputTypeSelectorViewController = nil;
    }
    
    if(appDelegate.IsVTTDevice && appDelegate.isVTTDeviceAuthorized)
    {
        inputTypeSelectorViewController = [[InputTypeSelectorViewController alloc] initWithContentFrame:CGRectMake(165, 97, 150, 200) andArrowDirection:@"U" andArrowStartPoint:CGPointMake(270, 85) andParentVC:self fromMainScreen:NO];
        [inputTypeSelectorViewController.view setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:inputTypeSelectorViewController.view];
        [self.view bringSubviewToFront:inputTypeSelectorViewController.view];
    }
    else
    {
        //Steve
        if (IS_IPAD) {
            self.isHandwritingShowing = YES;
            [self viewDidAppear:YES]; //Steve - loads Handwriting view for iPad as a childVC
        }
        else{ //iPhone
            
            HandWritingViewController *handWritingViewController = [[HandWritingViewController alloc] initWithNibName:@"HandWritingViewController"  bundle:[NSBundle mainBundle] withParent:self isPenIcon:NO];
            [self.navigationController pushViewController:handWritingViewController animated:NO];
        }
        
    }
    
}


-(IBAction)save_buttonClicked:(id)sender
{
    // NSLog(@"save_buttonClicked");
    
    /*
     if(IS_IPAD)
     {
     OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
     [myAppdelegate showTabBar:myAppdelegate.tabBarController];
     }
     */
    
    
    if ([appointmentType isEqualToString:@"H"]) {
        if (appointmentBinary == nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }else {
        if (titleTxtFld.text == nil || [titleTxtFld.text isEqualToString:@""] || [titleTxtFld.text isEqualToString:@"Title"] || [titleTxtFld.text isEqualToString:@"None"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        if (appointmentType == nil || [appointmentType isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    if (([startsLabel.text isEqualToString:@""] || [startsLabel.text isEqualToString:@"Starts"]) || ([endsLabel.text isEqualToString:@""] || [endsLabel.text isEqualToString:@"Ends"])) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill start/end date to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    /////////////////////////////////////////////////////////////////
    ////////////////////////  Steve added Event Store ///////////////
    /////////////////////////////////////////////////////////////////
    
    //changes to systemTimeZone to show events from iCal.
    //Events from iCal are on systemTimeZone
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    
    NSDate *strtDate =[dateFormatter dateFromString:startsLabel.text];
    NSDate *endDate=[dateFormatter dateFromString:endsLabel.text];
    
    
    // eventStore = [[EKEventStore alloc] init]; //Steve commented
    event = [EKEvent eventWithEventStore:eventStore];
    
    
    
    EKCalendar *calendar = [eventStore calendarWithIdentifier:defaultcal.calendarIdentifire];
    event.calendar = calendar;
    event.location  = locTxtFld.text;
    event.notes     = [notesLabel.text isEqualToString:@"None"] ? @"" : notesLabel.text;
    //event.startDate = strtDate;
    //event.endDate   = endDate;
    event.startDate = self.startDateInternationalCompatible; //Steve - keeps dates internatiional compatible
    event.endDate   = self.endDateInternationalCompatible; //Steve - keeps dates internatiional compatible
    event.allDay    = isAllDay;
    
    
    //////////Steve - checking for iCloud Calendar.  iCloud is the only calendar that can handle timezone = nil (Fixed)
    if (calendar.source.title && [calendar.source.title isEqualToString:@"iCloud"])
    {
        if (_timeZoneSwitch.on) { //Steve - setting to nil lets timezone float and time never changes
            event.timeZone = nil;
        }
        else{
            event.timeZone = [NSTimeZone systemTimeZone];
        }
    }
    else
    {
        event.timeZone = [NSTimeZone systemTimeZone];
    }
    
    /*
     NSLog(@"event.timeZone = %@", event.timeZone);
     NSLog(@"event.calendar = %@", event.calendar);
     NSLog(@"calendar.title = %@", calendar.title);
     NSLog(@"calendar.source.title = %@", calendar.source.title);
     NSLog(@"calendar.source = %u", calendar.type);
     */
    
    
    //NSLog(@"self.startDateInternationalCompatible =  %@", self.startDateInternationalCompatible);
    //NSLog(@"self.startDateInternationalCompatible =  %@", [self.startDateInternationalCompatible descriptionWithLocale:@"GMT"]);
    //NSLog(@"ADD -------> event.timeZone =  %@", event.timeZone);
    //NSLog(@"[strtDate timeIntervalSinceDate:endDate] =  %f", [strtDate timeIntervalSinceDate:endDate]);
    //NSLog(@"%@", [NSTimeZone knownTimeZoneNames]);
    
    
    //Steve added
    if ([strtDate timeIntervalSinceDate:endDate]) {
        event.allDay = isAllDay;
    }
    
    //Handles Handwritten events
    if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
    {
        event.title = [NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType];
    }
    else
    {
        event.title = titleTxtFld.text;
    }
    
    ////Alok Added to Setting Event Store's Event Alarms/////
    NSString * StrAlert = alertsLabel.text;
    NSTimeInterval AlertTimeInterval = 0;
    NSDate *AbsAlertDate = [[NSDate alloc]init];
    
    if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
    {
        if ([StrAlert isEqualToString:@"5 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 5;
        }
        else if ([StrAlert isEqualToString:@"15 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 15;
        }
        else if ([StrAlert isEqualToString:@"30 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 30;
        }
        else if ([StrAlert isEqualToString:@"1 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 2;
        }
        else if ([StrAlert  isEqualToString:@"1 Day Before"])
        {
            AlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Day Before"])
        {
            AlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"On Date of Event"])
        {
        }
        else
        {
            //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"]; //Steve commented.  Does not conform to international styles like Europe
            [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];//Steve
            NSDate *alertDate =[dateFormatter dateFromString:StrAlert];
            
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            NSString* strAlertdate  =@"";
            
            strAlertdate = [dateFormatter stringFromDate:alertDate];
            
            if ([dateFormatter dateFromString:strAlertdate])
            {
                AbsAlertDate = [dateFormatter dateFromString:strAlertdate];
            }
        }
    }
    
    StrAlert = secondalertsLabel.text;
    NSTimeInterval SecondAlertTimeInterval = 0;
    NSDate *AbsSecondAlertDate = [[NSDate alloc]init];
    if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
    {
        if ([StrAlert isEqualToString:@"5 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 5;
        }
        else if ([StrAlert isEqualToString:@"15 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 15;
        }
        else if ([StrAlert isEqualToString:@"30 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 30;
        }
        else if ([StrAlert isEqualToString:@"1 Hour Before"])
        {
            SecondAlertTimeInterval = D_HOUR * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Hour Before"])
        {
            SecondAlertTimeInterval = D_HOUR * 2;
        }
        else if ([StrAlert  isEqualToString:@"1 Day Before"])
        {
            SecondAlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Day Before"])
        {
            SecondAlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"On Date of Event"])
        {
        }
        else
        {
            //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"]; //Steve commented.  Does not conform to international styles like Europe
            [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];//Steve
            
            NSDate *alertDate =[dateFormatter dateFromString:StrAlert];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            NSString* strAlertdate  =@"";
            strAlertdate = [dateFormatter stringFromDate:alertDate];
            
            if ([dateFormatter dateFromString:strAlertdate])
            {
                AbsSecondAlertDate = [dateFormatter dateFromString:strAlertdate];
            }
        }
    }
    NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init];
    if(SecondAlertTimeInterval >0)
    {
        EKAlarm *alarm2 = [EKAlarm alarmWithRelativeOffset:-SecondAlertTimeInterval];
        [myAlarmsArray addObject:alarm2];
    }
    else if (AbsSecondAlertDate && ![secondalertsLabel.text isEqualToString:@"None"] && ![secondalertsLabel.text isEqualToString:@"Never"] )
    {
        EKAlarm *alarm2 = [EKAlarm alarmWithAbsoluteDate:AbsSecondAlertDate];
        [myAlarmsArray addObject:alarm2];
    }
    
    
    if(AlertTimeInterval >0)
    {
        EKAlarm *alarm1 = [EKAlarm alarmWithRelativeOffset:-AlertTimeInterval];
        [myAlarmsArray addObject:alarm1];
    }
    else if (AbsAlertDate && ![alertsLabel.text isEqualToString:@"None"] && ![alertsLabel.text isEqualToString:@"Never"])
    {
        EKAlarm *alarm1 = [EKAlarm alarmWithAbsoluteDate:AbsAlertDate];
        [myAlarmsArray addObject:alarm1];
    }
    
    if([myAlarmsArray count]>0){
        event.alarms = myAlarmsArray;
    }
    
    
    //Alok Gupta Added for Repet evnets ////////////////////////////////                               //Steve - fixes bug
    if(![repeatLabel.text isEqualToString:@"Never"] && ![repeatLabel.text isEqualToString:@"None"] && repeatLabel.text != nil)
    {
        
        EKRecurrenceFrequency freq;
        int recurrenceInterval = 1;
        
        if ([repeatLabel.text isEqualToString:@"Every Day"]) {
            freq = EKRecurrenceFrequencyDaily;
        }
        else if([repeatLabel.text isEqualToString:@"Every Week"])
        {
            freq = EKRecurrenceFrequencyWeekly;
        }
        else if([repeatLabel.text isEqualToString:@"Every 2 Weeks"])
        {
            freq = EKRecurrenceFrequencyWeekly;
            recurrenceInterval =2;
        }
        else if([repeatLabel.text isEqualToString:@"Every Month"])
        {
            freq = EKRecurrenceFrequencyMonthly;
        }
        else if([repeatLabel.text isEqualToString:@"Every Year"])
        {
            freq = EKRecurrenceFrequencyYearly;
        }
        
        
        EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:freq interval:recurrenceInterval end:nil];
        [event setRecurrenceRules:[NSArray arrayWithObjects:rule, nil]];
    }
    
    
    //[eventStore saveEvent:event span:EKSpanThisEvent error:nil];  //Saves event in eventStore
    NSError *error = nil;
    BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
    
    if(!result)
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error with adding event on ical calender" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
        return;
    }
    
    // NSLog(@"ADD -------> event.eventIdentifier =  %@", event.eventIdentifier);
    // NSLog(@"ADD -------> event.timeZone =  %@", event.timeZone);
    
    ///////////////////////////////////////////////////////////////////
    //////////////////////// Steve Event Store End ///////////////////
    //////////////////////////////////////////////////////////////////
    
    
    AppointmentsDataObject* appointmentobj = [[AppointmentsDataObject alloc] init];
    
    
    
    [appointmentobj   setRelation_Field1:event.eventIdentifier];  //Steve added
    
    //[appointmentobj setStartDateTime:strStrtDate];
    //[appointmentobj setDueDateTime:strEndDate];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    [appointmentobj setStartDateTime:[dateFormatter stringFromDate:self.startDateInternationalCompatible]];//Steve
    [appointmentobj setDueDateTime:[dateFormatter stringFromDate:self.endDateInternationalCompatible]];//Steve
    
    if (notesLabel.text == nil) {
        notesLabel.text = @"None";
    }
    
    [appointmentobj setShortNotes:[notesLabel.text isEqualToString:@"None"] ? @"" : notesLabel.text];
    [appointmentobj setCalID:calendarLabel.tag];
    [appointmentobj setTextLocName:locTxtFld.text];
    
    //NSLog(@"repeatLabel.text = %@", repeatLabel.text);
    
    //Steve - fixed a bug since Repeat Label can be nil or "Never" if user on iPad, for example, & doesn't scroll the table down, repeatLabel will be nil.
    if ([repeatLabel.text isEqualToString:@"None"] || [repeatLabel.text isEqualToString:@"Never"] || repeatLabel.text == nil) {
        
        [appointmentobj setRepeat:@""];
    }
    else{
        
        [appointmentobj setRepeat:repeatLabel.text];
    }
    
    
    [appointmentobj setAlert:[alertsLabel.text isEqualToString:@"None"] ? @"" : alertsLabel.text];
    [appointmentobj setAlertSecond:[secondalertsLabel.text isEqualToString:@"None"] ? @"" : secondalertsLabel.text];//Anils Added///
    
    //NSLog(@"appointmentobj.startDateTime = %@", appointmentobj.startDateTime);
    // NSLog(@"appointmentobj.repeat = %@", appointmentobj.repeat);
    
    
    if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil) {
        [appointmentobj setAppointmentTitle:[NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType]];  //Steve changed to show HW in EventStore
        [appointmentobj setAppointmentType:appointmentType];
        [appointmentobj setAppointmentBinary:appointmentBinary];
        [appointmentobj setAppointmentBinaryLarge:appointmentBinaryLarge];
    } else {
        [appointmentobj setAppointmentTitle:titleTxtFld.text];
        [appointmentobj setAppointmentType:appointmentType];
        NSString *dataStr = @"";
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        [appointmentobj setAppointmentBinary:data];
        [appointmentobj setAppointmentBinaryLarge:data];
    }
    
    [appointmentobj setIsAllDay:isAllDay];
    
    if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
        [AllInsertDataMethods insertAppointmentDataValues:appointmentobj];
    
    
    //////////// ********* MOVED******** ////////////////
    
    
    
    //Steve - changes back to GMT timeZone
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]]; //Steve commented. Was causing a bug that change the search critera to the wrong Start and End times.
    
    // NSLog(@"*****************");
    //NSLog(@"appDelegate.currentVisibleDate = %@",appDelegate.currentVisibleDate);
    
    
    /////////////////////// Steve added - resets appDelegate.currentVisibleDate to 12AM /////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *currentVisibleDate_Date = [formater dateFromString:appDelegate.currentVisibleDate];
    
    NSCalendar *calendarToRemoveTime = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendarToRemoveTime setLocale:[NSLocale currentLocale]];
    [calendarToRemoveTime setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSDateComponents *setTimeToDayBeginning = [calendarToRemoveTime components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:currentVisibleDate_Date];
    [setTimeToDayBeginning setHour:0];
    [setTimeToDayBeginning setMinute:0];
    [setTimeToDayBeginning setSecond:0];
    
    //NSLog(@" %@", event.timeZone);
    
    
    currentVisibleDate_Date = [calendarToRemoveTime dateFromComponents:setTimeToDayBeginning];
    
    //Steve - new currentVisibleDate starting at 12AM (beginning of day)
    [appDelegate setCurrentVisibleDate:[formater stringFromDate:currentVisibleDate_Date]];
    
    
    //Steve
    if (IS_IPAD)
    {
        [self dismissViewControllerAnimated:YES completion:Nil];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
    //NSLog(@"*****************");
    //NSLog(@"appDelegate.currentVisibleDate = %@",appDelegate.currentVisibleDate);
    
}

-(IBAction)update_buttonClicked:(id)sender
{
    //NSLog(@"update_buttonClicked AppointmentViewController");
    // NSLog(@"appointmentTitle ->%@",appointmentTitle);
    
    if ([appointmentType isEqualToString:@"H"] && (appointmentTitle.length>0?[self IsHandwritingEvent:appointmentTitle]:YES))
    {
        if (appointmentBinary == nil && ![self IsHandwritingEvent:appointmentTitle])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    else
    {
        if (titleTxtFld.text == nil || [titleTxtFld.text isEqualToString:@""] || [titleTxtFld.text isEqualToString:@"Title"] || [titleTxtFld.text isEqualToString:@"None"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill the title to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    if (([startsLabel.text isEqualToString:@""] || [startsLabel.text isEqualToString:@"Starts"]) || ([endsLabel.text isEqualToString:@""] || [endsLabel.text isEqualToString:@"Ends"]))
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill start/end date to save an appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //NSLog(@"repeatLabel.text = %@", repeatLabel.text);
    //NSLog(@"aAppObj.repeat = %@", aAppObj.repeat);
    
    
    //Steve - fixes bug. "None" trigers UIActionSheet since repeatLabel.text = "Never"
    if ([aAppObj.repeat isEqualToString:@"None"] || [aAppObj.repeat isEqualToString:@""] || aAppObj.repeat == nil) {
        aAppObj.repeat = @"Never";
    }
    
    
   	if((![repeatLabel.text isEqualToString:@"Never"] && ![repeatLabel.text isEqualToString:@"None"] && ![aAppObj.repeat isEqualToString:@"Never"] && ![aAppObj.repeat isEqualToString:@"None"])|| (![repeatLabel.text isEqualToString: aAppObj.repeat]))
    {
        
        UIActionSheet *UpdateAction = [[UIActionSheet alloc]initWithTitle:@"This is a repeating event." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Save for future events" otherButtonTitles:@"Save for this event only", nil];
        [UpdateAction setTag:124];
        [UpdateAction setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        [UpdateAction showInView:self.view];
        
        return;
    }
    
    
    AppointmentsDataObject* appointmentobj = [[AppointmentsDataObject alloc] init];
    
    ////////////////////////  Steve added Event Store ///////////////
    /////////////////////////////////////////////////////////////////
    //Events from iCal are on systemTimeZone
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    
    event = [eventStore eventWithIdentifier:aAppObj.relation_Field1];
    
    
    //Steve - fixes bug so user can change calendar
    EKCalendar *calendar = [eventStore calendarWithIdentifier:defaultcal.calendarIdentifire];
    event.calendar = calendar;
    
    
    //Handles Handwritten events
    if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
    {
        event.title = [NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType];
    }
    else
    {
        event.title = titleTxtFld.text;
    }
    
    event.location  = locTxtFld.text;
    event.notes     = notesLabel.text;
    
    
    //NSLog(@" timeInterval = %f", [self.endDateInternationalCompatible timeIntervalSinceDate:self.startDateInternationalCompatible]);
    
    
    if ( [self.endDateInternationalCompatible timeIntervalSinceDate:self.startDateInternationalCompatible] > 86339) {
        event.allDay = isAllDay;
    }
    
    //NSLog(@"isAllDay =  %d", isAllDay);
    
    
    event.startDate = self.startDateInternationalCompatible; //Steve - keeps dates internatiional compatible
    event.endDate   = self.endDateInternationalCompatible;
    event.allDay    = isAllDay;
    
    if (_timeZoneSwitch.on) { //Steve - setting to nil lets timezone float and time never changes
        event.timeZone = nil;
    }
    else{
        event.timeZone = [NSTimeZone systemTimeZone];
    }
    
    
    /*
     NSLog(@"self.startDateInternationalCompatible =  %@", self.startDateInternationalCompatible);
     NSLog(@"self.startDateInternationalCompatible =  %@", [self.startDateInternationalCompatible descriptionWithLocale:@"GMT"]);
     */
    
    
    
    ////Alok Added to Setting Event Store's Event Alarms/////
    NSString * StrAlert = alertsLabel.text;
    NSTimeInterval AlertTimeInterval = 0;
    NSDate *AbsAlertDate = [[NSDate alloc]init];
    
    if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
    {
        if ([StrAlert isEqualToString:@"5 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 5;
        }
        else if ([StrAlert isEqualToString:@"15 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 15;
        }
        else if ([StrAlert isEqualToString:@"30 Minutes Before"])
        {
            AlertTimeInterval = D_MINUTE * 30;
        }
        else if ([StrAlert isEqualToString:@"1 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Hour Before"])
        {
            AlertTimeInterval = D_HOUR * 2;
        }
        else if ([StrAlert  isEqualToString:@"1 Day Before"])
        {
            AlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Day Before"])
        {
            AlertTimeInterval = D_DAY * 2;
        }
        else if ([StrAlert isEqualToString:@"On Date of Event"])
        {
        }
        else
        {
            
            AbsAlertDate = self.alert_1_Date; //Steve
            
        }
    }
    
    StrAlert = secondalertsLabel.text;
    NSTimeInterval SecondAlertTimeInterval = 0;
    NSDate *AbsSecondAlertDate = [[NSDate alloc]init];
    if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
    {
        if ([StrAlert isEqualToString:@"5 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 5;
        }
        else if ([StrAlert isEqualToString:@"15 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 15;
        }
        else if ([StrAlert isEqualToString:@"30 Minutes Before"])
        {
            SecondAlertTimeInterval = D_MINUTE * 30;
        }
        else if ([StrAlert isEqualToString:@"1 Hour Before"])
        {
            SecondAlertTimeInterval = D_HOUR * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Hour Before"])
        {
            SecondAlertTimeInterval = D_HOUR * 2;
        }
        else if ([StrAlert  isEqualToString:@"1 Day Before"])
        {
            SecondAlertTimeInterval = D_DAY * 1;
        }
        else if ([StrAlert isEqualToString:@"2 Day Before"])
        {
            SecondAlertTimeInterval = D_DAY * 2;
        }
        else if ([StrAlert isEqualToString:@"On Date of Event"])
        {
        }
        else
        {
            AbsSecondAlertDate = self.alert_2_Date; //Steve
        }
    }
    
    
    NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init];
    if(SecondAlertTimeInterval >0)
    {
        EKAlarm *alarm2 = [EKAlarm alarmWithRelativeOffset:-SecondAlertTimeInterval];
        [myAlarmsArray addObject:alarm2];
    }
    else if (AbsSecondAlertDate && ![secondalertsLabel.text
                                     isEqualToString:@"None"] && ![secondalertsLabel.text
                                                                   isEqualToString:@"Never"])
    {
        EKAlarm *alarm2 = [EKAlarm alarmWithAbsoluteDate:AbsSecondAlertDate];
        [myAlarmsArray addObject:alarm2];
    }
    
    if(AlertTimeInterval >0)
    {
        EKAlarm *alarm1 = [EKAlarm alarmWithRelativeOffset:-AlertTimeInterval];
        [myAlarmsArray addObject:alarm1];
    }
    else if (AbsAlertDate && ![alertsLabel.text
                               isEqualToString:@"None"] && ![alertsLabel.text
                                                             isEqualToString:@"Never"])
    {
        EKAlarm *alarm1 = [EKAlarm alarmWithAbsoluteDate:AbsAlertDate];
        [myAlarmsArray addObject:alarm1];
    }
    
    
    event.alarms = myAlarmsArray;
    
    
    //NSLog(@"repeatLabel.text = %@", repeatLabel.text);
    
    //Alok Gupta Modified for Repet evnets ////////////////////////////////
    
    if(![repeatLabel.text isEqualToString:@"Never"] && ![repeatLabel.text isEqualToString:@"None"] && repeatLabel.text != nil) //Steve Added repeatLabel.text != nil
    {
        EKRecurrenceFrequency freq;
        int recurrenceInterval = 1;
        if ([repeatLabel.text isEqualToString:@"Every Day"]) {
            freq = EKRecurrenceFrequencyDaily;
        }
        else if([repeatLabel.text isEqualToString:@"Every Week"])
        {
            freq = EKRecurrenceFrequencyWeekly;
        }
        else if([repeatLabel.text isEqualToString:@"Every 2 Week"])
        {
            freq = EKRecurrenceFrequencyWeekly;
            recurrenceInterval =2;
        }
        else if([repeatLabel.text isEqualToString:@"Every Month"])
        {
            freq = EKRecurrenceFrequencyMonthly;
        }
        else if([repeatLabel.text isEqualToString:@"Every Year"])
        {
            freq = EKRecurrenceFrequencyYearly;
        }
        
        
        EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:freq interval:recurrenceInterval end:nil];
        [event setRecurrenceRules:[NSArray arrayWithObjects:rule, nil]];
    }
    
    
    
    //Steve - fixes bug when changing an All Day event to a normal event.  Not sure why its doing it, but this fixes it.
    event.startDate = self.startDateInternationalCompatible; //Steve - keeps dates internatiional compatible
    event.endDate   = self.endDateInternationalCompatible;
    
    
    NSError *error;
    BOOL result =[eventStore saveEvent:event span:EKSpanThisEvent error:&error];
    //NSLog(@"event--->%@",[event description]);
    
    
    /*
     NSLog(@"event.startDate =  %@", event.startDate);
     NSLog(@"event.startDate =  %@", [event.startDate descriptionWithLocale:@"GMT"]);
     
     NSLog(@"event.endDate =  %@", event.endDate);
     NSLog(@"event.endDate =  %@", [event.endDate descriptionWithLocale:@"GMT"]);
     */
    
    
    if(!result)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error while updating event on ical." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        NSLog(@"ERROR: %@",error);
        return;
    }
    
    //////////////////////// Steve Event Store End ///////////////////
    // NSLog(@"aAppObj.appointmentID->%d",aAppObj.appointmentID);
    
    [appointmentobj setRelation_Field1:event.eventIdentifier];
    [appointmentobj setAppointmentID:aAppObj.appointmentID];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    [appointmentobj setStartDateTime:[dateFormatter stringFromDate:self.startDateInternationalCompatible]];//Steve
    [appointmentobj setDueDateTime:[dateFormatter stringFromDate:self.endDateInternationalCompatible]];//Steve
    [appointmentobj setTimeZoneName:aAppObj.timeZoneName]; //Steve
    
    [appointmentobj setShortNotes:([notesLabel.text isEqualToString:@"None"]  || [notesLabel.text length]<1)? @"" : notesLabel.text];
    [appointmentobj setCalID:calendarLabel.tag];
    [appointmentobj setTextLocName:locTxtFld.text];
    [appointmentobj setAlert:[alertsLabel.text isEqualToString:@"None"] ? @"" : alertsLabel.text];
    [appointmentobj setAlertSecond:[secondalertsLabel.text isEqualToString:@"None"] ? @"" : secondalertsLabel.text];//Anil's Added///
    
    
    //[appointmentobj setRepeat:[repeatLabel.text isEqualToString: @"None"] ? @"" : repeatLabel.text]; //Steve commented
    
    //Steve - fixed a bug since Repeat Label can be nil or "Never" if user on iPad, for example, & doesn't scroll the table down, repeatLabel will be nil.
    if ([repeatLabel.text isEqualToString:@"None"] || [repeatLabel.text isEqualToString:@"Never"] || repeatLabel.text == nil) {
        
        [appointmentobj setRepeat:@""];
    }
    else{
        
        [appointmentobj setRepeat:repeatLabel.text];
    }
    
    
    BOOL isUpdated;
    [appointmentobj setIsAllDay:isAllDay];
    
    
    if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
    {
        [appointmentobj setAppointmentTitle:@""];
        [appointmentobj setAppointmentType:appointmentType];
        [appointmentobj setAppointmentBinary:appointmentBinary];
        [appointmentobj setAppointmentBinaryLarge:appointmentBinaryLarge];
        
        isUpdated = [AllInsertDataMethods updateAppointmentDataValues:appointmentobj UpdateLargeImage:YES];
        
    }
    
    
    
    //Steve
    if (IS_IPAD) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else{ //iPhone
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}


-(UIColor*) getCalNameData:(NSInteger)CalID {
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3_stmt *selectAllCalNameData = nil;
    UIColor *calColor = [UIColor clearColor];
    if(selectAllCalNameData == nil && appDelegate.database) {
        
        NSString* Sql = [[NSString alloc] initWithFormat:@"Select CalendarID, CalendarName, CalendarColorRed, CalendarColorGreen, CalendarColorBlue, isOrganizerCalendar, isDefaultCalendar From CalendarTable Where CalendarID =%d",CalID];
        
        if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAllCalNameData, NULL) == SQLITE_OK) {
            while (sqlite3_step(selectAllCalNameData) == SQLITE_ROW) {
                calColor = [[UIColor alloc] initWithRed:sqlite3_column_double(selectAllCalNameData, 2) green:sqlite3_column_double(selectAllCalNameData, 3) blue:sqlite3_column_double(selectAllCalNameData, 4) alpha:1];
            }
            sqlite3_finalize(selectAllCalNameData);
            selectAllCalNameData = nil;
        }
    }
    
    return calColor;
}
-(BOOL) setAlertForSpecificWeekDay:(int)_weekDay
{
    NSDate *today = notification.fireDate;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:today];
    int weekday = [weekdayComponents weekday];
    
    
    int iOffsetToFryday = -weekday+_weekDay;
    
    if(iOffsetToFryday <=0)
    {
        iOffsetToFryday = 7 + iOffsetToFryday;
    }
    
    weekdayComponents.weekday = iOffsetToFryday;
    
    NSDate *nextFriday = [[NSCalendar currentCalendar] dateByAddingComponents:weekdayComponents toDate:today options:0];
    
    
    notification.fireDate = nextFriday;
    
    notification.repeatInterval = NSWeekdayCalendarUnit;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    
    
    return YES;
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    //Action Sheet tag 123 for delete
    
    if(actionSheet.cancelButtonIndex == buttonIndex)return;
    
    if(actionSheet.tag==123){
        
        if(buttonIndex ==0){
            //NSLog(@"REMOVE ------->  = aAppObj.relation_Field1 %@", aAppObj.relation_Field1);
            
            
            EKEvent *eventToRemove = [eventStore eventWithIdentifier:aAppObj.relation_Field1];
            
            NSDateFormatter *formater = [[NSDateFormatter alloc] init];
            [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            if(![aAppObj.repeat isEqualToString:@"None"] && ![aAppObj.repeat isEqualToString:@"Never"] && [aAppObj.repeat length]>0)
            {
                
                //Steve changed - shows correct date for predicate.
                NSDate *eventStartDate_search = [formater dateFromString:aAppObj.startDateTime];
                NSDate *eventEndDate_search = [formater dateFromString:aAppObj.dueDateTime];
                
                
                // NSPredicate *predicate =
                //[store predicateForEventsWithStartDate:eventStartDate_search endDate:eventEndDate_search calendars:nil];
                //NSArray *events = [store eventsMatchingPredicate:predicate];
                
                
                NSPredicate *predicate =
                [eventStore predicateForEventsWithStartDate:eventStartDate_search endDate:eventEndDate_search calendars:nil];
                NSArray *events = [eventStore eventsMatchingPredicate:predicate];
                
                
                
                if([events count]>0){
                    NSPredicate *filter1 = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", aAppObj.appointmentTitle];
                    
                    NSArray * filteredTerms = [events filteredArrayUsingPredicate:filter1];
                    
                    eventToRemove = [filteredTerms objectAtIndex:0];
                }
            }
            
            
            
            
            BOOL result =NO;
            NSError* error = nil;
            
            
            
            if (eventToRemove != nil) {
                result=[eventStore removeEvent:eventToRemove span:EKSpanThisEvent error:&error];
            }
            
            if(!result){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error: while Deleting event from calender" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                NSLog(@"ERROR: %@",error);
                return;
            }
            else{
                ////TO Delete Events From data base For Handwritting Only
                if([aAppObj.appointmentType isEqualToString:@"H"])
                {
                    BOOL isDeleted = [GetAllDataObjectsClass deleteAppointmentWithID:aAppObj.appointmentID];
                    if (!isDeleted) {
                        NSLog(@"Error On Deleting Handwriting Appointment From database");
                    }
                }
            }
            
            //Steve commented - for old Nav???
            //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
            
            //Steve
            if (IS_IPAD) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
            else{ //iPhone
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } //delete this event only
        
        
        if(buttonIndex ==1){ //Delete all future events
            
            
            EKEvent *eventToRemove = [eventStore eventWithIdentifier:aAppObj.relation_Field1];//Steve
            
            
            NSDateFormatter *formater = [[NSDateFormatter alloc] init];
            [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
            
            
            if(![aAppObj.repeat isEqualToString:@"None"] && ![aAppObj.repeat isEqualToString:@"Never"] && [aAppObj.repeat length]>0)
            {
                
                //Steve changed - shows correct date for predicate.
                NSDate *eventStartDate_search = [formater dateFromString:aAppObj.startDateTime];
                NSDate *eventEndDate_search = [formater dateFromString:aAppObj.dueDateTime];
                
                
                NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:eventStartDate_search endDate:eventEndDate_search calendars:nil];
                NSArray *events = [eventStore eventsMatchingPredicate:predicate];
                
                if([events count]>0){
                    NSPredicate *filter1 = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", aAppObj.appointmentTitle];
                    
                    NSArray * filteredTerms = [events filteredArrayUsingPredicate:filter1];
                    
                    eventToRemove = [filteredTerms objectAtIndex:0];//[eventStore eventWithIdentifier:appObj.relation_Field1];
                }
            }
            else{
                eventToRemove = [eventStore eventWithIdentifier:aAppObj.relation_Field1];
            }
            
            BOOL result =NO;
            NSError* error = nil;
            
            if (eventToRemove != nil) {
                result=[eventStore removeEvent:eventToRemove span:EKSpanFutureEvents error:&error];
            }
            
            if(!result){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error: while Deleting event from calender" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                //NSLog(@"ERROR: %@",error);
                return;
            }
            
            ////TO Delete Events From data base For Handwritting Only
            if([aAppObj.appointmentType isEqualToString:@"H"])
            {
                BOOL isDeleted = [GetAllDataObjectsClass deleteAppointmentWithID:aAppObj.appointmentID];
                if (isDeleted == YES) {
                    //[appObjArray removeObjectAtIndex:actionSheet.tag];
                }
            }
            
            
            //Steve
            if (IS_IPAD) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
            else{ //iPhone
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            //Steve - for old Nav???
            // [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
            
        } //Delete all future events
        
        
        
        
    } //tag == 123
    
    ///////////////////////////////
    
    
    //Action Sheet tag 124 for Modification
    if(actionSheet.tag == 124 && (buttonIndex == 0 || buttonIndex == 1)){
        
        //NSLog(@"update_buttonClicked AppointmentViewController");
        
        
        AppointmentsDataObject* appointmentobj = [[AppointmentsDataObject alloc] init];
        
        ////////////////////////  Steve added Event Store ///////////////
        /////////////////////////////////////////////////////////////////
        //Events from iCal are on systemTimeZone
        [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        if (buttonIndex ==0)
            event = [eventStore eventWithIdentifier:aAppObj.relation_Field1];
        else
            event = [EKEvent eventWithEventStore:eventStore];
        
        
        //Handles Handwritten events
        if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
        {
            event.title = [NSString stringWithFormat:@"InFocus Handwritten Event on %@",appDelegate.DeviceType];
        }
        else
        {
            event.title = titleTxtFld.text;
        }
        
        event.location  = locTxtFld.text;
        event.notes     = notesLabel.text;
        event.startDate = self.startDateInternationalCompatible; //Steve - keeps dates internatiional compatible
        event.endDate   = self.endDateInternationalCompatible;
        event.allDay    = isAllDay;
        
        ////Alok Added to Setting Event Store's Event Alarms/////
        NSString * StrAlert = alertsLabel.text;
        NSTimeInterval AlertTimeInterval = 0;
        NSDate *AbsAlertDate = [[NSDate alloc]init];
        if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
        {
            if ([StrAlert isEqualToString:@"5 Minutes Before"])
            {
                AlertTimeInterval = D_MINUTE * 5;
            }
            else if ([StrAlert isEqualToString:@"15 Minutes Before"])
            {
                AlertTimeInterval = D_MINUTE * 15;
            }
            else if ([StrAlert isEqualToString:@"30 Minutes Before"])
            {
                AlertTimeInterval = D_MINUTE * 30;
            }
            else if ([StrAlert isEqualToString:@"1 Hour Before"])
            {
                AlertTimeInterval = D_HOUR * 1;
            }
            else if ([StrAlert isEqualToString:@"2 Hour Before"])
            {
                AlertTimeInterval = D_HOUR * 2;
            }
            else if ([StrAlert  isEqualToString:@"1 Day Before"])
            {
                AlertTimeInterval = D_DAY * 1;
            }
            else if ([StrAlert isEqualToString:@"2 Day Before"])
            {
                AlertTimeInterval = D_DAY * 1;
            }
            else if ([StrAlert isEqualToString:@"On Date of Event"])
            {
            }
            else
            {
                [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
                NSDate *alertDate =[dateFormatter dateFromString:StrAlert];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                NSString* strAlertdate  =@"";
                strAlertdate = [dateFormatter stringFromDate:alertDate];
                if ([dateFormatter dateFromString:strAlertdate])
                {
                    AbsAlertDate = [dateFormatter dateFromString:strAlertdate];
                }
            }
        }
        
        StrAlert = secondalertsLabel.text;
        NSTimeInterval SecondAlertTimeInterval = 0;
        NSDate *AbsSecondAlertDate = [[NSDate alloc]init];
        if (![StrAlert isEqualToString:@"None"] && ![StrAlert isEqualToString:@"Never"])
        {
            if ([StrAlert isEqualToString:@"5 Minutes Before"])
            {
                SecondAlertTimeInterval = D_MINUTE * 5;
            }
            else if ([StrAlert isEqualToString:@"15 Minutes Before"])
            {
                SecondAlertTimeInterval = D_MINUTE * 15;
            }
            else if ([StrAlert isEqualToString:@"30 Minutes Before"])
            {
                SecondAlertTimeInterval = D_MINUTE * 30;
            }
            else if ([StrAlert isEqualToString:@"1 Hour Before"])
            {
                SecondAlertTimeInterval = D_HOUR * 1;
            }
            else if ([StrAlert isEqualToString:@"2 Hour Before"])
            {
                SecondAlertTimeInterval = D_HOUR * 2;
            }
            else if ([StrAlert  isEqualToString:@"1 Day Before"])
            {
                SecondAlertTimeInterval = D_DAY * 1;
            }
            else if ([StrAlert isEqualToString:@"2 Day Before"])
            {
                SecondAlertTimeInterval = D_DAY * 1;
            }
            else if ([StrAlert isEqualToString:@"On Date of Event"])
            {
            }
            else
            {
                [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
                NSDate *alertDate =[dateFormatter dateFromString:StrAlert];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
                NSString* strAlertdate  =@"";
                strAlertdate = [dateFormatter stringFromDate:alertDate];
                if ([dateFormatter dateFromString:strAlertdate])
                {
                    AbsSecondAlertDate = [dateFormatter dateFromString:strAlertdate];
                }
            }
        }
        
        
        NSMutableArray *myAlarmsArray = [[NSMutableArray alloc] init];
        if(SecondAlertTimeInterval >0)
        {
            EKAlarm *alarm2 = [EKAlarm alarmWithRelativeOffset:-SecondAlertTimeInterval];
            [myAlarmsArray addObject:alarm2];
        }
        else if (AbsSecondAlertDate && ![secondalertsLabel.text
                                         isEqualToString:@"None"] && ![secondalertsLabel.text
                                                                       isEqualToString:@"Never"])
        {
            EKAlarm *alarm2 = [EKAlarm alarmWithAbsoluteDate:AbsSecondAlertDate];
            [myAlarmsArray addObject:alarm2];
        }
        
        if(AlertTimeInterval >0)
        {
            EKAlarm *alarm1 = [EKAlarm alarmWithRelativeOffset:-AlertTimeInterval];
            [myAlarmsArray addObject:alarm1];
        }
        else if (AbsAlertDate && ![alertsLabel.text
                                   isEqualToString:@"None"] && ![alertsLabel.text
                                                                 isEqualToString:@"Never"])
        {
            EKAlarm *alarm1 = [EKAlarm alarmWithAbsoluteDate:AbsAlertDate];
            [myAlarmsArray addObject:alarm1];
        }
        
        
        event.alarms = myAlarmsArray;
        
        
        //NSLog(@"repeatLabel.text = %@", repeatLabel.text);
        
        //Alok Gupta Modified for Repet evnets ////////////////////////////////
        if(![repeatLabel.text isEqualToString:@"Never"] && ![repeatLabel.text isEqualToString:@"None"])
        {
            EKRecurrenceFrequency freq;
            int recurrenceInterval = 1;
            if ([repeatLabel.text isEqualToString:@"Every Day"]) {
                freq = EKRecurrenceFrequencyDaily;
            }
            else if([repeatLabel.text isEqualToString:@"Every Week"])
            {
                freq = EKRecurrenceFrequencyWeekly;
            }
            else if([repeatLabel.text isEqualToString:@"Every 2 Week"])
            {
                freq = EKRecurrenceFrequencyWeekly;
                recurrenceInterval =2;
            }
            else if([repeatLabel.text isEqualToString:@"Every Month"])
            {
                freq = EKRecurrenceFrequencyMonthly;
            }
            else if([repeatLabel.text isEqualToString:@"Every Year"])
            {
                freq = EKRecurrenceFrequencyYearly;
            }
            
            
            EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:freq interval:recurrenceInterval end:nil];
            [event setRecurrenceRules:[NSArray arrayWithObjects:rule, nil]];
        }
        else { [event setRecurrenceRules:nil];}
        
        
        
        [event setCalendar:[eventStore calendarWithIdentifier:defaultcal.calendarIdentifire]];
        // NSLog(@"event--->%@",[event description]);
        
        NSError *error;
        BOOL result = NO;
        
        if (buttonIndex == 0) {
            
            result=[eventStore saveEvent:event span:EKSpanFutureEvents error:&error];
        }
        else if(buttonIndex ==1){
            [event setRecurrenceRules:nil];
            //NSLog(@"Event-->%@",event);
            result=[eventStore saveEvent:event span:EKSpanThisEvent error:&error];
        }
        
        
        if(!result)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error while updating event on ical." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            NSLog(@"ERROR: %@",error);
            return;
        }
        
        //////////////////////// Steve Event Store End ///////////////////
        // NSLog(@"aAppObj.appointmentID->%d",aAppObj.appointmentID);
        
        [appointmentobj setAppointmentID:aAppObj.appointmentID];
        [appointmentobj setRelation_Field1:event.eventIdentifier];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        [appointmentobj setStartDateTime:[dateFormatter stringFromDate:self.startDateInternationalCompatible]];//Steve
        [appointmentobj setDueDateTime:[dateFormatter stringFromDate:self.endDateInternationalCompatible]];//Steve
        
        [appointmentobj setShortNotes:([notesLabel.text isEqualToString:@"None"] ||[notesLabel.text length]<1) ? @"" : notesLabel.text];
        [appointmentobj setCalID:calendarLabel.tag];
        [appointmentobj setTextLocName:locTxtFld.text];
        [appointmentobj setRepeat:[repeatLabel.text isEqualToString: @"None"] ? @"" : repeatLabel.text];
        [appointmentobj setAlert:[alertsLabel.text isEqualToString:@"None"] ? @"" : alertsLabel.text];
        [appointmentobj setAlertSecond:[secondalertsLabel.text isEqualToString:@"None"] ? @"" : secondalertsLabel.text];//Anil's Added///
        
        BOOL isUpdated;
        [appointmentobj setIsAllDay:isAllDay];
        
        
        if ([appointmentType isEqualToString:@"H"] && appointmentBinary!=nil)
        {
            [appointmentobj setAppointmentTitle:@""];
            [appointmentobj setAppointmentType:appointmentType];
            [appointmentobj setAppointmentBinary:appointmentBinary];
            [appointmentobj setAppointmentBinaryLarge:appointmentBinaryLarge];
            isUpdated = [AllInsertDataMethods updateAppointmentDataValues:appointmentobj UpdateLargeImage:YES];
        }
        
        
        //Steve commented - for old Nav ???
        appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
        //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:([self.navigationController.viewControllers count] - 3)] animated:YES];
        
        
        //Steve
        if (IS_IPAD) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        else{ //iPhone
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
}



-(NSArray*)findsCalendarsToShow{
    
    NSMutableArray *calendarArray = [[NSMutableArray alloc]init];
    //self.calendarDictionary = [[NSMutableDictionary alloc]init];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Make an array of UserDefaults for Calendars that don't show
    NSArray *calendarsThatDontShowArray = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] copy]; //make  copy
    NSMutableArray *discardedItems = [NSMutableArray array]; //make an array of objects to be removed
    
    
    for (EKSource *source in eventStore.sources) {
        
        //[self.calendarArray removeAllObjects];
        
        
        //NSLog(@" *********************************************");
        //NSLog(@" source  = %@", source.title);
        //NSLog(@" calendarsThatDontShowArray  = %@", calendarsThatDontShowArray);
        //NSLog(@" *********************************************");
        
        
        
        
        NSSet *calendars = [source calendarsForEntityType:EKEntityTypeEvent];
        
        
        for (EKCalendar *calendar in calendars) {
            
            /*
             NSLog(@" *********************************************");
             NSLog(@" Calendar source Title = %@", calendar.source.title);
             NSLog(@" Calendar Title = %@", calendar.title);
             NSLog(@" Calendar Type = %d", calendar.type);
             //NSLog(@" Calendar CGColor = %@", calendar.CGColor);
             //NSLog(@" Calendar allowsContentModifications = %d", calendar.allowsContentModifications);
             NSLog(@" Calendar calendarIdentifier = %@", calendar.calendarIdentifier);
             */
            
            
            
            if (calendarsThatDontShowArray.count == 0) { //if nothing in UserDefaults not to show, then show all
                
                [calendarArray addObject: calendar]; //builds Calendar Array
            }
            
            
            CalendarDataObject *calendarObject = [[CalendarDataObject alloc]init];
            
            [calendarObject setCalendarIdentifire:calendar.calendarIdentifier];
            
            for (NSString *calendarIdentifier in calendarsThatDontShowArray) { //iterate thru copy of array of UserDefaults CalendarsDoNotShow
                
                if ([calendarObject.calendarIdentifire isEqualToString:calendarIdentifier]){ //looks for items that Dont match
                    
                    [discardedItems addObject: calendar]; //builds array of items to be removed from Calendar Array
                    break;
                    
                }
                else{
                    
                    [calendarArray addObject: calendar]; //builds Calendar Array
                }
                
            }
            
        }
        
        
    }
    
    //NSLog(@" calendarArray 1 = %@", calendarArray);
    
    [calendarArray removeObjectsInArray:discardedItems]; //removes all discarded items
    
    // NSLog(@" discardedItems = %@", discardedItems);
    //NSLog(@" calendarArray after discarded items removed = %@", calendarArray);
    //NSLog(@" *********************************************");
    
    
    return calendarArray;
}



-(IBAction) delete_buttonClicked:(id)sender {
    
    UIActionSheet *ActionSheet;
    
    if([aAppObj.repeat isEqualToString:@"None"] || [aAppObj.repeat isEqualToString:@"Never"])
        ActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure to delete this event?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Event" otherButtonTitles:nil,nil];
    else
        ActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure to delete this event?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete This Event Only" otherButtonTitles:@"Delete All Future Events",nil];
    
    [ActionSheet setTag:123];
    [ActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [ActionSheet showInView:self.view];
}

//Alok Added For Checking InFocus Handwritten Event
-(BOOL)IsHandwritingEvent:(NSString *) EventName{
    if([EventName length] >= 25)
    {
        if([[EventName substringToIndex:25] isEqualToString:@"InFocus Handwritten Event"])
            return YES;
        else return NO;
        
    }
    return NO;
}


#pragma mark -
#pragma mark Picker
#pragma mark -


-(void) startPickerChanged{ //startDate Picker changed value
    
    if (self.allDaySwitch.on) { //Steve added
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:( NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:self.startDatePicker.date];
        
        [components setHour:00];
        [components setMinute:00];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.startDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
        
        //NSLog(@" self.startDateInternationalCompatible = %@",  self.startDateInternationalCompatible);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        //Sets startLabel with No Time
        startsLabel.text = [dateFormatter stringFromDate:self.startDateInternationalCompatible];
        
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.endDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
        
        endsLabel.text = [dateFormatter stringFromDate:self.endDateInternationalCompatible];
        
        [self.myTableView reloadData];
        
    }
    else{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        //[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        NSDate *newStartDate_Date = [[NSDate alloc]init];
        newStartDate_Date = self.startDatePicker.date;
        startsLabel.text = [formatter stringFromDate:newStartDate_Date];
        
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:( NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:self.startDatePicker.date];
        
        //Steve - for startDate at 23:00 (11:00 PM), I don't want to show EndDate on the next day, so make EndTime Automaticall go to 23:55
        if (components.hour == 23 && components.minute == 0) {
            
            [components setHour:23];
            [components setMinute:00];
            [components setSecond:00];
            
            self.startDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
            startsLabel.text = [formatter stringFromDate:self.startDateInternationalCompatible];
            
            [components setHour:23];
            [components setMinute:55];
            [components setSecond:00];
            
            self.endDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
            endsLabel.text = [formatter stringFromDate:self.endDateInternationalCompatible];
 
        }
        else{ //otherwise add 1 hour to end time
            
            NSDate *newEndDate_Date = [[NSDate alloc]init];
            newEndDate_Date = [newStartDate_Date dateByAddingHours:1];
            endsLabel.text = [formatter stringFromDate:newEndDate_Date];
            
            self.startDateInternationalCompatible = newStartDate_Date;
            self.endDateInternationalCompatible = newEndDate_Date;
        }
        

        
        [self.myTableView reloadData];
    }
    
    
}

-(void) endPickerChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    //[formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *newEndDate_Date = [[NSDate alloc]init];
    newEndDate_Date = self.endDatePicker.date;
    
    /*
     NSDate *newStartDate_Date = [[NSDate alloc]init];
     newStartDate_Date = self.startDatePicker.date;
     */
    
    NSDate *newStartDate_Date = [[NSDate alloc]init];
    newStartDate_Date = self.startDateInternationalCompatible;
    
    //self.startDateInternationalCompatible = newStartDate_Date;
    self.endDateInternationalCompatible = newEndDate_Date;
    
    
    NSComparisonResult result = [newStartDate_Date compare:newEndDate_Date];
    
    if (result == NSOrderedDescending) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"End date can't be a date before Starting date. Please enter a date greater than starting date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [titleTxtFld resignFirstResponder];
        
    }
    else{
        
        endsLabel.text = [formatter stringFromDate:newEndDate_Date];
        [self.myTableView reloadData];
    }
    
    
}


//Alert 1 Picker Value Changed
-(void) alert_1_PickerValueChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    
    self.alert_1_Date = self.alert_1_Picker.date;
    
    alertsLabel.text = [formatter stringFromDate:self.alert_1_Date];
    
    
    [self.myTableView reloadData];
    
}


//Alert 2 Picker Value Changed
-(void) alert_2_PickerValueChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    
    self.alert_2_Date = self.alert_2_Picker.date;
    
    secondalertsLabel.text = [formatter stringFromDate:self.alert_2_Date];
    
    
    [self.myTableView reloadData];
    
}


-(void) allDaySwitchChangedValue{
    
    
    if (self.allDaySwitch.on) {
        
        isAllDay = YES;
        self.startDatePicker.datePickerMode = UIDatePickerModeDate;
        self.endDatePicker.datePickerMode = UIDatePickerModeDate;
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:( NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:self.startDateInternationalCompatible];
        
        [components setHour:00];
        [components setMinute:00];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.startDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
        
        //NSLog(@" self.startDateInternationalCompatible = %@",  self.startDateInternationalCompatible);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        //Sets startLabel with No Time
        startsLabel.text = [dateFormatter stringFromDate:self.startDateInternationalCompatible];
        
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:00];
        
        //Sets date to beginning of day
        self.endDateInternationalCompatible = [currentCalendarUsed dateFromComponents:components];
        
        endsLabel.text = [dateFormatter stringFromDate:self.endDateInternationalCompatible];
        
        [self.myTableView reloadData];
        
    }
    else{
        
        isAllDay = NO;
        
        self.startDatePicker.timeZone = [NSTimeZone localTimeZone];
        self.startDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
        self.endDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
        
        
        //Set new start date uning [NSDate date] since its an absolute time
        //needed since coming from "All Day", event has no timezone & therefore causes bugs
        NSDate *todayDate = [NSDate date];
        
        NSCalendar *currentCalendarUsed = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendarUsed components:NSMinuteCalendarUnit
                                                              fromDate:todayDate
                                                                toDate:self.startDateInternationalCompatible
                                                               options:0];
        
        
        NSInteger minutes = [components minute];
        NSDate *newDate = [todayDate dateByAddingMinutes:minutes];
        
        
        //NSLog(@"components = %@", components);
        //NSLog(@"minutes = %d", minutes);
        //NSLog(@"self.startDateInternationalCompatible = %@", self.startDateInternationalCompatible);
        //NSLog(@"self.startDateInternationalCompatible = %@", [self.startDateInternationalCompatible                                  descriptionWithLocale:@"GMT"]);
        //NSLog(@"newDate = %@", newDate);
        //NSLog(@"newDate = %@", [newDate descriptionWithLocale:@"GMT"]);
        
        
        
        self.startDateInternationalCompatible = newDate;
        self.startDatePicker.date = newDate;
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        //get date strings
        startsLabel.text = [dateFormatter stringFromDate:self.startDateInternationalCompatible];
        
        //End Date add 1 hour
        NSDate *newEndDate = [self.startDateInternationalCompatible dateByAddingHours:1];
        self.endDateInternationalCompatible = newEndDate;
        
       
        
        endsLabel.text = [dateFormatter stringFromDate:newEndDate];
        
        //NSLog(@"self.startDate_Date = %@", self.startDateInternationalCompatible);
        //NSLog(@"self.startDate_Date = %@", [self.startDateInternationalCompatible descriptionWithLocale:@"GMT"]);
        
        [self.myTableView reloadData];
        
    }
    
    
}


-(void) alert_1SegmentedControlValueChanged{
    
    
    NSInteger selectedSegment = self.Alert_1segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        self.alert_1_Picker.hidden = YES;
        self.alertCustomPicker_1.hidden = NO;
        
        alertsLabel.text = @"None";
        
        
        [self.myTableView reloadData];
        
        
    }
    else{
        
        self.alert_1_Picker.hidden = NO;
        self.alertCustomPicker_1.hidden = YES;
        
        self.alert_1_Date = self.alert_1_Picker.date;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        alertsLabel.text = [formatter stringFromDate:self.alert_1_Date];
        
        [self.myTableView reloadData];
        
    }
    
}


-(void) alert_2SegmentedControlValueChanged{
    
    NSInteger selectedSegment = self.Alert_2segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        self.alert_2_Picker.hidden = YES;
        self.alertCustomPicker_2.hidden = NO;
        
        secondalertsLabel.text = @"None";
        
        
        [self.myTableView reloadData];
        
        
    }
    else{
        
        self.alert_2_Picker.hidden = NO;
        self.alertCustomPicker_2.hidden = YES;
        
        self.alert_2_Date = self.alert_2_Picker.date;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        secondalertsLabel.text = [formatter stringFromDate:self.alert_2_Date];
        
        [self.myTableView reloadData];
        
    }
    
    
}

-(void)timeZoneSwitchedChanged{
    
    //Steve
    if (_timeZoneSwitch.on) { //Steve - setting to nil lets timezone float and time never changes
        aAppObj.timeZoneName = nil;
    }
    else{
        aAppObj.timeZoneName = [NSTimeZone systemTimeZone];
    }
    
    //NSLog(@"aAppObj.timeZoneName =  %@", aAppObj.timeZoneName);
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thepickerView
{
    return 1;
}


- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.alertPickerArray count];
}


- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.alertPickerArray objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    
    if (self.alertCustomPicker_1 == pickerView) {
        
        NSString *selectedRowInPicker = [self.alertPickerArray objectAtIndex:row];
        alertsLabel.text = selectedRowInPicker;
    }
    
    if (self.alertCustomPicker_2 == pickerView) {
        
        NSString *selectedRowInPicker = [self.alertPickerArray objectAtIndex:row];
        secondalertsLabel.text = selectedRowInPicker;
    }
    
    
}


#pragma mark -
#pragma mark TableView Methods
#pragma mark -

// Customize the number of sections in the table view.

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 15;
    return 7.0; //1
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 15.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections;
    
    if (isAdd)
        sections = 6;
    else
        sections = 7;
    
    
    return sections;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Steve - removes the borders & cell background from the deleteButton cell
    if([cell.reuseIdentifier isEqualToString:@"deleteButtonCell"]){
        cell.backgroundView = nil;
        
    }
    
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger rows;
    
    if (section == 0) //Title - Location
        rows = 2;
    else if (section == 1) //Start - End
        
        if (IS_IOS_7){
            
            if (isAdd){
                
                if (_isiCloudCalendar)
                    rows = 6;//only show Time Zone option when adding
                else
                    rows = 5;
            }
            else
                rows = 5;
            
        }
        else
            rows = 1;
    
        else if (section == 2)  //iOS 7 -->Alert, iOS 6 --> Repeat
            
            if (IS_IOS_7) {
                rows = 4; //iOS 7 -->Alert
            }
            else
                rows = 1; //iOS 6 --> Repeat
    
            else if (section == 3){ //iOS 7 --> Repeat, iOS 6 --> Alert 1 & 2
                
                if (IS_IOS_7)
                    rows = 1; //iOS 7 --> Repeat
                else
                    rows = 2; //iOS 6 --> Alert 1 & 2
                
            }
            else if (section == 4) //Notes
                rows = 1;
            else if (section == 5) //Calendar
                rows = 1;
            else if ( section == 6){ //Delete Button
                
                if (isAdd)
                    rows = 0;
                else
                    rows = 1;
            }
    
    
    
    return rows;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int height;
    int heightOfPicker = 216; //Picker Height
    
    if (IS_IOS_7) {
        
        if (indexPath.section == 1 && indexPath.row == 2 && isShowStartDatePicker) { //Start Date Picker On
            height = heightOfPicker;
        }
        else if (indexPath.section == 1 && indexPath.row == 2 && !isShowStartDatePicker){ //StartDate picker Off
            height = 0;
        }
        else if (indexPath.section == 1 && indexPath.row == 4 && isShowEndDatePicker){ //EndDate Picker On
            height = heightOfPicker;
        }
        else if (indexPath.section == 1 && indexPath.row == 4 && !isShowEndDatePicker){//EndDate Picker Off
            height = 0;
        }
        //else if (indexPath.section == 1 && indexPath.row == 5 && isAdd){//Time Zone if Adding new Event
        //    height = 92;
        //}
        else if (indexPath.section == 1 && indexPath.row == 5){//Time Zone if Adding new Event
            height = 102;
        }
        else if (indexPath.section == 2 && indexPath.row == 1 && !isShowAlert_1_Picker){ //Alert 1 picker off
            height = 0;
        }
        else if (indexPath.section == 2 && indexPath.row == 1 && isShowAlert_1_Picker){ //Alert 1 picker on
            height = heightOfPicker + 40; //Add a Segment Control also
        }
        else if (indexPath.section == 2 && indexPath.row == 3 && !isShowAlert_2_Picker){ //Alert 2 picker off
            height = 0;
        }
        else if (indexPath.section == 2 && indexPath.row == 3 && isShowAlert_2_Picker){ //Alert 2 picker on
            height = heightOfPicker + 40; //Add a Segment Control also
        }
        else // all other rows
            height = 40;
        
        
    }
    else{ //iOS 6
        
        if (indexPath.section == 1) {
            height = 55;
        }
        else{
            height = 40;
        }
    }
    
    
    return  height;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Steve - Changes time zone back to system time zone so that UIDatePicker shows properly
    [NSTimeZone resetSystemTimeZone];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    //indexPath.section == 1 && indexPath.row == 5
    //NSLog(@"indexPath.section = %d, indexPath.row = %d  ", indexPath.section, indexPath.row);
    
    
    static NSString *CellIdentifier;
    
    if (indexPath.section == 6) {
        CellIdentifier = @"deleteButtonCell";
    }
    else{
        CellIdentifier = @"Cell";
    }
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
    for (UIView  *subViews in [cell.contentView subviews]) {
        [subViews removeFromSuperview];
    }
    
    
    //makes sure all cell colors are back to white and accessory is set to none
    //otherwise when table gets reloaded it can change color or accessory since some cells have it and some don't
    if (IS_IOS_7) {
        cell.contentView.backgroundColor = [UIColor whiteColor];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }
    
    int y = 7; //y of each standard label
    
    
    if (indexPath.section == 0 && indexPath.row == 0) { //Title Section
        
        if (IS_IOS_7) {
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            UIButton *invisibleButton;
            
            if(IS_IPAD)
            {
                titleTxtFld.frame = CGRectMake(10, y, 600, 44);
                invisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(680, 0, 55, 40)];
                inputSelecterButton.frame = CGRectMake(700, 5, 30, 30);
            }
            else
            {
                titleTxtFld.frame = CGRectMake(10, y, 240, 24);
                invisibleButton = [[UIButton alloc]initWithFrame:CGRectMake(260, 0, 55, 40)];
                inputSelecterButton.frame = CGRectMake(280, 5, 30, 30);
            }
            
            [cell.contentView addSubview:titleTxtFld];
            
            invisibleButton.backgroundColor = [UIColor clearColor];
            [invisibleButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:invisibleButton];
            
            
            inputSelecterButton.backgroundColor = [UIColor clearColor];
            [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:inputSelecterButton];
            
        }
        else{  //iOS 6
            
            titleTxtFld.frame = CGRectMake(10, y, 240, 24);
            [cell.contentView addSubview:titleTxtFld];
            
            
            inputSelecterButton.frame = CGRectMake(260, 5, 30, 30);
            [inputSelecterButton addTarget:self action:@selector(inputTypeSelectorButton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:inputSelecterButton];
            
        }
        
    }
    else if (indexPath.section == 0 && indexPath.row == 1){ //Location
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if(IS_IPAD)
            locTxtFld.frame = CGRectMake(10, y, 600, 24);
        else
            locTxtFld.frame = CGRectMake(10, y, 240, 24);
        locTxtFld.placeholder = @"Location";
        [cell.contentView addSubview:locTxtFld];
    }
    else if (indexPath.section == 1 && indexPath.row == 0){ //iOS 7 --> All Day, iOS 6 --> Starts/Ends Section
        
        if (IS_IOS_7) { // All Day
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            int y_Top = 9;
            
            UILabel *allDayFixedLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, y_Top, 70, 24)];
            allDayFixedLabel.text = @"All-day";
            [allDayFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            allDayFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:allDayFixedLabel];
            
            //All Day Switch
            if(IS_IPAD)
                self.allDaySwitch.frame = CGRectMake(690, 3, 51, 31);
            else
                self.allDaySwitch.frame = CGRectMake(250, 3, 51, 31);
            self.allDaySwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
            [self.allDaySwitch addTarget:self action:@selector(allDaySwitchChangedValue) forControlEvents:UIControlEventValueChanged];
            
            [cell.contentView addSubview:self.allDaySwitch];
        }
        else{ //iOS 6 --> Starts/Ends Section
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            int y_Top = 2; // Start Label y (top label)
            int y_Bottom = 23; //Due Label y (Bottom Label)
            
            
            startLabel.frame = CGRectMake(10, y_Top, 70, 24);
            startLabel.text = @"Start";
            [startLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            startLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:startLabel];
            
            endLabel.frame = CGRectMake(10, y_Bottom, 70, 24);
            endLabel.text = @"Ends";
            [endLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            endLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:endLabel];
            
            
            if ([startsLabel.text isEqualToString:@""] || startsLabel.text == nil) {
                startsLabel.text = @"None";
            }
            
            startsLabel.frame = CGRectMake(65, y_Top, 202, 24);
            startsLabel.backgroundColor = [UIColor clearColor];
            startsLabel.numberOfLines = 1;
            //startsLabel.minimumFontSize = 12;
            startsLabel.minimumScaleFactor = 12;
            startsLabel.adjustsFontSizeToFitWidth = YES;
            startsLabel.textAlignment = NSTextAlignmentRight;
            startsLabel.textColor = [UIColor blackColor];
            [cell.contentView addSubview:startsLabel];
            
            if ([endsLabel.text isEqualToString:@""] || endsLabel.text == nil) {
                endsLabel.text = @"None";
            }
            
            endsLabel.frame = CGRectMake(65, y_Bottom, 202, 24);
            endsLabel.backgroundColor = [UIColor clearColor];
            endsLabel.numberOfLines = 1;
            //endsLabel.minimumFontSize = 12;
            endLabel.minimumScaleFactor = 12;
            endsLabel.adjustsFontSizeToFitWidth = YES;
            endsLabel.textAlignment = NSTextAlignmentRight;
            endsLabel.textColor = [UIColor blackColor];
            [cell.contentView addSubview:endsLabel];
            
            
        }
        
    }
    else if (indexPath.section == 1 && indexPath.row == 1){ //iOS 7 --> Start,  iOS 6 --> No Used
        
        
        if (IS_IOS_7) { //Start
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            
            int y_Top = 9; // Start Label y (top label)
            //int y_Bottom = 23; //Due Label y (Bottom Label)
            
            startLabel.frame = CGRectMake(10, y_Top, 70, 24);
            startLabel.text = @"Start";
            [startLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            startLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:startLabel];
            
            if ([startsLabel.text isEqualToString:@""] || startsLabel.text == nil) {
                startsLabel.text = @"None";
            }
            if(IS_IPAD)
                startsLabel.frame = CGRectMake(550, y_Top, 200, 24);
            else
                startsLabel.frame = CGRectMake(103, y_Top, 200, 24);
            startsLabel.backgroundColor = [UIColor clearColor];
            startsLabel.numberOfLines = 1;
            startsLabel.minimumScaleFactor = 12;
            startsLabel.adjustsFontSizeToFitWidth = YES;
            startsLabel.textAlignment = NSTextAlignmentRight;
            [cell.contentView addSubview:startsLabel];
            
            
            if (isShowStartDatePicker)
                startsLabel.textColor = [UIColor redColor];
            else
                startsLabel.textColor = [UIColor blackColor];
            
        }
        
    }
    else if (indexPath.section == 1 && indexPath.row == 2 && isShowStartDatePicker){ //****StartDate Picker (iOS7)
        
        //cell.contentView.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.2];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self.startDatePicker addTarget:self
                                 action:@selector(startPickerChanged)
                       forControlEvents:UIControlEventValueChanged];
        
        //NSLog(@"self.startDateInternationalCompatible)  %@", self.startDateInternationalCompatible);
        
        self.startDatePicker.date = self.startDateInternationalCompatible; //keeps international Date compatible
        self.startDatePicker.timeZone = [NSTimeZone localTimeZone];
        self.startDatePicker.locale   = [NSLocale currentLocale];
        self.startDatePicker.calendar = [NSCalendar currentCalendar];
        self.startDatePicker.minuteInterval = 5;
        //self.startDatePicker.backgroundColor =[UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.25];
        //self.startDatePicker.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
        
        if (isAllDay)
            self.startDatePicker.datePickerMode = UIDatePickerModeDate;
        else
            self.startDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
        
        [cell.contentView addSubview:self.startDatePicker];
        
    }
    else if (indexPath.section == 1 && indexPath.row == 3){ //EndDate iOS 7 only
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        int y_Top = 9; // Start Label y (top label)
        
        endLabel.frame = CGRectMake(10, y_Top, 70, 24);
        endLabel.text = @"Ends";
        [endLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        endLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:endLabel];
        
        if ([endsLabel.text isEqualToString:@""] || endsLabel.text == nil) {
            endsLabel.text = @"None";
        }
        if(IS_IPAD)
            endsLabel.frame = CGRectMake(550, y_Top, 200, 24);
        else
            endsLabel.frame = CGRectMake(103, y_Top, 200, 24);
        endsLabel.backgroundColor = [UIColor clearColor];
        endsLabel.numberOfLines = 1;
        endLabel.minimumScaleFactor = 12;
        endsLabel.adjustsFontSizeToFitWidth = YES;
        endsLabel.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:endsLabel];
        
        
        if (isShowEndDatePicker)
            endsLabel.textColor = [UIColor redColor];
        else
            endsLabel.textColor = [UIColor blackColor];
        
    }
    else if (indexPath.section == 1 && indexPath.row == 4 && isShowEndDatePicker){ //iOS7 EndDate Picker
        
        //cell.contentView.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.2];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self.endDatePicker addTarget:self
                               action:@selector(endPickerChanged)
                     forControlEvents:UIControlEventValueChanged];
        
        self.endDatePicker.date = self.endDateInternationalCompatible;
        self.endDatePicker.timeZone = [NSTimeZone localTimeZone];
        self.endDatePicker.locale   = [NSLocale currentLocale];
        self.endDatePicker.calendar = [NSCalendar currentCalendar];
        self.endDatePicker.minuteInterval = 5;
        
        
        if (isAllDay)
            self.endDatePicker.datePickerMode = UIDatePickerModeDate;
        else
            self.endDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
        
        
        [cell.contentView addSubview:self.endDatePicker];
        
        
    }
    else if (indexPath.section == 1 && indexPath.row == 5){//iOS 7 - TimeZone
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        int y_Top = 9;
        
        _timeZoneFixedLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, y_Top, 190, 24)];
        _timeZoneFixedLabel.text = @"Date & Time Fixed";
        [_timeZoneFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        _timeZoneFixedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:_timeZoneFixedLabel];
        
        //Description of time zone
        _timeZoneTextField = [[UITextView alloc] initWithFrame:CGRectMake(5, y_Top + 26, 310, 65)];
        _timeZoneTextField.text = @"When On, the date & time will never change no matter what time zone your in. When Off, the time zone will be represented by the time zone your device is set to. This only works with iCloud Calendars.";
        [_timeZoneTextField setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        _timeZoneTextField.backgroundColor = [UIColor clearColor];
        _timeZoneTextField.textColor = [UIColor grayColor];
        _timeZoneTextField.userInteractionEnabled = NO;
        [cell.contentView addSubview:_timeZoneTextField];
        
        
        //Time zone switch
        _timeZoneSwitch.frame = CGRectMake(250, 3, 51, 31);
        _timeZoneSwitch.onTintColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1.0];
        [_timeZoneSwitch addTarget:self action:@selector(timeZoneSwitchedChanged) forControlEvents:UIControlEventValueChanged];
        
        [cell.contentView addSubview:_timeZoneSwitch];
        
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0){ //iOS 7 --> Alert 1 Label, iOS 6 --> Repeat Section
        
        
        if (IS_IOS_7) { //iOS 7 --> Alert 1 Label
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            
            alert_1_FixedLabel.frame = CGRectMake(10, y, 100, 24);
            alert_1_FixedLabel.text = @"Alert 1";
            [alert_1_FixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            alert_1_FixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:alert_1_FixedLabel];
            
            if ([alertsLabel.text isEqualToString:@""] || alertsLabel.text == nil) {
                alertsLabel.text = @"None";
            }
            if(IS_IPAD)
                alertsLabel.frame = CGRectMake(550, y, 200, 24);
            else
                alertsLabel.frame = CGRectMake(103, y, 200, 24);
            alertsLabel.numberOfLines = 1;
            //alertsLabel.minimumFontSize = 12;
            alertsLabel.minimumScaleFactor = 12;
            alertsLabel.adjustsFontSizeToFitWidth = YES;
            alertsLabel.backgroundColor = [UIColor clearColor];
            alertsLabel.textAlignment = NSTextAlignmentRight;
            alertsLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:alertsLabel];
            
            
            if (isShowAlert_1_Picker)
                alertsLabel.textColor = [UIColor redColor];
            else
                alertsLabel.textColor = [UIColor grayColor];
            
        }
        else{ //iOS 6 -- > Repeat
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            repeatFixedLabel.frame = CGRectMake(10, y, 100, 24);
            repeatFixedLabel.text = @"Repeat";
            [repeatFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            repeatFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:repeatFixedLabel];
            
            if ([repeatLabel.text isEqualToString:@""] || repeatLabel.text == nil) {
                repeatLabel.text = @"Never";
            }
            
            repeatLabel.frame = CGRectMake(115, y, 152, 24);
            repeatLabel.numberOfLines = 1;
            repeatLabel.minimumScaleFactor = 12;
            repeatLabel.adjustsFontSizeToFitWidth = YES;
            repeatLabel.backgroundColor = [UIColor clearColor];
            repeatLabel.textAlignment = NSTextAlignmentRight;
            repeatLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:repeatLabel];
        }
        
        
    }
    else if (indexPath.section == 2 && indexPath.row == 1){ //iOS 7 --> Alert 1 Picker
        
        if (IS_IOS_7) { //Alert 1 Picker
            
            //cell.contentView.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.2];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            NSInteger selectedSegmentIndexSelected = self.Alert_1segmentedControl.selectedSegmentIndex;
            
            //Segmented Control "Qucik" "Custom"
            self.Alert_1segmentedControl.tintColor = [UIColor grayColor];
            if(IS_IPAD)
                self.Alert_1segmentedControl.frame = CGRectMake(560, 15, 120, 29);
            else
                self.Alert_1segmentedControl.frame = CGRectMake(320/2 - 120/2, 10, 120, 29);
            self.Alert_1segmentedControl.momentary = NO;
            [self.Alert_1segmentedControl addTarget:self action:@selector(alert_1SegmentedControlValueChanged) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:self.Alert_1segmentedControl];
            
            if (isShowAlert_1_Picker) {
                
                if (selectedSegmentIndexSelected == 0) { //Alert 1 Segment Control "Quick"
                    
                    self.alert_1_Picker.hidden = YES;
                    self.Alert_1segmentedControl.hidden = NO;
                    self.alertCustomPicker_1.hidden = NO;
                    
                    
                    //Steve
                    if(IS_IPAD){
                        
                        CGRect screenRect = [[UIScreen mainScreen] bounds];
                        CGFloat screenWidth = screenRect.size.width; //Finds screen width
                        
                        self.Alert_1segmentedControl.frame =
                        CGRectMake(screenWidth/2 - self.Alert_1segmentedControl.frame.size.width/2, 10, self.Alert_1segmentedControl.frame.size.width, self.Alert_1segmentedControl.frame.size.height);
                        self.alertCustomPicker_1.frame =
                        CGRectMake(screenWidth/2 - self.alertCustomPicker_1.frame.size.width/2, 2+ self.Alert_1segmentedControl.frame.size.height, self.alertCustomPicker_1.frame.size.width, self.alertCustomPicker_1.frame.size.height);
                        
                        
                    }
                    else //iPhone
                        self.alertCustomPicker_1.frame = CGRectMake(0, 40, 320, 216);
                    
                    
                    NSString *valueOfPickerItem = @"";
                    
                    for (int i = 0; i < [self.alertPickerArray count]; i++)  {
                        
                        //alertPickerArray values declared in viewDidLoad
                        valueOfPickerItem = [self.alertPickerArray objectAtIndex:i];
                        
                        if ([valueOfPickerItem isEqualToString:alertsLabel.text]) {
                            [self.alertCustomPicker_1 selectRow:i inComponent:0 animated:YES];
                        }
                    }
                    
                    [cell.contentView addSubview:self.alertCustomPicker_1];
                    
                }
                else{ //Alert 1 Segment Control  "Custom" (Dates)
                    
                    
                    self.alertCustomPicker_1.hidden = YES;
                    self.Alert_1segmentedControl.hidden = NO;
                    self.alert_1_Picker.hidden = NO;
                    
                    //Steve
                    if(IS_IPAD){
                        
                        CGRect screenRect = [[UIScreen mainScreen] bounds];
                        CGFloat screenWidth = screenRect.size.width; //Finds screen width
                        
                        self.Alert_1segmentedControl.frame =
                        CGRectMake(screenWidth/2 - self.Alert_1segmentedControl.frame.size.width/2, 10, self.Alert_1segmentedControl.frame.size.width, self.Alert_1segmentedControl.frame.size.height);
                        self.alert_1_Picker.frame =
                        CGRectMake(screenWidth/2 - self.alert_1_Picker.frame.size.width/2, 2+ self.Alert_1segmentedControl.frame.size.height, self.alert_1_Picker.frame.size.width, self.alert_1_Picker.frame.size.height);
                        
                    }
                    else //iPhone
                        self.alert_1_Picker.frame = CGRectMake(0, 40, 320, 216);
                    
                    
                    
                    [self.alert_1_Picker addTarget:self
                                            action:@selector(alert_1_PickerValueChanged)
                                  forControlEvents:UIControlEventValueChanged];
                    
                    self.alert_1_Picker.timeZone = [NSTimeZone localTimeZone];
                    self.alert_1_Picker.locale   = [NSLocale currentLocale];
                    self.alert_1_Picker.calendar = [NSCalendar currentCalendar];
                    self.alert_1_Picker.datePickerMode = UIDatePickerModeDateAndTime;
                    self.alert_1_Picker.minuteInterval = 5;
                    
                    //Sets picker value
                    //self.alert_1_Picker.date = self.startDateInternationalCompatible;
                    
                    [cell.contentView addSubview:self.alert_1_Picker];
                }
            }
            else{
                
                self.Alert_1segmentedControl.hidden = YES;
                self.Alert_2segmentedControl.hidden = YES;
            }
            
            
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 2){ //iOS 7 --> Alert 2 Label
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        secondalert_lbl.frame = CGRectMake(10, y, 100, 24);
        secondalert_lbl.text = @"Alert 2";
        [secondalert_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        secondalert_lbl.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:secondalert_lbl];
        
        if ([secondalertsLabel.text isEqualToString:@""] || secondalertsLabel.text == nil) {
            secondalertsLabel.text = @"None";
        }
        
        
        if(IS_IPAD)
            secondalertsLabel.frame = CGRectMake(550, y, 200, 24);
        else
            secondalertsLabel.frame = CGRectMake(103, y, 200, 24);
        
        
        secondalertsLabel.numberOfLines = 1;
        secondalertsLabel.minimumScaleFactor = 12;
        secondalertsLabel.adjustsFontSizeToFitWidth = YES;
        secondalertsLabel.backgroundColor = [UIColor clearColor];
        secondalertsLabel.textAlignment = NSTextAlignmentRight;
        secondalertsLabel.textColor = [UIColor grayColor];
        [cell.contentView addSubview:secondalertsLabel];
        
        
        if (isShowAlert_2_Picker)
            secondalertsLabel.textColor = [UIColor redColor];
        else
            secondalertsLabel.textColor = [UIColor grayColor];
        
        
    }
    else if (indexPath.section == 2 && indexPath.row == 3){ //iOS 7 --> Alert 2 Picker
        
        //cell.contentView.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.2];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSInteger selectedSegmentIndexSelected = self.Alert_2segmentedControl.selectedSegmentIndex;
        
        //Segmented Control "Qucik" "Custom"
        self.Alert_2segmentedControl.tintColor = [UIColor darkGrayColor];
        
        
        if(IS_IPAD){
            
            //self.Alert_2segmentedControl.frame = CGRectMake(560, 15, 120, 29);
            
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width; //Finds screen width
            
            self.Alert_2segmentedControl.frame =
            CGRectMake(screenWidth/2 - self.Alert_2segmentedControl.frame.size.width/2, 10, self.Alert_2segmentedControl.frame.size.width, self.Alert_2segmentedControl.frame.size.height);
            
        }
        else //iPhone
            self.Alert_2segmentedControl.frame = CGRectMake(320/2 - 120/2, 10, 120, 29);
        
        
        self.Alert_2segmentedControl.momentary = NO;
        [self.Alert_2segmentedControl addTarget:self action:@selector(alert_2SegmentedControlValueChanged) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:self.Alert_2segmentedControl];
        
        
        if (isShowAlert_2_Picker) {
            if (selectedSegmentIndexSelected == 0) { //Alert 2 "Quick"
                
                self.alertCustomPicker_2.hidden = NO;
                self.Alert_2segmentedControl.hidden = NO;
                self.alert_2_Picker.hidden = YES;
                
                
                if(IS_IPAD){
                    
                    //self.alertCustomPicker_2.frame = CGRectMake(440, 40, 320, 216);
                    
                    CGRect screenRect = [[UIScreen mainScreen] bounds];
                    CGFloat screenWidth = screenRect.size.width; //Finds screen width
                    
                    self.alertCustomPicker_2.frame =
                    CGRectMake(screenWidth/2 - self.alertCustomPicker_2.frame.size.width/2, 2 + self.Alert_2segmentedControl.frame.size.height, self.alertCustomPicker_2.frame.size.width, self.alertCustomPicker_2.frame.size.height);
                    
                }
                else //iPhone
                    self.alertCustomPicker_2.frame = CGRectMake(0, 40, 320, 216);
                
                
                //self.alertCustomPicker_2.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.5];
                
                NSString *valueOfPickerItem = @"";
                
                for (int i = 0; i < [self.alertPickerArray count]; i++)  {
                    
                    //alertPickerArray values declared in viewDidLoad
                    valueOfPickerItem = [self.alertPickerArray objectAtIndex:i];
                    
                    if ([valueOfPickerItem isEqualToString:alertsLabel.text]) {
                        [self.alertCustomPicker_2 selectRow:i inComponent:0 animated:YES];
                    }
                }
                
                [cell.contentView addSubview:self.alertCustomPicker_2];
                
            }
            else{ //Alert 2 "Custom"
                
                
                self.alert_2_Picker.hidden = NO;
                self.alertCustomPicker_2.hidden = YES;
                self.Alert_2segmentedControl.hidden = NO;
                
                
                if(IS_IPAD){
                    
                    //self.alert_2_Picker.frame = CGRectMake(440, 40, 320, 216);
                    CGRect screenRect = [[UIScreen mainScreen] bounds];
                    CGFloat screenWidth = screenRect.size.width; //Finds screen width
                    
                    self.alert_2_Picker.frame =
                    CGRectMake(screenWidth/2 - self.alert_2_Picker.frame.size.width/2, 2 + self.Alert_2segmentedControl.frame.size.height, self.alert_2_Picker.frame.size.width, self.alert_2_Picker.frame.size.height);
                }
                else
                    self.alert_2_Picker.frame = CGRectMake(0, 40, 320, 216);
                
                
                
                //self.alert_2_Picker.backgroundColor = [UIColor colorWithRed:191.0/255.0f green:239.0/255.0f blue:255.0/255.0f alpha:0.5];
                
                [self.alert_2_Picker addTarget:self
                                        action:@selector(alert_2_PickerValueChanged)
                              forControlEvents:UIControlEventValueChanged];
                
                self.alert_2_Picker.timeZone = [NSTimeZone localTimeZone];
                self.alert_2_Picker.locale   = [NSLocale currentLocale];
                self.alert_2_Picker.calendar = [NSCalendar currentCalendar];
                self.alert_2_Picker.datePickerMode = UIDatePickerModeDateAndTime;
                self.alert_2_Picker.minuteInterval = 5;
                
                
                [cell.contentView addSubview:self.alert_2_Picker];
            }
        }
        
        
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //iOS 7 --> Repeat, iOS 6 --> Alert 1 Label
        
        
        
        if (IS_IOS_7) { //iOS 7 --> Repeat
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            
            repeatFixedLabel.frame = CGRectMake(10, y, 100, 24);
            repeatFixedLabel.text = @"Repeat";
            [repeatFixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            repeatFixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:repeatFixedLabel];
            
            if ([repeatLabel.text isEqualToString:@""] || repeatLabel.text == nil) {
                repeatLabel.text = @"Never";
            }
            if(IS_IPAD)
                repeatLabel.frame = CGRectMake(540, y, 200, 24);
            else
                repeatLabel.frame = CGRectMake(135, y, 152, 24);
            repeatLabel.numberOfLines = 1;
            //repeatLabel.minimumFontSize = 12;
            repeatLabel.minimumScaleFactor = 12;
            repeatLabel.adjustsFontSizeToFitWidth = YES;
            repeatLabel.backgroundColor = [UIColor clearColor];
            repeatLabel.textAlignment = NSTextAlignmentRight;
            repeatLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:repeatLabel];
            
            
        }
        else{ //iOS 6 --> Alert 1 Label
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            
            alert_1_FixedLabel.frame = CGRectMake(10, y, 100, 24);
            alert_1_FixedLabel.text = @"Alert 1";
            [alert_1_FixedLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            alert_1_FixedLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:alert_1_FixedLabel];
            
            if ([alertsLabel.text isEqualToString:@""] || alertsLabel.text == nil) {
                alertsLabel.text = @"None";
            }
            
            alertsLabel.frame = CGRectMake(65, y, 202, 24);
            alertsLabel.numberOfLines = 1;
            //alertsLabel.minimumFontSize = 12;
            alertsLabel.minimumScaleFactor = 12;
            alertsLabel.adjustsFontSizeToFitWidth = YES;
            alertsLabel.backgroundColor = [UIColor clearColor];
            alertsLabel.textAlignment = NSTextAlignmentRight;
            alertsLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:alertsLabel];
        }
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ //iOS 6 --> Alert 2 Label  /// iOS 7 --> NOT USED
        
        
        if (!IS_IOS_7) { //iOS 6 --> Alert Label 2
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            secondalert_lbl.frame = CGRectMake(10, y, 100, 24); //CGRectMake(10, y, 110, 24)
            secondalert_lbl.text = @"Alert 2";
            [secondalert_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            secondalert_lbl.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:secondalert_lbl];
            
            if ([secondalertsLabel.text isEqualToString:@""] || secondalertsLabel.text == nil) {
                secondalertsLabel.text = @"None";
            }
            if(IS_IPAD)
                secondalertsLabel.frame = CGRectMake(550, y, 202, 24);
            else
                secondalertsLabel.frame = CGRectMake(65, y, 202, 24);
            secondalertsLabel.numberOfLines = 1;
            //secondalertsLabel.minimumFontSize = 12;
            secondalertsLabel.minimumScaleFactor = 12;
            secondalertsLabel.adjustsFontSizeToFitWidth = YES;
            secondalertsLabel.backgroundColor = [UIColor clearColor];
            secondalertsLabel.textAlignment = NSTextAlignmentRight;
            secondalertsLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:secondalertsLabel];
            
        }
        
        
        
    }
    else if (indexPath.section == 4 && indexPath.row == 0){ // Notes
        
        
        if (IS_IOS_7) {
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            
            notes_lbl.frame = CGRectMake(10, y, 110, 24);
            notes_lbl.text = @"Notes";
            [notes_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            notes_lbl.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:notes_lbl];
            
            if ([notesLabel.text isEqualToString:@""] || notesLabel.text == nil) {
                notesLabel.text = @"None";
            }
            if(IS_IPAD)
                notesLabel.frame = CGRectMake(540, y, 200, 24);
            else
                notesLabel.frame = CGRectMake(135, y, 152, 24);
            notesLabel.numberOfLines = 1;
            //notesLabel.minimumFontSize = 12;
            notesLabel.minimumScaleFactor = 12;
            notesLabel.adjustsFontSizeToFitWidth = YES;
            notesLabel.backgroundColor = [UIColor clearColor];
            notesLabel.textAlignment = NSTextAlignmentRight;
            notesLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:notesLabel];
        }
        else{
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            notes_lbl.frame = CGRectMake(10, y, 110, 24);
            notes_lbl.text = @"Notes";
            [notes_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            notes_lbl.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:notes_lbl];
            
            if ([notesLabel.text isEqualToString:@""] || notesLabel.text == nil) {
                notesLabel.text = @"None";
            }
            
            notesLabel.frame = CGRectMake(115, y, 152, 24);
            notesLabel.numberOfLines = 1;
            //notesLabel.minimumFontSize = 12;
            notesLabel.minimumScaleFactor = 12;
            notesLabel.adjustsFontSizeToFitWidth = YES;
            notesLabel.backgroundColor = [UIColor clearColor];
            notesLabel.textAlignment = NSTextAlignmentRight;
            notesLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:notesLabel];
        }
        
        
    }
    if (indexPath.section == 5 && indexPath.row == 0) { // Calendar
        
        if (IS_IOS_7) {
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            
            
            calendar_lbl.frame = CGRectMake(10, y, 100, 24);
            calendar_lbl.text = @"Calendar";
            [calendar_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            calendar_lbl.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:calendar_lbl];
            
            if ([calendarLabel.text isEqualToString:@""] || calendarLabel.text == nil) {
                calendarLabel.text = @"None";
            }
            if(IS_IPAD)
                calendarLabel.frame = CGRectMake(540, y, 200, 24);
            else
                calendarLabel.frame = CGRectMake(135, y, 152, 24);
            calendarLabel.backgroundColor = [UIColor clearColor];
            calendarLabel.textAlignment = NSTextAlignmentRight;
            calendarLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:calendarLabel];
            
        }
        else{ //iOS 6
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            calendar_lbl.frame = CGRectMake(10, y, 100, 24);
            calendar_lbl.text = @"Calendar";
            [calendar_lbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
            calendar_lbl.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:calendar_lbl];
            
            if ([calendarLabel.text isEqualToString:@""] || calendarLabel.text == nil) {
                calendarLabel.text = @"None";
            }
            
            calendarLabel.frame = CGRectMake(115, y, 152, 24);
            calendarLabel.backgroundColor = [UIColor clearColor];
            calendarLabel.textAlignment = NSTextAlignmentRight;
            calendarLabel.textColor = [UIColor grayColor];
            [cell.contentView addSubview:calendarLabel];
            
        }
        
        
    }
    if (indexPath.section == 6 && indexPath.row == 0) { //Delete Button
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        if (IS_IOS_7) {
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            int cellWidth = cell.frame.size.width;
            int cellHeight = cell.frame.size.height;
            int buttonWidth = 150;
            int buttonHeight = 35;
            
            UIButton *deleteButtonCustom = [UIButton buttonWithType:UIButtonTypeSystem];
            
            [deleteButtonCustom setTitle:@"Delete Event" forState:UIControlStateNormal];
            deleteButtonCustom.tintColor = [UIColor redColor];
            deleteButtonCustom.titleLabel.textAlignment = NSTextAlignmentCenter;
            deleteButtonCustom.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18]; //HelveticaNeue-Bold
            deleteButtonCustom.backgroundColor = [UIColor clearColor];
            if(IS_IPAD)
            {
                deleteButtonCustom.frame =
                CGRectMake(309, cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
            }
            else
            {
                deleteButtonCustom.frame =
                CGRectMake(cellWidth/2 - buttonWidth/2, cellHeight/2 - buttonHeight/2, buttonWidth, buttonHeight);
            }
            [deleteButtonCustom addTarget:self
                                   action:@selector(delete_buttonClicked:)
                         forControlEvents:UIControlEventTouchDown];
            
            [cell.contentView addSubview:deleteButtonCustom];
            
            
        }
        else{
            
            UIImage *deleteButtonImage = [UIImage imageNamed:@"DeleteBtn.png"];
            
            int cellWidth = cell.frame.size.width - 20; //approximate size of cell width
            int deleteButtonWidth = deleteButtonImage.size.width;
            
            deleteButton.frame = CGRectMake(cellWidth/2 - deleteButtonWidth/2, 2, deleteButtonWidth, 41);
            [deleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(delete_buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:deleteButton];
            
        }
        
        
    }
    
    
    
    if (IS_IOS_7) {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == 1 && indexPath.row == 0) { //iOS 6 --> Start/End
        
        if (!IS_IOS_7) { //iOS 6 --> Start/End
            [self startsEnds_buttonClicked:self];
        }
        
    }
    
    if (indexPath.section == 1 && indexPath.row == 1) { //Starts Label for iOS 7. Start-End for iOS 6
        
        [titleTxtFld resignFirstResponder];
        
        
        if (IS_IOS_7) {
            
            if (isShowStartDatePicker) {       //Closes cell to hide StartDatePicker
                
                isShowStartDatePicker = NO;
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:1];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:1];
                NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:1];
                NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:1];
                
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4,
                                       indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
            }
            else{                               //Opens cell to show StartDatePicker
                isShowStartDatePicker = YES;
                
                //closes all other pickers
                isShowEndDatePicker = NO;
                isShowAlert_1_Picker = NO;
                isShowAlert_2_Picker = NO;
                
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:1];
                NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:1];
                NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:1];
                NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:1];
                
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4,
                                       indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                [tableView reloadData];
            }
            
        }
        
        
        
    }
    else if (indexPath.section == 1 && indexPath.row == 3){ //End Date for iOS 7
        
        [titleTxtFld resignFirstResponder];
        
        
        if (isShowEndDatePicker) {
            isShowEndDatePicker = NO;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:1];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:1];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:1];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:1];
            
            NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
            NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4,
                                   indexPath5, indexPath6, indexPath7, indexPath8, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            //[tableView reloadData];
        }
        else{
            isShowEndDatePicker = YES;
            
            //closes all other pickers
            isShowStartDatePicker = NO;
            isShowAlert_1_Picker = NO;
            isShowAlert_2_Picker = NO;
            
            
            //************ animate cell reloading **********************
            // Build the two index paths
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:1];
            NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:1];
            NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:1];
            NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:1];
            
            NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
            NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
            NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
            NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
            
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, indexPath3, indexPath4,
                                   indexPath5, indexPath6, indexPath7, indexPath8, nil];
            [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
            
            [tableView reloadData];
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0){ //iOS 7 --> Alert 1 iOS 6 --> Repeat
        
        if (IS_IOS_7){ // Opens & Hides Alert Picker
            
            if (isShowAlert_1_Picker) {     //Closes cell to hide Alert 1 Picker
                
                isShowAlert_1_Picker = NO;
                
                
                //************ animate cell reloading **********************
                // Build the two index paths
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                //[tableView reloadData];
            }
            else{                           //Opens cell to show Alert 1 Picker
                
                isShowAlert_1_Picker = YES;
                
                //closes all other pickers
                isShowStartDatePicker = NO;
                isShowEndDatePicker = NO;
                isShowAlert_2_Picker = NO;
                
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [self.myTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                //Moves Section to top
                [self performSelector:@selector(animateAlertSectionToTop) withObject:nil afterDelay:0.25];
                
                [tableView reloadData];
            }
            
        }
        else{ //iOS 6 --> Repeat
            
            [self repeat_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row ==2){ //iOS 7 --> Show/Hide Alert 2 picker
        
        if (IS_IOS_7) {
            
            if (isShowAlert_2_Picker) {     //Closes cell to hide Alert 2 Picker
                
                isShowAlert_2_Picker = NO;
                
                //************ animate cell reloading **********************
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                // [tableView reloadData];
            }
            else{                           //Opens cell to show Alert 2 Picker
                
                isShowAlert_2_Picker = YES;
                
                //closes all other pickers
                isShowStartDatePicker = NO;
                isShowEndDatePicker = NO;
                isShowAlert_1_Picker = NO;
                
                
                //************ animate cell reloading **********************
                NSIndexPath* indexPath5 = [NSIndexPath indexPathForRow:0 inSection:2];
                NSIndexPath* indexPath6 = [NSIndexPath indexPathForRow:1 inSection:2];
                NSIndexPath* indexPath7 = [NSIndexPath indexPathForRow:2 inSection:2];
                NSIndexPath* indexPath8 = [NSIndexPath indexPathForRow:3 inSection:2];
                
                // Add them in an index path array
                NSArray* indexArray = [NSArray arrayWithObjects:indexPath5, indexPath6, indexPath7, indexPath8, nil];
                [self.myTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
                
                
                //Moves Section to top
                [self performSelector:@selector(animateAlertSectionToTop) withObject:nil afterDelay:0.25];
                
                [tableView reloadData];
            }
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //iOS 7 --> Repeat, iOS 6 -->Alert 1
        
        if (IS_IOS_7) {
            
            [self repeat_buttonClicked:self];
        }
        else{ //iOS 6
            
            [self alerts_buttonClicked:self];
        }
        
        
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ // iOS 6 --> Alert 2 clicked
        
        if (!IS_IOS_7) { //iOS 6
            [self alertsecond_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 4 && indexPath.row == 0){ //Notes
        [self notes_buttonClicked:self];
        
    }
    else if (indexPath.section == 5 && indexPath.row == 0){ //Calendar
        [self calendar_buttonClicked:self];
    }
    
    else if (indexPath.section == 6){ //Delete
        
        [self delete_buttonClicked:self];
    }
}


-(void) animateAlertSectionToTop{
    
    //************ animate cell reloading **********************
    // Build the two index paths
    /*
     NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:1];
     NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:2 inSection:1];
     NSIndexPath* indexPath3 = [NSIndexPath indexPathForRow:3 inSection:1];
     NSIndexPath* indexPath4 = [NSIndexPath indexPathForRow:4 inSection:1];
     */
    
    
    
    //Scrolls this section & row to the top for easier viewing
    NSInteger row = 0;
    NSInteger section = 2;
    NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:row inSection:section] ;
    [self.myTableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop  animated:YES];
    
    
}



- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1 && indexPath.row == 0) { //Starts/Ends
        
        [titleTxtFld resignFirstResponder];
        
        [self startsEnds_buttonClicked:self];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0){ //Repeat
        
        [self repeat_buttonClicked:self];
    }
    else if (indexPath.section == 3 && indexPath.row == 0){ //Alert 1
        
        if (!IS_IOS_7) {
            [self alerts_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 1){ //Alert 2
        
        if (!IS_IOS_7) {
            [self alertsecond_buttonClicked:self];
        }
        
    }
    else if (indexPath.section == 4 && indexPath.row == 0){ //Notes
        [self notes_buttonClicked:self];
        
    }
    else if (indexPath.section == 5 && indexPath.row == 0){ //Calendar
        [self calendar_buttonClicked:self];
    }
    
}

//Steve - Protocal method so it shows HW in Title for iPad when HandwritingView dissapears
-(void) showHandwritingForiPad{
    
    
    [self viewWillAppear:YES];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.blurredImageView.layer addAnimation:transition forKey:nil];
    self.blurredImageView.hidden = YES;
    
    self.isHandwritingShowing = NO;
    self.myTableView.userInteractionEnabled = YES;
}


-(UIImage*) captureScreenShotAndblurImage{
    
    
    // Create a graphics context with the target size
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    
    UIGraphicsBeginImageContext(imageSize);
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Iterate over every window from back to front
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context
            CGContextSaveGState(context);
            // Center the context around the window's anchor point
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            // Apply the window's transform about the anchor point
            CGContextConcatCTM(context, [window transform]);
            // Offset by the portion of the bounds left of and above the anchor point
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
            
            // Render the layer hierarchy to the current context
            //[[window layer] renderInContext:context];
            
            // iOS 7 New API
            [window drawViewHierarchyInRect:[[UIScreen mainScreen] bounds] afterScreenUpdates:NO];
            
            // Restore the context
            CGContextRestoreGState(context);
        }
    }
    
    
    // Get the snapshot
    _screenShotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Now apply the blur effect using Apple's UIImageEffect category
    UIImage *blurredScreenshotImage = [_screenShotImage applyDarkEffectWithLessBlur];//applyLightEffect  applyDarkEffectWithLessBlur
    
    // Or apply any other effects available in "UIImage+ImageEffects.h"
    // UIImage *blurredScreenshotImage = [screenShotImage applyDarkEffect];
    //UIImage *blurredScreenshotImage = [screenShotImage applyExtraLightEffect];
    
    UIGraphicsEndImageContext();
    
    
    return blurredScreenshotImage;
    
}

#pragma mark -
#pragma mark Memory Management Methods 
#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:YES];
    
    [titleTxtFld resignFirstResponder];
    [locTxtFld resignFirstResponder];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"NavigationNew"]){
        if (isAdd){
            saveButton.hidden = YES;
            [saveButton removeFromSuperview];
        }
        else{
            updateButton.hidden = YES;
            [updateButton removeFromSuperview];
        }
    }
    
    OrganizerAppDelegate * myAppdelegate = (OrganizerAppDelegate *)[UIApplication sharedApplication].delegate;
    myAppdelegate.tabBarController.tabBar.hidden=NO;
    
    
}


- (void)viewDidUnload {
    btn_CustomView = nil;
    alert_lbl = nil;
    navBar = nil;
    backButton = nil;
    titleLocationButton = nil;
    titleLocationButton = nil;
    titleLineDividerLabel = nil;
    startButton = nil;
    startLabel = nil;
    endLabel = nil;
    startDisclosureButton = nil;
    repeatButton = nil;
    repeatDisclosureButton = nil;
    repeatFixedLabel = nil;
    alert_1_FixedLabel = nil;
    alert1DisclosureButton = nil;
    
    titleTxtFld = nil;
    inputSelecterButton = nil;
    locTxtFld = nil;
    
    startsLabel = nil;
    endsLabel = nil;
    repeatLabel = nil;
    alertsLabel = nil;
    secondalert_lbl = nil;
    secondalertsLabel = nil;
    notes_lbl = nil;
    notesLabel = nil;
    calendar_lbl = nil;
    calendarLabel = nil;
    
    self.startDatePicker = nil;
    self.endDatePicker = nil;
    //self.customAlert_1_Switch = nil;
    //self.customAlert_2_Switch = nil;
    self.alertPickerArray = nil;
    self.alert_1_Picker = nil;
    self.alert_2_Picker = nil;
    self.alertCustomPicker_1 = nil;
    self.alertCustomPicker_2 = nil;
    self.startDateInternationalCompatible = nil;
    self.endDateInternationalCompatible = nil;
    self.Alert_1segmentedControl = nil;
    self.Alert_2segmentedControl = nil;
    
    [super viewDidUnload];
    
}


@end


//
//  HelpViewController.h
//  Organizer
//
//  Created by alok gupta on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface HelpViewController : UIViewController
@interface HelpViewController : UITableViewController //Steve
{
    IBOutlet UINavigationBar *navBar;
    IBOutlet UINavigationBar *navBarList;
    IBOutlet UISwitch *Switch_AllHelp;
    IBOutlet UISwitch *Switch_Home;
    IBOutlet UISwitch *Switch_Calender;
    IBOutlet UISwitch *Switch_Todo;
    IBOutlet UISwitch *Switch_List;
    IBOutlet UISwitch *Switch_Project;
    IBOutlet UISwitch *Switch_Notes;
    IBOutlet UISwitch *Switch_Handwriting;
    IBOutlet UIView   *ListView;
    IBOutlet UIView *todoView; //Steve
    IBOutlet UINavigationBar *navBarToDo; //Steve
        
    __weak IBOutlet UISwitch *Switch_List_AllHelp;
    __weak IBOutlet UISwitch *Switch_List_Calender;
    __weak IBOutlet UISwitch *Switch_List_Listonly;
    
    __weak IBOutlet UISwitch *Switch_TODO_AllHelp; //Steve
    __weak IBOutlet UISwitch *Switch_TODO_Calender; //Steve
    __weak IBOutlet UISwitch *Switch_TODO_TODOonly; //Steve
    
    //Steve added
    IBOutlet UIView *projectsView;
    IBOutlet UINavigationBar *navBarProjects;
    IBOutlet UISwitch *Switch_Projects_AllHelp;
    IBOutlet UISwitch *Switch_Projects_Calendar;
    IBOutlet UISwitch *Switch_Projects_Projects;
    
    //Steve added
    IBOutlet UIView *notesView;
    IBOutlet UINavigationBar *navBarNotes;
    IBOutlet UISwitch *Switch_Notes_AllHelp;
    IBOutlet UISwitch *Switch_Notes_Handwriting;
    IBOutlet UISwitch *Switch_Notes_Notes;
    
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *doneButton;

}



-(IBAction)BackButtonClicked:(id)sender;
-(IBAction)DoneButtonClicked:(id)sender;
- (IBAction)switch_AllHelpChanged:(id)sender;//Steve
- (IBAction)switch_ValueChanged:(id)sender; //Steve
@end

//
//  MasterViewController.m
//  AppointmentList
//
//  Created by Ole Begemann on 01.12.11.
//  Copyright (c) 2011 Ole Begemann. All rights reserved.
//

#import "CalendarList2ViewController.h"
#import "GetAllDataObjectsClass.h"
#import "NSDate-Utilities.h"
#import "ViewAppointmentScreen.h"
#import "iSpeechViewControllerTTS.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>


@interface CalendarList2ViewController ()
{
    UIButton *btn;
    UIImage *detailDiscloserImage;
    UIImageView *detailDiscloserImageView;
}
//@property (strong, nonatomic) NSMutableDictionary *sections;
//@property (strong, nonatomic) NSArray *sortedDays;
@property (strong, nonatomic) NSDateFormatter           *sectionDateFormatter;
@property (strong, nonatomic) NSDateFormatter           *cellDateFormatter;

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;
- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate;
-(void)setLodedValue;
-(void)getAppointmentsArrayOrderByDueDate;
-(void)SelectedDateAppointmentList;
-(NSArray*)findsCalendarsToShow;//Steve - finds Calendars to show
-(UIImage*)invertHWColor:(NSData*)image;//Steve - Used on Pic backgrounds, inverts the HW image color so Black is While & Blue is Yellow, for example

@end



@implementation CalendarList2ViewController

@synthesize sections;
@synthesize sortedDays;
@synthesize sectionDateFormatter;
@synthesize cellDateFormatter;
@synthesize currentVisibleDate = _currentVisibleDate;
@synthesize events = _events;
@synthesize tblAppinmentList,delegate; //ALok Added
@synthesize strTextToSpeak;
//@synthesize appointmentsArray;

- (void)awakeFromNib
{
    // NSLog(@"awakeFromNib CalendarList2ViewController");
    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    // NSLog(@"viewDidLoad CalendarList2ViewController");
    [super viewDidLoad];
    
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    //[NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    HWImagesDic = [[NSMutableDictionary alloc]init];
    eventStore = appDelegate.eventStore;
    if (eventStore==nil) {
        eventStore = [[EKEventStore alloc]init];
    }
    
    
    //"Loading..."
    UIView *loadingView;
    if(IS_IPAD)
        loadingView=[[UIView alloc]initWithFrame:CGRectMake(250, 164, 142, 26)];
    else
        loadingView=[[UIView alloc]initWithFrame:CGRectMake(103, 164, 142, 26)];
    
    [loadingView setBackgroundColor:[UIColor blackColor]];
    loadingView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5];
    [loadingView setOpaque:YES];
    [loadingView setTag:1111];
    [UIView beginAnimations:nil context:nil];
    
    loadingView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0];
    loadingView.layer.borderWidth = 0.0f;
    loadingView.layer.cornerRadius = 8.0f;
    loadingView.layer.shadowColor = [[UIColor grayColor] CGColor];
    loadingView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    loadingView.layer.shadowOpacity = 1.0f;
    loadingView.layer.shadowRadius = 5.0f;
    loadingView.layer.masksToBounds = NO;
    loadingView.clipsToBounds = NO;
    //[tblAppinmentList addSubview:loadingView];
    [self.view addSubview:loadingView];
    
    UILabel *lbl =[[UILabel alloc]initWithFrame:CGRectMake(19, -10, 120, 43)];
    [lbl setText:@"Loading........"];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setFont:[UIFont fontWithName: @"Helvetica" size: 16.0f]];
    [lbl setAlpha:0.0f];
    [loadingView addSubview:lbl];
    
    [UIView setAnimationDelay:0.25];
    [UIView setAnimationDuration:0.0];
    loadingView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5];
    [lbl setAlpha:1.0];
    [UIView commitAnimations];
    
    
    appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
	
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    // NSLog(@"appDelegate.currentVisibleDate 1 = %@", appDelegate.currentVisibleDate);
	
	if(!appDelegate.currentVisibleDate || [appDelegate.currentVisibleDate isEqualToString:@""]) {
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setLocale:[NSLocale currentLocale]];
        [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        
        NSDateComponents *setTimeToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:destinationDate];
        [setTimeToDayBeginning setHour:0];
        [setTimeToDayBeginning setMinute:0];
        [setTimeToDayBeginning setSecond:0];
        
        NSDate *currentVisibleDate_Date = [calendar dateFromComponents:setTimeToDayBeginning];
        
        //Steve - new currentVisibleDate starting at 12AM (beginning of day)
        [appDelegate setCurrentVisibleDate:[formater stringFromDate:currentVisibleDate_Date]];
        
        //NSLog(@"appDelegate.currentVisibleDate 2 = %@", appDelegate.currentVisibleDate);
        
        _currentVisibleDate = [formater dateFromString:appDelegate.currentVisibleDate];
        
	}
    else{
        _currentVisibleDate = [formater dateFromString:appDelegate.currentVisibleDate];
    }
    
    //NSLog(@"currentVisibleDate-->%@",appDelegate.currentVisibleDate);
    //NSLog(@"_currentVisibleDate-->%@",[_currentVisibleDate descriptionWithLocale:@"GMT"]);
    
    //Steve commented - doubles events after  I added EKEventStoreChangedNotification
    //Alok Gupta Added for get notification EKevent store changes
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"EventNotification" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViews) name:@"EventNotification" object:nil];
    
    //[self getAppointmentsArrayOrderByDueDate];
    //Steve changed to fix bug (evenstore sometimes didn't load completely from other views)
    // [self performSelector:@selector(getAppointmentsArrayOrderByDueDate) withObject:nil afterDelay:0.1];
    // [self performSelector:@selector(setLodedValue) withObject:nil afterDelay:0.5];
    
    
    //Steve added - notifies when events have been loaded to main thread
    //Then calls "SelectedDateAppointmentList" method to go to "currentVisibleDate"
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.Elixir.LoadDayEvents" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectedDateAppointmentList) name:@"com.Elixir.LoadDayEvents" object:nil];
    
    //Steve - notifies if events change
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViews) name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    
    
}


-(void)setLodedValue {
	[APPINSTANCE setIsLoded:YES] ;
    [[self.view viewWithTag:1111] setAlpha:0];
}


//Steve added - refreshes views in case Calendar was changed on iCal & user leaves app & comes back
- (void) refreshViews{
    //NSLog(@"refreshViews CalendarList2ViewController");
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(refreshViewsAfterDelay) userInfo:nil repeats:NO];
}

//Steve - sets delay on refreshViews so in iOS 6 data can load first, otherwise in iOS6 it duplicates the data
-(void) refreshViewsAfterDelay{
    // NSLog(@"refreshViewsAfterDelay CalendarList2ViewController");
    
    
	[self getAppointmentsArrayOrderByDueDate];
    [tblAppinmentList reloadData];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    
    //Steve - shuts off auto resize mask, otherwise, Password view will change views
    self.view.autoresizingMask = UIViewAutoresizingNone;
    self.clearsSelectionOnViewWillAppear = NO;
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        if (IS_IOS_7) {
            
        
            if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) {
                
                //Covers line on Nav Controller so if starts on ListView, Month, Week & Day also won't show line
                UIView *coverNavLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 44, 320, 1)];
                coverNavLineView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
                [self.navigationController.navigationBar addSubview:coverNavLineView];
            
            }
        }
    }
    else{ //Picture Calendar
        
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    
    //Steve changed to fix bug (evenstore sometimes didn't load completely from other views)
    [self performSelector:@selector(getAppointmentsArrayOrderByDueDate) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(setLodedValue) withObject:nil afterDelay:0.5];
    

 
    
    /*
     NSLog(@"self.view.frame =  %@", NSStringFromCGRect(self.view.frame));
     NSLog(@"self.view.bounds =  %@", NSStringFromCGRect(self.view.bounds));
     NSLog(@"tblAppinmentList.frame =  %@", NSStringFromCGRect(tblAppinmentList.frame));
     NSLog(@"tblAppinmentList.bounds =  %@", NSStringFromCGRect(tblAppinmentList.bounds));
     
     NSLog(@"  self.navigationController.view.bounds = %@",NSStringFromCGRect(self.navigationController.view.bounds));
     NSLog(@"  self.navigationController.view.frame = %@",NSStringFromCGRect(self.navigationController.view.frame));
     */
    
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    // NSLog(@"viewWillDisappear CalendarList2ViewController");

    
    
    if (self.sections.count != 0) { //Steve added to prevent a crash if indexPath is not available
    
        NSArray *visible = [tblAppinmentList indexPathsForVisibleRows];
        NSIndexPath *indexpath = (NSIndexPath*)[visible objectAtIndex:0];
        
        NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexpath.section];
        
        NSDate *sourceDate = [NSDate date];
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:dateRepresentingThisDay];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setLocale:[NSLocale currentLocale]];
        [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        
        NSDateComponents *setTimeToDayBeginning = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:destinationDate];
        [setTimeToDayBeginning setHour:0];
        [setTimeToDayBeginning setMinute:0];
        [setTimeToDayBeginning setSecond:0];
        
        NSDate *currentVisibleDate_Date = [calendar dateFromComponents:setTimeToDayBeginning];
        [appDelegate setCurrentVisibleDate:[formater stringFromDate:currentVisibleDate_Date]];
        
        // NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:gmtDate];
        //NSDate *localDate = [dateRepresentingThisDay dateByAddingTimeInterval:timeZoneOffset];
        //[appDelegate setCurrentVisibleDate:[format stringFromDate:localDate]];
    }
    
    
	[super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EventNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"com.Elixir.LoadDayEvents" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EKEventStoreChangedNotification object:appDelegate.eventStore];
    
    tblAppinmentList = nil;
    
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(void)getAppointmentsArrayOrderByDueDate{
    //NSLog(@"getAppointmentsArrayOrderByDueDate CalendarList2ViewController");
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    //[NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    
    NSDate *sourceDateForCurrentDate = [NSDate date];
	NSTimeZone *sourceTimeZoneForCurrentDate = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZoneForCurrentDate = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffsetForCurrentDate = [sourceTimeZoneForCurrentDate secondsFromGMTForDate:sourceDateForCurrentDate];
	NSInteger destinationGMTOffsetForCurrentDate = [destinationTimeZoneForCurrentDate secondsFromGMTForDate:sourceDateForCurrentDate];
	NSTimeInterval intervalForCurrentDate = destinationGMTOffsetForCurrentDate - sourceGMTOffsetForCurrentDate;
    //NSDate *nowBeginningOfDayForCurrentDate = [[NSDate alloc] initWithTimeInterval:intervalForCurrentDate sinceDate:[self dateAtBeginningOfDayForDate:sourceDateForCurrentDate]];
    
    NSDate *currentDate = [[NSDate alloc] initWithTimeInterval:intervalForCurrentDate sinceDate:[self dateAtBeginningOfDayForDate:[NSDate date]]];
    
    //NSDate *currentDate = nowBeginningOfDayForCurrentDate;
    //NSLog(@"currentDate = %@", currentDate);
    //NSLog(@"currentDate = %@", [currentDate descriptionWithLocale:@"GMT"]);
    
    NSDate *now = [formatter dateFromString:appDelegate.currentVisibleDate];
    NSDate *sourceDate = now; //[NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //NSDate *nowBeginningOfDay = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[self dateAtBeginningOfDayForDate:now]];
    
	NSDate *selectedDate = [formatter dateFromString:appDelegate.currentVisibleDate];
    
    NSDateFormatter *formatter99 = [[NSDateFormatter alloc] init];
    [formatter99 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    NSString *selectedDateString = [formatter99 stringFromDate:selectedDate];
    
    //////////////////////**************************//////////////////////////////
    //Events from iCal are on systemTimeZone, so to display correct show system time zone
    ////////////////////// Change Time Zone ///////////////////////////////////
    
    [NSTimeZone setDefaultTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateFormatter *formatter33 = [[NSDateFormatter alloc] init];
    [formatter33 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    [formatter33 setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString *selectedDate_NewString = [NSString stringWithString:selectedDateString];
    NSDate *selectedDate_DateNoTime = [formatter33 dateFromString:selectedDate_NewString];
    NSDate *selectedDateFinal = selectedDate_DateNoTime;
    
    //startDate & endDate are set on Local time, but then adjusted for GMT timezone
    NSDate *startDate11 = [selectedDateFinal dateBySubtractingDays:180];
    NSDate *endDate11 = [selectedDateFinal dateByAddingDays:180];
    
    //NSLog(@"startDate11 = %@", startDate11);
    //NSLog(@"startDate11 = %@", [startDate11 descriptionWithLocale:@"GMT"]);
    //NSLog(@"endDate11 = %@", endDate11);
    //NSLog(@"endDate11 = %@", [endDate11 descriptionWithLocale:@"GMT"]);
    
    ///////////////////////////////
    
    
    //Steve Finds the Calendars to Show
    NSArray *calendarsToShowArray = [self findsCalendarsToShow]; //method brings back the calendars to show
    
    if (calendarsToShowArray.count == 0) { //exits if no calendars in array
        return;
    }
    
    NSPredicate *searchPredicate = [eventStore predicateForEventsWithStartDate:startDate11 endDate:endDate11 calendars:calendarsToShowArray];
    //NSArray *events = [eventStore eventsMatchingPredicate:searchPredicate];
    NSMutableArray *events = [[NSMutableArray alloc]init];
    _events = events;
    
    dispatch_queue_t fetchingEvents_queue = dispatch_queue_create("Fetching Events", NULL);
    dispatch_async(fetchingEvents_queue, ^{
        
        NSArray *eventList = [eventStore eventsMatchingPredicate:searchPredicate];
        
        //NSLog(@"eventList.count = %d", eventList.count);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_events addObjectsFromArray:eventList];
            
            // NSLog(@"#1 events.count = %d", _events.count);
            // NSLog(@"Fetched %d events in %f seconds", _events.count, -1.f * [fetchProfilerStart timeIntervalSinceNow]);
            
            self.sections = [NSMutableDictionary dictionary];
            
            for (EKEvent *event in _events)
            {
                
                
                // Reduce event start date to date components (year, month, day)
                NSDate *dateRepresentingThisDay = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[self dateAtBeginningOfDayForDate:event.startDate]];
                
                // If we don't yet have an array to hold the events for this day, create one
                NSMutableArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
                
                if (eventsOnThisDay == nil) {
                    eventsOnThisDay = [NSMutableArray array];
                    
                    // NSLog(@"dateRepresentingThisDay->>>%@",dateRepresentingThisDay);
                    
                    [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
                }
                
                // Add the event to the list for this day
                [eventsOnThisDay addObject:event];
                
                
                /*
                NSLog(@"event.startDate =   %@", event.startDate);
                NSLog(@"event.startDate =   %@", [event.startDate descriptionWithLocale:@"GMT"]);
                NSLog(@"event.endDate =   %@", event.endDate);
                NSLog(@"event.endDate =   %@", [event.endDate descriptionWithLocale:@"GMT"]);
                NSLog(@"dateRepresentingThisDay =   %@", dateRepresentingThisDay);
                NSLog(@"dateRepresentingThisDay =   %@", [dateRepresentingThisDay descriptionWithLocale:@"GMT"]);
                NSLog(@"eventsOnThisDay =   %@", eventsOnThisDay);
                NSLog(@"self.sections.count =   %d", self.sections.count);
                NSLog(@"*************************");
                */
            }
            
            //NSLog(@"for loop ------> Fetched %d events in %f seconds", [self.events count], -1.f * [fetchProfilerStart timeIntervalSinceNow]);
            
            
            BOOL isTheObjectThere = [[self.sections allKeys] containsObject:currentDate];
            if (!isTheObjectThere) {
                
                EKEvent *tempevent = [EKEvent eventWithEventStore:eventStore];
                [tempevent setTitle:@"No Events"];
                
                [self.sections setObject:[NSArray arrayWithObject:tempevent] forKey:currentDate]; //nowBeginningOfDay
            }
            
            // Create a sorted list of days
            NSArray *unsortedDays = [self.sections allKeys];
            
            self.sortedDays = (NSMutableArray *)[unsortedDays sortedArrayUsingSelector:@selector(compare:)];
            
            
            self.sectionDateFormatter = [[NSDateFormatter alloc] init];
            [self.sectionDateFormatter setDateStyle:NSDateFormatterLongStyle];
            [self.sectionDateFormatter setTimeStyle:NSDateFormatterNoStyle];
            
            self.cellDateFormatter = [[NSDateFormatter alloc] init];
            [self.cellDateFormatter setDateStyle:NSDateFormatterNoStyle];
            [self.cellDateFormatter setTimeStyle:NSDateFormatterShortStyle];
            
            [self.tableView reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"com.Elixir.LoadDayEvents" object:self];
            
            appDelegate.AppSections = [self.sections mutableCopy];
            
            /*  NSArray * allTerms = [self.sections objectsForKeys:[self.sections allKeys] notFoundMarker:[NSNull null]];
             NSArray * collapsedTerms = [allTerms valueForKeyPath:@"@unionOfArrays.self"];
             NSPredicate *filter1 = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", @"Alok"];
             NSArray * filteredTerms = [collapsedTerms filteredArrayUsingPredicate:filter1];
             
             NSLog(@"%@",filteredTerms);*/
            
        });
        
        
    });
    
    // NSLog(@"#2 events.count = %d", _events.count);
    
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
}

-(NSArray*)findsCalendarsToShow{
    
    NSMutableArray *calendarArray = [[NSMutableArray alloc]init];
    //self.calendarDictionary = [[NSMutableDictionary alloc]init];
    
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    //Make an array of UserDefaults for Calendars that don't show
    NSArray *calendarsThatDontShowArray = [[sharedDefaults objectForKey:@"CalendarsDoNotShow"] copy]; //make  copy
    NSMutableArray *discardedItems = [NSMutableArray array]; //make an array of objects to be removed
    
    
    for (EKSource *source in eventStore.sources) {
        
        //[self.calendarArray removeAllObjects];
        
        /*
         NSLog(@" *********************************************");
         NSLog(@" source  = %@", source.title);
         NSLog(@" calendarsThatDontShowArray  = %@", calendarsThatDontShowArray);
         NSLog(@" *********************************************");
         */
        
        
        
        NSSet *calendars = [source calendarsForEntityType:EKEntityTypeEvent];
        
        
        for (EKCalendar *calendar in calendars) {
            
            /*
             NSLog(@" *********************************************");
             NSLog(@" Calendar source Title = %@", calendar.source.title);
             NSLog(@" Calendar Title = %@", calendar.title);
             NSLog(@" Calendar Type = %d", calendar.type);
             //NSLog(@" Calendar CGColor = %@", calendar.CGColor);
             //NSLog(@" Calendar allowsContentModifications = %d", calendar.allowsContentModifications);
             NSLog(@" Calendar calendarIdentifier = %@", calendar.calendarIdentifier);
             */
            
            
            
            if (calendarsThatDontShowArray.count == 0) { //if nothing in UserDefaults not to show, then show all
                
                [calendarArray addObject: calendar]; //builds Calendar Array
            }
            
            
            CalendarDataObject *calendarObject = [[CalendarDataObject alloc]init];
            
            [calendarObject setCalendarIdentifire:calendar.calendarIdentifier];
            
            for (NSString *calendarIdentifier in calendarsThatDontShowArray) { //iterate thru copy of array of UserDefaults CalendarsDoNotShow
                
                if ([calendarObject.calendarIdentifire isEqualToString:calendarIdentifier]){ //looks for items that Dont match
                    
                    [discardedItems addObject: calendar]; //builds array of items to be removed from Calendar Array
                    break;
                    
                }
                else{
                    
                    [calendarArray addObject: calendar]; //builds Calendar Array
                }
                
            }
            
        }
        
        
    }
    
    //NSLog(@" calendarArray 1 = %@", calendarArray);
    
    [calendarArray removeObjectsInArray:discardedItems]; //removes all discarded items
    
    // NSLog(@" discardedItems = %@", discardedItems);
    //NSLog(@" calendarArray after discarded items removed = %@", calendarArray);
    //NSLog(@" *********************************************");
    
    
    return calendarArray;
}


//Alok Added For Checking InFocus Handwritten Event
-(BOOL)IsHandwritingEvent:(NSString *) EventName{
    if([EventName length] >= 25)
    {
        if([[EventName substringToIndex:25] isEqualToString:@"InFocus Handwritten Event"])
            return YES;
        else return NO;
        
    }
    return NO;
}

-(void)SelectedDateAppointmentList
{
    //NSLog(@"SelectedDateAppointmentList");
    
    // NSDate *nowBeginningOfDay = [self dateAtBeginningOfDayForDate:now];
    
    //NSLog(@"currentVisibleDate-->%@",appDelegate.currentVisibleDate);
    //NSLog(@"_currentVisibleDate-->%@",[_currentVisibleDate descriptionWithLocale:@"GMT"]);
    
    
    NSDate *sourceDate = _currentVisibleDate;
    
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *nowBeginningOfDay = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[self dateAtBeginningOfDayForDate:sourceDate]];
    
    
    //NSLog(@"self.sortedDays.count  %d", self.sortedDays.count);
    //NSLog(@"self.sections.count  %d", self.sections.count);

    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    if([self.sortedDays count]>0 && [self.sortedDays containsObject:nowBeginningOfDay]){
        
        headerIndex = [self.sortedDays indexOfObject:nowBeginningOfDay];
         //NSLog(@"headerIndex  %d", headerIndex);
    }
    else{
        
        _currentVisibleDate =[ _currentVisibleDate dateBySubtractingDays:1];
        counter = counter + 1;
        
        if (counter < 90) { //limit loop to 90 times
            [self SelectedDateAppointmentList]; //loop again to find a date that has events
        }
        else
            headerIndex =0;
        
        //NSLog(@"sections.count = %d",sections.count);
    }
	
    
	if ([_events count] != 0)
	{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:headerIndex];
        
        //NSLog(@"indexPath = %d",headerIndex);
        
        //NSLog(@"self.tableView  %@", self.tableView);
        //NSLog(@"self.tableView.numberOfSections  %d", self.tableView.numberOfSections);
        //NSLog(@"self.sections.count  %d", self.sections.count);
        
        
        //if (headerIndex <= self.tableView.numberOfSections) { //Steve Test - prevents app from crashing when data doesn't match
        if (self.sections.count > 0) {
            
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
	}
	else {
		return [self.tableView reloadData];
	}
	//[tblAppinmentList reloadData];
}

-(void)gotoToday{
    // NSLog(@"gotoToday");
    
    
    if([appDelegate.AppSections count]>[self.sections count]){
        
        [self.sections setDictionary:appDelegate.AppSections];
        self.sortedDays = (NSMutableArray *)[[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
        [tblAppinmentList reloadData];
    }
    
    
    
    NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *nowBeginningOfDay = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[self dateAtBeginningOfDayForDate:sourceDate]];
    
    // NSLog(@"nowBeginningOfDay =  %@", nowBeginningOfDay);
    
    /* //Steve commented - has a bug
     NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
     [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
     
     //Steve added - changes search paramater to today's date so a wider range of dates will show closer to Today's date
     appDelegate = (OrganizerAppDelegate*)[[UIApplication sharedApplication] delegate];
     [appDelegate setCurrentVisibleDate:[formatter stringFromDate:nowBeginningOfDay]];
     [self refreshViews];
     */
    
    if(![self.sortedDays containsObject:nowBeginningOfDay])
    {
        EKEvent *tempevent = [EKEvent eventWithEventStore:eventStore];
        [tempevent setTitle:@"No Event"];
        [self.sections setObject:[NSArray arrayWithObject:tempevent] forKey:nowBeginningOfDay];
        self.sortedDays = (NSMutableArray *)[[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
        [tblAppinmentList reloadData];
    }
    headerIndex = [self.sortedDays indexOfObject:nowBeginningOfDay];
	
	if ([_events count] != 0)
	{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:headerIndex];
        
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
	}
	else {
		return [self.tableView reloadData];
	}
    
}


#pragma mark - Voice To Text

//ALOK's Added
-(void)eventToSpeak
{
    if([appDelegate.AppSections count]>[self.sections count]){
        [self.sections setDictionary:appDelegate.AppSections];
        self.sortedDays = (NSMutableArray *)[[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
        [tblAppinmentList reloadData];
    }
    
    internet_YES_NO = [self hasConnectivity];
    
    if(internet_YES_NO)
    {
        if([self.sortedDays count]==1)
        {
            NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:0];
            NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
            for (EKEvent *event in eventsOnThisDay)
            {
                if([event.title isEqualToString:@"No Events"])
                    return;
            }
            
        }
        
        [self finalString];
        
        self.strTextToSpeak = (NSMutableString *)[self.strTextToSpeak stringByAppendingFormat:@"%@",@"Please listen to your appointments on Day, week or month view. list view is not able to speak your appointments"];
        
        iSpeechViewControllerTTS *ptr_iSpeechViewControllerTTS = [iSpeechViewControllerTTS sharedApplication];
        [ptr_iSpeechViewControllerTTS readText:self.strTextToSpeak];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:@"Text to Voice requires an internet connection. Please connect to the internet to use Text to Voice." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
    }
}

-(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}
-(NSMutableString *)finalString
{
    NSDate *sourceDate = [NSDate date];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSDateFormatter *formate2 = [[NSDateFormatter alloc] init];
    [formate2 setDateFormat:@"MMM,dd,yyyy"];
    
    //[formate2 setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
    [formate2 setLocale:[NSLocale currentLocale]];//Steve
    [formate2 setDateStyle:NSDateFormatterNoStyle];//Steve
    [formate2 setTimeStyle:NSDateFormatterShortStyle];//Steve
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit| NSWeekdayCalendarUnit fromDate:destinationDate];
    NSInteger hour = [components hour];
    
    if(hour >= 0 && hour < 12)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good morning,"];
        NSLog(@"Good morning,");
    }
    else if(hour >= 12 && hour < 18)
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good afternoon,"];
        
        NSLog(@"Good afternoon,");
    }
    else if(hour >= 18 && hour < 24 )
    {
        self.strTextToSpeak = [NSMutableString stringWithString:@"Good evening,"];
        
        NSLog(@"Good evening,");
    }
    
    return self.strTextToSpeak;
    
}

#pragma mark - Date Calculations

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate
{
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setYear:numberOfYears];
    
    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    return newDate;
}


#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"*** numberOfSectionsInTableView ******");
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:0];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    EKEvent *event = [eventsOnThisDay objectAtIndex:0];
    
    //NSLog(@"event.eventIdentifier = %@", event.eventIdentifier);
    
    
    if (self.sections.count == 0) {
        return 1;
    }
    else{
        NSArray *unsortedDays = [self.sections allKeys];
        self.sortedDays = (NSMutableArray *)[unsortedDays sortedArrayUsingSelector:@selector(compare:)];
        
        return [self.sections count];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"**** numberOfRowsInSection ********");
    
    if (self.sections.count == 0) {
        return 0;
    }
    else
    {
        
        NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:section];
        NSArray *eventsOnThisDay1 = [self.sections objectForKey:dateRepresentingThisDay];
        
        return [eventsOnThisDay1 count];
    }
}


- (UIView *)tableView:(UITableView *) tableView viewForHeaderInSection:(NSInteger) section
{
    //NSLog(@"********** viewForHeaderInSection **************");
    // NSDateFormatter * formater = [[NSDateFormatter alloc]init];
    // [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    //  NSDate *dateRepresentingThisDay =[formater dateFromString:[self.sortedDays objectAtIndex:section]];
    
    if (self.sections.count == 0) {
        return 0;
    }
    
    
    NSDate *dateRepresentingThisDay;
    if ([self.sortedDays count]>0)
        dateRepresentingThisDay = [self.sortedDays objectAtIndex:section];
    
    // [self.sectionDateFormatter stringFromDate:dateRepresentingThisDay];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	UIImageView *secImageView = [[UIImageView alloc] init];
    
	[dateFormatter setDateFormat: @"MMM dd, yyyy"];
    
	NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	NSDate *currentDate = destinationDate;
	
	NSDate *dueDate = dateRepresentingThisDay;
	[dateFormatter setDateFormat: @"yyyy-MM-dd"];
	
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    UIImage *secImage;
    
    
    if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]) {
        
        if ([[dateFormatter stringFromDate: currentDate] isEqualToString: [dateFormatter stringFromDate:dueDate]])//Today's Date
        {
            
            if (IS_IOS_7){
                
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                    secImageView.backgroundColor = [UIColor colorWithRed:128.0/255.0f green:191.0/255.0f blue:255.0/255.0f alpha:1];
                }
                else{ //Dark Theme
                    
                    secImageView.backgroundColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:1];
                }
            }
            else{ //iOS 6
                
                secImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"BrouwnBar" ofType:@"png"]];
            }
           
        }
        else //Not Today's Date
        {
            
            if (IS_IOS_7) {
                if(IS_IPAD)
                {
                    [[self tblAppinmentList] setFrame:CGRectMake(0, 64, 768, 916)];
                    
                }
                if ([sharedDefaults floatForKey:@"DefaultAppThemeColorRed"] == 245) { //Light Theme
                    
                   secImageView.backgroundColor = [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0];
                }
                else{ //Dark Theme
                    
                    secImageView.backgroundColor = [UIColor grayColor];
                }
            }
            else{ //iOS 6
                
                secImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SectionHeaderGrey2iPhone" ofType:@"png"]];
            }
            
        }
        [secImageView setImage:secImage];
    }
    else{ //Picture Calendar
        
        if(IS_IPAD)
        {
            [[self tblAppinmentList] setFrame:CGRectMake(0, 64, 768, 916)];
        
        }
        if ([[dateFormatter stringFromDate: currentDate] isEqualToString: [dateFormatter stringFromDate:dueDate]]) //Today's Date
        {
            secImageView.backgroundColor = [UIColor colorWithRed:60.0/255.0f green:175.0/255.0f blue:255.0/255.0f alpha:0.80];
        }
        else
        {
            secImageView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:0.80];
   
        }
        
    }
    
	
    
    UILabel *weekdayNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 35, 22)];
	[weekdayNameLbl setTextColor:[UIColor whiteColor]];
	[weekdayNameLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 12.0]];
	[weekdayNameLbl setBackgroundColor:[UIColor clearColor]];
	[dateFormatter setDateFormat:@"eee"];
    [weekdayNameLbl setText:[dateFormatter stringFromDate:dateRepresentingThisDay]];
	[secImageView addSubview: weekdayNameLbl];
	
    UILabel *dateStlLbl;
    if(IS_IPAD)
        dateStlLbl = [[UILabel alloc] initWithFrame:CGRectMake(768 - 90, 0, 100, 22)];
    else
        dateStlLbl = [[UILabel alloc] initWithFrame:CGRectMake(320 - 90, 0, 100, 22)];
	[dateStlLbl setTextColor:[UIColor whiteColor]];
	[dateStlLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]];
	[dateStlLbl setBackgroundColor:[UIColor clearColor]];
    
    //[dateFormatter setDateFormat:@"MMM dd, yyyy"];//Steve commented - doesn't show correct on Different Regions like Europe, etc.
    [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];//Steve
    
	[dateStlLbl setText:[dateFormatter stringFromDate:dateRepresentingThisDay] ];//
	[secImageView addSubview: dateStlLbl];
	
	UILabel *sectTextLbl = [[UILabel alloc]initWithFrame:CGRectMake(weekdayNameLbl.frame.size.width + dateStlLbl.frame.size.width - 5, 0, 150, 22)];
	[sectTextLbl setTextColor:[UIColor whiteColor]];
	[sectTextLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 12.0]];
	[sectTextLbl setBackgroundColor:[UIColor clearColor]];
	[sectTextLbl setText:@""];
	[secImageView addSubview: sectTextLbl];
    
    
    
    if ( ![sharedDefaults boolForKey:@"CalendarTypeClassic"] ) { //Picture Calendar
        
        [weekdayNameLbl setTextColor:[UIColor blackColor]];
        [dateStlLbl setTextColor:[UIColor blackColor]];
        [sectTextLbl setTextColor:[UIColor blackColor]];
        
    }
    
    
	return secImageView;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"*** cellForRowAtIndexPath *****");
    
    CGFloat initialYPos = 0.0;
    static NSString *PlaceholderCellIdentifier = @"PlaceholderCell";
    
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    EKEvent *event = [eventsOnThisDay objectAtIndex:indexPath.row];
    
    // UITableViewCell *cell = nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PlaceholderCellIdentifier];
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if (cell == nil)
	{
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PlaceholderCellIdentifier];
        
        
        
        /////Alok Kumar Added
        if(event.location && ![event.location isEqualToString:@""]) {
			initialYPos = 60/2 - GENERAL_LABEL_HEIGHT/2;
		}else {
			initialYPos = 44/2 - GENERAL_LABEL_HEIGHT/2;
		}
        
        
        
        detailDiscloserImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DetailsDisclouserBtn" ofType:@"png"]];
        detailDiscloserImageView = [[UIImageView alloc] initWithImage:detailDiscloserImage];
        [detailDiscloserImageView setFrame:CGRectMake(tableView.frame.size.width - detailDiscloserImage.size.width - 5, 44/2 - 25/2, 21, 20)];
        
        //[cell.contentView addSubview: detailDiscloserImageView];
        // [cell setAccessoryView:detailDiscloserImageView];
        
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTag:60];
        [btn setBackgroundImage:detailDiscloserImage forState:UIControlStateNormal];
        [btn setFrame:CGRectMake(tableView.frame.size.width - detailDiscloserImage.size.width - 8, 44/2 - 20/2, 21, 20)];
        [btn addTarget: self
                action: @selector(btnAccessory:withEvent:)
      forControlEvents: UIControlEventTouchUpInside];
        
        [cell.contentView addSubview:btn];
        
        
        UIButton *btnCalColorIndicator = [[UIButton alloc] initWithFrame:CGRectMake(5, initialYPos+5, 20, 20)];
		[btnCalColorIndicator setUserInteractionEnabled:NO];
		[btnCalColorIndicator setTag:40];
		[btnCalColorIndicator.layer setCornerRadius:10.0];
		btnCalColorIndicator.layer.masksToBounds = YES;
		btnCalColorIndicator.layer.borderWidth = 1.5;
        btnCalColorIndicator.alpha = 0.65;
		[btnCalColorIndicator.layer setOpaque:YES];
		[btnCalColorIndicator setBackgroundColor:[UIColor clearColor]];
        
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"])
            btnCalColorIndicator.layer.borderColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:0.65f].CGColor;
        else
            btnCalColorIndicator.layer.borderColor = [UIColor whiteColor].CGColor;
        
        
        
		if (event.eventIdentifier)
            [cell.contentView addSubview:btnCalColorIndicator];
        
        if ([event.title isEqualToString:@""])
			btnCalColorIndicator.alpha =0;
        
        //[cell.contentView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f]]; //Steve commented
		
        UILabel *strtTimeLbl = [[UILabel alloc] initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 20, initialYPos +  GENERAL_LABEL_HEIGHT/2  - SMALL_LABEL_HEIGHT, 60 + 10, SMALL_LABEL_HEIGHT)];
        [strtTimeLbl setBackgroundColor:[UIColor clearColor]];
        [strtTimeLbl setTextAlignment:NSTextAlignmentCenter];
        [strtTimeLbl setTag:50];
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
            [strtTimeLbl setTextColor:[UIColor blackColor]];
            [strtTimeLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 12.0]];
        }
        else{ //iOS 6
            [strtTimeLbl setTextColor:[UIColor whiteColor]];
            [strtTimeLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 14.0]];
        }
        
        
        [cell.contentView addSubview: strtTimeLbl];
        
        UILabel *endTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 20, initialYPos +  GENERAL_LABEL_HEIGHT/2  - SMALL_LABEL_HEIGHT + SMALL_LABEL_HEIGHT, 60 + 10, SMALL_LABEL_HEIGHT)];
        [endTimeLbl setBackgroundColor:[UIColor clearColor]];
        [endTimeLbl setTextAlignment:NSTextAlignmentCenter];
        [endTimeLbl setTag:5050];
        
        if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
            [endTimeLbl setTextColor:[UIColor blackColor]];
            [endTimeLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 12.0]];
        }
        else{ //iOS 6
            [endTimeLbl setTextColor:[UIColor whiteColor]];
            [endTimeLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 14.0]];
        }
        
        [cell.contentView addSubview: endTimeLbl];
        
        
        
        if (event.location && ![event.location isEqualToString:@""]) {
            
            UILabel *aTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 2 + [cell.contentView viewWithTag:50].frame.size.width +  50 , 3, 150, GENERAL_LABEL_HEIGHT)];
           
            [aTitleLabel setTag:20];
            [aTitleLabel setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:aTitleLabel];
 			
			UILabel *anotes = [[UILabel alloc] initWithFrame:CGRectMake(btnCalColorIndicator.frame.size.width + 2 + [cell.contentView viewWithTag:50].frame.size.width +  50 ,GENERAL_LABEL_HEIGHT + 3, 150, 20)];
			[anotes setTag:30];
			[anotes setBackgroundColor:[UIColor clearColor]];
            
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
                [anotes setTextColor:[UIColor grayColor]];
                [anotes setFont:[UIFont fontWithName:@"Helvetica" size: 12.0]];
                
                [aTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 13.0]];
                [aTitleLabel setTextColor:[UIColor blackColor]];
            }
            else{ //Picture Calendar
                [anotes setTextColor:[UIColor whiteColor]];
                [anotes setFont:[UIFont fontWithName:@"Helvetica" size: 14.0]];
                
                [aTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 16.0]];
                [aTitleLabel setTextColor:[UIColor whiteColor]];
            }
            
			[cell.contentView addSubview:anotes];
            
		}
		else
		{
            UILabel *aTitleLabel = [[UILabel alloc] init];
            if(IS_IPAD)
            {
                aTitleLabel.frame = CGRectMake(btnCalColorIndicator.frame.size.width + 2 + [cell.contentView viewWithTag:50].frame.size.width +  50, initialYPos, 350, GENERAL_LABEL_HEIGHT);
            }
            else
            {
                aTitleLabel.frame = CGRectMake(btnCalColorIndicator.frame.size.width + 2 + [cell.contentView viewWithTag:50].frame.size.width +  50, initialYPos, 150, GENERAL_LABEL_HEIGHT);
            }
        
            [aTitleLabel setTag:20];
            
            [cell.contentView addSubview:aTitleLabel];
            [aTitleLabel setBackgroundColor:[UIColor clearColor]];
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
                [aTitleLabel setTextColor:[UIColor blackColor]];
                [aTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 13.0]];
            }
            else{ //Picture Calendar
                [aTitleLabel setTextColor:[UIColor whiteColor]];
                [aTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size: 16.0]];
            }
            
		}
        
    }
    
    
    if([self IsHandwritingEvent:event.title])
    {
        NSData * imgdata;
        
        if ([[HWImagesDic allKeys] containsObject:event.eventIdentifier])
        {
            imgdata = [HWImagesDic objectForKey:event.eventIdentifier];
        }
        else{
            imgdata = [self SetHandWringImage:event.eventIdentifier];
            
            if(imgdata == nil){
                NSString *dataStr = @"";
                imgdata = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
            }
            
            //Steve - crashes if eventIdentifier is nil
            if (event.eventIdentifier != nil) {
                [HWImagesDic setObject:imgdata forKey:event.eventIdentifier];
            }
            
            
        }
        
        if([imgdata length]>0){
            //   UIImageView* aImageView=(UIImageView*)[cell.contentView viewWithTag:10];
            //   UIImage* aImage=[[UIImage alloc]initWithData:imgdata];
            //  [aImageView setImage:aImage];
            
            
            
            if ([sharedDefaults boolForKey:@"CalendarTypeClassic"]){
                
                UIImage *img = [[UIImage alloc]initWithData:imgdata];
                UILabel* aTitleLabel=(UILabel*)[cell.contentView viewWithTag:20];
                [aTitleLabel setText:event.title];
                CGSize imgSize = aTitleLabel.frame.size;
                
                UIGraphicsBeginImageContext( imgSize );
                [img drawInRect:CGRectMake(0,0,imgSize.width,imgSize.height)];
                UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                [aTitleLabel setBackgroundColor:[UIColor colorWithPatternImage:newImage]];
                [aTitleLabel setText:@""];
                
            }
            else{ //iOS 6
                
                UIImage* aImage = [UIImage new];
                aImage = [self invertHWColor:imgdata]; //method that inverts HW image
                //[aImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];//Steve added
                //[aImageView setImage:aImage];
                
                //UIImage *img = [[UIImage alloc]initWithData:aImage];
                UIImage *img = aImage;
                UILabel* aTitleLabel=(UILabel*)[cell.contentView viewWithTag:20];
                [aTitleLabel setText:event.title];
                CGSize imgSize = aTitleLabel.frame.size;
                
                UIGraphicsBeginImageContext( imgSize );
                [img drawInRect:CGRectMake(0,0,imgSize.width,imgSize.height)];
                UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                [aTitleLabel setBackgroundColor:[UIColor colorWithPatternImage:newImage]];
                [aTitleLabel setText:@""];
                
                
                
            }
            
            
            
        }else{
            UILabel* aTitleLabel=(UILabel*)[cell.contentView viewWithTag:20];
            [aTitleLabel setBackgroundColor:[UIColor clearColor]];
            
            [aTitleLabel setText:event.title];
        }
    }
    else{
        
        UILabel* aTitleLabel=(UILabel*)[cell.contentView viewWithTag:20];
        [aTitleLabel setText:event.title];
        [aTitleLabel setBackgroundColor:[UIColor clearColor]];
    }
    
    
    UILabel* anotes = (UILabel*)[cell.contentView viewWithTag:30];
    [anotes setText:event.location];
    
    UIButton *btnCalColorIndicator = (UIButton*)[cell.contentView viewWithTag:40];
    [btnCalColorIndicator setBackgroundColor:[UIColor colorWithCGColor:event.calendar.CGColor]];
    [btnCalColorIndicator setAlpha:0.85]; //Steve - dim color slightly
    
    if(!event.eventIdentifier)[btnCalColorIndicator setAlpha:0];
    else [btnCalColorIndicator setAlpha:1];
    /////
    
    NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    
    NSDateFormatter* formatte = [[NSDateFormatter alloc] init];
    [formatte setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    // NSDate* strtDatetime = [formatte dateFrom:event.startDate];
    
    //[formatte setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
    [formatte setLocale:[NSLocale currentLocale]];//Steve
    [formatte setDateStyle:NSDateFormatterNoStyle];//Steve
    [formatte setTimeStyle:NSDateFormatterShortStyle];//Steve
    
    NSDate *startDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:event.startDate];
    NSString *strtTime = [formatte stringFromDate:startDate];
    UILabel* strtTimeLbl = (UILabel*)[cell.contentView viewWithTag:50];
    if(event.eventIdentifier)
        [strtTimeLbl setText:strtTime];
    else [strtTimeLbl setText:@""];
    
    [formatte setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    // NSDate* endDatetime = [formatte dateFromString:appObj.dueDateTime];
    
    //[formatte setDateFormat:@"h:mm a"];//Steve commented.  Doesn't work with 24 hr clock & International Regional Settings
    [formatte setLocale:[NSLocale currentLocale]];//Steve
    [formatte setDateStyle:NSDateFormatterNoStyle];//Steve
    [formatte setTimeStyle:NSDateFormatterShortStyle];//Steve
    
    NSDate *endDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:event.endDate];
    NSString *endTime = [formatte stringFromDate:endDate];
	
    UILabel* endTimeLbl = (UILabel *)[cell.contentView viewWithTag:5050];
    if(event.eventIdentifier)
        [endTimeLbl setText:endTime];
    else [endTimeLbl setText:@""];
    if (!event.eventIdentifier)
        [[cell.contentView viewWithTag:60] setAlpha:0];
    else
        [[cell.contentView viewWithTag:60] setAlpha:1];
    
    if (event.location && ![event.location isEqualToString:@""])
    {
        //pooja-iPad
        if(IS_IPAD)
        {
            
            [btn removeFromSuperview];
            
            detailDiscloserImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DetailsDisclouserBtn" ofType:@"png"]];
            detailDiscloserImageView = [[UIImageView alloc] initWithImage:detailDiscloserImage];
            [detailDiscloserImageView setFrame:CGRectMake(768 - detailDiscloserImage.size.width - 5, 60/2 - 25/2, 21, 20)];
            
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTag:60];
            [btn setBackgroundImage:detailDiscloserImage forState:UIControlStateNormal];
            [btn setFrame:CGRectMake(tableView.frame.size.width - detailDiscloserImage.size.width - 8, 60/2 - 20/2, 21, 20)];
            [btn addTarget: self
                    action: @selector(btnAccessory:withEvent:)
          forControlEvents: UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:btn];
        }

       else
           [[cell.contentView viewWithTag:60] setFrame:CGRectMake(tableView.frame.size.width - 21 - 8, 60/2 - 20/2, 21, 20)];
    }
    else
    {
       //pooja-iPad
        if(IS_IPAD)
        {
            //NSLog(@"tableview frame is : %@",NSStringFromCGRect(tblAppinmentList.frame));
            [btn removeFromSuperview];
            
            detailDiscloserImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DetailsDisclouserBtn" ofType:@"png"]];
            detailDiscloserImageView = [[UIImageView alloc] initWithImage:detailDiscloserImage];
            [detailDiscloserImageView setFrame:CGRectMake(tableView.frame.size.width - detailDiscloserImage.size.width - 5, 44/2 - 25/2, 21, 20)];
            
            //[cell.contentView addSubview: detailDiscloserImageView];
            // [cell setAccessoryView:detailDiscloserImageView];
            
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTag:60];
            [btn setBackgroundImage:detailDiscloserImage forState:UIControlStateNormal];
            [btn setFrame:CGRectMake(768 - detailDiscloserImage.size.width - 8, 44/2 - 20/2, 21, 20)];
            [btn addTarget: self
                    action: @selector(btnAccessory:withEvent:)
          forControlEvents: UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:btn];
        }
        else
            [[cell.contentView viewWithTag:60] setFrame:CGRectMake(tableView.frame.size.width - 21 - 8, 44/2 - 20/2, 21, 20)];

        
    }
    
    [cell setSelectionStyle:UITableViewCellEditingStyleNone];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"*** heightForRowAtIndexPath *****");
    
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    EKEvent *event = [eventsOnThisDay objectAtIndex:indexPath.row];
	
	if(event.location && ![event.location isEqualToString:@""])
	{
		return 60;
	}
	else
	{
		return 44;
	}
	return 44;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IOS_7) {
        
        NSUserDefaults *sharedDefualts = [NSUserDefaults standardUserDefaults];
        
        if ( [sharedDefualts boolForKey:@"CalendarTypeClassic"]) { //Classic Calendar
            
            cell.backgroundColor = [UIColor whiteColor];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            tableView.backgroundColor = [UIColor whiteColor];
        }
        else{ //Picture Calendar
            
            cell.backgroundColor = [UIColor clearColor];
            tableView.backgroundColor = [UIColor clearColor];
            
            
            if ([sharedDefualts boolForKey:@"CalendarClarityTable"]) { //Checks to see if Clarity event table view is on
                
                ////////////////// Steve - Adjust Clarity with semi-transparent black view //////////////////////
                NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
                float alphaOfBackground = [sharedDefaults floatForKey:@"CalendarClarityAlphaValue"];
                cell.contentView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha: alphaOfBackground];
            }
            else{
                
                cell.contentView.backgroundColor = [UIColor clearColor];
            }
            
        }
        
    }
    else{ //iOS 6
        
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f] ] ;
        [cell setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f] ] ;
        tableView.backgroundColor = [UIColor colorWithRed:244.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    EKEvent * SelEvent = [eventsOnThisDay objectAtIndex:indexPath.row];
    
    NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    AppointmentsDataObject* appObj = [[AppointmentsDataObject alloc]init];
    [appObj setTextLocName:SelEvent.location];
    [appObj setAppointmentTitle:SelEvent.title];
    [appObj setRelation_Field1:SelEvent.eventIdentifier];
    NSString *calName = [NSString stringWithFormat:@"%@~%@",SelEvent.calendar.title,SelEvent.calendar.calendarIdentifier];
    [appObj setCalendarName:calName];
    [appObj setShortNotes:SelEvent.notes];
    [appObj setTimeZoneName:SelEvent.timeZone];//Steve
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *startDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:SelEvent.startDate];
    NSDate *endDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:SelEvent.endDate];
    
    [appObj setStartDateTime:[formate stringFromDate:startDate]];
    [appObj setDueDateTime:[formate stringFromDate:endDate]];
    [appObj setIsAllDay:SelEvent.isAllDay];
    if([self IsHandwritingEvent:SelEvent.title])
        [appObj setAppointmentType:@"H"];
    else
        [appObj setAppointmentType:@"T"];
    //Alok Gupta Added For setting Repeat & alarms
    if([SelEvent.recurrenceRules count]>0){
        EKRecurrenceRule *rule = [SelEvent.recurrenceRules objectAtIndex:0];
        [appObj setRepeat:[self getfrequency:rule.frequency :rule.interval]];
    }else [appObj setRepeat:@"None"];
    
    if([SelEvent.alarms count]==1)
        [appObj setAlert:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:0]]];
    if([SelEvent.alarms count]==2){
        [appObj setAlert:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:0]]];
        [appObj setAlertSecond:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:1]]];
    }
    
    
    // Alok Gupta AddedFor Handwritting Appointment Data binary
    if([appObj.appointmentType isEqualToString:@"H"])
    {
        //////////////////////// SQL Query - Find Handwriting ///////////////////
        /////////////////////////////////////////////////////////////////////////
        sqlite3_stmt *selectAppointment_Stmt = nil;
        
        if(selectAppointment_Stmt == nil && appDelegate.database) {
            
            NSString *Sql = [[NSString alloc] initWithFormat: @"Select a.AppointmentID,a.AppointmentType, a.AppointmentBinary, a.AppointmentBinaryLarge From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", SelEvent.eventIdentifier];
            //NSLog(@"Sql->%@",Sql);
            if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
            {
                //NSLog(@"SQL FAILED");
                NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
            }
            else
            {
                while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
                {
                    [appObj setAppointmentID:sqlite3_column_int(selectAppointment_Stmt,0)];
                    NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 2);
                    NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 2) length:blobLength];
                    [appObj setAppointmentBinary:binaryData];
                    
                    blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 3);
                    NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 3) length:blobLength];
                    [appObj setAppointmentBinaryLarge:binaryDataLarge];
                    
                }//while end
                
            }//else end
            sqlite3_finalize(selectAppointment_Stmt);
            selectAppointment_Stmt = nil;
        }
        else
        {
            NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
    }
    
    
	ViewAppointmentScreen *viewAppointmentScreen = [[ViewAppointmentScreen alloc] initWithAppObject:appObj];
	if (SelEvent.eventIdentifier){
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"]){
            // Update  (Karan Changes)
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ViewAppointmentStoryboard" bundle:nil];
            UINavigationController *navigationController = [sb instantiateViewControllerWithIdentifier:@"ViewAppointmentStoryboard"];
            ViewAppointmentScreen *viewAppointmentScreen = navigationController.viewControllers[0];
            viewAppointmentScreen.appDataObj = appObj; //Sends data to appDataObj
            
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        
        else{
            [appDelegate.navigationController pushViewController:viewAppointmentScreen animated:YES];
        }
        
    }
    
    
}


//Steve - inverts the HW image color so Black is While & Blue is Yellow, for example
-(UIImage*)invertHWColor:(NSData*)image{
    
    CIImage *newImage = [CIImage imageWithData:image];// Load the image as a CIImage
    CIFilter *invertColour = [CIFilter filterWithName:@"CIColorInvert" keysAndValues:@"inputImage", newImage, nil];   // Create a CIFilter using the image and one of Apple's document filters
    CIImage *filteredImage = [invertColour outputImage];  // Create a pointer to the image output by the filter
    UIImage *newImg = [UIImage imageWithCIImage:filteredImage];
    
    
    return newImg;
}

-(NSData *)SetHandWringImage:(NSString *) eventIdentifier{
    
    ///Alok Gupta AddedFor Handwritting Appointment Data binary
    if(eventIdentifier)
    {
        //////////////////////// SQL Query - Find Handwriting ///////////////////
        /////////////////////////////////////////////////////////////////////////
        sqlite3_stmt *selectAppointment_Stmt = nil;
        if(selectAppointment_Stmt == nil && appDelegate.database) {
            
            NSString *Sql = [[NSString alloc] initWithFormat: @"Select  a.AppointmentBinary From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", eventIdentifier];
            if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
            {
                // NSLog(@"SQL FAILED");
                NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
            }
            else
            {
                if(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
                {
                    
                    NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 0);
                    NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 0) length:blobLength];
                    
                    return binaryData;
                    
                }
                
            }//else end
            sqlite3_finalize(selectAppointment_Stmt);
            selectAppointment_Stmt = nil;
        }
        else
        {
            NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
    }
    return NO;
}


- (void) btnAccessory: (UIControl *) button withEvent: (UIEvent *) event
{
    NSIndexPath * indexPath = [self->tblAppinmentList indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self->tblAppinmentList]];
    [self->tblAppinmentList deselectRowAtIndexPath:indexPath animated:YES];
	
	/*NSInteger row;	  //= [indexPath row];
     NSInteger section = [indexPath section];
     
     if (section == 0) {
     row = [indexPath row];
     }else {
     NSInteger tempInt = 0;
     for (int i = 0; i < section; i++) {
     tempInt = tempInt + [self->tblAppinmentList numberOfRowsInSection:i];
     }
     row = tempInt + [indexPath row];
     }*/
	
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    EKEvent * SelEvent = [eventsOnThisDay objectAtIndex:indexPath.row];
    
    NSDate *sourceDate = [NSDate date];
	NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    AppointmentsDataObject* appObj = [[AppointmentsDataObject alloc]init];
    [appObj setTextLocName:SelEvent.location];
    [appObj setAppointmentTitle:SelEvent.title];
    [appObj setRelation_Field1:SelEvent.eventIdentifier];
    NSString *calName = [NSString stringWithFormat:@"%@~%@",SelEvent.calendar.title,SelEvent.calendar.calendarIdentifier];
    [appObj setCalendarName:calName];
    [appObj setShortNotes:SelEvent.notes];
    NSDateFormatter *formate = [[NSDateFormatter alloc] init];
    [formate setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *startDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:SelEvent.startDate];
    NSDate *endDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:SelEvent.endDate];
    
    [appObj setStartDateTime:[formate stringFromDate:startDate]];
    [appObj setDueDateTime:[formate stringFromDate:endDate]];
    [appObj setIsAllDay:SelEvent.isAllDay];
    if([self IsHandwritingEvent:SelEvent.title])
        [appObj setAppointmentType:@"H"];
    else
        [appObj setAppointmentType:@"T"];
    //Alok Gupta Added For setting Repeat & alarms
    if([SelEvent.recurrenceRules count]>0){
        EKRecurrenceRule *rule = [SelEvent.recurrenceRules objectAtIndex:0];
        [appObj setRepeat:[self getfrequency:rule.frequency :rule.interval]];
    }else [appObj setRepeat:@"None"];
    
    if([SelEvent.alarms count]==1)
        [appObj setAlert:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:0]]];
    if([SelEvent.alarms count]==2){
        [appObj setAlert:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:0]]];
        [appObj setAlertSecond:[self EKAlarmToStr:[SelEvent.alarms objectAtIndex:1]]];
    }
    
    
    ///Alok Gupta AddedFor Handwritting Appointment Data binary
    if([appObj.appointmentType isEqualToString:@"H"])
    {
        //////////////////////// SQL Query - Find Handwriting ///////////////////
        /////////////////////////////////////////////////////////////////////////
        sqlite3_stmt *selectAppointment_Stmt = nil;
        
        if(selectAppointment_Stmt == nil && appDelegate.database) {
            
            NSString *Sql = [[NSString alloc] initWithFormat: @"Select a.AppointmentID,a.AppointmentType, a.AppointmentBinary, a.AppointmentBinaryLarge From AppointmentsTable a  where a.relation_Field1 like '%%%@%%'   Order By a.StartDateTime Asc", SelEvent.eventIdentifier];
            //NSLog(@"Sql->%@",Sql);
            if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectAppointment_Stmt, nil) != SQLITE_OK)
            {
                // NSLog(@"SQL FAILED");
                NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
            }
            else
            {
                while(sqlite3_step(selectAppointment_Stmt) == SQLITE_ROW)
                {
                    [appObj setAppointmentID:sqlite3_column_int(selectAppointment_Stmt,0)];
                    NSUInteger blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 2);
                    NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 2) length:blobLength];
                    [appObj setAppointmentBinary:binaryData];
                    
                    blobLength = sqlite3_column_bytes(selectAppointment_Stmt, 3);
                    NSData *binaryDataLarge = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAppointment_Stmt, 3) length:blobLength];
                    [appObj setAppointmentBinaryLarge:binaryDataLarge];
                    
                }//while end
                
            }//else end
            sqlite3_finalize(selectAppointment_Stmt);
            selectAppointment_Stmt = nil;
        }
        else
        {
            NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(appDelegate.database));
        }
    }
    
    
	ViewAppointmentScreen *viewAppointmentScreen = [[ViewAppointmentScreen alloc] initWithAppObject:appObj];
	if (SelEvent.eventIdentifier){
        
        NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([sharedDefaults boolForKey:@"NavigationNew"])
            [self.navigationController pushViewController:viewAppointmentScreen animated:YES];
        else
            [appDelegate.navigationController pushViewController:viewAppointmentScreen animated:YES];
        
    }
    
}


#pragma mark - Get Alarm & Repeat Event Strings
-(NSString *)EKAlarmToStr:(EKAlarm*)ekAlarm{
    NSTimeInterval interval = [ekAlarm relativeOffset];
    interval = abs(interval);
    if (interval>0)
    {  NSString *strTime = @"";
        if (interval == D_MINUTE * 5)
        {
            strTime = @"5 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 10)
        {
            strTime = @"10 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 15)
        {
            strTime = @"15 Minutes Before";
            return strTime;
        }
        else if (interval == D_MINUTE * 30)
        {
            strTime = @"30 Minutes Before";
            return strTime;
        }
        else if (interval == D_HOUR)
        {
            strTime = @"1 Hour Before";
            return strTime;
        }
        else if (interval == D_HOUR * 2)
        {
            strTime = @"2 Hour Before";
            return strTime;
        }
        else if (interval == D_DAY)
        {
            strTime = @"1 Day Before";
            return strTime;
        }
        else if (interval == D_DAY * 2)
        {
            strTime = @"2 Day Before";
            return strTime;
        }
    }
    else
    {
        NSDate *alertdate = [ekAlarm absoluteDate];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        //[dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"]; //Steve comented - not international compatible
        [dateFormatter setLocale:[NSLocale currentLocale]];//Steve
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];//Steve
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];//Steve
        
        //Steve - EKAlarm is in GMT time zone. This displays converts it to Local time zone so it displays correct
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        NSString *alertDate =[dateFormatter stringFromDate:alertdate];
        return alertDate;
    }
    
    return @"None";
}
-(NSString *)getfrequency:(int) freq:(int) interval{
    if(freq == 0)
        return @"Every Day";
    else if(freq == 1 && interval == 2)
        return @"Every 2 Week";
    else if(freq == 1)
        return @"Every Week";
    else if(freq == 2)
        return @"Every Month";
    else if(freq == 3)
        return @"Every Year";
    else return @"Never";
}

@end



#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (RichTextEditor)

- (UIColor *)colorOfPoint:(CGPoint)point;
- (UIViewController *)firstAvailableViewController;

@end

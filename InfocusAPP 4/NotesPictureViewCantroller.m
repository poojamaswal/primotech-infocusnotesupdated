//
//  NotesPictureViewCantroller.m
//  Organizer
//
//  Created by Naresh Chauhan on 8/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NotesPictureViewCantroller.h"
#import "OrganizerAppDelegate.h"

@implementation NotesPictureViewCantroller

@synthesize theNewTable;
@synthesize bkbutton;
@synthesize GETNotesObject;
@synthesize GetnotesID,getAllImageArray;



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidLoad 
{
    [super viewDidLoad];
	NSLog(@"=====>%d",GetnotesID);
	
	getAllImageArray  = [[NSMutableArray alloc]init];
	
	[self SelectImgNotes:GetnotesID];
}



-(void)SelectImgNotes:(NSInteger)NodID
{
	
	
	NSLog(@"%d",NodID);
	OrganizerAppDelegate  *appDelegate = (OrganizerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
	
	sqlite3_stmt *selectNotes_Stmt = nil; 
	
	if(selectNotes_Stmt == nil && appDelegate.database) 
	{	
		
		NSString *Sql = [[NSString alloc] initWithFormat:@"SELECT ImgID ,ImgInfo FROM NotesImgInfo WHERE NotesID = %d",NodID];
		
		NSLog(@" query %@",Sql);
		if(sqlite3_prepare_v2(appDelegate.database, [Sql UTF8String], -1, &selectNotes_Stmt, nil) != SQLITE_OK) 
		{
			NSAssert1(0, @"Error: Failed to get the values from database with message '%s'.", sqlite3_errmsg(appDelegate.database));
		}
		else
		{
			[getAllImageArray removeAllObjects];
			while(sqlite3_step(selectNotes_Stmt) == SQLITE_ROW) 
			{
				ImgDataObject *imgdataobj=[[ImgDataObject alloc]init];
				
				[imgdataobj setImgid: sqlite3_column_int(selectNotes_Stmt, 0)];
				
				NSUInteger blobLength = sqlite3_column_bytes(selectNotes_Stmt, 1);
				
				NSData *binaryData = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectNotes_Stmt, 1) length:blobLength];
				
				[imgdataobj setImginfo:binaryData];
				
				[getAllImageArray addObject:imgdataobj];
				//[imgdataobj release]; //Steve for ARC
				//[binaryData release]; //Steve for ARC
				
			}							        
			sqlite3_finalize(selectNotes_Stmt);		
			selectNotes_Stmt = nil;        
		}
		//[Sql release]; //Steve for ARC
	}	
}

/*
//Override to allow orientations other than the default portrait orientation.
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
// Return YES for supported orientations.
return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	return 130;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{	
	return [getAllImageArray count];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
	
	//return [getValueArray count];
	return 1;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = 
	[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
        
		//cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease]; //Steve for ARC
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier]; //Steve for ARC
	}	
	
	
	for (UIView *subView in [cell.contentView subviews])
	{
		[subView removeFromSuperview];
	}
	
	
	
	UIImage *titleImage = [[UIImage alloc ] initWithData:
						   [[getAllImageArray objectAtIndex:indexPath.section]imginfo]];
	UIButton *titleImageView = [[UIButton alloc] initWithFrame:CGRectMake(23, 13, 250, 103)];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateNormal];
	[titleImageView setBackgroundImage:titleImage forState:UIControlStateHighlighted];
	[titleImageView.layer setBorderWidth:1.0];
	[titleImageView.layer setCornerRadius:5.0];
	titleImageView.layer.masksToBounds = YES;
	[titleImageView.layer setBorderColor:[[UIColor colorWithWhite:0.3 alpha:0.7] CGColor]];
	[cell.contentView addSubview:titleImageView];
	//[titleImage release]; //Steve for ARC
	//[titleImageView release]; //Steve for ARC
	
		
	return cell;
	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{	
	//NotesFullViewCantroller *notesFullViewCantroller = [[NotesFullViewCantroller alloc] initWithNibName:@"NotesFullViewCantroller" bundle:nil];
	//NSUInteger section = [indexPath section];
	////notesFullViewCantroller.fetchNotesObject=[getValueArray objectAtIndex:section];	
	//[[self navigationController] pushViewController:notesFullViewCantroller animated:YES];
	//[notesFullViewCantroller release];
	
}

-(IBAction)BackButton_Clicked:(id)sender 
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//Steve commented for ARC
/*
- (void)dealloc
{
    [super dealloc];
}
*/

@end

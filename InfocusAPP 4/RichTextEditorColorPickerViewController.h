

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIView+RichTextEditor.h"

typedef enum {
	RichTextEditorColorPickerActionTextForegroudColor,
	RichTextEditorColorPickerActionTextBackgroundColor
}RichTextEditorColorPickerAction;

@protocol RichTextEditorColorPickerViewControllerDelegate <NSObject>
- (void)richTextEditorColorPickerViewControllerDidSelectColor:(UIColor *)color withAction:(RichTextEditorColorPickerAction)action;
- (void)richTextEditorColorPickerViewControllerDidSelectClose;
@end

@protocol RichTextEditorColorPickerViewControllerDataSource <NSObject>
- (BOOL)richTextEditorColorPickerViewControllerShouldDisplayToolbar;
@end

@interface RichTextEditorColorPickerViewController : UIViewController

@property (nonatomic, weak) id <RichTextEditorColorPickerViewControllerDelegate> delegate;
@property (nonatomic, weak) id <RichTextEditorColorPickerViewControllerDataSource> dataSource;
@property (nonatomic, assign) RichTextEditorColorPickerAction action;
@property (nonatomic, strong) UIImageView *colorsImageView;
@property (nonatomic, strong) UIView *selectedColorView;

- (IBAction)doneSelected:(id)sender;
- (IBAction)closeSelected:(id)sender;

@end
